<div align="center">
  <a href="https://atim-software.ca/en/home/" target="_blank">
      <img alt="ATiM | The Advanced Tissue Management Application" src="https://atim-software.ca/wp-content/uploads/2019/05/Logo_ATIM_CMYK.png" width="400" />

  </a>
</div>


## ATiM (The Advanced Tissue Management Application)                                      

ATiM is a Biobank Information Management System (BIMS) designed and developed by
CTRNet and the CRCHUM in conjunction with the canadian biobanks for the operation 
of biobanks. 

## Introduction

The Canadian Tissue Repository Network (CTRNet) is a non-profit association of 
Canadian tissue biorepositories that aims to support health research using human 
biospecimens through initiatives that improve and promote access, quality, 
and standardization in biobanking. 

Part of CTRNet’s strategy to achieve this goal has been to participate to the 
development of the Advanced Tissue Management software (ATiM), a Biobank 
Information Management System (BIMS) in collaboration with the CRCHUM. 

ATiM was designed and developed by CTRNet and the CRCHUM, in conjunction with 
canadian biobanking experts, to support biobank operations.

## Licensing

ATiM is released for free non-commercial use under GPL v3. See the file 
called LICENSE.txt in the root directory.

## Documentation

### ATiM
  
Please visit our [ATiM site](http://atim-software.ca/) to get general information.

### CTRNet

For any inforamtion about CTRNet, the Canadian Tissu Repository Netwrok, please visit the [CTRNet website](http://ctrnet.ca).

### OBER

To get educational, operational, and research tools for researchers and biobankers who collect, store, or study human biospecimens, we recommand you to visit the [Biobank Resource Centre (BRC)](http://biobanking.org), our partener.

### CRCHUM

For any inforamtion about the CRCHUM, the Centre de recherche du Centre hospitalier de l’Université de Montréal, please visit the [CRCHUM](http://crchum.chumontreal.qc.ca/en) website.

### Installation
------------------------------------------------------------------------------------

Please visit our ATiM website access the [system requirements](http://atim-software.ca/en/system-requirements/) page.

### Contact
------------------------------------------------------------------------------------


[ATiM](http://atim-software.ca/en/contact/) : [info@atim-software.ca](info@atim-software.ca)

[CTRNet](http://ctrnet.ca/contact) : [info@ctrnet.ca](info@ctrnet.ca)