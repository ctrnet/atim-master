<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class StructureValidation
 */
class StructureValidation extends AppModel
{

    public $name = 'StructureValidation';

    public function setDataBeforeSaveFB(&$data, $options = array())
    {
        $flagBatchAction = !empty($data['InventoryActionControl']['flag_batch_action']);
        $validationData = json_decode($data["validationData"], true);
        $validationNormalizedData = array();
        if (isset($options["prefix-common"])) {
            $dataCommon = isset($data[$options["prefix-common"]]) ? $data[$options["prefix-common"]] : array();
            $index = 0;
            foreach ($dataCommon as $key => &$value) {
                if (is_numeric($key)) {
                    if($value['StructureField']['type'] =="validateIcd10WhoCode"){
                        $errorMessage = "invalid disease code";
                        $setting = "size=10,url=/CodingIcd/CodingIcd10s/autoComplete/who,tool=/CodingIcd/CodingIcd10s/tool/who";
                    }elseif($value['StructureField']['type'] =="validateIcdo3MorphoCode"){
                        $errorMessage = "invalid morphology code";
                        $setting = "size=10,url=/CodingIcd/CodingIcdo3s/autoComplete/morpho,tool=/CodingIcd/CodingIcdo3s/tool/morpho";
                    }elseif($value['StructureField']['type'] =="validateIcdo3TopoCode"){
                        $errorMessage = "invalid topography code";
                        $setting = "size=10,url=/CodingIcd/CodingIcdo3s/autoComplete/topo,tool=/CodingIcd/CodingIcdo3s/tool/topo";
                    }
                    if (in_array($value['StructureField']['type'], array("validateIcd10WhoCode", "validateIcdo3MorphoCode", "validateIcdo3TopoCode", "validateIcd10CaCode"))!==false){
                        $validationNormalizedData[] = array(
                            "structure_field_id" => $index,
                            "rule" => $value['StructureField']['type'],
                            "language_message" => $errorMessage
                        );
//                        $value['StructureField']['type'] = "autocomplete";
                        $value['StructureField']['setting'] = $setting;
                    }
                    $validation = $validationData[$index];
                    if (is_array($validation)){
                        foreach ($validation as $k1 => $v1){
                            if ($v1['name'] == "data[FunctionManagement][between_from]"  && isset($v1['value']) && $v1['value'] != ""){
                                $vFrom = $v1['value'];
                                $vTo = "";
                                foreach ($validation as $k2 => $v2){
                                    if ($v2['name'] == "data[FunctionManagement][between_to]"){
                                        $vTo = $v2['value'];
                                        unset($validation[$k2]);
                                        unset($validation[$k1]);
                                        break;
                                    }
                                }
                                if (!empty($vTo)){
                                    $validationNormalizedData[] = array(
                                        "structure_field_id" => $index,
                                        "rule" => "between,$vFrom,$vTo",
                                        "language_message" => __("error-the %s length should be between %s and %s", $value['StructureField']['language_label'], $vFrom, $vTo)
                                    );
                                }
                            }
                            if ($v1['name'] == "data[FunctionManagement][range_from]" && isset($v1['value']) && $v1['value'] != ""){
                                $vFrom = $v1['value'];
                                $vTo = "";
                                foreach ($validation as $k2 => $v2){
                                    if ($v2['name'] == "data[FunctionManagement][range_to]"){
                                        $vTo = $v2['value'];
                                        unset($validation[$k2]);
                                        unset($validation[$k1]);
                                        break;
                                    }
                                }
                                $vf=0;
                                $vt=0;
                                if (in_array($value['StructureField']['type'], array("float1", "float2", "float5")) !== false) {
                                    $n = pow(10, -intval(substr($value['StructureField']['type'], 5, 1)));
                                    
                                    $vf = $vFrom - $n;
                                    $vt = $vTo + $n;
                                }else{
                                    $vf = $vFrom - 1;
                                    $vt = $vTo + 1;
                                }
                                if ($vf>$vt){
                                    $tmp = $vf;
                                    $vf = $vt;
                                    $vt = $tmp;
                                    $tmp = $vFrom;
                                    $vFrom = $vTo;
                                    $vTo = $tmp;
                                }

                                if (!empty($vTo)){
                                    $validationNormalizedData[] = array(
                                        "structure_field_id" => $index,
                                        "rule" => "range,$vf,$vt",
                                        "language_message" => __("error-the %s should be between %s and %s", $value['StructureField']['language_label'], $vFrom, $vTo)
                                    );
                                }
                            }
                            if($v1['name'] == "data[FunctionManagement][is_unique]"  && isset($v1['value']) && $v1['value'] == 1){
                                $validationNormalizedData[] = array(
                                    "structure_field_id" => $index,
                                    "rule" => "isUnique",
                                    "language_message" => __("this field must be unique")
                                );
                                if ($flagBatchAction){
                                    $this->validationErrors['language_label'][] = 'fb_ the form cannot be used in batch action and have unique field at the same time';
                                }
                            }

                            if($v1['name'] == "data[FunctionManagement][not_blank]" && isset($v1['value']) && $v1['value'] ==1){
                                $validationNormalizedData[] = array(
                                    "structure_field_id" => $index,
                                    "rule" => "notBlank",
                                    "language_message" => __("this field is required")
                                );
                            }
                        }
                    }
                    $index++;
                }
            }
            unset ($data["validationData"]);
            $data["StructureValidation"] = $validationNormalizedData;
            $data[$options["prefix-common"]] = $dataCommon;
        }
    }

    public function setDataBeforeEditFB(&$data)
    {
        $flagBatchAction = !empty($data['InventoryActionControl']['flag_batch_action']);
        $validationData = json_decode($data["validationData"], true);

        $validationNormalizedData = array();
        $index = 0;
        foreach (array('masterTest', 'detailTest') as $k){
            foreach ($data[$k] as $key => &$value) {
                if (is_numeric($key)) {
                    if($value['StructureField']['type'] =="validateIcd10WhoCode"){
                        $errorMessage = "invalid disease code";
                        $setting = "size=10,url=/CodingIcd/CodingIcd10s/autoComplete/who,tool=/CodingIcd/CodingIcd10s/tool/who";
                    }elseif($value['StructureField']['type'] =="validateIcdo3MorphoCode"){
                        $errorMessage = "invalid morphology code";
                        $setting = "size=10,url=/CodingIcd/CodingIcdo3s/autoComplete/morpho,tool=/CodingIcd/CodingIcdo3s/tool/morpho";
                    }elseif($value['StructureField']['type'] =="validateIcdo3TopoCode"){
                        $errorMessage = "invalid topography code";
                        $setting = "size=10,url=/CodingIcd/CodingIcdo3s/autoComplete/topo,tool=/CodingIcd/CodingIcdo3s/tool/topo";
                    }
                    if (in_array($value['StructureField']['type'], array("validateIcd10WhoCode", "validateIcdo3MorphoCode", "validateIcdo3TopoCode", "validateIcd10CaCode"))!==false){
                        $validationNormalizedData[] = array(
                            "structure_field_id" => $index,
                            "rule" => $value['StructureField']['type'],
                            "language_message" => $errorMessage
                        );
                        //$value['StructureField']['type'] = "autocomplete";
                        $value['StructureField']['setting'] = $setting;
                    }
                    $validation = $validationData[$index];
                    if (is_array($validation)){
                        foreach ($validation as $k1 => $v1){
                            if ($v1['name'] == "data[FunctionManagement][between_from]"  && isset($v1['value']) && $v1['value'] != ""){
                                $vFrom = $v1['value'];
                                $vTo = "";
                                foreach ($validation as $k2 => $v2){
                                    if ($v2['name'] == "data[FunctionManagement][between_to]"){
                                        $vTo = $v2['value'];
                                        unset($validation[$k2]);
                                        unset($validation[$k1]);
                                        break;
                                    }
                                }
                                if (!empty($vTo)){
                                    $validationNormalizedData[] = array(
                                        "structure_field_id" => $index,
                                        "rule" => "between,$vFrom,$vTo",
                                        "language_message" => __("error-the %s length should be between %s and %s", $value['StructureField']['language_label'], $vFrom, $vTo)
                                    );
                                }
                            }
                            if ($v1['name'] == "data[FunctionManagement][range_from]" && isset($v1['value']) && $v1['value'] != ""){
                                $vFrom = $v1['value'];
                                $vTo = "";
                                foreach ($validation as $k2 => $v2){
                                    if ($v2['name'] == "data[FunctionManagement][range_to]"){
                                        $vTo = $v2['value'];
                                        unset($validation[$k2]);
                                        unset($validation[$k1]);
                                        break;
                                    }
                                }
                                $vf=0;
                                $vt=0;
                                if (in_array($value['StructureField']['type'], array("float1", "float2", "float5")) !== false) {
                                    $n = pow(10, -intval(substr($value['StructureField']['type'], 5, 1)));
                                    
                                    $vf = $vFrom - $n;
                                    $vt = $vTo + $n;
                                }else{
                                    $vf = $vFrom - 1;
                                    $vt = $vTo + 1;
                                }

                                if ($vf>$vt){
                                    $tmp = $vf;
                                    $vf = $vt;
                                    $vt = $tmp;
                                    $tmp = $vFrom;
                                    $vFrom = $vTo;
                                    $vTo = $tmp;
                                }
                                
                                if (!empty($vTo)){
                                    $validationNormalizedData[] = array(
                                        "structure_field_id" => $index,
                                        "rule" => "range,$vf,$vt",
                                        "language_message" => __("error-the %s should be between %s and %s", $value['StructureField']['language_label'], $vFrom, $vTo)
                                    );
                                }
                            }
                            if($v1['name'] == "data[FunctionManagement][is_unique]"  && isset($v1['value']) && $v1['value'] == 1){
                                $validationNormalizedData[] = array(
                                    "structure_field_id" => $index,
                                    "rule" => "isUnique",
                                    "language_message" => __("this field must be unique")
                                );
                                if ($flagBatchAction){
                                    $this->validationErrors['language_label'][] = 'fb_ the form cannot be used in batch action and have unique field at the same time';
                                }
                            }

                            if($v1['name'] == "data[FunctionManagement][not_blank]" && isset($v1['value']) && $v1['value'] ==1){
                                $validationNormalizedData[] = array(
                                    "structure_field_id" => $index,
                                    "rule" => "notBlank",
                                    "language_message" => __("this field is required")
                                );
                            }
                        }
                    }
                    $index++;
                }
            }
//            unset ($data["validationData"]);
            $data["StructureValidation"] = $validationNormalizedData;
        }
    }

    public function validatesFormBuilder($options = array(), $metaData = array()) 
    {
        return parent::validates($options);
    }

    public function validatesEditFormBuilder($metaData)
    {
        return parent::validates();
    }
}