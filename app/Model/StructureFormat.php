<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class StructureFormat
 */
class StructureFormat extends AppModel
{

    public $name = 'StructureFormat';

    public $belongsTo = array(
        'StructureField'
    );
    
    public function validatesFormBuilder($options = array(), &$errors = array()) 
    {
        if (empty($errors)){
            $errors = array("common" => array());
        }
        $valid = true;
        if (isset($options["prefix-common"])) {
            $dataCommon = isset($this->data[$options["prefix-common"]]) ? $this->data[$options["prefix-common"]] : array();
            $validationErrors = [];
            $index = 0;
            foreach ($dataCommon as $key => $value) {
                if (is_numeric($key)) {
                    $this->data = array();
                    $this->set($value);
                    $isValid = parent::validates($options);

                    $valid &= $isValid;

                    $errors["common"][$index] = (isset($errors["common"][$index]))?$errors["common"][$index]:array();
                    foreach ($this->validationErrors as $field => $errs) {
                        $errors["common"][$index][] = $field;
                    }
                    $this->validationErrors = array_merge($validationErrors, $this->validationErrors);
                    $validationErrors = $this->validationErrors;
                    $index++;
                }
            }
            unset($options["prefix-common"]);
        }
        return $valid;
    }

    public function validatesEditFormBuilder(&$errors = array()) 
    {
        $valid = true;
        foreach (array('masterTest', 'detailTest') as $k){
            $validationErrors = [];
            $index = 0;
            foreach ($this->data[$k] as $key => $value) {
                if (is_numeric($key)) {
                    $this->set($value);
                    $isValid = parent::validates();

                    $valid &= $isValid;

                    $errors[$k][$index] = (isset($errors[$k][$index]))?$errors[$k][$index]:array();
                    foreach ($this->validationErrors as $field => $errs) {
                        $errors[$k][$index][] = $field;
                    }
                    $this->validationErrors = array_merge($validationErrors, $this->validationErrors);
                    $validationErrors = $this->validationErrors;
                    $index++;
                }
            }
        }
        return $valid;
    }

    public function addWritableField($field = array(), $tablename = null) 
    {
        if (!is_array($field)){
            $field = array($field);
        }
        parent::addWritableField(array_merge($field, array("id", "structure_id", "structure_field_id", "display_column", "display_order", "language_heading", "flag_override_label", "language_label", "flag_override_tag", "language_tag", "flag_override_help", "language_help", "flag_override_type", "type", "flag_override_setting", "setting", "flag_override_default", "default", "flag_add", "flag_add_readonly", "flag_edit", "flag_edit_readonly", "flag_search", "flag_search_readonly", "flag_addgrid", "flag_addgrid_readonly", "flag_editgrid", "flag_editgrid_readonly", "flag_summary", "flag_batchedit", "flag_batchedit_readonly", "flag_index", "flag_detail", "flag_float", "margin")), $tablename);
    }

    public function setDataBeforeCopyFB(&$data)
    {
        $data["language_label"] = (!isset($data["language_label"])) ? "" : $data["language_label"];
        $data["language_tag"] = (!isset($data["language_tag"])) ? "" : $data["language_tag"];
        $data["language_help"] = (!isset($data["language_help"])) ? "" : $data["language_help"];
        $data["language_heading"] = (!isset($data["language_heading"])) ? "" : $data["language_heading"];
        
        $data["flag_add"] = (!isset($data["flag_add"])) ? "" : $data["flag_add"];
        $data["flag_addgrid"] = (!isset($data["flag_addgrid"])) ? "" : $data["flag_addgrid"];
        $data["flag_edit"] = (!isset($data["flag_edit"])) ? "" : $data["flag_edit"];
        $data["flag_editgrid"] = (!isset($data["flag_editgrid"])) ? "" : $data["flag_editgrid"];
    }
}