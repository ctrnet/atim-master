<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class StructureField
 */
class StructureField extends AppModel
{

    public $name = 'StructureField';

    public $hasMany = array(
        'StructureValidation'
    );

    /*
     * var $hasOne = array(
     * 'StructureValueDomain' => array(
     * 'className' => 'StructureValueDomain',
     * 'foreignKey' => false,
     * 'finderQuery' => '
     * SELECT
     * StructureValueDomain.*
     * FROM
     * structure_fields AS StructureField,
     * structure_value_domains AS StructureValueDomain
     * WHERE
     * StructureField.id={$__cakeID__$}
     * AND StructureField.structure_value_domain=StructureValueDomain.domain_name
     * '
     * )
     * );
     */
    public $belongsTo = array(
        'StructureValueDomain' => array(
            'className' => 'StructureValueDomain',
            'foreignKey' => 'structure_value_domain'
        )
    );

    // when building SUMMARIES, function used to look up, translate, and return translated VALUE

    /**
     *
     * @param null $plugin
     * @param null $model
     * @param array $fieldAndValue
     * @return array|mixed|null
     */
    public function findPermissibleValue($plugin = null, $model = null, $fieldAndValue = array())
    {
        $return = null;

        if (countCustom($fieldAndValue)) {

            $field = $fieldAndValue[1];
            $value = $fieldAndValue[0];

            if ($value) {

                $conditions = array();
                $conditions['StructureField.field'] = $field;
                if ($model)
                    $conditions['StructureField.model'] = $model;
                if ($plugin)
                    $conditions['StructureField.plugin'] = $plugin;

                $results = $this->find('first', array(
                    'conditions' => $conditions,
                    'limit' => 1,
                    'recursive' => 3
                ));

                $return = $results;

                if ($results && isset($results['StructureValueDomain'])) {
                    if (! empty($results['StructureValueDomain']['StructurePermissibleValue'])) {
                        foreach ($results['StructureValueDomain']['StructurePermissibleValue'] as $option) {
                            if ($option['value'] == $value)
                                $return = __($option['language_alias']);
                        }
                    } elseif (! empty($results['StructureValueDomain']['source'])) {
                        $pullDown = StructuresComponent::getPulldownFromSource($results['StructureValueDomain']['source']);
                        foreach ($pullDown as $option) {
                            if ($option['value'] == $value)
                                $return = $option['default'];
                        }
                    }
                }
            }

            if (! $return)
                $return = null;
        }

        return $return;
    }
    
    public function validatesFormBuilder($options = array(), &$errors = array()) 
    {
        if (empty($errors)){
            $errors = array("common" => array());
        }
        if (isset($options["prefix-common"])) {
            $dataCommon = isset($this->data[$options["prefix-common"]]) ? $this->data[$options["prefix-common"]] : array();
            $valid = true;
            $validationErrors = [];
            $index = 0;
            foreach ($dataCommon as $key => $value) {
                if (is_numeric($key)) {
                    $this->set($value);
                    $isValid = parent::validates($options);

                    $valid &= $isValid;

                    $errors["common"][$index] = (isset($errors["common"][$index]))?$errors["common"][$index]:array();
                    foreach ($this->validationErrors as $field => $errs) {
                        $errors["common"][$index][] = $field;
                    }
                    $this->validationErrors = array_merge($validationErrors, $this->validationErrors);
                    $validationErrors = $this->validationErrors;
                    $index++;
                }
            }
            unset($options["prefix-common"]);
        }
        return $valid;
    }
    
    public function setDataBeforeSaveFB(&$data, $options = array())
    {
        $prefix = getPrefix();
        $valueDomainData = json_decode($data["valueDomainData"], true);
        $fbSetting = json_decode($data["settings"], true);
        if (isset($options["prefix-common"])) {
            $dataCommon = isset($data[$options["prefix-common"]]) ? $data[$options["prefix-common"]] : array();
            $index = 0;
            $now = time();
            foreach ($dataCommon as $key => &$value) {
                if (is_numeric($key)) {

                    if (!empty($fbSetting[$index])){
                        if (!empty($fbSetting[$index]['can_paste'])){
                            if ($fbSetting[$index]['can_paste'] == 'no' && !preg_match('/class\s*=\s*pasteDisabled/i', $value['StructureField']['setting'])){
                                if (empty($value['StructureField']['setting'])){
                                    $value['StructureField']['setting'] = 'class=pasteDisabled';
                                }else{
                                    $value['StructureField']['setting'] .= ',class=pasteDisabled';
                                }
                            }elseif ($fbSetting[$index]['can_paste'] == 'yes'){
                                $value['StructureField']['setting'] = preg_replace('/class\s*=\s*pasteDisabled/i', '', $value['StructureField']['setting']);
                            }
                        }
                    }

                    $value['FunctionManagement']['is_structure_value_domain'] = "";
                    if ($value['StructureField']['type']=='select'){
                        if (isset($valueDomainData[$index]['value']) && !empty($valueDomainData[$index]['value'])){
                            $value['StructureField']['structure_value_domain'] = $valueDomainData[$index]['id'];
                            $value['StructureField']['structure_value_domain_value'] = $valueDomainData[$index]['value'];
                            $value['StructureField']['setting'] = $valueDomainData[$index]['multi-select'] ? "class=atim-multiple-choice": "";
                            $value['FunctionManagement']['is_structure_value_domain'] = "OK";
                        }
                    }else{
                        if (isset($valueDomainData[$index]['value']) && !empty($valueDomainData[$index]['value'])){
//                            AppController::addWarningMsg(__("just for the list data type can have the value list"));
                        }
                        $value['StructureField']['structure_value_domain'] = null;
                        $value['FunctionManagement']['is_structure_value_domain'] = "OK";
                    }
                    $name = $value['StructureField']['language_label'];
                    $value["StructureField"]["field"] = createFieldName($prefix, $now,$name , ($index + 1 ) * 1000 + rand(0, 99));
                    $index++;
                }
            }
            unset ($data["valueDomainData"]);
            $data[$options["prefix-common"]] = $dataCommon;
        }
    }

    public function validatesEditFormBuilder(&$errors = array()) 
    {
        $valid = true;
        foreach (array('masterTest', 'detailTest') as $k){
            $validationErrors = [];
            $index = 0;
            foreach ($this->data[$k] as $key => $value) {
                if (is_numeric($key)) {
                    $this->set($value);
                    $isValid = parent::validates();

                    $valid &= $isValid;
                    $errors[$k][$index] = (isset($errors[$k][$index]))?$errors[$k][$index]:array();
                    foreach ($this->validationErrors as $field => $errs) {
                        $errors[$k][$index][] = $field;
                    }
                    $this->validationErrors = array_merge($validationErrors, $this->validationErrors);
                    $validationErrors = $this->validationErrors;
                    $index++;
                }
            }
        }
        return $valid;
    }
    
    public function setDataBeforeEditFB(&$data)
    {
        $valueDomainData = json_decode($data["valueDomainData"], true);
        $fbSetting = json_decode($data["settings"], true);
        $prefix = getPrefix();
        
        $now = time();
        $index = 0;

        //TODO::Check if can remove these 3 unused variables
        $alias = isset($data[$data['others']["FormBuilder"]["model"]]["detail_form_alias"])?$data[$data['others']["FormBuilder"]["model"]]["detail_form_alias"]:$prefix.$data['others']["FormBuilder"]["default_alias"];
        $masterAlias = $prefix . $data['others']["FormBuilder"]["default_alias"];
        $formBuilderId = $prefix . $data['others']["FormBuilder"]["id"];

        foreach (array('masterTest', 'detailTest') as $k){
            foreach ($data[$k] as $key => &$value) {
                if (is_numeric($key)) {

                    if (!empty($fbSetting[$index])){
                        if (!empty($fbSetting[$index]['can_paste'])){
                            if ($fbSetting[$index]['can_paste'] == 'no' && !preg_match('/class\s*=\s*pasteDisabled/i', $value['StructureField']['setting'])){
                                if (empty($value['StructureField']['setting'])){
                                    $value['StructureField']['setting'] = 'class=pasteDisabled';
                                }else{
                                    $value['StructureField']['setting'] .= ',class=pasteDisabled';
                                }
                            }elseif ($fbSetting[$index]['can_paste'] == 'yes'){
                                $value['StructureField']['setting'] = preg_replace('/class\s*=\s*pasteDisabled/i', '', $value['StructureField']['setting']);
                            }
                        }
                    }

                    $value['FunctionManagement']['is_structure_value_domain'] = "";
                    if ($value['StructureField']['type']=='select'){
                        if (isset($valueDomainData[$index]['value']) && !empty($valueDomainData[$index]['value'])){
                            $value['StructureField']['structure_value_domain'] = $valueDomainData[$index]['id'];
                            $value['StructureField']['structure_value_domain_value'] = $valueDomainData[$index]['value'];
                            if (isset($valueDomainData[$index]['multi-select'])){
                                $value['StructureField']['setting'] = $valueDomainData[$index]['multi-select'] ? "class=atim-multiple-choice": "";
                            }
                            $value['FunctionManagement']['is_structure_value_domain'] = "OK";
                        }
                    }else{
                        if (isset($valueDomainData[$index]['value']) && !empty($valueDomainData[$index]['value'])){
//                            AppController::addWarningMsg(__("just for the list data type can have the value list"));
                        }
                        $value['StructureField']['structure_value_domain'] = null;
                        $value['FunctionManagement']['is_structure_value_domain'] = "OK";
                    }
                    if (isset($value['StructureField']['id']) && !empty($value['StructureField']['id'])){
                        $savedData = $this->findById($value['StructureField']['id']);
                        $value["StructureField"]["field"] = $savedData['StructureField']['field'];
                    }else{
                        $name = $value['StructureField']['language_label'];
                        $value["StructureField"]["field"] = createFieldName($prefix, $now, $name, ($index + 1 ) * 1000 + rand(0, 99));
                    }
                    $index++;
                }
            }
        }
    }

    public function addWritableField($field = array(), $tablename = null) 
    {
        if (!is_array($field)){
            $field = array($field);
        }
        parent::addWritableField(array_merge($field, array("id", "public_identifier", "setting", "plugin", "model", "tablename", "field", "language_label", "language_tag", "type", "setting", "default", "structure_value_domain", "language_help", "validation_control", "value_domain_control", "field_control", "flag_confidential", "sortable", "flag_form_builder", "flag_test_mode", "flag_copy_from_form_builder")), $tablename);
    }

    public function setDataBeforeCopyFB(&$field, $index)
    {
        $prefix = getPrefix();
        $now = time();
        if (!empty($field['language_label'])){
            $name = preg_replace('/\s+/', '_', $field['language_label']);
        }elseif(!empty($field['language_tag'])){
            $name = preg_replace('/\s+/', '_', $field['language_tag']);
        }else{
            $name = "No_Name_".rand(0, 9999);
        }
        if (!empty($field['flag_copy_from_form_builder'])){
            $field['field'] = createFieldName($prefix, $now, $name, rand(0, 9999));
        }
        $field['id'] = null;
        $field['flag_test_mode'] = 1;
        $field['flag_form_builder'] = 1;
        if (empty($field['language_help'])){
            $field['language_help'] = "";
        }
    }
    
}