<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class AtimDebug
 */
class AtimDebug
{
    private static $timeout = 1000;
    private static $counter = 0;

    private static $end = false;
    private static $saveInFile = true;
    private static $fileNameOutput = "logs.txt";
    private static $screen = true;
    private static $menu = true;
    private static $conditions = true;
    private static $from = 1;
    private static $count = 65536;
    private static $function = false;
    private static $file = false;
    private static $class = false;
    private static $force = false;
    private static $clear = false;
    private static $data = array();
    private static $options = array();
    private static $key = "";
    private static $time = "";
    private static $template = "";
    private static $hasFunction = "";
    private static $hasClass = "";
    private static $hasFile = "";
    private static $lineSeperator = "\n\t\t------------------------\n";


    /**
     *
     * @param int $counter after $counter time that this function run, it send $message to API
     * @param array $message the message that will be send to API after $counter time of execution of this function
     */
    public static function stop($counter = 1, $message = array('message'))
    {
        if (! is_array($message)) {
            $message = array(
                $message
            );
        }
        if (self::$counter == $counter) {
            d($message);
            die("stop");
        } else {
            self::$counter ++;
        }
    }


    /**
     * @return array
     */
    private static function getStackTrace()
    {
        $bt = debug_backtrace();
        $result = array();
        extract(self::$options);
        self::$hasFunction = empty($function)? true : false;
        self::$hasFile = empty($file)? true : false;
        self::$hasClass = empty($class)? true : false;

        foreach ($bt as $key => $unit) {
            unset($unit['args']);
            unset($unit['object']);
            $u['file'] = isset($unit['file'])? $unit['file'] : '?';
            $u['line'] = isset($unit['line'])? $unit['line'] : '?';
            $u['function'] = isset($unit['function'])? $unit['function'] : '?';
            $u['class'] = isset($unit['class'])? $unit['class'] : '?';

            $files = explode(DS, $u['file']);

            if (!self::$hasFile){
                foreach ($files as $f) {
                    if (strtolower($f) == strtolower($file)) {
                        self::$hasFile = true;
                        break;
                    }
                }
            }

            if (!self::$hasFunction ) {
                if (strtolower($function) == strtolower($u['function'])){
                    self::$hasFunction = true;
                }
            }

            if (!self::$hasClass ) {
                if (strtolower($class) == strtolower($u['class'])){
                    self::$hasClass = true;
                }
            }

            if (!self::$hasFunction || !self::$hasFile || !self::$hasClass){
                self::$hasFunction = empty($function)? true : false;
                self::$hasClass = empty($class)? true : false;
                self::$hasFile = empty($file)? true : false;
            }

            foreach ($files as $f) {
                if ($f == "DebugKit") {
                    self::$hasFile = false;
                    self::$hasFunction = false;
                    self::$hasClass = false;
                }
            }

            if ($u['class'] != __CLASS__){
                //$result[] = $u['file'] . $u['line'] . $u['function'] . $u['class']."\n";
            }else{
                $result[0] = $u['file'] . $u['line'] . $u['function'] . $u['class']."\n";
            }
        }
        return $result;
    }

    /**
     * @return string
     */
    private static function getHashKey()
    {
        return md5(json_encode(self::getStackTrace()));
    }

    /**
     * @param null $key
     */
    private static function cleanup($key = null)
    {
        extract(self::$options);
        $key = ($clear) ? 'all' : $key;

        if (isset($_SESSION['ATiMDebug']) && is_array($_SESSION['ATiMDebug'])){
            if (strtolower($key) == 'all'){
                $_SESSION['ATiMDebug'] = array(
                    'time' => self::$time
                );
            }else{
                foreach ($_SESSION['ATiMDebug'] as $key => &$debug){
                    if (is_numeric($key)){
                        if ((self::$time - $key > self::$timeout * 10) || (self::$time != $key && empty($debug))){
                            unset($_SESSION['ATiMDebug'][$key]);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $options
     */
    private static function initialize($options)
    {
//        self::$options = array_change_key_case_recursive($options);

        self::$options = $options + array(
            'end' => self::$end,
            'saveInFile' => self::$saveInFile,
            'fileNameOutput' => self::$fileNameOutput,
            'screen' => self::$screen,
            'menu' => self::$menu,
            'conditions' => self::$conditions,
            'from' => self::$from,
            'count' => self::$count,
            'function' => self::$function,
            'file' => self::$file,
            'class' => self::$class,
            'force' =>self::$force,
            'clear' => self::$clear
        );

        self::$key = self::getHashKey();

        self::$time = isset($_SESSION['ATiMDebug']['time']) ? $_SESSION['ATiMDebug']['time'] : round(microtime(true) * 10);
        if (!isset($_SESSION['ATiMDebug'][self::$time])) {
            $_SESSION['ATiMDebug'][self::$time] = array(
            );
        }

        App::uses('Debugger', 'Utility');

        if (!isset(self::$data[self::$key])){
            self::$data[self::$key] = array(
                'time' => date('j-m-Y H:i:s'),
                'counter' => 0,
                'runCounter' => 0,
                'message' => "",
            );
        }

    }

    /**
     * @return bool
     */
    private static function CanContinue()
    {
        $canContinue = true;

        extract(self::$options);
        if (!self::$hasFunction || !self::$hasFile || !self::$hasClass){
            $canContinue = false;
        }

        if ((! Configure::read('debug'))) {
            if ($force){
                if (!isset($_SESSION['Auth']['User']['group_id']) || $_SESSION['Auth']['User']['group_id'] != 1){
                    self::$options['screen'] = false;
                    self::$options['menu'] = false;
                }
            }else{
                self::$options['screen'] = false;
                self::$options['menu'] = false;
            }
        }

        if(!$conditions){
            $canContinue = false;
        }


        if ($canContinue && self::$data[self::$key]['counter'] < $from - 1){
            self::$data[self::$key]['counter'] ++;
            $canContinue = false;
        }

        if (self::$data[self::$key]['runCounter'] > $count - 1){
            $canContinue = false;
        }

        return $canContinue;
    }

    /**
     * ### options
     * - end(false): End the execution
     * - saveInFile(true): Determine if save in a file or not
     * - fileNameOutput(logs.txt): The output file name
     * - screen(true): Show the result on the screen
     * - menu(true): Show the result on debug-kit D menu
     * - conditions(true): Show if the condition is true
     * - from(1): Show the message from which counter
     * - count(65536): The maximum of time to show the message
     * - function: Show the output if the function exists in the call sequence
     * - class: Show the output if the class exists in the call sequence
     * - file: Show the output if the file exists in the call sequence
     * - force: Force to show in file even in the debug mode and the usr belong admin group
     * - clear: Erase the Session
     * @param string $message The message will be as an output to file, screen or debugKit d menu
     * @param array $options The different debug options
     * @return null
     */
    public static function d($message = "", $options = array())
    {
        self::initialize($options);

        self::cleanup();
        if (!self::CanContinue()){
            return;
        }
        extract(self::$options);

        App::uses('Debugger', 'Utility');

        $trace = Debugger::trace(array(
            'start' => 1,
            'depth' => 2,
            'format' => 'array'
        ));
        $fileName = str_replace(array(
            CAKE_CORE_INCLUDE_PATH,
            ROOT
        ), '', $trace[0]['file']);

        $line = $trace[0]['line'];
        $html = <<<HTML
<div class="cake-debug-output">
<div class="minus-button"><a href="javascript:void(0)" class="debug-button">-</a></div>
%s
<pre class="cake-debug">
%s
</pre>
</div>
HTML;
        $text = <<<TEXT
%s
########## DEBUG ##########
%s
###########################

TEXT;
        $template = $html;
        self::$data[self::$key]["runCounter"]++;
        $time = date('j-m-Y H:i:s', round(self::$time / 10));
        $counter = self::$data[self::$key]["runCounter"];
        if (PHP_SAPI === 'cli') {
            $template = $text;
            $lineInfo = sprintf('%s- %s%s (line %s)', $counter, $fileName, $line, $time);
        }
        $var = print_r($message, true);
        $var = h($var);
        self::$data[self::$key]["message"] = $var;
        $lineInfo = sprintf('<span><strong>%s- %s</strong> (line <strong>%s</strong>)</span><span style="color: green"> %s</span>', $counter, $fileName, $line, $time);
        if ($screen) {
            printf($template, $lineInfo, $var);
        }

        $_SESSION['ATiMDebug']['template'] = $template;
        if ($menu) {
            $_SESSION['ATiMDebug'][self::$time][] = array(
                'message' => $var,
                'lineInfo' => $lineInfo,
            );
        }

        if ($saveInFile){
            $var = print_r($message, true);
            $print = self::$lineSeperator . self::$data[self::$key]["runCounter"]. "- " . $fileName . "(line $line) ". $time . "\n" .$var;
            $fileNameOutput = LOGS . $fileNameOutput;
            file_put_contents($fileNameOutput, $print, FILE_APPEND);
        }

        if ($end) {
            die();
        }

    }
}

/**
 * @param $arr
 * @return array
 */
function array_change_key_case_recursive($arr)
{
    return array_map(function($item){
        if(is_array($item))
            $item = array_change_key_case_recursive($item);
        return $item;
    },array_change_key_case($arr));
}

/**
 *
 * @param $message1
 * @param string $message2
 */
function debug_die($message1, $message2 = "")
{
    debug($message1);
    if (is_array($message2)) {
        $message2 = json_encode($message2);
    }
    die($message2);
}

// override_function('__', '$singular, $args', 'return newTranslate($singular, $args);');
/**
 *
 * @param $singular
 * @param null $args
 * @return mixed|string
 */
function newTranslate($singular, $args = null)
{
    if (is_numeric($singular)) {
        return '123456789';
    }
    return __($singular, $args);
}

/**
 *
 * @param array|int $arr
 * @return bool
 */
function isAssoc(array $arr)
{
    if (array() === $arr) {
        return false;
    }
    return array_keys($arr) !== range(0, countCustom($arr) - 1);
}

/**
 *
 * @param $data
 * @param int $level
 * @param int $formattage
 * @return string
 */
function convertJSONtoArray($data, $level = 3, $formattage = 0)
{
    $s = "";
    if ($formattage == 0) {
        $newLine = "";
        $tab = "";
    } else {
        $newLine = "\n";
        $tab = "\t";
    }
    if (isAssoc($data)) {
        foreach ($data as $key1 => $value1) {
            if (is_array($value1)) {
                $s .= str_repeat($tab, $level) . "'" . $key1 . "' => [" . $newLine;
                $s .= convertJSONtoArray($value1, $level + 1);
                $s .= str_repeat($tab, $level) . "]," . $newLine;
            } else {
                if (($value1 == 'true') || ($value1 == 'false') || (is_numeric($value1))) {
                    $s .= str_repeat($tab, $level) . "'" . $key1 . "' => '" . $value1 . "'," . $newLine;
                } else {
                    $s .= str_repeat($tab, $level) . "'" . $key1 . "' => '" . $value1 . "'," . $newLine;
                }
            }
        }
        $s = rtrim($s, ",");
    } else {
        foreach ($data as $key1 => $value1) {
            if (is_array($value1)) {
                $s .= str_repeat($tab, $level) . "[" . $newLine;
                $s .= convertJSONtoArray($value1, $level + 1);
                $s .= str_repeat($tab, $level) . "]," . $newLine;
            } else {
                if (($value1 == 'true') || ($value1 == 'false') || (is_numeric($value1))) {
                    $s .= str_repeat($tab, $level) . "'" . $value1 . "'," . $newLine;
                } else {
                    $s .= str_repeat($tab, $level) . "'" . $value1 . "'," . $newLine;
                }
            }
        }
        $s = rtrim($s, ",");
    }
    return $s;
}

/**
 *
 * @param $data
 * @param int $level
 * @param int $formattage
 * @return string
 */
function json_encode_js($data, $level = 3, $formattage = 0)
{
    $s = "";
    if ($formattage == 0) {
        $newLine = "";
        $tab = "";
    } else {
        $newLine = "\n";
        $tab = "\t";
    }
    if (isAssoc($data)) {
        foreach ($data as $key1 => $value1) {
            if (is_array($value1)) {
                $s .= str_repeat($tab, $level) . "'" . $key1 . "': [" . $newLine;
                $s .= json_encode_js($value1, $level + 1);
                $s .= str_repeat($tab, $level) . "]," . $newLine;
            } else {
                if (($value1 == 'true') || ($value1 == 'false') || (is_numeric($value1))) {
                    $s .= str_repeat($tab, $level) . "'" . $key1 . "' : '" . $value1 . "'," . $newLine;
                } else {
                    $s .= str_repeat($tab, $level) . "'" . $key1 . "' : '" . $value1 . "'," . $newLine;
                }
            }
        }
        $s = rtrim($s, ",");
    } else {
        foreach ($data as $value1) {
            if (is_array($value1)) {
                $s .= str_repeat($tab, $level) . "[" . $newLine;
                $s .= json_encode_js($value1, $level + 1);
                $s .= str_repeat($tab, $level) . "]," . $newLine;
            } else {

                if (($value1 == 'true') || ($value1 == 'false') || (is_numeric($value1))) {
                    $s .= str_repeat($tab, $level) . "'" . $value1 . "'," . $newLine;
                } else {
                    $s .= str_repeat($tab, $level) . "'" . $value1 . "'," . $newLine;
                }
            }
        }
        $s = rtrim($s, ",");
    }
    return $s;
}

/**
 *
 * @param $str
 * @return mixed
 */
function stringCorrection($str)
{
    if (empty($str)) {
        return $str;
    } elseif (is_string($str)) {
        return str_replace("~~~~", "\\", str_replace("\\\\", "~~~~", $str));
    } elseif (is_array($str)) {
        foreach ($str as &$value) {
            $value = stringCorrection($value);
        }
        return $str;
    } else {
        return $str;
    }
}

/**
 *
 * @param mixed $var The variable that will be printed.
 * @param array $options The options of the function
 * @return void
 */
function d($var, $options = array())
{
    $options += array(
        "screen" => true,
        "log" => true,
        "die" => false
    );
    extract($options);

    if (! Configure::read('debug')) {
        return;
    }
    App::uses('Debugger', 'Utility');

    $trace = Debugger::trace(array(
        'start' => 1,
        'depth' => 2,
        'format' => 'array'
    ));
    $file = str_replace(array(
        CAKE_CORE_INCLUDE_PATH,
        ROOT
    ), '', $trace[0]['file']);
    $line = $trace[0]['line'];
    $html = <<<HTML
<div class="cake-debug-output">
<div class="minus-button"><a href="javascript:void(0)" class="debug-button">-</a></div>
%s
<pre class="cake-debug">
%s
</pre>
</div>
HTML;
    $text = <<<TEXT
%s
########## DEBUG ##########
%s
###########################

TEXT;
    $template = $html;
    if (PHP_SAPI === 'cli') {
        $template = $text;
        $lineInfo = sprintf('%s%s (line %s)', $file, $line, date('j-m-Y H:i:s'));
    }
    $var = print_r($var, true);
    $var = h($var);
    $lineInfo = sprintf('<span><strong>%s</strong> (line <strong>%s</strong>)</span><span style="color: green"> %s</span>', $file, $line, date('j-m-Y H:i:s'));
    if ($screen) {
        printf($template, $lineInfo, $var);
    }
    if ($log) {
        $l = empty($_SESSION['debug']['dl']) ? 0 : countCustom($_SESSION['debug']['dl']);
        $_SESSION['debug']['dl'][$l][0] = $template;
        $_SESSION['debug']['dl'][$l][1] = $lineInfo;
        $_SESSION['debug']['dl'][$l][2] = $var;
    }
    if ($die) {
        die();
    }
}

/**
 *
 * @param int $number
 */
function dc($number = 0)
{
    if (! Configure::read('debug')) {
        return;
    }
    if ($number == 0) {
        unset($_SESSION['debug']);
    } else {
        array_splice($_SESSION['debug']['dl'], 0, $number);
    }
}

/**
 *
 * @param array $phpArray
 * @param $jsArray
 */
function convertArrayToJavaScript($phpArray, $jsArray)
{
    if (is_string($jsArray) && is_array($phpArray) && ! empty($phpArray) && ! is_array_empty($phpArray)) {
        $_SESSION['js_post_data'] = "\r\n" . 'var ' . $jsArray . "=" . json_encode($phpArray) . "\r\n";
    }
}

/**
 *
 * @param array $inputVariable
 * @return bool
 */
function is_array_empty($inputVariable)
{
    $result = true;
    if (is_array($inputVariable) && countCustom($inputVariable) > 0) {
        foreach ($inputVariable as $value) {
            $result = $result && is_array_empty($value);
            if (! $result) {
                return false;
            }
        }
    } else {
        $result = empty($inputVariable) && $inputVariable != 0 && $inputVariable != '0';
    }

    return $result;
}

/**
 *
 * @param $data
 * @return array
 */
function removeEmptySubArray($data)
{
    if (is_array($data)) {
        $data = is_integer(key($data)) ? array_values(array_filter($data, 'removeEmptyStringArray')) : array_filter($data, 'removeEmptyStringArray');

        foreach ($data as &$v) {
            $v = removeEmptySubArray($v);
        }
        $data = is_integer(key($data)) ? array_values(array_filter($data, 'removeEmptyStringArray')) : array_filter($data, 'removeEmptyStringArray');
    }
    return $data;
}

/**
 *
 * @param $value
 * @return bool
 */
function removeEmptyStringArray($value)
{
    return ($value != "" && $value != array());
}

/**
 * @param bool $error
 * @return float
 */
function getTotalMemoryCapacity(&$error = false)
{
    $os = substr(PHP_OS, 0, 3);
    $defaultValue = 4294967296;
    if ($os == "WIN") {
        $totalMemory = array();
        exec('wmic memorychip get capacity', $totalMemory);
        if (isset($totalMemory[1])) {
            if (is_numeric($totalMemory[1])) {
                return round($totalMemory[1] / 1024 / 1024);
            } else {
                $totalMemory[1] = preg_replace("/[^0-9.]/", "", $totalMemory[1]);
                if (is_numeric($totalMemory[1])) {
                    return round($totalMemory[1] / 1024 / 1024);
                } else {
                    $error = true;
                    return round($defaultValue / 1024 / 1024);
                }
            }
        } elseif (isset($totalMemory[0])) {
            $totalMemory[0] = preg_replace("/[^0-9.]/", "", $totalMemory[0]);
            if (is_numeric($totalMemory[0])) {
                return round($totalMemory[0] / 1024 / 1024);
            } else {
                $error = true;
                return round($defaultValue / 1024 / 1024);
            }
        }
    } elseif ($os == "Lin") {
        $fh = fopen('/proc/meminfo', 'r');
        $mem = 0;
        while ($line = fgets($fh)) {
            $pieces = array();
            if (preg_match('/^MemTotal:\s+(\d+)\skB$/', $line, $pieces)) {
                $mem = $pieces[1];
                break;
            }
        }
        fclose($fh);
        return round($mem / 1024);
    }
}

/**
 * @param $data
 * @return float|int
 */
function convertFromKMG($data)
{
    if (strtoupper(substr($data, - 1)) == 'K') {
        $number = floatval(substr($data, 0, - 1)) * 1024;
    } elseif (strtoupper(substr($data, - 1)) == 'M') {
        $number = floatval(substr($data, 0, - 1)) * 1024 * 1024;
    } elseif (strtoupper(substr($data, - 1)) == 'G') {
        $number = floatval(substr($data, 0, - 1)) * 1024 * 1024 * 1024;
    } elseif (is_numeric($data)) {
        $number = floatval($data);
    }
    return $number;
}

/**
 * @param $word
 * @return null|string|string[]
 */
function ___($word)
{
    $string = __($word);
    $pattern = '/(.*)(\<span class\=[\"|\']{1}untranslated[\"|\']{1}\>)(.+)(\<\/span\>)(.*)/i';
    $replacement = '$3';
    $response = preg_replace($pattern, $replacement, $string);

    return $response;
}

function isEmpty($inputVariable)
{
    $result = true;

    if (is_array($inputVariable) && countCustom($inputVariable) > 0){
        foreach ($inputVariable as $value){
            $result = $result && isEmpty($value);
        }
    }else{
        $result = empty($inputVariable);
    }

    return $result;
}

function flatten(array $array)
{
    $return = array();
    array_walk_recursive($array, function ($a) use (&$return) {
        $return[] = $a;
    });
    return $return;
}

function getPrefix()
{
    return (empty(Configure::read('prefix')) ? "_" : substr(Configure::read('prefix'), 0, 20));
}

function encodeStamp($n)
{
    $encoded = "";
    if (is_int($n)) {
        $a = [];
        for ($i = ord("0"); $i <= ord("9"); $i++) {
            $a[] = chr($i);
        }
        for ($i = ord("a"); $i <= ord("z"); $i++) {
            $a[] = chr($i);
        }
        while ($n != 0) {
            $encoded = $a[$n % 36] . $encoded;
            $n = floor($n / 36);
        }
    }
    return $encoded;

}

function decodeStamp($s)
{
    $decoded = 0;
    if (is_string($s)) {
        $a = [];
        for ($i = 0; $i <= 9; $i++) {
            $a[(string)$i] = $i;
        }
        for ($i = 10; $i <= 35; $i++) {
            $a[chr($i + 87)] = $i;
        }

        for ($i = 0; $i < strlen($s); $i++) {
            $decoded = $decoded * 36 + $a[$s[$i]];
        }
    }
    return $decoded;

}

function createTableName($prefix, $timeStamp, &$name)
{
    return createFieldName($prefix, $timeStamp, $name, 0);
}

function createFieldName($prefix, $timeStamp, &$name, $randomNumber = 0)
{
    $timeStamp = encodeStamp($timeStamp);
    $randomNumber = $randomNumber ? encodeStamp($randomNumber) : 0;

    $name = preg_replace('/\s+/', '_', $name);
    $name = preg_replace('/[^0-9a-zA-Z\_]/', '', $name);
    while (strpos($name, "__")!==false){
        $name = str_replace("__", "_", $name);
    }
    $name = preg_replace('/_$/', '', $name);
    $name = preg_replace('/^_/', '', $name);

//    $name = Inflector::variable($name);
    $name = strtolower($name);
    $name = substr($name, 0, 20);

    $fieldName = $randomNumber ? sprintf("%s_%s_%s_%s", $prefix, $timeStamp, $randomNumber, $name) : sprintf("%s_%s_%s", $prefix, $timeStamp, $name);

    while (strpos($fieldName, "__")!==false){
        $fieldName = str_replace("__", "_", $fieldName);
    }
     return $fieldName;

}

function countCustom($data)
{
    $count = 0;
    if (is_array($data)){
        $count = count($data);
    }elseif (is_null($data)){
        $count = 0;
    }else{
        $count = 1;
        if (Configure::read('debug')){
            AppController::addWarningMsg(__('the count parameter is not an array [%s]', gettype($data)), true);
//            AppController::getInstance()->log([__('the count parameter is not an array [%s]', gettype($data)), $data, AppController::getStackTrace()], 'notice');
        }
    }
    return $count;
}

function serializeArray(Array $data = [], Array $array = [])
{
    $response = [];
    foreach ($data as $key => $value){
        if (!is_array($value)){
            $response[] = [
                'name' => $array[0] . "[" . implode("][", array_slice($array, 1)) . "][$key]",
                'value' => $value
            ];
        }elseif(empty($value)){
            $response[] = [
                'name' => $array[0] . "[" . implode("][", array_slice($array, 1)) . "][$key]",
                'value' => ""
            ];
        }else{
            $response = array_merge($response, serializeArray($value, array_merge($array, [$key])));
        }
    }
    return $response;
}

function normalizeCompressedDataForSend($data = [], &$search = ["'", "[", "]",], &$replace =["||singleQuote||", "||openBrace||", "||closeBrace||", ])
{
    return arrayReplace($data, $search, $replace);
}

function normalizeCompressedDataForReceive(&$data, &$search = ["||singleQuote||", "||openBrace||", "||closeBrace||", ], &$replace = ["'", "[", "]", ])
{
    return arrayReplace($data, $search, $replace);
}

function arrayReplace(&$data = [], &$search = [], &$replace =[])
{
    if(is_string($search)){
        $search = [$search];
    }

    if(is_string($replace)){
        $replace = [$replace];
    }

    $tmpData = [];
    if (is_array($data)){
        foreach ($data as $key => $val){
            if (is_string($key)){
                $key = str_replace($search, $replace, $key);
            }

            if(!is_array($val)){
                if (is_string($val)){
                    $val = str_replace($search, $replace, $val);
                }
                $tmpData[$key] = $val;
            }else{
                $tmpData[$key] = arrayReplace($val, $search, $replace);
            }
        }
        return $tmpData;
    }else{
        return $data;
    }


}

function convertIndexedArrayToAssociative(Array $array)
{
    $response = [];
    foreach ($array as $v){
        $response[$v] = true;
    }
    return $response;
}

function removeAccents($string, $options = ['fr' => true])
{
    $chars = [
        // Decompositions for Latin-1 Supplement
        chr(195) . chr(128) => 'A', chr(195) . chr(129) => 'A',
        chr(195) . chr(130) => 'A', chr(195) . chr(131) => 'A',
        chr(195) . chr(132) => 'A', chr(195) . chr(133) => 'A',
        chr(195) . chr(135) => 'C', chr(195) . chr(136) => 'E',
        chr(195) . chr(137) => 'E', chr(195) . chr(138) => 'E',
        chr(195) . chr(139) => 'E', chr(195) . chr(140) => 'I',
        chr(195) . chr(141) => 'I', chr(195) . chr(142) => 'I',
        chr(195) . chr(143) => 'I', chr(195) . chr(145) => 'N',
        chr(195) . chr(146) => 'O', chr(195) . chr(147) => 'O',
        chr(195) . chr(148) => 'O', chr(195) . chr(149) => 'O',
        chr(195) . chr(150) => 'O', chr(195) . chr(153) => 'U',
        chr(195) . chr(154) => 'U', chr(195) . chr(155) => 'U',
        chr(195) . chr(156) => 'U', chr(195) . chr(157) => 'Y',
        chr(195) . chr(159) => 's', chr(195) . chr(160) => 'a',
        chr(195) . chr(161) => 'a', chr(195) . chr(162) => 'a',
        chr(195) . chr(163) => 'a', chr(195) . chr(164) => 'a',
        chr(195) . chr(165) => 'a', chr(195) . chr(167) => 'c',
        chr(195) . chr(168) => 'e', chr(195) . chr(169) => 'e',
        chr(195) . chr(170) => 'e', chr(195) . chr(171) => 'e',
        chr(195) . chr(172) => 'i', chr(195) . chr(173) => 'i',
        chr(195) . chr(174) => 'i', chr(195) . chr(175) => 'i',
        chr(195) . chr(177) => 'n', chr(195) . chr(178) => 'o',
        chr(195) . chr(179) => 'o', chr(195) . chr(180) => 'o',
        chr(195) . chr(181) => 'o', chr(195) . chr(182) => 'o',
        chr(195) . chr(182) => 'o', chr(195) . chr(185) => 'u',
        chr(195) . chr(186) => 'u', chr(195) . chr(187) => 'u',
        chr(195) . chr(188) => 'u', chr(195) . chr(189) => 'y',
        chr(195) . chr(191) => 'y',
        // Decompositions for Latin Extended-A
        chr(196) . chr(128) => 'A', chr(196) . chr(129) => 'a',
        chr(196) . chr(130) => 'A', chr(196) . chr(131) => 'a',
        chr(196) . chr(132) => 'A', chr(196) . chr(133) => 'a',
        chr(196) . chr(134) => 'C', chr(196) . chr(135) => 'c',
        chr(196) . chr(136) => 'C', chr(196) . chr(137) => 'c',
        chr(196) . chr(138) => 'C', chr(196) . chr(139) => 'c',
        chr(196) . chr(140) => 'C', chr(196) . chr(141) => 'c',
        chr(196) . chr(142) => 'D', chr(196) . chr(143) => 'd',
        chr(196) . chr(144) => 'D', chr(196) . chr(145) => 'd',
        chr(196) . chr(146) => 'E', chr(196) . chr(147) => 'e',
        chr(196) . chr(148) => 'E', chr(196) . chr(149) => 'e',
        chr(196) . chr(150) => 'E', chr(196) . chr(151) => 'e',
        chr(196) . chr(152) => 'E', chr(196) . chr(153) => 'e',
        chr(196) . chr(154) => 'E', chr(196) . chr(155) => 'e',
        chr(196) . chr(156) => 'G', chr(196) . chr(157) => 'g',
        chr(196) . chr(158) => 'G', chr(196) . chr(159) => 'g',
        chr(196) . chr(160) => 'G', chr(196) . chr(161) => 'g',
        chr(196) . chr(162) => 'G', chr(196) . chr(163) => 'g',
        chr(196) . chr(164) => 'H', chr(196) . chr(165) => 'h',
        chr(196) . chr(166) => 'H', chr(196) . chr(167) => 'h',
        chr(196) . chr(168) => 'I', chr(196) . chr(169) => 'i',
        chr(196) . chr(170) => 'I', chr(196) . chr(171) => 'i',
        chr(196) . chr(172) => 'I', chr(196) . chr(173) => 'i',
        chr(196) . chr(174) => 'I', chr(196) . chr(175) => 'i',
        chr(196) . chr(176) => 'I', chr(196) . chr(177) => 'i',
        chr(196) . chr(178) => 'IJ', chr(196) . chr(179) => 'ij',
        chr(196) . chr(180) => 'J', chr(196) . chr(181) => 'j',
        chr(196) . chr(182) => 'K', chr(196) . chr(183) => 'k',
        chr(196) . chr(184) => 'k', chr(196) . chr(185) => 'L',
        chr(196) . chr(186) => 'l', chr(196) . chr(187) => 'L',
        chr(196) . chr(188) => 'l', chr(196) . chr(189) => 'L',
        chr(196) . chr(190) => 'l', chr(196) . chr(191) => 'L',
        chr(197) . chr(128) => 'l', chr(197) . chr(129) => 'L',
        chr(197) . chr(130) => 'l', chr(197) . chr(131) => 'N',
        chr(197) . chr(132) => 'n', chr(197) . chr(133) => 'N',
        chr(197) . chr(134) => 'n', chr(197) . chr(135) => 'N',
        chr(197) . chr(136) => 'n', chr(197) . chr(137) => 'N',
        chr(197) . chr(138) => 'n', chr(197) . chr(139) => 'N',
        chr(197) . chr(140) => 'O', chr(197) . chr(141) => 'o',
        chr(197) . chr(142) => 'O', chr(197) . chr(143) => 'o',
        chr(197) . chr(144) => 'O', chr(197) . chr(145) => 'o',
        chr(197) . chr(146) => 'OE', chr(197) . chr(147) => 'oe',
        chr(197) . chr(148) => 'R', chr(197) . chr(149) => 'r',
        chr(197) . chr(150) => 'R', chr(197) . chr(151) => 'r',
        chr(197) . chr(152) => 'R', chr(197) . chr(153) => 'r',
        chr(197) . chr(154) => 'S', chr(197) . chr(155) => 's',
        chr(197) . chr(156) => 'S', chr(197) . chr(157) => 's',
        chr(197) . chr(158) => 'S', chr(197) . chr(159) => 's',
        chr(197) . chr(160) => 'S', chr(197) . chr(161) => 's',
        chr(197) . chr(162) => 'T', chr(197) . chr(163) => 't',
        chr(197) . chr(164) => 'T', chr(197) . chr(165) => 't',
        chr(197) . chr(166) => 'T', chr(197) . chr(167) => 't',
        chr(197) . chr(168) => 'U', chr(197) . chr(169) => 'u',
        chr(197) . chr(170) => 'U', chr(197) . chr(171) => 'u',
        chr(197) . chr(172) => 'U', chr(197) . chr(173) => 'u',
        chr(197) . chr(174) => 'U', chr(197) . chr(175) => 'u',
        chr(197) . chr(176) => 'U', chr(197) . chr(177) => 'u',
        chr(197) . chr(178) => 'U', chr(197) . chr(179) => 'u',
        chr(197) . chr(180) => 'W', chr(197) . chr(181) => 'w',
        chr(197) . chr(182) => 'Y', chr(197) . chr(183) => 'y',
        chr(197) . chr(184) => 'Y', chr(197) . chr(185) => 'Z',
        chr(197) . chr(186) => 'z', chr(197) . chr(187) => 'Z',
        chr(197) . chr(188) => 'z', chr(197) . chr(189) => 'Z',
        chr(197) . chr(190) => 'z', chr(197) . chr(191) => 's',
    ];

    $fr = [
        chr(195) . chr(169) => 'é', //é
        chr(195) . chr(137) => 'É', //É
        chr(195) . chr(160) => 'à', //à
        chr(195) . chr(128) => 'À', //À
        chr(195) . chr(168) => 'è', //è
        chr(195) . chr(136) => 'È', //È
        chr(195) . chr(170) => 'ê', //ê
        chr(195) . chr(138) => 'Ê', //Ê
        chr(195) . chr(167) => 'ç', //ç
        chr(195) . chr(135) => 'Ç', //Ç
        chr(195) . chr(185) => 'ù', //ù
        chr(195) . chr(153) => 'Ù', //Ù
        chr(197) . chr(147) => 'œ', //œ
        chr(195) . chr(175) => 'ï', //ï
        chr(195) . chr(180) => 'ô', //ô
        chr(195) . chr(148) => 'Ô', //Ô
        chr(195) . chr(130) => 'Â', //Â
        chr(195) . chr(162) => 'â', //â
    ];

    if ($options['fr']){
        $chars = array_diff_key($chars, $fr);
    }

    $string = strtr($string, $chars);
    return $string;
}

function CorrectDosUnicodeAccent($string)
{
    $chars = [
        chr(0x85) => 'à',
        chr(0x3F) => 'À',
        chr(0x8A) => 'è',
        chr(0x3F) => 'È',
        chr(0x82) => 'é',
        chr(0x90) => 'É',
        chr(0x88) => 'ê',
        chr(0x3F) => 'Ê',
        chr(0x87) => 'ç',
        chr(0x80) => 'Ç',
        chr(0x93) => 'ô',
        chr(0x3F) => 'Ô',
        chr(0x97) => 'ù',
        chr(0x3F) => 'Ù',
        chr(0x3F) => 'œ',
        chr(0x3F) => 'Â',
        chr(0x83) => 'â',
        chr(0x8B) => 'ï',
    ];
    $string = strtr($string, $chars);
    return $string;
}

function CorrectMacUnicodeAccent($string)
{
    $chars = [
        chr(0x88) => 'à',
        chr(0xCB) => 'À',
        chr(0x8F) => 'è',
        chr(0xE9) => 'È',
        chr(0x8E) => 'é',
        chr(0x83) => 'É',
        chr(0x90) => 'ê',
        chr(0xE6) => 'Ê',
        chr(0x8D) => 'ç',
        chr(0x82) => 'Ç',
        chr(0x99) => 'ô',
        chr(0xEF) => 'Ô',
        chr(0x9D) => 'ù',
        chr(0xF4) => 'Ù',
        chr(0xCF) => 'œ',
        chr(0xE5) => 'Â',
        chr(0x89) => 'â',
        chr(0x95) => 'ï',
    ];
    $string = strtr($string, $chars);
    return $string;
}

function removeSpecialChars($string)
{
    $chars = [
        chr(0x60) => "'",
        chr(0x82) => ",",
        chr(0x84) => '"',
        chr(0x85) => "...",
        chr(0x88) => '^',
        chr(0x89) => '%',
        chr(0x8A) => 'è',
        chr(0x8B) => '<',
        chr(0x91) => "'",
        chr(0x92) => "'",
        chr(0x93) => '"',
        chr(0x94) => '"',
        chr(0x95) => '*',
        chr(0x96) => '-',
        chr(0x97) => '--',
        chr(0x98) => '~',
        chr(0x9B) => '>',
        chr(0xA6) => '|',
        chr(0xAB) => "'",
        chr(0xB4) => "'",
        chr(0xB7) => '.',
        chr(0xBB) => '"',
    ];
    $string = strtr($string, $chars);
    return $string;
}

class OpenSSL
{
    private static $salt = "password";
    private static $algorithm = "AES-128-ECB";

    public static function encrypt($text = "", $salt = "")
    {
        return openssl_encrypt($text, self::$algorithm, self::$salt . $salt);
    }

    public static function decrypt($text = "", $salt = "")
    {
        return openssl_decrypt($text, self::$algorithm, self::$salt . $salt);
    }
}