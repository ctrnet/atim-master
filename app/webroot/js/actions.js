function actionsInit()
{
    checkErrors();
    setCss();
}

function checkErrors()
{
    if (typeof errorsGrid != "undefined" && errorsGrid != "") {
        $form = $("form");
        $form.find("input, select, textarea").removeClass("error");

        for (var name in errorsGrid) {
            if ($("[name^='" + name + "']").length > 0){
                $("[name^='" + name + "']").attr("data-error-id", name);
                $("[name^='" + name + "']").addClass('error');
                if ($("[name^='" + name + "']").hasClass("atim-multiple-choice")){
                    $("select[name^='" + name + "']").next("div.chosen-container").addClass("error");
                }
            }else {

            }
        }
    }
}

function scrollToError(link)
{
    var target = $("[data-error-id^='" + link + "']");

    if (target.length != 0){
        if (target.hasClass("atim-multiple-choice")){
            target = target.next("div.atim-multiple-choice");
            target.addClass("error");
        }

        $("html, body").animate({
            scrollTop: target.eq(0).offset().top - ($(window).height() - target.eq(0).height()) / 2,
            scrollLeft: target.eq(0).offset().left - ($(window).width() - target.eq(0).width()) / 2
        }, 500);

        for (var i=0; i<3; i++){
            setTimeout(function () {
                $(target).removeClass("error");
            }, i*1000+500);

            setTimeout(function () {
                $(target).addClass("error");
            }, i*1000+1000);

        }
    }
}

function showDetail(event)
{
    $this = $(event.target);
    if ($this.text().trim() == ">>>"){
        $this.text("<<<");
        $this.siblings("p").eq(0).removeClass("display-none");
        $this.siblings("p").eq(0).addClass("display-inline-block");
    }else{
        $this.text(">>>");
        $this.siblings("p").eq(0).addClass("display-none");
        $this.siblings("p").eq(0).removeClass("display-inline-block");
    }
}

function setCss()
{
    $('table.show_all_the_parents').css("width", "auto");
}