function showDetailInfo(event){
    $this = $(event.target);
    if ($this.text().trim() == ">>>"){
        $this.text("<<<");
        $this.siblings("p.detail-errors").eq(0).removeClass("display-none");
        $this.siblings("p.detail-errors").eq(0).addClass("display-inline-block");
    }else{
        $this.text(">>>");
        $this.siblings("p.detail-errors").eq(0).addClass("display-none");
        $this.siblings("p.detail-errors").eq(0).removeClass("display-inline-block");
    }
}

function checkDatabrowserData(){
    if (typeof dataBrowserData['errors'] != 'undefined'){
        setValidationErrors(dataBrowserData['errors'], 'error');
    }

    if (typeof dataBrowserData['warnings'] != 'undefined'){
        setValidationErrors(dataBrowserData['warnings'], 'warning');
    }

    if (typeof dataBrowserData['ids'] != 'undefined'){
        selectCheckboxes(dataBrowserData['ids']);
    }

}

function selectCheckboxes(ids){
    if ($("form").children("table.structure").eq(0).attr("data-atim-type") == 'index'){
        var i;
        for(i in ids){
            $("input[type='checkbox'][value='" + ids[i] +"']").prop("checked", true);
        }
    }
}

function setValidationErrors(dataBrowserError, className){
    var validationDiv = $('<div class="validation"><ul class="' + className + '"></ul></div>');
    var validationLi = $('<li><span class="icon16 ' + className + ' mr5px"></span><span class = "message"><span class = "text-message"></span><span class="expandable-span-error-messages" onclick="showDetailInfo(event)">>>></span><p style="margin: 0px" class="display-none detail-errors"></p> </span></li>');

    var i, errorMessage, errors, validationErrors = "";
    var index = $("form").children("table.structure").eq(0).attr("data-atim-type") == 'index';
    for(errorMessage in dataBrowserError){
        errors = dataBrowserError[errorMessage];
        validationLiClone = validationLi.clone();
        validationLiClone.find("span.text-message").append(errorMessage);
        if (index){
            for(id in errors){
                setErrorClass(errors[id], className);
                validationLiClone.find("p.detail-errors").append('<a href="javascript:void(0)" class="scroll-to-error-message" onclick="scrollToErrorLink(\''+errors[id]+'\', \''+ className +'\')">'+id+'</a> ');
            }
        }else{
            for(id in errors){
                validationLiClone.find("p.detail-errors").append('<a href="' + errors[id] + '" target="_blank">'+id+'</a> ');
            }
        }
        validationDiv.find("ul." + className).append(validationLiClone);
    }
    $(".validationWrapper").append(validationDiv);
}

function setErrorClass(link, className){
    var target = $("a.icon16.detail[href='" + link + "']");
    if (target.length != 0) {
        var tr = target.closest("tr");
        if (tr.legth != 0) {
            tr.addClass(className);
        }
    }
}

function scrollToErrorLink(link, className){
    var target = $("a.icon16.detail[href='" + link + "']");
    if (target.length != 0){
        var tr = target.closest("tr");
        if (tr.legth != 0){
            $("html, body").animate({
                scrollTop: tr.eq(0).offset().top - ($(window).height() - tr.eq(0).height()) / 2,
                // scrollLeft: target.eq(0).offset().left - ($(window).width() - target.eq(0).width()) / 2
            }, 500);

            for (var i=0; i<3; i++){
                setTimeout(function () {
                    $(tr).removeClass(className);
                }, i*1000+500);

                setTimeout(function () {
                    $(tr).addClass(className);
                }, i*1000+1000);

            }
        }
    }
}
