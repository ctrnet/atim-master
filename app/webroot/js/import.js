/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */

function setCsvConfig(id){
    $('#csvPopupConfiguration').popup('close');
    $('#csvPopupConfiguration').closest('.popup_outer').remove();

    var form = '<div id="csvPopupConfiguration" class="std_popup question" style="margin: auto; position: relative; display: block;">\n' +
        '   <div class="wrapper">\n' +
        '      <h4>CSV</h4>\n' +
        '      <div style="padding: 10px; background-color: #fff;">\n' +
        '         <div>\n' +
        '            <table class="structure" cellspacing="0">\n' +
        '               <tbody>\n' +
        '                  <tr>\n' +
        '                     <td class="this_column_1 total_columns_1">\n' +
        '                        <table class="columns detail" cellspacing="0">\n' +
        '                           <tbody>\n' +
        '                              <tr>\n' +
        '                                 <td class="label">Separator</td>\n' +
        '                                 <td class="content"><span><span class="nowrap"><input name="data[Config][define_csv_separator]" class=" required" value="' + CSV_SEPARATOR + '" tabindex="2" size="3" maxlength="1" required="required" type="text"> </span></span></td>\n' +
        '                                 <td class="help">\n' +
        '                                    <span class="icon16 help">\n' +
        '                                       &nbsp;&nbsp;&nbsp;&nbsp;\n' +
        '                                       <div>When exporting data to file from the Query Tool this value is used as a separator between fields.</div>\n' +
        '                                    </span>\n' +
        '                                 </td>\n' +
        '                              </tr>\n' +
        '                           </tbody>\n' +
        '                        </table>\n' +
        '                     </td>\n' +
        '                  </tr>\n' +
        '               </tbody>\n' +
        '            </table>\n' +
        '            <div class="submitBar">\n' +
        '               <div class="flyOverSubmit">\n' +
        '                  <div class="bottom_button"><input class="submit" type="submit" value="Submit" style="display: none;"><a href="javascript:setCsvSeparator(' + id + ')" tabindex="6" class="submit"><span class="icon16 submit"></span>Submit</a></div>\n' +
        '               </div>\n' +
        '            </div>\n' +
        '         </div>\n' +
        '      </div>\n' +
        '   </div>\n' +
        '</div>';

    $(form).popup();
    $("#csvPopupConfiguration").siblings(".popup_close").off("click").on("click", function(){
        $('#csvPopupConfiguration').popup('close');
    });
    $('#csvPopupConfiguration input').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            $('#csvPopupConfiguration').popup('close');
            setCsvSeparator(id);
            return false;
        }
    });
}

csvSeparator = null;
function setCsvSeparator(id){

    csvSeparator = $("input[name='data[Config][define_csv_separator]'").val();
    $('#csvPopupConfiguration').popup('close');

    window.location.replace(root_url + "Tools/Imports/exportSummary/" + id + "/" + csvSeparator);
}

function downloadCSVFile(id) {
    window.location.replace(root_url + "Tools/Imports/downloadCsvFile/" + id);
}

function downloadCSVTemplate(id) {
    window.location.replace(root_url + "Tools/Imports/downloadCsvTemplate/" + id);
}