    function fbSettings(e){
        var $this = $(e);

        if ($this.css("cursor") == "not-allowed"){
            return false;
        }

        var data = $this.data("fbSettings");
        var url = root_url + 'Administrate/FormBuilders/settings/';
        if ($("#fbSettings").length===0){
            $("body").append('<div id="fbSettings" class="hidden std_popup"></div>');
        }
        $("#fbSettings").html("<div class='loading'>--- " + STR_LOADING + " ---</div>");
        $("#fbSettings").popup({minWidth: '340px'});
        $.ajax({
            data: data,
            traditional: true,
            type: 'POST',
            url: url,
            success: function (data){
                data = normaliseData(data);

                var isVisible = $("#fbSettings:visible").length;

                $popupForm = $("#fbSettings");
                $popupForm.html('<div class="wrapper"><div class="frame">' + data + '</div></div>');

                if ($popupForm.find('select[name*="can_paste]').length == 1 && typeof $this.data('settings') !== 'undefined'&& typeof $this.data('settings')['can_paste'] !== 'undefined'){
                    $popupForm.find('select[name*="can_paste]').val($this.data('settings')['can_paste']);
                }

                globalInit($popupForm);

                if(isVisible){
                    //recenter popup
                    $popupForm.popup('close');
                    $popupForm.popup({minWidth: '340px'});
                }
                $popupForm.popup({minWidth: '340px'});
                $popupForm.find("form").eq(0).off("submit").on("submit", function(){
                    $thisPopup = $(this);
                    $data = $thisPopup.find("[type!='hidden']").serializeArray();
                    var normalisedData = {};
                    $data.forEach(function(item){
                        normalisedData[item.name.substring(item.name.lastIndexOf("[") + 1, item.name.length -1)] = item.value;
                        // normalisedData[item.name.substring(item.name.lastIndexOf("[") + 1, item.name.length -1).replace(/_./g, x=>x[1].toUpperCase())] = item.value;
                    });

                    $this.data({settings : normalisedData});
                    // $this.siblings(".user-friendly-data").eq(0).text(JSON.stringify(normalisedData));
                    normalised('settings', {settings : normalisedData}, $this.siblings("span.user-friendly-data"));

                    $popupForm.popup("close");
                    return false;
                });
            }
        });

    }

    function addValueDomain(e){
        var $this = $(e);
        
        if ($this.css("cursor") == "not-allowed"){
            return false;
        }
        
        var data = $this.data("valueDomain");
        var url = root_url + 'Administrate/FormBuilders/valueDomain/';
        if ($("#addValueDomain").length===0){
            $("body").append('<div id="addValueDomain" class="hidden std_popup"></div>');
        }
        $("#addValueDomain").html("<div class='loading'>--- " + STR_LOADING + " ---</div>");
        $("#addValueDomain").popup();
        $.ajax({
            data: data,
            traditional: true,
            type: 'POST',
            url: url,
            success: function (data){
                data = normaliseData(data);

                var isVisible = $("#addValueDomain:visible").length;
                
                $popupForm = $("#addValueDomain");
                $popupForm.html('<div class="wrapper"><div class="frame">' + data + '</div></div>');
                globalInit($popupForm);

                if(isVisible){
                    //recenter popup
                    $popupForm.popup('close');
                    $popupForm.popup();
                }
                $popupForm.popup();
                $popupForm.find("form").eq(0).off("submit").on("submit", function(){
                    var $autoComplete = $popupForm.find("input[type=autocomplete]").eq(0);
                    var isMultiSelect = $("input[type='checkbox'].is-multi-select").prop('checked');
                    var valueDomain = {value: $autoComplete.val(), id: (typeof $autoComplete.data("id") !== "undefined")?$autoComplete.data("id"):"0", "multi-select": isMultiSelect};
                    var textMessage = $autoComplete.val() + (isMultiSelect ? multiplechoiceMessage: "");
                    $this.data("valueDomain", valueDomain);
                    $this.siblings("span.user-friendly-data").text(textMessage);
                    $this.siblings("span.user-friendly-data").attr("title", textMessage);
                    if ($autoComplete.val().trim()!=""){
                        $this.parent().find(".fb-value-domain-warning").remove();
                    }else if ($this.parent().find(".fb-value-domain-warning").length == 0){
                        $this.parent().prepend("<span class='fb-value-domain-warning icon16 warning' title = '" + warningValueDomainMessage + "'></span>");
                    }
                    $popupForm.popup("close");
                    return false;
                });
            }        
        });        
    }
    
    function addValidation(e){
        var $this = $(e);
        
        if ($this.css("cursor") === "not-allowed"){
            return false;
        }

        var data = $this.data("validation");
        var url = root_url + 'Administrate/FormBuilders/addValidation/'+$this.closest("tr").find("select.fb_type_select").val();
        if ($("#addValidation").length===0){
            $("body").append('<div id="addValidation" class="hidden std_popup"></div>');
        }
        $("#addValidation").html("<div class='loading'>--- " + STR_LOADING + " ---</div>");
        $("#addValidation").popup();
        $.ajax({
            data: data,
            traditional: true,
            type: 'POST',
            url: url,
            success: function (data) {
                data = normaliseData(data);

                var isVisible = $("#addValidation:visible").length;
                
                $popupForm = $("#addValidation");
                $popupForm.html('<div class="wrapper"><div class="frame">' + data + '</div></div>');
                globalInit($popupForm);

                if(isVisible){
                    //recenter popup
                    $popupForm.popup('close');
                    $popupForm.popup();
                }
                $popupForm.popup();
                
                $trs = $popupForm.find("form table table tr");
                if (addValidationsData == null){
                    addValidationsData = [];
                }
                $trs.each(function() {
                    var $this = $(this);
                    if ($this.find("input").length !== 0) {
                        var shouldDelete = true;
                        addValidationsData.forEach(function(item) {
                            if ($this.find("input[name*=" + item + "]").length > 0) {
                                shouldDelete = false;
                            }
                        });
                        if (shouldDelete) {
                            $this.find("input").prop("disabled", true);
                            $this.find("input").prop("checked", false);
                            $this.find("input").val("");
                        }
                    }
                });

                checkAllTheTime();

                $popupForm.find("input[name*=range_from]").off("change").on("change", function(){
                    var $from = $(this);
                    var $to = $from.closest("td").find("input[name*=range_to]");
                    $to.attr("min", $from.val())
                    if (parseInt($to.val())<parseInt($from.val())){
                        $to.val($from.val());
                    }
                });

                $popupForm.find("input[name*=range_to]").off("change").on("change", function(){
                    var $to = $(this);
                    var $from = $to.closest("td").find("input[name*=range_from]");
                    $from.attr("max", $to.val())
                    if (parseInt($to.val())<parseInt($from.val())){
                        $from.val($to.val());
                    }
                });

                $popupForm.find("input[name*=between_from]").attr("min", "1");
                $popupForm.find("input[name*=between_to]").attr("min", "1");
                
                $popupForm.find("input[name*=between_from]").attr("max", "1000");
                $popupForm.find("input[name*=between_to]").attr("max", "1000");
                
                $popupForm.find("input[name*=between_from]").off("change").on("change", function(){
                    var $from = $(this);
                    var $to = $from.closest("td").find("input[name*=between_to]");
                    $to.attr("min", $from.val())
                    if (parseInt($to.val())<parseInt($from.val())){
                        $to.val($from.val());
                    }
                });

                $popupForm.find("input[name*=between_to]").off("change").on("change", function(){
                    var $to = $(this);
                    var $from = $to.closest("td").find("input[name*=between_from]");
                    $from.attr("max", $to.val())
                    if (parseInt($to.val())<parseInt($from.val())){
                        $from.val($to.val());
                    }
                });

                $popupForm.find("form").eq(0).off("submit").on("submit", function(){
                    
                    $bTo = $popupForm.find("input[name*=between_to]");
                    $bFrom = $popupForm.find("input[name*=between_from]");
                    $rTo = $popupForm.find("input[name*=range_to]");
                    $rFrom = $popupForm.find("input[name*=range_from]");

                    if ($bTo.val() =="" && $bFrom.val()!="" || $bTo.val() !="" && $bFrom.val()==""){
                        $bTo.closest("td").addClass("form-builder-flash-error");
                        setTimeout(function(){
                            $bTo.closest("td").removeClass("form-builder-flash-error");
                        }, 3000);
                        alert(errorToFromMesage);
                        return false;
                    }

                    if ($rTo.val() !="" && $rFrom.val()==""){
                        $rFrom.val("-2147483648");
                    }
                    if ($rTo.val() =="" && $rFrom.val()!=""){
                        $rTo.val("2147483647");
                    }

                    var validationData = $popupForm.find("form").eq(0).serializeArray();
                    $popupForm.find("form").eq(0).find("input[type=hidden]").remove();
                    var validationSerializeData = $popupForm.find("form").eq(0).serialize();
                    validationData = validationData.sort(function (a, b) {
                        return ((a.name < b.name) || (a.name === b.name && a.value > b.value)) ? -1 : 1;
                    }).filter(function (item, index) {
                        return (index === validationData.findIndex(function (item2) {
                            return item2.name === item.name;
                        }));
                    });

                    $this.parent().find(".fb-validation-warning").remove();
                    $this.data("validation", validationData);
                    $this.data("validationSerialise", validationSerializeData);
                    normalised('validation', validationData, $this.siblings("span.user-friendly-data"));

                    $popupForm.popup('close');

                    return false;
                });
            }
        });
    }

    function normalised(type, data, e){
        if (type == 'validation'){
            url = root_url + 'Administrate/FormBuilders/normalised/validation';
        }else if(type == 'settings'){
            url = root_url + 'Administrate/FormBuilders/normalised/settings';
        }

        $.ajax({
            data: data,
            type: 'POST',
            url: url,
            success: function (data) {
                data = JSON.parse(data);
                $(e).text(data.text);
                $(e).attr("title", data.title);
            }
        });
        
    }

    function makeInactiveLink(selector){
        selector.css("opacity", "0.5");
        selector.css("cursor", "not-allowed");
        selector.siblings("span.user-friendly-data").text("");
    }
    
    function makeActiveLink(selector){
        selector.css("opacity", "1");
        selector.css("cursor", "pointer");
    }
    
    function checkAllTheTime(){
        // $("table[atim-structure='form_builder_structure'] input[type='checkbox'][name*='][StructureFormat][flag_add]']").attr('checked', true).attr('disabled', true);

        $(".removeLineLink").on("click", function(){
//            if ($(this).closest("tbody").children("tr").length == 1){
//                return false;
//            }
        });
        $addButton = $("<a name-data = 'has_validation' class='fb_has_validation icon16 add_mini copy-paste-enable' href='javascript:void(0)' title='"+addValidationMessage+"'></a><span class ='user-friendly-data'></span>");
        $("button.fb_has_validation").each(function(){
            $(this).replaceWith($addButton.clone());
        });
        $(".fb_has_validation").off("click").on("click", function(){
            addValidation(this);
            return false;
        });

        $addButton = $("<a name-data = 'is_structure_value_domain' class='fb_is_structure_value_domain_input icon16 add_mini copy-paste-enable' href='javascript:void(0)' title='"+addValueDomainMessage+"'></a><span class ='user-friendly-data'></span>");
        $("button.fb_is_structure_value_domain_input").each(function(){
//            $addButton.attr("tabindex", parseInt($(this).closest("td").prev("td").find("input:visible, select:visible").eq(0).attr("tabindex"))+1);
            $(this).replaceWith($addButton.clone());
        });
        $(".fb_is_structure_value_domain_input").off("click").on("click", function(){
            addValueDomain(this);
            return false;
        });

        $addButton = $("<a name-data = 'is_structure_value_domain' class='fb_settings icon16 add_mini copy-paste-enable' href='javascript:void(0)'></a><span class ='user-friendly-data'></span>");
        $("button.fb_settings").each(function(){
            $(this).replaceWith($addButton.clone());
        });
        $(".fb_settings").off("click").click(function(){
            fbSettings(this);
            return false;
        });

        $select = $("select.fb_type_select");
        $select.off("change").on("change", function(e, b){
            checkSelect($(this), b);
        });

        $("input[type=checkbox]").each(function () {
            $this = $(this);
            if ($this.closest("td").width() < 500) {
                $this.css("width", "100%");
            } else {
                $this.css("width", "12%");
            }
        });
    }
    
    function checkSelect(e, b) {
        val = e.val();
        var $parent = e.closest("tr");
        var $valueDomainInput = $parent.find(".fb_is_structure_value_domain_input").eq(0);
        if (val !== 'select') {
            makeInactiveLink($valueDomainInput);
        } else {
            makeActiveLink($valueDomainInput);
        }

        if ([""].indexOf(val) < 0) {
            makeActiveLink($parent.find(".fb_has_validation"));
        } else {
            makeInactiveLink($parent.find(".fb_has_validation"));
        }

        if ([""].indexOf(val) == 0){
            $parent.find(".fb_has_validation").parent().find(".fb-validation-warning").remove();
        }else if (b != "autoTriger" && JSON.stringify($parent.find(".fb_has_validation").data())!="{}" && $parent.find(".fb_has_validation").data("validation") != "" && $parent.find(".fb-validation-warning").length == 0) {
            $parent.find(".fb_has_validation").parent().prepend("<span class='fb-validation-warning icon16 warning' title = '" + warningValidationMessage + "'></span>");
        }

        $emptyValueDomainData = (typeof $valueDomainInput.data("valueDomain") == "undefined" || typeof $valueDomainInput.data("valueDomain")["value"] == "undefined" || $valueDomainInput.data("valueDomain")["value"] == "");
        
        if (b != "autoTriger"){
            if (val == 'select'){
                if ($emptyValueDomainData && $parent.find(".fb-value-domain-warning").length == 0){
                    $valueDomainInput.parent().prepend("<span class='fb-value-domain-warning icon16 warning' title = '" + warningValueDomainMessage + "'></span>");
                }else if(!$emptyValueDomainData){
                    $parent.find(".fb_is_structure_value_domain_input").siblings("span.user-friendly-data").text($valueDomainInput.data("valueDomain").value);
                    $parent.find(".fb_is_structure_value_domain_input").siblings("span.user-friendly-data").attr("title", $valueDomainInput.data("valueDomain").value);
                }
            }else{
                $valueDomainInput.parent().find(".fb-value-domain-warning").remove();
                $valueDomainInput.parent().find(".user-friendly-data").text("");
            }
        }

    }

    function customizeMainSubmit(){
        // var classNames = [{class: "form_builder_treatment", action: ['add']}, {class: "form_builder_specimen_review", action: ['add']}];
        var classNames = [];

        classNames.forEach(function (item) {
            var $table = $(document).find("table[atim-structure=" + item.class + "]");
            if ($table.length > 0) {

                $table.find("input[label]").each(function () {
                    $this = $(this);
                    var value = $this.val();
                    if (value.indexOf("|||") > -1) {
                        var id = value.split("|||")[1];
                        var val = value.split("|||")[0];
                        $this.val(val);
                        $this.data("id", id);
                    }else{
                        var label = $this.attr("label");
                        var name = $this.attr("name");
//                        var newName = name.substring(0, name.length - 1) + "_id]";
                        if ($("input."+label).length == 1 && $("input."+label).val()!=""){
                            $this.data("id", $("input."+label).val());
                        }
                    }
                });

                $form = $table.closest("form");
                $submit = $form.find("a.submit");
                $submit.on("click", $table, submitFBForm);
                $form.on("submit", $table, submitFBForm);
            }
        });
    }

    function submitFBForm(event){
        $table = event.data;
        $table.find("input[label][type=autocomplete]").each(function () {
            $this = $(this);
            var label = $this.attr("label");
            var name = $this.attr("name");
            var value = $this.val().trim();
            id = (typeof $this.data("id") !== "undefined" && value!='') ? $this.data("id") : 0;
            $("input."+label).val(id);
        });
    }

    function commonFunctionAfterLoad(){
        $("table[atim-structure='form_builder_master_structure'] td p.wraped-text").each(function(){
            $this = $(this);
            searchArray = [
                "The validation rules for formbuilder |||",
                "The settings of each fields are |||"
            ];
            for(i in searchArray){
                if ($this.text().indexOf(searchArray[i]) == 0){
                    text = $this.text();
                    text = text.replace(searchArray[i], "");
    //         title = text.replace(/ \|\|\|\-/g, "\n");
                    text = text.replace(/ \|\|\|\-/g, ", ");
                    text = text.substr(0, text.length - 3);


    // 	$this.html("<span title = "+title+">"+text+"</span>");
                    $this.html("<span>"+text+"</span>");
                }
            }
        });

        $('table[atim-structure^="form_builder_consent"]:not(.index),\n\
            table[atim-structure^="form_builder_diagnosis"]:not(.index),\n\
            table[atim-structure^="form_builder_event"]:not(.index),\n\
            table[atim-structure^="form_builder_treatment"]:not(.index),\n\
            table[atim-structure^="form_builder_specimen_review"]:not(.index),\n\
            table[atim-structure^="form_builder_aliquot_review"]:not(.index),\n\
            table[atim-structure^="form_builder_protocol"]:not(.index),\n\
            table[atim-structure^="form_builder_inventory_action"]:not(.index),\n\
            table[atim-structure^="form_builder_treatment_extend"]:not(.index)').css("width", $(window).width());

        customizeMainSubmit();

        checkAllTheTime();
        
        var $select = $("select.fb_type_select");
        $select.each(function(){
            checkSelect($(this));
        });
        
        if (validationData!=""){
            validationData = JSON.parse(validationData);
            validationData.forEach(function(validation, index){
                var $this = $("table[atim-structure=form_builder_structure] a.fb_has_validation").eq(index);
                $this.data("validation", validation);
                normalised('validation', validation, $this.siblings("span.user-friendly-data"));
            });
        }

        if (fbSettingsData!=""){
            fbSettingsData = JSON.parse(fbSettingsData);
            fbSettingsData.forEach(function(settings, index){
                var $this = $("table[atim-structure=form_builder_structure] a.fb_settings").eq(index);
                $this.data("settings", settings);
                normalised('settings', {settings: settings}, $this.siblings("span.user-friendly-data"));
            });
        }

        if (valueDomainData!=""){
            valueDomainData = JSON.parse(valueDomainData);
            valueDomainData.forEach(function(valueDomain, index){
                var $this = $("table[atim-structure=form_builder_structure] a.fb_is_structure_value_domain_input").eq(index);
                $this.data("valueDomain", valueDomain);
                if ($this.closest("tr").find("select.fb_type_select").val()=="select"){
                    var textMessage = valueDomain["value"] + (valueDomain["multi-select"] == 1 ? multiplechoiceMessage : "");
                    $this.siblings("span.user-friendly-data").text(textMessage);
                    $this.siblings("span.user-friendly-data").attr("title", textMessage);
                }

                if (valueDomain["value"].trim()!=""){
                    $this.parent().find(".fb-value-domain-warning").remove();
                }else if ($this.parent().find(".fb-value-domain-warning").length == 0 && ($this.closest("tr").find("select.fb_type_select").val()=="select")){
                    $this.parent().prepend("<span class='fb-value-domain-warning icon16 warning' title = '" + warningValueDomainMessage + "'></span>");
                }

            });
        }

        if (fbErrorsGrid != "") {
            fbErrorsGrid = JSON.parse(fbErrorsGrid);
            ["common", "detailTest", "masterTest"].forEach(function (i) {
                var $trs = $("table[atim-structure=form_builder_structure] table tbody tr");
                var trs = [];
                $trs.each(function(indexItem, item){
                    if ($(item).find("[name^='data[" +i+ "]']").length!=0){
                        trs.push(item);
                    }
                });
                
                $trs = $(trs);
                $trs.find("*").removeClass("error");
                var $elem;
                if (typeof fbErrorsGrid[i] != "undefined") {
                    var arrayError = Object.keys(fbErrorsGrid[i]).map(function(key) {
                      return fbErrorsGrid[i][key];
                    });                
                    arrayError.forEach(function (error, index) {
                        error.forEach(function (field) {
                            $elem = $trs.eq(index).find("[name$='][" + field + "]']");
                            if ($elem.length == 0) {
                                $elem = $trs.eq(index).find("[name-data=" + field + "]").closest("td");
                            }
                            if ($elem.length == 1) {
                                $elem.addClass("form-builder-flash-error");
                            }
                        });
                    });

                }
            });
        }
    }

    function clickONPlusButton(){
        $(".detailTest .addLineLink").on("click", function(){
            checkAllTheTime();
        });
        
        $(".masterTest .addLineLink").on("click", function(){
//            buildConfirmDialog("addFieldToMaster", addFieldToMaster, [
//                {label:STR_OK, icon: "warning", "action": function(){
//                    $("#addFieldToMaster").popup("close")
                    checkAllTheTime();
//                    return false;
//                }}
//            ]);
//            $("#addFieldToMaster").popup();
//            return false;
        });
        
        $(".common .addLineLink").on("click", function(){
            checkAllTheTime();
        });
    }

    function clickOnSubmit(){
        var validations = [];
        var valueDomains = [];
        var settings = [];
        $("div.plugin_Administrate.controller_FormBuilders form").eq(0).on("submit", function(){
            
            $(this).find(".fb_has_validation").each(function(){
                if (typeof $(this).data("validation") !== 'undefined'){
                    validations.push($(this).data("validation"));
                }else{
                    validations.push("");
                }
            });
            
            $(this).find(".fb_is_structure_value_domain_input").each(function(){
                if (typeof $(this).data("valueDomain") !== 'undefined'){
                    valueDomains.push($(this).data("valueDomain"));
                }else{
                    valueDomains.push({"value":"","id":"0"});
                }
            });
            
            $(this).find(".fb_settings").each(function(){
                if (typeof $(this).data("settings") !== 'undefined'){
                    settings.push($(this).data("settings"));
                }
            });

            $hiddenInput = $("<input type = 'hidden' name = 'settings'>");
            $(this).append($hiddenInput.val(JSON.stringify(settings)));
            
            $hiddenInput = $("<input type = 'hidden' name = 'validationData'>");
            $(this).append($hiddenInput.val(JSON.stringify(validations)));
            
            $hiddenInput = $("<input type = 'hidden' name = 'valueDomainData'>");
            $(this).append($hiddenInput.val(JSON.stringify(valueDomains)));
            
        });
    }

    function formBuilderInitialise(){
        
        $(document).ready(function(){

            initATiMSelectChosen();

            commonFunctionAfterLoad();
            
            clickONPlusButton();
            
            clickOnSubmit();
           
        });

    }

    function initATiMSelectChosen() {

        if ($("table[atim-structure='form_builder_inventory_action']").length >= 1){
            $('select[name="data[InventoryActionControl][apply_on]"]').on('focus', function () {
                // Store the current value on focus and on change
                previous = this.value;
            }).on("change", function (event, init = false) {
                if (!init && (($('select.samples-control_ids').val() !="" || $('select.aliquots-control_ids').val() !="") && this.value != "")) {
                    if (!confirm(confirmChangeApplyOnOptionMessage)) {
                        $(this).val(previous);
                        return false;
                    }
                }
                previous = this.value;

                var selectedValue = $(this).val();

                if (selectedValue == '') {
                    $('select.samples-control_ids').prop('disabled', true).val("").trigger("chosen:updated");
                    $('select.aliquots-control_ids').prop('disabled', true).val("").trigger("chosen:updated");

                } else if (selectedValue == 'samples') {
                    //Sample: active, Aliquot: inactive
                    $("select.samples-control_ids option[value='all']").show();
                    $("select.samples-control_ids option[value='all']").prop("disabled", false);
                    $('select.samples-control_ids').off('change');
                    $('select.aliquots-control_ids').prop('disabled', true).val("").trigger("chosen:updated");
                    $('select.samples-control_ids').prop('disabled', false).trigger("chosen:updated");
                } else if (selectedValue == 'samples and all related aliquots') {
                    //Sample: active, Aliquot: inactive
                    $("select.samples-control_ids option[value='all']").show();
                    $("select.samples-control_ids option[value='all']").prop("disabled", false);
                    $('select.samples-control_ids').off('change');
                    $('select.aliquots-control_ids').prop('disabled', true).val("").trigger("chosen:updated");
                    $('select.samples-control_ids').prop('disabled', false).trigger("chosen:updated");
                } else if (selectedValue == 'samples and some related aliquots') {
                    //Sample: active, Aliquot: active but updated according to the selected samples
                    $("select.samples-control_ids option[value='all']").hide();
                    $("select.samples-control_ids option[value='all']").prop("disabled", "disabled");
                    $('select.samples-control_ids').on("change", function () {

                        var vals = $(this).val();
                        var allPossibleOptions = [];
                        var i;
                        $("select.aliquots-control_ids option").hide();
                        $("select.aliquots-control_ids option").prop("disabled", "disabled");
                        for (i = 0; i < vals.length; i++) {
                            if (vals[i] != '') {
                                sampleMasterIds[vals[i]].forEach(function (aliquotId) {
                                    $("select.aliquots-control_ids option[value='" + aliquotId + "']").show();
                                    $("select.aliquots-control_ids option[value='" + aliquotId + "']").prop("disabled", false);
                                    allPossibleOptions.push(aliquotId);
                                });
                            }
                        }

                        var optionVals = $("select.aliquots-control_ids").val();
                        var filteredOptions = optionVals ? optionVals.filter(value => allPossibleOptions.includes(parseInt(value))): [];

                        $("select.aliquots-control_ids").val(filteredOptions);

                        $("select.aliquots-control_ids").trigger("chosen:updated");
                        $("select.samples-control_ids").trigger("chosen:updated");

                    });

                    if (!init){
                        $('select.samples-control_ids').prop('disabled', false).val("").trigger("chosen:updated");
                        $("select.aliquots-control_ids option").hide();
                        $("select.aliquots-control_ids option").prop("disabled", "disabled");
                        $('select.aliquots-control_ids').prop('disabled', false).val("").trigger("chosen:updated");
                    }else{
                        $('select.samples-control_ids').trigger("change");
                    }
                } else if (selectedValue == 'samples and aliquots not necessarily related') {
                    //Sample: active, Aliquot: active
                    $("select.samples-control_ids option[value='all']").show();
                    $("select.samples-control_ids option[value='all']").prop("disabled", false);
                    $("select.aliquots-control_ids option").show();
                    $("select.aliquots-control_ids option").prop("disabled", false);
                    $('select.samples-control_ids').prop('disabled', false).trigger("chosen:updated");
                    $('select.aliquots-control_ids').prop('disabled', false).trigger("chosen:updated");
                    $('select.samples-control_ids').off('change');
                } else if (selectedValue == 'aliquots') {
                    //Sample: inactive, Aliquot: active
                    $("select.aliquots-control_ids option").show();
                    $("select.aliquots-control_ids option").prop("disabled", false);
                    $('select.samples-control_ids').prop('disabled', true).val("").trigger("chosen:updated");
                    $('select.aliquots-control_ids').prop('disabled', false).trigger("chosen:updated");
                } else if (selectedValue = 'aliquots related to samples') {
                    //Sample: active, Aliquot: inactive
                    $("select.samples-control_ids option[value='all']").show();
                    $("select.samples-control_ids option[value='all']").prop("disabled", false);
                    $('select.aliquots-control_ids').prop('disabled', true).val("").trigger("chosen:updated");
                    $('select.samples-control_ids').prop('disabled', false).trigger("chosen:updated");
                }

            });

            if ($('select[name="data[InventoryActionControl][apply_on]"]').val() == ''){
                $('select.samples-control_ids').prop('disabled', true).val("").trigger("chosen:updated");
                $('select.aliquots-control_ids').prop('disabled', true).val("").trigger("chosen:updated");
            }else {
                $('select[name="data[InventoryActionControl][apply_on]"]').trigger("change", ['true']);
            }

        }

    }

    function formBuilderPreview(a){
        url = $(a).attr("href");
        if ($("#formBuilderPreview").length===0){
            $("body").append('<div id="formBuilderPreview" class="hidden std_popup"></div>');
        }
        $("#formBuilderPreview").html("<div class='loading'>--- " + STR_LOADING + " ---</div>");
        $("#formBuilderPreview").popup();
        $.ajax({
            data: [],
            type: 'POST',
            url: url,
            success: function (data) {
                data = normaliseData(data)


                var isVisible = $("#formBuilderPreview:visible").length;

                $popupForm = $("#formBuilderPreview");
                $popupForm.html('<div class="wrapper"><div class="frame">' + data + '</div></div>');
                globalInit($popupForm);
                
                
                if (isVisible) {
                    $popupForm.popup('close');
                    $popupForm.popup();
                }
                $popupForm.popup();
                
                $popupForm.find("a").attr("href", "javascript:alert(\"" + disableLinkMessage + "\")");
                $popupForm.find("a").prop("target", "");
            }
        });
    }
    
    function formBuilderTestToProd(a){
        var url = $(a).attr("href");
        buildConfirmDialog("noTestToProdConfirmation", noTestToProdConfirmation, [
            {label:STR_YES, icon: "detail", "action": function(){
                $("#noTestToProdConfirmation").popup("close");
                document.location = $("#noTestToProdConfirmation").data('link');
            }},
            {label:STR_NO, icon: "delete", "action": function(){
                $("#noTestToProdConfirmation").popup("close");
                return false;
            }}, 
        ]);
        $("#noTestToProdConfirmation").popup();
        $("#noTestToProdConfirmation").data('link', $(event.currentTarget).prop("href"));
        $("#noTestToProdConfirmation").find(".bottom_button").eq(1).find("a").trigger("focus")
    }
    
    function formBuilderDisable(a){
        var url = $(a).attr("href");
        message = (url.indexOf('disable') > 0) ? disableTheControl : enableTheControl;
        buildConfirmDialog("disableTheControl", message, [
            {label:STR_YES, icon: "detail", "action": function(){
                $("#disableTheControl").popup("close");
                document.location = $("#disableTheControl").data('link');
            }},
            {label:STR_NO, icon: "delete", "action": function(){
                $("#disableTheControl").popup("close");
                return false;
            }}, 
        ]);
        $("#disableTheControl").popup();
        $("#disableTheControl").data('link', $(event.currentTarget).prop("href"));
        $("#disableTheControl").find(".bottom_button").eq(1).find("a").trigger("focus")
    }

    function formBuilderActivateDeactivate(a){
        var url = $(a).attr("href");
        message = (url.indexOf('deactivate') > 0) ? activateTheControl : deactivateTheControl;
        buildConfirmDialog("disableTheControl", message, [
            {label:STR_YES, icon: "detail", "action": function(){
                $("#disableTheControl").popup("close");
                document.location = $("#disableTheControl").data('link');
            }},
            {label:STR_NO, icon: "delete", "action": function(){
                $("#disableTheControl").popup("close");
                return false;
            }},
        ]);
        $("#disableTheControl").popup();
        $("#disableTheControl").data('link', $(event.currentTarget).prop("href"));
        $("#disableTheControl").find(".bottom_button").eq(1).find("a").trigger("focus")
    }

    function formBuilderDictionary(a){
        url = $(a).attr("href");
        if ($("#formBuilderDictionary").length===0){
            $("body").append('<div id="formBuilderDictionary" class="hidden std_popup"></div>');
        }
        $("#formBuilderDictionary").html("<div class='loading'>--- " + STR_LOADING + " ---</div>");
        $("#formBuilderDictionary").popup();
        $.ajax({
            data: [],
            type: 'POST',
            url: url,
            success: function (data) {
                data = normaliseData(data)

                if (data == "nok"){
                    $("#formBuilderDictionary").popup("close");
                    buildConfirmDialog("formBuilderDictionary", noDataDictionary, [{label:"OK", icon: "confirm", "action": function(){$("#formBuilderDictionary").popup("close")}}]);
                    $("#formBuilderDictionary").popup();
                }else{
                    var isVisible = $("#formBuilderDictionary:visible").length;

                    $popupForm = $("#formBuilderDictionary");
                    $popupForm.html('<div class="wrapper"><div class="frame">' + data + '</div></div>');
                    globalInit($popupForm);


                    if (isVisible) {
                        $popupForm.popup('close');
                        $popupForm.popup({isModal: false});
                    }
                    $popupForm.popup({isModal: false});

                    $firstActionButton = $popupForm.find("div.actions div.bottom_button").eq(0);
                    if ($firstActionButton.position().left !=0){
                        $firstActionButton.offset({left: $firstActionButton.offset().left - $firstActionButton.position().left + 10});
                    }
                    $firstActionButton.off("click").on("click", function () {
                        $popupForm.popup('close');
                        return false;
                    });
                    count = $popupForm.find("table[atim-structure='form_builder_i18n'] tbody").eq(1).find("td:first-child").length;
                    $popupForm.find("table[atim-structure='form_builder_i18n'] tbody").eq(1).find("td:first-child").each(function (a) {
                        $this = $(this).find("input");
                        $this.attr("tabindex", 2 * a + 1);
                    });

                    $popupForm.find("table[atim-structure='form_builder_i18n'] tbody").eq(1).find("td:last-child").each(function (a) {
                        $this = $(this).find("input");
                        $this.attr("tabindex", 2 * a + 2);
                    });

                    $popupForm.find("a.submit").attr("tabindex", 2 * count + 3);
                    $popupForm.find("a.cancel").attr("tabindex", 2 * count + 4);

                }
            }
        });
    }

    function formBuilderCopy(a){
        var url = $(a).attr("href");
        buildConfirmDialog("copyTheControl", copyTheControl, [
            {label:STR_YES, icon: "detail", "action": function(){
                $("#copyTheControl").popup("close");
                document.location = $("#copyTheControl").data('link');
            }},
            {label:STR_NO, icon: "delete", "action": function(){
                $("#copyTheControl").popup("close");
                return false;
            }},
        ]);
        $("#copyTheControl").popup();
        $("#copyTheControl").data('link', $(event.currentTarget).prop("href"));
        $("#copyTheControl").find(".bottom_button").eq(1).find("a").trigger("focus")
    }