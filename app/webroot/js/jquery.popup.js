/**
 * Script for displaying popups
 * @author: FM L'Heureux
 * @date: 2010-07-10
 * @version: 0.0.1
 * @see: http://frank-mich.com/
 **/
var FmPopup = function(popup){
	$(popup).data('FmPopup', this);
	this.closePop = true;
	this.isModal = true;
	this.closable = true;
	this.movable = true;
	this.popupOuter = null;
	this.minWidth = 0;
	this.retainFocus = FmPopup.prototype.retainFocus;
};

jQuery.fn.popup = function(options){
    setPopUpTabIndex($(this));
	var fmPopup = $(this).data('FmPopup') == undefined ? new FmPopup(this) : $(this).data('FmPopup');
	if(options != undefined && options.closable != undefined){
		fmPopup.closable = options.closable; 
	}
	if(options != undefined && options.minWidth != undefined){
		fmPopup.minWidth = options.minWidth;
	}
	
	if(options != undefined && options.isModal != undefined){
		fmPopup.isModal = options.isModal;
	}

	if(options != undefined && options.movable != undefined){
		fmPopup.movable = options.movable; 
	}

	if(fmPopup.popupOuter == null){
		$("body").append("<div class='popup_outer'>"
				+ "<div class='popup_container' style='min-width: " + fmPopup.minWidth + "'></div>"
				+ "<div class='popup_close'><a href='javascript:void(0)'>X</a></div>"
				+ "</div>");
		fmPopup.popupOuter = $("body div.popup_outer").last();
		fmPopup.popupOuter.data("FmPopup.prototype.id", FmPopup.prototype.id ++);
		if(options != undefined && options.background != undefined){
			fmPopup.popupOuter.css("background", options.background);
		}
		$(fmPopup.popupOuter).on("dblclick", function () {
                    if (fmPopup.closePop){
                        $(fmPopup.popupOuter).hide();
                        setMainFormTabIndex();
                    }
                    fmPopup.closePop = true;
		});
		$(fmPopup.popupOuter).on("click", function(){
			if(fmPopup.closePop && fmPopup.closable && fmPopup.isModal){
				$(fmPopup.popupOuter).hide();
				setMainFormTabIndex();
			}else{
				fmPopup.closePop = true;
			}
		});
		$(fmPopup.popupOuter).find(".popup_container").on("click", function(event){
			fmPopup.closePop = false;
		});
		$(fmPopup.popupOuter).find(".popup_container").on("dblclick", function(event){
			fmPopup.closePop = false;
		});
		$(document).on("keyup", function(event) {
			if(event.keyCode == 27 && fmPopup.closable) { // Capture Esc key
				//TODO: close only toppest popup
				$(fmPopup.popupOuter).hide();
                                setMainFormTabIndex();
			}
		});
		
		$(this).css({
			"margin" : "auto",
			"position" : "relative"
		});
		
		fmPopup.popupOuter.find(".popup_container").append($(this));
		if(fmPopup.closable){
			fmPopup.popupOuter.find(".popup_container").append(fmPopup.popupOuter.find(".popup_close"));
			fmPopup.popupOuter.find(".popup_close").on("click", function(){fmPopup.popupOuter.hide();setMainFormTabIndex();});
		}else{
			fmPopup.popupOuter.find(".popup_close").hide();
                        setMainFormTabIndex();
		}
	}
	
	if(options == "close"){
		fmPopup.popupOuter.hide();
                setMainFormTabIndex();
	}else if(options == "center"){
		if(fmPopup.popupOuter.find(":visible").first()){
			var container = $(fmPopup.popupOuter).find(".popup_container");
			container.css({
				left : $(window).width() / 2 - container.width() / 2 + "px",
				top : $(window).height() / 2 - container.height() / 2 + "px"
			});
		}
	}else{
		$(this).show();
		var container = $(fmPopup.popupOuter).find(".popup_container");
		$(fmPopup.popupOuter).show();
                $(fmPopup.popupOuter).find("input[type!='hidden'], select").eq(0).trigger("focus");
		container.css({
			left : $(window).width() / 2 - container.width() / 2 + "px",
			top : $(window).height() / 2 - container.height() / 2 + "px"
		});
		
		fmPopup.popupOuter.find(":tabbable").off('keydown', fmPopup.retainFocus).on("keydown", fmPopup, fmPopup.retainFocus);
                if (fmPopup.movable){
                    $(this).closest(".popup_container").draggable({ cancel: 'td,tr,input,a,.popup_close,select', cursor: "all-scroll"});
                }

        $(this).find("input:visible:not(.chosen-search-input), select:visible, textarea:visible").first().trigger("focus");

	}
	
};
FmPopup.prototype.id = 1;
/**
 * Keeps focus within the popup at all times
 * @param event
 * @returns {Boolean}
 */
FmPopup.prototype.retainFocus = function(event){
	if(event.keyCode == 9){
		$(":focus").data("FmPopup.startingFocus", true);
		//ToDo:: (JQuey Update) Check if JQuery update works (First & last)
		var findParameter;
		if (event.shiftKey){
			findParameter = event.data.popupOuter.find(":tabbable").first();
		}else{
			findParameter = event.data.popupOuter.find(":tabbable").last();
		}
		if($(findParameter).data("FmPopup.startingFocus")){
			$(":focus").data("FmPopup.startingFocus", null);

			//ToDo:: (JQuey Update) Check if JQuery update works (First & last)
			if (event.shiftKey){
				event.data.popupOuter.find(":tabbable").last().trigger("focus");
			}else{
				event.data.popupOuter.find(":tabbable").first().trigger("focus");
			}

			return false;
		}
		$(":focus").data("FmPopup.startingFocus", null);
		return true;
	}
};

function setMainFormTabIndex(){
    $(".mainFieldset").find("input, textarea, select, button, a").each(function(){
        var $this = $(this);
        var tabIndex = $this.attr("data-tabindex-old");
		if (typeof tabIndex !== 'undefined'){
        $this.attr("tabindex", tabIndex);
		}
    });
}

function setPopUpTabIndex(popup){
    $(".mainFieldset").find("input, textarea, select, button, a").filter(function(){
        return $(this).closest(popup).length == 0
    }).each(function(){
        var $this = $(this);
        var tabIndex = $this.attr("tabindex");
        if (tabIndex =="" || tabIndex == 0 || isNaN(tabIndex) || tabIndex>0){
            $this.attr("data-tabindex-old", tabIndex);
            $this.attr("tabindex", -9999);
        }
    });
}