<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

$structureOptions = array(
    'links' => [
        'top' => '/Users/checkTfa/',
    ],
    'type' => 'add',
    'settings' => array(
        'header' => __('tfa_structure_header', null)
    )
);

$this->Structures->build($atimStructure, $structureOptions);
?>
<style>
    .pointer {
        cursor: pointer;
    }
</style>

<script>
    var tfa = true;
    var clickHereToLoadTheQRCode = "<?= ___('click here to load the tfa_secret_qr_code') ?>"
    var clickHereToHideTheQRCode = "<?= ___('click here to hide the tfa_secret_qr_code') ?>"
    var clickHereToLoadSecretKey = "<?= ___('click here to load the tfa_secret_url') ?>"
    var clickHereToHideSecretKey = "<?= ___('click here to hide the tfa_secret_url') ?>"

    function initTfa() {
        initButtons();
    }

    function initButtons() {
        $("button.request-qr-code-tfa").replaceWith("<span class = 'request-qr-code-tfa'></span>");
        $("span.request-qr-code-tfa").css({"display": "inline-block", "vertical-align": "top"}).text("").addClass("icon16").addClass("detail").addClass("pointer").addClass("no-border").attr("title", clickHereToLoadTheQRCode).on('click', loadTheQRCode);
        $("span.request-qr-code-tfa").after("<span id='qr-code-image-id'></span>");

        $("button.request-secret-code-tfa").replaceWith("<span class = 'request-secret-code-tfa'></span>");
        $("span.request-secret-code-tfa").css({"display": "inline-block", "vertical-align": "top"}).text("").addClass("icon16").addClass("detail").addClass("pointer").addClass("no-border").attr("title", clickHereToLoadSecretKey).on('click', loadSecretKey);
        $("span.request-secret-code-tfa").after("<span id='qr-code-secret-id'></span>");
    }

    function loadTheQRCode(event) {
        var target = event.target;
        if ($(target).hasClass("loading")) {
            return false;
        }
        $(target).addClass("loading").removeClass('detail').removeClass("pointer");

        $.get(root_url + "Users/getTFAData/qrcode/", function (data) {
            data = normaliseData(data);
            if ($(target).hasClass("loading")) {
                $(target).removeClass("loading").addClass('hide').addClass("pointer").off('click').on('click', hideTheQRCode).attr("title", clickHereToHideTheQRCode);
                $("#qr-code-image-id").html(data);
            }
        });

        return false;
    }

    function hideTheQRCode(event) {
        var target = event.target;
        $(target).removeClass("loading").addClass('detail').addClass("pointer").off('click').on('click', loadTheQRCode).attr("title", clickHereToLoadTheQRCode);
        $("#qr-code-image-id").html("");
        return false;
    }

    function loadSecretKey(event) {
        var target = event.target;
        if ($(target).hasClass("loading")) {
            return false;
        }
        $(target).addClass("loading").removeClass('detail').removeClass("pointer");

        $.get(root_url + "Users/getTFAData/secretKey/", function (data) {
            data = normaliseData(data);
            if ($(target).hasClass("loading")) {
                $(target).removeClass("loading").addClass('hide').addClass("pointer").off('click').on('click', hideSecretKey).attr("title", clickHereToHideSecretKey);
                $("#qr-code-secret-id").html(data);
            }
        });

        return false;

    }

    function hideSecretKey(event) {
        var target = event.target;
        $(target).removeClass("loading").addClass('detail').addClass("pointer").off('click').on('click', loadSecretKey).attr("title", clickHereToLoadSecretKey);
        $("#qr-code-secret-id").html("");
        return false;
    }

</script>