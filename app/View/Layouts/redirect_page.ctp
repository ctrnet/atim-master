<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

 ?>

<form id="redirectForm" action="<?php echo $url ?>" method="post">
    <input id="compressedData" type="hidden" name = 'compressedData' value=''>
</form>
<script>
    var compressedData = JSON.stringify(<?php echo $compressedData;?>);
    document.getElementById('compressedData').value = compressedData;
    document.getElementById('redirectForm').submit();
</script>