<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link http://cakephp.org CakePHP(tm) Project
 * @package app.Controller
 * @since CakePHP(tm) v 0.2.9
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    private static $missingTranslations = array();

    private static $me = null;

    private static $acl = null;

    public static $beignFlash = false;

    const CONFIRM = 1;

    const ERROR = 2;

    const WARNING = 3;

    const INFORMATION = 4;

    public $uses = array(
        'Config',
        'SystemVar'
    );

    public $components = array(
        'Acl',
        'Session',
        'SessionAcl',
        'Auth',
        'Menus',
        'RequestHandler',
        'Structures',
        'PermissionManager',
        'Paginator',
        'Flash',
        'DebugKit.Toolbar',
        'Setup'
    );

    public $helpers = array(
        'Session',
        'Csv',
        'Html',
        'Js',
        'Shell',
        'Structures',
        'Time',
        'Form'
    );
    
    // use AppController::getCalInfo to get those with translations
    private static $calInfoShort = array(
        1 => 'jan',
        'feb',
        'mar',
        'apr',
        'may',
        'jun',
        'jul',
        'aug',
        'sep',
        'oct',
        'nov',
        'dec'
    );

    private static $calInfoLong = array(
        1 => 'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    );

    public static $query;

    private static $calInfoShortTranslated = false;

    private static $calInfoLongTranslated = false;

    public static $highlightMissingTranslations = true;
    
    // Used as a set from the array keys
    public $allowedFilePrefixes = array();

    private function addUrl($type = "nonAjax")
    {
        if (! empty(Router::getPaths($this->here)->url)) {
            $_SESSION['url'][$type][] = "/" . Router::getPaths($this->here)->url;
        }
    }
    
    private function decompressData(&$data)
    {
        $compressedData = json_decode($data['compressedData'], true);
        if (!empty($compressedData[0]['name']) && $compressedData[0]['name'] == 'compressedData'){
            if (!empty($compressedData[0]['value']) && is_string($compressedData[0]['value'])){
                $compressedData = json_decode($compressedData[0]['value'], true);
            }
        }
        unset($data['compressedData']);
        $index= 0;
        foreach ($compressedData as &$datum){
            if (substr($datum['name'], strlen($datum['name'])-2, strlen($datum['name']))=='[]'){
                $datum['name'] = substr($datum['name'],0, strlen($datum['name'])-2) . "[$index]";
                $index++;
            }else{
                $index= 0;
            }
            $datum['name'] = str_replace(array('[', ']'), array("['", "']"), $datum['name']);
            eval('return $'.$datum["name"].' = $datum["value"];');
        }
    }

    private function decompressBatchSetData(&$data)
    {
        $batchSetData = $data['BatchSetCompressedData'];
        if (!empty(key($batchSetData)) && !empty(key(current($batchSetData))) && !empty(current(current($batchSetData)))){
            $modelKey = key($batchSetData);
            $fieldKey = key(current($batchSetData));
            $values = explode(",", current(current($batchSetData)));
            unset($data['BatchSetCompressedData']);
            foreach ($values as $value){
                eval('return $data["'.$modelKey.'"]["'.$fieldKey.'"][] = "'.$value.'";');
            }
        }
    }

    private function isCSVExportMode()
    {
        $csvExportMode = false;
        if (!empty($this->request->params['plugin']) && !empty($this->request->params['controller']) && !empty($this->request->params['action'])){
            if ($this->request->params['plugin'] == 'Datamart' && $this->request->params['controller'] == 'Browser' && $this->request->params['action'] == 'csv'){
                $csvExportMode = true;
            }elseif ($this->request->params['plugin'] == 'Datamart' && $this->request->params['controller'] == 'Csv' && $this->request->params['action'] == 'csv'){
                $csvExportMode = true;
            }
        }
        return $csvExportMode;
    }

    private function checkData()
    {
        if (session_status() !== PHP_SESSION_ACTIVE) {
            return false;
        }

        if ($this->isCSVExportMode()){
            Configure::write('debug', 0);
            $this->Components->unload('DebugKit.Toolbar');
        }
        if (empty($this->request->data) && !$this->request->is('ajax')){
            if (isset($_SESSION['aliases'])){
                unset($_SESSION['aliases']);
            }
        }
        if (!$this->request->is('ajax')){
            if (isset($_SESSION['notification'])){
                if (!Configure::read('browser_notification')){
                    unset($_SESSION['notification']);
                }else{
                    $_SESSION['notification']['ready'] = "OK";
                }
            }
            $_SESSION['ATiMDebug']['time'] = round(microtime(true) * 10);
            $_SESSION['ajaxURLs'] = array();

            $_SESSION['hooks'] = array();
            $this->set('appControllerParams', json_encode(array(
                'controller' => $this->params['controller'],
                'action' => $this->params['action'],
                'plugin' => $this->params['plugin'],
                'parameters' => $this->params['pass'])));
        }

        if (isset($this->request->data['compressedData'])){
            $this->decompressData($this->request->data);
            $this->request->data = normalizeCompressedDataForReceive($this->request->data);
        }

        if (isset($this->request->data['BatchSetCompressedData'])){
            $this->decompressBatchSetData($this->request->data);
        }

        if (Configure::read('installation_mode') != 0){
            if (ob_get_contents()){
                ob_end_clean();
            }

            echo "<h1>".__('The ATiM is on maintenance for now.')."</h1>";
            die();
        }
    
        if (session_status() === PHP_SESSION_ACTIVE) {
            if (in_array($this->params->params['controller'], array('FormBuilders')) !== false) {
                $_SESSION['force_rewrite_cache'] = true;
            } else {
                $_SESSION['force_rewrite_cache'] = false;

            }
        }

    }

    private function checkUrl()
    {
        $maintenanceMode = Configure::read("maintenance_mode");

        if ($maintenanceMode){
            $pca = strtolower($this->request->params['plugin']) . "/" . strtolower($this->request->params['controller']) . "/" . strtolower($this->request->params['action']);
            if (!empty($_SESSION['Auth']['User']) && $pca != '/users/logout' && $pca != '/users/login'){
                if ($_SESSION['Auth']['User']['dev_user'] != 1){

                    if (ob_get_contents()){
                        ob_end_clean();
                    }

                    $wwwRoot = $this->request->webroot;

                    echo "<a href = '$wwwRoot/Users/logout'><h1>".__('The ATiM is on maintenance for now.')."</h1></a>";
                    echo "<script>setTimeout(function(){window.location.replace('$wwwRoot/Users/logout');}, 5000);</script>";

                    ob_flush();
                    die();
                }
            }

        }

        if (session_status() !== PHP_SESSION_ACTIVE) {
            return false;
        }
        if (empty($_SESSION['url'])) {
            $_SESSION['url'] = array(
                'nonAjax' => array(),
                'ajax' => array(),
                'all' => array()
            );
        }
        
        if (! $this->request->is('ajax')) {
            $this->addUrl('nonAjax');
        } else {
            $this->addUrl('ajax');
        }
        $this->addUrl('all');
    }

    private function getBackHistoryUrl($type = "nonAjax", $num = 1)
    {
        $url = "/Menus";
        if (isset($_SERVER["HTTP_REFERER"])) {
            $url = $_SERVER["HTTP_REFERER"];
        } else {
            $size = countCustom($_SESSION['url'][$type]);
            if ($size <= $num) {
                $url = "/Menus";
            } else {
                $currentUrl = $_SESSION['url'][$type][$size - 1];
                $url = $_SESSION['url'][$type][$size - $num - 1];
                $index = $size - $num - 1;
                while ($url == $currentUrl && $index > 0) {
                    $index --;
                    $url = $_SESSION['url'][$type][$index];
                }
                if ($index == - 1) {
                    $url = "/Menus";
                }
                array_splice($_SESSION['url'][$type], $index + 1);
            }
        }
        return $url;
    }

    private function checkMultiLogin()
    {
        $multiUserLogin = Configure::read('multi_user_login');

        if (session_status() !== PHP_SESSION_ACTIVE) {
            return false;
        }

        if ($this->request->is('ajax')){
            return false;
        }

        if (!empty($multiUserLogin)){
            return false;
        }

        if ($this->params['action'] == 'logout' && $this->params['controller'] == 'Users'){
            return false;
        }

        $userCookieModel = AppModel::getInstance('', "UserCookie", false);
        $atimSessionID = (!empty($_COOKIE[Configure::read("Session.cookie")])) ? $_COOKIE[Configure::read("Session.cookie")] : null;

        if(empty($atimSessionID)){
            return false;
        }

        $userId = !empty($_SESSION['Auth']['User']['id']) ? $_SESSION['Auth']['User']['id'] : 0;

        if(empty($userId)){
            return false;
        }

        $userCookies = $userCookieModel->find('first', [
            'conditions' => [
                $userCookieModel->name.'.user_id' => $userId,
                $userCookieModel->name.'.session_id' => $atimSessionID,
            ]
        ]);
        if (empty($userCookies)){
            $userCookieModel->save(['UserCookie'=>[
                'user_id' => $userId,
                'session_id' => $atimSessionID,
                'deleted' => '0',

            ]]);
        }elseif(isset($userCookies['UserCookie']['deleted']) && $userCookies['UserCookie']['flag_active'] == '0'){
            $this->atimFlashError(__('there is another login with your username'), '/Users/logout');
        }

        $userCookieModel->updateAll(
            [
                'flag_active' => '0',
            ],[
                $userCookieModel->name.'.user_id' => $userId,
                $userCookieModel->name.'.flag_active' => '1',
                $userCookieModel->name.'.session_id <>' => $atimSessionID,
            ]
        );

    }

    private function initialization()
    {
        $this->checkUrl();
        $this->checkData();
        $this->checkMultiLogin();
    }

    /**
     * This function is executed before every action in the controller.
     * It's a
     * handy place to check for an active session or inspect user permissions.
     */
    public function beforeFilter()
    {
        $this->initialization();
        App::uses('Sanitize', 'Utility');
        AppController::$me = $this;
        if (Configure::read('debug') != 0) {
            Cache::clear(false, "structures");
            Cache::clear(false, "menus");
        }
        
        if ($this->Session->read('permission_timestamp') < $this->SystemVar->getVar('permission_timestamp')) {
            $this->resetPermissions();
        }
        if (Configure::read('Config.language') != $this->Session->read('Config.language')) {
            // set language
            // echo(Configure::read('Config.language'));
            $this->Session->write('Config.language', Configure::read('Config.language'));
        }
        
        $this->Auth->authorize = 'Actions';
        
        // Check password should be reset
        $lowerUrlHere = strtolower($this->request->here);
        if ($this->Session->read('Auth.User.force_password_reset') && strpos($lowerUrlHere, '/users/logout') === false) {
            if (strpos($lowerUrlHere, '/customize/passwords/index') === false) {
                if (! $this->request->is('ajax')) {
                    $this->redirect('/Customize/Passwords/index/');
                }
            }
        }

        if (
            !empty(Configure::read('2fa_microsoft_google_auth')) &&
            ($this->Session->read('Auth.User.force_tfa_show') || $this->Session->read('Auth.User.force_tfa')) &&
            (strpos($lowerUrlHere, '/users/logout') === false && strpos($lowerUrlHere, 'users/gettfadata') === false && !empty($_SESSION['Auth'])) &&
            empty($this->Session->read('Auth.User.force_password_reset'))
        ) {
            if (strpos($lowerUrlHere, '/users/checktfa') === false) {
                if (!$this->request->is('ajax')) {
                    $this->redirect('/Users/checkTfa');
                }
            }
        }

        // record URL in logs
        $logActivityData['UserLog']['user_id'] = $this->Session->read('Auth.User.id');
        $logActivityData['UserLog']['url'] = $this->request->here;
        $logActivityData['UserLog']['visited'] = now();
        $logActivityData['UserLog']['allowed'] = 1;
        
        if (isset($this->UserLog)) {
            $logActivityModel = & $this->UserLog;
        } else {
            App::uses('UserLog', 'Model');
            $logActivityModel = new UserLog();
        }
        
        if ($logActivityData['UserLog']['user_id']) {
        $logActivityModel->save($logActivityData);
        }
        

        // record URL in logs file
        
        $logPath = Configure::read('atim_user_log_output_path');
        $debug = Configure::read("debug");
        if (!empty($logPath)) {
            if (is_dir($logPath)){
                $userLogFileHandle = fopen($logPath . '/user_logs.txt', "a");
                if ($userLogFileHandle) {
                    $userLogStrg = '[' . $logActivityData['UserLog']['visited'] . '] ' . '{IP: ' . AppModel::getRemoteIPAddress() . ' || user_id: ' . (strlen($logActivityData['UserLog']['user_id']) ? $logActivityData['UserLog']['user_id'] : 'NULL') . '} ' . $logActivityData['UserLog']['url'] . ' (allowed:' . $logActivityData['UserLog']['allowed'] . ')';
                    fwrite($userLogFileHandle, "$userLogStrg\n");
                    fclose($userLogFileHandle);
                } else {
                    $logDirectory = $logPath;
                    $permission = substr(sprintf('%o', fileperms($logDirectory)), - 4);
                    if ($debug > 0) {
                        if ($permission != '0777') {
                            AppController::addWarningMsg(__('the permission of "log" directory is not correct.'));
                        } else {
                            AppController::addWarningMsg(__("unable to write user log data into 'user_logs.txt' file"));
                        }
                    }
                }
            }else{
                if ($debug > 0) {
                    AppController::addWarningMsg(__('the log directory does not exist: %s', $logPath));
                }
            }
        }
        
        
        // menu grabbed for HEADER
        if ($this->request->is('ajax')) {
            Configure::write('debug', 0);
            $this->Components->unload('DebugKit.Toolbar');
        } else {
            $atimSubMenuForHeader = array();
            $menuModel = AppModel::getInstance("", "Menu", true);
            
            $mainMenuItems = $menuModel->find('all', array(
                'conditions' => array(
                    'Menu.parent_id' => 'MAIN_MENU_1'
                )
            ));
            foreach ($mainMenuItems as $item) {
                $atimSubMenuForHeader[$item['Menu']['id']] = $menuModel->find('all', array(
                    'conditions' => array(
                        'Menu.parent_id' => $item['Menu']['id'],
                        'Menu.is_root' => 1
                    ),
                    'order' => array(
                        'Menu.display_order'
                    )
                ));
            }
            
            $this->set('atimMenuForHeader', $this->Menus->get('/menus/tools'));
            $this->set('atimSubMenuForHeader', $atimSubMenuForHeader);
            
            // menu, passed to Layout where it would be rendered through a Helper
            $this->set('atimMenuVariables', array());
            $this->set('atimMenu', $this->Menus->get());
        }
        // get default STRUCTRES, used for forms, views, and validation
        $this->Structures->set();
        
        $this->checkIfDownloadFile();
        
        // Fixe for issue #3396: Msg "You are not authorized to access that location." is not translated
        $this->Auth->authError = __('You are not authorized to access that location.');
    }

    public function checkUserLogRecordNumber()
    {
        $logPath = Configure::read('atim_user_log_output_path');
        if (!empty($logPath)) {
            $userLogModel = AppModel::getInstance('', "UserLog", false);
            $currentYear = date('Y');
            $userLogsNumber = $userLogModel->find('count', array(
                'conditions' => array(
                    'YEAR(visited) < ' => $currentYear - 2
                )
            ));
            if ($userLogsNumber>0){
                $this->addWarningMsg(__('the user logs table overflow warning message'));
            }
        }
    }

    private function checkIfDownloadFile()
    {
        if (isset($this->request->query['file']) && $this->Auth->isAuthorized()) {
            $plugin = $this->request->params['plugin'];
            $modelName = Inflector::camelize(Inflector::singularize($this->request->params['controller']));
            $fileName = $this->request->query['file'];
            $shortFileName = $this->request->query['filename'];
            if (! $this->Session->read('flag_show_confidential')) {
                preg_match('/(' . $modelName . ')\.(.+)\.([0-9]+)\.(.+)/', $fileName, $matches, PREG_OFFSET_CAPTURE);
                if (! empty($matches)) {
                    if ($matches[1][0] == $modelName) {
                        $model = AppModel::getInstance($plugin, $modelName, true);
                        $fields = $model->schema();
                        if (isset($fields[$matches[2][0]])) {
                            $this->atimFlashError(__('You are not authorized to access that location.'), '/Menus');
                        }
                    }
                }
            }
            $file = Configure::read('uploadDirectory') . DS . $fileName;
            if (file_exists($file)) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($shortFileName) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                readfile($file);
            } else {
                $this->atimFlashError(__('file does not exist'), '');
            }
        }
    }

    /**
     * AppController constructor.
     *
     * @param null $request
     * @param null $response
     */
    public function __construct($request = null, $response = null)
    {
        $className = get_class($this);
        $this->name = substr($className, 0, strlen(get_class($this)) - (strpos($className, 'ControllerCustom') === false ? 10 : 16));
        parent::__construct($request, $response);
    }

    /**
     *
     * @param string $hookExtension
     * @return bool|string
     */
    public function hook($hookExtension = '')
    {
        if ($hookExtension) {
            $hookExtension = '_' . $hookExtension;
        }
        
        $hookFile = APP . ($this->request->params['plugin'] ? 'Plugin' . DS . $this->request->params['plugin'] . DS : '') . 'Controller' . DS . 'Hook' . DS . $this->request->params['controller'] . '_' . $this->request->params['action'] . $hookExtension . '.php';
        $hook = $hookFile;
        if (! file_exists($hookFile)) {
            $hookFile = false;
        }
        $_SESSION['hooks'][] = array($hook, !empty($hookFile));
        
        return $hookFile;
    }

    /**
     *
     * @return CakeResponse|null
     */
    private function handleFileRequest()
    {
        $file = $this->request->query['file'];
        
        $redirectInvalidFile = function ($caseType) use(&$file) {
            CakeLog::error("User tried to download invalid file (" . $caseType . "): " . $file);
            if ($caseType === 3) {
                AppController::getInstance()->redirect("/Pages/err_file_not_auth?p[]=" . $file);
            } else {
                AppController::getInstance()->redirect("/Pages/err_file_not_found?p[]=" . $file);
            }
        };
        
        $index = - 1;
        foreach (range(0, 1) as $_) {
            $index = strpos($file, '.', $index + 1);
        }
        $prefix = substr($file, 0, $index);
        if ($prefix && array_key_exists($prefix, $this->allowedFilePrefixes)) {
            $dir = Configure::read('uploadDirectory');
            // NOTE: Cannot use flash for errors because file is still in the
            // url and that would cause an infinite loop
            if (strpos($file, '/') > - 1 || strpos($file, '\\') > - 1) {
                $redirectInvalidFile(1);
            }
            $fullFile = $dir . '/' . $file;
            if (! file_exists($fullFile)) {
                $redirectInvalidFile(2);
            }
            $index = strpos($file, '.', $index + 1) + 1;
            $this->response->file($fullFile, array(
                'name' => substr($file, $index)
            ));
            return $this->response;
        }
        $redirectInvalidFile(3);
    }

    /**
     * Called after controller action logic, but before the view is rendered.
     * This callback is not used often, but may be needed if you are calling
     * render() manually before the end of a given action.
     */
    public function beforeRender()
    {
        if (isset($this->request->query['file'])) {
            return $this->handleFileRequest();
        }
        // Fix an issue where cakephp 2.0 puts the first loaded model with the key model in the registry.
        // Causes issues on validation messages
        ClassRegistry::removeObject('model');
        
        if (isset($this->passedArgs['batchsetVar'])) {
            // batchset handling
            $data = null;
            if (isset($this->viewVars[$this->passedArgs['batchsetVar']])) {
                $data = $this->viewVars[$this->passedArgs['batchsetVar']];
            }
            if (empty($data)) {
                unset($this->passedArgs['batchsetVar']);
                $this->atimFlashWarning(__('there is no data to add to a temporary batchset'), 'javascript:history.back()');
                return false;
            }
            if (isset($this->passedArgs['batchsetCtrl'])) {
                $data = $data[$this->passedArgs['batchsetCtrl']];
            }
            $this->requestAction('/Datamart/BatchSets/add/0', array(
                '_data' => $data
            ));
        }
        
        if ($this->layout == 'ajax') {
            $_SESSION['query']['previous'][] = $this->getQueryLogs('default');
        }
    }

    public function afterFilter()
    {
        // global $startTime;
        // echo("Exec time (sec): ".(AppController::microtimeFloat() - $startTime));
        if (sizeof(AppController::$missingTranslations) > 0) {
            App::uses('MissingTranslation', 'Model');
            $mt = new MissingTranslation();
            foreach (AppController::$missingTranslations as $missingTranslation) {
                $missingTranslation = substr($missingTranslation, 0, 765);
                $mt->set(array(
                    "MissingTranslation" => array(
                        "id" => substr($missingTranslation, 0, 254)
                    )
                ));
                $mt->save(); // ignore errors, kind of insert ignore
            }
        }
    }

    /**
     * Simple function to replicate PHP 5 behaviour
     */
    public static function microtimeFloat()
    {
        list ($usec, $sec) = explode(" ", microtime());
        return ((float) $usec + (float) $sec);
    }

    /**
     *
     * @param $word
     */
    public static function missingTranslation(&$word)
    {
        $source = isset(AppController::getStackTrace()[3])?AppController::getStackTrace()[3]:'Cake';
        $source = (strpos($source, ROOT.DS."lib".DS."Cake".DS) !==0)?'ATiM':'Cake';

        $addUntranslated = true;
        if ($source == 'ATiM'){
            $i18nModel = new Model(array(
                'table' => 'i18n',
                'name' => 'GeneratedI18nForMissingTranslation'
            ));
            $translation = $i18nModel->find("first", array("conditions" => array('id' => $word)));
            if (!empty($translation)){
                $lang = Configure::read('Config.language') == 'eng' ? "en" : "fr";
                if (!empty($translation['GeneratedI18nForMissingTranslation'][$lang])){
                    $word = $translation['GeneratedI18nForMissingTranslation'][$lang];
                    $addUntranslated = false;
                }
            }
        }
        if ($addUntranslated && ! is_numeric($word) && strpos($word, "<span class='untranslated'>") === false) {
            AppController::$missingTranslations[] = $word;
            if (Configure::read('debug') == 2 && self::$highlightMissingTranslations) {
                $word = "<span class='untranslated'>" . $word . "</span>";
            }
        }
    }

    /**
     *
     * @param array|string $url
     * @param null $status
     * @param bool $exit
     * @return \Cake\Network\Response|null|void
     */
    public function redirect($url, $status = null, $exit = true)
    {
        $_SESSION['query']['previous'][] = $this->getQueryLogs('default');
        parent::redirect($url, $status, $exit);
    }

    /**
     *
     * @param $message
     * @param $url
     * @param int $type
     */
    public function atimFlash($message, $url, $type = self::CONFIRM)
    {
        if (empty($url)) {
            $url = "/Menus";
        }
        if (strpos(strtolower($url), 'javascript') !== false) {
            $url = $this->getBackHistoryUrl();
        }
        if (strpos(strtolower($url), 'javascript') === false) {
            if ($type == self::CONFIRM) {
                if (!empty($message)){
                    $_SESSION['ctrapp_core']['confirm_msg'] = $message;
                }
                $title = ___('confirm');
                $icon = 'img/icons/small/confirm.png';
            } elseif ($type == self::INFORMATION) {
                if (!empty($message)){
                    $_SESSION['ctrapp_core']['info_msg'][] = $message;
                }
                $title = ___('information');
                $icon = 'img/icons/small/information.png';
            } elseif ($type == self::WARNING) {
                if (!empty($message)){
                    $_SESSION['ctrapp_core']['warning_trace_msg'][] = $message;
                }
                $title = ___('warning');
                $icon = 'img/icons/small/warning.png';
            } elseif ($type == self::ERROR) {
                if (!empty($message)){
                    $_SESSION['ctrapp_core']['error_msg'][] = $message;
                }
                $title = ___('error');
                $icon = 'img/icons/small/delete.png';
            }
            $_SESSION['notification'] = array(
                'type' => $type,
                'message' => $message,
                'title' =>$title,
                'icon' =>$icon
            );

            $this->redirect($url);
        } elseif (false) {
            // TODO:: Check if can use javascript functions for blue screen message
            echo '<script type="text/javascript">', 'javascript:history.back();', 'window.location.reload();', '</script>';
        } else {
            $this->autoRender = false;
            $this->set('url', Router::url($url));
            $this->set('message', $message);
            $this->set('pageTitle', $message);
            $this->render(false, "flash");
        }
    }

    /**
     *
     * @param $message
     * @param $url
     */
    public function atimFlashError($message, $url)
    {
        $this->atimFlash($message, $url, self::ERROR);
    }

    /**
     *
     * @param $message
     * @param $url
     */
    public function atimFlashInfo($message, $url)
    {
        $this->atimFlash($message, $url, self::INFORMATION);
    }

    /**
     *
     * @param $message
     * @param $url
     */
    public function atimFlashConfirm($message, $url)
    {
        $this->atimFlash($message, $url, self::CONFIRM);
    }

    /**
     *
     * @param $message
     * @param $url
     */
    public function atimFlashWarning($message, $url)
    {
        $this->atimFlash($message, $url, self::WARNING);
    }

    /**
     *
     * @return null
     */
    public static function getInstance()
    {
        return AppController::$me;
    }

    public static function init()
    {
        Configure::write('Config.language', 'eng');
        Configure::write('Acl.classname', 'AtimAcl');
        Configure::write('Acl.database', 'default');
        
        // ATiM2 configuration variables from Datatable
        
        define('VALID_INTEGER', '/^[-+]?[\\s]?[0-9]+[\\s]?$/');
        define('VALID_INTEGER_POSITIVE', '/^[+]?[\\s]?[0-9]+[\\s]?$/');
        define('VALID_FLOAT', '/^[-+]?[\\s]?[0-9]{0,%s}[,\\.]?[0-9]+[\\s]?$/');
        define('VALID_FLOAT_POSITIVE', '/^[+]?[\\s]?[0-9]{0,%s}[,\\.]?[0-9]+[\\s]?$/');
        define('VALID_24TIME', '/^([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/');
        
        // ripped from validation.php date + time
        define('VALID_DATETIME_YMD', '%^(?:(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00)))(-)(?:0?2\\1(?:29)))|(?:(?:(?:1[6-9]|2\\d)\\d{2})(-)(?:(?:(?:0?[13578]|1[02])\\2(?:31))|(?:(?:0?(1|[3-9])|1[0-2])\\2(29|30))|(?:(?:0?[1-9])|(?:1[0-2]))\\2(?:0?[1-9]|1\\d|2[0-8])))\s([0-1][0-9]|2[0-3])\:[0-5][0-9]\:[0-5][0-9]$%');
        
        // parse URI manually to get passed PARAMS
        global $startTime;
        $startTime = AppController::microtimeFloat();
        
        $requestUriParams = array();
        
        // Fix REQUEST_URI on IIS
        if (! isset($_SERVER['REQUEST_URI'])) {
            $_SERVER['REQUEST_URI'] = substr($_SERVER['PHP_SELF'], 1);
            if (isset($_SERVER['QUERY_STRING'])) {
                $_SERVER['REQUEST_URI'] .= '?' . $_SERVER['QUERY_STRING'];
            }
        }
        $requestUri = $_SERVER['REQUEST_URI'];
        $requestUri = explode('/', $requestUri);
        $requestUri = array_filter($requestUri);
        
        foreach ($requestUri as $uri) {
            $explodedUri = explode(':', $uri);
            if (countCustom($explodedUri) > 1) {
                $requestUriParams[$explodedUri[0]] = $explodedUri[1];
            }
        }
        
        // import APP code required...
        App::import('Model', 'Config');
        $configDataModel = new Config();
        
        // get CONFIG data from table and SET
        $configResults = false;
        
        App::uses('CakeSession', 'Model/Datasource');
        $userId = CakeSession::read('Auth.User.id');
        $loggedInUser = CakeSession::read('Auth.User.id');
        $loggedInGroup = CakeSession::read('Auth.User.group_id');
        $configResults = $configDataModel->getConfig(CakeSession::read('Auth.User.group_id'), CakeSession::read('Auth.User.id'));
        // parse result, set configs/defines
        if ($configResults) {
            
            Configure::write('Config.language', $configResults['Config']['config_language']);
            foreach ($configResults['Config'] as $configKey => $configData) {
                if (strpos($configKey, '_') !== false) {
                    
                    // break apart CONFIG key
                    $configKey = explode('_', $configKey);
                    $configFormat = array_shift($configKey);
                    $configKey = implode('_', $configKey);
                    
                    // if a DEFINE or CONFIG, set new setting for APP
                    if ($configFormat == 'define') {
                        
                        // override DATATABLE value with URI PARAM value
                        if ($configKey == 'PAGINATION_AMOUNT' && isset($requestUriParams['per'])) {
                            $configData = $requestUriParams['per'];
                        }
                        
                        define(strtoupper($configKey), $configData);
                    } elseif ($configFormat == 'config') {
                        Configure::write($configKey, $configData);
                    }
                }
            }
        }
        
        define('CONFIDENTIAL_MARKER', __('confidential data')); // Moved here to allow translation
        
        if (Configure::read('debug') == 0) {
            set_error_handler("myErrorHandler");
        }
    }

    /**
     *
     * @param boolean $short Wheter to return short or long month names
     * @return array associative array containing the translated months names so that key = month_number and value = month_name
     */
    public static function getCalInfo($short = true)
    {
        if ($short) {
            if (! AppController::$calInfoShortTranslated) {
                AppController::$calInfoShortTranslated = true;
                AppController::$calInfoShort = array_map(function ($a){return __($a);}, AppController::$calInfoShort);
            }
            return AppController::$calInfoShort;
        } else {
            if (! AppController::$calInfoLongTranslated) {
                AppController::$calInfoLongTranslated = true;
                AppController::$calInfoLong = array_map(function ($a){return __($a);}, AppController::$calInfoLong);
            }
            return AppController::$calInfoLong;
        }
    }

    /**
     *
     * @param int $year
     * @param mixed int | string $month
     * @param int $day
     * @param boolean $nbspSpaces True if white spaces must be printed as &nbsp;
     * @param boolean $shortMonths True if months names should be short (used if $month is an int)
     * @return string The formated datestring with user preferences
     */
    public static function getFormatedDateString($year, $month, $day, $nbspSpaces = true, $shortMonths = true)
    {
        $result = null;
        if (empty($year) && empty($month) && empty($day)) {
            $result = "";
        } else {
            $divider = $nbspSpaces ? "&nbsp;" : " ";
            if (is_numeric($month)) {
                $monthStr = AppController::getCalInfo($shortMonths);
                $month = $month > 0 && $month < 13 ? $monthStr[(int) $month] : "-";
            }
            if (DATE_FORMAT == 'MDY') {
                $result = $month . (empty($month) ? "" : $divider) . $day . (empty($day) ? "" : $divider) . $year;
            } elseif (DATE_FORMAT == 'YMD') {
                $result = $year . (empty($month) ? "" : $divider) . $month . (empty($day) ? "" : $divider) . $day;
            } else { // default of DATE_FORMAT=='DMY'
                $result = $day . (empty($day) ? "" : $divider) . $month . (empty($month) ? "" : $divider) . $year;
            }
        }
        return $result;
    }

    /**
     *
     * @param $hour
     * @param $minutes
     * @param bool $nbspSpaces
     * @return string
     */
    public static function getFormatedTimeString($hour, $minutes, $nbspSpaces = true)
    {
        if (TIME_FORMAT == 12) {
            $meridiem = $hour >= 12 ? "PM" : "AM";
            $hour %= 12;
            if ($hour == 0) {
                $hour = 12;
            }
            return $hour . (empty($minutes) ? '' : ":" . $minutes . ($nbspSpaces ? "&nbsp;" : " ")) . $meridiem;
        } elseif (empty($minutes)) {
            return $hour . __('hour_sign');
        } else {
            return $hour . ":" . $minutes;
        }
    }

    /**
     *
     * Enter description here ...
     *
     * @param String $datetimeString with format yyyy[-MM[-dd[ hh[:mm:ss]]]] (missing parts represent the accuracy
     * @param boolean $nbspSpaces True if white spaces must be printed as &nbsp;
     * @param boolean $shortMonths True if months names should be short (used if $month is an int)
     * @return string The formated datestring with user preferences
     */
    public static function getFormatedDatetimeString($datetimeString, $nbspSpaces = true, $shortMonths = true)
    {
        $month = null;
        $day = null;
        $hour = null;
        $minutes = null;
        if (strpos($datetimeString, ' ') === false) {
            $date = $datetimeString;
        } else {
            list ($date, $time) = explode(" ", $datetimeString);
            if (strpos($time, ":") === false) {
                $hour = $time;
            } else {
                list ($hour, $minutes) = explode(":", $time);
            }
        }
        
        $date = explode("-", $date);
        $year = $date[0];
        switch (countCustom($date)) {
            case 3:
                $day = $date[2];
            case 2:
                $month = $date[1];
                break;
        }
        $formatedDate = self::getFormatedDateString($year, $month, $day, $nbspSpaces);
        return $hour === null ? $formatedDate : $formatedDate . ($nbspSpaces ? "&nbsp;" : " ") . self::getFormatedTimeString($hour, $minutes, $nbspSpaces);
    }

    /**
     * Return formatted date in SQL format from a date array returned by an application form.
     *
     * @param Array $datetimeArray gathering date data into following structure:
     *        array('month' => string, '
     *        'day' => string,
     *        'year' => string,
     *        'hour' => string,
     *        'min' => string)
     * @param Specify|string $dateType Specify
     *        the type of date ('normal', 'start', 'end')
     *        - normal => Will force function to build a date witout specific rules.
     *        - start => Will force function to build date as a 'start date' of date range defintion
     *        (ex1: when just year is specified, will return a value like year-01-01 00:00)
     *        (ex2: when array is empty, will return a value like -9999-99-99 00:00)
     *        - end => Will force function to build date as an 'end date' of date range defintion
     *        (ex1: when just year is specified, will return a value like year-12-31 23:59)
     *        (ex2: when array is empty, will return a value like 9999-99-99 23:59)
     * @return string The formated SQL date having following format yyyy-MM-dd hh:mn
     */
    public static function getFormatedDatetimeSQL($datetimeArray, $dateType = 'normal')
    {
        $formattedDate = '';
        switch ($dateType) {
            case 'normal':
                if ((! empty($datetimeArray['year'])) && (! empty($datetimeArray['month'])) && (! empty($datetimeArray['day']))) {
                    $formattedDate = $datetimeArray['year'] . '-' . $datetimeArray['month'] . '-' . $datetimeArray['day'];
                }
                if ((! empty($formattedDate)) && (! empty($datetimeArray['hour']))) {
                    $formattedDate .= ' ' . $datetimeArray['hour'] . ':' . (empty($datetimeArray['min']) ? '00' : $datetimeArray['min']);
                }
                break;
            
            case 'start':
                if (empty($datetimeArray['year'])) {
                    $formattedDate = '-9999-99-99 00:00';
                } else {
                    $formattedDate = $datetimeArray['year'];
                    if (empty($datetimeArray['month'])) {
                        $formattedDate .= '-01-01 00:00';
                    } else {
                        $formattedDate .= '-' . $datetimeArray['month'];
                        if (empty($datetimeArray['day'])) {
                            $formattedDate .= '-01 00:00';
                        } else {
                            $formattedDate .= '-' . $datetimeArray['day'];
                            if (empty($datetimeArray['hour'])) {
                                $formattedDate .= ' 00:00';
                            } else {
                                $formattedDate .= ' ' . $datetimeArray['hour'] . ':' . (empty($datetimeArray['min']) ? '00' : $datetimeArray['min']);
                            }
                        }
                    }
                }
                break;
            
            case 'end':
                if (empty($datetimeArray['year'])) {
                    $formattedDate = '9999-12-31 23:59';
                } else {
                    $formattedDate = $datetimeArray['year'];
                    if (empty($datetimeArray['month'])) {
                        $formattedDate .= '-12-31 23:59';
                    } else {
                        $formattedDate .= '-' . $datetimeArray['month'];
                        if (empty($datetimeArray['day'])) {
                            $formattedDate .= '-31 23:59';
                        } else {
                            $formattedDate .= '-' . $datetimeArray['day'];
                            if (empty($datetimeArray['hour'])) {
                                $formattedDate .= ' 23:59';
                            } else {
                                $formattedDate .= ' ' . $datetimeArray['hour'] . ':' . (empty($datetimeArray['min']) ? '59' : $datetimeArray['min']);
                            }
                        }
                    }
                }
                break;
            
            default:
        }
        
        return $formattedDate;
    }

    /**
     * Clones the first level of an array
     *
     * @param array $arr The array to clone
     * @return array
     */
    public static function cloneArray(array $arr)
    {
        $result = array();
        foreach ($arr as $k => $v) {
            if (is_array($v)) {
                $result[$k] = self::cloneArray($v);
            } else {
                $result[$k] = $v;
            }
        }
        return $result;
    }

    /**
     *
     * @param $msg
     * @param bool $withTrace
     */
    public static function addWarningMsg($msg, $withTrace = false)
    {
        if ($withTrace) {
            $_SESSION['ctrapp_core']['warning_trace_msg'][] = array(
                'msg' => $msg,
                'trace' => self::getStackTrace()
            );
        } else {
            if (isset($_SESSION['ctrapp_core']['warning_no_trace_msg'][$msg])) {
                $_SESSION['ctrapp_core']['warning_no_trace_msg'][$msg] ++;
            } else {
                $_SESSION['ctrapp_core']['warning_no_trace_msg'][$msg] = 1;
            }
        }
    }

    /**
     *
     * @param $msg
     */
    public static function addInfoMsg($msg)
    {
        if (isset($_SESSION['ctrapp_core']['info_msg'][$msg])) {
            $_SESSION['ctrapp_core']['info_msg'][$msg] ++;
        } else {
            $_SESSION['ctrapp_core']['info_msg'][$msg] = 1;
        }
    }
    
    /**
     *
     * @param $msg
     */
    public static function addErrorMsg($msg)
    {
        if (!is_array($msg)){
            $msg = [$msg];
        }
        foreach ($msg as $m){
            $_SESSION['ctrapp_core']['error_msg'][] = $m;
        }
    }
    /**
     *
     * @param $msg
     */
    public static function addConfirmMsg($msg)
    {
        $_SESSION['ctrapp_core']['confirm_msg'] = $msg;
    }

    /**
     */
    public static function forceMsgDisplayInPopup()
    {
        $_SESSION['ctrapp_core']['force_msg_display_in_popup'] = true;
    }

    /**
     *
     * @return array
     */
    public static function getStackTrace()
    {
        $bt = debug_backtrace();
        $result = array();
        foreach ($bt as $unit) {
            $result[] = (isset($unit['file']) ? $unit['file'] : '?') . ", " . $unit['function'] . " at line " . (isset($unit['line']) ? $unit['line'] : '?');
        }
        return $result;
    }

    /**
     * Builds the value definition array for an updateAll call
     *
     * @param array $data
     * @return array
     * @internal param $ array They data array to build the values with* array They data array to build the values with
     */
    public static function getUpdateAllValues(array $data)
    {
        $result = array();
        foreach ($data as $model => $fields) {
            foreach ($fields as $name => $value) {
                if (is_array($value)) {
                    if (strlen($value['year'])) {
                        $result[$model . "." . $name] = "'" . AppController::getFormatedDatetimeSQL($value) . "'";
                    }
                } elseif (strlen($value)) {
                    $result[$model . "." . $name] = "'" . $value . "'";
                }
            }
        }
        return $result;
    }

    /**
     * Encrypt a text
     *
     * @param String $string
     * @return String
     */
    public static function encrypt($string)
    {
        return Security::hash($string, null, true);
    }

    /**
     * cookie manipulation to counter cake problems.
     * see eventum #1032
     *
     * @param $skipExpirationCookie
     */
    public static function atimSetCookie($skipExpirationCookie)
    {
        if (! $skipExpirationCookie) {
            $sessionExpiration = time() + Configure::read("Session.timeout");
            setcookie('last_request', time(), $sessionExpiration, '/');
            setcookie('session_expiration', $sessionExpiration, $sessionExpiration, '/');
            if (isset($_COOKIE[Configure::read("Session.cookie")])) {
                setcookie(Configure::read("Session.cookie"), $_COOKIE[Configure::read("Session.cookie")], $sessionExpiration, "/");
            }
            $sessionId = (! empty($_SESSION['Auth']['User']['id'])) ? self::encrypt($_SESSION['Auth']['User']['id']) : self::encrypt("nul string");
            setcookie('sessionId', $sessionId, 0, "/");
        }
    }

    /**
     * Global function to initialize a batch action.
     * Redirect/flashes on error.
     *
     * @param AppModel $model The model to work on
     * @param string $dataModelName The model name used in $this->request->data
     * @param string $dataKey The data key name used in $this->request->data
     * @param string $controlKeyName The name of the control field used in the model table
     * @param AppModel $possibilitiesModel The model to fetch the possibilities from
     * @param string $possibilitiesParentKey The possibilities parent key to base the search on
     * @param $noPossibilitiesMsg
     * @return array array with the ids and the possibilities
     */
    public function batchInit($model, $dataModelName, $dataKey, $controlKeyName, $possibilitiesModel, $possibilitiesParentKey, $noPossibilitiesMsg)
    {
        if (empty($this->request->data)) {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        } elseif (! is_array($this->request->data[$dataModelName][$dataKey]) && strpos($this->request->data[$dataModelName][$dataKey], ',')) {
            return array(
                'error' => "batch init - number of submitted records too big"
            );
        }
        // extract valid ids
        $ids = $model->find('all', array(
            'conditions' => array(
                $model->name . '.id' => $this->request->data[$dataModelName][$dataKey]
            ),
            'fields' => array(
                $model->name . '.id'
            ),
            'recursive' => - 1
        ));
        if (empty($ids)) {
            return array(
                'error' => "batch init no data"
            );
        }
        $model->sortForDisplay($ids, $this->request->data[$dataModelName][$dataKey]);
        $ids = self::defineArrayKey($ids, $model->name, 'id');
        $ids = array_keys($ids);
        
        $controls = $model->find('all', array(
            'conditions' => array(
                $model->name . '.id' => $ids
            ),
            'fields' => array(
                $model->name . '.' . $controlKeyName
            ),
            'group' => array(
                $model->name . '.' . $controlKeyName
            ),
            'recursive' => - 1
        ));
        if (countCustom($controls) != 1) {
            return array(
                'error' => "you must select elements with a common type"
            );
        }
        
        $possibilities = $possibilitiesModel->find('all', array(
            'conditions' => array(
                $possibilitiesParentKey => $controls[0][$model->name][$controlKeyName],
                $possibilitiesModel->name . '.flag_active' => '1'
            )
        ));
        
        if (empty($possibilities)) {
            return array(
                'error' => $noPossibilitiesMsg
            );
        }
        
        return array(
            'ids' => implode(',', $ids),
            'possibilities' => $possibilities,
            'control_id' => $controls[0][$model->name][$controlKeyName]
        );
    }

    /**
     * Replaces the array key (generally of a find) with an inner value
     *
     * @param array $inArray
     * @param string $model The model ($inArray[$model])
     * @param string $field The field (new key = $inArray[$model][$field])
     * @param bool $unique If true, the array block will be directly under the model.field, not in an array.
     * @return array
     */
    public static function defineArrayKey($inArray, $model, $field, $unique = false)
    {
        $outArray = array();
        if ($unique) {
            foreach ($inArray as $val) {
                $outArray[$val[$model][$field]] = $val;
            }
        } else {
            foreach ($inArray as $val) {
                if (isset($val[$model])) {
                    $outArray[$val[$model][$field]][] = $val;
                } else {
                    // the key cannot be foud
                    $outArray[- 1][] = $val;
                }
            }
        }
        return $outArray;
    }

    /**
     * Recursively removes entries returning true on empty($value).
     *
     * @param array $data
     * @internal param $ array &$data* array &$data
     */
    public static function removeEmptyValues(array &$data)
    {
        foreach ($data as $key => &$val) {
            if (is_array($val)) {
                self::removeEmptyValues($val);
            }
            if (empty($val)) {
                unset($data[$key]);
            }
        }
    }

    /**
     *
     * @return mixed
     */
    public static function getNewSearchId()
    {
        return AppController::getInstance()->Session->write('search_id', AppController::getInstance()->Session->read('search_id') + 1);
    }

    /**
     *
     * @param string $link The link to check
     * @return True if the user can access that page, false otherwise
     */
    public static function checkLinkPermission($link)
    {
        if (strpos($link, 'javascript:') === 0 || strpos($link, '#') === 0) {
            return true;
        }
        
        $parts = Router::parse($link);
        if (empty($parts)) {
            return false;
        }
        $acoAlias = 'Controller/' . ($parts['plugin'] ? Inflector::camelize($parts['plugin']) . '/' : '');
        $acoAlias .= ($parts['controller'] ? Inflector::camelize($parts['controller']) . '/' : '');
        $acoAlias .= ($parts['action'] ? $parts['action'] : '');
        $instance = AppController::getInstance();
        $acoAlias = $instance->PermissionManager->getSubstitutedUrl($acoAlias);
        return strpos($acoAlias, 'Controller/Users') !== false || strpos($acoAlias, 'Controller/Pages') !== false || $acoAlias == "Controller/Menus/index" || $instance->SessionAcl->check('Group::' . $instance->Session->read('Auth.User.group_id'), $acoAlias);
    }

    /**
     *
     * @param $inArray
     * @param $model
     * @param $field
     */
    public static function applyTranslation(&$inArray, $model, $field)
    {
        foreach ($inArray as &$part) {
            $part[$model][$field] = __($part[$model][$field]);
        }
    }

    /**
     * Handles automatic pagination of model records Adding
     * the necessary bind on the model to fetch detail level, if there is a unique ctrl id
     *
     * @param Model|string $object Model to paginate (e.g: model instance, or 'Model', or 'Model.InnerModel')
     * @param string|array $scope Conditions to use while paginating
     * @param array $whitelist List of allowed options for paging
     * @return array Model query results
     */
    public function paginate($object = null, $scope = array(), $whitelist = array())
    {
        $this->setControlerPaginatorSettings($object);
        $modelName = isset($object->baseModel) ? $object->baseModel : $object->name;
        if (isset($object->Behaviors->MasterDetail->__settings[$modelName])) {
            extract(self::convertArrayKeyFromSnakeToCamel($object->Behaviors->MasterDetail->__settings[$modelName]));
            if ($isMasterModel && isset($scope[$modelName . '.' . $controlForeign]) && preg_match('/^[0-9]+$/', $scope[$modelName . '.' . $controlForeign])) {
                self::buildDetailBinding($object, array(
                    $modelName . '.' . $controlForeign => $scope[$modelName . '.' . $controlForeign]
                ), $emptyStructureAlias);
            }
        }
        return parent::paginate($object, $scope, $whitelist);
    }

    /**
     * Finds and paginate search results.
     * Stores search in cache.
     * Handles detail level when there is a unique ctrl_id.
     * Defines/updates the result structure.
     * Sets 'result_are_unique_ctrl' as true if the results are based on a unique ctrl id,
     * false otherwise. (Non master/detail models will return false)
     *
     * @param int $searchId The search id used by the pagination
     * @param Object $model The model to search upon
     * @param string $structureAlias The structure alias to parse the search conditions on
     * @param string $url The base url to use in the pagination links (meaning without the search_id)
     * @param bool $ignoreDetail If true, even if the model is a master_detail ,the detail level won't be loaded
     * @param mixed $limit If false, will make a paginate call, if an int greater than 0, will make a find with the limit
     */
    public function searchHandler($searchId, $model, $structureAlias, $url, $ignoreDetail = false, $limit = false)
    {
        // setting structure
        $structure = $this->Structures->get('form', $structureAlias);
        $this->set('atimStructure', $structure);
        if (empty($searchId)) {
            $plugin = $this->request->params['plugin'];
            $controller = $this->request->params['controller'];
            $action = $this->request->params['action'];
            if (isset($_SESSION['post_data'][$plugin][$controller][$action])) {
                convertArrayToJavaScript($_SESSION['post_data'][$plugin][$controller][$action], 'jsPostData');
            }
            
            $this->Structures->set('empty', 'emptyStructure');
        } else {
            if ($this->request->data) {
                
                // newly submitted search, parse conditions and store in session
                $_SESSION['ctrapp_core']['search'][$searchId]['criteria'] = $this->Structures->parseSearchConditions($structure);
            } elseif (! isset($_SESSION['ctrapp_core']['search'][$searchId]['criteria'])) {
                self::addWarningMsg(__('you cannot resume a search that was made in a previous session'));
                $this->redirect('/Menus');
                exit();
            }
            
            // check if the current model is a master/detail one or a similar view
            if (! $ignoreDetail) {
                self::buildDetailBinding($model, $_SESSION['ctrapp_core']['search'][$searchId]['criteria'], $structureAlias);
                $this->Structures->set($structureAlias);
            }
            
            if ($limit) {
                $this->request->data = $model->find('all', array(
                    'conditions' => $_SESSION['ctrapp_core']['search'][$searchId]['criteria'],
                    'limit' => $limit
                ));
            } else {
                $this->setControlerPaginatorSettings($model);
                $this->request->data = $this->Paginator->paginate($model, $_SESSION['ctrapp_core']['search'][$searchId]['criteria']);
            }
            
            // if SEARCH form data, save number of RESULTS and URL (used by the form builder pagination links)
            if ($searchId == - 1) {
                // don't use the last search button if search id = -1
                unset($_SESSION['ctrapp_core']['search'][$searchId]);
            } else {
                $_SESSION['ctrapp_core']['search'][$searchId]['results'] = empty($this->request->params['paging'][$model->name]['count']) ? 0 : $this->request->params['paging'][$model->name]['count'];
                $_SESSION['ctrapp_core']['search'][$searchId]['url'] = $url;
            }
        }
        
        if ($this->request->is('ajax')) {
            Configure::write('debug', 0);
            $this->set('isAjax', true);
        }
    }

    /**
     * Set the Pagination settings based on user preferences and controller Pagination settings.
     *
     * @param Object $model The model to search upon
     */
    public function setControlerPaginatorSettings($model)
    {
        if (PAGINATION_AMOUNT) {
            $this->Paginator->settings = array_merge($this->Paginator->settings, array(
                'limit' => PAGINATION_AMOUNT
            ));
        }
        if ($model && isset($this->paginate[$model->name])) {
            $this->Paginator->settings = array_merge($this->Paginator->settings, $this->paginate[$model->name]);
        }
    }

    /**
     * Adds the necessary bind on the model to fetch detail level, if there is a unique ctrl id
     *
     * @param AppModel &$model
     * @param array $conditions Search conditions
     * @param string &$structureAlias
     */
    public static function buildDetailBinding(&$model, array $conditions, &$structureAlias)
    {
        $controller = AppController::getInstance();
        $masterClassName = isset($model->baseModel) ? $model->baseModel : $model->name;
        if (! isset($model->Behaviors->MasterDetail->__settings[$masterClassName])) {
            $controller->$masterClassName; // try to force lazyload
            if (! isset($model->Behaviors->MasterDetail->__settings[$masterClassName])) {
                if (Configure::read('debug') != 0) {
                    AppController::addWarningMsg("buildDetailBinding requires you to force instanciation of model " . $masterClassName);
                }
                return;
            }
        }
        if ($model->Behaviors->MasterDetail->__settings[$masterClassName]['is_master_model']) {
            $ctrlIds = null;
            $singleCtrlId = $model->getSingleControlIdCondition(array(
                'conditions' => $conditions
            ));
            $controlField = $model->Behaviors->MasterDetail->__settings[$masterClassName]['control_foreign'];
            if ($singleCtrlId === false) {
                // determine if the results contain only one control id
                $ctrlIds = $model->find('all', array(
                    'fields' => array(
                        $model->name . '.' . $controlField
                    ),
                    'conditions' => $conditions,
                    'group' => array(
                        $model->name . '.' . $controlField
                    ),
                    'limit' => 2
                ));
                if (countCustom($ctrlIds) == 1) {
                    $singleCtrlId = current(current($ctrlIds[0]));
                }
            }
            if ($singleCtrlId !== false) {
                // only one ctrl, attach detail
                $hasOne = array();
                extract(self::convertArrayKeyFromSnakeToCamel($model->Behaviors->MasterDetail->__settings[$masterClassName]));
                $ctrlModel = isset($controller->$controlClass) ? $controller->$controlClass : AppModel::getInstance('', $controlClass, false);
                if (! $ctrlModel) {
                    if (Configure::read('debug') != 0) {
                        AppController::addWarningMsg('buildDetailBinding requires you to force instanciation of model ' . $controlClass);
                    }
                    return;
                }
                $ctrlData = $ctrlModel->findById($singleCtrlId);
                $ctrlData = current($ctrlData);
                // put a new instance of the detail model in the cache
                ClassRegistry::removeObject($detailClass); // flush the old detail from cache, we'll need to reinstance it
                assert(strlen($ctrlData['detail_tablename'])) or die("detail_tablename cannot be empty");
                new AppModel(array(
                    'table' => $ctrlData['detail_tablename'],
                    'name' => $detailClass,
                    'alias' => $detailClass
                ));
                
                // has one and win
                $hasOne[$detailClass] = array(
                    'className' => $detailClass,
                    'foreignKey' => $masterForeign
                );
                
                if ($masterClassName == 'SampleMaster') {
                    // join specimen/derivative details
                    if ($ctrlData['sample_category'] == 'specimen') {
                        $hasOne['SpecimenDetail'] = array(
                            'className' => 'SpecimenDetail',
                            'foreignKey' => 'sample_master_id'
                        );
                    } else {
                        // derivative
                        $hasOne['DerivativeDetail'] = array(
                            'className' => 'DerivativeDetail',
                            'foreignKey' => 'sample_master_id'
                        );
                    }
                }
                
                // persistent bind
                $model->bindModel(array(
                    'hasOne' => $hasOne,
                    'belongsTo' => array(
                        $controlClass => array(
                            'className' => $controlClass
                        )
                    )
                ), false);
                isset($model->{$detailClass}); // triggers model lazy loading (See cakephp Model class)
                                               
                // updating structure
                if (($pos = strpos($ctrlData['form_alias'], ',')) !== false) {
                    $structureAlias = $structureAlias . ',' . substr($ctrlData['form_alias'], $pos + 1);
                }
                
                ClassRegistry::removeObject($detailClass); // flush the new model to make sure the default one is loaded if needed
            } elseif (countCustom($ctrlIds) > 0) {
                // more than one
                AppController::addInfoMsg(__("the results contain various data types, so the details are not displayed"));
            }
        }
    }

    /**
     * Builds menu options based on 1-display_order and 2-translation
     *
     * @param array $menuOptions An array containing arrays of the form array('order' => #, 'label' => '', 'link' => '')
     *        The label must be translated already.
     */
    public static function buildBottomMenuOptions(array &$menuOptions)
    {
        $tmp = array();
        foreach ($menuOptions as $menuOption) {
            $tmp[sprintf("%05d", $menuOption['order']) . '-' . $menuOption['label']] = $menuOption['link'];
        }
        ksort($tmp);
        $menuOptions = array();
        foreach ($tmp as $label => $link) {
            $menuOptions[preg_replace('/^[0-9]+-/', '', $label)] = $link;
        }
    }

    /**
     * Sets url_to_cancel based on $this->request->data['url_to_cancel']
     * If nothing exists, javascript:history.go(-1) is used.
     * If a similar entry exists, the value is decremented.
     * Otherwise, url_to_cancel is uses as such.
     * 
     * @deprecated : Use getCancelLink() to get cancel url and manage urlToCancel into your controller function. See ParticipantMessage.add() as an example.
     */
    public function setUrlToCancel()
    {
        if (isset($this->request->data['url_to_cancel'])) {
            $pattern = '/^javascript:history.go\((-?[0-9]*)\)$/';
            $matches = array();
            if (preg_match($pattern, $this->request->data['url_to_cancel'], $matches)) {
                $back = empty($matches[1]) ? - 2 : $matches[1] - 1;
                $this->request->data['url_to_cancel'] = 'javascript:history.go(' . $back . ')';
            }
        } else {
            $this->request->data['url_to_cancel'] = 'javascript:history.go(-1)';
        }
        
        $this->set('urlToCancel', $this->request->data['url_to_cancel']);
    }

    public function resetPermissions()
    {
        if ($this->Auth->user()) {
            $userModel = AppModel::getInstance('', 'User', true);
            $user = $userModel->findById($this->Session->read('Auth.User.id'));
            $this->Session->write('Auth.User.group_id', $user['User']['group_id']);
            $this->Session->write('flag_show_confidential', $user['Group']['flag_show_confidential']);
            $this->Session->write('permission_timestamp', time());
            $this->SessionAcl->flushCache();
        }
    }

    /**
     *
     * @param array $list
     * @param $lModel
     * @param $lKey
     * @param array $data
     * @param $dModel
     * @param $dKey
     * @return bool
     */
    public function setForRadiolist(array &$list, $lModel, $lKey, array $data, $dModel, $dKey)
    {
        foreach ($list as &$unit) {
            if ($data[$dModel][$dKey] == $unit[$lModel][$lKey]) {
                // we found the one that interests us
                $unit[$dModel] = $data[$dModel];
                return true;
            }
        }
        return false;
    }

    /**
     * Builds a cancel link based on the passed data.
     * Works for data send by batch sets and browsing.
     *
     * @param string or null $data
     * @return null|string
     */
    public static function getCancelLink($data)
    {
        $result = null;
        if (isset($data['node']['id'])) {
            $result = '/Datamart/Browser/browse/' . $data['node']['id'];
        } elseif (isset($data['BatchSet']['id'])) {
            $result = '/Datamart/BatchSets/listAll/' . $data['BatchSet']['id'];
        } elseif (isset($data['cancel_link'])) {
            $result = $data['cancel_link'];
        } elseif (isset($data['url_to_cancel'])) {
            $result = $data['url_to_cancel'];
        } elseif (!empty($_SESSION['batch_action_url_to_cancel'])) {
            $result = $_SESSION['batch_action_url_to_cancel'];
        }
        return $result;
    }

    /**
     * Does multiple tasks related to having a version updated
     * -Permissions
     * -i18n version field
     * -language files
     * -cache
     * -Delete all browserIndex > Limit
     * -databrowser lft rght
     */
    public function newVersionSetup()
    {
        $this->Setup->newVersion();
    }

    /**
     *
     * @param $config
     */
    public function configureCsv($config)
    {
        $this->csvConfig = $config;
        $this->Session->write('Config.language', $config['config_language']);
    }

    /**
     *
     * @param $connection
     * @param array $options
     * @return string
     */
    public function getQueryLogs($connection, $options = array())
    {
        $db = ConnectionManager::getDataSource($connection);
        $log = $db->getLog();
        if ($log['count'] == 0) {
            $out = "";
        } else {
            $out = 'Total Time: ' . $log['time'] . '<br>Total Queries: ' . $log['count'] . '<br>';
            $out .= '<table class="debug-table">' . '<thead>' . '<tr>' . '<th>Query</th>' . '<th>Affected</th>' . '<th>Num. rows</th>' . '<th>Took (ms)</th>' . 
            // '<th>Actions</th>'.
            '</tr>';
            $class = 'odd';
            foreach ($log['log'] as $i => $value) {
                $out .= '<tr class="' . $class . '">' . '<td>' . $value['query'] . '</td>' . '<td>' . $value['affected'] . '</td>' . '<td>' . $value['numRows'] . '</td>' . '<td>' . $value['took'] . '</td>' . '</tr>';
                $class = ($class == 'even' ? 'odd' : 'even');
            }
            $out .= "</thead>" . "</table>";
        }
        return $out;
    }

    /**
     *
     * @param null $array
     * @return array
     */
    public static function convertArrayKeyFromSnakeToCamel($array = null)
    {
        $answer = array();
        if ($array) {
            foreach ($array as $key => $value) {
                $answer[Inflector::variable($key)] = $value;
            }
        }
        return $answer;
    }

    public function redirectPost($url, array $data = [], array $options = [])
    {
        $data = normalizeCompressedDataForSend($data);
        $serializedData = serializeArray($data, ['data']);

        $this->set('compressedData', json_encode($serializedData));
        $url = str_replace("//", "/", $this->request->webroot . $url);
        $this->set('url', $url);
        $this->layout = 'redirect_page';
        $this->render(false, 'redirect_page');
    }
}

AppController::init();

/**
 * Returns the date in a classic format (useful for SQL)
 *
 * @throws Exception
 */
function now()
{
    return date("Y-m-d H:i:s");
}

/**
 *
 * @param $errno
 * @param $errstr
 * @param $errfile
 * @param $errline
 * @param null $context
 * @return bool
 */
function myErrorHandler($errno, $errstr, $errfile, $errline, $context = null)
{
    if (class_exists("AppController")) {
        $controller = AppController::getInstance();
        if ($errno == E_USER_WARNING && strpos($errstr, "SQL Error:") !== false && $controller->name != 'Pages') {
            $traceMsg = "<table><tr><th>File</th><th>Line</th><th>Function</th></tr>";
            try {
                throw new Exception("");
            } catch (Exception $e) {
                $traceArr = $e->getTrace();
                foreach ($traceArr as $traceLine) {
                    if (is_array($traceLine)) {
                        $traceMsg .= "<tr><td>" . (isset($traceLine['file']) ? $traceLine['file'] : "") . "</td><td>" . (isset($traceLine['line']) ? $traceLine['line'] : "") . "</td><td>" . $traceLine['function'] . "</td></tr>";
                    } else {
                        $traceMsg .= "<tr><td></td><td></td><td></td></tr>";
                    }
                }
            }
            $traceMsg .= "</table>";
            $controller->redirect('/Pages/err_query?err_msg=' . urlencode($errstr . $traceMsg));
        }
    }
    return false;
}