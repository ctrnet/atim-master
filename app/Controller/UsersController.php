<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

require_once APP . 'Vendor' . DS . 'autoload.php';

use PragmaRX\Google2FAQRCode\Google2FA;

/**
 * Class UsersController
 */
class UsersController extends AppController
{

    public $uses = array(
        'User',
        'UserLoginAttempt',
        'Version',
        'Setup',
    );

    /**
     * Before Filter Callback
     *
     * @return void
     */
    public function beforeFilter()
    {
        parent::beforeFilter();
        if (Configure::read('reset_forgotten_password_feature')) {
            $this->Auth->allow('login', 'logout', 'resetForgottenPassword', 'checkTfa', 'getTFAData', 'regenerateSecretKey', 'changeTheSuperAdminStatus');
        } else {
            $this->Auth->allow('login', 'logout');
        }
        if ($this->request->is('ajax')) {
            $this->Auth->allow('getUserId');
        }
        $this->Auth->authenticate = array(
            'Form' => array(
                'userModel' => 'User',
                'scope' => array(
                    'User.flag_active'
                )
            )
        );
        $this->set('atimStructure', $this->Structures->get('form', 'login'));
    }

    /**
     *
     * @return bool
     */
    private function doLogin($params = '')
    {
        $isLdap = Configure::read("if_use_ldap_authentication");
        $twoFA = Configure::read('2fa_microsoft_google_auth');

        $response = false;

        if (empty($isLdap)) {
            $response = $this->Auth->login();
        } elseif ($isLdap === true) {
            if (!isset($this->request->data['User']['username']) && !isset($this->request->data['User']['password']) && $this->Auth->login()) {
                $response = true;
            } elseif (isset($this->request->data['User']['username'])) {

                $username = (isset($this->request->data['User']['username'])) ? $this->request->data['User']['username'] : null;
                $password = (isset($this->request->data['User']['password'])) ? $this->request->data['User']['password'] : null;

                $conditions = array(
                    'User.username' => $username,
                    'User.deleted' => '0',
                    'User.flag_active' => 1
                );
                $user = $this->User->find('first', array(
                    'conditions' => $conditions
                ));
                $forcePasswordReset = (!empty($user)) ? $user['User']['force_password_reset'] : null;
                if (empty($forcePasswordReset)) {
                    $adServer = Configure::read('ldap_server');
                    $ldaprdn = Configure::read('ldap_domain');

                    $ldaprdn = sprintf($ldaprdn, $username);

                    $ldap = ldap_connect($adServer);
                    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
                    ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

                    try {
                        $bind = @ldap_bind($ldap, $ldaprdn, $password);
                    } catch (Exception $ex) {
                        $bind = null;
                    }

                    if (!empty($bind)) {
                        $conditions = array(
                            'User.username' => $username,
                            'User.deleted' => '0',
                            'User.flag_active' => 1
                        );
                        $user = $this->User->find('all', array(
                            'conditions' => $conditions
                        ));
                        if (empty($user)) {
                            $response = false;
                        } elseif (countCustom($user) === 1) {
                            unset($user[0]['User']['password']);
                            $tempUser = $user[0]['User'];
                            $tempUser['Group'] = $user[0]['Group'];
                            $this->Auth->login($tempUser);
                            $response = true;
                        } else {
                            $response = false;
                        }
                    }
                } else {
                    $response = $this->Auth->login();
                }
            } else {
                $response = false;
            }
        }

        $maintenanceModeAndNotDevUser = Configure::read("maintenance_mode") && isset($_SESSION['Auth']['User']['dev_user']) && empty($_SESSION['Auth']['User']['dev_user']);
        if ($response && $maintenanceModeAndNotDevUser){
            return $response;
        }elseif ($response && $twoFA && $params != 'popup') {
            if (!empty($this->request->data['User']['username'])){
                $this->UserLoginAttempt->saveFailedLogin($this->request->data['User']['username']);
            }
            $this->redirect("/Users/checkTfa");
        } elseif ($response && $twoFA && $params != 'popup-other-user') {
            return $params;
        } else {
            return $response;
        }
    }

    public function checkTfa()
    {
        if (empty($_SESSION['Auth']) || empty(Configure::read('2fa_microsoft_google_auth'))) {
            $this->redirect("/Users/login");
        }
        $salt = Configure::read('atim_instance_for_2fa_microsoft_google_auth');
        $isDeveloper = !empty($_SESSION['Auth']['User']['dev_user']);
        $forceTFAShow = !empty($_SESSION['Auth']['User']['force_tfa_show']);

        if (!empty($_SESSION['Auth'])) {
            $_SESSION['Auth']['User']['force_tfa'] = 1;
        }

        if ($forceTFAShow) {
            $this->Structures->set('user_tfa', 'atimStructure');
        } else {
            $this->Structures->set('user_tfa_login', 'atimStructure');
        }

        $name = [];
        if (!empty($_SESSION['Auth']['User']['first_name'])) {
            $name [] = $_SESSION['Auth']['User']['first_name'];
        }
        if (!empty($_SESSION['Auth']['User']['last_name'])) {
            $name [] = $_SESSION['Auth']['User']['last_name'];
        }
        $name = implode(" ", $name);
        $data = [
            'FunctionManagement' => [
                'name' => !empty($name) ? $name : "-",
//                'secret_key' => $_SESSION['Auth']['User']['secret_key'],
            ]
        ];
        if (empty($this->request->data)) {
            $this->request->data = $data;
        } else {
            if (!$forceTFAShow) {
                $this->request->data['FunctionManagement']['agreement'] = '1';
            }
            $submittedDataValidates = $this->User->validateTFA($this->request->data);

            if ($submittedDataValidates) {
                $google2fa = new Google2FA();
                $submittedDataValidates = $google2fa->verifyKey(OpenSSL::decrypt($_SESSION['Auth']['User']['secret_key']), $this->request->data['FunctionManagement']['validation_code']);
                if (!$submittedDataValidates) {
                    $this->request->data['FunctionManagement']['validation_code'] = "";
                    $this->User->validationErrors['validation_code'][] = "the validation code is invalid";
                }

                $hookLink = $this->hook('post_validation');
                if ($hookLink) {
                    require($hookLink);
                }

                if ($submittedDataValidates) {
                    $_SESSION['Auth']['User']['force_tfa_show'] = 0;
                    $_SESSION['Auth']['User']['force_tfa'] = 0;
                    $this->User->updateAll(['User.force_tfa_show' => '0'], ['User.id' => $_SESSION['Auth']['User']['id']]);
                    $this->redirect('/Menus');
                }
            }
        }
    }

    public function getTFAData($type = '', $userId = 0)
    {
        if (!empty($_SESSION['Auth']) && (!empty($_SESSION['Auth']['User']['force_tfa_show']) || !empty($_SESSION['Auth']['User']['is_super_admin']))) {

            if (empty($userId)) {
                $userId = $_SESSION['Auth']['User']['id'];
            }
            $data = $this->User->findById($userId);
            $google2fa = new Google2FA();

            $salt = Configure::read('atim_instance_for_2fa_microsoft_google_auth');

            $name = [];
            if (!empty($data['User']['first_name'])) {
                $name [] = $data['User']['first_name'];
            }
            if (!empty($data['User']['last_name'])) {
                $name [] = $data['User']['last_name'];
            }
            $name = implode(" ", $name);

            $qrCodeUrl = $google2fa->getQRCodeUrl(
                $salt,
                $name,
                OpenSSL::decrypt($data['User']['secret_key'])
            );

            $inlineUrl = $google2fa->getQRCodeInline(
                $salt,
                $name,
                OpenSSL::decrypt($data['User']['secret_key'])
            );
            if ($type == 'qrcode') {
                echo $inlineUrl;
            } elseif ($type == 'secretKey') {
                echo $qrCodeUrl;
            }
        } else {
            $this->atimFlashError(__("You are not authorized to access that location."), '/Users/login');
        }
        die();
    }

    public function regenerateSecretKey($id)
    {
        $response = "nok";

        if (!empty($_SESSION['Auth']) && !empty($_SESSION['Auth']['User']['is_super_admin']) && $this->request->is('ajax')) {
            $userData = $this->User->findById($id);
            if (!empty($userData)) {
                $google2fa = new Google2FA();
                $secretKey = $google2fa->generateSecretKey();
                $userData['User']['secret_key'] = OpenSSL::encrypt($secretKey);
                $userData['User']['force_tfa_show'] = 1;
                $this->User->addWritableField(['secret_key', 'force_tfa_show',]);
                $this->User->id = $id;
                if ($this->User->save($userData, true)){
                    $response = 'ok';
                }
            }
        }
        echo $response;
        die();
    }

    public function changeTheSuperAdminStatus($id, $status)
    {
        $canModifyTheStatus = false;
        if (!empty($_SESSION['Auth']) && !empty($_SESSION['Auth']['User']['is_super_admin']) && in_array($status, ['inactive', 'active']) && !empty(Configure::read('num_of_super_admin')) && $this->request->is('ajax')) {
            $canModifyTheStatus = true;
            $userData = $this->User->findById($id);
            if (empty($userData)) {
                $canModifyTheStatus = false;
                AppController::addErrorMsg(__('user not found'));
            } elseif ($userData['User']['group_id'] != 1) {
                $canModifyTheStatus = false;
                AppController::addErrorMsg(__('the user can not be a tfa super admin'));
            } elseif ($userData['User']['id'] == $_SESSION['Auth']['User']['id']) {
                $canModifyTheStatus = false;
                AppController::addErrorMsg(__('the user can not change himself his tfa super admin role'));
            } elseif (! $userData['User']['flag_active']) {
                $canModifyTheStatus = false;
                AppController::addErrorMsg(__('the tfa super admin role can not be changed for an inactive user'));
            } else {
                $numberOfSuperUsers = $this->User->find('count', [
                    'conditions' => [
                        'User.is_super_admin' => 1,
                        'User.flag_active' => 1,
                    ],
                ]);

                $minNumberOfSuperAdmin = Configure::read('num_of_super_admin')[0];
                $maxNumberOfSuperAdmin = Configure::read('num_of_super_admin')[1];

                if ($minNumberOfSuperAdmin == $numberOfSuperUsers && $status == 'active') {
                    $canModifyTheStatus = false;
                    AppController::addErrorMsg(__('the number of tfa super admins have reached the minimum limit'));
                } elseif ($maxNumberOfSuperAdmin == $numberOfSuperUsers && $status == 'inactive') {
                    $canModifyTheStatus = false;
                    AppController::addErrorMsg(__('the number of tfa super admins have reached the maximum limit'));
                }
            }

            if ($canModifyTheStatus) {
                $userData['User']['is_super_admin'] = $status == 'active' ? 0 : 1;
                $this->User->addWritableField(['is_super_admin']);
                $this->User->id = $id;
                $this->User->save($userData, false);
                AppController::addConfirmMsg($status == 'active' ? __('the user is not tfa super admin anymore') : __('the user is tfa super admin'));
            }
        }else{
            AppController::addErrorMsg(__('You are not authorized to access that location.'));
        }
        die();
    }

    /**
     * Login Method
     *
     * @return \Cake\Network\Response|null
     */
    public function login()
    {
        $username = $this->UserLoginAttempt->find('first', array(
            'order' => 'attempt_time DESC'
        ));
        $username = (isset($username["UserLoginAttempt"]["username"]) ? $username["UserLoginAttempt"]["username"] : null);
        if (! empty($_SESSION['Auth']['User']) && ! isset($this->passedArgs['login'])) {
            return $this->redirect('/Menus');
        }

        if ($this->request->is('ajax') && ! isset($this->passedArgs['login'])) {
            echo json_encode(array(
                'logged_in' => isset($_SESSION['Auth']['User']),
                'server_time' => time()
            ));
            exit();
        }

        // Load version data and check if initialization is required
        $versionData = $this->Version->find('first', array(
            'fields' => array(
                'MAX(id) AS id'
            )
        ));
        $this->Version->id = $versionData[0]['id'];
        $this->Version->read();

        if ($this->Version->data['Version']['permissions_regenerated'] == 0) {
            $this->newVersionSetup();
        }else{
            $this->Setup->reIndexingTheTables(false);
        }

        $this->set('skipExpirationCookie', true);
        if ($this->User->shouldLoginFromIpBeDisabledAfterFailedAttempts()) {
            // Too many login attempts - froze atim for couple of minutes
            $this->request->data = array();
            $this->Auth->flash(__('too many failed login attempts - connection to atim disabled temporarily for %s mn', Configure::read('time_mn_IP_disabled')));
        } elseif ((! isset($this->passedArgs['login'])) && $this->doLogin()) {
            // Log in user
            if ($this->request->data['User']['username']) {
                $this->UserLoginAttempt->saveSuccessfulLogin($this->request->data['User']['username']);
            }
            $this->_initializeNotificationSessionVariables();

            $this->checkUserLogRecordNumber();

            $this->_setSessionSearchId();
            $this->resetPermissions();

            // Authentication credentials expiration
            if ($this->User->isPasswordResetRequired()) {
                $this->Session->write('Auth.User.force_password_reset', '1');
                return $this->redirect('/Customize/Passwords/index');
            }

            return $this->redirect('/Menus');
        } elseif (isset($this->request->data['User']['username']) && ! isset($this->passedArgs['login'])) {
            // Save failed login attempt
            $this->UserLoginAttempt->saveFailedLogin($this->request->data['User']['username']);
            if ($this->User->disableUserAfterTooManyFailedAttempts($this->request->data['User']['username'])) {
                AppController::addWarningMsg(__('your username has been disabled - contact your administartor'));
            }
            $this->request->data = array();
            $ldap = Configure::read("if_use_ldap_authentication");
            if (empty($ldap)) {
                $this->Auth->flash(__('login failed - invalid username or password or disabled user'));
            } else {
                $this->Auth->flash(__('login failed - invalid username or password or disabled user or LDAP server connection error'));
            }
        } elseif (isset($this->request->data['User']['username']) && isset($this->passedArgs['login']) && $username === $this->request->data['User']['username']) {
            if ($this->doLogin('popup')) {
                // Log in user
                if ($this->request->data['User']['username']) {
                    $this->UserLoginAttempt->saveSuccessfulLogin($this->request->data['User']['username']);
                }
                $this->_initializeNotificationSessionVariables();

                $this->_setSessionSearchId();
                $this->resetPermissions();
                return $this->render('ok');
            }
        } elseif (isset($this->request->data['User']['username']) && isset($this->passedArgs['login']) && $username !== $this->request->data['User']['username']) {
            if ($this->doLogin('popup-other-user')) {
                // Log in user
                if ($this->request->data['User']['username']) {
                    $this->UserLoginAttempt->saveSuccessfulLogin($this->request->data['User']['username']);
                }
                $this->_initializeNotificationSessionVariables();

                $this->_setSessionSearchId();
                $this->resetPermissions();

                ob_clean();
                die('popup-other-user');
            }
        }

        // User got returned to the login page, tell him why
        if (isset($_SESSION['Message']['auth']['message'])) {
            $this->User->validationErrors[] = __($_SESSION['Message']['auth']['message']) . ($_SESSION['Message']['auth']['message'] == "You are not authorized to access that location." ? __("if you were logged id, your session expired.") : '');
            unset($_SESSION['Message']['auth']);
        }

        if (isset($this->passedArgs['login'])) {
            AppController::addInfoMsg(__('your session has expired'));
        }

        $this->User->showErrorIfInternetExplorerIsBelowVersion(8);
    }

    /**
     * Set Session Search Id
     *
     * @return void
     */
    public function _setSessionSearchId()
    {
        if (! $this->Session->read('search_id')) {
            $this->Session->write('search_id', 1);
            $this->Session->write('ctrapp_core.search', array());
        }
    }

    /**
     * Logs a user out
     *
     * @return void
     */
    public function logout()
    {
        $this->Acl->flushCache();
        $this->Session->destroy();
        $this->redirect($this->Auth->logout());
    }

    /**
     * initializeNotificationSessionVariables
     *
     * @return void
     */
    public function _initializeNotificationSessionVariables()
    {
        // Init Session variables
        $this->Session->write('ctrapp_core.warning_no_trace_msg', array());
        $this->Session->write('ctrapp_core.warning_trace_msg', array());
        $this->Session->write('ctrapp_core.info_msg', array());
        $this->Session->write('ctrapp_core.force_msg_display_in_popup', false);
    }

    /**
     * Reset a forgotten password of a user based on personnal questions.
     *
     * @return void
     */
    public function resetForgottenPassword()
    {
        if (! Configure::read('reset_forgotten_password_feature')) {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        if ($this->Session->read('Auth.User.id')) {
            $this->redirect('/');
        }

        $ipTemporarilyDisabled = $this->User->shouldLoginFromIpBeDisabledAfterFailedAttempts();

        if (empty($this->request->data) || $ipTemporarilyDisabled) {
            // 1- Initial access to the function:
            // Display of the form to set the username.

            $this->Structures->set('username');
            if ($ipTemporarilyDisabled) {
                $this->User->validationErrors[][] = __('too many failed login attempts - connection to atim disabled temporarily for %s mn', Configure::read('time_mn_IP_disabled'));
            }

            $this->set('resetForgottenPasswordStep', '1');
        } else {
            // Check username exists in the database and is not disabled
            if (! isset($this->request->data['User']['username'])) {
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            }

            $resetFormFields = $this->User->getForgottenPasswordResetFormFields();
            $resetFormQuestionFields = array_keys($resetFormFields);
            $dbUserData = $this->User->find('first', array(
                'conditions' => array(
                    'User.username' => $this->request->data['User']['username'],
                    'User.flag_active' => '1'
                )
            ));

            foreach ($resetFormFields as $questionFieldName => $answerFieldName) {
                if (empty($dbUserData['User'][$questionFieldName])) {
                    $this->atimFlashWarning(__('User has not been yet answered to the reset questions.'), "/Users/resetForgottenPassword/");
                }
            }

            if (! $dbUserData) {

                // 2- User name does not exist in the database or is disabled
                // Display or re-display the form to set the username

                $this->User->validationErrors['username'][] = __('invalid username or disabled user');
                $this->Structures->set('username');
                $this->set('resetForgottenPasswordStep', '1');
                $this->UserLoginAttempt->saveFailedLogin($this->request->data['User']['username']);
                $this->request->data = array();
            } elseif (! array_key_exists(array_shift($resetFormQuestionFields), $this->request->data['User'])) {

                // 3- User name does exist in the database
                // Display the form to set the username confidential questions

                $this->Structures->set('forgotten_password_reset' . ((Configure::read('reset_forgotten_password_feature') != '1') ? ',other_user_login_to_forgotten_password_reset' : ''));
                $this->set('resetForgottenPasswordStep', '2');
                $this->request->data = array(
                    'User' => array(
                        'username' => $this->request->data['User']['username']
                    )
                );
                foreach ($resetFormFields as $questionFieldName => $answerFieldName) {
                    $this->request->data['User'][$questionFieldName] = $dbUserData['User'][$questionFieldName];
                }
            } else {

                // 4- User name does exist in the database and answers have been completed by the user
                // Check the answers set by the user to reset the password

                $this->Structures->set('forgotten_password_reset' . ((Configure::read('reset_forgotten_password_feature') != '1') ? ',other_user_login_to_forgotten_password_reset' : ''));
                $this->set('resetForgottenPasswordStep', '2');

                $submittedDataValidates = true;

                // Validate user questions answers
                foreach ($resetFormFields as $questionFieldName => $answerFieldName) {
                    // Check db/form questions matche

                    if ($dbUserData['User'][$questionFieldName] != $this->request->data['User'][$questionFieldName]) {
                        $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                    }
                    // Check answers
                    if (! strlen($dbUserData['User'][$answerFieldName])) {
                        // No answer in db. Validation can not be done
                        $this->User->validationErrors['username']['#1'] = __('at least one error exists in the questions you answered - password can not be reset');
                        $submittedDataValidates = false;
                    }
                    if ($dbUserData['User'][$answerFieldName] !== $this->User->hashSecuritAnswer($this->request->data['User'][$answerFieldName])) {
                        $this->User->validationErrors['username']['#1'] = __('at least one error exists in the questions you answered - password can not be reset');
                        $submittedDataValidates = false;
                    }
                }

                // Validate other user login

                if (Configure::read('reset_forgotten_password_feature') != '1') {
                    if (! isset($this->request->data['0']['other_user_check_username'])) {
                        $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                    }

                    $conditions = array(
                        'User.username' => $this->request->data['0']['other_user_check_username'],
                        'User.flag_active' => '1',
                        'User.password' => Security::hash($this->request->data['0']['other_user_check_password'], null, true)
                    );
                    if (! $this->User->find('count', array(
                        'conditions' => $conditions
                    ))) {
                        $this->User->validationErrors['other_user_check_username'][] = __('other user control') . ' : ' . __('invalid username or disabled user');
                        $submittedDataValidates = false;
                    }
                }

                if ($submittedDataValidates) {
                    // Save new password
                    $this->User->id = $dbUserData['User']['id'];
                    $suerDataToSave = array(
                        'User' => array(
                            'id' => $dbUserData['User']['id'],
                            'group_id' => $dbUserData['User']['group_id'],
                            'new_password' => $this->request->data['User']['new_password'],
                            'confirm_password' => $this->request->data['User']['confirm_password']
                        )
                    );
                    if ($this->User->savePassword($suerDataToSave, true)) {
                        $this->atimFlash(__('your data has been updated'), '/');
                    }
                } else {
                    // Save failed login attempt
                    $this->UserLoginAttempt->saveFailedLogin($this->request->data['User']['username']);
                    if ($this->User->disableUserAfterTooManyFailedAttempts($this->request->data['User']['username'])) {
                        AppController::addWarningMsg(__('your username has been disabled - contact your administartor'));
                    }
                }

                // Flush login information
                $this->request->data['0']['other_user_check_username'] = '';
                $this->request->data['0']['other_user_check_password'] = '';
                $this->request->data['User']['new_password'] = '';
                $this->request->data['User']['confirm_password'] = '';
            }
        }
    }

    /**
     * Reset user Id and esncrypte it just in AJAX mode to front-end.
     */
    public function getUserId()
    {
        if ($this->request->is('ajax')) {
            ob_clean();
            die((! empty($_SESSION['Auth']['User']['id'])) ? AppController::encrypt($_SESSION['Auth']['User']['id']) : AppController::encrypt("nul string"));
        }
    }
}