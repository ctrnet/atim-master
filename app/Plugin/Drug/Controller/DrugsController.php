<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class DrugsController
 */
class DrugsController extends DrugAppController
{

    public $uses = array(
        'Drug.Drug'
    );

    public $components = [
        'Drug.RxNorm',
    ];

    public $paginate = array(
        'Drug' => array(
            'order' => 'Drug.generic_name ASC'
        )
    );

    /**
     *
     * @param int $searchId
     */
    public function search($searchId = 0)
    {
        $hookLink = $this->hook('pre_search_handler');
        if ($hookLink) {
            require ($hookLink);
        }

        $this->searchHandler($searchId, $this->Drug, 'drugs', '/Drug/Drugs/search');


        // CUSTOM CODE: FORMAT DISPLAY DATA
        $hookLink = $this->hook('format');
        if ($hookLink) {
            require ($hookLink);
        }

        if (empty($searchId)) {
            // index
            $this->render('index');
        }
    }

    public function synchronize()
    {
        if (Configure::read('drug_method_name') == 'rxnorm') {
            $drugs = $this->Drug->find('all', [
                'conditions' => [
                    'OR' => [
                        'rxnorm_id IS NULL',
                        'rxnorm_id = ""',
                    ],
                ],
            ]);

            $sucessfull = $unsuccessfull = 0;
            foreach ($drugs as $drug) {
                $rxNorm = $this->RxNorm->getRxNumber(!empty($drug['Drug']['generic_name']) ? $drug['Drug']['generic_name'] : '');
                if (is_numeric($rxNorm)) {
                    $drug['Drug']['rxnorm_id'] = $rxNorm;
                    $drug['Drug']['rx_number'] = $rxNorm;
                    $this->Drug->id = $drug['Drug']['id'];
                    if ($this->Drug->save($drug)) {
                        $sucessfull++;
                    } else {
                        $unsuccessfull++;
                    }
                } else {
                    $unsuccessfull++;
                }
            }
            $this->atimFlashConfirm(__('%s drugs information updated and can not update %s drugs', $sucessfull, $unsuccessfull), '/Drug/Drugs/search');
        }
        $this->atimFlashError(__('You are not authorized to access that location.'), '/Drug/Drugs/search/');

    }

    public function add()
    {
        $drugMethodName = Configure::read("drug_method_name");

        $this->set('atimMenu', $this->Menus->get('/Drug/Drugs/search/'));

        // CUSTOM CODE: FORMAT DISPLAY DATA
        $hookLink = $this->hook('format');
        if ($hookLink) {
            require ($hookLink);
        }

        if (empty($this->request->data)) {
            $this->request->data = array(
                array()
            );

            $hookLink = $this->hook('initial_display');
            if ($hookLink) {
                require ($hookLink);
            }
        } else {

            $errorsTracking = array();

            // Validation

            // Check submitted duplicat values when isUnique validation is attached to the field
            foreach ($this->Drug->checkUniqueGridFields($this->request->data) as $isUniqueField => $isUniqueMessages) {
                foreach ($isUniqueMessages as $isUniqueMsg) {
                    $errorsTracking[$isUniqueField][$isUniqueMsg][''] = '';
                }
            }
            
            $rowCounter = 0;
            foreach ($this->request->data as &$dataUnit) {
                $rowCounter ++;
                $this->Drug->id = null;
                if ($drugMethodName == "rxnorm"){
                    $response = $this->RxNorm->getRxNumber(!empty($dataUnit['Drug']['generic_name']) ? $dataUnit['Drug']['generic_name'] : "");
                    if (!empty($response['error'])){
                        $errorsTracking['rxnorm_id'][__('error loading rx norm id')][] = $rowCounter;
                    }else{
                        $rxNumber = $response;
                    }
                    $dataUnit['Drug']['rx_number'] = !empty($rxNumber) && is_numeric($rxNumber) ? $rxNumber : "";
                }
                $this->Drug->set($dataUnit);
                if (! $this->Drug->validates()) {
                    foreach ($this->Drug->validationErrors as $field => $msgs) {
                        $msgs = is_array($msgs) ? $msgs : array(
                            $msgs
                        );
                        foreach ($msgs as $msg)
                            $errorsTracking[$field][$msg][] = $rowCounter;
                    }
                }
                $dataUnit = $this->Drug->data;
            }
            unset($dataUnit);

            $hookLink = $this->hook('presave_process');
            if ($hookLink) {
                require ($hookLink);
            }

            // Launch Save Process

            if (empty($this->request->data)) {
                $this->Drug->validationErrors[][] = 'at least one record has to be created';
            } elseif (empty($errorsTracking)) {
                AppModel::acquireBatchViewsUpdateLock();
                // save all
                foreach ($this->request->data as $newDataToSave) {
                    $this->Drug->id = null;
                    $this->Drug->data = array();
                    if (! $this->Drug->save($newDataToSave, false)){
                        $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                    }
                }
                $hookLink = $this->hook('postsave_process_batch');
                if ($hookLink) {
                    require ($hookLink);
                }
                AppModel::releaseBatchViewsUpdateLock();
                $this->atimFlash(__('your data has been updated'), '/Drug/Drugs/search/');
            } else {
                $this->Drug->validationErrors = array();
                foreach ($errorsTracking as $field => $msgAndLines) {
                    foreach ($msgAndLines as $msg => $lines) {
                        $this->Drug->validationErrors[$field][] = $msg . ' - ' . str_replace('%s', implode(",", $lines), __('see line %s'));
                    }
                }
            }
        }
    }

    /**
     *
     * @param $drugId
     */
    public function edit($drugId)
    {
        $drugMethodName = Configure::read("drug_method_name");

        $drugData = $this->Drug->getOrRedirect($drugId);

        $this->set('atimMenuVariables', array(
            'Drug.id' => $drugId
        ));

        $hookLink = $this->hook('format');
        if ($hookLink) {
            require ($hookLink);
        }

        if (empty($this->request->data)) {
            $this->request->data = $drugData;
        } else {
            $submittedDataValidates = true;

            $hookLink = $this->hook('presave_process');
            if ($hookLink) {
                require ($hookLink);
            }

            if ($submittedDataValidates) {
                $this->Drug->id = $drugId;
                if ($drugMethodName == "rxnorm"){
                    $response = $this->RxNorm->getRxNumber(!empty($this->request->data['Drug']['generic_name']) ? $this->request->data['Drug']['generic_name'] : "");
                    if (!empty($response['error'])){
                        $this->Drug->validationErrors['rxnorm_id'][] = __('error loading rx norm id');
                        $submittedDataValidates = false;
                    }else{
                        $rxNumber = $response;
                    }
                    $this->request->data['Drug']['rx_number'] = !empty($rxNumber) && is_numeric($rxNumber) ? $rxNumber : "";
                }
                if ($submittedDataValidates && $this->Drug->save($this->request->data, true)) {
                    $hookLink = $this->hook('postsave_process');
                    if ($hookLink) {
                        require ($hookLink);
                    }
                    $this->atimFlash(__('your data has been updated'), '/Drug/Drugs/detail/' . $drugId);
                }
            }
        }
    }

    /**
     *
     * @param $drugId
     */
    public function detail($drugId)
    {
        $this->request->data = $this->Drug->getOrRedirect($drugId);

        $this->set('atimMenuVariables', array(
            'Drug.id' => $drugId
        ));

        $hookLink = $this->hook('format');
        if ($hookLink) {
            require ($hookLink);
        }
    }

    /**
     *
     * @param $drugId
     */
    public function delete($drugId)
    {
        $drugData = $this->Drug->getOrRedirect($drugId);
        $arrAllowDeletion = $this->Drug->allowDeletion($drugId);

        // CUSTOM CODE

        $hookLink = $this->hook('delete');
        if ($hookLink) {
            require ($hookLink);
        }

        if ($arrAllowDeletion['allow_deletion']) {
            $this->Drug->data = null;
            if ($this->Drug->atimDelete($drugId)) {
                $hookLink = $this->hook('postsave_process');
                if ($hookLink) {
                    require ($hookLink);
                }
                $this->atimFlash(__('your data has been deleted'), '/Drug/Drugs/search/');
            } else {
                $this->atimFlashError(__('error deleting data - contact administrator'), '/Drug/Drugs/search/');
            }
        } else {
            $this->atimFlashWarning(__($arrAllowDeletion['msg']), '/Drug/Drugs/detail/' . $drugId);
        }
    }

    public function autoCompleteDrug()
    {

        // -- NOTE ----------------------------------------------------------
        //
        // This function is linked to functions of the Drug model
        // called getDrugIdFromDrugDataAndCode() and
        // getDrugDataAndCodeForDisplay().
        //
        // When you override the autocompleteDrug() function, check
        // if you need to override these functions.
        //
        // ------------------------------------------------------------------

        // layout = ajax to avoid printing layout
        $this->layout = 'ajax';
        // debug = 0 to avoid printing debug queries that would break the javascript array
        Configure::write('debug', 0);

        // query the database
        $term = str_replace(array(
            "\\",
            '%',
            '_'
        ), array(
            "\\\\",
            '\%',
            '\_'
        ), $_GET['term']);
        $terms = array();
        foreach (explode(' ', $term) as $keyWord) {
            $terms[] = array(
                "Drug.generic_name LIKE" => '%' . $keyWord . '%'
            );
        }

        $conditions = array(
            'AND' => $terms
        );
        $fields = 'Drug.*';
        $order = 'Drug.generic_name ASC';
        $joins = array();

        $hookLink = $this->hook('query_args');
        if ($hookLink) {
            require ($hookLink);
        }

        $data = $this->Drug->find('all', array(
            'conditions' => $conditions,
            'fields' => $fields,
            'order' => $order,
            'joins' => $joins,
            'limit' => 10
        ));

        // build javascript textual array
        $result = "";
        foreach ($data as $dataUnit) {
            $result .= '"' . str_replace(array(
                '\\',
                '"'
            ), array(
                '\\\\',
                '\"'
            ), $this->Drug->getDrugDataAndCodeForDisplay($dataUnit)) . '", ';
        }
        if (strlen($result) > 0) {
            $result = substr($result, 0, - 2);
        }

        $hookLink = $this->hook('format');
        if ($hookLink) {
            require ($hookLink);
        }

        $this->set('result', "[" . $result . "]");
    }

    public function autoCompleteRxNormDrug()
    {
        $this->layout = 'ajax';
        Configure::write('debug', 0);

        // query the database
        $term = str_replace(array("\\", '%', '_' ), array("\\\\", '\%', '\_'), $_GET['term']);
        $query = "SELECT * FROM `rx_norm_drugs` WHERE %s ORDER BY `rx_norm_drugs`.`name`";
        $terms1 = [];
        foreach (explode(' ', $term) as $keyWord) {
            $terms1[] = "`rx_norm_drugs`.`name` LIKE '$keyWord'";
        }

        $terms2 = [];
        foreach (explode(' ', $term) as $keyWord) {
            $terms2[] = "`rx_norm_drugs`.`name` LIKE '$keyWord%'";
        }

        $terms3 = [];
        foreach (explode(' ', $term) as $keyWord) {
            $terms3[] = "`rx_norm_drugs`.`name` LIKE '%$keyWord%'";
        }

        $hookLink = $this->hook('query_args');
        if ($hookLink) {
            require ($hookLink);
        }

        $data = $this->Drug->query(sprintf($query, implode(" AND ", $terms1)));
        $data = array_merge($data, $this->Drug->query(sprintf($query, implode(" AND ", $terms2))));
        $data = array_merge($data, $this->Drug->query(sprintf($query, implode(" AND ", $terms3))));
        $result = [];
        $ids = [];
        foreach ($data as $datum) {
            if (!in_array($datum['rx_norm_drugs']['id'], $ids)){
                $result[] = $datum['rx_norm_drugs']['name'] . "|||" . $datum['rx_norm_drugs']['rxnorm_id'];
                $ids[] = $datum['rx_norm_drugs']['id'];
            }
        }
        $result = json_encode($result);

        $hookLink = $this->hook('format');
        if ($hookLink) {
            require ($hookLink);
        }

        $this->set('result', $result);
    }

    public function beforeFilter()
    {
        $drugMethodName = Configure::read("drug_method_name");
        if ($drugMethodName == 'rxnorm' && $this->RxNorm->verifyRxNormAPI()){
            $this->RxNorm->checkRxNormVersion();
        }
        parent::beforeFilter();
    }
}