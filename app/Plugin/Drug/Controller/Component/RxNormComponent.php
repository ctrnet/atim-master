<?php

class RxNormComponent extends Component
{
    private $controller;
    private $ch;

    private static $errors = [
        '0' => 'Error',
        '400' => 'Bad Request',
        '401' => 'Unauthorized',
        '403' => 'Forbidden',
        '404' => 'Not Found',
        '500' => 'Internal Server Error',
        '502' => 'Bad Gateway',
        '503' => 'Service Unavailable',
        '504' => 'Gateway Timeout',
    ];

    public function initialize(Controller $controller)
    {
        parent::initialize($controller);
        $this->controller = $controller;

        $this->ch = curl_init();

        $this->setOptions([
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => 1,
        ]);

    }

    public function verifyRxNormAPI($showMessage = true)
    {
        $response = "";

        $this->setOptions([
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
        ]);

        $output = $this->get('https://rxnav.nlm.nih.gov/REST/version.json');

        if(!empty($output['errors']) && $showMessage){
            AppController::addWarningMsg(__("error in connecting to the rx_norm API"));
        }

        return empty($output['errors']);
    }

    public function checkRxNormVersion()
    {
        $currentVersion = $rxNormVersion = "";
        if (empty($_SESSION['drug'])) {
            $rxNormVersion = $this->getVersion();
            if (!empty($rxNormVersion['version'])) {
                $_SESSION['drug']['rxNormVersion'] = $rxNormVersion;
            }

            $currentVersionTemp = $this->controller->Drug->query("SELECT * FROM `rx_norm_versions` ORDER BY `id` DESC LIMIT 1");
            if (!empty($currentVersionTemp[0]['rx_norm_versions'])) {
                $currentVersion = [
                    'version' => $currentVersionTemp[0]['rx_norm_versions']['version'],
                    'apiVersion' => $currentVersionTemp[0]['rx_norm_versions']['api_version'],
                ];
                $_SESSION['drug']['currentVersion'] = $currentVersionTemp;
            }
        } else {
            $currentVersion = $_SESSION['drug']['currentVersion'];
            $rxNormVersion = $_SESSION['drug']['rxNormVersion'];
        }
        if ($currentVersion != $rxNormVersion) {
            $this->downloadAllRxNorM();
            $_SESSION['drug']['currentVersion'] = $rxNormVersion;
            $this->controller->Drug->query(sprintf("INSERT INTO `rx_norm_versions` (`version`, `api_version`) VALUES ('%s', '%s')", $rxNormVersion['version'], $rxNormVersion['apiVersion']));
        }
    }

    private function downloadAllRxNorM()
    {
        $maxAllowedPacket = $this->controller->Drug->query("SHOW VARIABLES LIKE 'max_allowed_packet'");
        if (!empty($maxAllowedPacket[0]['session_variables']['Value'])){
            $maxAllowedPacket = $maxAllowedPacket[0]['session_variables']['Value'];
        }elseif (!empty($maxAllowedPacket[0]['SESSION_VARIABLES']['Value'])){
            $maxAllowedPacket = $maxAllowedPacket[0]['SESSION_VARIABLES']['Value'];
        }else{
            $maxAllowedPacket = 4194304;
        }

        $rawDrugNames = $this->get("https://rxnav.nlm.nih.gov/REST/displaynames.json");

        $drugNames = [];
        $query = 'INSERT IGNORE INTO `rx_norm_drugs` (`name`) VALUES ("%s");';

        if (!empty($rawDrugNames['data'])) {
            $drugRowNameLength = strlen($rawDrugNames['data']);

            $rawDrugNames = json_decode($rawDrugNames['data'], true)['displayTermsList']['term'];

            $rawDrugNames = array_filter($rawDrugNames, function ($item) {
                return strpos($item, " / ") === false;
            });

            $drugNumber = countCustom($rawDrugNames);
            $qurtySizeNeed = $drugRowNameLength + $drugNumber * 5; // ("Name"), 5 characters

            if ($qurtySizeNeed < $maxAllowedPacket) {
                $insertQuery = sprintf($query, implode('"),("', $rawDrugNames));
                $this->controller->Drug->query($insertQuery);
            } else {
                $time = floor($maxAllowedPacket / $qurtySizeNeed) * 4;
                $chunkNumber = floor($drugNumber / $time) + 1;
                for ($i = 0; $i <= $time; $i++) {
                    $insertQuery = sprintf($query, implode('"),("', array_slice($rawDrugNames, $i * $chunkNumber, ($i + 1) * $chunkNumber - 1 )));
                    $this->controller->Drug->query($insertQuery);
                }
            }
        }
    }

    private function getVersion()
    {
        $response = "";

        $this->setOptions([
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
        ]);

        $output = $this->get('https://rxnav.nlm.nih.gov/REST/version.json');
        if (empty($output['errors'])) {
            $response = json_decode($output['data'], true);
        }

        return $response;
    }

    public function getRxNumber($drugName = "")
    {
        $response = [];

        if(empty($drugName)){
            return $response;
        }

        $this->setOptions([
            CURLOPT_HTTPHEADER => ['Content-Type:application/json'],
        ]);

        $output = $this->get('https://rxnav.nlm.nih.gov/REST/rxcui.json', ['name' => $drugName]);
        if (empty($output['errors'])) {
            $output = json_decode($output['data'], true);
            $response = !empty($output['idGroup']['rxnormId'][0]) ? $output['idGroup']['rxnormId'][0] : __("rx norm id not found");
        }else{
            $response['error'] = !empty($output['errors'][0]) ? $output['errors'][0] : __('error');
        }

        return $response;
    }

    private function setOptions($options = [])
    {
        foreach ($options as $key => $value) {
            curl_setopt($this->ch, $key, $value);
        }
    }

    private function get($url, $parameters = [])
    {
        $this->setOptions([
            CURLOPT_POST => 0,
            CURLOPT_HTTPGET => 1,
            CURLOPT_URL => $url . (!empty($parameters) ? "?" . http_build_query($parameters) : ""),
        ]);
        $response = $this->call();

        return $response;
    }

    private function call()
    {
        $response = [
            'data' => [],
            'errors' => [],
        ];

        try {
            $output = curl_exec($this->ch);
            $httpCode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
            if ($httpCode == '200') {
                $response['data'] = $output;
            } else {
                $response['errors'][$httpCode] = self::$errors[$httpCode];
            }
        } catch (Exception $ex) {
            $response['errors'][] = $ex->getMessage();
        }

        return $response;
    }

}