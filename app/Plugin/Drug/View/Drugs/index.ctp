<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

$structureLinks = array(
    'bottom' => array(
        'add' => '/Drug/Drugs/add/',
    )
);

if (Configure::read('drug_method_name') == 'rxnorm'){
    $structureLinks['bottom']['complete missing drug rx norm id'] = [
        'icon' => 'refresh',
        'link' => '/Drug/Drugs/synchronize/',
        'title' => __('download the saved drugs information (takes time)'),
    ];
}

$finalAtimStructure = $atimStructure;
$finalOptions = array(
    'type' => 'search',
    'links' => array(
        'top' => array(
            'search' => '/Drug/Drugs/search/' . AppController::getNewSearchId()
        )
    ),
    'settings' => array(
        'actions' => false,
        'header' => __('search type', null) . ': ' . __('drugs', null),
        'return' => true,
    )
);

$finalAtimStructure2 = $emptyStructure;
$finalOptions2 = array(
    'links' => $structureLinks,
    'extras' => '<div class="ajax_search_results"></div>'
);

// CUSTOM CODE
$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}

// BUILD FORM
$form = $this->Structures->build($finalAtimStructure, $finalOptions);
if(Configure::read("drug_method_name") == 'rxnorm'){
    $form = str_replace('jqueryAutocomplete', '', $form);
}
echo $form;
$this->Structures->build($finalAtimStructure2, $finalOptions2);
$drugMethodName = Configure::read("drug_method_name");
if ($drugMethodName == "rxnorm"){
    require_once "rx_norm_drug.ctp";
}
?>
<script>
    var drugMethodName = '<?= $drugMethodName ?>';
</script>