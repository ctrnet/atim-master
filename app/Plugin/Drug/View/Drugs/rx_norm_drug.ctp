<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

?>

<script>
    var errorLoadingTheRxNormId = "<?php echo ___('error loading rx norm id'); ?>";
    function rxNormDrugInitScript(scope) {
        if ($(scope).hasClass("rxnorm_generic_name")) {
            $(scope).autocomplete("option", "minLength", 4);
            $(scope).autocomplete("option", "autoFocus", true);

            $(scope).on("autocompleteselect", function (event, ui) {

                var formType = "", found = false;
                rx_norm_id = -1, rx_norm_label = ui.item.label;
                $(this).closest("form").find("table").each(function () {
                    formType = $(this).attr("data-atim-type");
                    if (formType == 'add' || formType == 'edit' || formType == 'addgrid' || formType == 'editgrid') {
                        found = formType;
                    }
                });

                var rcCodeInput = "";
                if (found == 'add' || found == 'edit') {
                    rcCodeInput = $(this).closest("table").find("input.rxnorm_id");
                } else if (found == 'addgrid' || found == 'editgrid') {
                    rcCodeInput = $(this).closest("tr").find("input.rxnorm_id");
                }
                if (rcCodeInput.length == 1) {

                    getRxCodeByAPI();

                    function getRxCodeByAPI(){
                        $(rcCodeInput).removeClass("error");
                        $.ajax({
                            url: "https://rxnav.nlm.nih.gov/REST/rxcui.json?name=" + rx_norm_label,
                            type: 'GET',
                            success: function(data){
                                if (typeof data?.idGroup?.rxnormId[0] != 'undefined') {
                                    rxNumber = data?.idGroup?.rxnormId[0];
                                    $(rcCodeInput).val(rxNumber);
                                    $(rcCodeInput).siblings("span.icon16.redo").remove();
                                }
                            },
                            error: function(data) {
                                $(rcCodeInput).addClass("error", 500);
                                $(rcCodeInput).siblings("span.icon16.redo").remove();
                                $(rcCodeInput).after("<span class = 'icon16 redo' title = '" + errorLoadingTheRxNormId + "'></span>");
                                $(rcCodeInput).siblings("span.icon16.redo").css("cursor", "pointer");
                                $(rcCodeInput).siblings("span.icon16.redo").off("click").on("click", getRxCodeByAPI);
                            }
                        });
                    }
                }

            });
        }
    }

</script>