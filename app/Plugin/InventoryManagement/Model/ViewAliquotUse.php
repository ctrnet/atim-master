<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class ViewAliquotUse
 */
class ViewAliquotUse extends InventoryManagementAppModel
{

    const JOINS = 0;

    const SOURCE_ID = 1;

    const USE_DEFINITION = 2;

    const USE_CODE = 3;

    const USE_DETAIL = 4;

    const USE_VOLUME = 5;

    const CREATED = 6;

    const DETAIL_URL = 7;

    const SAMPLE_MASTER_ID = 8;

    const COLLECTION_ID = 9;

    const USE_DATETIME = 10;

    const USE_BY = 11;

    const VOLUME_UNIT = 12;

    const PLUGIN = 13;

    const USE_DATETIME_ACCU = 14;

    const DURATION = 15;

    const DURATION_UNIT = 16;

    const STUDY_SUMMARY_ID = 17;

    const STUDY_TITLE = 18;

    public $baseModel = "InventoryActionMaster";

    public $basePlugin = 'InventoryManagement';

    // Don't put extra delete != 1 check on joined tables or this might result in deletion issues.
    public static $tableCreateQuery = "CREATE TABLE view_aliquot_uses (
		  id int(20) NOT NULL,
		  aliquot_master_id int NOT NULL,
		  use_definition varchar(360) DEFAULT NULL,
		  use_code varchar(250) DEFAULT NULL,
		  use_details VARchar(250) DEFAULT NULL,
		  used_volume decimal(10,5) DEFAULT NULL,
		  aliquot_volume_unit varchar(20) DEFAULT NULL,
		  use_datetime datetime DEFAULT NULL,
		  use_datetime_accuracy char(1) DEFAULT NULL,
		  duration int(6) DEFAULT NULL,
		  duration_unit VARCHAR(250) DEFAULT NULL,
		  used_by VARCHAR(50) DEFAULT NULL,
		  created datetime DEFAULT NULL,
		  detail_url varchar(250) DEFAULT NULL,
		  sample_master_id int(11) NOT NULL,
		  collection_id int(11) NOT NULL,
		  study_summary_id int(11) DEFAULT NULL,
		  study_summary_title varchar(45) DEFAULT NULL
		)";
    
    // must respect concat(id, #) defined in $tableQuery variable
    private $concatDashNbrToModel = array(
        '1' => 'SourceAliquot',
        '2' => 'Realiquoting',
        '4' => 'OrderItem',
        '7' => 'OrderItem',
        '9' => 'InventoryActionMaster'
    );
    
    public static $tableQuery = "SELECT CONCAT(SourceAliquot.id, 1) AS `id`,
		AliquotMaster.id AS aliquot_master_id,
		CONCAT('sample derivative creation#', SampleMaster.sample_control_id) AS use_definition,
		SampleMaster.sample_code AS use_code,
		'' AS `use_details`,
		SourceAliquot.used_volume AS used_volume,
		AliquotControl.volume_unit AS aliquot_volume_unit,
		DerivativeDetail.creation_datetime AS use_datetime,
		DerivativeDetail.creation_datetime_accuracy AS use_datetime_accuracy,
		NULL AS `duration`,
		'' AS `duration_unit`,
		DerivativeDetail.creation_by AS used_by,
		SourceAliquot.created AS created,
		CONCAT('/InventoryManagement/SampleMasters/detail/',SampleMaster.collection_id,'/',SampleMaster.id) AS detail_url,
		SampleMaster2.id AS sample_master_id,
		SampleMaster2.collection_id AS collection_id,
		NULL AS study_summary_id,
		'' AS study_title
		FROM source_aliquots AS SourceAliquot
		JOIN sample_masters AS SampleMaster ON SampleMaster.id = SourceAliquot.sample_master_id
		JOIN derivative_details AS DerivativeDetail ON SampleMaster.id = DerivativeDetail.sample_master_id
		JOIN aliquot_masters AS AliquotMaster ON AliquotMaster.id = SourceAliquot.aliquot_master_id
		JOIN aliquot_controls AS AliquotControl ON AliquotMaster.aliquot_control_id = AliquotControl.id
		JOIN sample_masters SampleMaster2 ON SampleMaster2.id = AliquotMaster.sample_master_id
		WHERE SourceAliquot.deleted <> 1 %%WHERE%%
	
		UNION ALL
	
		SELECT CONCAT(Realiquoting.id ,2) AS id,
		AliquotMaster.id AS aliquot_master_id,
		'realiquoted to' AS use_definition,
		AliquotMasterChild.barcode AS use_code,
		'' AS use_details,
		Realiquoting.parent_used_volume AS used_volume,
		AliquotControl.volume_unit AS aliquot_volume_unit,
		Realiquoting.realiquoting_datetime AS use_datetime,
		Realiquoting.realiquoting_datetime_accuracy AS use_datetime_accuracy,
		NULL AS duration,
		'' AS duration_unit,
		Realiquoting.realiquoted_by AS used_by,
		Realiquoting.created AS created,
		CONCAT('/InventoryManagement/AliquotMasters/detail/',AliquotMasterChild.collection_id,'/',AliquotMasterChild.sample_master_id,'/',AliquotMasterChild.id) AS detail_url,
		AliquotMaster.sample_master_id AS sample_master_id,
		AliquotMaster.collection_id AS collection_id,
		NULL AS study_summary_id,
		'' AS study_title
		FROM realiquotings AS Realiquoting
		JOIN aliquot_masters AS AliquotMaster ON AliquotMaster.id = Realiquoting.parent_aliquot_master_id
		JOIN aliquot_controls AS AliquotControl ON AliquotMaster.aliquot_control_id = AliquotControl.id
		JOIN aliquot_masters AS AliquotMasterChild ON AliquotMasterChild.id = Realiquoting.child_aliquot_master_id
		WHERE Realiquoting.deleted <> 1 %%WHERE%%
	
		UNION ALL
		
		SELECT CONCAT(OrderItem.id, 4) AS id,
		AliquotMaster.id AS aliquot_master_id,
		IF(OrderItem.shipment_id, 'aliquot shipment', 'order preparation') AS use_definition,
		IF(OrderItem.shipment_id, Shipment.shipment_code, Order.order_number) AS use_code,
		'' AS use_details,
		NULL AS used_volume,
		'' AS aliquot_volume_unit,
		IF(OrderItem.shipment_id, Shipment.datetime_shipped, OrderItem.date_added) AS use_datetime,
		IF(OrderItem.shipment_id, Shipment.datetime_shipped_accuracy, IF(OrderItem.date_added_accuracy = 'c', 'h', OrderItem.date_added_accuracy)) AS use_datetime_accuracy,
		NULL AS duration,
		'' AS duration_unit,
		IF(OrderItem.shipment_id, Shipment.shipped_by, OrderItem.added_by) AS used_by,
		IF(OrderItem.shipment_id, Shipment.created, OrderItem.created) AS created,
		IF(OrderItem.shipment_id,
				CONCAT('/Order/Shipments/detail/',OrderItem.order_id,'/',OrderItem.shipment_id),
				IF(OrderItem.order_line_id,
						CONCAT('/Order/OrderLines/detail/',OrderItem.order_id,'/',OrderItem.order_line_id),
						CONCAT('/Order/Orders/detail/',OrderItem.order_id))
		) AS detail_url,
		AliquotMaster.sample_master_id AS sample_master_id,
		AliquotMaster.collection_id AS collection_id,
		IF(OrderLine.study_summary_id, OrderLine.study_summary_id, Order.default_study_summary_id) AS study_summary_id,
		IF(OrderLine.study_summary_id, OrderLineStudySummary.title, OrderStudySummary.title) AS study_title
		FROM order_items OrderItem
		JOIN aliquot_masters AS AliquotMaster ON AliquotMaster.id = OrderItem.aliquot_master_id
		LEFT JOIN shipments AS Shipment ON Shipment.id = OrderItem.shipment_id
		LEFT JOIN order_lines AS OrderLine ON  OrderLine.id = OrderItem.order_line_id
		LEFT JOIN study_summaries AS OrderLineStudySummary ON OrderLineStudySummary.id = OrderLine.study_summary_id AND OrderLineStudySummary.deleted != 1
		JOIN `orders` AS `Order` ON  Order.id = OrderItem.order_id
		LEFT JOIN study_summaries AS OrderStudySummary ON OrderStudySummary.id = Order.default_study_summary_id AND OrderStudySummary.deleted != 1
		WHERE OrderItem.deleted <> 1 %%WHERE%%
		
		UNION ALL
		
		SELECT CONCAT(OrderItem.id, 7) AS id,
		AliquotMaster.id AS aliquot_master_id,
		'shipped aliquot return' AS use_definition,
		Shipment.shipment_code AS use_code,
		'' AS use_details,
		NULL AS used_volume,
		'' AS aliquot_volume_unit,
		OrderItem.date_returned AS use_datetime,
		IF(OrderItem.date_returned_accuracy = 'c', 'h', OrderItem.date_returned_accuracy) AS use_datetime_accuracy,
		NULL AS duration,
		'' AS duration_unit,
		OrderItem.reception_by AS used_by,
		OrderItem.modified AS created,
		CONCAT('/Order/Shipments/detail/',OrderItem.order_id,'/',OrderItem.shipment_id) AS detail_url,
		AliquotMaster.sample_master_id AS sample_master_id,
		AliquotMaster.collection_id AS collection_id,
		IF(OrderLine.study_summary_id, OrderLine.study_summary_id, Order.default_study_summary_id) AS study_summary_id,
		IF(OrderLine.study_summary_id, OrderLineStudySummary.title, OrderStudySummary.title) AS study_title
		FROM order_items OrderItem
		JOIN aliquot_masters AS AliquotMaster ON AliquotMaster.id = OrderItem.aliquot_master_id
		JOIN shipments AS Shipment ON Shipment.id = OrderItem.shipment_id
		LEFT JOIN order_lines AS OrderLine ON  OrderLine.id = OrderItem.order_line_id
		LEFT JOIN study_summaries AS OrderLineStudySummary ON OrderLineStudySummary.id = OrderLine.study_summary_id AND OrderLineStudySummary.deleted != 1
		JOIN `orders` AS `Order` ON  Order.id = OrderItem.order_id
		LEFT JOIN study_summaries AS OrderStudySummary ON OrderStudySummary.id = Order.default_study_summary_id AND OrderStudySummary.deleted != 1
		WHERE OrderItem.deleted <> 1 AND OrderItem.status = 'shipped & returned' %%WHERE%%
		
		UNION ALL
		
        SELECT CONCAT(InventoryActionMaster.id,9) AS id,
        AliquotMaster.id AS aliquot_master_id,
        InventoryActionControl.type AS use_definition,
        InventoryActionMaster.title AS use_code,
        '' AS use_details,
        InventoryActionMaster.used_volume AS used_volume,
        AliquotControl.volume_unit AS aliquot_volume_unit,
        InventoryActionMaster.date AS use_datetime,
		IF(InventoryActionMaster.date_accuracy = 'c', 'h', InventoryActionMaster.date_accuracy) AS use_datetime_accuracy,
        null AS duration,
        '' AS duration_unit,
        InventoryActionMaster.person AS used_by,
        InventoryActionMaster.created AS created,
        CONCAT('/InventoryManagement/InventoryActionMasters/detail/',InventoryActionMaster.id) AS detail_url,
        AliquotMaster.sample_master_id AS sample_master_id,
        AliquotMaster.collection_id AS collection_id,
        StudySummary.id AS study_summary_id,
        StudySummary.title AS study_title
        FROM inventory_action_masters AS InventoryActionMaster
        JOIN inventory_action_controls AS InventoryActionControl ON InventoryActionControl.id = InventoryActionMaster.inventory_action_control_id
        INNER JOIN aliquot_masters AS AliquotMaster ON AliquotMaster.id = InventoryActionMaster.aliquot_master_id
        JOIN aliquot_controls AS AliquotControl ON AliquotMaster.aliquot_control_id = AliquotControl.id
        LEFT JOIN study_summaries AS StudySummary ON StudySummary.id = InventoryActionMaster.study_summary_id AND StudySummary.deleted != 1
        WHERE InventoryActionMaster.aliquot_master_id IS NOT NULL
        AND InventoryActionMaster.deleted <> 1 %%WHERE%%";

    /**
     *
     * @return array
     */
    public function getUseDefinitions()
    {
        $result = array(
            ___('order and shipping') => [
                'order preparation' => __('order preparation'),
                'aliquot shipment' => __('aliquot shipment'),
                'shipped aliquot return' => __('shipped aliquot return')
            ]
        );

        // Add invetory action type
        $lang = Configure::read('Config.language') == "eng" ? "en" : "fr";
        $inventoryActionControl = AppModel::getInstance('InventoryManagement', 'InventoryActionControl', true);
        $result[___('inventory action')] = $inventoryActionControl->getActiveTypesPermissibleValues();
            
        // Rwaliquoting
        $result[___('realiquoting')] = ['realiquoted to' => __('realiquoted to')];
        
        // Develop sample derivative creation
        $this->SampleControl = AppModel::getInstance("InventoryManagement", "SampleControl", true);
        $sampleControls = $this->SampleControl->getSampleTypePermissibleValuesFromId();
        $result[___('sample derivative creation')] = [];
        foreach ($sampleControls as $samplControlId => $sampleType) {
            $result[___('sample derivative creation')]['sample derivative creation#' . $samplControlId] = __('sample derivative creation#') . $sampleType;
        }
        return $result;
    }

    public function getUseDefinitionsSearch()
    {
        $result = array(
            ___('aliquot use') => [
                'aliquot shipment' => __('aliquot shipment'),
                'shipped aliquot return' => __('shipped aliquot return'),
                'order preparation' => __('order preparation'),
                'realiquoted to' => __('realiquoted to'),
            ]
        );

        // Add invetory action type
        $lang = Configure::read('Config.language') == "eng" ? "en" : "fr";
        $inventoryActionControl = AppModel::getInstance('InventoryManagement', 'InventoryActionControl', true);
        $result[___('inventory action')] = $inventoryActionControl->getActiveAliquotTypesPermissibleValues();

        // Develop sample derivative creation
        $this->SampleControl = AppModel::getInstance("InventoryManagement", "SampleControl", true);
        $sampleControls = $this->SampleControl->getAllDerivatedTypePermissibleValues();
        $result[___('sample derivative creation')] = [];
        foreach ($sampleControls as $samplControlId => $sampleType) {
            $result[___('sample derivative creation')]['sample derivative creation#' . $samplControlId] = __('sample derivative creation#') . $sampleType;
        }
        return $result;
    }



    /**
     *
     * @param $data
     * @return array
     */
    public function getPkeyAndModelToCheck($data)
    {
        $pkey = null;
        $model = null;
        if (preg_match('/^([0-9]+)([0-9])$/', current(current($data)), $matches)) {
            $pkey = $matches[1];
            $modelId = $matches[2];
            if (isset($this->concatDashNbrToModel[$modelId])) {
                $model = $this->concatDashNbrToModel[$modelId];
            } else {
                AppController::getInstance()->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            }
        } else {
            AppController::getInstance()->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        return array(
            'pkey' => $pkey,
            'base_model' => $model
        );
    }
}