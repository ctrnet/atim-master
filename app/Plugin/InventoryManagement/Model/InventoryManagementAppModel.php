<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class InventoryManagementAppModel
 */
class InventoryManagementAppModel extends AppModel
{
    /**
     * WARNING: Duplicated function in ClinicalAnnotationAppModel and InventoryManagementAppModel
     * To allow validation in both plugins.
     *
     * @param $id
     * @return mixed
     */
    public function validateIcdo3TopoCode($id)
    {
        $icdO3TopoModel = AppModel::getInstance('CodingIcd', 'CodingIcdo3Topo', true);
        return $icdO3TopoModel::validateId($id);
    }
    
    /**
     * WARNING: Duplicated function in ClinicalAnnotationAppModel and InventoryManagementAppModel
     * To allow validation in both plugins.
     *
     * @return mixed
     */
    public function getIcdO3TopoCategoriesCodes()
    {
        $icdO3TopoModel = AppModel::getInstance('CodingIcd', 'CodingIcdo3Topo', true);
        return $icdO3TopoModel::getTopoCategoriesCodes();
    }
    
    /**
     * WARNING: Duplicated function in ClinicalAnnotationAppModel and InventoryManagementAppModel
     * To allow validation in both plugins.
     *
     * @param $id
     * @return mixed
     */
    public function validateIcdo3MorphoCode($id)
    {
        $icdO3MorphoModel = AppModel::getInstance('CodingIcd', 'CodingIcdo3Morpho', true);
        return $icdO3MorphoModel::validateId($id);
    }
    
    /**
     * WARNING: Duplicated function in ClinicalAnnotationAppModel and InventoryManagementAppModel
     * To allow validation in both plugins.
     *
     * @param $id
     * @param $mCodeValueSetName
     * @return mixed
     */
    public function validateMcodeCode($id, $mCodeValueSetName)
    {
        $codingMCodeModel = AppModel::getInstance('CodingIcd', 'CodingMCode', true);
        $codingMCodeModel->setValueSetName($mCodeValueSetName);
        return $codingMCodeModel::validateId($id, $mCodeValueSetName);
    }
    
    /**
     * WARNING: Duplicated function in ClinicalAnnotationAppModel and InventoryManagementAppModel
     * To allow validation in both plugins.
     *
     * @param $id
     * @param $otherCodeValueSetName
     * @return mixed
     */
    public function validateOtherCode($id, $otherCodeValueSetName)
    {
        $codingOtherCodeModel = AppModel::getInstance('CodingIcd', 'CodingOtherCode', true);
        $codingOtherCodeModel->setValueSetName($otherCodeValueSetName);
        return $codingOtherCodeModel::validateId($id, $otherCodeValueSetName);
    }
}