<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class ActionMaster
 */
class InventoryActionMaster extends InventoryManagementAppModel
{
    public $belongsTo = array(
        'InventoryActionControl' => array(
            'className' => 'InventoryManagement.InventoryActionControl',
            'foreignKey' => 'inventory_action_control_id',
            'type' => 'INNER'
        ),
        'SampleMaster' => array(
            'className' => 'InventoryManagement.SampleMaster',
            'foreignKey' => 'sample_master_id',
            'type' => 'INNER'
        ),
        'AliquotMaster' => array(
            'className' => 'InventoryManagement.AliquotMaster',
            'foreignKey' => 'aliquot_master_id'
        ),
        'StudySummary' => array(
            'className' => 'Study.StudySummary',
            'foreignKey' => 'study_summary_id'
        )
    );
    
    public $registeredView = array(
        'InventoryManagement.ViewAliquotUse' => array(
            'InventoryActionMaster.id'
        )
    );

    public function validates($options = array())
    {
        $this->validatesStudy();
        return parent::validates($options);
    }

    private function validatesStudy()
    {
        if (!empty($this->data['FunctionManagement']['autocomplete_aliquot_internal_use_study_summary_id'])) {
            $studyTitle = $this->data['FunctionManagement']['autocomplete_aliquot_internal_use_study_summary_id'];
            $studyModel = AppModel::getInstance("Study", "StudySummary");
            $studyCode = $studyModel->getStudyIdFromStudyDataAndCode($studyTitle);
            if (!empty($studyCode['error'])) {
                $this->validationErrors['autocomplete_aliquot_internal_use_study_summary_id'][] = $studyCode['error'];
            } elseif (!empty($studyCode[$studyModel->name]['id'])) {
                $this->data['InventoryActionMaster']['study_summary_id'] = $studyCode[$studyModel->name]['id'];
            }
        }
    }

    public function afterFind($results, $primary = false)
    {
        foreach ($results as &$result){
            $result['FunctionManagement']['aliquot_master_name'] = (! empty($result['AliquotMaster']['barcode'])) ? $this->formatInvActionAliquotMasterNameAliquot($result) : '';
        }
        return parent::afterFind($results, $primary);
    }
    
    public function formatInvActionAliquotMasterNameAliquot($aliquotData)
    {
        return $aliquotData['AliquotMaster']['barcode'] . (strlen($aliquotData['AliquotMaster']['aliquot_label'])? ' [' . $aliquotData['AliquotMaster']['aliquot_label'] . ']' : '');
    }

    public function summary($variables = [])
    {
        $return = false;

        return $return;

    }

    public function getUseCodeForDisplayInTreeView($passedData) {
        $formatedData = '';
        if (isset($passedData['ViewAliquotUse']['use_code']) && strlen($passedData['ViewAliquotUse']['use_code']) && $passedData['ViewAliquotUse']['use_code'] != '-') {
            $formatedData = $passedData['ViewAliquotUse']['use_code'];
        } elseif (isset($passedData['InventoryActionMaster']['title']) && strlen($passedData['InventoryActionMaster']['title']) && $passedData['InventoryActionMaster']['title'] != '-') {
            $formatedData = $passedData['InventoryActionMaster']['title'];
        }
        if (isset($passedData['InventoryActionControl'])) {
            $structureValueDomainModel = AppModel::getInstance('', 'StructureValueDomain', true);
            switch($passedData['InventoryActionControl']['category'] . '-' . $passedData['InventoryActionControl']['type']) {
                case 'quality control-quality control':
                    if (isset($passedData['InventoryActionDetail']['test']) && $passedData['InventoryActionControl']['detail_form_alias'] == 'inventory_action_qualityctrls') {
                        $formatedData = [];
                        $formatedData[] = $structureValueDomainModel->getValueToDisplay('quality_control_tests', $passedData['InventoryActionDetail']['test']);
                        $formatedData[] = isset($passedData['InventoryActionMaster']['title'])? $passedData['InventoryActionMaster']['title']: '';
                        $formatedData = array_filter($formatedData, 'strlen');
                        $formatedData = implode(' - ', $formatedData);
                    }
                    break;
                    
                case 'aliquot_internal_uses-laboratory activity':
                    if (isset($passedData['InventoryActionDetail']['type']) && $passedData['InventoryActionControl']['detail_form_alias'] == 'inventory_action_lab_activities') {
                        $formatedData = [];
                        $formatedData[] = $structureValueDomainModel->getValueToDisplay('inventory_action_lab_activity_types', $passedData['InventoryActionDetail']['type']);
                        $formatedData[] = isset($passedData['InventoryActionMaster']['title'])? $passedData['InventoryActionMaster']['title']: '';
                        $formatedData = array_filter($formatedData, 'strlen');
                        $formatedData = implode(' - ', $formatedData);
                    }
                    break;
                
                default:                
            }
        }
        return $formatedData;
    }

    public function beforeFind($query)
    {
        // If no IEA type selected
        if (!isset($query['conditions']['InventoryActionControl.type']) && !empty($query['conditions']['InventoryActionControl.all_types'])) {
            $query['conditions']['InventoryActionControl.type'] = $query['conditions']['InventoryActionControl.all_types'];
        } elseif (!empty($query['conditions']['InventoryActionControl.type'])) {
        } elseif (!empty($query['conditions']['InventoryActionMaster.sample_master_id']) && is_array($query['conditions']['InventoryActionMaster.sample_master_id']) && (empty($query['recursive']) || $query['recursive'] > -1)) {
            $model = AppModel::getInstance('InventoryManagement', 'InventoryActionControl');
            $query['conditions']['InventoryActionControl.type'] = array_keys($model->getActiveAliquotSampleTypesPermissibleValues());
        } elseif (!empty($query['conditions']['InventoryActionMaster.aliquot_master_id']) && is_array($query['conditions']['InventoryActionMaster.aliquot_master_id']) && (empty($query['recursive']) || $query['recursive'] > -1)) {
            $model = AppModel::getInstance('InventoryManagement', 'InventoryActionControl');
            $query['conditions']['InventoryActionControl.type'] = array_keys($model->getActiveAliquotTypesPermissibleValues());
        }

        unset($query['conditions']['InventoryActionControl.all_types']);

        foreach ($query['conditions'] as $key => $condition){
            if (is_string($condition) && $condition == "(InventoryActionControl.aliquot_master_id_field LIKE '%1%')"){
                unset($query['conditions'][$key]);
                if (empty($query['conditions']['InventoryActionMaster.aliquot_master_id'])){
                    $query['conditions'][] = "InventoryActionMaster.aliquot_master_id IS NOT NULL";
                }
                break;
            }
        }

        return $query;
    }
}