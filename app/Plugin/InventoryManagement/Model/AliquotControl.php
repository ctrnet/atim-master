<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class AliquotControl
 */
class AliquotControl extends InventoryManagementAppModel
{

    public $masterFormAlias = 'aliquot_masters';
    public $controlType = 'aliquot_type';

    /**
     * Get permissible values array gathering all existing aliquot types.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getAliquotTypePermissibleValuesFromId()
    {
        return $this->getAliquotsTypePermissibleValues(true, null);
    }

    /**
     * Get permissible values array gathering all existing aliquot types.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getAliquotTypePermissibleValues()
    {
        return $this->getAliquotsTypePermissibleValues(false, null);
    }

    /**
     *
     * @param $useId
     * @param $parentSampleControlId
     * @return array
     */
    public function getAliquotsTypePermissibleValues($useId, $parentSampleControlId)
    {
        $result = array();

        // Build tmp array to sort according translation
        $conditions = array(
            'AliquotControl.flag_active' => 1
        );
        if ($parentSampleControlId != null) {
            $conditions['AliquotControl.sample_control_id'] = $parentSampleControlId;
        }

        if ($useId) {
            $this->bindModel(array(
                'belongsTo' => array(
                    'SampleControl' => array(
                        'className' => 'InventoryManagement.SampleControl',
                        'foreignKey' => 'sample_control_id'
                    )
                )
            ));
            $aliquotControls = $this->find('all', array(
                'conditions' => $conditions
            ));
            foreach ($aliquotControls as $aliquotControl) {
                $result[$aliquotControl['AliquotControl']['id']] = __($aliquotControl['AliquotControl']['aliquot_type']);
                // $aliquotTypePrecision = $aliquotControl['AliquotControl']['aliquot_type_precision'];
                // $result[$aliquotControl['AliquotControl']['id']] = __($aliquotControl['AliquotControl']['aliquot_type'])
                // . ' ['.__($aliquotControl['SampleControl']['sample_type'])
                // . (empty($aliquotTypePrecision)? '' : ' - ' . __($aliquotTypePrecision)) . ']';
            }
        } else {
            $aliquotControls = $this->find('all', array(
                'conditions' => $conditions
            ));
            foreach ($aliquotControls as $aliquotControl) {
                $result[$aliquotControl['AliquotControl']['aliquot_type']] = __($aliquotControl['AliquotControl']['aliquot_type']);
            }
        }
        natcasesort($result);

        return $result;
    }

    /**
     *
     * @param $parentSampleControlId
     * @return array
     */
    public function getPermissibleAliquotsArray($parentSampleControlId, $haveToBeActive = true)
    {
        $conditions = [
            'AliquotControl.sample_control_id' => $parentSampleControlId
        ];

        if ($haveToBeActive){
            $conditions['AliquotControl.flag_active'] = true;
        }

        $controls = $this->find('all', array(
            'conditions' => $conditions
        ));
        $aliquotControlsList = array();
        foreach ($controls as $control) {
            $aliquotControlsList[$control['AliquotControl']['id']]['AliquotControl'] = $control['AliquotControl'];
        }
        return $aliquotControlsList;
    }

    /**
     * Get permissible values array gathering all existing sample aliquot types
     * (realtions existing between sample type and aliquot type).
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getSampleAliquotTypesPermissibleValues()
    {
        // Get list of active sample type
        $conditions = array(
            'ParentToDerivativeSampleControl.flag_active' => true
        );

        $this->ParentToDerivativeSampleControl = AppModel::getInstance("InventoryManagement", "ParentToDerivativeSampleControl", true);
        $controls = $this->ParentToDerivativeSampleControl->find('all', array(
            'conditions' => $conditions,
            'fields' => array(
                'DerivativeControl.*'
            )
        ));

        $specimenSampleControlIdsList = array();
        foreach ($controls as $control) {
            $specimenSampleControlIdsList[] = $control['DerivativeControl']['id'];
        }

        // Build final list
        $this->bindModel(array(
            'belongsTo' => array(
                'SampleControl' => array(
                    'className' => 'InventoryManagement.SampleControl',
                    'foreignKey' => 'sample_control_id'
                )
            )
        ));
        $result = $this->find('all', array(
            'conditions' => array(
                'AliquotControl.flag_active' => '1',
                'AliquotControl.sample_control_id' => $specimenSampleControlIdsList
            ),
            'order' => array(
                'SampleControl.sample_type' => 'asc',
                'AliquotControl.aliquot_type' => 'asc'
            )
        ));

        $workingArray = array();
        $lastSampleType = '';
        foreach ($result as $newSampleAliquot) {
            $sampleControlId = $newSampleAliquot['SampleControl']['id'];
            $aliquotControlId = $newSampleAliquot['AliquotControl']['id'];

            $sampleType = $newSampleAliquot['SampleControl']['sample_type'];
            $aliquotType = $newSampleAliquot['AliquotControl']['aliquot_type'];

            // New Sample Type
            if ($lastSampleType != $sampleType) {
                // Add just sample type to the list
                $workingArray[$sampleControlId . '|'] = __($sampleType);
            }

            // New Sample-Aliquot
            $workingArray[$sampleControlId . '|' . $aliquotControlId] = __($sampleType) . ' - ' . __($aliquotType);
        }
        natcasesort($workingArray);

        return $workingArray;
    }

    public function getAliquotTypesPermissibleValues()
    {
        // Get list of active sample type
        $conditions = array(
            'ParentToDerivativeSampleControl.flag_active' => true
        );

        $this->ParentToDerivativeSampleControl = AppModel::getInstance("InventoryManagement", "ParentToDerivativeSampleControl", true);
        $controls = $this->ParentToDerivativeSampleControl->find('all', array(
            'conditions' => $conditions,
            'fields' => array(
                'DerivativeControl.*'
            )
        ));

        $specimenSampleControlIdsList = array();
        foreach ($controls as $control) {
            $specimenSampleControlIdsList[] = $control['DerivativeControl']['id'];
        }

        // Build final list
        $this->bindModel(array(
            'belongsTo' => array(
                'SampleControl' => array(
                    'className' => 'InventoryManagement.SampleControl',
                    'foreignKey' => 'sample_control_id'
                )
            )
        ));
        $result = $this->find('all', array(
            'conditions' => array(
                'AliquotControl.flag_active' => '1',
                'AliquotControl.sample_control_id' => $specimenSampleControlIdsList
            ),
            'order' => array(
                'SampleControl.sample_type' => 'asc',
                'AliquotControl.aliquot_type' => 'asc'
            )
        ));

        $workingArray = array();
        foreach ($result as $newSampleAliquot) {
            $sampleControlId = $newSampleAliquot['SampleControl']['id'];
            $aliquotControlId = $newSampleAliquot['AliquotControl']['id'];

            $sampleType = $newSampleAliquot['SampleControl']['sample_type'];
            $aliquotType = $newSampleAliquot['AliquotControl']['aliquot_type'];


            // New Sample-Aliquot
            $workingArray[$aliquotControlId] = __($sampleType) . ' - ' . __($aliquotType);
//            $workingArray[$sampleControlId . '|' . $aliquotControlId] = __($sampleType) . ' - ' . __($aliquotType);
        }
        natcasesort($workingArray);
//        $workingArray = ['all|all' => __('all')] + $workingArray;
        $workingArray = ['all' => __('all')] + $workingArray;

        return $workingArray;
    }

    /**
     *
     * @param mixed $results
     * @param bool $primary
     * @return mixed
     */
    public function afterFind($results, $primary = false)
    {
        return $this->applyMasterFormAlias($results, $primary);
    }

    public function setDataBeforeSaveFB(&$data)
    {
        $prefix = getPrefix();
        $model = $this->name;
        $maxDisplayOrder = $this->find("first", array(
            'fields'=>array("display_order"),
            'order'=>array("display_order DESC")
        ));

        if (!isset($data[$model][$this->controlType])){
            return;
        }

        $name = $data[$model][$this->controlType];
        $detailTableName = createTableName($prefix . 'al', time(), $name);

        $data[$model]["detail_tablename"] = strtolower($detailTableName);
        $data[$model]["detail_form_alias"] = $detailTableName;
        $data[$model]["flag_active"] = 0;
        $data[$model]["display_order"] = empty($maxDisplayOrder[$model]["display_order"]) ? 1 : $maxDisplayOrder[$model]["display_order"] + 1;
        $data[$model]["flag_test_mode"] = '1';
        $this->addWritableField();
    }

    public function setDataBeforeEditFB(&$data)
    {

        $controlItem = $this->getOrRedirectFB($this->id);
        $this->dataToDic($controlItem);

        if (isset($data[$this->name]) && !empty($data[$this->name])){
            foreach ($data[$this->name] as $key=>$value) {
                $controlItem[$this->name][$key] = $value;
            }
        }
        $data[$this->name] = $controlItem[$this->name];

        if (empty($data[$this->name]['detail_form_alias']) && !empty($data['detailTest'])){
            $prefix = getPrefix();
            $model = $this->name;

            if (!isset($data[$model][$this->controlType])){
                return;
            }
            $name = $data[$model][$this->controlType];
            $detailTableName = createTableName($prefix . 'al', time(), $name);

            $data[$model]["detail_form_alias"] = (!empty(trim($data[$model]["detail_form_alias"]))) ? implode(",", array_merge(explode(",", $data[$model]["detail_form_alias"]), array($detailTableName))) : $detailTableName;
            $this->addWritableField();

        }
    }

    public function validates($options = array())
    {
        $data = $this->find('all', array(
            'conditions' => array(
                $this->name.'.'.$this->controlType.' LIKE ' => '% || '.$this->data[$this->name][$this->controlType]
            )
        ));

        if (!empty($data) && (( countCustom($data) ==1 && $data[0][$this->name]['id'] != $this->id) || countCustom($data) > 1)){
            $this->validationErrors[$this->controlType][] = __("this type has already existed");
        }

        return parent::validates($options);
    }

    public function addWritableField($field = array(), $tablename = null)
    {
        if (!is_array($field)){
            $field = array($field);
        }
        parent::addWritableField(array_merge($field, array("sample_type", "sample_category", "detail_form_alias", "detail_tablename", "display_order", "flag_active", "flag_test_mode", "flag_form_builder", "flag_active_input")), $tablename);
    }

    public function getName()
    {
        return __('aliquot');
    }

    public function dataToDic(&$data)
    {
        if (isset($data[$this->name])){
            if (isset($data[$this->name][$this->controlType])){
                $data[$this->name][$this->controlType.'_id'] = $data[$this->name][$this->controlType];
            }
        }else{
            foreach ($data as &$value) {
                if (isset($value[$this->name][$this->controlType])){
                    $value[$this->name][$this->controlType.'_id'] = $value[$this->name][$this->controlType];
                }
            }
        }
    }


}