<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class SampleControl
 */
class SampleControl extends InventoryManagementAppModel
{

    public $masterFormAlias = 'sample_masters';
    
    public $controlType = 'sample_type';

    public $actsAs = array(
        'OrderByTranslate' => array(
            'sample_type',
            'sample_category'
        )
    );

    /**
     * Get permissible values array gathering all existing sample types.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getSampleTypePermissibleValuesFromId()
    {
        return $this->getSamplesPermissibleValues(true, false);
    }

    /**
     *
     * @return array
     */
    public function getParentSampleTypePermissibleValuesFromId()
    {
        return $this->getSamplesPermissibleValues(true, false, false);
    }

    /**
     * Get permissible values array gathering all existing sample types.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getSampleTypePermissibleValues()
    {
        return $this->getSamplesPermissibleValues(false, false);
    }

    /**
     *
     * @return array
     */
    public function getParentSampleTypePermissibleValues()
    {
        return $this->getSamplesPermissibleValues(false, false, false);
    }

    /**
     * Get permissible values array gathering all existing specimen sample types.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getSpecimenSampleTypePermissibleValues()
    {
        return $this->getSamplesPermissibleValues(false, true);
    }

    /**
     * Get permissible values array gathering all existing specimen sample types.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getSpecimenSampleTypePermissibleValuesFromId()
    {
        return $this->getSamplesPermissibleValues(true, true);
    }

    public function getSpecimenSampleTypePermissibleValuesFromIdAll()
    {
        $result = $this->getSamplesPermissibleValues(true, false);
        $result = ['all' => __('all')] + $result;

        return $result;
    }

    public function getAllSampleIdPermissibleValues()
    {
        return $this->getSamplesPermissibleValues(true, false, true, false);
    }

    public function getAllSampleTypePermissibleValues()
    {
        return $this->getSamplesPermissibleValues(false, false, true, false);
    }

    public function getAllDerivatedTypePermissibleValues()
    {
        return $this->getSamplesPermissibleValues(false, false, true, false, true);
    }

    /**
     *
     * @param $byId
     * @param $onlySpecimen
     * @param bool $dontLimitToSamplesThatCanBeParents
     * @return array
     */
    public function getSamplesPermissibleValues($byId, $onlySpecimen, $dontLimitToSamplesThatCanBeParents = true, $flagActive = true, $justDrivated = false)
    {
        $result = array();

        // Build tmp array to sort according translation
        App::uses("ParentToDerivativeSampleControl", "Plugin/InventoryManagement/Model");
        $this->ParentToDerivativeSampleControl = new ParentToDerivativeSampleControl();
        $conditions = array();
        if ($flagActive){
            $conditions = array(
                'ParentToDerivativeSampleControl.flag_active' => true
            );
        }
        if ($onlySpecimen) {
            $conditions['DerivativeControl.sample_category'] = 'specimen';
        }elseif ($justDrivated){
            $conditions['DerivativeControl.sample_category'] = 'derivative';
        }
        $controls = null;
        $modelName = null;
        if ($dontLimitToSamplesThatCanBeParents) {
            $modelName = 'DerivativeControl';
            $controls = $this->ParentToDerivativeSampleControl->find('all', array(
                'conditions' => $conditions,
                'fields' => array(
                    'DerivativeControl.*'
                )
            ));
        } else {
            $modelName = 'ParentSampleControl';
            $conditions['NOT'] = array(
                'ParentToDerivativeSampleControl.parent_sample_control_id' => null
            );
            $controls = $this->ParentToDerivativeSampleControl->find('all', array(
                'conditions' => $conditions,
                'fields' => array(
                    'ParentSampleControl.id',
                    'ParentSampleControl.sample_type'
                )
            ));
        }

        if ($byId) {
            foreach ($controls as $control) {
                $result[$control[$modelName]['id']] = __($control[$modelName]['sample_type']);
            }
        } else {
            foreach ($controls as $control) {
                $result[$control[$modelName]['sample_type']] = __($control[$modelName]['sample_type']);
            }
        }
        natcasesort($result);

        return $result;
    }

    /**
     * Gets a list of sample types that could be created from a sample type.
     *
     * @param $parentId
     * @return array of allowed aliquot types stored into the following array:
     *         array('aliquot_control_id' => 'aliquot_type')
     * @internal param ID $sampleControlId of the sample control linked to the studied sample.* of the sample control linked to the studied sample.
     *
     * @author N. Luc
     * @since 2009-11-01
     * @author FMLH 2010-08-04 (new flag_active policy)
     */
    public function getPermissibleSamplesArray($parentId)
    {
        $conditions = array(
            'ParentToDerivativeSampleControl.flag_active' => true
        );
        if ($parentId == null) {
            $conditions[] = 'ParentToDerivativeSampleControl.parent_sample_control_id IS NULL';
        } else {
            $conditions['ParentToDerivativeSampleControl.parent_sample_control_id'] = $parentId;
        }

        $this->ParentToDerivativeSampleControl = AppModel::getInstance("InventoryManagement", "ParentToDerivativeSampleControl", true);
        $controls = $this->ParentToDerivativeSampleControl->find('all', array(
            'conditions' => $conditions,
            'fields' => array(
                'DerivativeControl.*'
            )
        ));
        $specimenSampleControlsList = array();
        foreach ($controls as $control) {
            $specimenSampleControlsList[$control['DerivativeControl']['id']]['SampleControl'] = $control['DerivativeControl'];
        }
        return $specimenSampleControlsList;
    }

    /**
     *
     * @param mixed $results
     * @param bool $primary
     * @return mixed
     */
    public function afterFind($results, $primary = false)
    {
        return $this->applyMasterFormAlias($results, $primary);
    }
    
    public function setDataBeforeSaveFB(&$data)
    {
        $prefix = getPrefix();
        $model = $this->name;
        $maxDisplayOrder = $this->find("first", array(
            'fields'=>array("display_order"),
            'order'=>array("display_order DESC")
        ));

        if (!isset($data[$model][$this->controlType])){
            return;
        }

        $name = $data[$model][$this->controlType];
        $detailTableName = createTableName($prefix . 'sm', time(), $name);

        $data[$model]["detail_tablename"] = strtolower($detailTableName);
        $data[$model]["detail_form_alias"] = $detailTableName;
        $data[$model]["flag_active"] = 0;
        $data[$model]["display_order"] = empty($maxDisplayOrder[$model]["display_order"]) ? 1 : $maxDisplayOrder[$model]["display_order"] + 1;
        $data[$model]["flag_test_mode"] = '1';
        $this->addWritableField();
    }

    public function setDataBeforeEditFB(&$data)
    {
        
        $controlItem = $this->getOrRedirectFB($this->id);
        $this->dataToDic($controlItem);

        if (isset($data[$this->name]) && !empty($data[$this->name])){
            foreach ($data[$this->name] as $key=>$value) {
                $controlItem[$this->name][$key] = $value;
            }
        }
        $data[$this->name] = $controlItem[$this->name];

        
        if (empty($data[$this->name]['detail_form_alias']) && !empty($data['detailTest'])){
            $prefix = getPrefix();
            $model = $this->name;

            if (!isset($data[$model][$this->controlType])){
                return;
            }
            $name = $data[$model][$this->controlType];
            $detailTableName = createTableName($prefix . 'sm', time(), $name);

            $data[$model]["detail_form_alias"] = (!empty(trim($data[$model]["detail_form_alias"]))) ? implode(",", array_merge(explode(",", $data[$model]["detail_form_alias"]), array($detailTableName))) : $detailTableName;
            $this->addWritableField();

        }
    }
     
    public function validates($options = array()) 
    {
        $data = $this->find('all', array(
            'conditions' => array(
                $this->name.'.'.$this->controlType.' LIKE ' => '% || '.$this->data[$this->name][$this->controlType]
            )
        ));

        if (!empty($data) && (( countCustom($data) ==1 && $data[0][$this->name]['id'] != $this->id) || countCustom($data) > 1)){
            $this->validationErrors[$this->controlType][] = __("this type has already existed");
        }
        
        return parent::validates($options);
    }
    
    public function addWritableField($field = array(), $tablename = null)
    {
        if (!is_array($field)){
            $field = array($field);
        }
        parent::addWritableField(array_merge($field, array("sample_type", "sample_category", "detail_form_alias", "detail_tablename", "display_order", "flag_active", "flag_test_mode", "flag_form_builder", "flag_active_input")), $tablename);
    }

    public function getName()
    {
        return __('sample');
    }

    public function dataToDic(&$data)
    {
        if (isset($data[$this->name])){
            if (isset($data[$this->name][$this->controlType])){
                $data[$this->name][$this->controlType.'_id'] = $data[$this->name][$this->controlType];
            }
        }else{
            foreach ($data as &$value) {
                if (isset($value[$this->name][$this->controlType])){
                    $value[$this->name][$this->controlType.'_id'] = $value[$this->name][$this->controlType];
                }
            }
        }
    }

    public function getSampleTypeAndDerivatedOrRedirect($id, $filter = true)
    {
        if (!is_array($id)){
            $id = [$id];
        }

        $parentToDerivativeSampleControlModel = new Model(array(
                'table' => 'parent_to_derivative_sample_controls',
                'name' => 'ParentToDerivativeSampleControl'
            ));

        $conditions = [
            $this->alias . "." . $this->primaryKey => $id
        ];

        $joins =
            [
                [
                    'table' => 'parent_to_derivative_sample_controls',
                    'alias' => 'ParentToDerivativeSampleControl2',
                    'type' => 'LEFT',
                    'conditions' => [
                        'ParentToDerivativeSampleControl.derivative_sample_control_id = ParentToDerivativeSampleControl2.parent_sample_control_id'
                    ],
                ],[
                'table' => 'sample_controls',
                'alias' => 'SampleControl',
                'type' => 'LEFT',
                'conditions' => [
                    'ParentToDerivativeSampleControl.derivative_sample_control_id = SampleControl.id'
                ],
            ],[
                'table' => 'sample_controls',
                'alias' => 'SampleControlDerivate',
                'type' => 'LEFT',
                'conditions' => [
                    'ParentToDerivativeSampleControl.parent_sample_control_id = SampleControlDerivate.id'
                ],
            ],[
                'table' => 'sample_controls',
                'alias' => 'ParentSample',
                'type' => 'LEFT',
                'conditions' => [
                    'ParentToDerivativeSampleControl2.derivative_sample_control_id = ParentSample.id'
                ],
            ],[
                'table' => 'aliquot_controls',
                'alias' => 'AliquotControl',
                'type' => 'LEFT',
                'conditions' => [
                    'AliquotControl.sample_control_id = SampleControl.id'
                ],
            ],
            ];

        $fields =
            [
                "SampleControl.id AS sample_control_id",
                "SampleControl.sample_type AS sample_type",
                "SampleControl.sample_category AS category",
                "GROUP_CONCAT(ParentToDerivativeSampleControl.flag_active) flag_active",
                "GROUP_CONCAT(DISTINCT SampleControlDerivate.sample_type) AS parent_sample_type",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (ParentToDerivativeSampleControl2.flag_active = 1 , ParentSample.sample_type, -1)), ',-1', ''), '-1,', ''), '-1', '') AS derivative_sample_type",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (ParentToDerivativeSampleControl2.flag_active = 0 , ParentSample.sample_type, -1)), ',-1', ''), '-1,', ''), '-1', '') AS derivative_sample_type_inactive",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (ParentToDerivativeSampleControl2.flag_active = 1 , ParentSample.id, -1)), ',-1', ''), '-1,', ''), '-1', '') AS derivative_sample_id",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (ParentToDerivativeSampleControl2.flag_active = 0 , ParentSample.id, -1)), ',-1', ''), '-1,', ''), '-1', '') AS derivative_sample_id_inactive",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (AliquotControl.flag_active = 1, AliquotControl.aliquot_type, -1)), ',-1', ''), '-1,', ''), '-1', '') AS aliquot_type",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (AliquotControl.flag_active = 0, AliquotControl.aliquot_type, -1)), ',-1', ''), '-1,', ''), '-1', '') AS aliquot_type_inactive ",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (AliquotControl.flag_active = 1, AliquotControl.id, -1)), ',-1', ''), '-1,', ''), '-1', '') AS aliquot_id",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (AliquotControl.flag_active = 0, AliquotControl.id, -1)), ',-1', ''), '-1,', ''), '-1', '') AS aliquot_id_inactive ",
            ];

        $order =
            [
                "sample_control_id"
            ];

        $group =
            [
                "sample_type",
                "category",
            ];

        if (!$filter){
            $joins =
                [
                    [
                    'table' => 'sample_controls',
                    'alias' => 'SampleControl',
                    'type' => 'LEFT',
                    'conditions' => [
                        'ParentToDerivativeSampleControl.derivative_sample_control_id = SampleControl.id'
                    ],
                ],
            ];

            $fields = [
                "SampleControl.*",
                "ParentToDerivativeSampleControl.*",
            ];

            $group = [];

            $order =
                [
                ];

        }

        $data = $parentToDerivativeSampleControlModel->find('all', [
            'conditions' => $conditions,
            'joins' => $joins,
            'fields' => $fields,
            'group' => $group,
            'order' => $order,
        ]);

        if (!empty($data)) {
            return $data;
        }else{
            $bt = debug_backtrace();
            AppController::getInstance()->redirect('/Pages/err_plugin_no_data?method=' . $bt[1]['function'] . ',line=' . $bt[0]['line'], null, true);
            return null;
        }
    }

    public function getDerivativeSampleType()
    {
        return $this->getSampleType(true, false);
    }

    public function getAllSampleType()
    {
        return $this->getSampleType();
    }

    public function getSampleType($derivative = true, $specimen = true)
    {
        $result = [];
        $conditions = [];

        if ($derivative && $specimen){
            // Show all the sample types
        }elseif ($derivative){
            $conditions[$this->alias . ".sample_category"] = "derivative";
        }elseif ($specimen){
            $conditions[$this->alias . ".sample_category"] = "specimen";
        }else{
            // Show all the sample types
        }

        $data = $this->find('all', [
            'conditions' => $conditions
        ]);

        foreach ($data as $datum){
            $result[$datum['SampleControl']['sample_type']] = __($datum['SampleControl']['sample_type']);
        }
        natcasesort($result);
        return $result;
    }

}