<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class AliquotControl
 */
class InventoryActionControl extends InventoryManagementAppModel
{

    public $controlType = 'type';

    public $masterFormAlias = 'inventory_action_masters';

    public static $listCommonInventoryAction = array();

    public function listCommonInventoryAction()
    {
        return self::$listCommonInventoryAction;
    }

    public function actionControlInit()
    {
        $updateQuery = "UPDATE inventory_action_controls SET s_ids = '%s' , a_ids = '%s' WHERE id = '%s'";

        $allActiveActionControlsQuery = "SELECT * FROM inventory_action_controls WHERE flag_active = 1 and flag_test_mode = 0 ORDER BY id";
        $allActiveActionControls = $this->query($allActiveActionControlsQuery);

        $allActiveAliquotControlsQuery = "SELECT id FROM aliquot_controls WHERE flag_active = 1 ORDER BY id";
        $allActiveAliquotControlIds = $this->query($allActiveAliquotControlsQuery);
        $allActiveAliquotControlIds = Hash::extract($allActiveAliquotControlIds, '{n}.aliquot_controls.id');

        $allActiveAliquotSampleControlsQuery = "SELECT sample_control_id sid, GROUP_CONCAT(id) aids FROM aliquot_controls WHERE flag_active = 1 GROUP BY sample_control_id";
        $allActiveAliquotSampleControlsTemp = $this->query($allActiveAliquotSampleControlsQuery);

        $allActiveAliquotSampleControls = array();
        foreach ($allActiveAliquotSampleControlsTemp as $allActiveAliquotSampleControl) {
            $allActiveAliquotSampleControls[$allActiveAliquotSampleControl['aliquot_controls']['sid']] = explode(",", $allActiveAliquotSampleControl[0]['aids']);
        }

        foreach ($allActiveActionControls as $actionControl){
            extract(AppController::convertArrayKeyFromSnakeToCamel($actionControl['inventory_action_controls']));
            $aliquotControlIds = preg_replace('/\s+/', '', $aliquotControlIds);
            $sampleControlIds = preg_replace('/\s+/', '', $sampleControlIds);
            $sids = array();
            $aids = array();
            $applyOn = trim(strtolower($applyOn));
            if ($applyOn == 'aliquots'){
                if ($aliquotControlIds == 'all'){
                    $aids = $allActiveAliquotControlIds;
                }elseif (!empty($aliquotControlIds)){
                    $aids = explode("|||", $aliquotControlIds);
                }
            }elseif ($applyOn == 'aliquots related to samples'){
                if ($sampleControlIds == 'all'){
                    $aids = $allActiveAliquotControlIds;
                }elseif (!empty($sampleControlIds)){
                    $tempIds = explode("|||", $sampleControlIds);
                    foreach($tempIds as $i){
                        $aids = array_merge($aids, $allActiveAliquotSampleControls[$i]);
                    }
                }
            }elseif ($applyOn == 'samples'){
                if ($sampleControlIds == 'all'){
                    $sids = array_keys($allActiveAliquotSampleControls);
                }elseif (!empty($sampleControlIds)){
                    $sids = explode("|||", $sampleControlIds);
                }
            }elseif ($applyOn == 'samples and all related aliquots'){
                if ($sampleControlIds == 'all'){
                    $aids = $allActiveAliquotControlIds;
                    $sids = array_keys($allActiveAliquotSampleControls);
                }elseif (!empty($sampleControlIds)){
                    $sids = explode("|||", $sampleControlIds);
                    foreach($sids as $i){
                        $aids = array_merge($aids, $allActiveAliquotSampleControls[$i]);
                    }
                }
            }elseif ($applyOn == 'samples and some related aliquots'){
                if ($sampleControlIds == 'all'){
                    $sids = array_keys($allActiveAliquotSampleControls);
                }elseif (!empty($sampleControlIds)){
                    $sids = explode("|||", $sampleControlIds);
                }
                if ($aliquotControlIds == 'all'){
                    foreach($sids as $i){
                        $aids = array_merge($aids, $allActiveAliquotSampleControls[$i]);
                    }
                }elseif (!empty($aliquotControlIds)){
                    $aids = explode("|||", $aliquotControlIds);
                }
            }elseif ($applyOn == 'samples and aliquots not necessarily related'){
                if ($sampleControlIds == 'all'){
                    $sids = array_keys($allActiveAliquotSampleControls);
                }elseif (!empty($sampleControlIds)){
                    $sids = explode("|||", $sampleControlIds);
                }
                if ($aliquotControlIds == 'all'){
                    $aids = $allActiveAliquotControlIds;
                }elseif (!empty($aliquotControlIds)){
                    $aids = explode("|||", $aliquotControlIds);
                }
            }
                $this->query(sprintf($updateQuery, implode(",", $sids), implode(",", $aids), $id));
        }

    }

    public function getAliquotSampleActionLinks($aliquotData)
    {
    }

    public function getSampleActionLinks($sampleData)
    {
        $actionControls = $this->getAllActiveActions();
        $sampleMasterId = $sampleData['SampleMaster']['id'];
        $sid = $sampleData['SampleControl']['id'];
        $ids = array();
        foreach ($actionControls as $actionControl) {
            $actionControl = current($actionControl);
            $sids = explode(",", $actionControl['s_ids']);
            if (in_array($sid, $sids)!==false){
                $ids[] = $actionControl;
            }
        }

        $links = array();
        foreach ($ids as $id){
            $links[$this->getHeaderToDisplay($id)] = array(
                'link' => "/InventoryManagement/InventoryActionMasters/add/sample/" . $id['id'] . "/" . $sampleMasterId,
                'icon' => 'use'
            );
        }
        return $links;
    }

    public function getAliquotActionLinks($aliquotData)
    {
        $actionControls = $this->getAllActiveActions();
        $aliquotMasterId = $aliquotData['AliquotMaster']['id'];
        $aid = $aliquotData['AliquotControl']['id'];
        $ids = array();
        foreach ($actionControls as $actionControl) {
            $actionControl = current($actionControl);
            $aids = explode(",", $actionControl['a_ids']);
            if (in_array($aid, $aids)!==false){
                $ids[] = $actionControl;
            }
        }

        $links = array();
        foreach ($ids as $id){
            $links[$this->getHeaderToDisplay($id)] = array(
                'link' => "/InventoryManagement/InventoryActionMasters/add/aliquot/" . $id['id'] . "/" . $aliquotMasterId,
                'icon' => 'use'
            );
            ksort($links);
        }
        return $links;
    }
    
    public function getHeaderToDisplay($controlData) {
        if (isset($controlData['InventoryActionControl'])) {
            $controlData = $controlData['InventoryActionControl'];
        }
        if (array_key_exists('type', $controlData) && array_key_exists('category', $controlData)) {
            $category = null;
            if ($controlData['category'] != $controlData['type']) {
                $structureValueDomainModel = AppModel::getInstance('', 'StructureValueDomain', true);                
                $category = $structureValueDomainModel->getValueToDisplay('inventory_action_categories', $controlData['category']);
            }
            return is_null($category)? __($controlData['type']) : $category  . ' - ' . __($controlData['type']);
        } else {
            return '';
        }
    }

    private function hasAliquotAction($applyOn)
    {
        return $this->determinActionApplyOnType($applyOn, 'aliquot');
    }

    private function hasSampleAction($applyOn)
    {
        return $this->determinActionApplyOnType($applyOn, 'sample');
    }

    private function determinActionApplyOnType($applyOn, $type)
    {
        if ($type == 'sample') {
            return in_array($applyOn, array_merge($this->applyOnList['samples'], $this->applyOnList['all'])) !== false;
        } elseif ($type = 'aliquot') {
            return in_array($applyOn, array_merge($this->applyOnList['aliquots'], $this->applyOnList['all'])) !== false;
        }
    }

    public function getAllActiveActions($type = [])
    {
        $conditions = [
            'flag_active' => 1,
            'flag_test_mode' => 0,
            'flag_active_input' => 1,
        ];
        if (!empty($type)){
            $applyOn = [];
            if(in_array('aliquot', $type)){
                $applyOn = array_merge($applyOn, [
                    'aliquots',
                    'aliquots related to samples',
                    'samples and all related aliquots',
                    'samples and some related aliquots',
                    'samples and aliquots not necessarily related',
                ]);
            }
            if(in_array('sample', $type)){
                $applyOn = array_merge($applyOn, [
                    'samples',
                    'samples and all related aliquots',
                    'samples and some related aliquots',
                    'samples and aliquots not necessarily related',
                ]);
            }
            $applyOn = array_unique($applyOn);
            $conditions['apply_on'] = $applyOn;
        }

        $result = $this->find('all', array(
            'conditions' => $conditions,
            'orders' => array(
                'display_order'
            )
        ));
        return $result;
    }

    public function setDataBeforeSaveFB(&$data)
    {
        $prefix = getPrefix();
        $model = $this->name;

        $maxDisplayOrder = $this->find("first", array(
            'fields'=>array("display_order"),
            'order'=>array("display_order DESC")
        ));

        if (!isset($data[$model][$this->controlType])){
            return;
        }

        $name = $data[$model][$this->controlType];
        $detailTableName = createTableName($prefix . 'ia', time(), $name);

        $data[$model]["detail_tablename"] = strtolower($detailTableName);
        $data[$model]["detail_form_alias"] = $detailTableName;
        $data[$model]["flag_active"] = 0;
        $data[$model]["display_order"] = empty($maxDisplayOrder[$model]["display_order"]) ? 1 : $maxDisplayOrder[$model]["display_order"] + 1;
        $data[$model]["flag_test_mode"] = '1';

        if (is_string($data[$model]['aliquot_control_ids'])){
            $data[$model]['aliquot_control_ids'] = explode("|||", $data[$model]['aliquot_control_ids']);
        }
        if (is_string($data[$model]['sample_control_ids'])){
            $data[$model]['sample_control_ids'] = explode("|||", $data[$model]['sample_control_ids']);
        }

        if (empty($data[$model]['aliquot_control_ids'])){
            $data[$model]['aliquot_control_ids'] = [];
        }elseif(in_array('all', $data[$model]['aliquot_control_ids']) !== false){
            $data[$model]['aliquot_control_ids'] = ['all'];
        }else{
            $data[$model]["aliquot_control_ids"] =  array_filter($data[$model]["aliquot_control_ids"]);
        }

        if (empty($data[$model]['sample_control_ids'])) {
            $data[$model]['sample_control_ids'] = [];
        }elseif (in_array('all', $data[$model]['sample_control_ids']) !== false){
            $data[$model]['sample_control_ids'] = ['all'];
        }else{
            $data[$model]['sample_control_ids'] = array_filter($data[$model]['sample_control_ids']);
        }
        $this->addWritableField();
    }

    public function setDataBeforeEditFB(&$data)
    {
        $controlItem = $this->getOrRedirectFB($this->id);

        $model = $this->name;

        $this->dataToDic($controlItem);
        if (isset($data[$model]) && !empty($data[$model])){
            foreach ($data[$model] as $key=>$value) {
                $controlItem[$model][$key] = $value;
            }
        }
        $data[$model] = $controlItem[$model];

        if (empty($data[$model]['detail_form_alias']) && !empty($data['detailTest'])){
            $prefix = getPrefix();

            if (!isset($data[$model][$this->controlType])){
                return;
            }
            $name = $data[$model][$this->controlType];
            $detailTableName = createTableName($prefix . 'ia', time(), $name);

            $data[$model]["detail_form_alias"] = (!empty(trim($data[$model]["detail_form_alias"]))) ? implode(",", array_merge(explode(",", $data[$model]["detail_form_alias"]), array($detailTableName))) : $detailTableName;

        }

        if ($controlItem['InventoryActionControl']['flag_test_mode']){
            if (empty($data[$model]['aliquot_control_ids'])){
                $data[$model]['aliquot_control_ids'] = [];
            }elseif(in_array('all', $data[$model]['aliquot_control_ids']) !== false){
                $data[$model]['aliquot_control_ids'] = ['all'];
            }else{
                $data[$model]["aliquot_control_ids"] =  array_filter($data[$model]["aliquot_control_ids"]);
            }

            if (empty($data[$model]['sample_control_ids'])) {
                $data[$model]['sample_control_ids'] = [];
            }elseif (in_array('all', $data[$model]['sample_control_ids']) !== false){
                $data[$model]['sample_control_ids'] = ['all'];
            }else{
                $data[$model]['sample_control_ids'] = array_filter($data[$model]['sample_control_ids']);
            }
        }

        $this->addWritableField();
    }

    public function validates($options = array())
    {
        $valid = true;

        $data = $this->find('all', array(
            'conditions' => array(
                $this->name.'.'.$this->controlType.' LIKE ' => '% || '.$this->data[$this->name][$this->controlType]
            )
        ));

        if (!empty($data) && (( countCustom($data) ==1 && $data[0][$this->name]['id'] != $this->id) || countCustom($data) > 1)){
            $this->validationErrors[$this->controlType][] = __("this type has already existed");
        }

        $applyOn = $this->data[$this->name]['apply_on'];
        $aliquotControlIds = array_filter(explode("|||", $this->data[$this->name]['aliquot_control_ids']));
        $sampleControlIds = array_filter(explode("|||", $this->data[$this->name]['sample_control_ids']));

        if ($applyOn == 'aliquots'){
            if (empty($aliquotControlIds)){
                $valid = false;
                $this->validationErrors['aliquot_control_ids'][] = __('please select some aliquots');
            }
        }elseif ($applyOn == 'aliquots related to samples' || $applyOn == 'samples' || $applyOn == 'samples and all related aliquots' || $applyOn == 'samples and some related aliquots'){
            if (empty($sampleControlIds)) {
                $valid = false;
                $this->validationErrors['sample_control_ids'][] = __('please select some sample types');
            }
        }elseif ($applyOn == 'samples and aliquots not necessarily related'){
            if (empty($sampleControlIds) && empty($aliquotControlIds)) {
                $valid = false;
                $this->validationErrors['sample_control_ids'][] = __('please select some sample or aliquot types');
                $this->validationErrors['aliquot_control_ids'][] = __('please select some sample or aliquot types');
            }
        }

        return parent::validates($options) && $valid;
    }

    public function addWritableField($field = array(), $tablename = null)
    {
        if (!is_array($field)){
            $field = array($field);
        }
        parent::addWritableField(array_merge($field, array("type", "category", "apply_on", "sample_control_ids", "aliquot_control_ids", "detail_form_alias", "detail_tablename", "display_order", "flag_active", "flag_link_to_study", "flag_link_to_storage", "flag_test_mode", "flag_form_builder", "flag_active_input", "flag_batch_action")), $tablename);
    }

    public function getName()
    {
        return __('inventory action');
    }

    public function getActiveTypesPermissibleValues()
    {
        $result = $this->getAllActiveActions();
        $orderedResults = array();
        foreach ($result as $newResult) {
            $orderedResults[$newResult['InventoryActionControl']['type']] = __($newResult['InventoryActionControl']['type']);
        }
        natcasesort($orderedResults);
        return $orderedResults;
    }

    public function getActiveAliquotTypesPermissibleValues()
    {
        $result = $this->getAllActiveActions(['aliquot']);
        $orderedResults = array();
        foreach ($result as $newResult) {
            $orderedResults[$newResult['InventoryActionControl']['type']] = __($newResult['InventoryActionControl']['type']);
        }
        natcasesort($orderedResults);
        return $orderedResults;
    }

    public function getActiveAliquotSampleTypesPermissibleValues()
    {
        $result = $this->getAllActiveActions(['sample', 'aliquot']);
        $orderedResults = array();
        foreach ($result as $newResult) {
            $orderedResults[$newResult['InventoryActionControl']['type']] = __($newResult['InventoryActionControl']['type']);
        }
        natcasesort($orderedResults);
        return $orderedResults;
    }
}