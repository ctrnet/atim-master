<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class InventoryActionMastersController
 */
class InventoryActionMastersController extends InventoryManagementAppController
{
    public $components = array(
        'InventoryManagement.InventoryActions'
    );

    public $initialDisplay;
    public $masterModel;
    public $controlId;
    public $ids;

    public $errors;

    public $uses = array(
        'InventoryManagement.InventoryActionControl',
        'InventoryManagement.InventoryActionMaster',
        'InventoryManagement.InventoryActionDetail',
        'InventoryManagement.AliquotMaster',
        'InventoryManagement.SampleMaster',
        'InventoryManagement.ViewAliquot',
        'InventoryManagement.ViewAliquotUse',
    );

    public function selectActionTypePage ($batch = false)
    {
        InventoryActionControl::$listCommonInventoryAction = $this->InventoryActions->initSelectActionType($this->request->data, $batch);
        $this->set('atimMenu', $this->Menus->get('/InventoryManagement/Collections/search'));
        $links['cancel'] = $this->InventoryActions->urlToCancel();
        if (empty(InventoryActionControl::$listCommonInventoryAction)) {
            $this->atimFlashWarning(__('no inventory event/annotation type can be selected for the selected items and the launched action'), $links['cancel']);
        }
        $_SESSION['batch_action_url_to_cancel'] = $links['cancel'];
        $links['submit'] = !$batch ? "/InventoryManagement/InventoryActionMasters/add/{$this->inventoryActionType}" : "/InventoryManagement/InventoryActionMasters/addOneEventInBatchToMany/{$this->inventoryActionType}";
        $this->set('links', $links);

        if (empty($this->request->data)){
            $this->atimFlashError(__('you have been redirected automatically'). ' (#' . __LINE__ . ')', $links['cancel']);
        }

        $this->Structures->set('inventory_action_selection_type', 'inventory_action_selection_structure');
        $this->set('type', $this->inventoryActionType);
        $this->set('nodeId', isset($this->request->data['node']['id'])? $this->request->data['node']['id'] : null);
        $this->set('batchSetId', isset($this->request->data['BatchSet']['id'])? $this->request->data['BatchSet']['id'] : null);
        
        $hookLink = $this->hook('format');
        if ($hookLink) {
            require ($hookLink);
        }
    }

    public function add($type = null, $controlId = null, $dataId = null)
    {
        $this->errors = array();

        $this->InventoryActions->initialAdd($type, $controlId, $dataId);
        if (!$this->InventoryActions->checkIEAControlId($type, $controlId, $dataId)){
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }

        $actionControlData = $this->InventoryActionControl->getOrRedirect($controlId);
        $this->set('actionControlData', $actionControlData);

        // AVOID USE OF URL TO ADD WHEN ADD BUTTON IS HIDDEN
        if ($actionControlData['InventoryActionControl']['flag_active'] != "1"
            || $actionControlData['InventoryActionControl']['flag_active_input'] != "1"
            || $actionControlData['InventoryActionControl']['flag_test_mode'] == "1") {
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }

        $actionDetailAlias = $actionControlData[$this->InventoryActionControl->name]['form_alias'];
        $this->set('type', $type);
        if (isset($this->request->data['node']['id']) && !empty($this->request->data['node']['id'])){
            $this->set('nodeId', $this->request->data['node']['id']);
        }
        if (isset($this->request->data['BatchSet']['id']) && !empty($this->request->data['BatchSet']['id'])){
            $this->set('batchSetId', $this->request->data['BatchSet']['id']);
        }
        
        $maxInputVarsValidation = (isset($this->request->data['max_input_vars_validation']) ? true : false);
        unset($this->request->data['max_input_vars_validation']);

        $parentData = $this->masterModel->find('all', array(
            'conditions' => array(
                $this->masterModel->name . '.id' => $this->ids
            )
        ));
        $modelIds = Hash::extract($parentData, "{n}.{$this->masterModel->name}.{$this->masterModel->primaryKey}");
        $diffId = array_diff($this->ids, $modelIds);

        $links['cancel'] = $this->InventoryActions->urlToCancel($type, $dataId, $parentData);
        
        $links['submit'] = "/InventoryManagement/InventoryActionMasters/add/$type/$controlId/$dataId";
        $this->set('links', $links);
        if (!empty($diffId)) {
            $this->atimFlashError(__('there are some invalid ids (%s)', implode(", ", $diffId)), $links['cancel']);
        }
        if (empty($this->ids)) {
            $this->atimFlashError(__('there is no selected %s', __($type)), $links['cancel']);
        }
        //Set Action Menus
        extract($this->InventoryActions->getAddMenus($type, $dataId, $parentData));
        $this->set('atimMenu', $this->Menus->get($atimMenuLink));
        $this->set('atimMenuVariables', $atimMenuVariables);
        $this->set('idParameterSet', $dataId? true : false);

        $postData = $this->InventoryActions->formattingDataForAdd($parentData);
        
        $displayLimit = Configure::read('InventoryActionCreation_processed_items_limit');
        if (sizeof($postData) > $displayLimit) {
            $this->atimFlashWarning(__("batch init - number of submitted records too big") . " (" . sizeof($postData) . " > $displayLimit)", $links['cancel']);
            return;
        }

        $previousData = $this->request->data;
        $this->set('data', $postData);
        $actionDetailAlias .= ",inventory_action_sample_types_float";
        $this->Structures->set($actionDetailAlias, 'action_detail_alias');
        if ($type == 'aliquot') {
            $this->Structures->set('used_parent_label_for_displays,used_aliq_in_stock_details', 'action_parent_structure');
            $this->Structures->set($actionDetailAlias . ',inventory_action_volume', 'action_detail_alias_volume');
            $this->Structures->set($actionDetailAlias . ',inventory_action_study', 'action_detail_alias_study');
            $this->Structures->set($actionDetailAlias . ',inventory_action_study,inventory_action_volume', 'action_detail_alias_study_volume');
            $this->Structures->set('inventory_action_batch_process_aliq_storage_and_in_stock_details', 'action_apply_all');
        } elseif ($type == 'sample') {
            $this->Structures->set('used_parent_label_for_displays', 'action_parent_structure');
            $this->Structures->set($actionDetailAlias . ',inventory_action_study', 'action_detail_alias_study');
        }
        
        $this->set('controlHeader', $this->InventoryActionControl->getHeaderToDisplay($actionControlData));

        $hookLink = $this->hook('format');
        if ($hookLink) {
            require($hookLink);
        }

        if (!$this->initialDisplay) {
            
            if (! $maxInputVarsValidation) {
                // The submitted data is not complete. The max_input_vars variable is probably too small compared to the submitted data. Part of the data to save are missing. Please submit less data or contact your administrator.
                $this->atimFlashWarning(__("submitted data is not complete - max_input_vars issue"), $links['cancel']);
                return;
            }
            
            $iSUniqueErrors = [];
            foreach ($postData as $id => &$rowData) {
                $this->InventoryActions->cleanup($rowData);

                if ($type == 'aliquot' && !empty($rowData['parent']['AliquotControl']['volume_unit'])){
                    $this->masterModel->validatesVolume($rowData, $this->errors);
                }

                $this->masterModel->id = $id;

                $storageData = [
                    $rowData['parent']['AliquotMaster']['storage_master_id'] ?? null,
                    $rowData['parent']['FunctionManagement']['remove_from_storage'] ?? 0
                ];
                unset($rowData['parent']['AliquotMaster']['storage_master_id']);
                unset($rowData['parent']['FunctionManagement']['remove_from_storage']);
                $this->masterModel->set($rowData['parent']);
                $this->masterModel->validates();

                $rowData['parent']['AliquotMaster']['storage_master_id'] = $storageData[0];
                $rowData['parent']['FunctionManagement']['remove_from_storage'] = $storageData[1];

                if (!empty($this->masterModel->validationErrors)) {
                    $this->errors[$id][$this->masterModel->name] = $this->masterModel->validationErrors;
                }
                
                if (isset($rowData['children'])) {
                    if (empty($rowData['children'])) {
                        $this->errors[$id][0][$this->InventoryActionMaster->name]['title'][]  = __('at least one event/annotation has to be created per parent');
                    }
                    $rowCounter = 0;
                    $iSUniqueErrors[] = $this->InventoryActionMaster->checkUniqueGridFields($rowData['children']);
                    foreach ($rowData['children'] as $key => &$actionData) {
                        $this->InventoryActionMaster->data = array();
                        $this->InventoryActionMaster->set($actionData);
                        $this->InventoryActionMaster->validates();
                        $actionData = $this->InventoryActionMaster->data;
                        if (!empty($this->InventoryActionMaster->validationErrors)) {
                            $this->InventoryActionMaster->validationErrors = array_map('unserialize',array_diff(array_map('serialize', $this->InventoryActionMaster->validationErrors), array_map('serialize', $this->InventoryActionDetail->validationErrors)));
                            foreach ($this->InventoryActionMaster->validationErrors as $field => $messages) {
                                if (in_array($field, array('autocomplete_aliquot_internal_use_study_summary_id')) !== false) {
                                    $this->errors[$id][$rowCounter]['FunctionManagement'][$field] = $messages;
                                } else {
                                    $this->errors[$id][$rowCounter][$this->InventoryActionMaster->name][$field] = $messages;
                                }
                            }
                            foreach ($this->InventoryActionDetail->validationErrors as $field => $messages) {
                                $this->errors[$id][$rowCounter][$this->InventoryActionDetail->name][$field] = $messages;
                            }
                        }
                        $rowCounter ++;
                    }
                } else {
                    $this->errors[$id][0][$this->InventoryActionMaster->name]['title'][]  = __('at least one event/annotation has to be created per parent');
                }
            }
           
            $tempIsUniqueError = [];
            foreach ($iSUniqueErrors as $iSUniqueError){
                if (!empty($iSUniqueError)){
                    foreach ($iSUniqueError as $isUniqueField => $isUniqueMessages){
                        $tempIsUniqueError[] = json_encode([$isUniqueField => $isUniqueMessages]);
                    }
                }
            }

            $tempIsUniqueError = array_unique($tempIsUniqueError);
            $iSUniqueErrors = [];
            foreach ($tempIsUniqueError as $tempError) {
                $tempErrorArray = json_decode($tempError, true);
                foreach ($tempErrorArray as $fName => $value){
                    foreach ($value as $k => $v) {
                        $iSUniqueErrors[$fName][$k] = $v;
                    }

                }
            }

            if (!empty($iSUniqueErrors)){
                foreach ($postData as $id => $rowDataCheck) {
                    foreach ($rowDataCheck['children'] as $key => $actionDataTemp) {
                        foreach ($iSUniqueErrors as $isUniqueField => $isUniqueMessages){
                            foreach ($isUniqueMessages as $label => $isUniqueMsg) {
                                $value = explode("|||", $label)[1];
                                if (isset($actionDataTemp['InventoryActionDetail'][$isUniqueField]) && $actionDataTemp['InventoryActionDetail'][$isUniqueField] == $value){
                                    $this->errors[$id][$key]['InventoryActionDetail'][$isUniqueField][] = $isUniqueMsg;
                                }
                                if (isset($actionDataTemp['InventoryActionMaster'][$isUniqueField]) && $actionDataTemp['InventoryActionMaster'][$isUniqueField] == $value){
                                    $this->errors[$id][$key]['InventoryActionMaster'][$isUniqueField][] = $isUniqueMsg;
                                }
                            }
                        }
                    }
                }
            }

            $saveData = true;
          
            $hookLink = $this->hook('presave_process');
            if ($hookLink) {
                require($hookLink);
            }
            
            if (!empty($this->errors)) {
                $actionDetail1 = AppModel::getInstance('InventoryManagement', 'InventoryActionDetail');
                $actionDetail1->validationErrors = $this->InventoryActionMaster->validationErrors = array();
                $this->masterModel->validationErrors = $this->InventoryActions->formattingErrors();
            
            } elseif ($saveData) {
                
                AppModel::acquireBatchViewsUpdateLock();
                
                $tmpBatchSetIds = [
                    'parents' => [],
                    'children' => []
                ];
                
                foreach ($postData as $id => $unitData) {
                    
                    if (!empty($unitData['parent'])) {
                        $this->InventoryActions->preSaveProcessing($type, $unitData);
                        $this->masterModel->id = $id;
                        $tmpBatchSetIds['parents'][] = $id;
                        if ($type == 'aliquot' && ((!empty($unitData['parent'][$this->masterModel->name]['in_stock']) && $unitData['parent'][$this->masterModel->name]['in_stock'] == 'no') || (!empty($unitData['parent']['FunctionManagement']['remove_from_storage'])))) {
                            $unitData['parent'][$this->masterModel->name]['storage_master_id'] = null;
                            $unitData['parent'][$this->masterModel->name]['storage_coord_x'] = '';
                            $unitData['parent'][$this->masterModel->name]['storage_coord_y'] = '';
                            $this->masterModel->addWritableField(array(
                                'storage_master_id',
                                'storage_coord_x',
                                'storage_coord_y'
                            ));
                        }
                        if (isset($unitData['parent'][$this->masterModel->name]['current_volume'])){
                            $this->masterModel->addWritableField(array('current_volume'));
                        }
                        
                        $hookLink = $this->hook('presave_process_parent');
                        if ($hookLink) {
                            require($hookLink);
                        }
                        
                        if (! $this->masterModel->save($unitData['parent'], false)) {
                            $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                        }
                        
                    }

                    if (!empty($unitData['children'])) {
                        foreach ($unitData['children'] as $row) {
                            $this->InventoryActionMaster->addWritableField(array(
                                'inventory_action_control_id',
                                'sample_master_id',
                                'aliquot_master_id',
                                'initial_specimen_sample_id',
                                'study_summary_id',
                                'person',
                                'title'
                            ));

                            $this->InventoryActionMaster->id = null;
                            $hookLink = $this->hook('presave_process_children');
                            if ($hookLink) {
                                require($hookLink);
                            }
                            $this->InventoryActionMaster->data = $row;
                            if (! $this->InventoryActionMaster->save($row, false)) {
                                $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                            }
                            
                            $tmpBatchSetIds['children'][] = $this->InventoryActionMaster->getLastInsertId();
                        }
                    }
                }

                $urlToFlash = $links['cancel'];
                
                if (empty($dataId) && ($tmpBatchSetIds['children'] || $tmpBatchSetIds['parents'])) {
                    $datamartStructure = AppModel::getInstance("Datamart", "DatamartStructure", true);
                    $batchSetData = array(
                        'BatchSet' => array(
                            'datamart_structure_id' => $datamartStructure->getIdByModelName($tmpBatchSetIds['children']? 'InventoryActionMaster' : ($this->masterModel->name == 'AliquotMaster'? 'ViewAliquot' : 'ViewSample')),
                            'flag_tmp' => true
                        )
                    );
                    $batchSetModel = AppModel::getInstance('Datamart', 'BatchSet', true);
                    $batchSetModel->saveWithIds($batchSetData, $tmpBatchSetIds['children']? $tmpBatchSetIds['children'] : $tmpBatchSetIds['parents']);
                    $urlToFlash = '/Datamart/BatchSets/listAll/' . $batchSetModel->getLastInsertId();
                }

                $hookLink = $this->hook('postsave_process');
                if ($hookLink) {
                    require($hookLink);
                }
                
                AppModel::releaseBatchViewsUpdateLock();
                
                $this->atimFlashConfirm(__('your data has been saved'), $urlToFlash);
            }

        }
    }

    public function edit($actionMasterId)
    {
        $actionData= $this->InventoryActionMaster->find('first', array(
            'conditions' => array(
                $this->InventoryActionMaster->name . ".id" => $actionMasterId
            ),
        ));
        if (empty($actionData)){
            $actionData= $this->InventoryActionMaster->getOrRedirect($actionMasterId);
        }
        $type = (empty($actionData[$this->InventoryActionMaster->name]['aliquot_master_id'])) ? 'sample' : 'aliquot';
        $actionData = $this->InventoryActions->initialEdit($type, $actionData);
        $actionDetailAlias = explode(',', $actionData[$this->InventoryActionControl->name]['form_alias']);
        $this->set('actionControlData', $actionData[$this->InventoryActionControl->name]);
        $this->set('type', $type);
        if ($type == 'aliquot'){
            $links['cancel'] = "/InventoryManagement/AliquotMasters/detail/".$actionData['SampleMaster']['collection_id']."/".$actionData['SampleMaster']['id']."/".$actionData['InventoryActionMaster']['aliquot_master_id'];
        }else{
            $links['cancel'] = "/InventoryManagement/SampleMasters/detail/".$actionData['SampleMaster']['collection_id']."/".$actionData['SampleMaster']['id'];
        }
        $links['submit'] = "/InventoryManagement/InventoryActionMasters/edit/$actionMasterId";
        $this->set('links', $links);

        extract($this->InventoryActions->getEditMenus($type, $actionMasterId, $actionData));
        //Set Action Menus
        $this->set('atimMenu', $this->Menus->get($atimMenuLink));
        $this->set('atimMenuVariables', $atimMenuVariables);

        if ($type == 'aliquot') {
            if (!empty($actionData['AliquotControl']['volume_unit'])){
                $actionDetailAlias[]="inventory_action_volume";
            }
        }

        if (!empty($actionData['InventoryActionControl']['flag_link_to_study'])){
            $actionDetailAlias[]="inventory_action_study";
        }

        $actionDetailAlias = implode(',', $actionDetailAlias);

        $this->Structures->set($actionDetailAlias, 'final_structure');


        $hookLink = $this->hook('format');
        if ($hookLink) {
            require($hookLink);
        }

        if (!empty($this->request->data)){
            $submittedDataValidates = true;
            
            $this->InventoryActionMaster->data = array();
            $this->InventoryActionMaster->set($this->request->data);
            $this->InventoryActionMaster->validates();

            $hookLink = $this->hook('presave_process');
            if ($hookLink) {
                require($hookLink);
            }
            
            if (empty($this->InventoryActionMaster->validationErrors) && $submittedDataValidates) {
                $this->InventoryActionMaster->addWritableField(array('inventory_action_control_id', 'sample_master_id', 'aliquot_master_id', 'initial_specimen_sample_id', 'study_summary_id', 'person', 'title'));
                $this->InventoryActionMaster->id = $actionMasterId;
                $updateDone = true;
                if ($type == 'aliquot'){
                    $updateDone = $this->AliquotMaster->updateAliquotVolumeAfterEditAction($actionData, $this->request->data);
                }
                if ($updateDone){
                    if (! $this->InventoryActionMaster->save($this->InventoryActionMaster->data, false)) {
                        $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                    }
                }
                
                $hookLink = $this->hook('postsave_process');
                if ($hookLink) {
                    require($hookLink);
                }
                
                if ($updateDone){
                    $this->atimFlash(__('your data has been updated'), '/InventoryManagement/InventoryActionMasters/detail/' . $actionMasterId);
                }
            }
        }else{
            $this->request->data = $actionData;
            
            $hookLink = $this->hook('initial_display');
            if ($hookLink) {
                require ($hookLink);
            }
        }
    }

    public function detail($actionMasterId)
    {
        $actionData= $this->InventoryActionMaster->getOrRedirect($actionMasterId);

        $type = (empty($actionData[$this->InventoryActionMaster->name]['aliquot_master_id'])) ? 'sample' : 'aliquot';
        $actionData = $this->InventoryActions->initialDetail($type, $actionData);

        $this->request->data = $actionData;

        $links['edit'] = "/InventoryManagement/InventoryActionMasters/edit/$actionMasterId";
        $links['delete'] = "/InventoryManagement/InventoryActionMasters/delete/$actionMasterId";

        $this->set('links', $links);

        extract($this->InventoryActions->getDetailMenus($type, $actionMasterId, $actionData));
        //Set Action Menus
        $this->set('atimMenu', $this->Menus->get($atimMenuLink));
        $this->set('atimMenuVariables', $atimMenuVariables);

        $actionDetailAlias = explode(',', $actionData[$this->InventoryActionControl->name]['form_alias']);

        if ($type == 'aliquot') {
            if (!empty($actionData['AliquotControl']['volume_unit'])){
                $actionDetailAlias[]="inventory_action_volume";
            }
        }

        if (!empty($actionData['InventoryActionControl']['flag_link_to_study'])){
            $actionDetailAlias[]="inventory_action_study";
        }


        $actionDetailAlias = implode(',', $actionDetailAlias);

        $this->Structures->set($actionDetailAlias, 'final_structure');
        $this->set('controlHeader', $this->InventoryActionControl->getHeaderToDisplay($actionData));
        
        // CUSTOM CODE: FORMAT DISPLAY DATA
        $hookLink = $this->hook('format');
        if ($hookLink) {
            require ($hookLink);
        }

    }

    public function delete($actionMasterId)
    {
        $actionData= $this->InventoryActionMaster->getOrRedirect($actionMasterId);

        $type = (empty($actionData[$this->InventoryActionMaster->name]['aliquot_master_id'])) ? 'sample' : 'aliquot';
        if ($type == 'aliquot'){
            $flashUrl = '/InventoryManagement/AliquotMasters/detail/' . $actionData['AliquotMaster']['collection_id'] . '/' . $actionData['AliquotMaster']['sample_master_id'] . '/' . $actionData['AliquotMaster']['id'];
        }elseif($type == 'sample'){
            $flashUrl = '/InventoryManagement/SampleMasters/detail/' . $actionData['SampleMaster']['collection_id'] . '/' . $actionData['SampleMaster']['id'] . '/';
        }

        // LAUNCH DELETION
        
        // Check deletion is allowed
        $arrAllowDeletion = $this->InventoryActionMaster->allowDeletion($actionMasterId);
        
        $hookLink = $this->hook('delete');
        if ($hookLink) {
            require ($hookLink);
        }
        
        if ($arrAllowDeletion['allow_deletion']) {
            if ($this->InventoryActionMaster->atimDelete($actionMasterId)) {
                if ($actionData['InventoryActionMaster']['aliquot_master_id'] != null) {
                    $this->AliquotMaster->updateAliquotVolume($actionData['InventoryActionMaster']['aliquot_master_id']);
                }
                $hookLink = $this->hook('postsave_process');
                if ($hookLink) {
                    require ($hookLink);
                }
                $this->atimFlash(__('your data has been deleted'), $flashUrl);
            } else {
                $this->atimFlashError(__('error deleting data - contact administrator'), $flashUrl);
            }
        } else {
            $this->atimFlashWarning(__($arrAllowDeletion['msg']), $flashUrl);
        }
    }
    
    public function listAliquotSampleById($type, $id, $reportType = 'master')
    {
        if (!$this->request->is('ajax')) {
            $this->atimFlashError(__("You are not authorized to access that location."), 'javascript:history.back()');
        }
        $actionData = $this->InventoryActions->initIndex($type, $id);
        $this->set('reportType', $reportType);
        if ($reportType == 'master') {
            $this->Structures->set('inventory_action_masters', 'aliquot_masters_structures');
            $this->set('actionData', $actionData);
        } elseif ($reportType == 'detail') {
            $aliases = array();
            $data = array();
            foreach ($actionData as $actionDatum) {
                $tmpAlias = (!empty($actionDatum[$this->InventoryActionMaster->name]['used_volume'])) ? ",inventory_action_volume" : "";
                $aliases[$actionDatum['InventoryActionControl']['id']] = $actionDatum['InventoryActionControl']['form_alias'] . $tmpAlias;
                if (!empty($actionDatum['InventoryActionControl']['flag_link_to_study'])){
                    $aliases[$actionDatum['InventoryActionControl']['id']] .= ",inventory_action_study";
                }
                $data[$actionDatum['InventoryActionControl']['id']][] = $actionDatum;
            }
            foreach ($aliases as $key => $alias) {
                $this->Structures->set($alias, "detailAlias$key");
            }
            $this->set('actionData', $data);
        }
    }

    public function addOneEventInBatchToMany($type)
    {
        extract($this->InventoryActions->initSelectInventoryUseEventPage($type));
        $this->set('atimMenu', $this->Menus->get('/InventoryManagement/'));
        
        $displayLimit = Configure::read('InventoryActionCreation_processed_items_limit') * 5;
        if (sizeof(explode(',', $this->request->data[0]['aliquotSampleIds'])) > $displayLimit) {
            $this->atimFlashWarning(__("batch init - number of submitted records too big") . " (" . sizeof(explode(',', $this->request->data[0]['aliquotSampleIds'])) . " > $displayLimit)", $links['cancel']);
            return;
        }
        
        $controlData = $controlData['InventoryActionControl'];
        $aliases = $controlData['form_alias'] . ",inventory_action_selection_type_hidden";
        if ($type == 'aliquot'){
            $aliases .= ",iea_aliquot_master_edit_in_batchs";
        }
        if (!empty($controlData['flag_link_to_study'])){
            $aliases .= ",inventory_action_study";
        }
        $this->set('actionControlData', $controlData);

        $this->Structures->set($aliases, 'atim_add_one_event_in_batch_to_many');
        $dataForView = [
            'links' => $links,
        ];
        $this->set('dataForView', $dataForView);
        
        $hookLink = $this->hook('format');
        if ($hookLink) {
            require($hookLink);
        }
        
        if (!empty($this->request->data['InventoryActionMaster'])){
            $this->request->data['InventoryActionMaster']['inventory_action_control_id'] = $controlId;
            $this->InventoryActionMaster->set($this->request->data);

            $validate = $this->InventoryActionMaster->validates();
            $validate = $this->InventoryActions->IEAActionInBatchValidates($type) && $validate;
            if ($validate){
                if ($this->InventoryActionMaster->data['InventoryActionMaster']['study_summary_id']) {
                    foreach ($dataToSave as &$tmpDataToSave) {
                        $tmpDataToSave['InventoryActionMaster']['study_summary_id'] = $this->InventoryActionMaster->data['InventoryActionMaster']['study_summary_id'];
                    }
                }
                $this->InventoryActionMaster->data = [];
                $this->InventoryActionMaster->id = null;
                $this->InventoryActionMaster->addWritableField(['inventory_action_control_id','sample_master_id','aliquot_master_id','initial_specimen_sample_id','study_summary_id']);

                if ($type == 'aliquot'){
                    $aliquotBatchData = $this->InventoryActions->prepareAliquotBatchData($this->request->data['FunctionManagement'], $dataModelToSave);
                    unset($dataModelToSave);
                }

                $submittedDataValidates = true;
                
                $hookLink = $this->hook('presave_process');
                if ($hookLink) {
                    require($hookLink);
                }
                
                if ($submittedDataValidates) {
                    $dataSource = $this->InventoryActionMaster->getDataSource();
                    $dataSource->begin();

                    if (isset($aliquotBatchData['updateData']['storage_master_id']) && empty($aliquotBatchData['updateData']['storage_master_id'])) {
                        $aliquotBatchData['updateData']['storage_master_id'] = null;
                    }

                    if ($this->InventoryActionMaster->saveAll($dataToSave) && $type == 'aliquot' && $this->InventoryActions->saveAllAliquots($aliquotBatchData)){
    
                        $urlToFlash = $links['cancel'];
                        
                        if (isset($this->request->data[0]['aliquotSampleIds'])) {
                            $datamartStructure = AppModel::getInstance("Datamart", "DatamartStructure", true);
                            $batchSetData = array(
                                'BatchSet' => array(
                                    'datamart_structure_id' => $datamartStructure->getIdByModelName($type == 'aliquot'? 'ViewAliquot' : 'ViewSample'),
                                    'flag_tmp' => true
                                )
                            );
                            $batchSetModel = AppModel::getInstance('Datamart', 'BatchSet', true);
                            $batchSetModel->saveWithIds($batchSetData, explode(',', $this->request->data[0]['aliquotSampleIds']));
                            $urlToFlash = '/Datamart/BatchSets/listAll/' . $batchSetModel->getLastInsertId();
                        }
                                                
                        $hookLink = $this->hook('postsave_process');
                        if ($hookLink) {
                            require($hookLink);
                        }
    
                        $dataSource->commit();
                        $this->atimFlashConfirm(__('your data has been saved'), $urlToFlash);
                    } else {
                        $dataSource->rollback();
                        $this->redirect('/Pages/err_plugin_record_err?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                    }
                }
            }
        }
    }

    public function listAll($type, $id)
    {
        $links['urls'] = [];
        if (in_array($type, ['aliquot', 'specimenaliquot', 'samplealiquot'])) {
            $subtype = $type;
            $type = 'aliquot';
            
            $aliquotData = $this->ViewAliquot->getOrRedirect($id);

            // Menu
            $this->set('atimMenuVariables', [
                'Collection.id' => $aliquotData['ViewAliquot']['collection_id'],
                'SampleMaster.id' => $aliquotData['ViewAliquot']['sample_master_id'],
                'SampleMaster.initial_specimen_sample_id' => $aliquotData['SampleMaster']['initial_specimen_sample_id'],
                'AliquotMaster.id' => $id,
            ]);
            $menuLink = "/InventoryManagement/InventoryActionMasters/listAll/$subtype/%%AliquotMaster.id%%";
            $this->set('atimMenu', $this->Menus->get($menuLink));
            
            //data
            $data = $this->InventoryActionMaster->find('all', [
                'conditions' => [
                    $this->InventoryActionMaster->name . ".aliquot_master_id" => $id,
                ], 'fields' => [
                    'Distinct (InventoryActionControl.id)'
                ],
            ]);
            $data = Hash::extract($data, '{n}.InventoryActionControl.id');
            
            // links
            
            $historyConditions = [
                'aliquot_master_id' => $id,
                'OR' => [
                    'use_definition' => [
                        'realiquoted to',
                        'aliquot shipment',
                        'order preparation',
                        'shipped aliquot return',
                    ],
                    'use_definition LIKE ' => 'sample derivative creation#%',
                ],
            ];
            $this->request->data = $this->paginate($this->ViewAliquotUse, $historyConditions);
            // Realiquotings, Derivatives and Orders
            $historyConditions = [
                'aliquot_master_id' => $id,
                'OR' => [
                    'use_definition' => [
                        'realiquoted to',
                        'aliquot shipment',
                        'order preparation',
                        'shipped aliquot return',
                    ],
                    'use_definition LIKE ' => 'sample derivative creation#%',
                ],
            ];
            if ($this->ViewAliquotUse->find('count', ['conditions' => $historyConditions])) {
                $links['urls'][] = "InventoryManagement/InventoryActionMasters/listAllAjax/aliquot/$id/";
            }
            // Storage Event
            if (!empty($this->AliquotMaster->getStorageHistory($id))){
                $links['urls'][] = "InventoryManagement/InventoryActionMasters/listAllAjax/storage/$id";
            }
            // Inventory Actions
            foreach ($data as $actionControlId){
                $links['urls'][] = "InventoryManagement/InventoryActionMasters/listAllAjax/aliquot/$id/$actionControlId";
            }
            
            $links['back'] = "/InventoryManagement/AliquotMasters/detail/{$aliquotData['ViewAliquot']['collection_id']}/{$aliquotData['ViewAliquot']['sample_master_id']}/$id";
            
        } elseif ($type == 'sample') {
            $sampleData = $this->SampleMaster->getOrRedirect($id);
            
            // Menu
            $this->set('atimMenuVariables', [
                'Collection.id' => $sampleData['SampleMaster']['collection_id'],
                'SampleMaster.id' => $id,
                'SampleMaster.initial_specimen_sample_id' => $sampleData['SampleMaster']['initial_specimen_sample_id'],
            ]);
            $menuLink = '/InventoryManagement/InventoryActionMasters/listAll/sample/%%SampleMaster.id%%';
            $this->set('atimMenu', $this->Menus->get($menuLink));

            // Data
            $data = $this->InventoryActionMaster->find('all', [
                'conditions' => [
                    $this->InventoryActionMaster->name . ".sample_master_id" => $id,
                ], 'fields' => [
                    'DISTINCT InventoryActionControl.id',
                    'InventoryActionControl.type',
                    'InventoryActionControl.category',
                ],
            ]);

            $response = [];
            foreach ($data as $datum) {
                $response[$datum['InventoryActionControl']['id']] = $this->InventoryActionControl->getHeaderToDisplay($datum);
            }
            asort($response );

            // Links
            $links['back'] = "/InventoryManagement/SampleMasters/detail/{$sampleData['SampleMaster']['collection_id']}/$id";
            $links['filter'][__('summary')] = [
              'icon' => 'Inventory Action',
              'link' => "javascript:showDetailEventAnnotationForm('sample', $id, 'detailPage')",
            ];
            foreach ($response as $actionControlId => $text){
                $links['filter'][$text] = [
                    'icon' => 'Inventory Action',
                  'link' => "javascript:showDetailEventAnnotationForm('sample', $id, $actionControlId)",
                ];
            }
            $links['urls'] = ["InventoryManagement/InventoryActionMasters/listAllAjax/sample/$id/detailPage"];
        } elseif ($type == 'specimen') {
            $sampleData = $this->SampleMaster->getOrRedirect($id);
            
            // Menu
            $this->set('atimMenuVariables', [
                'Collection.id' => $sampleData['SampleMaster']['collection_id'],
                'SampleMaster.id' => $id,
                'SampleMaster.initial_specimen_sample_id' => $sampleData['SampleMaster']['initial_specimen_sample_id'],
            ]);
            $menuLink = '/InventoryManagement/InventoryActionMasters/listAll/specimen/%%SampleMaster.initial_specimen_sample_id%%';
            $this->set('atimMenu', $this->Menus->get($menuLink));

            // Data
            $data = $this->InventoryActionMaster->find('all', [
                'conditions' => [
                    $this->InventoryActionMaster->name . ".initial_specimen_sample_id" => $id,
                ], 'fields' => [
                    'DISTINCT InventoryActionControl.id',
                    'InventoryActionControl.type',
                    'InventoryActionControl.category',
                ],
            ]);

            $response = [];
            foreach ($data as $datum) {
                $response[$datum['InventoryActionControl']['id']] = $this->InventoryActionControl->getHeaderToDisplay($datum);
            }
            asort($response );

            // Links
            $links['back'] = "/InventoryManagement/SampleMasters/detail/{$sampleData['SampleMaster']['collection_id']}/$id";
            $links['filter'][__('summary')] = [
                'icon' => 'Inventory Action',
                'link' => "javascript:showDetailEventAnnotationForm('specimen', $id, 'detailPage')",
            ];
            foreach ($response as $actionControlId => $text){
                $links['filter'][$text] = [
                    'icon' => 'Inventory Action',
                    'link' => "javascript:showDetailEventAnnotationForm('specimen', $id, $actionControlId)",
                ];
            }
            $links['urls'] = ["InventoryManagement/InventoryActionMasters/listAllAjax/specimen/$id/detailPage"];
        } else {
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        $this->set('links', $links);
        $this->set('type', $type);
        
        $hookLink = $this->hook('format');
        if ($hookLink) {
            require ($hookLink);
        }
    }

    public function noHistoryFound()
    {
    }

    public function listAllAjax($type, $id, $actionControlId = null)
    {
        $displayIndex = false;
        if (!$actionControlId){
            if ($type == 'aliquot') {
                $aliquotData = $this->ViewAliquot->getOrRedirect($id);
                $displayIndex = 'ViewAliquotUse';
                $historyConditions = [
                    'aliquot_master_id' => $id,
                    'OR' => [
                        'use_definition' => [
                            'realiquoted to',
                            'aliquot shipment',
                            'order preparation',
                            'shipped aliquot return',
                        ],
                        'use_definition LIKE ' => 'sample derivative creation#%',
                    ],
                ];
                $this->request->data = $this->paginate($this->ViewAliquotUse, $historyConditions);
                $this->Structures->set('viewaliquotuses');
                $this->set('header', __('realiquotings, derivatives creations and orders'));
            }elseif ($type == 'viewaliquotuse'){
                $aliquotData = $this->ViewAliquot->getOrRedirect($id);
                $displayIndex = 'ViewAliquotUse';
                $historyConditions = [
                    'aliquot_master_id' => $id
                ];
                $this->request->data = $this->paginate($this->ViewAliquotUse, $historyConditions);
                $this->Structures->set('viewaliquotuses');
            }elseif ($type == 'storage'){
                $this->request->data = $this->AliquotMaster->getStorageHistory($id);
                $this->Structures->set('custom_aliquot_storage_history');
                $this->set('header', __('storage') . ' (' . __('system data') . ')');
                $this->set('pagination', 'No');

            }else{
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            }
        }elseif ($actionControlId == 'mainPage'){
            $displayIndex = 'Action';
            if (in_array($type, ['aliquot', 'sample', 'specimen'])){
                $this->Structures->set("inventory_action_masters" . ($type == 'aliquot'? '' : ',inventory_action_study'));
                if ($type == 'aliquot'){
                    $aliquotData = $this->ViewAliquot->getOrRedirect($id);

                    $this->request->data = $this->paginate($this->InventoryActionMaster, [
                        $this->InventoryActionMaster->name . ".aliquot_master_id" => $id,
                    ]);

                }elseif ($type == 'sample'){
                    $sampleData = $this->SampleMaster->getOrRedirect($id);

                    $this->request->data = $this->paginate($this->InventoryActionMaster, [
                        $this->InventoryActionMaster->name . ".sample_master_id" => $id,
                    ]);
                }elseif ($type == 'specimen'){
                    $sampleData = $this->SampleMaster->getOrRedirect($id);

                    $this->request->data = $this->paginate($this->InventoryActionMaster, [
                        $this->InventoryActionMaster->name . ".sample_master_id" => $id,
                    ]);
                }
            }else{
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            }
        }elseif ($actionControlId == 'detailPage'){
            $displayIndex = 'Action';
            if (in_array($type, ['aliquot', 'sample', 'specimen'])){
                $this->Structures->set("inventory_action_masters,inventory_action_sample_types" . ($type == 'aliquot'? '' : ',inventory_action_study'));
                if ($type == 'aliquot'){
                    $aliquotData = $this->ViewAliquot->getOrRedirect($id);

                    $this->request->data = $this->paginate($this->InventoryActionMaster, [
                        $this->InventoryActionMaster->name . ".aliquot_master_id" => $id,
                    ]);

                }elseif ($type == 'sample'){
                    $sampleData = $this->SampleMaster->getOrRedirect($id);
                    $this->request->data = $this->paginate($this->InventoryActionMaster, [
                        $this->InventoryActionMaster->name . ".sample_master_id" => $id,
                    ]);
                }elseif ($type == 'specimen'){
                    $sampleData = $this->SampleMaster->getOrRedirect($id);
                    $this->paginate = [
                        'fields' => "*",
                        'joins' => [
                            [
                                'alias' => 'SampleControl',
                                'table' => 'sample_controls',
                                'type' => 'INNER',
                                'conditions' => '`SampleControl`.`id` = `SampleMaster`.`sample_control_id`',
                            ],[
                                'alias' => 'i18n',
                                'table' => 'i18n',
                                'type' => 'LEFT',
                                'conditions' => '`SampleControl`.`sample_type` = `i18n`.`id`',
                            ],
                        ],
                    ];

                    $lang = substr(CakeSession::read('Config.language'), 0, 2);
                    $this->InventoryActionMaster->virtualFields ['sample_type'] = "i18n.$lang";

                    $this->request->data = $this->paginate($this->InventoryActionMaster, [
                        $this->InventoryActionMaster->name . ".initial_specimen_sample_id" => $id,
                    ]);
                }
            }else{
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            }
        }else{
            $displayIndex = 'Action';
            $actionControlData = $this->InventoryActionControl->getOrRedirect($actionControlId);
            $aliases = explode(',', $actionControlData['InventoryActionControl']['form_alias']);
            $this->Structures->set($aliases);
            if (in_array($type, ['aliquot', 'sample', 'specimen'])){
                if ($type == 'aliquot'){
                    $aliquotData = $this->ViewAliquot->getOrRedirect($id);
                    $this->request->data = $this->paginate($this->InventoryActionMaster, [
                        $this->InventoryActionMaster->name . ".aliquot_master_id" => $id,
                        $this->InventoryActionMaster->name . ".inventory_action_control_id" => $actionControlId,
                    ]);

                }elseif ($type == 'sample'){
                    $sampleData = $this->SampleMaster->getOrRedirect($id);

                    $this->request->data = $this->paginate($this->InventoryActionMaster, [
                        $this->InventoryActionMaster->name . ".sample_master_id" => $id,
                        $this->InventoryActionMaster->name . ".inventory_action_control_id" => $actionControlId,
                    ]);
                }elseif ($type == 'specimen'){
                    $sampleData = $this->SampleMaster->getOrRedirect($id);

                    $this->request->data = $this->paginate($this->InventoryActionMaster, [
                        $this->InventoryActionMaster->name . ".initial_specimen_sample_id" => $id,
                        $this->InventoryActionMaster->name . ".inventory_action_control_id" => $actionControlId,
                    ]);
                }
                $this->set('header', $this->InventoryActionControl->getHeaderToDisplay($this->request->data[0]));
            }else{
                $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            }
        }
        $this->set('displayIndex', $displayIndex);
        
        $hookLink = $this->hook('format');
        if ($hookLink) {
            require ($hookLink);
        }
    }
}