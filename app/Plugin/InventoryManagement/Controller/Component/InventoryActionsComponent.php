<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class ActionsComponent
 */
class InventoryActionsComponent extends Component
{
    private $controller;

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
        parent::initialize($controller);
    }

    public function initSelectActionType(&$data, $batch = false)
    {
        if (!empty($data['ViewSample']['sample_master_id'])){
            $ids = $data['ViewSample']['sample_master_id'] != 'all' ? array_filter($data['ViewSample']['sample_master_id']) : 'all';
            $type = 'sample';
            $this->controller->masterModel = AppModel::getInstance('InventoryManagement', 'SampleMaster', true);
        }elseif (!empty($data['ViewAliquot']['aliquot_master_id'])){
            $ids = $data['ViewAliquot']['aliquot_master_id'] != 'all' ? array_filter($data['ViewAliquot']['aliquot_master_id']) : 'all';
            $this->controller->masterModel = AppModel::getInstance('InventoryManagement', 'AliquotMaster', true);
            $type = 'aliquot';
        }else{
            $this->controller->atimFlashWarning(__('you must select an action type'), AppController::getCancelLink($data));
        }

        if ($ids == 'all' && $data['node']) {
            $this->BrowsingResult = AppModel::getInstance('Datamart', 'BrowsingResult', true);
            $browsingResult = $this->BrowsingResult->find('first', array(
                'conditions' => array(
                    'BrowsingResult.id' => $data['node']['id']
                )
            ));
            $ids = explode(",", $browsingResult['BrowsingResult']['id_csv']);
        }

        $displayLimit = Configure::read('InventoryActionCreation_processed_items_limit');
        if (countCustom($ids) > $displayLimit) {
            $this->controller->atimFlashWarning(__("batch init - number of submitted records too big") . " (" . countCustom($ids) . " > $displayLimit)", "javascript::back()");
            return;
        }

        $this->controller->inventoryActionType = $type;

        $masterData = $this->controller->masterModel->find('all', array(
            'conditions' => array(
                $this->controller->masterModel->name.".id" => $ids
        )));
        $data[0]['aliquotSampleIds'] = implode(",", $ids);
        $controlName = str_replace('Master', 'Control', $this->controller->masterModel->name);
        $controlIds = array_unique(Hash::extract($masterData, "{n}.{$controlName}.id"));
        sort($controlIds);

        $actionControlModel = AppModel::getInstance('InventoryManagement', 'InventoryActionControl', true);
        $idsField = ($type == 'aliquot') ? 'a_ids' : 's_ids';
        $conditions = [
            $idsField . " != " => '',
            'flag_test_mode' => 0,
            'flag_active_input' => 1,
            'flag_active' => 1,
        ];
        if ($batch){
            $conditions['flag_batch_action'] = "true";
        }
        $actionControlData = $actionControlModel->find('all', array(
            'conditions'=> $conditions,
            'order' => array(
                'display_order'
        )));

        $response = array();
        foreach ($actionControlData as $actionControlDatum){
            if ($batch){
                $tempsStructure = $this->controller->Structures->get("rule", $actionControlDatum['InventoryActionControl']["form_alias"]);
                if (isset($tempsStructure['StudySummary']['title']) && is_array($tempsStructure['StudySummary']['title'])){
                    foreach ($tempsStructure['StudySummary']['title'] as $k => &$study){
                        if ($study['rule'] == 'isUnique'){
                            unset($tempsStructure['StudySummary']['title'][$k]);
                        }
                    }
                }
                $allRules = Hash::extract($tempsStructure, "{s}.{s}.{n}.rule");
                if (in_array('isUnique', $allRules)){
                    continue;
                }
            }
            $tempIds = explode(",", $actionControlDatum['InventoryActionControl'][$idsField]);
            $controlIds = array_values($controlIds);
            sort($controlIds);
            $controlTempIds = array_values(array_intersect($tempIds, $controlIds));
            sort($controlTempIds);
            if ($controlTempIds == $controlIds){
                $response[$actionControlDatum['InventoryActionControl']['id']]  =  $this->controller->InventoryActionControl->getHeaderToDisplay($actionControlDatum);
            }
        }
        $response = array_flip($response);
        ksort($response);
        $response = array_flip($response);
        return $response;
    }

    public function urlToCancel($type = null, $id = null, $data = array())
    {
        $urlToCancel = '';
        if (!empty($id)) {
            $data = current($data);
            if ($type == 'aliquot') {
                $urlToCancel = '/InventoryManagement/AliquotMasters/detail/' . $data['Collection']['id'] . '/' . $data['SampleMaster']['id'] . '/' . $data['AliquotMaster']['id'] . '/';
            } elseif ($type == 'sample') {
                $urlToCancel = '/InventoryManagement/SampleMasters/detail/' . $data['Collection']['id'] . '/' . $data['SampleMaster']['id'] . '/';
            }
        } else {
            $urlToCancel = AppController::getCancelLink($this->controller->request->data);
        }
        unset($this->controller->request->data['url_to_cancel']);
        if (!$urlToCancel) {
            $urlToCancel = isset($_SESSION['batch_action_url_to_cancel']) ? $_SESSION['batch_action_url_to_cancel'] : 'javascript:history.back();';
        }

        return $urlToCancel;
    }

    public function getAddMenus($type, $id, $data)
    {
        $atimMenuLink = '/InventoryManagement/';
        $atimMenuVariables = array();
        if (!empty($id)) {
            $data = current($data);
            if ($type == 'aliquot') {
                if ($data['SampleControl']['sample_category'] == 'specimen') {
                    $atimMenuLink = '/InventoryManagement/AliquotMasters/detail/%%Collection.id%%/%%SampleMaster.initial_specimen_sample_id%%/%%AliquotMaster.id%%';
                } else {
                    $atimMenuLink = '/InventoryManagement/AliquotMasters/detail/%%Collection.id%%/%%SampleMaster.id%%/%%AliquotMaster.id%%';
                }
            } elseif ($type == 'sample') {
                if ($id != $data['SampleMaster']['initial_specimen_sample_id']){
                    $atimMenuLink = '/InventoryManagement/SampleMasters/detail/%%Collection.id%%/%%SampleMaster.id%%';
                }else{
                    $atimMenuLink = '/InventoryManagement/SampleMasters/detail/%%Collection.id%%/%%SampleMaster.initial_specimen_sample_id%%';
                }
            }
            $atimMenuVariables = array(
                'Collection.id' => $data['Collection']['id'],
                'SampleMaster.id' => $data['SampleMaster']['id'],
                'SampleMaster.initial_specimen_sample_id' => $data['SampleMaster']['initial_specimen_sample_id'],
                'AliquotMaster.id' => empty($data['AliquotMaster']['id']) ? null : $data['AliquotMaster']['id'],
            );

        }
        return array(
            'atimMenuLink' => $atimMenuLink,
            'atimMenuVariables' => $atimMenuVariables
        );
    }

    public function getDetailMenus($type, $id, $data)
    {
        return $this->getEditMenus($type, $id, $data);
    }

    public function getEditMenus($type, $id, $data)
    {
        $atimMenuLink = '/InventoryManagement/';
        $atimMenuVariables = array();
        if (!empty($id)) {
            if ($type == 'aliquot') {
                $subtype = ($data['SampleMaster']['id'] == $data['SampleMaster']['initial_specimen_sample_id'])? 'specimenaliquot' : 'samplealiquot';
                $atimMenuLink =  "/InventoryManagement/InventoryActionMasters/listAll/$subtype/%%AliquotMaster.id%%";
            } else {
                $subtype = ($data['SampleMaster']['id'] == $data['SampleMaster']['initial_specimen_sample_id'])? 'specimen' : 'sample';
                $subtypeId = ($subtype == 'specimen') ? 'SampleMaster.initial_specimen_sample_id' : 'SampleMaster.id';
                $atimMenuLink =  "/InventoryManagement/InventoryActionMasters/listAll/$subtype/%%$subtypeId%%";
            }
            $atimMenuVariables = array(
                'Collection.id' => $data['SampleMaster']['collection_id'],
                'SampleMaster.id' => $data['SampleMaster']['id'],
                'SampleMaster.initial_specimen_sample_id' => $data['SampleMaster']['initial_specimen_sample_id'],
                'AliquotMaster.id' => empty($data['AliquotMaster']['id']) ? null : $data['AliquotMaster']['id'],
            );

        }
        return array(
            'atimMenuLink' => $atimMenuLink,
            'atimMenuVariables' => $atimMenuVariables
        );
    }

    public function formattingDataForAdd($parentData)
    {
        $postData = array();
        if ($this->controller->initialDisplay) {
            foreach ($parentData as $datum) {
                $i = $datum[$this->controller->masterModel->name][$this->controller->masterModel->primaryKey];
                if (isset($this->controller->request->data[$i])) {
                    $childrenData = $this->controller->request->data[$i];
                    $childrenData['SampleControl']['sample_type'] = $this->controller->request->data[$i]['SampleControl']['sample_type'];
                } else {
                    $childrenData[0]['InventoryActionMaster']['sample_type'] = __($datum['SampleControl']['sample_type']);
                    $childrenData[0]['InventoryActionMaster']['inventory_action_control_id'] = $this->controller->controlId;
                    $childrenData[0]['InventoryActionMaster']['aliquot_master_id'] = (!empty($datum['AliquotMaster']['id'])) ? $datum['AliquotMaster']['id'] : null;
                    $childrenData[0]['InventoryActionMaster']['sample_master_id'] = $datum['SampleMaster']['id'];
                    $childrenData[0]['InventoryActionMaster']['initial_specimen_sample_id'] = $datum['SampleMaster']['initial_specimen_sample_id'];
                }
                list ($datum['Generated']['used_parent_label_for_display_title'], $datum['Generated']['used_parent_label_for_display_description']) = $this->controller->{$this->controller->masterModel->name}->getBatchActionUsedParentLabelForDisplay($datum);
                $postData[$i] = array(
                    'parent' => $datum,
                    'children' => $childrenData,
                );
            }
        } elseif (!empty($this->controller->request->data)) {
            $controlName = str_replace('Master', 'Control', $this->controller->masterModel->name);
            foreach ($parentData as $datum) {
                $i = $datum[$this->controller->masterModel->name][$this->controller->masterModel->primaryKey];
                if (isset($this->controller->request->data[$i])) {
                    $this->controller->request->data[$i][$controlName] = $datum[$controlName];
                    foreach ($datum[$this->controller->masterModel->name] as $key => $value){
                        if (!isset($this->controller->request->data[$i][$this->controller->masterModel->name][$key])){
                            $this->controller->request->data[$i][$this->controller->masterModel->name][$key] = $value;
                        }
                    }
                }
            }
            $removeFromStorage = isset($this->controller->request->data['FunctionManagement']['remove_from_storage']) ? !empty($this->controller->request->data['FunctionManagement']['remove_from_storage']) : 0;
            $removeInStockDetail = isset($this->controller->request->data['FunctionManagement']['remove_in_stock_detail']) ? !empty($this->controller->request->data['FunctionManagement']['remove_in_stock_detail']) : 0;
            $inStock = isset($this->controller->request->data['FunctionManagement']['in_stock']) ? $this->controller->request->data['FunctionManagement']['in_stock'] : '';

            $inStockDetail = (isset($this->controller->request->data['AliquotMaster']['in_stock_detail']) && !empty($this->controller->request->data['AliquotMaster']['in_stock_detail']))? $this->controller->request->data['AliquotMaster']['in_stock_detail'] : '';
            
            if ($removeInStockDetail && $inStockDetail){
                $this->controller->errors[-1][-1]["AliquotMaster"]["in_stock_detail"] = __('data conflict: you can not delete data and set a new one') . ": " .  __('see detail options in data to apply to all');
            }

            foreach ($this->controller->request->data as $key => &$datum) {
                if (is_numeric($key)) {
                    if (empty($postData[$key]['children'])) {
                        $postData[$key]['children'] = array();
                    }
                    if ($this->controller->masterModel->name == 'AliquotMaster') {
                        if ($inStock){
                            $datum['AliquotMaster']['in_stock'] = $inStock;
                        }
                        if ($inStockDetail) {
                            $datum['AliquotMaster']['in_stock_detail'] = $inStockDetail;
                        }
                        if ($removeInStockDetail){
                            $datum['AliquotMaster']['in_stock_detail'] = '';
                        }
                        if ($removeFromStorage || $inStock === 'no'){
                            $datum['FunctionManagement']['remove_from_storage'] = 1;
                            $datum[$this->controller->masterModel->name]['storage_master_id'] = null;
                            $datum[$this->controller->masterModel->name]['storage_coord_x'] = '';
                            $datum[$this->controller->masterModel->name]['storage_coord_y'] = '';
                        }


                    } elseif ($this->controller->masterModel->name == 'SampleMaster') {

                    }

                    foreach ($datum as $k => $d) {
                        if (is_numeric($k)) {
                            $postData[$key]['children'][$k] = $d;
                            $postData[$key]['children'][$k]['InventoryActionMaster']['inventory_action_control_id'] = $this->controller->controlId;                           
                            $postData[$key]['children'][$k]['InventoryActionMaster']['sample_type'] = ($this->controller->masterModel->name == 'SampleMaster') ? __($datum['SampleControl']['sample_type']) : '';
                        } else {
                            $postData[$key]['parent'][$k] = $d;
                        }
                    }
                    if (!empty($postData[$key]['parent'][$this->controller->masterModel->name])) {
                        $postData[$key]['parent'][$this->controller->masterModel->name][$this->controller->masterModel->primaryKey] = $key;
                    }
                }
            }
        }
        return $postData;
    }

    public function initialEdit($type, $actionData)
    {
        if (!empty($actionData['StudySummary']['id'])) {
            $actionData['FunctionManagement']['autocomplete_aliquot_internal_use_study_summary_id'] = $actionData['StudySummary']['title'] . " [" . $actionData['StudySummary']['id'] . "]";
        }
        if ($type == 'aliquot') {
            $this->controller->masterModel = AppModel::getInstance('InventoryManagement', 'AliquotMaster', true);
            $masterId = $actionData['InventoryActionMaster']['aliquot_master_id'];
            $aliquotMasterData = $this->controller->masterModel->findById($masterId);
            $actionData['AliquotControl'] = $aliquotMasterData['AliquotControl'];
        } elseif ($type == 'sample') {
            $this->controller->masterModel = AppModel::getInstance('InventoryManagement', 'SampleMaster', true);
        } else {
            $this->controller->atimFlashError(__('the selected type is not correct (%s)', __($type)), 'javascript:history.back()');
        }

        return $actionData;
    }

    public function initialDetail($type, $actionData)
    {
        $actionData = $this->initialEdit($type, $actionData);
        return $actionData;
    }

    public function initialAdd(&$type, &$controlId, $id)
    {
        if (empty($type) && !empty($this->controller->request->data['type'])) {
            $type = $this->controller->request->data['type'];
        }
        if (empty($controlId) && !empty($this->controller->request->data[0]['inventory_action_control_id'])) {
            $controlId = $this->controller->request->data[0]['inventory_action_control_id'];
        }elseif (empty($controlId) && empty($this->controller->request->data[0]['inventory_action_control_id'])){
            $this->controller->atimFlashWarning(__('you must select an action type'), AppController::getCancelLink($this->controller->request->data));
        }
        if (!empty($this->controller->request->data[0]['aliquotSampleIds'])){
            if ($type == 'aliquot'){
                $this->controller->request->data['ViewAliquot']['aliquot_master_id'] = explode(",", str_replace(' ', '', $this->controller->request->data[0]['aliquotSampleIds']));
            }elseif ($type == 'sample'){
                $this->controller->request->data['ViewSample']['sample_master_id'] = explode(",", str_replace(' ', '', $this->controller->request->data[0]['aliquotSampleIds']));
            }
        }
        if ($type == 'aliquot') {
            $this->controller->masterModel = AppModel::getInstance('InventoryManagement', 'AliquotMaster', true);
        } elseif ($type == 'sample') {
            $this->controller->masterModel = AppModel::getInstance('InventoryManagement', 'SampleMaster', true);
        }else{
            $this->controller->atimFlashError(__('the selected type is not correct (%s)', __($type)), 'javascript:history.back()');
        }

        $this->controller->controlId = $controlId;
        $this->controller->initialDisplay = false;
        $ids = array();
        if ($id && !is_array($id)) {
            $ids[] = $id;
            if (empty($this->controller->request->data)) {
                $this->controller->initialDisplay = true;
            }
        } elseif (!empty($this->controller->request->data)) {
            if ($type == 'aliquot' && !empty($this->controller->request->data['ViewAliquot']['aliquot_master_id'])) {
                $ids = $this->controller->request->data['ViewAliquot']['aliquot_master_id'];
                unset($this->controller->request->data['ViewAliquot']);
                $this->controller->initialDisplay = true;
            } elseif ($type == 'sample' && !empty($this->controller->request->data['ViewSample']['sample_master_id'])) {
                $ids = $this->controller->request->data['ViewSample']['sample_master_id'];
                unset($this->controller->request->data['ViewSample']);
                $this->controller->initialDisplay = true;
            } else {
                $ids = array_filter(array_keys($this->controller->request->data), function ($k) {
                    return is_int($k);
                });
            }

            if ($ids == 'all' && $this->controller->request->data['node']) {
                $this->BrowsingResult = AppModel::getInstance('Datamart', 'BrowsingResult', true);
                $browsingResult = $this->BrowsingResult->find('first', array(
                    'conditions' => array(
                        'BrowsingResult.id' => $this->controller->request->data['node']['id']
                    )
                ));
                $ids = explode(",", $browsingResult['BrowsingResult']['id_csv']);
            }

            if ($ids == 'all') {
                $ids = array();
            }
        }

        $ids = array_filter($ids);

        $this->controller->ids = $ids;
    }

    public function checkIEAControlId($type = "", $controlId = null, $ids = [])
    {
        $this->initialAdd($type, $controlId, $ids);

        $data = [];
        if ($type == 'sample'){
            $data['ViewSample']['sample_master_id'] = $this->controller->ids;
        }elseif ($type == 'aliquot'){
            $data['ViewAliquot']['aliquot_master_id'] = $this->controller->ids;
        }
        $controlIds = $this->initSelectActionType($data);
        return in_array($controlId, array_keys($controlIds)) !== false;
    }
    
    public function formattingErrors()
    {
        $tmpMessages = array();
        $errorsMassages = array();
        $validationErrors = array();
        $errors = $this->controller->errors;

        if (!empty($errors)){
            foreach ($errors as $id => $errorsInRow) {
                foreach ($errorsInRow as $row => $errorsByModel) {
                    if (!is_array($errorsByModel)){
                        $index = "data[$id][$row]";
                        $currentMessages = $errors[$id][$row];
                        $tmpMessages[$currentMessages][$id] = "$index||||$row";
                        $errorsMassages[$index] = $currentMessages;
                    }else{
                        foreach ($errorsByModel as $model => $errorsByField) {
                            foreach ($errorsByField as $field => $messages) {
                                if ($id == -1 && $row == -1){
                                    $index = "data[$model][$field]";
                                    $currentMessages[0] = $errors[$id][$row][$model][$field];
                                }elseif (is_numeric($row)) {
                                    $index = "data[$id][$row][$model][$field]";
                                    $currentMessages = $errors[$id][$row][$model][$field];
                                } else {
                                    $index = "data[$id][$row][$model]";
                                    $currentMessages = $errors[$id][$row][$model];
                                }
                                $errorsMassages[$index] = $currentMessages;
                                if (is_array($currentMessages)) {
                                    foreach ($currentMessages as $k => $message) {
                                        if ($k === 'duplicatedModelName'){
                                            foreach ($message as $m){
                                                $tmpMessages[$m][$id][$row] = "$index||||$field||||duplicatedModelName111111111";
                                            }
                                        }elseif ($field === 'duplicatedModelName') {
                                            foreach ($message as $m){
                                                $tmpMessages[$m][$id] = "$index||||$model||||duplicatedModelName22222222";
                                            }
                                        }elseif (is_numeric($row)) {
                                            $tmpMessages[$message][$id][$row] = "$index||||$field";
                                        } else {
                                            $tmpMessages[$message][$id] = "$index||||$model";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $this->controller->errors = $errorsMassages;
            $this->controller->set('errorsGrid', $this->controller->errors);

            foreach ($tmpMessages as $message => $data) {
                $ids = array();
                foreach ($data as $id => $datum) {
                    if (is_array($datum)) {
                        foreach ($datum as $row => $link) {
                            $l = explode("||||", $link)[0];
                            $field = explode("||||", $link)[1];
                            $idRow = ($id!==-1) ? "$id-$row" : __("Link");
                            $ids[] = "<a href = 'javascript:void(0)' class = 'scroll-to-error-message' onclick='scrollToError(\"" . $l . "\")'>$idRow" . "</a>";
                        }
                    } else {
                        $l = explode("||||", $datum)[0];
                        $field = explode("||||", $datum)[1];
                        $ids[] = "<a href = 'javascript:void(0)' class = 'scroll-to-error-message' onclick='scrollToError(\"" . $l . "\")'>" . $id . "</a>";
                    }
                }
                $validationErrors[$field][] = $message . " <span class = 'expandable-span-error-messages' onclick = 'showDetail(event)'>>>> </span> <p style = 'margin: 0px' class = 'display-none'>(" . implode(",", $ids) . ")</p>";
            }

        }

        return $validationErrors;
    }

    public function cleanup(&$rowData)
    {
        unset($rowData['parent']['AliquotMaster']['storage_coord_x']);
        unset($rowData['parent']['AliquotMaster']['storage_coord_y']);
        unset($rowData['parent']['StorageMaster']['selection_label']);
    }

    public function ifExistsActionsById($type, $ids, $options = array())
    {
        $options+=[
            'recursive' => false
        ];
        if (!is_array($ids)) {
            $ids = array($ids);
        }
        $actionMasterModel = AppModel::getInstance('InventoryManagement', 'InventoryActionMaster', true);
        if ($type == 'aliquot') {
            $actionsData = $actionMasterModel->find('first', array(
                'conditions' => array(
                    $actionMasterModel->name . '.aliquot_master_id' => $ids
                ),
                'recursive' => -1
            ));
        } elseif ($type == 'sample') {
            if (empty($options['recursive'])){
                $conditions = array(
                    $actionMasterModel->name . '.sample_master_id' => $ids,
                    $actionMasterModel->name . '.aliquot_master_id IS NULL'
                );
            }else{
                $conditions = array(
                    $actionMasterModel->name . '.sample_master_id' => $ids,
                );
            }
            $actionsData = $actionMasterModel->find('first', array(
                'conditions' => $conditions,
                'recursive' => -1
            ));
        }
        return !empty($actionsData);
    }

    public function initIndex($type, $id, $options = array())
    {
        if (!is_array($id)) {
            $id = array($id);
        }
        $options+=[
            'recursive' => false
        ];
        $actiondata = array();
        $actionModel = AppModel::getInstance('InventoryManagement', 'InventoryActionMaster', true);
        $aliquotMasterModel = AppModel::getInstance('InventoryManagement', 'AliquotMaster', true);
        if ($type == 'aliquot') {
            $conditions = array(
                $actionModel->name . ".aliquot_master_id" => $id
            );
        } elseif ($type == 'sample') {
            if (empty($options['recursive'])){
                $conditions = array(
                    $actionModel->name . '.sample_master_id' => $id,
                    $actionModel->name . '.aliquot_master_id IS NULL'
                );
            }else{
                $conditions = array(
                    $actionModel->name . '.sample_master_id' => $id,
                );
            }
        } elseif ($type == 'init') {
            $conditions = array(
                $actionModel->name . ".initial_specimen_sample_id" => $id
            );
        }
        if (!empty($conditions)) {
            $actiondata = $actionModel->find('all', array(
                'conditions' => $conditions,
                'recursive' => 1
            ));
        }

        foreach ($actiondata as &$data) {
            if (!empty($data['InventoryActionMaster']['aliquot_master_id'])){
                $aliquotMasterData = $aliquotMasterModel->findById($data['InventoryActionMaster']['aliquot_master_id']);
                $data['AliquotControl'] = $aliquotMasterData['AliquotControl'];
            }
        }

        return $actiondata;
    }

    public function preSaveProcessing($type, &$unitData)
    {
        $modelData = $this->controller->masterModel->findById($unitData['parent'][$this->controller->masterModel->name][$this->controller->masterModel->primaryKey]);
        foreach ($unitData['children'] as $k => $child) {
            $unitData['children'][$k]['InventoryActionMaster']['inventory_action_control_id'] = $this->controller->controlId;
            $unitData['children'][$k]['InventoryActionMaster']['sample_master_id'] = $modelData['SampleMaster']['id'];
            $unitData['children'][$k]['InventoryActionMaster']['initial_specimen_sample_id'] = $modelData['SampleMaster']['initial_specimen_sample_id'];
            $unitData['children'][$k]['InventoryActionMaster']['aliquot_master_id'] = !empty($modelData['AliquotMaster']['id']) ? $modelData['AliquotMaster']['id'] : null;
        }
    }

    public function initSelectInventoryUseEventPage(&$type)
    {
        $type = strtolower($type);
        $response = [];
        $response['links']['cancel'] = $this->urlToCancel();
        $response['links']['submit'] = "/InventoryManagement/InventoryActionMasters/addOneEventInBatchToMany/$type";
        if (empty($this->controller->request->data)){
            $this->controller->atimFlashError(__('the selected type is not correct (%s)', __($type)), $response['links']['cancel']);
        }

        if (($type == 'aliquot' || $type == 'sample') && !empty($this->controller->request->data[0]['aliquotSampleIds']) && !empty($this->controller->request->data[0]['inventory_action_control_id'])){
                $response['controlId'] = $this->controller->request->data[0]['inventory_action_control_id'];
                $ids = $this->controller->request->data[0]['aliquotSampleIds'];
                if (!empty($ids)){
                    if($type == 'aliquot'){
                        $masterModel = AppModel::getInstance('InventoryManagement', 'AliquotMaster', true);
                        $fields = [
                            'AliquotMaster.*',
                            'SampleMaster.*',
//                            'AliquotMaster.id',
//                            'SampleMaster.id',
//                            'SampleMaster.initial_specimen_sample_id',
                        ];
                        $orderBy = [
                            'AliquotMaster.id'
                        ];
                    }elseif ($type == 'sample'){
                        $masterModel = AppModel::getInstance('InventoryManagement', 'SampleMaster', true);
                        $fields = [
                            'SampleMaster.*',
//                            'SampleMaster.id',
//                            'SampleMaster.initial_specimen_sample_id',
                        ];
                        $orderBy = [
                            'SampleMaster.id'
                        ];
                    }
                    $idArray = explode(",", $ids);
                    sort($idArray);
                    $masterData = $masterModel->find('all', [
                        'conditions' => [
                            $masterModel->name . ".id" => $idArray,
                        ],
                        'recursive' => 0,
                        'fields' => $fields,
                        'order' => $orderBy,
                    ]);
                    if($type == 'aliquot'){
                        $masterDataIds = Hash::extract($masterData, "{n}.AliquotMaster.id");
                        if($idArray != $masterDataIds){
                            $notFoundIds = implode(", ", array_diff($idArray, $masterDataIds));
                            AppController::addWarningMsg(__('some aliquots not found [%s]', $notFoundIds));
                        }
                    }elseif ($type == 'sample'){
                        $masterDataIds = Hash::extract($masterData, "{n}.SampleMaster.id");
                        if($idArray != $masterDataIds){
                            $notFoundIds = implode(", ", array_diff($idArray, $masterDataIds));
                            AppController::addWarningMsg(__('some samples not found [%s]', $notFoundIds));
                        }
                    }
                }
                $dataToSave = [];
                $dataModelToSave = [];
                if (!empty($this->controller->request->data['InventoryActionMaster'])){
                    foreach ($masterData as $v){
                        $index = countCustom($dataToSave);
                        $dataToSave[$index]['InventoryActionMaster'] = $this->controller->request->data['InventoryActionMaster'];
                        $dataToSave[$index]['InventoryActionMaster']['inventory_action_control_id'] = $response['controlId'];
                        if (!empty($this->controller->request->data['InventoryActionDetail'])){
                            $dataToSave[$index]['InventoryActionDetail'] = $this->controller->request->data['InventoryActionDetail'];
                        }
                        $dataToSave[$index]['InventoryActionMaster']['sample_master_id'] = $v['SampleMaster']['id'];
                        $dataToSave[$index]['InventoryActionMaster']['initial_specimen_sample_id'] = $v['SampleMaster']['initial_specimen_sample_id'];
                        if ($type == 'aliquot'){
                            $dataToSave[$index]['InventoryActionMaster']['aliquot_master_id'] = $v['AliquotMaster']['id'];
                        }
                        if (!empty($v['AliquotMaster'])){
                            $dataModelToSave[$index]['AliquotMaster'] = $v['AliquotMaster'];
                        }
                        if (!empty($v['AliquotMaster'])){
                            $dataModelToSave[$index]['SampleMaster'] = $v['SampleMaster'];
                        }
                    }
                }
                $response['dataToSave'] = $dataToSave;
                $response['dataModelToSave'] = $dataModelToSave;
        }else{
            $this->controller->atimFlashError(__('you must select an action type'), $response['links']['cancel']);
        }
        $response['controlData'] = $this->controller->InventoryActionControl->findById($response['controlId']);

        return $response;
    }

    public function IEAActionInBatchValidates($type)
    {
        $storageModel = AppModel::getInstance('StorageLayout', 'StorageMaster', true);
        $studySummaryModel = AppModel::getInstance("Study", "StudySummary", true);
        $validationErrors = [];
        
        if ($type == 'aliquot') {
            $tmpDataAliquotMaster = $this->controller->AliquotMaster->data;  // Not sure requested
            $aliquotIds = explode(',', $this->controller->request->data[0]['aliquotSampleIds']);
            
            $this->controller->request->data['FunctionManagement']['in_stock_detail'] = $this->controller->request->data['AliquotMaster']['in_stock_detail'];
            $data = $this->controller->request->data['FunctionManagement'];
            
            if (!empty($data['autocomplete_aliquot_master_study_summary_id'])) {
                $arrStudySelectionResults = $studySummaryModel->getStudyIdFromStudyDataAndCode($data['autocomplete_aliquot_master_study_summary_id']);
                if (isset($arrStudySelectionResults['error'])) {
                    $validationErrors['autocomplete_aliquot_master_study_summary_id'][] = $arrStudySelectionResults['error'];
                }else{
                    $this->controller->request->data['FunctionManagement']['StudySummary'] = $arrStudySelectionResults['StudySummary'];
                }
            }
            if (!empty($data['autocomplete_aliquot_master_study_summary_id']) && !empty($data['remove_study_summary_id'])) {
                $validationErrors['autocomplete_aliquot_master_study_summary_id'][] = __('data conflict: you can not delete data and set a new one');
            }

            if (!empty($data['recorded_storage_selection_label']) && (!empty($data['remove_from_storage']) || ($data['in_stock'] == 'no'))) {
                $validationErrors['recorded_storage_selection_label'][] = __('data conflict: you can not remove aliquot and set a storage');
                if ($data['in_stock'] == 'no') {
                    $validationErrors['in_stock'][] = __('data conflict: you can not remove aliquot and set a storage');
                }
            }

            if (!empty($data['in_stock_detail']) && !empty($data['remove_in_stock_detail'])) {
                $validationErrors['in_stock_detail'][] = __('data conflict: you can not delete data and set a new one');
            }

            if (!empty($data['recorded_storage_selection_label'])){
                $storageData = $storageModel->getStorageDataFromStorageLabelAndCode($data['recorded_storage_selection_label']);
                if (!empty($storageData['error'])){
                    $validationErrors['recorded_storage_selection_label'][] = $storageData['error'];
                }else{
                    $this->controller->request->data['FunctionManagement']['StorageMaster'] = $storageData['StorageMaster'];
                }
                
                if (empty($data['in_stock'])) {
                    // New stock value has not been set then won't be updated: Control above detected no conflict - Check data in db
                    $condtions = array(
                        'AliquotMaster.id' => $aliquotIds,
                        'AliquotMaster.in_stock' => 'no'
                    );
                    $aliquotNotInStock = $this->controller->AliquotMaster->find('count', array(
                        'conditions' => $condtions,
                        'recursive' => - 1
                    ));
                    if ($aliquotNotInStock) {
                        $validationErrors['recorded_storage_selection_label'][] = __('data conflict: at least one updated aliquot is defined as not in stock - please update in stock value');
                    }
                }
            }
            
            if (! $validationErrors) {
                // Aliquot validation using AliquotMaster.validate()
                // Will check stored aliquot type vs storage type (TMA block....)
                
                $aliquotMasterDataForfinalValidation = $this->controller->request->data['AliquotMaster'];
                $aliquotMasterDataForfinalValidation['in_stock'] = $this->controller->request->data['FunctionManagement']['in_stock'];
                $aliquotMasterDataForfinalValidation = array(
                    'AliquotMaster' => array_filter($aliquotMasterDataForfinalValidation)
                );
                
                if (! empty($data['remove_from_storage']) || ($data['in_stock'] == 'no')) {
                    // Data set just for validation.
                    // Data will be set for record in InventoryActions->prepareAliquotBatchData()
                    $aliquotMasterDataForfinalValidation['AliquotMaster']['storage_master_id'] = null;
                    $aliquotMasterDataForfinalValidation['AliquotMaster']['storage_coord_x'] = null;
                    $aliquotMasterDataForfinalValidation['AliquotMaster']['storage_coord_y'] = null;
                    AppController::addWarningMsg(__('aliquots positions have been deleted'));
                } elseif (! empty($data['recorded_storage_selection_label'])) {
                    // Data set just for validation.
                    // Data will be set for record in InventoryActions->prepareAliquotBatchData()
                    $aliquotMasterDataForfinalValidation['FunctionManagement']['recorded_storage_selection_label'] = $data['recorded_storage_selection_label'];
                    $aliquotMasterDataForfinalValidation['AliquotMaster']['storage_coord_x'] = null;
                    $aliquotMasterDataForfinalValidation['AliquotMaster']['storage_coord_y'] = null;
                    AppController::addWarningMsg(__('aliquots positions have been deleted'));
                }
                
                $aliquotMasterDataForfinalValidation['AliquotMaster']['aliquot_control_id'] = 1; // to allow validation, remove afterward
                $notCoreNbr = $this->controller->AliquotMaster->find('count', array(
                    'conditions' => array(
                        'AliquotMaster.id' => $aliquotIds,
                        "AliquotControl.aliquot_type != 'core'"
                    )
                ));
                $aliquotMasterDataForfinalValidation['AliquotControl']['aliquot_type'] = $notCoreNbr ? 'not core' : 'core'; // to allow tma storage check (check aliquot != than core is not stored into TMA block), remove afterward
                $this->controller->AliquotMaster->set($aliquotMasterDataForfinalValidation);                
                $this->controller->AliquotMaster->validates();
                $validationErrors = $this->controller->AliquotMaster->validationErrors;
            }
            
            $this->controller->AliquotMaster->data = $tmpDataAliquotMaster;  // Not sure requested
            
            $this->controller->InventoryActionMaster->validationErrors = array_merge($this->controller->InventoryActionMaster->validationErrors, $validationErrors);
        }
        return empty($validationErrors);
    }

    public function saveAllAliquots($data)
    {
        $response = false;
        if(!empty($data)){
            $ids = $data['ids'];
            $updateData = $data['updateData'];
            
            if (! empty($updateData)) {
                $this->controller->AliquotMaster->addWritableField(array_keys($updateData));
                $response = true;
                foreach ($ids as $aliquotId) {
                    $this->controller->AliquotMaster->id = $aliquotId;
                    $this->controller->AliquotMaster->data = null;
                    if (! $this->controller->AliquotMaster->save(['AliquotMaster' => $updateData], false)) {
                        $response = false;
                    }
                }
            } else {
                $response = true;
            }
        }else{
            $response = true;
        }
        return $response;
    }

    public function prepareAliquotBatchData($data, $dataModelToSave)
    {
        $ids = Hash::extract($dataModelToSave, '{n}.AliquotMaster.id');
        $updateData = [];
        if (!empty($ids)){
            if (!empty($data['remove_from_storage']) || $data['in_stock'] == 'no') {
                //$updateData['in_stock'] = "no";
                $updateData['storage_master_id'] = "";
                $updateData['storage_coord_x'] = "";
                $updateData['storage_coord_y'] = "";
            } elseif (!empty($data['StorageMaster']['storage_id'])) {
                $updateData['storage_master_id'] = $data['StorageMaster']['storage_id'];
                $updateData['storage_coord_x'] = "";
                $updateData['storage_coord_y'] = "";
            } if(!empty($data['StorageMaster']['id'])){
                $updateData['storage_master_id'] = $data['StorageMaster']['id'];
                $updateData['storage_coord_x'] = "";
                $updateData['storage_coord_y'] = "";
            }
            
            if (!empty($data['in_stock']) /*&& $data['in_stock'] != 'no'*/) {
                $updateData['in_stock'] = $data['in_stock'];
            }

            if (!empty($data['in_stock_detail'])) {
                $updateData['in_stock_detail'] = $data['in_stock_detail'];
            } elseif (!empty($data['remove_in_stock_detail'])) {
                $updateData['in_stock_detail'] = "";
            }
        }
        
        return [
            'ids' => $ids,
            'updateData' =>$updateData
        ];
    }
}