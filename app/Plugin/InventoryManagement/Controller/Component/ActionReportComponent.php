<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

class ActionReportComponent extends Component
{
    private $controller;

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
        parent::initialize($controller);
    }

    /*
     *  Set the ids and the master model for ALiquotMastersController
     * */
    public function initDataReport()
    {
        if (!empty($this->controller->request->data['ViewSample']['sample_master_id'])){
            $ids = $this->controller->request->data['ViewSample']['sample_master_id'] != 'all' ? array_filter($this->controller->request->data['ViewSample']['sample_master_id']) : 'all';
            $type = 'sample';
            $this->controller->masterModel = AppModel::getInstance('InventoryManagement', 'SampleMaster', true);
        }elseif (!empty($this->controller->request->data['ViewAliquot']['aliquot_master_id'])){
            $ids = $this->controller->request->data['ViewAliquot']['aliquot_master_id'] != 'all' ? array_filter($this->controller->request->data['ViewAliquot']['aliquot_master_id']) : 'all';
            $this->controller->masterModel = AppModel::getInstance('InventoryManagement', 'AliquotMaster', true);
            $type = 'aliquot';
        }else {
            $this->controller->atimFlashWarning(__('you must select an action type'), AppController::getCancelLink($this->controller->request->data));
        }

        if ($ids == 'all' && $this->controller->request->data['node']) {
            $this->BrowsingResult = AppModel::getInstance('Datamart', 'BrowsingResult', true);
            $browsingResult = $this->BrowsingResult->find('first', array(
                'conditions' => array(
                    'BrowsingResult.id' => $this->controller->request->data['node']['id']
                )
            ));
            $ids = explode(",", $browsingResult['BrowsingResult']['id_csv']);
        }
        $this->controller->ids = $ids;

    }
    
}