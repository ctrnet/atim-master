<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

if (!empty($links['urls'])){
    $firstOne = true;
    foreach ($links['urls'] as $url){
        $finalOptions = array(
            'type' => 'detail',
            'settings' => array(
                'actions' => false
            ),
            'extras' => $this->Structures->ajaxIndex($url)
        );
        if ($firstOne) {
            if ($type == 'specimen') {
                $finalOptions['settings']['header'] = [
                    'title' => __('event_annotation_listall_specimen_header_string'),
                    'description' => __('event_annotation_listall_specimen_description_string')
                ];
            } elseif ($type == 'sample') {
                $finalOptions['settings']['header'] = [
                    'title' => __("uses and events"),
                    'description' => __('event_annotation_listall_derivative_description_string')
                ];
            } else {
                $finalOptions['settings']['header'] = [
                    'title' => __("uses and events"),
                    'description' => null
                ];
            }
        }
        $firstOne = false;
        $this->Structures->build([], $finalOptions);
    }

}else{
    $finalOptions = array(
        'type' => 'detail',
        'settings' => array(
            'actions' => false
        ),
        'extras' => $this->Structures->ajaxIndex("/InventoryManagement/InventoryActionMasters/noHistoryFound")
    );
    $this->Structures->build([], $finalOptions);
}

$structureLinks = array(
    'bottom' => array(
        'back' => $links['back'],
    )
);

$finalOptions = array(
    'links' => [
        'bottom' => array(
            'back' => $links['back'],
            'events/annotation types' => (!empty($links['filter'])) ? $links['filter'] : null,
        )
    ],
    'type' => 'detail',
);
$this->Structures->build([], $finalOptions);
?>

<script>
    function showDetailEventAnnotationForm(type, id, controlId){
        $(".wrapper.plugin_InventoryManagement.controller_InventoryActionMasters.action_listAll .indexZone").eq(0).html("<div class='loading'>---" + STR_LOADING + "---</div>");
        var link = root_url + "/InventoryManagement/InventoryActionMasters/listAllAjax/" + type + "/" + id + "/" + controlId ;
        $.get(link, null, function(data){
           data = normaliseData(data);
           $(".wrapper.plugin_InventoryManagement.controller_InventoryActionMasters.action_listAll .indexZone").eq(0).html(data);
        });
    }


</script>