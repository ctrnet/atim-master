<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

$finalAtimStructure = $inventoryActionSelectionStructure;
$finalOptions = array(
    'type' => 'add',
    'settings' => array(
        'header' => __('inventory action creation process') . ' - ' . __('event/annotation selection'),
        'stretch' => false
    ),
    'links' => array(
        'top' => $links['submit'],
        'bottom' => array(
            'cancel' => $links['cancel']
        )
    ),
    'extras' => '<input type="hidden" name="data[type]" value="'.$type.'"/>'
);
if (isset($nodeId)){
    $finalOptions['extras'] .= '<input type="hidden" name="data[node][id]" value="'.$nodeId.'"/>';
}
if (isset($batchSetId)){
    $finalOptions['extras'] .= '<input type="hidden" name="data[BatchSet][id]" value="'.$batchSetId.'"/>';
}

// CUSTOM CODE
$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}

// BUILD FORM
$this->Structures->build($finalAtimStructure, $finalOptions);