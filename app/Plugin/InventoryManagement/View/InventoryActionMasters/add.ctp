<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

$structureLinks = array(
    'top' => $links['submit'],
    'bottom' => array(
        'cancel' => $links['cancel']
    )
);

$parentOptions = array(
    'links' => $structureLinks,
    'type' => 'edit',
    'settings' => array(
        'pagination' => true,
        'add_fields' => true,
        'del_fields' => true,
        'actions' => false,
        'form_bottom' => false,
        'form_top' => true,
        'class' => 'show_all_the_parents',
    ),
);

$childrenOptions = array(
    'links' => $structureLinks,
    'type' => 'addgrid',
    'settings' => array(
        'pagination' => true,
        'add_fields' => true,
        'del_fields' => true,
        'class' => 'show_all_the_children'
    ),
);


$finalApplyAllStructure= array();
// Form to apply data to all parents
$structureOptions = $parentOptions;
unset($structureOptions['settings']['language_heading']);
unset($structureOptions['settings']['class']);
$structureOptions['settings']['section_start'] = false;
$structureOptions['settings']['header'] = array(
    'title' => __('inventory action creation process') . ' : ' . $controlHeader,
    'description' => null
);

if (!empty($actionControlData['InventoryActionControl']['flag_link_to_storage']) && $type == 'aliquot' && !$idParameterSet){
    $structureOptions['settings']['header'] = array(
        'title' => __('inventory action creation process') . ' : ' . $controlHeader,
        'description' => '');
    $structureOptions['settings']['language_heading'] = __('data to be applied to all aliquots used -- fields values of the section below will be applied to all other sections if entered and will replace sections fields values'); 
    $finalApplyAllStructure= $actionApplyAll;
}

$hookLink = $this->Structures->hook('apply_to_all');
if ($hookLink) {
    require ($hookLink);
}

$this->Structures->build($finalApplyAllStructure, $structureOptions);

$endKey = (array_keys($data)[countCustom($data)-1]);
$firstKey = (array_keys($data)[0]);
$counter = 0;
$oneParent = (sizeof($data) == 1) ? true : false;
foreach ($data as $k => $datum){
    $counter ++;
    $parentData = $datum['parent'];
    $childrenData = $datum['children'];

    $finalChildrenStructure = $actionDetailAlias;
    $finalParentStructure = $actionParentStructure;
    unset($childrenOptions['override']['AliquotControl.volume_unit']);

    if ($type == 'aliquot'){
        if (!empty($parentData['AliquotControl']['volume_unit']) && !empty($actionControlData['InventoryActionControl']['flag_link_to_study'])){
            $finalChildrenStructure = $actionDetailAliasStudyVolume;
        }elseif (!empty($parentData['AliquotControl']['volume_unit'])){
            $finalChildrenStructure = $actionDetailAliasVolume;
        }elseif (!empty($actionControlData['InventoryActionControl']['flag_link_to_study'])){
            $finalChildrenStructure = $actionDetailAliasStudy;
        }
        if (!empty($parentData['AliquotControl']['volume_unit'])){
            $childrenOptions['override']['AliquotControl.volume_unit'] = $parentData['AliquotControl']['volume_unit'];
        }
    }elseif($type == 'sample'){
        if (!empty($actionControlData['InventoryActionControl']['flag_link_to_study'])){
            $finalChildrenStructure = $actionDetailAliasStudy;
        }
    }

    $childrenOptions['settings']['actions'] = ($endKey == $k);
    $childrenOptions['settings']['section_end'] = true;
    $childrenOptions['settings']['confirmation_msg'] = ($endKey == $k) ? __('multi_entry_form_confirmation_msg') : null;

    $childrenOptions['settings']['form_bottom'] = ($endKey == $k);
    $childrenOptions['settings']['form_top'] = empty($finalParentStructure) && ($firstKey == $k);
    $childrenOptions['settings']['name_prefix'] = $parentOptions['settings']['name_prefix'] = $k;
    if ($endKey == $k){
        $extras = [];
        if (isset($nodeId)){
            $extras[] = '<input type="hidden" name="data[node][id]" value="'.$nodeId.'"/>';
        }
        if (isset($batchSetId)){
            $extras[] = '<input type="hidden" name="data[BatchSet][id]" value="'.$batchSetId.'"/>';
        }
        $extras[] = '<input type="hidden" name="data[max_input_vars_validation]" value="1" class = "max_input_vars_validation" />';
        $childrenOptions['extras'] = implode(' ', $extras);
    }
    
    $this->request->data = [$k => $parentData];
    $parentOptions['settings']['section_start'] = true;
    
    $parentOptions['settings']['header']['title'] = $parentData['Generated']['used_parent_label_for_display_title'] . ($oneParent ? '' : " (#$counter)") . ' ';
    $parentOptions['settings']['header']['description'] = $parentData['Generated']['used_parent_label_for_display_description'];
    $parentOptions['data'] = $parentData;
    
    // CUSTOM CODE
    $hookLink = $this->Structures->hook();
    if ($hookLink) {
        require ($hookLink);
    }
    
    $this->Structures->build($finalParentStructure, $parentOptions);
    $this->request->data = [$k => $childrenData];
    $this->Structures->build($finalChildrenStructure, $childrenOptions);
}

echo $this->Html->script('actions');
?>
<script type="text/javascript">
    var copyControl = true;

    var compressAllFormData = '<?php echo $this->Structures->compressPostData(); ?>';

    var errorsGrid = <?php echo empty($errorsGrid) ? "''" : json_encode($errorsGrid); ?>;
</script>