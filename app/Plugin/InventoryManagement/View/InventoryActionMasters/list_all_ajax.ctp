<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

$structurelinks = [];
if ($displayIndex) {
    if ($displayIndex == 'Action') {
        $structurelinks = [
            'index' => [
                'detail' => '/InventoryManagement/InventoryActionMasters/detail/%%InventoryActionMaster.id%%',
                'edit' => '/InventoryManagement/InventoryActionMasters/edit/%%InventoryActionMaster.id%%',
                'delete' => '/InventoryManagement/InventoryActionMasters/delete/%%InventoryActionMaster.id%%',
            ],
        ];
    } elseif ($displayIndex == 'ViewAliquotUse') {
        $structurelinks = [
            'index' => [
                'detail' => '%%ViewAliquotUse.detail_url%%'
            ],
        ];
    }
}

$finalOptions = array(
    'type' => 'index',
    'links' => $structurelinks,
    'settings' => array(
        'language_heading' => isset($header)? $header : null,
        'pagination' => !isset($pagination),
        'actions' => false
    )
);

$this->Structures->build($atimStructure, $finalOptions);