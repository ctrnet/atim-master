<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

extract($dataForView);
$structureLinks = array(
    'top' => $links['submit'],
    'bottom' => array(
        'cancel' => $links['cancel']
    ),
);

$finalOptions = array(
    'links' => $structureLinks,
    'type' => 'add',
    'settings' => [
        'header' => array(
            'title' =>__('inventory action creation process') . ' - ' . __($actionControlData['type']),
            'description' => __('you are about to create event/annotation for %d element(s)', substr_count($this->data[0]['aliquotSampleIds'], ',') + 1)
        ),
        'form_bottom' => true,
        'actions' => true,
        'confirmation_msg' => __('batch_edit_confirmation_msg')
    ],
);

// CUSTOM CODE
$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}
$this->Structures->build($atimAddOneEventInBatchToMany, $finalOptions);