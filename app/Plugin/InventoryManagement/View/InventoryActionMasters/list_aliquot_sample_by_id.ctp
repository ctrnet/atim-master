<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

if ($reportType == 'master'){
    $this->request->data = $actionData;
    $finalOptions = array(
        'type' => 'index',
        'links' => array(
            'index' => array(
                'detail' => '/InventoryManagement/InventoryActionMasters/detail/%%InventoryActionMaster.id%%',
                'edit' => '/InventoryManagement/InventoryActionMasters/edit/%%InventoryActionMaster.id%%',
                'delete' => '/InventoryManagement/InventoryActionMasters/delete/%%InventoryActionMaster.id%%'
            )
        ),
        'settings' => array(
            'pagination' => false,
            'actions' => false
        )
    );

    $hookLink = $this->Structures->hook();
    if ($hookLink) {
        require ($hookLink);
    }

    $this->Structures->build($aliquotMastersStructures, $finalOptions);
}elseif ($reportType == 'detail'){
    foreach ($actionData as $key => $actionDatum){
        $this->request->data = $actionDatum;
        $finalOptions = array(
            'type' => 'index',
            'links' => array(
                'index' => array(
                    'detail' => '/InventoryManagement/InventoryActionMasters/detail/%%InventoryActionMaster.id%%',
                    'edit' => '/InventoryManagement/InventoryActionMasters/edit/%%InventoryActionMaster.id%%',
                    'delete' => '/InventoryManagement/InventoryActionMasters/delete/%%InventoryActionMaster.id%%'
                )
            ),
            'settings' => array(
                'pagination' => false,
                'actions' => false
            )
        );

        $finalOptions['settings']['language_heading'] = __(current($actionDatum)['InventoryActionControl']['category']);

        $hoolName = str_replace(" ", "_", current($actionDatum)['InventoryActionControl']['category']);
        $hookLink = $this->Structures->hook();
        if ($hookLink) {
            require ($hookLink);
        }
        $finalStructure = "detailAlias$key";
        $this->Structures->build($$finalStructure, $finalOptions);
    }
}