<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

$settings = array();

// Set links
$structureLinks = array(
    'index' => array(),
    'bottom' => array()
);
$colIdSampIdAlId = $atimMenuVariables['Collection.id'] . '/' . $atimMenuVariables['SampleMaster.id'] . '/' . $atimMenuVariables['AliquotMaster.id'];
$structureLinks['bottom']['edit'] = '/InventoryManagement/AliquotMasters/edit/' . $colIdSampIdAlId;
$structureLinks['bottom']['delete'] = '/InventoryManagement/AliquotMasters/delete/' . $colIdSampIdAlId;

if ($displayPrintBarcodesButton) {
    $structureLinks['bottom']['print barcode'] = array(
        'link' => '/InventoryManagement/AliquotMasters/printBarcodes/model:AliquotMaster/id:' . $atimMenuVariables['AliquotMaster.id'],
        'icon' => 'barcode'
    );
}

$structureLinks['bottom']['storage'] = '/underdevelopment/';
if (! empty($aliquotStorageData)) {
    $structureLinks['bottom']['storage'] = array(
        'plugin storagelayout access to storage' => array(
            "link" => '/StorageLayout/StorageMasters/detail/' . $aliquotStorageData['StorageMaster']['id'],
            "icon" => "storage"
        ),
        'remove from storage' => array(
            "link" => '/InventoryManagement/AliquotMasters/removeAliquotFromStorage/' . $colIdSampIdAlId,
            "icon" => "storage"
        )
    );
}
if (!in_array(Configure::read('order_item_type_config'), array('0', '3'))){
    $structureLinks['bottom']['add to order'] = array(
        "link" => '/Order/OrderItems/addOrderItemsInBatch/AliquotMaster/' . $atimMenuVariables['AliquotMaster.id'] . '/',
        "icon" => "add_to_order"
    );
}
$structureLinks['bottom']['add inventory actions'] = $actionsLink;

$structureLinks['bottom']['realiquoting'] = array(
    'realiquot' => array(
        "link" => '/InventoryManagement/AliquotMasters/realiquotInit/creation/' . $atimMenuVariables['AliquotMaster.id'],
        "icon" => "aliquot"
    ),
    'define realiquoted children' => array(
        "link" => '/InventoryManagement/AliquotMasters/realiquotInit/definition/' . $atimMenuVariables['AliquotMaster.id'],
        "icon" => "aliquot"
    )
);
$structureLinks['bottom']['create derivative'] = $canCreateDerivative ? '/InventoryManagement/SampleMasters/batchDerivativeInit/' . $atimMenuVariables['AliquotMaster.id'] : 'cannot';

if ($isFromTreeViewOrLayout == 1) {
    // Tree view
    $settings['header'] = __('aliquot', null) . ': ' . __('details');
} elseif ($isFromTreeViewOrLayout == 2) {
    // Storage Layout
    $structureLinks = array();
    $structureLinks['bottom']['access to aliquot'] = '/InventoryManagement/AliquotMasters/detail/' . $atimMenuVariables['Collection.id'] . '/' . $atimMenuVariables['SampleMaster.id'] . '/' . $atimMenuVariables['AliquotMaster.id'];
    if ($aliquotMasterData['Collection']['participant_id'])
        $structureLinks['bottom']['access to participant'] = '/ClinicalAnnotation/Participants/profile/' . $aliquotMasterData['Collection']['participant_id'];
    $settings['header'] = __('aliquot', null);
}

$finalAtimStructure = $atimStructure;
if ($isFromTreeViewOrLayout) {
    // DISPLAY ONLY ALIQUOT DETAIL FORM
    // 1- ALIQUOT DETAIL
    $finalOptions = array(
        'links' => $structureLinks,
        'data' => $aliquotMasterData,
        'settings' => $settings
    );
    
    // CUSTOM CODE
    $hookLink = $this->Structures->hook('aliquot_detail_1');
    if ($hookLink) {
        require ($hookLink);
    }
    
    // BUILD FORM
    $this->Structures->build($finalAtimStructure, $finalOptions);
} else {
    // DISPLAY BOTH ALIQUOT DETAIL FORM AND ALIQUOT USES LIST
    // 1- ALIQUOT DETAIL
    $finalOptions = array(
        'settings' => array(
            'actions' => false
        ),
        'data' => $aliquotMasterData
    );
    
    // CUSTOM CODE
    $hookLink = $this->Structures->hook('aliquot_detail_2');
    if ($hookLink) {
        require ($hookLink);
    }
    
    // BUILD FORM
    $this->Structures->build($finalAtimStructure, $finalOptions);
    
    // 2- EVENTS/ANNOTATIONS LIST
    $finalAtimStructure = $emptyStructure;

    $link = sprintf("<a href = '%s' target = '_top' class = 'open-in-window'>" . __('here') ."</a>", $this->request->webroot."InventoryManagement/InventoryActionMasters/listAll/$sampleType/{$atimMenuVariables['AliquotMaster.id']}");
    $languageHeading = __('for more details and access the all aliquot events annotations list click %s', $link);

    $finalOptions = array(
        'data' => array(),
        'settings' => array(
            'header' =>[
                'title' => __('uses and events'),
                'description' => $languageHeading,
            ],
            'actions' => false
        ),
        'extras' => $this->Structures->ajaxIndex("InventoryManagement/InventoryActionMasters/listAllAjax/viewaliquotuse/" . $atimMenuVariables['AliquotMaster.id'])
    );
    
    // CUSTOM CODE
    $hookLink = $this->Structures->hook('actions_list');
    if ($hookLink) {
        require ($hookLink);
    }
    
    $this->Structures->build([], $finalOptions);
    
    // 4 - REALIQUOTED PARENTS
    $dataUrl = sprintf('InventoryManagement/AliquotMasters/listAllRealiquotedParents/%d/%d/%d/', $atimMenuVariables['Collection.id'], $atimMenuVariables['SampleMaster.id'], $atimMenuVariables['AliquotMaster.id']);
    unset($structureLinks['index']);
    $finalAtimStructure = $emptyStructure;
    $finalOptions = array(
        'links' => $structureLinks,
        'settings' => array(
            'header' => __('realiquoted parent'),
            'actions' => 1
        ),
        'extras' => $this->Structures->ajaxIndex($dataUrl)
    );

    $hookLink = $this->Structures->hook('realiquoted_parent');
    if ($hookLink) {
        require ($hookLink);
    }

    $this->Structures->build($finalAtimStructure, $finalOptions);

}