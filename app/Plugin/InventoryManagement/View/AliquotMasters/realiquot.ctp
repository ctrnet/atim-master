<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

if ($isAjax) {
    ob_start();
}

 $options = array(
    "links" => array(
        "top" => "/InventoryManagement/AliquotMasters/realiquot/" . $aliquotId,
        'bottom' => array(
            'cancel' => $urlToCancel
        )
    )
);

$structureOverride = array();
if (isset($templateNodeDefaultValues)) {
    $templateNodeDefaultValues = array_filter($templateNodeDefaultValues, function ($var) {
        return (! ($var == '' || is_null($var)));
    });
    $structureOverride = array_merge($structureOverride, $templateNodeDefaultValues);
}

$args = AppController::getInstance()->passedArgs;
if (isset($args['templateInitId'])) {
    $templateInitDefaultValues = Set::flatten(AppController::getInstance()->Session->read('Template.init_data.' . $args['templateInitId']));
    $templateInitDefaultValues = array_filter($templateInitDefaultValues, function ($var) {
        return (! ($var == '' || is_null($var)));
    });
    $structureOverride = array_merge($structureOverride, $templateInitDefaultValues);
}
//$structureOverride = array_merge($structureOverride, $parentSampleDataOverride);

$optionsParent = array_merge($options, array(
    "type" => "edit",
    'override' => $structureOverride,
    "settings" => array(
        "actions" => false,
        "form_top" => false,
        "form_bottom" => false,
        "stretch" => false,
        'section_start' => true
    )
));
$optionsChildren = array_merge($options, array(
    "type" => "addgrid",
    'override' => $structureOverride,
    "settings" => array(
        "add_fields" => true,
        "del_fields" => true,
        "actions" => false,
        "form_top" => false,
        "form_bottom" => false,
        'section_end' => true
    )
));

// CUSTOM CODE
$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}

// Display empty structure with hidden fields to fix issue#2243 : Derivative in batch: control id not posted when last record is hidden
$structureOptions = $optionsParent;
$structureOptions['settings']['form_top'] = true;
$structureOptions['settings']['section_start'] = false;
$structureOptions['settings']['header'] = array(
    'title' => __('realiquoting process'),
    'description' => null
);
$structureOptions['data'] = array();
$structureOptions['extras'] = '<input type="hidden" name="data[ids]" value="' . $parentAliquotsIds . '"/>
		<input type="hidden" name="data[sample_ctrl_id]" value="' . $sampleCtrlId . '"/>
		<input type="hidden" name="data[realiquot_from]" value="' . $realiquotFrom . '"/>
		<input type="hidden" name="data[realiquot_into]" value="' . $realiquotInto . '"/>
		<input type="hidden" name="data[Realiquoting][lab_book_master_code]" value="' . $labBookCode . '"/>
		<input type="hidden" name="data[Realiquoting][sync_with_lab_book]" value="' . $syncWithLabBook . '"/>
		<input type="hidden" name="data[url_to_cancel]" value="' . $urlToCancel . '"/>';
$structureToUse = $emptyStructure;

if ($displayBatchProcessAliqStorageAndInStockDetails) {
    // Form to aplly data to all parents
    $structureOptions['settings']['header'] = array(
        'title' => __('realiquoting process') . ': ' . __("children aliquot creation"),
        'description' => ''
    );
    $structureOptions['settings']['language_heading'] = __('data to be applied to all aliquots used -- fields values of the section below will be applied to all other sections if entered and will replace sections fields values');
    $structureToUse = $batchProcessAliqStorageAndInStockDetails;
}

$hookLink = $this->Structures->hook('apply_to_all');

$this->Structures->build($structureToUse, $structureOptions);

// print the layout

$hookLink = $this->Structures->hook('loop');

$counter = 0;
$oneParent = (sizeof($this->request->data) == 1) ? true : false;
$finalParentStructure = null;
$finalChildrenStructure = null;
while ($data = array_shift($this->request->data)) {
    $counter ++;
    $parent = $data['parent'];
    $finalOptionsParent = $optionsParent;
    $finalOptionsChildren = $optionsChildren;
    $finalOptionsParent['settings']['header']['title'] = $parent['Generated']['used_parent_label_for_display_title'] . ($oneParent ? '' : " (#$counter)") . ' ';
    $finalOptionsParent['settings']['header']['description'] = $parent['Generated']['used_parent_label_for_display_description'];
    $finalOptionsParent['settings']['name_prefix'] = $parent['AliquotMaster']['id'];
    $finalOptionsParent['data'] = $parent;
    
    $finalOptionsChildren['settings']['name_prefix'] = $parent['AliquotMaster']['id'];

    $finalOptionsChildren['override'] = array_merge($createdAliquotStructureOverride, $structureOverride);
    if(isset($createdAliquotStructureOverridePerParentAliquot[$parent['AliquotMaster']['id']])) {
        $finalOptionsChildren["override"] = array_merge($finalOptionsChildren["override"], $createdAliquotStructureOverridePerParentAliquot[$parent['AliquotMaster']['id']]);
    }
    $finalOptionsChildren['data'] = $data['children'];
    
    $finalParentStructure = $inStockDetail;
    
    if ($hookLink) {
        require ($hookLink);
    }
    
    $this->Structures->build($finalParentStructure, $finalOptionsParent);
    $this->Structures->build($atimStructure, $finalOptionsChildren);
}

// Display empty structure with hidden fields max_input_vars_validation to fix issue when max_input_vars is too small (issue#3729 : Unable to create 200 aliquots from 200 samples)
$emptyOptions = array(
    'type' => 'edit',
    'settings' => array(
        "form_top" => false,
        'confirmation_msg' => __('multi_entry_form_confirmation_msg')
    ),
    'data' => array(),
    'links' => $options['links'],
    'extras' => '
		<input type="hidden" name="data[max_input_vars_validation]" value="1" class = "max_input_vars_validation" />'
);
$this->Structures->build(array(), $emptyOptions);

echo $this->Html->script('actions');

?>

<script type="text/javascript">
var copyStr = "<?php echo(__("copy", null)); ?>";
var pasteStr = "<?php echo(__("paste")); ?>";
var copyingStr = "<?php echo(__("copying")); ?>";
var pasteOnAllLinesStr = "<?php echo(__("paste on all lines")); ?>";
var copyControl = true;
var compressAllFormData = '<?php echo $this->Structures->compressPostData(); ?>';
var labBookFields = new Array("<?php echo implode('", "', $labBookFields); ?>");
var labBookHideOnLoad = true;

var popUpStorageLayout = true;

var errorsGrid = <?php echo empty($errorsGrid) ? "''" : json_encode($errorsGrid); ?>;
</script>

<?php
if ($isAjax) {
    $display = ob_get_contents();
    ob_end_clean();
    $this->layout = 'json';
    $this->json = array(
        'goToNext' => false,
        'page' => $display,
        'id' => null
    );
}