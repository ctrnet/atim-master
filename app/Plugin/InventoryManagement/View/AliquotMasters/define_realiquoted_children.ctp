<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

$structureLinks = array(
    'top' => '/InventoryManagement/AliquotMasters/defineRealiquotedChildren/' . $aliquotId,
    'bottom' => array(
        'cancel' => $urlToCancel
    )
);

$structureSettings = array(
    'pagination' => false,
    'form_top' => false,
    'form_bottom' => false,
    'actions' => false
);

$finalAtimStructure = $atimStructureForChildrenAliquotsSelection;
$options = array(
    'type' => 'editgrid',
    'links' => $structureLinks,
    'settings' => $structureSettings
);
$parentOptions = array_merge($options, array(
    "type" => "edit",
    "settings" => array_merge($structureSettings, array(
        "stretch" => false,
        'section_start' => true
    ))
));

$childrenOptions = array_merge($options, array(
    'type' => 'addgrid',
    'links' => $structureLinks,
    'settings' => array_merge($structureSettings, array(
        'section_end' => true
    ))
));

// CUSTOM CODE
$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}

// Display empty structure with hidden fields to fix issue#2243 : Derivative in batch: control id not posted when last record is hidden
$structureOptions = $options;
$structureOptions['type'] = "edit";
$structureOptions['settings']['form_top'] = true;
$structureOptions['settings']['section_start'] = false;
$structureOptions['settings']['header'] = array(
    'title' => __('realiquoting process') . ': ' . __("children aliquot selection"),
    'description' => null
);
$structureOptions['data'] = array();

$structureOptions['extras'] = '<input type="hidden" name="data[ids]" value="' . $parentAliquotsIds . '"/>
		<input type="hidden" name="data[sample_ctrl_id]" value="' . $sampleCtrlId . '"/>
		<input type="hidden" name="data[realiquot_from]" value="' . $realiquotFrom . '"/>
		<input type="hidden" name="data[realiquot_into]" value="' . $realiquotInto . '"/>
		<input type="hidden" name="data[Realiquoting][lab_book_master_code]" value="' . $labBookCode . '"/>
		<input type="hidden" name="data[Realiquoting][sync_with_lab_book]" value="' . $syncWithLabBook . '"/>
		<input type="hidden" name="data[url_to_cancel]" value="' . $urlToCancel . '"/>';
$structureToUse = $emptyStructure;

if ($displayBatchProcessAliqStorageAndInStockDetails) {
    // Form to aplly data to all parents
    $structureOptions['settings']['language_heading'] = __('data to be applied to all aliquots used -- fields values of the section below will be applied to all other sections if entered and will replace sections fields values');
    $structureToUse = $batchProcessAliqStorageAndInStockDetails;
}

$hookLink = $this->Structures->hook('apply_to_all');

$this->Structures->build($structureToUse, $structureOptions);

// BUILD FORM
$hookLink = $this->Structures->hook('loop');
$counter = 0;
$elementNbr = sizeof($this->request->data);
foreach ($this->request->data as $aliquot) {
    $counter ++;
    
    $finalParentOptions = $parentOptions;
    $finalChildrenOptions = $childrenOptions;
    $finalParentOptions['settings']['header']['title'] = $aliquot['parent']['Generated']['used_parent_label_for_display_title'] . (empty($aliquotId) ? " (#$counter)" : '') . ' ';
    $finalParentOptions['settings']['header']['description'] = $aliquot['parent']['Generated']['used_parent_label_for_display_description'];
    $finalParentOptions['settings']['name_prefix'] = $aliquot['parent']['AliquotMaster']['id'];
    $finalParentOptions['data'] = $aliquot['parent'];
    $finalChildrenOptions['settings']['name_prefix'] = $aliquot['parent']['AliquotMaster']['id'];
    $finalChildrenOptions['data'] = $aliquot['children'];
    
    if ($hookLink) {
        require ($hookLink);
    }
    
    $this->Structures->build($inStockDetail, $finalParentOptions);
    $this->Structures->build($finalAtimStructure, $finalChildrenOptions);
}

// Display empty structure with hidden fields max_input_vars_validation to fix issue when max_input_vars is too small (issue#3729 : Unable to create 200 aliquots from 200 samples)
$emptyOptions = array(
    'type' => 'edit',
    'settings' => array(
        "form_top" => false,
        'confirmation_msg' => __('multi_entry_form_confirmation_msg')
    ),
    'data' => array(),
    'links' => $options['links'],
    'extras' => '
		<input type="hidden" name="data[max_input_vars_validation]" value="1" class = "max_input_vars_validation" />'
);
$this->Structures->build(array(), $emptyOptions);
?>

<div id="debug"></div>
<script type="text/javascript">
var copyStr = "<?php echo(__("copy", null)); ?>";
var pasteStr = "<?php echo(__("paste")); ?>";
var copyingStr = "<?php echo(__("copying")); ?>";
var pasteOnAllLinesStr = "<?php echo(__("paste on all lines")); ?>";
var copyControl = true;
var compressAllFormData = '<?php echo $this->Structures->compressPostData(); ?>';
var labBookFields = new Array("<?php echo implode('", "', $labBookFields); ?>");
var labBookHideOnLoad = true;
</script>