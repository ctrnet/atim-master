<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

if ($isAjax) {
    ob_start();
}

if (isset($this->request->data[0]['parent']['AliquotMaster'])) {
    $childStructureToUse = $sourcealiquots;
    // merging parent structures
    if (! empty($this->request->data[0]['parent']['AliquotControl']['volume_unit'])) {
        // volume structure
        $parentStructure['Structure'] = array_merge(array(
            $parentStructure['Structure']
        ), $childStructureToUse['Structure']);
        $derivativeStructure = $derivativeVolumeStructure;
    } else {
        $parentStructure['Structure'] = array(
            $parentStructure['Structure'],
            $childStructureToUse['Structure']
        );
    }
    $parentStructure['Sfs'] = array_merge($parentStructure['Sfs'], $childStructureToUse['Sfs']);
}

// structure options
$options = array(
    "links" => array(
        "top" => '/InventoryManagement/SampleMasters/batchDerivative/' . $aliquotMasterId,
        'bottom' => array(
            'cancel' => $urlToCancel
        )
    )
);

$optionsParent = array_merge($options, array(
    "type" => "edit",
    "settings" => array(
        "actions" => false,
        "form_top" => false,
        "form_bottom" => false,
        "stretch" => false,
        'section_start' => true
    )
));

$structureOverride = array();
if (isset($templateNodeDefaultValues)) {
    $templateNodeDefaultValues = array_filter($templateNodeDefaultValues, function ($var) {
        return (! ($var == '' || is_null($var)));
    });
    $structureOverride = array_merge($structureOverride, $templateNodeDefaultValues);
}
$args = AppController::getInstance()->passedArgs;
if (isset($args['templateInitId'])) {
    $templateInitDefaultValues = Set::flatten(AppController::getInstance()->Session->read('Template.init_data.' . $args['templateInitId']));
    $templateInitDefaultValues = array_filter($templateInitDefaultValues, function ($var) {
        return (! ($var == '' || is_null($var)));
    });
    $structureOverride = array_merge($structureOverride, $templateInitDefaultValues);
}

$optionsChildren = array_merge($options, array(
    "type" => "addgrid",
    "settings" => array(
        "add_fields" => true,
        "del_fields" => true,
        "actions" => false,
        "form_top" => false,
        "form_bottom" => false,
        'section_end' => true
    ),
    "override" => array_merge($structureOverride, $createdSampleStructureOverride)
));

$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}

// Display empty structure with hidden fields to fix issue#2243 : Derivative in batch: control id not posted when last record is hidden
// Plus display action title
$structureOptions = $optionsParent;
$structureOptions['settings']['form_top'] = true;
$structureOptions['settings']['section_start'] = false;
$structureOptions['settings']['header'] = array(
    'title' => __('derivative creation process') . ' - ' . __('creation'),
    'description' => null
);
$structureOptions['data'] = array();
$structureOptions['extras'] = '
	<input type="hidden" name="data[SampleMaster][sample_control_id]" value="' . $childrenSampleControlId . '"/>
	<input type="hidden" name="data[DerivativeDetail][lab_book_master_code]" value="' . $labBookMasterCode . '"/>
	<input type="hidden" name="data[DerivativeDetail][sync_with_lab_book]" value="' . $syncWithLabBook . '"/>
	<input type="hidden" name="data[ParentToDerivativeSampleControl][parent_sample_control_id]" value="' . $parentSampleControlId . '"/>
	<input type="hidden" name="data[url_to_cancel]" value="' . $urlToCancel . '"/>
	<input type="hidden" name="data[sample_master_ids]" value="' . $sampleMasterIds . '"/>';
$structureToUse = $emptyStructure;

if ($displayBatchProcessAliqStorageAndInStockDetails) {
    // Form to aplly data to all parents
    $structureOptions['settings']['language_heading'] = __('data to be applied to all aliquots used -- fields values of the section below will be applied to all other sections if entered and will replace sections fields values');
    $structureToUse = $batchProcessAliqStorageAndInStockDetails;
}

$hookLink = $this->Structures->hook('apply_to_all');

$this->Structures->build($structureToUse, $structureOptions);

// print the layout

$hookLink = $this->Structures->hook('loop');
$counter = 0;
$oneParent = (sizeof($this->request->data) == 1) ? true : false;
while ($data = array_shift($this->request->data)) {
    $counter ++;
    $parent = $data['parent'];
    
    $finalOptionsParent = $optionsParent;
    $finalOptionsChildren = $optionsChildren;
    
    $prefix = isset($parent['AliquotMaster']) ? $parent['AliquotMaster']['id'] : $parent['ViewSample']['sample_master_id'];
    
    $finalOptionsParent['settings']['header']['title'] = $parent['Generated']['used_parent_label_for_display_title'] . ($oneParent ? '' : " (#$counter)") . ' ';
    $finalOptionsParent['settings']['header']['description'] = $parent['Generated']['used_parent_label_for_display_description'];
    $finalOptionsParent['settings']['name_prefix'] = $prefix;
    $finalOptionsParent['data'] = $parent;
    
    $finalOptionsChildren['settings']['name_prefix'] = $prefix;
    $finalOptionsChildren['data'] = $data['children'];
    if (isset($parent['AliquotMaster']) && ! empty($parent['AliquotControl']['volume_unit'])) {
        $finalOptionsChildren['override']['AliquotControl.volume_unit'] = $parent['AliquotControl']['volume_unit'];
    }
    
    if ($hookLink) {
        require ($hookLink);
    }
    
    $this->Structures->build($parentStructure, $finalOptionsParent);
    $this->Structures->build($derivativeStructure, $finalOptionsChildren);
}

// Display empty structure with hidden fields max_input_vars_validation to fix issue when max_input_vars is too small (issue#3729 : Unable to create 200 aliquots from 200 samples)
$emptyOptions = array(
    'type' => 'edit',
    'settings' => array(
        "form_top" => false,
        'confirmation_msg' => __('multi_entry_form_confirmation_msg')
    ),
    'data' => array(),
    'links' => $options['links'],
    'extras' => '
		<input type="hidden" name="data[max_input_vars_validation]" value="1" class = "max_input_vars_validation" />'
);
$this->Structures->build(array(), $emptyOptions);

echo $this->Html->script('actions');

?>
<script type="text/javascript">
var copyStr = "<?php echo(__("copy", null)); ?>";
var pasteStr = "<?php echo(__("paste")); ?>";
var copyingStr = "<?php echo(__("copying")); ?>";
var pasteOnAllLinesStr = "<?php echo(__("paste on all lines")); ?>";
var compressAllFormData = '<?php echo $this->Structures->compressPostData(); ?>';
var copyControl = true;
var labBookFields = new Array("<?php echo is_array($labBookFields) ? implode('", "', $labBookFields) : ""; ?>");
var labBookHideOnLoad = true;

var errorsGrid = <?php echo empty($errorsGrid) ? "''" : json_encode($errorsGrid); ?>;

</script>
<?php
if ($isAjax) {
    $display = ob_get_contents();
    ob_end_clean();
    $this->layout = 'json';
    $this->json = array(
        'goToNext' => false,
        'page' => $display,
        'id' => null,
    );
}