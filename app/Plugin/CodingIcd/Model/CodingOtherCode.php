<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class CodingOtherCode
 */
class CodingOtherCode extends CodingIcdAppModel
{

    protected static $singleton = null;

    public $name = 'CodingOtherCode';

    public $useTable = 'coding_other_codes';

    public $icdDescriptionTableFields = array(
        'search_format' => array(
            'description',
            'code_system'
        ),
        'detail_format' => array(
            'description',
            'code_system'
        )
    );

    public $validate = array();

    protected static $valueSetName = null;

    /**
     * CodingOtherCode constructor.
     */
    public function __construct()
    {
        parent::__construct();
        self::$singleton = $this;
    }

    public static function setValueSetName($otherCodeValueSetName)
    {
        self::$valueSetName = $otherCodeValueSetName;
    }

    /**
     *
     * @param $id
     * @return bool
     */
    public static function validateId($id)
    {
        return self::$singleton->globalValidateId($id);
    }

    /**
     *
     * @return CodingOtherCode|null
     */
    public static function getSingleton()
    {
        return self::$singleton;
    }

    /**
     *
     * @param array $queryData
     * @return array
     */
    public function beforeFind($queryData)
    {
        if (self::$valueSetName) {
            $queryData['conditions'] = array(
                'AND' => array(
                    $queryData['conditions'],
                    array(
                        "CodingOtherCode.value_set_name = '" . self::$valueSetName . "'"
                    )
                )
            );
        }
        return $queryData;
    }

    /**
     *
     * @param $otherCodeValueSetName Value
     *            Set Name
     * @return array OtherCode Value Set Details
     */
    public function getValueSetDetails($otherCodeValueSetName)
    {
        $otherCodeValueSetName = is_array($otherCodeValueSetName) ? array_shift($otherCodeValueSetName) : $otherCodeValueSetName;
        $lang = Configure::read('Config.language') == "eng" ? "en" : "fr";
        $conditions = array(
            "CodingOtherCode.value_set_name = '$otherCodeValueSetName'"
        );
        $valueSetDetails = array();
        foreach ($this->find('all', array('conditions' => $conditions)) as $newCodeSystem) {
            $valueToDisplay = $newCodeSystem['CodingOtherCode'][$lang . '_description'];
            if (! strlen($valueToDisplay)) {
                $valueToDisplay = $newCodeSystem['CodingOtherCode'][($lang == 'en'? 'fr' : 'en') . '_description'];
            }
            $valueSetDetails[$newCodeSystem['CodingOtherCode']['id']] = $newCodeSystem['CodingOtherCode']['id'] . (strlen($valueToDisplay)? ' - ' . $valueToDisplay : '');
        }
        asort($valueSetDetails);
        return $valueSetDetails;
    }
}