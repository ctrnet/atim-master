<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

?>
<div id="me">
<?php
$this->Structures->build($atimStructure, array(
    'type' => 'search',
    'links' => array(
        'top' => '/'
    ),
    'settings' => array(
        'header' => __('search for a %s code', $otherCodeValueSetName),
        'actions' => false
    )
));
?>
<div id="results"></div>
<?php
$this->Structures->build($empty, array(
    'type' => 'search',
    'settings' => array(
        'form_top' => false
    )
));
?>
</div>
<script type="text/javascript">
var popupSearch = function(){
	$.post(root_url + "CodingIcd/CodingOtherCodes/search/<?php echo($otherCodeValueSetName); ?>/1", $("#me form").serialize(), function(data){
            data = normaliseData(data);
            
		$("#default_popup").html("<div class='wrapper'><div class='frame'>" + data + "</div></div>").popup();
	});
	return false;
};

$(function(){
	$("#me .adv_ctrl").hide();
	$("#me .form.search").off('click').attr("onclick", null).on("click", popupSearch);
	$("#me form").on("submit", popupSearch);
	$("#me div.search-result-div").hide();
});

</script>