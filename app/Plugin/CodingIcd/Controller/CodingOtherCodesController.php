<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class CodingOtherCodesController
 */
class CodingOtherCodesController extends CodingIcdAppController
{

    public $uses = array(
        "CodingIcd.CodingOtherCode"
    );

    public $icdDescriptionTableFields = array(
        'description'
    );

    /*
     * Forms Helper appends a "tool" link to the "add" and "edit" form types
     * Clicking that link reveals a DIV tag with this Action/View that should have functionality to affect the indicated form field.
     */
    /**
     *
     * @param $otherCodeValueSetName
     */
    public function tool($otherCodeValueSetName)
    {
        parent::tool($otherCodeValueSetName);
        $this->set("otherCodeValueSetName", $otherCodeValueSetName);
    }

    /**
     *
     * @param string $otherCodeValueSetName
     * @param bool $isTool
     */
    public function search($otherCodeValueSetName, $isTool = true)
    {
        $this->CodingOtherCode->setValueSetName($otherCodeValueSetName);
        parent::globalSearch($isTool, $this->CodingOtherCode);
        $this->set("otherCodeValueSetName", $otherCodeValueSetName);
    }

    /**
     *
     * @param string $otherCodeValueSetName
     */
    public function autoComplete($otherCodeValueSetName)
    {
        $this->CodingOtherCode->setValueSetName($otherCodeValueSetName);
        parent::globalAutocomplete($this->CodingOtherCode);
    }
}