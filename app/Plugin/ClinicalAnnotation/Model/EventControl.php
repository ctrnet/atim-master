<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class EventControl
 */
class EventControl extends ClinicalAnnotationAppModel
{
    public $controlType = 'event_type';
    
    public $masterFormAlias = 'eventmasters';

    /**
     * Get permissible values array gathering all existing event disease sites.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getEventDiseaseSitePermissibleValues()
    {
        $result = array();
        
        $structureValueDomainModel = AppModel::getInstance('', 'StructureValueDomain', true);
        
        // Build tmp array to sort according translation
        foreach ($this->find('all', array(
            'conditions' => array(
                'flag_active = 1'
            )
        )) as $eventControl) {
            $result[$eventControl['EventControl']['disease_site']] = $structureValueDomainModel->getValueToDisplay('event_disease_site_list', $eventControl['EventControl']['disease_site']);
        }
        natcasesort($result);

        return $result;
    }

    /**
     *
     * @return array
     */
    public function getEventGroupPermissibleValues()
    {
        $result = array();

        // Build tmp array to sort according translation
        foreach ($this->find('all', array(
            'conditions' => array(
                'flag_active = 1'
            )
        )) as $eventControl) {
            $result[$eventControl['EventControl']['event_group']] = __($eventControl['EventControl']['event_group']);
        }
        natcasesort($result);

        return $result;
    }

    /**
     * Get permissible values array gathering all existing event types.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getEventTypePermissibleValues()
    {
        $result = array();

        // Build tmp array to sort according translation
        foreach ($this->find('all', array(
            'conditions' => array(
                'flag_active = 1'
            )
        )) as $eventControl) {
            $result[$eventControl['EventControl']['event_type']] = __($eventControl['EventControl']['event_type']);
        }
        natcasesort($result);

        return $result;
    }

    /**
     *
     * @param $eventCtrlData
     * @param $participantId
     * @param $eventGroup
     * @return array
     */
    public function buildAddLinks($eventCtrlData, $participantId, $eventGroup)
    {
        $structureValueDomainModel = AppModel::getInstance('', 'StructureValueDomain', true);
        
        $links = array();
        foreach ($eventCtrlData as $eventCtrl) {
            if ($eventCtrl['EventControl']['flag_active_input']){
                $links[] = array(
                    'order' => $eventCtrl['EventControl']['display_order'],
                    'label' => __($eventCtrl['EventControl']['event_type']) . (empty($eventCtrl['EventControl']['disease_site']) ? '' : ' - ' . $structureValueDomainModel->getValueToDisplay('event_disease_site_list', $eventCtrl['EventControl']['disease_site'])),
                    'link' => '/ClinicalAnnotation/EventMasters/add/' . $participantId . '/' . $eventCtrl['EventControl']['id']
                );
            }
        }
        AppController::buildBottomMenuOptions($links);
        return $links;
    }

    /**
     *
     * @param mixed $results
     * @param bool $primary
     * @return mixed
     */
    public function afterFind($results, $primary = false)
    {
        return $this->applyMasterFormAlias($results, $primary);
    }
    
    public function setDataBeforeSaveFB(&$data)
    {
        $prefix = getPrefix();
        $model = $this->name;
        $maxDisplayOrder = $this->find("first", array(
            'fields'=>array("display_order"),
            'order'=>array("display_order DESC")
        ));

        if (!isset($data[$model][$this->controlType])){
            return;
        }

        $name = $data[$model][$this->controlType];
        $detailTableName = createTableName($prefix . 'ev', time(), $name);

        $data[$model]["detail_tablename"] = strtolower($detailTableName);
        $data[$model]["detail_form_alias"] = $detailTableName;
        $data[$model]["flag_active"] = 0;
        $data[$model]["display_order"] = empty($maxDisplayOrder[$model]["display_order"]) ? 1 : $maxDisplayOrder[$model]["display_order"] + 1;
        $data[$model]["flag_test_mode"] = '1';

        $this->addWritableField();
    }
    
    public function setDataBeforeEditFB(&$data)
    {
        $controlItem = $this->getOrRedirectFB($this->id);
        $this->dataToDic($controlItem);

        if (isset($data[$this->name]) && !empty($data[$this->name])){
            foreach ($data[$this->name] as $key=>$value) {
                $controlItem[$this->name][$key] = $value;
            }
        }        
        $data[$this->name] = $controlItem[$this->name];
        
        if (empty($data[$this->name]['detail_form_alias']) && !empty($data['detailTest'])){
            $prefix = getPrefix();
            $model = $this->name;

            if (!isset($data[$model][$this->controlType])){
                return;
            }
            $name = $data[$model][$this->controlType];
            $detailTableName = createTableName($prefix . 'ev', time(), $name);

            $data[$model]["detail_form_alias"] = (!empty(trim($data[$model]["detail_form_alias"]))) ? implode(",", array_merge(explode(",", $data[$model]["detail_form_alias"]), array($detailTableName))) : $detailTableName;
            $this->addWritableField();

        }
    }
    
    public function validates($options = array()) 
    {
        $data = $this->find('all', array(
            'conditions' => array(
                $this->name.'.'.$this->controlType.' LIKE ' => '% || '.$this->data[$this->name][$this->controlType]
            )
        ));

        if (!empty($data) && (( countCustom($data) ==1 && $data[0][$this->name]['id'] != $this->id) || countCustom($data) > 1)){
            $this->validationErrors[$this->controlType][] = __("this type has already existed");
        }
        
        return parent::validates($options);    
    }
    
    public function addWritableField($field = array(), $tablename = null)
    {
        if (!is_array($field)){
            $field = array($field);
        }
        parent::addWritableField(array_merge($field, array("event_type", "disease_site", "event_group", "flag_active", "detail_form_alias", "detail_tablename", "display_order", "flag_use_for_ccl", "use_addgrid", "use_detail_form_for_index", "flag_test_mode", "flag_form_builder", "flag_active_input")), $tablename);
    }
    
    public function getName()
    {
        return __('event');
    }
    
}