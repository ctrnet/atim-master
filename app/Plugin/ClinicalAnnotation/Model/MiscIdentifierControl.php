<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class MiscIdentifierControl
 */
class MiscIdentifierControl extends ClinicalAnnotationAppModel
{

    private $confidentialIds = null;
    public $controlType = 'misc_identifier_name';

    /**
     * Get permissible values array gathering all existing misc identifier names.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getMiscIdentifierNamePermissibleValues($linkableonly = false)
    {
        $result = array();
        
        $confitions = [
            'flag_active = 1'
        ];
        if ($linkableonly)
        {
            $confitions['MiscIdentifierControl.flag_use_for_ccl'] = '1';
        }
        
        // Build tmp array to sort according translation
        foreach ($this->find('all', array(
            'conditions' => $confitions
        )) as $identCtrl) {
            $result[$identCtrl['MiscIdentifierControl']['misc_identifier_name']] = __($identCtrl['MiscIdentifierControl']['misc_identifier_name']);
        }
        natcasesort($result);

        return $result;
    }
    
    /**
     * Get permissible values array gathering all existing misc identifier names linkable to collection.
     *
     * @author N. Luc
     * @since 2023-02-10
     * @updated N. Luc
     */
    public function getMiscIdentifierNameLincakbleToColPermissibleValues()
    {
        return $this->getMiscIdentifierNamePermissibleValues(true);
    }
    

    /**
     * Get permissible values array gathering all existing misc identifier names.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getMiscIdentifierNamePermissibleValuesFromId()
    {
        $result = array();

        // Build tmp array to sort according translation
        foreach ($this->find('all', array(
            'conditions' => array(
                'flag_active = 1'
            )
        )) as $identCtrl) {
            $result[$identCtrl['MiscIdentifierControl']['id']] = __($identCtrl['MiscIdentifierControl']['misc_identifier_name']);
        }
        natcasesort($result);

        return $result;
    }

    /**
     *
     * @return array|null
     */
    public function getConfidentialIds()
    {
        if ($this->confidentialIds == null) {
            $miscControls = $this->find('all', array(
                'fields' => array(
                    'MiscIdentifierControl.id'
                ),
                'conditions' => array(
                    'flag_confidential' => 1
                )
            ));
            $this->confidentialIds = array();
            foreach ($miscControls as $miscControl) {
                $this->confidentialIds[] = $miscControl['MiscIdentifierControl']['id'];
            }
        }
        return $this->confidentialIds;
    }

    public function setDataBeforeSaveFB(&$data)
    {
        $prefix = getPrefix();
        $model = $this->name;
        $maxDisplayOrder = $this->find("first", array(
            'fields'=>array("display_order"),
            'order'=>array("display_order DESC")
        ));

        if (!isset($data[$model][$this->controlType])){
            return;
        }

        $name = $data[$model][$this->controlType];
        $detailTableName = createTableName($prefix . 'id', time(), $name);

        $data[$model]["detail_form_alias"] = $detailTableName;
        $data[$model]["flag_active"] = 0;
        $data[$model]["display_order"] = empty($maxDisplayOrder[$model]["display_order"]) ? 1 : $maxDisplayOrder[$model]["display_order"] + 1;
        $data[$model]["flag_test_mode"] = '1';
        $this->addWritableField();
    }

    public function setDataBeforeEditFB(&$data)
    {
        $controlItem = $this->getOrRedirectFB($this->id);
        $this->dataToDic($controlItem);

        if (isset($data[$this->name]) && !empty($data[$this->name])){
            foreach ($data[$this->name] as $key=>$value) {
                $controlItem[$this->name][$key] = $value;
            }
        }
        $data[$this->name] = $controlItem[$this->name];

        if (empty($data[$this->name]['detail_form_alias']) && !empty($data['detailTest'])){
            $prefix = getPrefix();
            $model = $this->name;

            if (!isset($data[$model][$this->controlType])){
                return;
            }
            $name = $data[$model][$this->controlType];
            $detailTableName = createTableName($prefix . 'id', time(), $name);

            $data[$model]["detail_form_alias"] = (!empty(trim($data[$model]["detail_form_alias"]))) ? implode(",", array_merge(explode(",", $data[$model]["detail_form_alias"]), array($detailTableName))) : $detailTableName;
            $this->addWritableField();

        }elseif($this->name == 'MiscIdentifierControl'){
            $prefix = getPrefix();

            if (!isset($data[$this->name][$this->controlType])){
                return;
            }
            $name = $data[$this->name][$this->controlType];
            $data[$this->name]["detail_form_alias"] = createTableName($prefix . 'id', time(), $name);
        }
    }

    public function validates($options = array())
    {
        $data = $this->find('all', array(
            'conditions' => array(
                $this->name.'.'.$this->controlType.' LIKE ' => '% || '.$this->data[$this->name][$this->controlType]
            )
        ));

        if (!empty($data) && (( countCustom($data) ==1 && $data[0][$this->name]['id'] != $this->id) || countCustom($data) > 1)){
            $this->validationErrors[$this->controlType][] = __("this type has already existed");
        }

        return parent::validates($options);
    }

    public function addWritableField($field = array(), $tablename = null)
    {
        if (!is_array($field)){
            $field = array($field);
        }
        parent::addWritableField(array_merge($field, array("misc_identifier_name", "flag_active", "display_order", "autoincrement_name", "detail_tablename", "misc_identifier_format", "flag_once_per_participant", "flag_once_per_participant", "flag_confidential", "flag_unique", "pad_to_length", "reg_exp_validation", "user_readable_format", "flag_link_to_study", "flag_test_mode", "flag_form_builder", "flag_active_input")), $tablename);
    }

    public function getName()
    {
        return __('misc identifiers');
    }
    
}