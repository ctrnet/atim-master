<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class DiagnosisControl
 */
class DiagnosisControl extends ClinicalAnnotationAppModel
{

    public $masterFormAlias = 'diagnosismasters';
    public $controlType = 'controls_type';

    /**
     * Get permissible values array gathering all existing diagnosis types.
     *
     * @author N. Luc
     * @since 2010-05-26
     *        @updated N. Luc
     */
    public function getDiagnosisTypePermissibleValuesFromId()
    {
        $result = array();

        // Build tmp array to sort according translation
        foreach ($this->find('all', array(
            'conditions' => array(
                'flag_active = 1'
            )
        )) as $diagnosisControl) {
            $result[$diagnosisControl['DiagnosisControl']['id']] = __($diagnosisControl['DiagnosisControl']['controls_type']);
        }
        natcasesort($result);

        return $result;
    }

    /**
     *
     * @return array
     */
    public function getTypePermissibleValues()
    {
        $result = array();

        // Build tmp array to sort according translation
        foreach ($this->find('all', array(
            'conditions' => array(
                'flag_active = 1'
            )
        )) as $diagnosisControl) {
            $result[$diagnosisControl['DiagnosisControl']['controls_type']] = __($diagnosisControl['DiagnosisControl']['controls_type']);
        }
        natcasesort($result);

        return $result;
    }

    /**
     *
     * @return array
     */
    public function getCategoryPermissibleValues()
    {
        $result = array();

        // Build tmp array to sort according translation
        foreach ($this->find('all', array(
            'conditions' => array(
                'flag_active = 1'
            )
        )) as $diagnosisControl) {
            $result[$diagnosisControl['DiagnosisControl']['category']] = __($diagnosisControl['DiagnosisControl']['category']);
        }
        natcasesort($result);

        return $result;
    }

    /**
     *
     * @param mixed $results
     * @param bool $primary
     * @return mixed
     */
    public function afterFind($results, $primary = false)
    {
        return $this->applyMasterFormAlias($results, $primary);
    }
    
    public function setDataBeforeSaveFB(&$data)
    {
        $prefix = getPrefix();
        $model = $this->name;
        $maxDisplayOrder = $this->find("first", array(
            'fields'=>array("display_order"),
            'order'=>array("display_order DESC")
        ));

        $name = $data[$model][$this->controlType];
        $detailTableName = createTableName($prefix . 'dx', time(), $name);

        $data[$model]["detail_tablename"] = strtolower($detailTableName);
        $data[$model]["detail_form_alias"] = $detailTableName;
        $data[$model]["flag_active"] = 0;
        $data[$model]["display_order"] = empty($maxDisplayOrder[$model]["display_order"]) ? 1 : $maxDisplayOrder[$model]["display_order"] + 1;
        $data[$model]["flag_test_mode"] = '1';
        $this->addWritableField();
    }

    public function setDataBeforeEditFB(&$data)
    {
        
        $controlItem = $this->getOrRedirectFB($this->id);
        $this->dataToDic($controlItem);

        if (isset($data[$this->name]) && !empty($data[$this->name])){
            foreach ($data[$this->name] as $key=>$value) {
                $controlItem[$this->name][$key] = $value;
            }
        }
        $data[$this->name] = $controlItem[$this->name];

        
        if (empty($data[$this->name]['detail_form_alias']) && !empty($data['detailTest'])){
            $prefix = getPrefix();
            $model = $this->name;

            if (!isset($data[$model][$this->controlType])){
                return;
            }
            $name = $data[$model][$this->controlType];
            $detailTableName = createTableName($prefix . 'dx', time(), $name);

            $data[$model]["detail_form_alias"] = (!empty(trim($data[$model]["detail_form_alias"]))) ? implode(",", array_merge(explode(",", $data[$model]["detail_form_alias"]), array($detailTableName))) : $detailTableName;
            $this->addWritableField();

        }
    }
     
    public function validates($options = array()) 
    {
        $data = $this->find('all', array(
            'conditions' => array(
                $this->name.'.'.$this->controlType.' LIKE ' => '% || '.$this->data[$this->name][$this->controlType]
            )
        ));

        if (!empty($data) && (( countCustom($data) ==1 && $data[0][$this->name]['id'] != $this->id) || countCustom($data) > 1)){
            $this->validationErrors[$this->controlType][] = __("this type has already existed");
        }
        
        return parent::validates($options);
    }
    
    public function addWritableField($field = array(), $tablename = null)
    {
        if (!is_array($field)){
            $field = array($field);
        }
        parent::addWritableField(array_merge($field, array("controls_type", "category", "flag_active", "detail_form_alias", "detail_tablename", "display_order", "flag_test_mode", "flag_form_builder", "flag_active_input")), $tablename);
    }

    public function getName()
    {
        return __('diagnosis');
    }

}