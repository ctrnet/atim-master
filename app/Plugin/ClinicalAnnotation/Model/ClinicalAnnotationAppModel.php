<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class ClinicalAnnotationAppModel
 */
class ClinicalAnnotationAppModel extends AppModel
{

    /**
     *
     * @param $id
     * @return mixed
     */
    public function validateIcd10WhoCode($id)
    {
        $icd10Model = AppModel::getInstance('CodingIcd', 'CodingIcd10Who', true);
        return $icd10Model::validateId($id);
    }

    /**
     *
     * @return mixed
     */
    public function getSecondaryIcd10WhoCodesList()
    {
        $icd10Model = AppModel::getInstance('CodingIcd', 'CodingIcd10Who', true);
        return $icd10Model::getSecondaryDiagnosisList();
    }

    /**
     *
     * @param $id
     * @return mixed
     */
    public function validateIcd10CaCode($id)
    {
        $icd10Model = AppModel::getInstance('CodingIcd', 'CodingIcd10Ca', true);
        return $icd10Model::validateId($id);
    }

    /**
     * WARNING: Duplicated function in ClinicalAnnotationAppModel and InventoryManagementAppModel
     * To allow validation in both plugins.
     *
     * @param $id
     * @return mixed
     */
    public function validateIcdo3TopoCode($id)
    {
        $icdO3TopoModel = AppModel::getInstance('CodingIcd', 'CodingIcdo3Topo', true);
        return $icdO3TopoModel::validateId($id);
    }

    /**
     * WARNING: Duplicated function in ClinicalAnnotationAppModel and InventoryManagementAppModel
     * To allow validation in both plugins.
     *
     * @return mixed
     */
    public function getIcdO3TopoCategoriesCodes()
    {
        $icdO3TopoModel = AppModel::getInstance('CodingIcd', 'CodingIcdo3Topo', true);
        return $icdO3TopoModel::getTopoCategoriesCodes();
    }

    /**
     * WARNING: Duplicated function in ClinicalAnnotationAppModel and InventoryManagementAppModel
     * To allow validation in both plugins.
     *
     * @param $id
     * @return mixed
     */
    public function validateIcdo3MorphoCode($id)
    {
        $icdO3MorphoModel = AppModel::getInstance('CodingIcd', 'CodingIcdo3Morpho', true);
        return $icdO3MorphoModel::validateId($id);
    }

    /**
     * WARNING: Duplicated function in ClinicalAnnotationAppModel and InventoryManagementAppModel
     * To allow validation in both plugins.
     *
     * @param $id
     * @param $mCodeValueSetName
     * @return mixed
     */
    public function validateMcodeCode($id, $mCodeValueSetName)
    {
        $codingMCodeModel = AppModel::getInstance('CodingIcd', 'CodingMCode', true);
        $codingMCodeModel->setValueSetName($mCodeValueSetName);
        return $codingMCodeModel::validateId($id, $mCodeValueSetName);
    }
    
    /**
     * WARNING: Duplicated function in ClinicalAnnotationAppModel and InventoryManagementAppModel
     * To allow validation in both plugins.
     *
     * @param $id
     * @param $otherCodeValueSetName
     * @return mixed
     */
    public function validateOtherCode($id, $otherCodeValueSetName)
    {
        $codingOtherCodeModel = AppModel::getInstance('CodingIcd', 'CodingOtherCode', true);
        $codingOtherCodeModel->setValueSetName($otherCodeValueSetName);
        return $codingOtherCodeModel::validateId($id, $otherCodeValueSetName);
    }
    
    /**
     *
     * @param bool $created
     * @param array $options
     */
    public function afterSave($created, $options = array())
    {
        if ($this->name != 'Participant') {
            // manages Participant.last_modification and Participant.last_modification_ds_id
            if (isset($this->data[$this->name]['deleted']) && $this->data[$this->name]['deleted']) {
                // retrieve participant after a delete operation
                assert($this->id);
                $this->data = $this->find('first', array(
                    'conditions' => array(
                        $this->name . '.' . $this->primaryKey => $this->id,
                        $this->name . '.deleted' => 1
                    )
                ));
            }

            $participantId = null;
            $name = $this->name;
            if (isset($this->data[$this->name]['participant_id'])) {
                $participantId = $this->data[$this->name]['participant_id'];
            } elseif ($this->name == 'TreatmentExtendMaster') {
                $treatmentMaster = AppModel::getInstance('ClinicalAnnotation', 'TreatmentMaster', true);
                $txData = $treatmentMaster->find('first', array(
                    'conditions' => array(
                        'TreatmentMaster.id' => $this->data['TreatmentExtendMaster']['treatment_master_id']
                    ),
                    'fields' => array(
                        'TreatmentMaster.participant_id'
                    )
                ));
                $participantId = $txData['TreatmentMaster']['participant_id'];
                $name = 'TreatmentMaster';
            } else {
                $prevData = $this->data;
                $currData = $this->findById($this->id);
                $this->data = $prevData;
                $participantId = null;
                if (isset($currData[$this->name]) && isset($currData[$this->name]['participant_id'])){
                    $participantId = $currData[$this->name]['participant_id'];
                }
            }
            $datamartStructureModel = AppModel::getInstance('Datamart', 'DatamartStructure', true);

            if (substr($name, -7)!='Control'){
                $datamartStructure = $datamartStructureModel->find('first', array(
                    'conditions' => array(
                        'DatamartStructure.model' => $name
                    )
                ));
                if (! $datamartStructure) {
                    AppController::getInstance()->redirect('/Pages/err_plugin_no_data?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                }
            }
            
            if ($participantId) {
                $participantModel = AppModel::getInstance('ClinicalAnnotation', 'Participant', true);
                $participantModel->data = array();
                $participantModel->id = $participantId;
                $participantModel->addWritableField(array(
                    'last_modification',
                    'last_modification_ds_id'
                ));
                $participantModel->save(array(
                    'last_modification' => $this->data[$this->name]['modified'],
                    'last_modification_ds_id' => $datamartStructure['DatamartStructure']['id']
                ));
            }
        }
        parent::afterSave($created);
    }

    /**
     * @deprecated
     */
    public function getCCLsList() {
        return $this->getLinkableObjectsListFromBrowsingControl('InventoryManagement', 'ViewCollection');
    }
    
    /**
     * Return all DatamartStructures that are linked to the studied DatamartStrucure in the DatamartBrowsingControl records
     * plus information about the status of the link (active/inactive). 
     * 
     * @param String $studiedDatamartStructurePlugin
     * @param String $studiedDatamartStructureModel
     * 
     * @return Array Linked DatamasrtStructures models and plusings plus the status of the link
     */    
    public function getLinkableObjectsListFromBrowsingControl($studiedDatamartStructurePlugin, $studiedDatamartStructureModel) {
        $datamartStructureModel = AppModel::getInstance('Datamart', 'DatamartStructure', true);
        $query = "SELECT id1, id2, ceil((flag_active_1_to_2 + flag_active_2_to_1)/2) active , ds2.`model` model, ds2.`plugin` plugin " . 
            "FROM `datamart_browsing_controls` dbc " . 
            "JOIN `datamart_structures` ds ON ds.id = dbc.id1 " . 
            "JOIN `datamart_structures` ds2 ON ds2.id = dbc.id2 " . 
            "WHERE ds.model = '$studiedDatamartStructureModel' and ds.plugin = '$studiedDatamartStructurePlugin' " . 
            "UNION " . 
            "SELECT id1, id2, ceil((flag_active_1_to_2 + flag_active_2_to_1)/2) active, ds.`model` model, ds.`plugin` plugin " . 
            "FROM `datamart_browsing_controls` dbc " . 
            "JOIN `datamart_structures` ds2 ON ds2.id = dbc.id2 " . 
            "JOIN `datamart_structures` ds ON ds.id = dbc.id1 " . 
            "WHERE ds2.model = '$studiedDatamartStructureModel' and ds2.plugin = '$studiedDatamartStructurePlugin' ";
        $result = $datamartStructureModel->query($query);
        foreach ($result as $k => $v) {
            $result[$v[0]['model']] = $v[0];
            unset($result[$k]);
        }
        return $result;
    }
}