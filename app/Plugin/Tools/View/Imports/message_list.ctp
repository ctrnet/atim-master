<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */

$structureLinks = array(
    'bottom' => array(
        'back' => '/Tools/Imports/index/',
        'download the upload report' => [
            'title' => 'download the upload report',
            'link' => "javascript:setCsvConfig($id)",
            'icon' => 'reports_download'
        ]
    )
);

if ($dataForHeader['status'] == 'upload_success') {
    $structureLinks['bottom']["download csv file"] = [
        'link' => "javascript:downloadCSVFile($id)",
        'icon' => 'csv_upload',
        'title' => 'download csv file'
    ];
}

$settings = [
    'header' => [
        'title' => $dataForHeader['name'] . (strlen($dataForHeader['version'])? (' - ' . $dataForHeader['version']) : '') . ' : ' . __($dataForHeader['status']),
        'description' => $dataForHeader['csv_file_name'] . ' (' . substr($dataForHeader['upload_datetime'], 0, 10) . ')'
    ]
];

$finalOptions = array(
    'type' => 'index',
    'settings' => $settings,
    'links' => $structureLinks,
);

// CUSTOM CODE
$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}

// BUILD FORM
$this->Structures->build($importSelectMessageList, $finalOptions);

?>
    <script>
        var CSV_SEPARATOR = "<?php echo CSV_SEPARATOR; ?>";
    </script>
<?php
echo $this->Html->script('import');
?>
<script>
    var changeAddIconToMessageType = true;
    function addIconMessage(){
        var text, a, found, hrefText, index, th;
        found = false;
        $("table thead th").each(function(){
            if (! found) {
                text = $(this).text().trim();
                if (text == "Type"){
                    a = $(this).children('a');
                    if (typeof a != "undefined"){
                        hrefText = $(a).attr("href").search("sort:BatchUploadMessage.message_type");
                        if (hrefText > -1){
                            found = true;
                            index = $(this).index();
                            th = $(this);
                            th.css("width", "6.5em")
                        }
                    }
                }
            }
        });

        th.closest("table").find("tbody td:nth-child(" + (index + 1) + ")").each(function(){
            $this = $(this);
            console.log($this.text());
            if ($this.text().trim() == 'Error' || $this.text().trim() == 'Erreur'){
                $this.children("p").prepend('<span class="icon16 delete"></span>&nbsp;');
            } else if ($this.text().trim() == 'Message' || $this.text().trim() == 'Message'){
                $this.children("p").prepend('<span class="icon16 confirm"></span>&nbsp;');
            }else if ($this.text().trim() == 'Warning' || $this.text().trim() == 'Avertissement'){
                $this.children("p").prepend('<span class="icon16 warning"></span>&nbsp;');
            }
        });
    }
</script>