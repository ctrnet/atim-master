<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */

$structureLinks = array(
    "bottom" => array(
        "new upload" => array(
            'link' => "/Tools/Imports/importBatch/",
            'icon' => 'add'
        ),
    )
);

if (!empty($csvTemplateLinks)){
    $structureLinks["bottom"]["download templates"] = $csvTemplateLinks;
}

$settings = array(
    'header' => array(
        'title' => __('successful uploads list'),
        'description' => __('the list of all successful uploads')
    ),
    'form_bottom' => false,
    'actions' => false,
    'pagination' => false
);

$this->Structures->build([], array(
    'type' => 'detail',
    'settings' => $settings,
    'links' => $structureLinks,
    'extras' => $this->Structures->ajaxIndex('Tools/imports/listAll/success'),
));

$settings = array(
    'header' => array(
        'title' => __('the last uploads list'),
        'description' => __('the list of the last uploads per each control')
    ),
    'form_top' => false,
    'actions' => true,
);

$this->Structures->build([], array(
    'type' => 'detail',
    'settings' => $settings,
    'links' => $structureLinks,
    'extras' => $this->Structures->ajaxIndex('Tools/imports/listAll/last'),
));