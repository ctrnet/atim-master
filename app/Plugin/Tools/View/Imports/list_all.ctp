<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License
 */

?>
    <script>
        var CSV_SEPARATOR = "<?php echo CSV_SEPARATOR; ?>";
    </script>
<?php
echo $this->Html->script('import');

if (in_array($type, ['success', 'last']) !== false) {
    $structureLinks = [
        "index" => [
            "detail" => [
                'link' => "/Tools/Imports/messageList/%%ImportList.batch_upload_id%%/",
                'icon' => 'reports',
                'title' => __('display the upload report')
            ],
            "download the upload report" => [
                'link' => "javascript:setCsvConfig(%%ImportList.batch_upload_id%%)",
                'icon' => 'reports_download',
                'title' => __('download the upload report')
            ],
        ]
    ];
    if ($type == 'success') {
        $structureLinks['index']["download csv file"] = [
            'link' => "javascript:downloadCSVFile(%%ImportList.batch_upload_id%%)",
            'icon' => 'csv_upload',
            'title' => __('download csv file')
        ];
    }

    $settings = [
        'form_bottom' => false,
        'actions' => false
    ];
    $this->Structures->build($importList, [
        'data' => $data,
        'type' => 'index',
        'links' => $structureLinks,
        'settings' => $settings
    ]);

}