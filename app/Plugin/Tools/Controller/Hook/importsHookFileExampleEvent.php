<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * importsHookFileExample : Class MainScript
 *
 * Example of custom hook file that could be used as a template to develop batch upload script.
 *
 * This script allows users to add participants a clinical annotation to an existing participant.
 * The example work with the 'Presentation (CTRNet Trunk)' form proposed in the ATiM Demo version.

Participant Id;Date of event;Weight;Height;Notes
P0006;2021-08-07;75,5;191;Note for test
P0005;2021-09-22;62;162;

 *
 * Script can be tested on v2.8.0 ATiM demo executing following queries in database:
 *

INSERT IGNORE INTO `batch_upload_controls` (`name`, `version`, `script`, `description`, `max_csv_lines_number`, `flag_active`, `template_csv`, `flag_manipulate_confidential_data`)
VALUES
('Data Upload Example - Presentation (CTRNet Trunk)', '1.0', 'importsHookFileExampleEvent.php', 'Example of custom hook file.', '-1', '1', 'DataUploadExampleTemplateEvent.csv', 1);

 *
 * Create file 'DataUploadExampleTemplateEvent.csv' with following data in '../csvDirectory/templates' directory
 * defined by core variable [Configure::read('uploadDirectory') . DS . Configure::read('csvDirectory') . DS . "templates" . DS]:
 *

TEMPLATE;;;;;;;;;;;
Participant Id;Date of event;Weight;Height;Notes
(string);(date);(float);(float);(string)
(required);;;;
;;;;;;;;;;;
;;;;;;;;;;;
EXAMPLE;;;;;;;;;;;
Participant Id;Date of event;Weight;Height;Notes
P0006;2021-08-07;75,5;191;Note for test
P0005;2021-09-22;62;162;

 */
class MainScript
{
    private $importModel;

    private $options = [];

    // List of actions the user has to have permission to launch to allow script execution
    public $linksToCheckForPermissions = [
        'ClinicalAnnotation/Participants/add/',
        'ClinicalAnnotation/EventMasters/add/'
    ];
    
    // List of tables and linked models that will
    // be populated by the custom script to lock them.
    public $populatedAtimTablesAndModelsList = [
        "participants  Participant",
        "event_masters EventMaster",
        "event_controls EventControl"
    ];
    
    // CSV fields names plus attached information to :
    //  - create fields of the temporary table used for migration.
    //  - validate the imported values based on different ATiM fields properties (type, length, list of values, etc).
    //  - export csv data into the temporary table.
    
    public $csvFieldsToTmpTableFieldsDefinitions = [
        'Participant Id' => [
            'tmp_table_field' => 'csv_participant_identifier',
            'field_type' => 'string',
            'not_blank' => true,
            'unique_in_file' => true,
            'field_length' => '50',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Date of event' => [
            'tmp_table_field' => 'csv_event_date',
            'field_type' => 'date',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Weight' => [
            'tmp_table_field' => 'csv_weight',
            'field_type' => 'float',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '5',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Height' => [
            'tmp_table_field' => 'csv_height',
            'field_type' => 'float',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '5',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Notes' => [
            'tmp_table_field' => 'csv_event_notes',
            'field_type' => 'string',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '500',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ]
    ];
    
    // Temporary table additional fields definition
    public $additionalTmpTableFieldsDefinitions = [
        ['field_name' => 'atim_participant_id', 'data_type' => "int(11) DEFAULT NULL"],
        ['field_name' => 'atim_event_master_id', 'data_type' => "int(11) DEFAULT NULL"]
    ];
    
    // Variable specific to the custom script
    //=======================================
    
    private $allDataCreationCounters = [
        'Created events' => 0
    ];
    
    /**
     * MainScript constructor.
     * Do not remove.
     *
     * @param BatchUploadControl $importModel
     */
    public function __construct(BatchUploadControl $importModel)
    {
        $this->importModel = $importModel;
        /**
         * Set parameter length of the fgetcsv() function.
         * Must be greater than the longest line (in characters) to be found in the CSV file (allowing for trailing line-end characters).
         */
        $this->options['csv_line_buffer_length'] = 50000;
        $this->importModel->options = $this->options;
        $this->importModel->customImportClass = $this;
    }
    
    //==================================================================================================================================================================================
    // INIT
    //==================================================================================================================================================================================
    
    /**
     * Import init custom function.
     *
     * Function called by main import controller before any file content validation (based on $populatedAtimTablesAndModelsList definitions)
     * and any copy of the file values into the temporary table.
     *
     * Should be used to set script custom variables.
     *
     * @throws ImportException
     */
    public function importInit()
    {
        // Set up migration counter to 0 in messages list
        foreach ($this->allDataCreationCounters as $key => $value) {
            $this->importModel->recordErrorAndMessage(__('batch data upload summary'), BatchUploadControl::MESSAGE,
                "Migration counters.", "$key : $value", $key);
        }
        
        // Add unknown pattern to the list of patterns used for not accuracy date (2021-{pattern}-{pattern} as an example)
        $this->importModel->unknownMnHrDayMonthPatterns[] = '.';
        $this->importModel->unknownMnHrDayMonthPatterns[] = '??';
    }
    
    //==================================================================================================================================================================================
    // INSERT DATA INTO TEMPORARY TABLE
    //==================================================================================================================================================================================
    
    /**
     * Process line data before 'trunk' validation (done by the ATiM trunk function) based on $csvFieldsToTmpTableFieldsDefinitions data.
     *
     * Function could be used to:
     *  - Manipulate the csv line data to change value by custom code before validation
     *
     * @param $csvLineData array CSV line data ['CSV Column Name' => 'CSV Column Value'.
     * @param $csvLineCounter string CSV line number of the parsed row.     *
     *
     * @return $csvLineData array.
     *
     * @throws ImportException
     */
    public function processLineDataBeforeTrunkValidation($csvLineData, $csvLineCounter) {
        return $csvLineData;
    }

    /**
     * Process line data before insert in temporary table.
     *
     * Function could be used to:
     * - Manipulate the validated and formatted CSV file line data before insertion into the temporary table.
     * - Execute a last validation round on the formatted CSV file line data before insertion into the temporary table.
     * - Not import line data after control by returning an empty array.
     *
     * @param $csvLineData array
     *            CSV line data ['CSV Column Name' => 'CSV Column Value'.
     * @param $sortedCsvLineDataToLoad array
     *            formatted and validated CSV line data to insert into the temporary table ['Temporary Table Field Name' => 'Value To Insert'].
     * @param $csvLineCounter string
     *            CSV line number of the parsed row. *
     *            
     * @return $sortedCsvLineDataToLoad array formatted and validated CSV line data to insert into the temporary table ['Temporary Table Field Name' => 'Value To Insert']. Return an empty array to skip the line.
     *        
     * @throws ImportException
     */
    public function processFormatedLineDataBeforeInsertInTemporaryTable($csvLineData, $sortedCsvLineDataToLoad, $csvLineCounter)
    {

        // Following test can be done in validateDataIntegrity() too.
        // Added here as an example

        // Check event data exists
        $csvLineDataForCheck = array_filter($csvLineData, 'strlen');
        unset($csvLineDataForCheck['Participant Id']);
        if (empty($csvLineDataForCheck)) {
            // No data to save
            // Send message and skip line data insert in temporary table
            $this->importModel->recordErrorAndMessage(
                'File Data Check', 
                BatchUploadControl::WARNING, 
                "No participant data to import. Line won't be migrated.", 
                "See participant [" . $csvLineData['Participant Id'] . "] at line '$csvLineCounter'.");
            return [];  // Return empty arry to skip insert.
        }

        return $sortedCsvLineDataToLoad;
    }
    
    //==================================================================================================================================================================================
    // VALIDATE DATA
    //==================================================================================================================================================================================
    
    /**
     * Custom validation function.
     *
     * Function called by main import controller after the all CSV file data validation (based on $populatedAtimTablesAndModelsList definitions)
     * has been done and the all CSVfile data have been recorded into the import temporary table.
     *
     * Should be used to:
     * - Validate the data integrity of the imported data based on custom specific business rules and constraints attached to your local installation.
     * - Populate custom column in temporary table copying (atim)ids (participant_id, event_master_id, etc), creating unique keys, etc.
     * - Validate two imported values with each other.
     * - Etc.
     *
     * Function "$this->importModel->getTmpTableFieldNameFromCsvHeader();" can be used to retrieve temporary table field name from csv field name.
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->getAtimControls()" can be used to get record of a specific ATiM control table.
     * Function "$this->importModel->recordErrorAndMessage()" has to be used to record any message (BatchUploadControl::MESSAGE), warning (BatchUploadControl::WARNING) or error  (BatchUploadControl::ERROR) generated by this custom validation process. Remember that any error message will stop the script.
     *
     * @throws ImportException
     */
    public function validateDataIntegrity()
    {
        $eventControl = $this->importModel->getAtimControls('event_controls', 'ctrnet trunk - presentation');
        
        // Get ATiM participant_id && event_master_id (when the form is already completed in ATiM for the participant as an example of control)
        //**********************************************************************************************************************************************************************************
        
        // Get id
        
        $queries = [
            // Get atim participant_id
            "UPDATE {$this->importModel->temporaryTableName}, participants Participant
                SET {$this->importModel->temporaryTableName}.atim_participant_id = Participant.id
                WHERE Participant.participant_identifier = {$this->importModel->temporaryTableName}.csv_participant_identifier
                AND Participant.deleted <> 1;",
            // Get event id if form already completed (example of control)
            "UPDATE {$this->importModel->temporaryTableName}, event_masters EventMaster
                SET {$this->importModel->temporaryTableName}.atim_event_master_id = EventMaster.id
                WHERE {$this->importModel->temporaryTableName}.atim_participant_id = EventMaster.participant_id
                AND EventMaster.deleted <> 1
                AND EventMaster.event_control_id = {$eventControl['id']};",
        ];
        foreach ($queries as $query) {
            $this->importModel->customQuery($query);
        }
        
        // Validate csv data vs atim data
        //**********************************************************************************************************************************************************************************
        
        $query = "SELECT *
            FROM {$this->importModel->temporaryTableName} 
            WHERE atim_participant_id IS NULL 
            OR atim_event_master_id IS NOT NULL";
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract(AppController::convertArrayKeyFromSnakeToCamel($newResult));
            // Validate participant exists in ATIM
            if (! $atimParticipantId) {
                $this->importModel->recordErrorAndMessage(
                    'File Data vs ATiM Data Check',
                    BatchUploadControl::ERROR,
                    "Participant not found.",
                    "See participant [$csvParticipantIdentifier] at line '$csvLine'.");
            }
        
            // Validate no form is already completed in ATiM for the participant (rule : 1 form per participant as an example)
            // Note: 2 form can not be created from the csv file for a same participant because field 'unique_in_file' is set to true on Participant Id csv field.

            if ($atimEventMasterId) {
                $this->importModel->recordErrorAndMessage(
                    'File Data vs ATiM Data Check',
                    BatchUploadControl::ERROR,
                    "Participant Presentation form already completed in ATIM.",
                    "See participant [$csvParticipantIdentifier] at line '$csvLine'.");
            }
        }
    }

        // ==================================================================================================================================================================================
        // INSERT DATA INTO ATIM TABLES
        // ==================================================================================================================================================================================

    /**
     * Function to use to insert data into ATiM tables.
     * This function is launched by the script if no error message has been generated by
     * validation functions (both trunk and custom code).
     *
     * Function "$this->importModel->getTmpTableFieldNameFromCsvHeader();" can be used to retrieve temporary table field name from csv field name.
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->getAtimControls()" can be used to get record of a specific ATiM control table.
     * Function "$this->importModel->migrationDie()" has to be used to stop insertion for any code error.
     * Function "$this->importModel->customInsertRecord()" has to be used to insert data.
     * Function "$this->importModel->customQuery()" has to be used to update data.
     *
     * @throws ImportException
     */
    public function insertDataIntoATiM()
    {
        // Creation :: Events
        // **********************************************************************************************************************************************************************************
        $eventControl = $this->importModel->getAtimControls('event_controls', 'ctrnet trunk - presentation');

        $query = "SELECT *
            FROM {$this->importModel->temporaryTableName}
            WHERE {$this->importModel->temporaryTableName}.atim_event_master_id IS NULL 
            AND {$this->importModel->temporaryTableName}.atim_participant_id IS NOT NULL";
        
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract(AppController::convertArrayKeyFromSnakeToCamel($newResult));
            $eventData = [
                'event_masters' => [
                    'participant_id' => $atimParticipantId,
                    'event_control_id' => $eventControl['id'],
                    'event_date' => $csvEventDate,
                    'event_date_accuracy' => $csvEventDateAccuracy,
                    'event_summary' => $csvEventNotes
                ],
                $eventControl['detail_tablename'] => [
                    'weight' => $csvWeight,
                    'height' => $csvHeight
                ]
            ];
            $this->importModel->customInsertRecord($eventData);
            $this->allDataCreationCounters['Created events'] ++;
        }

        // Creation :: Final queries
        // **********************************************************************************************************************************************************************************

        $finalQueries = [];
        foreach ($finalQueries as $newQuery) {
            $this->importModel->customQuery($newQuery);
        }

        $this->populateViews();
    }
    
        
    //==================================================================================================================================================================================
    // END OF PROCESS
    //==================================================================================================================================================================================
    
    /**
     * Function to use to update data at the end of the script execution, set up messages as a migration summary to display to user, etc.
     *
     * Could be used to repopulate views, generate storage codes, etc.
     *
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->recordErrorAndMessage()" has to be used to record any message (BatchUploadControl::MESSAGE), warning (BatchUploadControl::WARNING) or error  (BatchUploadControl::ERROR) generated by this custom validation process. Remember that any error message will stop the script.
     *
     * @throws ImportException
     */
    public function importFinish()
    {
        // Complete migration summary
        //**********************************************************************************************************************************************************************************
        
        foreach ($this->allDataCreationCounters as $key => $value) {
            $this->importModel->recordErrorAndMessage(__('batch data upload summary'), BatchUploadControl::MESSAGE,
                "Migration counters.", "$key : $value", $key);
        }
    }
    
    /**
     * Function written here as an example to repopulate views.
     *
     * @throws ImportException
     */
    public function populateViews()
    {}
}