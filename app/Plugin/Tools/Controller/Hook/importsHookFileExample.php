<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * importsHookFileExample : Class MainScript
 *
 * Example of custom hook file that could be used as a template to develop batch upload script.
 *
 * This script allows users to create participants, collections and consents from csv file having the following format:
 *

Participant Id;Date of birth;Consent Date;Consent Version;Consent Status;Diagnosis Date;Disease Code;Collection Date and Time;Sample Type;Barcode;Storage;Position
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-08-07 2:13;blood;74893974884;F-2-BDemo1;1
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-08-07 2:13;plasma;94939030300;F-2-BDemo1;2
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-08-07 2:13;plasma;89493005948;F-2-BDemo2;1
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-08-07 2:13;serum;85736289478;F-2-BDemo2;2
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-09-10  14:??;blood;77477389323;F-2-BDemo1;5
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-09-10  14:??;plasma;59003927734;F-2-BDemo1;4
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-09-10  14:??;urine;59003927734;F-2-BDemo1;7
Participant2;1954-05-06;2020-05-10;;obtained;2019-12-22;C619;2020-05-10;serum;88839937832;F-2-BDemo2;3
Participant3;1959-10-16;2021-02-15;2011-04-21;pending;;;2021-02-17 2:13;plasma;88837738884;F-2-BDemo4;1

 *
 * Script can be tested on v2.7.4 ATiM demo executing following queries in database:
 *
 *
 *

INSERT IGNORE INTO `batch_upload_controls` (`name`, `version`, `script`, `description`, `max_csv_lines_number`, `flag_active`, `template_csv`, `flag_manipulate_confidential_data`)
VALUES
('Data Upload Example', '1.0', 'importsHookFileExample.php', 'Example of custom hook file.', '-1', '1', 'DataUploadExampleTemplate.csv', 1);

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('patient info', 'Patient Info', 'Info Patient'),
('consent info', 'Consent Info', 'Info Consentement'),
('inventory info', 'Inventory Info', 'Info Inventaire'),
('storage info', 'Storage Info', 'Info Entreposage');

 *
 * Create file 'DataUploadExampleTemplate.csv' with following data in '../csvDirectory/templates' directory
 * defined by core variable [Configure::read('uploadDirectory') . DS . Configure::read('csvDirectory') . DS . "templates" . DS]:
 *
 *

TEMPLATE;;;;;;;;;;;
Participant Id;Date of birth;Consent Date;Consent Version;Consent Status;Diagnosis Date;Disease Code;Collection Date and Time;Sample Type;Barcode;Storage;Position
(string);(date);(date);(string from list);(string from list);(date);(string);(datetime);(string from list);(string);(string);(int)
(required);;;;(required);;;(required);(required);(required);;
;;;See custom drop down list 'Consent Form Versions';pending', 'obtained', 'denied', 'withdrawn';;;;blood','plasma','serum';;;1-81
;;;;;;;;;;;
;;;;;;;;;;;
EXAMPLE;;;;;;;;;;;
Participant Id;Date of birth;Consent Date;Consent Version;Consent Status;Diagnosis Date;Disease Code;Collection Date and Time;Sample Type;Barcode;Storage;Position
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-08-07 2:13;blood;74893974884;F-2-BDemo1;1
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-08-07 2:13;plasma;94939030300;F-2-BDemo1;2
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-08-07 2:13;plasma;89493005948;F-2-BDemo2;1
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-08-07 2:13;serum;85736289478;F-2-BDemo2;2
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-09-10  14:??;blood;77477389323;F-2-BDemo1;5
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-09-10  14:??;plasma;59003927734;F-2-BDemo1;4
Participant1;1954-05-06;2021-08-07;;pending;2017-02-??;C500;2021-09-10  14:??;urine;59003927734;F-2-BDemo1;7
Participant2;1954-05-06;2020-05-10;;obtained;2019-12-22;C619;2020-05-10;serum;88839937832;F-2-BDemo2;3
Participant3;1959-10-16;2021-02-15;2011-04-21;pending;;;2021-02-17 2:13;plasma;88837738884;F-2-BDemo4;1

 */
class MainScript
{
    private $importModel;

    private $options = [];

    // List of actions the user has to have permission to launch to allow script execution
    public $linksToCheckForPermissions = [
        'ClinicalAnnotation/Participants/add/',
        'ClinicalAnnotation/ConsentMasters/add/',
        'ClinicalAnnotation/DiagnosisMasters/add/',
        'InventoryManagement/Collections/add/',
        'InventoryManagement/SampleMasters/add/',
        'InventoryManagement/AliquotMasters/add/',
        'StorageLayout/StorageMasters/add/'
    ];
    
    // List of tables and linked models that will
    // be populated by the custom script to lock them.
    public $populatedAtimTablesAndModelsList = [
        "participants  Participant",
        "consent_masters ConsentMaster",
        "consent_controls ConsentControl",
        "collections Collection",
        "sample_masters SampleMaster",
        "aliquot_masters AliquotMaster",
        "storage_masters StorageMaster",
        
        "view_collections ViewCollection",
        "view_samples ViewSample",
        "view_aliquots ViewAliquot",
        "view_aliquot_uses ViewAliquotUse",
        
        "view_storage_masters ViewStorageMaster"
        
    ];
    
    // CSV fields names plus attached information to :
    //  - create fields of the temporary table used for migration.
    //  - validate the imported values based on different ATiM fields properties (type, length, list of values, etc).
    //  - export csv data into the temporary table.
    
    public $csvFieldsToTmpTableFieldsDefinitions = [
        'Participant Id' => [
            'tmp_table_field' => 'participant_identifier',
            'field_type' => 'string',
            'not_blank' => true,
            'unique_in_file' => false,
            'field_length' => '50',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Date of birth' => [
            'tmp_table_field' => 'participant_d_of_b',
            'field_type' => 'date',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Consent Date' => [
            'tmp_table_field' => 'cst_date',
            'field_type' => 'date',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Consent Version' => [
            'tmp_table_field' => 'cst_version',
            'field_type' => 'select',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '50',
            'structure_domain_name_for_validation' => 'custom_consent_from_verisons',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Consent Status' => [
            'tmp_table_field' => 'cst_status',
            'field_type' => 'select',
            'not_blank' => true,
            'unique_in_file' => false,
            'field_length' => '50',
            'structure_domain_name_for_validation' => 'consent_status',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Diagnosis Date' => [
            'tmp_table_field' => 'dx_date',
            'field_type' => 'date',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Disease Code' => [
            'tmp_table_field' => 'dx_code',
            'field_type' => 'CodingIcdo3Topo',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '10',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Collection Date and Time' => [
            'tmp_table_field' => 'collection_datetime',
            'field_type' => 'datetime',
            'not_blank' => true,
            'unique_in_file' => false,
            'field_length' => '',
            'structure_domain_name_for_validation' => 'collection_date',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Sample Type' => [
            'tmp_table_field' => 'sample_type',
            'field_type' => 'select',
            'not_blank' => true,
            'unique_in_file' => false,
            'field_length' => '30',
            'structure_domain_name_for_validation' => 'sample_type',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Barcode' => [
            'tmp_table_field' => 'barcode',
            'field_type' => 'string',
            'not_blank' => true,
            'unique_in_file' => true,
            'field_length' => '60',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Storage' => [
            'tmp_table_field' => 'storage',
            'field_type' => 'string',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '30',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Position' => [
            'tmp_table_field' => 'position',
            'field_type' => 'integer',
            'not_blank' => false,
            'unique_in_file' => false,
            'field_length' => '3',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ]
    ];
    
    // Temporary table additional fields definition
    public $additionalTmpTableFieldsDefinitions = [
        ['field_name' => 'atim_participant_id', 'data_type' => "int(11) DEFAULT NULL"],
        ['field_name' => 'atim_consent_master_id', 'data_type' => "int(11) DEFAULT NULL"],
        ['field_name' => 'atim_diagnosis_master_id', 'data_type' => "int(11) DEFAULT NULL"],
        ['field_name' => 'duplicated_barcode', 'data_type' => "tinyint(1) DEFAULT 0"]
    ];
    
    // Variable specific to the custom script
    //=======================================
    
    private $allDataCreationCounters = [
        'Created participants' => 0,
        'Created consents' => 0,
        'Created diagnosis' => 0,
        'Created collections' => 0,
        'Created aliquots' => 0,
        'Created storages' => 0,
    ];
    
    private $patientIdsToATiMRecords = [];
    
    private $storageSelectionLabelToATiMIds = [];

    private $storageMasterModel = null;
    
    private $completedStorageMasterIds = [-1];
        
    /**
     * MainScript constructor.
     * Do not remove.
     *
     * @param BatchUploadControl $importModel
     */
    public function __construct(BatchUploadControl $importModel)
    {
        $this->importModel = $importModel;
        /**
         * Set parameter length of the fgetcsv() function.
         * Must be greater than the longest line (in characters) to be found in the CSV file (allowing for trailing line-end characters).
         */
        $this->options['csv_line_buffer_length'] = 50000;
        $this->importModel->options = $this->options;
        $this->importModel->customImportClass = $this;
    }
    
    //==================================================================================================================================================================================
    // INIT
    //==================================================================================================================================================================================
    
    /**
     * Import init custom function.
     *
     * Function called by main import controller before any file content validation (based on $populatedAtimTablesAndModelsList definitions)
     * and any copy of the file values into the temporary table.
     *
     * Should be used to set script custom variables.
     *
     * @throws ImportException
     */
    public function importInit()
    {
        // Set up migration counter to 0 in messages list
        foreach ($this->allDataCreationCounters as $key => $value) {
            $this->importModel->recordErrorAndMessage(__('batch data upload summary'), BatchUploadControl::MESSAGE,
                "Migration counters.", "$key : $value", $key);
        }
        
        // Add unknown pattern to the list of patterns used for not accuracy date (2021-{pattern}-{pattern} as an example)
        $this->importModel->unknownMnHrDayMonthPatterns[] = '.';
        $this->importModel->unknownMnHrDayMonthPatterns[] = '??';
        
        $this->storageMasterModel = AppModel::getInstance('StorageLayout', 'StorageMaster', true);
    }
    
    //==================================================================================================================================================================================
    // INSERT DATA INTO TEMPORARY TABLE
    //==================================================================================================================================================================================
    
    /**
     * Process line data before 'trunk' validation (done by the ATiM trunk function) based on $csvFieldsToTmpTableFieldsDefinitions data.
     *
     * Function could be used to:
     *  - Manipulate the csv line data to change value by custom code before validation
     *
     * @param $csvLineData array CSV line data ['CSV Column Name' => 'CSV Column Value'.
     * @param $csvLineCounter string CSV line number of the parsed row.     *
     *
     * @return $csvLineData array.
     *
     * @throws ImportException
     */
    public function processLineDataBeforeTrunkValidation($csvLineData, $csvLineCounter) {
        return $csvLineData;
    }   
    
    /**
     * Process line data before insert in temporary table.
     *
     * Function could be used to:
     *  - Manipulate the validated and formatted CSV file line data before insertion into the temporary table.
     *  - Execute a last validation round on the formatted CSV file line data before insertion into the temporary table.
     *  - Not import line data after control by returning an empty array.
     *
     * @param $csvLineData array CSV line data ['CSV Column Name' => 'CSV Column Value'.
     * @param $sortedCsvLineDataToLoad array formatted and validated CSV line data to insert into the temporary table ['Temporary Table Field Name' => 'Value To Insert'].
     * @param $csvLineCounter string CSV line number of the parsed row.     *
     *
     * @return $sortedCsvLineDataToLoad array formatted and validated CSV line data to insert into the temporary table ['Temporary Table Field Name' => 'Value To Insert']. Return an empty array to skip the line.
     *
     * @throws ImportException
     */
    public function processFormatedLineDataBeforeInsertInTemporaryTable($csvLineData, $sortedCsvLineDataToLoad, $csvLineCounter) {
        
        // Following test can be done in validateDataIntegrity() too.
        // Added here as an example
        if (!in_array($csvLineData['Sample Type'], array('blood', 'serum', 'plasma'))) {
            $this->importModel->recordErrorAndMessage(__('inventory info'), BatchUploadControl::WARNING, "Sample type [" . $csvLineData['Sample Type'] . "] not supported by the script. Line won't be migrated.",
                "See line '$csvLineCounter'.");
            return [];
        }
        
        return $sortedCsvLineDataToLoad;
    }
    
    //==================================================================================================================================================================================
    // VALIDATE DATA
    //==================================================================================================================================================================================
    
    /**
     * Custom validation function.
     *
     * Function called by main import controller after the all CSV file data validation (based on $populatedAtimTablesAndModelsList definitions)
     * has been done and the all CSVfile data have been recorded into the import temporary table.
     *
     * Should be used to:
     * - Validate the data integrity of the imported data based on custom specific business rules and constraints attached to your local installation.
     * - Populate custom column in temporary table copying (atim)ids (participant_id, consent_master_id, etc), creating unique keys, etc.
     * - Validate two imported values with each other.
     * - Etc.
     *
     * Function "$this->importModel->getTmpTableFieldNameFromCsvHeader();" can be used to retrieve temporary table field name from csv field name.
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->getAtimControls()" can be used to get record of a specific ATiM control table.
     * Function "$this->importModel->recordErrorAndMessage()" has to be used to record any message (BatchUploadControl::MESSAGE), warning (BatchUploadControl::WARNING) or error  (BatchUploadControl::ERROR) generated by this custom validation process. Remember that any error message will stop the script.
     *
     * @throws ImportException
     */
    public function validateDataIntegrity()
    {
        foreach (array_keys($this->csvFieldsToTmpTableFieldsDefinitions) as $csvFieldName) {
            ${Inflector::variable($csvFieldName) . 'TmpTableFieldName'} = $this->importModel->getTmpTableFieldNameFromCsvHeader($csvFieldName);
        }
                    
        // VALIDATION: Participant + consent + diagnosis information unique per participant in file
        //**********************************************************************************************************************************************************************************
        
        $query = "SELECT DISTINCT $participantIdTmpTableFieldName AS csvPatientId,
            $dateOfBirthTmpTableFieldName,
            $consentDateTmpTableFieldName,
            $consentStatusTmpTableFieldName,
            $consentVersionTmpTableFieldName,
            $consentDateTmpTableFieldName,
            $diseaseCodeTmpTableFieldName
            FROM {$this->importModel->temporaryTableName}";
        $parsedParticipants = [];
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract($newResult);
            if (in_array($csvPatientId, $parsedParticipants)) {
                $this->importModel->recordErrorAndMessage(__('patient info'), BatchUploadControl::ERROR, "The Participant consent and diagnosis is not identical all across the csv file.",
                    "See 'Participant Id' [$csvPatientId]");
            } else {
                $parsedParticipants[] = $csvPatientId;
            }
        }
        
        // Match CSV data and ATiM data:
        //    - Participant on participants.participant_identifier and Participant Id
        //    - Consent on participant plus consent date
        //**********************************************************************************************************************************************************************************
        
        $consentControl = $this->importModel->getAtimControls('consent_controls', 'ctrnet trunk - consent national');
        $dataCleanUpQueries = [
            // Don't duplicate participant
            "UPDATE {$this->importModel->temporaryTableName}, participants Participant
                SET {$this->importModel->temporaryTableName}.atim_participant_id = Participant.id
                WHERE Participant.participant_identifier = {$this->importModel->temporaryTableName}.$participantIdTmpTableFieldName
                AND Participant.deleted <> 1;",
                // Don't duplicate consent
            "UPDATE {$this->importModel->temporaryTableName}, consent_masters ConsentMaster
                SET {$this->importModel->temporaryTableName}.atim_consent_master_id = ConsentMaster.id
                WHERE {$this->importModel->temporaryTableName}.atim_participant_id = ConsentMaster.participant_id
                AND ConsentMaster.consent_control_id = " . $consentControl['id'] . "
                AND ConsentMaster.consent_signed_date = {$this->importModel->temporaryTableName}.$consentDateTmpTableFieldName
                AND ConsentMaster.consent_signed_date_accuracy = {$this->importModel->temporaryTableName}.$consentDateTmpTableFieldName" . "_accuracy" . "
                AND ConsentMaster.deleted <> 1;",
                ];
        foreach ($dataCleanUpQueries as $newQuery) {
            $this->importModel->customQuery($newQuery);
        }
        
        // Validate CSV consent data and ATiM consent data:
        //    - Check exiting participant consents have same status in both csv and ATiM
        //**********************************************************************************************************************************************************************************
        
        $query = "SELECT {$this->importModel->temporaryTableName}.$participantIdTmpTableFieldName AS csvPatientId,
            ConsentMaster.consent_signed_date AS consentSignedDate,
            {$this->importModel->temporaryTableName}.cst_status AS csvCstStatus,
            ConsentMaster.consent_status AS atimConsentStatus,
            GROUP_CONCAT(DISTINCT csv_line ORDER BY csv_line ASC SEPARATOR ', ') AS csvLines
            FROM {$this->importModel->temporaryTableName}, consent_masters ConsentMaster
            WHERE {$this->importModel->temporaryTableName}.atim_consent_master_id = ConsentMaster.id
            AND {$this->importModel->temporaryTableName}.cst_status != ConsentMaster.consent_status
            GROUP BY $participantIdTmpTableFieldName, ConsentMaster.consent_signed_date, {$this->importModel->temporaryTableName}.cst_status, ConsentMaster.consent_status";
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract($newResult);
            $lineInfoForDisplay = "See lines $csvLines.";
            $this->importModel->recordErrorAndMessage(__('consent info'), BatchUploadControl::ERROR, "Participant consent already exists but status are different.",
                "See 'Participant Id' [$csvPatientId] and consent on '$consentSignedDate': status '$csvCstStatus' != '$atimConsentStatus'. $lineInfoForDisplay");
        }
        
        // Validate Diagnosis
        //**********************************************************************************************************************************************************************************
        
        // No control. New diagnosis will be created any time in our example.
        
        // Validate CSV storage information and position:
        //    - Storage format should be similar than freezer_short_label-rack_short_label-box_short_label /^([^-]{1,10})\-([^-]{1,10})\-([^-]{1,10})$/
        //    - Storage position from 1 to 99
        //**********************************************************************************************************************************************************************************
        
        $query = "SELECT {$this->importModel->temporaryTableName}.$storageTmpTableFieldName AS csvStorage,
            GROUP_CONCAT(DISTINCT csv_line ORDER BY csv_line ASC SEPARATOR ', ') AS csvLines
            FROM {$this->importModel->temporaryTableName}
            WHERE {$this->importModel->temporaryTableName}.$storageTmpTableFieldName IS NOT NULL
            AND {$this->importModel->temporaryTableName}.$storageTmpTableFieldName NOT LIKE ''
            AND {$this->importModel->temporaryTableName}.$storageTmpTableFieldName NOT REGEXP '^([^-]{1,10})\-([^-]{1,10})\-([^-]{1,10})$'
            GROUP BY $storageTmpTableFieldName";
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract($newResult);
            $lineInfoForDisplay = "See lines $csvLines.";
            $this->importModel->recordErrorAndMessage(__('storage info'), BatchUploadControl::ERROR, "Wrong Storage label format.",
                "See 'Storage' [$csvStorage]. $lineInfoForDisplay");
        }
        
        $query = "SELECT {$this->importModel->temporaryTableName}.$positionTmpTableFieldName AS csvPosition,
            GROUP_CONCAT(DISTINCT csv_line ORDER BY csv_line ASC SEPARATOR ', ') AS csvLines
            FROM {$this->importModel->temporaryTableName}
            WHERE {$this->importModel->temporaryTableName}.$positionTmpTableFieldName IS NOT NULL
            AND {$this->importModel->temporaryTableName}.$positionTmpTableFieldName NOT LIKE ''
            AND ({$this->importModel->temporaryTableName}.$positionTmpTableFieldName < 1 OR {$this->importModel->temporaryTableName}.$positionTmpTableFieldName > 99)
            GROUP BY $positionTmpTableFieldName";
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract($newResult);
            $lineInfoForDisplay = "See lines $csvLines.";
            $this->importModel->recordErrorAndMessage(__('storage info'), BatchUploadControl::ERROR, "Wrong Storage position.",
                "See 'Position' [$csvPosition]. $lineInfoForDisplay");
        }
        
        // Validate CSV barcode:
        //    - Barcode should not exist in ATiM
        //**********************************************************************************************************************************************************************************
        
        $query = "UPDATE {$this->importModel->temporaryTableName}, aliquot_masters AliquotMaster
            SET {$this->importModel->temporaryTableName}.duplicated_barcode = 1
            WHERE AliquotMaster.barcode = {$this->importModel->temporaryTableName}.$barcodeTmpTableFieldName
            AND AliquotMaster.deleted <> 1;";
        $this->importModel->customQuery($query);
        
        $query = "SELECT {$this->importModel->temporaryTableName}.$barcodeTmpTableFieldName AS csvBarcode,
            csv_line AS csvLines
            FROM {$this->importModel->temporaryTableName}
            WHERE {$this->importModel->temporaryTableName}.duplicated_barcode = 1";
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract($newResult);
            $lineInfoForDisplay = "See line $csvLines.";
            $this->importModel->recordErrorAndMessage(__('inventory info'), BatchUploadControl::ERROR, "Barcode already exists in ATiM.",
                "See 'Barcode' [$csvBarcode]. $lineInfoForDisplay");
        }
    }
    
    //==================================================================================================================================================================================
    // INSERT DATA INTO ATIM TABLES
    //==================================================================================================================================================================================
    
    /**
     * Function to use to insert data into ATiM tables. This function is launched by the script if no error message has been generated by
     * validation functions (both trunk and custom code).
     *
     * Function "$this->importModel->getTmpTableFieldNameFromCsvHeader();" can be used to retrieve temporary table field name from csv field name.
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->getAtimControls()" can be used to get record of a specific ATiM control table.
     * Function "$this->importModel->migrationDie()" has to be used to stop insertion for any code error.
     * Function "$this->importModel->customInsertRecord()" has to be used to insert data.
     * Function "$this->importModel->customQuery()" has to be used to update data.
     *
     * @throws ImportException
     */
    public function insertDataIntoATiM()
    {
        foreach (array_keys($this->csvFieldsToTmpTableFieldsDefinitions) as $csvFieldName) {
            ${Inflector::variable($csvFieldName) . 'TmpTableFieldName'} = $this->importModel->getTmpTableFieldNameFromCsvHeader($csvFieldName);
        }
        
        // Creation :: Participants
        //**********************************************************************************************************************************************************************************
        
        $query = "SELECT DISTINCT $participantIdTmpTableFieldName AS csvParticipantIdentifier,
            $dateOfBirthTmpTableFieldName AS csvParticipantDateOfBirth,
            {$dateOfBirthTmpTableFieldName}_accuracy AS csvParticipantDateOfBirthAcc
            FROM {$this->importModel->temporaryTableName}
            WHERE ({$this->importModel->temporaryTableName}.atim_participant_id IS NULL
            OR {$this->importModel->temporaryTableName}.atim_participant_id LIKE '')";
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract($newResult);
            $participantData = [
                'participants' => [
                    'participant_identifier' => $csvParticipantIdentifier,
                    'date_of_birth' => $csvParticipantDateOfBirth,
                    'date_of_birth_accuracy' => $csvParticipantDateOfBirthAcc,
                    'notes' => 'Imported by script'
                ]
            ];
            $a = $this->importModel->customInsertRecord($participantData);

            $this->allDataCreationCounters['Created participants']++;
        }
        
        // Copy participant_id in temporary table
        $query = "UPDATE {$this->importModel->temporaryTableName}, participants Participant
            SET {$this->importModel->temporaryTableName}.atim_participant_id = Participant.id
            WHERE Participant.participant_identifier = {$this->importModel->temporaryTableName}.$participantIdTmpTableFieldName
            AND Participant.deleted <> 1;";
        $this->importModel->customQuery($query);
        
        // Creation :: Consents
        //**********************************************************************************************************************************************************************************
        
        $consentControl = $this->importModel->getAtimControls('consent_controls', 'ctrnet trunk - consent national');
        
        $query = "SELECT DISTINCT {$this->importModel->temporaryTableName}.atim_participant_id AS atimParticipantId,
            {$this->importModel->temporaryTableName}.$consentDateTmpTableFieldName AS consentSignedDate,
            {$this->importModel->temporaryTableName}.$consentDateTmpTableFieldName" . "_accuracy" . " consentSignedDateAccuracy,
            {$this->importModel->temporaryTableName}.$consentVersionTmpTableFieldName AS consentVersion,
            {$this->importModel->temporaryTableName}.$consentStatusTmpTableFieldName AS consentStatus
            FROM {$this->importModel->temporaryTableName}
            WHERE {$this->importModel->temporaryTableName}.atim_consent_master_id IS NULL";
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract($newResult);
            $consentData = [
                'consent_masters' => [
                    'participant_id' => $atimParticipantId,
                    'consent_control_id' => $consentControl['id'],
                    'consent_signed_date' => $consentSignedDate,
                    'consent_signed_date_accuracy' => $consentSignedDateAccuracy,
                    'form_version' => $consentVersion,
                    'consent_status' => $consentStatus,
                    'notes' => 'Imported by script'
                ],
                $consentControl['detail_tablename'] => []
            ];
            $this->importModel->customInsertRecord($consentData);
            $this->allDataCreationCounters['Created consents']++;
        }
        
        // Copy consent_master_id in temporary table
        $query = "UPDATE {$this->importModel->temporaryTableName}, consent_masters ConsentMaster
            SET {$this->importModel->temporaryTableName}.atim_consent_master_id = ConsentMaster.id
            WHERE {$this->importModel->temporaryTableName}.atim_participant_id = ConsentMaster.participant_id
            AND ConsentMaster.consent_control_id = " . $consentControl['id'] . "
            AND ConsentMaster.consent_signed_date = {$this->importModel->temporaryTableName}.$consentDateTmpTableFieldName
            AND ConsentMaster.consent_signed_date_accuracy = {$this->importModel->temporaryTableName}.$consentDateTmpTableFieldName" . "_accuracy" . "
            AND ConsentMaster.deleted <> 1;";
        $this->importModel->customQuery($query);
            
        // Creation :: Diagnosis
        //**********************************************************************************************************************************************************************************
        
        $primaryDxControl = $this->importModel->getAtimControls('diagnosis_controls', 'primary-primary diagnosis unknown');
        $secondaryDxControl = $this->importModel->getAtimControls('diagnosis_controls', 'secondary - distant-ctrnet trunk - undetailed');
        
        $participantIdToDxId = [];
        $query = "SELECT DISTINCT {$this->importModel->temporaryTableName}.atim_participant_id AS atimParticipantId,
            {$this->importModel->temporaryTableName}.$diagnosisDateTmpTableFieldName AS dxDate,
            {$this->importModel->temporaryTableName}.{$diagnosisDateTmpTableFieldName}_accuracy AS dxDateAccuracy,
            {$this->importModel->temporaryTableName}.$diseaseCodeTmpTableFieldName AS dxCode
            FROM {$this->importModel->temporaryTableName}
            WHERE {$this->importModel->temporaryTableName}.$diagnosisDateTmpTableFieldName IS NOT NULL AND {$this->importModel->temporaryTableName}.$diagnosisDateTmpTableFieldName NOT LIKE ''
            AND {$this->importModel->temporaryTableName}.$diseaseCodeTmpTableFieldName IS NOT NULL AND {$this->importModel->temporaryTableName}.$diseaseCodeTmpTableFieldName NOT LIKE ''";
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract($newResult);
            $dxData = [
                'diagnosis_masters' => [
                    'participant_id' => $atimParticipantId,
                    'diagnosis_control_id' => $primaryDxControl['id'],
                    'icd10_code' => 'D489',
                    'notes' => 'Imported by script'
                ],
                $primaryDxControl['detail_tablename'] => []
            ];
            $dxData = [
                'diagnosis_masters' => [
                    'participant_id' => $atimParticipantId,
                    'parent_id' => $this->importModel->customInsertRecord($dxData),
                    'diagnosis_control_id' => $secondaryDxControl['id'],
                    'dx_date' => $dxDate,
                    'dx_date_accuracy' => $dxDateAccuracy,
                    'topography' => $dxCode,
                    'notes' => 'Imported by script'
                ],
                $secondaryDxControl['detail_tablename'] => []
            ];
            $participantIdToDxId[$atimParticipantId] = $this->importModel->customInsertRecord($dxData);
            $this->allDataCreationCounters['Created diagnosis']++;
        }
            
        // Creation :: Collections
        //**********************************************************************************************************************************************************************************
        
        $collectionContentIds = [];
        
        $query = "SELECT DISTINCT {$this->importModel->temporaryTableName}.atim_participant_id  AS atimParticipantId,
            {$this->importModel->temporaryTableName}.atim_consent_master_id  AS atimConsentMasterId,
            $collectionDateAndTimeTmpTableFieldName AS collectionDateTime,
            {$collectionDateAndTimeTmpTableFieldName}_accuracy AS collectionDateTimeAccuracy
            FROM {$this->importModel->temporaryTableName}";
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract($newResult);
            $collectionKey = "$atimParticipantId-$collectionDateTime-$collectionDateTimeAccuracy";
            $collectionData = [
                'collections' => [
                    'participant_id' => $atimParticipantId,
                    'consent_master_id' => $atimConsentMasterId,
                    'diagnosis_master_id' => (isset($participantIdToDxId[$atimParticipantId])? $participantIdToDxId[$atimParticipantId] : null),
                    'collection_notes' => 'Imported by script',
                    'collection_property' => 'participant collection'
                ]
            ];
            if ($collectionDateTime) {
                $collectionData['collections']['collection_datetime'] = $collectionDateTime;
                $collectionData['collections']['collection_datetime_accuracy'] = $collectionDateTimeAccuracy;
            }
            $collectionContentIds[$collectionKey] = ['collection_id' => $this->importModel->customInsertRecord($collectionData), 'sample_master_ids' => []];
            $this->allDataCreationCounters['Created collections']++;
        }
        
        // Creation :: Samples & Aliquots
        //**********************************************************************************************************************************************************************************
        
        $query = "SELECT DISTINCT {$this->importModel->temporaryTableName}.atim_participant_id  AS atimParticipantId,
            {$this->importModel->temporaryTableName}.atim_consent_master_id  AS atimConsentMasterId,
            $collectionDateAndTimeTmpTableFieldName AS collectionDateTime,
            $collectionDateAndTimeTmpTableFieldName" . "_accuracy" . " AS collectionDateTimeAccuracy,
            $sampleTypeTmpTableFieldName AS sampleType,
            $storageTmpTableFieldName AS storageLabel,
            $positionTmpTableFieldName AS storagePosition,
            $barcodeTmpTableFieldName AS barcode
            FROM {$this->importModel->temporaryTableName}";
        foreach ($this->importModel->getSelectQueryResult($query) as $newResult) {
            extract($newResult);
            $collectionKey = "$atimParticipantId-$collectionDateTime-$collectionDateTimeAccuracy";
            // Create blood
            if (!isset($collectionContentIds[$collectionKey]['sample_master_ids']['blood'])) {
                $sampleAtimControls = $this->importModel->getAtimControls('sample_controls', 'blood');
                $sampleData = [
                    'sample_masters' => [
                        'collection_id' => $collectionContentIds[$collectionKey]['collection_id'],
                        'sample_control_id' => $sampleAtimControls['id'],
                        'notes' => 'Imported by script'
                    ],
                    'specimen_details' => [
                    ],
                    $sampleAtimControls['detail_tablename'] => []
                ];
                $collectionContentIds[$collectionKey]['sample_master_ids']['blood'] = $this->importModel->customInsertRecord($sampleData);
            }
            // Create derivative
            if ($sampleType != 'blood' && !isset($collectionContentIds[$collectionKey]['sample_master_ids'][$sampleType])) {
                $sampleAtimControls = $this->importModel->getAtimControls('sample_controls', $sampleType);
                $sampleData = [
                    'sample_masters' => [
                        'collection_id' => $collectionContentIds[$collectionKey]['collection_id'],
                        'sample_control_id' => $sampleAtimControls['id'],
                        'parent_id' => $collectionContentIds[$collectionKey]['sample_master_ids']['blood'],
                        'notes' => 'Imported by script'
                    ],
                    'derivative_details' => [
                    ],
                    $sampleAtimControls['detail_tablename'] => []
                ];
                $collectionContentIds[$collectionKey]['sample_master_ids'][$sampleType] = $this->importModel->customInsertRecord($sampleData);
            }
            // Create aliquot
            $aliquotAtimControls = $this->importModel->getAtimControls('aliquot_controls', "$sampleType-tube");
            $storageMasterID = $this->getStorageMasterId($storageLabel);
            if (! $storageMasterID) {
                $storagePosition = null;
            } else {
                $this->completedStorageMasterIds[$storageMasterID] = $storageMasterID;
            }
            $aliquotData = [
                'aliquot_masters' => [
                    "barcode" => $barcode,
                    "aliquot_control_id" => $aliquotAtimControls['id'],
                    "collection_id" => $collectionContentIds[$collectionKey]['collection_id'],
                    "sample_master_id" => $collectionContentIds[$collectionKey]['sample_master_ids'][$sampleType],
                    "aliquot_label" => '',
                    'in_stock' => 'yes - available',
                    'storage_master_id' =>$storageMasterID,
                    'storage_coord_x' => $storagePosition,
                    'notes' => 'Imported by script'
                ],
                $aliquotAtimControls['detail_tablename'] => []
            ];
            $this->importModel->customInsertRecord($aliquotData);
            $this->allDataCreationCounters['Created aliquots']++;
        }
        
        // Creation :: Final queries
        //**********************************************************************************************************************************************************************************
                
        $finalQueries = [];
        foreach ($finalQueries as $newQuery) {
            $this->importModel->customQuery($newQuery);
        }
        
        $this->populateViews();
    }
    
    public function getStorageMasterId($storageLabel) {
        $id = isset($this->storageSelectionLabelToATiMIds[$storageLabel])? $this->storageSelectionLabelToATiMIds[$storageLabel] : null;
        if (!$id && strlen($storageLabel)) {
            list($freezer, $rack, $box) = explode('-', $storageLabel);
            $parentShortLabels = [];
            $parentId = null;
            foreach (['freezer' => $freezer, 'rack11' => $rack, 'box81' => $box] as $storageType => $shortLabel) {
                $selectionLabel = implode('', $parentShortLabels) . $shortLabel;
                if (! isset($this->storageSelectionLabelToATiMIds[$selectionLabel])) {
                    $storageAtimControls = $this->importModel->getAtimControls('storage_controls', "$storageType");
                    // Check storage exists else create storage
                    // Use ATiM model but could be also a query built in the script
                    // $query = "SELECT id FROM storage_masters WHERE selection_label ='$selectionLabel' AND deleted <> 1 AND storage_control_id = " . $storageAtimControls['id']
                    $atimStorage = $this->storageMasterModel->find('first', [
                            'conditions' => [
                                'StorageMaster.selection_label' => $selectionLabel,
                                'StorageMaster.storage_control_id' => $storageAtimControls['id']
                            ],
                            'recursive' => -1
                        ]);
                    if ($atimStorage) {
                        $this->storageSelectionLabelToATiMIds[$selectionLabel] = $atimStorage['StorageMaster']['id'];
                    } else {
                        //TODO Generated Selection Label Length Check
                        // Don't forget to add code in validateDataIntegrity() or insertDataIntoATiM() functions:
                        //  - To compare length of $shortLabel to storage_masters.short_label length to not generate SQL error in .
                        //  - To compare length of the generated selection label to storage_masters.selection_label length to not generate SQL error.
                        // Else customInsertRecord() will call migrationDie error
                        $storageData = [
                            'storage_masters' => [
                                "storage_control_id" => $storageAtimControls['id'],
                                "parent_id" => $parentId,
                                "short_label" => $shortLabel,   //Don't forget to compare length of $shortLabel to storage_masters.short_label length plus test selection_label length too 
                                'notes' => 'Imported by script'
                            ],
                            $storageAtimControls['detail_tablename'] => []
                        ];
                        $this->storageSelectionLabelToATiMIds[$selectionLabel] = $this->importModel->customInsertRecord($storageData);
                        if ($parentId) {
                            $this->completedStorageMasterIds[$parentId] = $parentId;
                        }
                        $this->allDataCreationCounters['Created storages']++;
                    }
                }
                $parentId = $this->storageSelectionLabelToATiMIds[$selectionLabel];
                $parentShortLabels[] = "$shortLabel-";
            }
            $id = $this->storageSelectionLabelToATiMIds[$storageLabel];
        }
        return $id;
    }
        
    //==================================================================================================================================================================================
    // END OF PROCESS
    //==================================================================================================================================================================================
    
    /**
     * Function to use to update data at the end of the script execution, set up messages as a migration summary to display to user, etc.
     *
     * Could be used to repopulate views, generate storage codes, etc.
     *
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->recordErrorAndMessage()" has to be used to record any message (BatchUploadControl::MESSAGE), warning (BatchUploadControl::WARNING) or error  (BatchUploadControl::ERROR) generated by this custom validation process. Remember that any error message will stop the script.
     *
     * @throws ImportException
     */
    public function importFinish()
    {
        // Complete migration summary
        //**********************************************************************************************************************************************************************************
        
        foreach ($this->allDataCreationCounters as $key => $value) {
            $this->importModel->recordErrorAndMessage(__('batch data upload summary'), BatchUploadControl::MESSAGE,
                "Migration counters.", "$key : $value", $key);
        }
    }
    
    /**
     * Function written here as an example to repopulate views.
     *
     * @throws ImportException
     */
    public function populateViews()
    {
        $queries = [];
        
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewCollection', true);
        $whereCriteria = "
            AND Collection.created = '{$this->importModel->importDate}'
            AND Collection.created_by = {$this->importModel->importedBy}
            AND Collection.modified = '{$this->importModel->importDate}'
            AND Collection.modified_by = {$this->importModel->importedBy}";
        $queries[] = 'INSERT INTO view_collections (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')';
        
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewSample', true);
        $whereCriteria = "
            AND SampleMaster.created = '{$this->importModel->importDate}'
            AND SampleMaster.created_by = {$this->importModel->importedBy}
            AND SampleMaster.modified = '{$this->importModel->importDate}'
            AND SampleMaster.modified_by = {$this->importModel->importedBy}";
        $queries[] = 'INSERT INTO view_samples (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')';
        
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewAliquot', true);
        $whereCriteria = "
            AND AliquotMaster.created = '{$this->importModel->importDate}'
            AND AliquotMaster.created_by = {$this->importModel->importedBy}
            AND AliquotMaster.modified = '{$this->importModel->importDate}'
            AND AliquotMaster.modified_by = {$this->importModel->importedBy}";
        $queries[] = 'INSERT INTO view_aliquots (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')';
        
        /*
         Not used in this script
        
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewAliquotUse', true);
        foreach(explode('UNION ALL', $viewModel::$tableQuery) as $newViewSelect) {
            if (preg_match('/SELECT CONCAT\(InventoryActionMaster/', $newViewSelect, $match)) {
                $whereCriteria = "
                    AND InventoryActionMaster.created = '{$this->importModel->importDate}'
                    AND InventoryActionMaster.created_by = {$this->importModel->importedBy}
                    AND InventoryActionMaster.modified = '{$this->importModel->importDate}'
                    AND InventoryActionMaster.modified_by = {$this->importModel->importedBy}";
                $queries[] = 'INSERT INTO view_aliquot_uses (' . str_replace('%%WHERE%%', $whereCriteria, $newViewSelect) . ')';
            }
        }
         */
        
        $viewModel = AppModel::getInstance('StorageLayout', 'ViewStorageMaster', true);
        // To rebuild view for any filled storage
        $queries[] = "DELETE FROM view_storage_masters WHERE id IN (" . implode(",", $this->completedStorageMasterIds) . ")";
        $whereCriteria = "
            AND ((
                StorageMaster.created = '{$this->importModel->importDate}'
                AND StorageMaster.created_by = {$this->importModel->importedBy}
                AND StorageMaster.modified = '{$this->importModel->importDate}'
                AND StorageMaster.modified_by = {$this->importModel->importedBy}
            ) OR (StorageMaster.id IN (" . implode(",", $this->completedStorageMasterIds) . ")))";
        $queries[] = 'INSERT INTO view_storage_masters (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')';
        
        foreach ($queries as $sql) {
            $this->importModel->customQuery($sql);
        }
    }
}