<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class MainScript
 * 
 * Template of custom hook file that could be used to develop batch upload script.
 * 
 * See importHookFileExample.php as an example.
 */
class MainScript
{
    private $importModel;
    
    private $options = [];

    // Variables requested and to complete
    // =======================================

    /**
     * List of actions the user has to have permission to launch to allow script execution.
     *
     * @var array Format:
     *      [
     *          'link1',
     *          'link2',
     *          
     *          'ClinicalAnnotation/Participants/add/',
     *          'ClinicalAnnotation/ConsentMasters/add/',
     *          
     *          ...
     *      ]
     */
    // 
    public $linksToCheckForPermissions = [];
    
    
    /**
     * List of ATiM tables and attached ATiM models that will be populated by the custom script
     * to lock them during import.
     *
     * @var array Format:
     *      [
     *      "table_name_1 ModelName1",
     *      "table_name_2 ModelName2"
     *      
     *      "participants  Participant",
     *      "consent_masters ConsentMaster",
     *      "consent_controls ConsentControl",
     *      "collections Collection",
     *      "sample_masters SampleMaster",
     *      "aliquot_masters AliquotMaster",
     *      "storage_masters StorageMaster",
     *      "view_collections ViewCollection",
     *      "view_samples ViewSample",
     *      "view_aliquots ViewAliquot",
     *      "view_aliquot_uses ViewAliquotUse",
     *      
     *      "view_storage_masters ViewStorageMaster"
     *      
     *      ...
     *      ]
     */
    public $populatedAtimTablesAndModelsList = [
    ];
    
    /**
     *  CSV fields names plus attached information use by ATiM import process to:
     *  - Create fields of the temporary table (table used by import process to copy the csv file data into this one and make them available to be processed and imported).
     *  - Validate the imported csv values based on different criteria (data type, data length, list of accepted values, etc) before to copy them into the temporary table.
     *  - Then copy them into the temporary table.
     * 
     * @var array
     * Format:
     * [
     *      "CSV Field 1" => [                                  // CSV field name.
     *            'tmp_table_field' => 'field_name_1',          // Name of the field in temporary table where the csv value will be recorded after validation.
     *            'field_type' => 'string',                     // Type of the field ('yes_no', 'yes_no_unknown', 'checkbox', 'varchar', 'string', 'integer', 'float', 'date', 'datetime', 
     *                                                          //   'time', 'select', 'CodingIcd10Ca', 'CodingIcd10Who', 'CodingIcdo3Morpho', 'CodingIcdo3Topo', 'CodingMCode').
     *                                                          //   -> Will help the system to validate the migrated data.
     *            'not_blank' => true,                          // Field can be blank or not.
     *            'unique_in_file' => false,                    // Field value can be duplicated or not in file.
     *                                                          //   Note: To compare that value is unique with database value, custom validations have to be written in MainScript.validateDataIntegrity().
     *            'field_length' => '250',                      // Max size for 'varchar', 'string', 'integer', 'float', 'select', 'CodingIcd10Ca', 'CodingIcd10Who', 
     *                                                          //   'CodingIcdo3Morpho', 'CodingIcdo3Topo', 'CodingMCode'
     *            'structure_domain_name_for_validation' => '', // When field_type is 'select', structure_value_domains.domain_name used to populate the 'select' field.
     *                                                          //   -> Will help the system to validate the migrated data (should be equal to the data recorded in database and not the translated value).
     *            'str_replace' => []                           // Used to replace csv value before data validation and insertion in temporary table [array|string $search , array|string $replace].
     *                                                          //   -> Replace all csv cell content occurrences of the search string with the replacement string.
     *            'cell_str_replace' => []                      // Used to replace csv value before data validation and insertion in temporary table [array|string $search , array|string $replace].
     *                                                          //   -> Replace all csv cell contents matching exactly the cell string with the replacement string.
     *      ],
     *      "CSV Field 2" => []
     *      ...
     * ]
     */
        
    public $csvFieldsToTmpTableFieldsDefinitions = [];

    /**
     * Additional fields to create in temporary table and used by custom import function.
     * 
     * As an example an additional column could be used to:
     * - generate a temporary participant unique key based on many imported values, and help to select rows in temporary table with data of a specific participant.
     * - create the datetime value from a date column and a time column.
     * - etc.
     *
     * Format:
     * [
     *   [
     *      "field_name" => 'my_custom_field',                  // Name of the field to create in temporary table
     *      "data_type" => 'varchar(500) NOT NULL DEFAULT ''',  // Data type of the field plus - DEFAULT VALUE requested
     *   ]
     * ]
     */ 
    public $additionalTmpTableFieldsDefinitions = [];
    
    // Variables specific to the custom script
    //========================================
        
    private $completedStorageMasterIds = [-1];
    private $myVariable1 = 0;
    // ...
    
    /**
     * MainScript constructor.
     * Do not remove.
     * 
     * @param BatchUploadControl $importModel
     */
    public function __construct(BatchUploadControl $importModel)
    {
        $this->importModel = $importModel;
        /**
         * Set parameter length of the fgetcsv() function.
         * Must be greater than the longest line (in characters) to be found in the CSV file (allowing for trailing line-end characters). 
         */
        $this->options['csv_line_buffer_length'] = 50000;
        $this->importModel->options = $this->options;
        $this->importModel->customImportClass = $this;
    }

    //==================================================================================================================================================================================
    // INIT
    //==================================================================================================================================================================================

    /**
     * Import init custom function. 
     * 
     * Function called by main import controller before any file content validation (based on $populatedAtimTablesAndModelsList definitions) 
     * and any copy of the file values into the temporary table.
     * 
     * Should be used to set script custom variables.
     * 
     * @throws ImportException
     */
    public function importInit()
    {
        // Check Permissions
        if (! AppController::checkLinkPermission('Plugin/Controller/Action/')) {;
            AppController::getInstance()->atimFlashError(__("you don't have permission to launch this batch data upload process"), "/Tools/Imports/importBatch/");
        }
    }
    
    //==================================================================================================================================================================================
    // INSERT DATA INTO TEMPORARY TABLE
    //==================================================================================================================================================================================
    
    /**
     * Process line data before 'trunk' validation (done by the ATiM trunk function) based on $csvFieldsToTmpTableFieldsDefinitions data.
     *
     * Function could be used to:
     *  - Manipulate the csv line data to change value by custom code before validation
     *
     * @param $csvLineData array CSV line data ['CSV Column Name' => 'CSV Column Value'.
     * @param $csvLineCounter string CSV line number of the parsed row.     *
     *
     * @return $csvLineData array.
     *
     * @throws ImportException
     */
    public function processLineDataBeforeTrunkValidation($csvLineData, $csvLineCounter) {
        return $csvLineData;
    }   
    
    /**
     * Process line data before insert in temporary table.
     *
     * Function could be used to:
     *  - Manipulate the validated and formatted CSV file line data before insertion into the temporary table.
     *  - Execute a last validation round on the formatted CSV file line data before insertion into the temporary table.
     *  - Not import line data after control by returning an empty array.
     *
     * @param $csvLineData array CSV line data ['CSV Column Name' => 'CSV Column Value'.
     * @param $sortedCsvLineDataToLoad array formatted and validated CSV line data to insert into the temporary table ['Temporary Table Field Name' => 'Value To Insert'].
     * @param $csvLineCounter string CSV line number of the parsed row.     *
     *
     * @return $sortedCsvLineDataToLoad array formatted and validated CSV line data to insert into the temporary table ['Temporary Table Field Name' => 'Value To Insert']. Return an empty array to skip the line.
     *
     * @throws ImportException
     */
    public function processFormatedLineDataBeforeInsertInTemporaryTable($csvLineData, $sortedCsvLineDataToLoad, $csvLineCounter) {
        return $sortedCsvLineDataToLoad;
    }
    
    //==================================================================================================================================================================================
    // VALIDATE DATA
    //==================================================================================================================================================================================
    
    /**
     * Custom validation function.
     * 
     * Function called by main import controller after the all CSV file data validation (based on $populatedAtimTablesAndModelsList definitions) 
     * has been done and the all CSVfile data have been recorded into the import temporary table.
     * 
     * Should be used to:
     * - Validate the data integrity of the imported data based on custom specific business rules and constraints attached to your local installation.
     * - Populate custom column in temporary table copying (atim)ids (participant_id, consent_master_id, etc), creating unique keys, etc.
     * - Validate two imported values with each other.
     * - Etc.
     * 
     * Function "$this->importModel->getTmpTableFieldNameFromCsvHeader();" can be used to retrieve temporary table field name from csv field name.
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->getAtimControls()" can be used to get record of a specific ATiM control table.
     * Function "$this->importModel->recordErrorAndMessage()" has to be used to record any message (BatchUploadControl::MESSAGE), warning (BatchUploadControl::WARNING) or error  (BatchUploadControl::ERROR) generated by this custom validation process. Remember that any error message will stop the script.
     * 
     * @throws ImportException
     */
    public function validateDataIntegrity()
    {}

    //==================================================================================================================================================================================
    // INSERT DATA INTO ATIM TABLES
    //==================================================================================================================================================================================

    /**
     * Function to use to insert data into ATiM tables. This function is launched by the script if no error message has been generated by 
     * validation functions (both trunk and custom code).
     * 
     * Function "$this->importModel->getTmpTableFieldNameFromCsvHeader();" can be used to retrieve temporary table field name from csv field name.
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->getAtimControls()" can be used to get record of a specific ATiM control table.
     * Function "$this->importModel->migrationDie()" has to be used to stop insertion for any code error.
     * Function "$this->importModel->customInsertRecord()" has to be used to insert data.
     * Function "$this->importModel->customQuery()" has to be used to update data.
     * 
     * @throws ImportException
     */
    public function insertDataIntoATiM()
    {}
    
    //==================================================================================================================================================================================
    // END OF PROCESS
    //==================================================================================================================================================================================
    
    /**
     * Function to use to update data at the end of the script execution, set up messages as a migration summary to display to user, etc.
     * 
     * Could be used to repopulate views, generate storage codes, etc.
     * 
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->recordErrorAndMessage()" has to be used to record any message (BatchUploadControl::MESSAGE), warning (BatchUploadControl::WARNING) or error  (BatchUploadControl::ERROR) generated by this custom validation process. Remember that any error message will stop the script.
     * 
     * @throws ImportException
     */
    public function importFinish()
    {
        //This call is added here as an example but does not have to be used necessarily.
        $this->populateViews();
    }
    
    /**
     * Function written here as an example to repopulate views.
     *
     * @throws ImportException
     */
    private function populateViews()
    {
        $queries = [];
            
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewCollection', true);
        $whereCriteria = "Collection.created = '{$this->importModel->importDate}'
            AND Collection.created_by = {$this->importModel->importedBy}
            AND Collection.modified = '{$this->importModel->importDate}'
            AND Collection.modified_by = {$this->importModel->importedBy}";
        $queries[] = 'INSERT INTO view_collections (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')'; 
        
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewSample', true);
        $whereCriteria = "SampleMaster.created = '{$this->importModel->importDate}'
            AND SampleMaster.created_by = {$this->importModel->importedBy}
            AND SampleMaster.modified = '{$this->importModel->importDate}'
            AND SampleMaster.modified_by = {$this->importModel->importedBy}";
        $queries[] = 'INSERT INTO view_samples (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')'; 
        
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewAliquot', true);
        $whereCriteria = "SampleMaster.created = '{$this->importModel->importDate}'
            AND AliquotMaster.created_by = {$this->importModel->importedBy}
            AND AliquotMaster.modified = '{$this->importModel->importDate}'
            AND AliquotMaster.modified_by = {$this->importModel->importedBy}";
        $queries[] = 'INSERT INTO view_aliquots (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')'; 
        
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewAliquotUse', true);
        foreach(explode('UNION ALL', $viewModel::$tableQuery) as $newViewSelect) {
            if (preg_match('/SELECT CONCAT\(SourceAliquot/', $newViewSelect, $match)) {
                $whereCriteria = "SourceAliquot.created = '{$this->importModel->importDate}'
                    AND SourceAliquot.created_by = {$this->importModel->importedBy}
                    AND SourceAliquot.modified = '{$this->importModel->importDate}'
                    AND SourceAliquot.modified_by = {$this->importModel->importedBy}";
                $queries[] = 'INSERT INTO view_aliquot_uses (' . str_replace('%%WHERE%%', $whereCriteria, $newViewSelect) . ')';
            } elseif (preg_match('/SELECT CONCAT\(Realiquoting/', $newViewSelect, $match)) {
                $whereCriteria = "Realiquoting.created = '{$this->importModel->importDate}'
                    AND Realiquoting.created_by = {$this->importModel->importedBy}
                    AND Realiquoting.modified = '{$this->importModel->importDate}'
                    AND Realiquoting.modified_by = {$this->importModel->importedBy}";
                $queries[] = 'INSERT INTO view_aliquot_uses (' . str_replace('%%WHERE%%', $whereCriteria, $newViewSelect) . ')';
            }
        }
        
        $viewModel = AppModel::getInstance('StorageLayout', 'ViewStorageMaster', true);
        // To rebuild view for any filled storage
        $queries[] = "DELETE FROM view_storage_masters WHERE id IN (" . implode(",", $this->completedStorageMasterIds) . ")";
        $whereCriteria = "
            AND ((
                StorageMaster.created = '{$this->importModel->importDate}'
                AND StorageMaster.created_by = {$this->importModel->importedBy}
                AND StorageMaster.modified = '{$this->importModel->importDate}'
                AND StorageMaster.modified_by = {$this->importModel->importedBy}
            ) OR (StorageMaster.id IN (" . implode(",", $this->completedStorageMasterIds) . ")))";
        $queries[] = 'INSERT INTO view_storage_masters (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')';
        
        foreach ($queries as $sql) {
            $this->importModel->customQuery($sql);
        }
    }
}