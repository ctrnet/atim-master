<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class MainScript
 * 
 * Template of custom hook file that could be used to develop batch upload script.
 *
 * In this example template you can import the Participants, collection, samples (specimen not derivative), aliquots
 *
 * The validations are on the uniqueness of the barcode, check the relation between sample and aliquot and also check if the samples are specimen
*/

//<editor-fold desc="The MySQL queries and template CSV files">

/* MySQL queries to create a sample import

INSERT IGNORE INTO `batch_upload_controls` (`name`, `version`, `script`, `description`, `max_csv_lines_number`, `flag_active`, `template_csv`, `flag_manipulate_confidential_data`)
VALUES
('Data Upload Example', '2', 'importsHookFileExample02.php', 'Example of custom hook file to upload some inventories on batch.', '-1', '1', 'DataUploadExampleTempalate02.csv', 1);

An example of the data CSV file:

Participant Id;Sample Type;Collection Date and Time;Barcode;Aliquot Type
p0002;ascite;2021-10-11 12:13;22222;tube
p150;Blood;2020-09-10 11:12;33333;whatman paper
p0001;Blood;2019-08-09 10:11;44444;tube
p220;urine;2018-07-08 09:10;55555;tube

Create the DataUploadExampleTempalate02.csv file inside the $uploadDirectory/$csvDirectory/templates and put the below data in it:
Participant Id;Sample Type;Collection Date and Time;Aliquot Type;Barcode
(string);(string from list);(datetime);(string from list);(string)
(required);(required);(required);(required);(required)

*/

//</editor-fold>


class MainScript
{
    private $importModel;
    
    private $options = [];

    private $allDataCreationCounters = [
        'Created participants' => 0,
        'Updated participants' => 0,
        'Created collections' => 0,
        'Created aliquots' => 0,
        'Created samples' => 0,
    ];
    
    // List of actions the user has to have permission to launch to allow script execution
    public $linksToCheckForPermissions = [
        'ClinicalAnnotation/Participants/add/',
        'InventoryManagement/Collections/add/',
        'InventoryManagement/SampleMasters/add/',
        'InventoryManagement/AliquotMasters/add/'
    ];
    
    // Variables requested and to complete
    // =======================================

    /**
     * List of ATiM tables and attached ATiM models that will be populated by the custom script
     * to lock them during import.
     *
     * @var array Format:
     *      [
     *      "table_name_1 ModelName1",
     *      "table_name_2 ModelName2"
     *      ...
     *      ]
     */
    public $populatedAtimTablesAndModelsList = [
         "participants  Participant",
         "collections Collection",
         "sample_masters SampleMaster",
         "aliquot_masters AliquotMaster",
         "view_collections ViewCollection",
         "view_samples ViewSample",
         "view_aliquots ViewAliquot",
    ];
    
    /**
     *  CSV fields names plus attached information use by ATiM import process to:
     *  - Create fields of the temporary table (table used by import process to copy the csv file data into this one and make them available to be processed and imported).
     *  - Validate the imported csv values based on different criteria (data type, data length, list of accepted values, etc) before to copy them into the temporary table.
     *  - Then copy them into the temporary table.
     * 
     * @var array
     * Format:
     * [
     *      "CSV Field 1" => [                                  // CSV field name.
     *            'tmp_table_field' => 'field_name_1',          // Name of the field in temporary table where the csv value will be recorded after validation.
     *            'field_type' => 'string',                     // Type of the field ('yes_no', 'yes_no_unknown', 'checkbox', 'varchar', 'string', 'integer', 'float', 'date', 'datetime', 
     *                                                          //   'time', 'select', 'CodingIcd10Ca', 'CodingIcd10Who', 'CodingIcdo3Morpho', 'CodingIcdo3Topo', 'CodingMCode').
     *                                                          //   -> Will help the system to validate the migrated data.
     *            'not_blank' => true,                          // Field can be blank or not.
     *            'unique_in_file' => false,                    // Field value can be duplicated or not in file.
     *                                                          //   Note: To compare that value is unique with database value, custom validations have to be written in MainScript.validateDataIntegrity().
     *            'field_length' => '250',                      // Max size for 'varchar', 'string', 'integer', 'float', 'select', 'CodingIcd10Ca', 'CodingIcd10Who', 
     *                                                          //   'CodingIcdo3Morpho', 'CodingIcdo3Topo', 'CodingMCode'
     *            'structure_domain_name_for_validation' => '', // When field_type is 'select', structure_value_domains.domain_name used to populate the 'select' field.
     *                                                          //   -> Will help the system to validate the migrated data (should be equal to the data recorded in database and not the translated value).
     *            'str_replace' => []                           // Used to replace csv value before data validation and insertion in temporary table [array|string $search , array|string $replace].
     *                                                          //   -> Replace all csv cell content occurrences of the search string with the replacement string.
     *            'cell_str_replace' => []                      // Used to replace csv value before data validation and insertion in temporary table [array|string $search , array|string $replace].
     *                                                          //   -> Replace all csv cell contents matching exactly the cell string with the replacement string.
     *      ],
     *      "CSV Field 2" => []
     *      ...
     * ]
     */
        
    public $csvFieldsToTmpTableFieldsDefinitions = [
        'Participant Id' => [
            'tmp_table_field' => 'participant_identifier',
            'field_type' => 'string',
            'not_blank' => true,
            'unique_in_file' => false,
            'field_length' => '50',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Collection Date and Time' => [
            'tmp_table_field' => 'collection_datetime',
            'field_type' => 'datetime',
            'not_blank' => true,
            'unique_in_file' => false,
            'field_length' => '',
            'structure_domain_name_for_validation' => 'collection_date',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Sample Type' => [
            'tmp_table_field' => 'sample_type',
            'field_type' => 'select',
            'not_blank' => true,
            'unique_in_file' => false,
            'field_length' => '30',
            'structure_domain_name_for_validation' => 'sample_type',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Aliquot Type' => [
            'tmp_table_field' => 'aliquot_type',
            'field_type' => 'select',
            'not_blank' => true,
            'unique_in_file' => false,
            'field_length' => '30',
            'structure_domain_name_for_validation' => 'aliquot_type',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
        'Barcode' => [
            'tmp_table_field' => 'barcode',
            'field_type' => 'string',
            'not_blank' => true,
            'unique_in_file' => true,
            'field_length' => '60',
            'structure_domain_name_for_validation' => '',
            'str_replace' => [],
            'cell_str_replace' => []
        ],
    ];

    /**
     * Additional fields to create in temporary table and used by custom import function.
     * 
     * As an example an additional column could be used to:
     * - generate a temporary participant unique key based on many imported values, and help to select rows in temporary table with data of a specific participant.
     * - create the datetime value from a date column and a time column.
     * - etc.
     *
     * Format:
     * [
     *   [
     *      "field_name" => 'my_custom_field',                  // Name of the field to create in temporary table
     *      "data_type" => 'varchar(500) NOT NULL DEFAULT ''',  // Data type of the field plus - DEFAULT VALUE requested
     *   ]
     * ]
     */
    public $additionalTmpTableFieldsDefinitions = [
        ['field_name' => 'atim_participant_id', 'data_type' => "int(11) DEFAULT NULL"],
        ['field_name' => 'sample_control_id', 'data_type' => "int(11) DEFAULT NULL"],
    ];

    
    // Variables specific to the custom script
    //========================================
        
    private $completedStorageMasterIds = [-1];
    private $myVariable1 = 0;
    // ...
    
    /**
     * MainScript constructor.
     * Do not remove.
     * 
     * @param BatchUploadControl $importModel
     */
    public function __construct(BatchUploadControl $importModel)
    {
        $this->importModel = $importModel;
        /**
         * Set parameter length of the fgetcsv() function.
         * Must be greater than the longest line (in characters) to be found in the CSV file (allowing for trailing line-end characters).
         */
        $this->options['csv_line_buffer_length'] = 50000;
        $this->importModel->options = $this->options;
        $this->importModel->customImportClass = $this;
    }

    //==================================================================================================================================================================================
    // INIT
    //==================================================================================================================================================================================

    /**
     * Import init custom function. 
     * 
     * Function called by main import controller before any file content validation (based on $populatedAtimTablesAndModelsList definitions) 
     * and any copy of the file values into the temporary table.
     * 
     * Should be used to set script custom variables.
     * 
     * @throws ImportException
     */
    public function importInit()
    {
        // Set up migration counter to 0 in messages list
        foreach ($this->allDataCreationCounters as $key => $value) {
            $this->importModel->recordErrorAndMessage(__('batch data upload summary'), BatchUploadControl::MESSAGE,
                "Migration counters.", "$key : $value", $key);
        }

        // Add unknown pattern to the list of patterns used for not accuracy date (2021-{pattern}-{pattern} as an example)
        $this->importModel->unknownMnHrDayMonthPatterns[] = '.';
        $this->importModel->unknownMnHrDayMonthPatterns[] = '??';

        $this->importModel->addToTableList(["{$this->importModel->temporaryTableName} T", "participants P", "sample_controls SC"]);
    }
    
    //==================================================================================================================================================================================
    // INSERT DATA INTO TEMPORARY TABLE
    //==================================================================================================================================================================================
    
    /**
     * Process line data before 'trunk' validation (done by the ATiM trunk function) based on $csvFieldsToTmpTableFieldsDefinitions data.
     *
     * Function could be used to:
     *  - Manipulate the csv line data to change value by custom code before validation
     *
     * @param $csvLineData array CSV line data ['CSV Column Name' => 'CSV Column Value'.
     * @param $csvLineCounter string CSV line number of the parsed row.     *
     *
     * @return $csvLineData array.
     *
     * @throws ImportException
     */
    public function processLineDataBeforeTrunkValidation($csvLineData, $csvLineCounter) {
        return $csvLineData;
    }   
    
    /**
     * Process line data before insert in temporary table.
     *
     * Function could be used to:
     *  - Manipulate the validated and formatted CSV file line data before insertion into the temporary table.
     *  - Execute a last validation round on the formatted CSV file line data before insertion into the temporary table.
     *  - Not import line data after control by returning an empty array.
     *
     * @param $csvLineData array CSV line data ['CSV Column Name' => 'CSV Column Value'.
     * @param $sortedCsvLineDataToLoad array formatted and validated CSV line data to insert into the temporary table ['Temporary Table Field Name' => 'Value To Insert'].
     * @param $csvLineCounter string CSV line number of the parsed row.     *
     *
     * @return $sortedCsvLineDataToLoad array formatted and validated CSV line data to insert into the temporary table ['Temporary Table Field Name' => 'Value To Insert']. Return an empty array to skip the line.
     *
     * @throws ImportException
     */
    public function processFormatedLineDataBeforeInsertInTemporaryTable($csvLineData, $sortedCsvLineDataToLoad, $csvLineCounter) {
        return $sortedCsvLineDataToLoad;
    }
    
    //==================================================================================================================================================================================
    // VALIDATE DATA
    //==================================================================================================================================================================================
    
    /**
     * Custom validation function.
     * 
     * Function called by main import controller after the all CSV file data validation (based on $populatedAtimTablesAndModelsList definitions) 
     * has been done and the all CSVfile data have been recorded into the import temporary table.
     * 
     * Should be used to:
     * - Validate the data integrity of the imported data based on custom specific business rules and constraints attached to your local installation.
     * - Populate custom column in temporary table copying (atim)ids (participant_id, consent_master_id, etc), creating unique keys, etc.
     * - Validate two imported values with each other.
     * - Etc.
     * 
     * Function "$this->importModel->getTmpTableFieldNameFromCsvHeader();" can be used to retrieve temporary table field name from csv field name.
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->getAtimControls()" can be used to get record of a specific ATiM control table.
     * Function "$this->importModel->recordErrorAndMessage()" has to be used to record any message (BatchUploadControl::MESSAGE), warning (BatchUploadControl::WARNING) or error  (BatchUploadControl::ERROR) generated by this custom validation process. Remember that any error message will stop the script.
     * 
     * @throws ImportException
     */
    public function validateDataIntegrity()
    {
        // Participant Id           => $participantId
        // Collection Date and Time => $collectionDateAndTime
        // Sample Type              => $sampleType
        // Aliquot Type              => $aliquotType
        // Barcode                  => $barcode
        foreach (array_keys($this->csvFieldsToTmpTableFieldsDefinitions) as $csvFieldName){
            ${Inflector::variable($csvFieldName)} = $this->importModel->getTmpTableFieldNameFromCsvHeader($csvFieldName);
        }

        //Barcode unique validation

        $query = "
            SELECT GROUP_CONCAT({$barcode}) AS `Barcode`, GROUP_CONCAT(csv_line) as `Lines`
            FROM {$this->importModel->temporaryTableName} WHERE {$barcode} in (
                SELECT barcode as B FROM aliquot_masters
            )";

        $duplicatedBarcodes = $this->importModel->getSelectQueryResult($query);
        if (!empty($duplicatedBarcodes[0]['Barcode'])){
            $this->importModel->migrationDie("The barcode should be unique. There are some Barcodes already exist in ATiM database ({$duplicatedBarcodes[0]['Barcode']}), Lines ({$duplicatedBarcodes[0]['Lines']})");
        }

        // Set Participant.id if the identifier already exist in ATiM
        $query = "
            UPDATE {$this->importModel->temporaryTableName} T, participants P
            SET T.atim_participant_id = P.id
            WHERE 
                T.$participantId = P.participant_identifier AND 
                P.deleted != 1;
        ";
        $this->importModel->customQuery($query);

        //Check if the Sample is derivative or specimen
        $query = "
            SELECT $sampleType sampleType, GROUP_CONCAT(csv_line) as `lines` 
            FROM {$this->importModel->temporaryTableName}
            GROUP BY $sampleType
        ";
        $sampleTypes = $this->importModel->getSelectQueryResult($query);
        $derivativeSamples = [];
        foreach ($sampleTypes as $sampleTypeLine){
            $sampleControlRow = $this->importModel->getAtimControls("sample_controls", $sampleTypeLine['sampleType']);
            if ($sampleControlRow['sample_category'] == 'derivative'){
                $derivativeSamples[$sampleTypeLine['sampleType']] = $sampleTypeLine['lines'];
            }
        }
        if (!empty($derivativeSamples)){
            $errorMessage = sprintf("This batch import support just the specimens but some derivative samples (%s) are entered (see lines: %s).", implode(",", array_keys($derivativeSamples)), implode(",", array_values($derivativeSamples)));
            $this->importModel->migrationDie($errorMessage);
        }

        //Check if the Aliquot Control is related to the Sample Control
        $query = "
            SELECT DISTINCT CONCAT($sampleType, '-', $aliquotType) sampleTypeAliquotType
            FROM {$this->importModel->temporaryTableName}
        ";
        $sampleTypeAliquotTypes = $this->importModel->getSelectQueryResult($query);
        foreach ($sampleTypeAliquotTypes as $sampleTypeAliquotType){
            $this->importModel->getAtimControls("aliquot_controls", $sampleTypeAliquotType['sampleTypeAliquotType']);
        }

        // Set Sample_controls.id to the temp table
        $query = "
            UPDATE {$this->importModel->temporaryTableName} T, sample_controls SC
            SET T.sample_control_id = SC.id
            WHERE 
                T.$sampleType = SC.sample_type;
        ";
        $this->importModel->customQuery($query);
    }

    //==================================================================================================================================================================================
    // INSERT DATA INTO ATIM TABLES
    //==================================================================================================================================================================================

    /**
     * Function to use to insert data into ATiM tables. This function is launched by the script if no error message has been generated by 
     * validation functions (both trunk and custom code).
     * 
     * Function "$this->importModel->getTmpTableFieldNameFromCsvHeader();" can be used to retrieve temporary table field name from csv field name.
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->getAtimControls()" can be used to get record of a specific ATiM control table.
     * Function "$this->importModel->migrationDie()" has to be used to stop insertion for any code error.
     * Function "$this->importModel->customInsertRecord()" has to be used to insert data.
     * Function "$this->importModel->customQuery()" has to be used to update data.
     * 
     * @throws ImportException
     */
    public function insertDataIntoATiM()
    {
        foreach (array_keys($this->csvFieldsToTmpTableFieldsDefinitions) as $csvFieldName){
            ${Inflector::variable($csvFieldName)} = $this->importModel->getTmpTableFieldNameFromCsvHeader($csvFieldName);
        }

        $query = "
            SELECT 
                   {$participantId} as participantId, 
                   {$collectionDateAndTime} as collectionDateAndTime, 
                   {$collectionDateAndTime}_accuracy as collectionDateAndTimeAccuracy, 
                   {$sampleType} as theSampleType, 
                   {$aliquotType} as theAliquotType, 
                   {$barcode} as barcode,
                   atim_participant_id as atimParticipantId,
                   sample_control_id as sampleControlId
                   
            FROM {$this->importModel->temporaryTableName};
        ";

        $allParticipantIdentifiers = [];
        foreach($this->importModel->getSelectQueryResult($query) as $csvLine){
            extract($csvLine);


            //<editor-fold desc="Check if the participant exists or not">
            if (!empty($allParticipantIdentifiers[$participantId])){
                $atimParticipantId = $allParticipantIdentifiers[$participantId];
                $this->allDataCreationCounters['Updated participants'] ++;
            }elseif(!empty($atimParticipantId)){
                $allParticipantIdentifiers[$participantId] = $atimParticipantId;
                $this->allDataCreationCounters['Updated participants'] ++;
            }else{
                $participantData = [
                    'participants' => [
                        'participant_identifier' => $participantId,
                        'notes' => 'Imported by script'
                    ]
                ];
                $atimParticipantId = $this->importModel->customInsertRecord($participantData);
                $this->allDataCreationCounters['Created participants'] ++;
            }
            $allParticipantIdentifiers[$participantId] = $atimParticipantId;
            //</editor-fold>

            //<editor-fold desc="Save the Collection Data">
            $collectionData = [
                'collections' => [
                    'participant_id' => $atimParticipantId,
                    'collection_datetime' => $collectionDateAndTime,
                    'collection_datetime_accuracy' => $collectionDateAndTimeAccuracy
                ]
            ];
            $collectionId = $this->importModel->customInsertRecord($collectionData);
            $this->allDataCreationCounters['Created collections'] ++;
            //</editor-fold>

            //<editor-fold desc="Save the Sample Data">
            $sampleDetail = $this->importModel->getAtimControls('sample_controls', $theSampleType);
            $sampleDetailTableName = $sampleDetail['detail_tablename'];
            $sampleData = [
                'sample_masters' => [
                    'collection_id' => $collectionId,
                    'sample_control_id' => $sampleControlId,
                    'notes' => 'Imported by script'
                ],
                'specimen_details' => [
                ],
                $sampleDetailTableName =>[
                ]
            ];

            $sampleMasterId = $this->importModel->customInsertRecord($sampleData);
            $this->allDataCreationCounters['Created samples'] ++;

            $query = "
                UPDATE sample_masters 
                SET initial_specimen_sample_id = $sampleMasterId
                WHERE id = $sampleMasterId
            ";
            $this->importModel->customQuery($query);
            //</editor-fold>

            //<editor-fold desc="Save the Aliquot Data">
            $aliquotControlDetail = $this->importModel->getAtimControls('aliquot_controls', $theSampleType . "-" . $theAliquotType);
            $aliquotDetailTableName = $aliquotControlDetail['detail_tablename'];
            $aliquotControlId = $aliquotControlDetail['id'];
            $aliquotData = [
                'aliquot_masters' => [
                    'barcode' => $barcode,
                    'aliquot_control_id' => $aliquotControlId,
                    'collection_id' => $collectionId,
                    'sample_master_id' => $sampleMasterId,
                    'notes' => 'Imported by script'
                ],
                $aliquotDetailTableName =>[
                ]
            ];
            $aliquotMasterId = $this->importModel->customInsertRecord($aliquotData);
            $this->allDataCreationCounters['Created aliquots'] ++;
            //</editor-fold>

        }
    }
    
    //==================================================================================================================================================================================
    // END OF PROCESS
    //==================================================================================================================================================================================
    
    /**
     * Function to use to update data at the end of the script execution, set up messages as a migration summary to display to user, etc.
     * 
     * Could be used to repopulate views, generate storage codes, etc.
     * 
     * Function "$this->importModel->getSelectQueryResult()" can be used to query the temporary table.
     * Function "$this->importModel->recordErrorAndMessage()" has to be used to record any message (BatchUploadControl::MESSAGE), warning (BatchUploadControl::WARNING) or error  (BatchUploadControl::ERROR) generated by this custom validation process. Remember that any error message will stop the script.
     * 
     * @throws ImportException
     */
    public function importFinish()
    {

        foreach ($this->allDataCreationCounters as $key => $value) {
            $this->importModel->recordErrorAndMessage(__('batch data upload summary'), BatchUploadControl::MESSAGE,
                "Migration counters.", "$key : $value", $key);
        }

        //This call is added here as an example but does not have to be used necessarily.
        $this->populateViews();
    }
    
    /**
     * Function written here as an example to repopulate views.
     *
     * @throws ImportException
     */
    private function populateViews()
    {
        $queries = [];
            
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewCollection', true);
        $whereCriteria = "
            AND Collection.created = '{$this->importModel->importDate}'
            AND Collection.created_by = {$this->importModel->importedBy}
            AND Collection.modified = '{$this->importModel->importDate}'
            AND Collection.modified_by = {$this->importModel->importedBy}";
        $queries[] = 'INSERT INTO view_collections (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')'; 
        
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewSample', true);
        $whereCriteria = "
            AND SampleMaster.created = '{$this->importModel->importDate}'
            AND SampleMaster.created_by = {$this->importModel->importedBy}
            AND SampleMaster.modified = '{$this->importModel->importDate}'
            AND SampleMaster.modified_by = {$this->importModel->importedBy}";
        $queries[] = 'INSERT INTO view_samples (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')'; 
        
        $viewModel = AppModel::getInstance('InventoryManagement', 'ViewAliquot', true);
        $whereCriteria = "
            AND SampleMaster.created = '{$this->importModel->importDate}'
            AND AliquotMaster.created_by = {$this->importModel->importedBy}
            AND AliquotMaster.modified = '{$this->importModel->importDate}'
            AND AliquotMaster.modified_by = {$this->importModel->importedBy}";
        $queries[] = 'INSERT INTO view_aliquots (' . str_replace('%%WHERE%%', $whereCriteria, $viewModel::$tableQuery) . ')'; 

        foreach ($queries as $sql) {
            $this->importModel->customQuery($sql);
        }
    }
}