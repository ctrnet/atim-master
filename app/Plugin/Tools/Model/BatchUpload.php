<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

class BatchUpload extends AppModel
{

    public $hasMany = [
        'BatchUploadMessage' => [
            'className' => 'Tools.BatchUploadMessage',
            'foreignKey' => 'batch_upload_id',
        ]
    ];

    public $useTable = 'batch_uploads';

    public function summary($variables =[])
    {
        $return = false;
        if (!empty($variables['BatchUpload.id'])){
            $this->unbindModel([
                'hasMany' => [
                    'BatchUploadMessage'
                ]
            ]);
            $this->bindModel(array(
                'belongsTo' => array(
                    'BatchUploadControl' => array(
                        'className' => 'Tools.BatchUploadControl',
                        'foreignKey' => 'batch_upload_control_id'
                    )
                )
            ));
            $result = $this->find('first', [
                'conditions' => [
                    'BatchUpload.id' => $variables['BatchUpload.id']
                ]
            ]);
            if (!empty($result['BatchUpload'])){
                $data = ['ImportList' => array_merge($result['BatchUpload'], $result['BatchUploadControl'])];
                $return = array(
                    'title' => array(
                        null,
                        __("batch upload summary")
                    ),
                    'data' => $data,
                    'structure alias' => 'import_list'
                );
            }
        }

        return $return;
    }
    
    /**
     * Return true when a user can access data (csv source file, upload summary) of a previous batch upload.
     * 
     * Will be replaced in the futur by a function management similar than batchset and databrowser sharing.
     * See https://gitlab.com/ctrnet/atim/-/issues/332
     * 
     * @return boolean true when access allowed
     */
    public function accessToImportDataAllowed($batchUploadId) {
        $this->unbindModel([
            'hasMany' => [
                'BatchUploadMessage'
            ]
        ]);
        $this->bindModel(array(
            'belongsTo' => array(
                'BatchUploadControl' => array(
                    'className' => 'Tools.BatchUploadControl',
                    'foreignKey' => 'batch_upload_control_id'
                )
            )
        )); 
        $data = $this->find('first', [
            'conditions' => [
                'BatchUpload.id' => $batchUploadId
            ]
        ]);
        $userData = AppController::getInstance()->Session->read('Auth.User');
        if ($data['BatchUpload']['user_id'] != $userData['id'] && $userData['Group']['id'] != 1) {
            // Only user and administartor can access the batch upload summary
            // Rule to update after https://gitlab.com/ctrnet/atim/-/issues/332 completion.
            return false;
        }
        if ($data['BatchUploadControl']['flag_manipulate_confidential_data'] && ! $userData['Group']['flag_show_confidential']) {
            return false;
        }
        return true;
    }

}