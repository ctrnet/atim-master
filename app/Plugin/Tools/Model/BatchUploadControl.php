<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class Import
 */
class BatchUploadControl extends AppModel
{

    private static $instance;

    public $options;

    public $useTable = 'batch_upload_controls';
    public $scriptFileName = '';

    public $csvFileName = '';
    public $csvTempFileName = '';
    public $csvSeparator = '';
    public $csvFileSize = 0;
    public $csvDataLanguage = 'en';
    public $maxRecordNumber = -1;
    public $csvFileHandle;
    public $csvFileHeaders = [];
    public $csvLineNumber;
    public $csvFileContents = [];
    public $datasource;
    public $csvUploadDirectory = "";
    
    public $isUploadTest = false;
    
    private $allTableList = [];
    private $heaviestTableList = [];
    private $csvFieldsInfos = [];
    private $scriptFieldsForTmpTable = [];

    public $temporaryTableName = 'batch_upload_csv_file_data_tmp';
    public $messages = [];

    const ERROR = 'error';
    const WARNING = 'warning';
    const MESSAGE = 'message';

    public $migrationProcessVersion;
    public $importDate;
    public $importedBy;
    public $atimControls;
    public $domainsValues;
    public $importSummary;
    public $importSummaryTypeCounter;
    public $modifiedDatabaseTablesList;
    public $batchUploadControlId;
    public $unknownMnHrDayMonthPatterns = ['99', 'xx', '?'];
    
    public $customImportClass;
    
    private $structureValueDomainModel = null;
    private $codingModels = [];
    
    public $specialCharReplaceForSql = ["\n" => " ", "\r" => " ", "'" => "''", "\\" => "\\\\", '"' => '\"'];
    
    //</editor-fold>

    //==================================================================================================================================================================================
    // MAIN FUNCTIONS
    //==================================================================================================================================================================================
    
    /**
     * @return BatchUploadControl
     */
    public static function getOrCreate()
    {
        if (!isset(self::$instance)) {
            self::$instance = new BatchUploadControl();
        }
        return self::$instance;
    }

    public function initSystemScripts()
    {
        $this->migrationProcessVersion = '';
        
        $this->importDate = date('Y-m-d H:i:s');
        $this->importedBy = $_SESSION['Auth']['User']['id'];

        $this->atimControls = [];

        // ---- VALUE DOMAIN FUNCTIONS & EXCEL LIST VALIDATION FUNCTION --------------------------------------------------------------------------------------------------------------------
        $this->domainsValues = [];

        // ---- TITLE, ERROR AND MESSAGE DISPLAY  ------------------------------------------------------------------------------------------------------------------------------------------
        $this->importSummary = [];
        $this->importSummaryTypeCounter = [];

        $this->modifiedDatabaseTablesList = [];
    }

    public function lockAllTables()
    {
        $query = "LOCK tables " . implode(" write, ", $this->allTableList) . " write;";
        $this->query($query);
    }

    public function unlockAllTables()
    {
        $this->query("UNLOCK TABLES;");
    }

    public function reIndexing()
    {
        $this->query("ANALYZE TABLE ". implode(", ", $this->heaviestTableList) . ";");
    }

    public function finalizing()
    {
        $this->reIndexing();
        $this->unlockAllTables();
    }

    private function getAllTablesName()
    {
        $tableList = $this->query("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA='{$this->datasource->config['database']}' ORDER BY TABLE_NAME");
        $this->allTableList = array_map('strtolower', Hash::extract($tableList, "{n}.TABLES.TABLE_NAME"));
    }

    /**
     * Set in system all tables and linked models that will be used by the custom script to lock them.
     * 
     * @param array $tables List of tables names and models in array("table_name TableModelName", "...")
     */
    public function addToTableList($tables)
    {
        if (is_string($tables)) {
            $tables = [$tables];
        }

        foreach ($tables as $table) {
            if (!in_array(strtolower($table), $this->allTableList)) {
                $this->allTableList[] = $table;
            }
        }
    }

    /**
     * Drop and Create the temporary table used to import csv values
     * and Lock all the ATiM tables that will be used by the script.
     * 
     * @param $fields the column of the tmp table
     * @throws ImportException
     */
    public function createTempTable($fields)
    {
        $this->customQuery("DROP TABLE IF EXISTS {$this->temporaryTableName};");
        $query = "CREATE TABLE {$this->temporaryTableName}( " . implode(', ', $fields) . ");";

        $this->customQuery($query);

        if (!in_array(strtolower($this->temporaryTableName), $this->allTableList)) {
            $this->allTableList[] = $this->temporaryTableName;
        }
    }

    /**
     * @return mixed
     */
    public function getBatchUploadControlId()
    {
        return $this->batchUploadControlId;
    }

    /**
     * @return array
     */
    public function getAllTableList()
    {
        return $this->allTableList;
    }

    public function saveCsvFile($prefix)
    {
        $return = copy($this->csvTempFileName, $this->csvUploadDirectory . DS . $prefix . $this->csvFileName);
        if ($return){
            chmod($this->csvUploadDirectory . DS . $prefix . $this->csvFileName, 0600);
        }
        return $return;
    }

    /**
     * @param $data
     */
    public function initialization($data)
    {
        $this->datasource = $this->getDataSource();

        $this->getAllTablesName();

        $this->heaviestTableList = $this->getHeaviestTablesName();
        
        $this->initSystemScripts();
        
        $validationErrors = [];
        
        $data = $data['FunctionManagement'];
        
        $batchUploadControl = $this->getOrRedirect($data['script']);
        if (! $batchUploadControl['BatchUploadControl']['flag_active']) {
            AppController::getInstance()->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        if ($batchUploadControl['BatchUploadControl']['flag_manipulate_confidential_data'] && ! AppController::getInstance()->Session->read('Auth.User.Group.flag_show_confidential')) {
            AppController::getInstance()->atimFlashError(__("you don't have permission to launch this batch data upload process"), "javascript:history.back();");
        }
        
        $this->maxRecordNumber = $batchUploadControl['BatchUploadControl']['max_csv_lines_number'];
        $this->batchUploadControlId = $batchUploadControl['BatchUploadControl']['id'];
        
        $data['script'] = $batchUploadControl['BatchUploadControl']['script'];
        $this->scriptFileName = APP . "Plugin" . DS . "Tools" . DS . "Controller" . DS . "Hook" . DS . $batchUploadControl['BatchUploadControl']['script'];
        
        $this->csvSeparator = strlen($data['define_csv_separator'])? $data['define_csv_separator'] : CSV_SEPARATOR;
        $this->csvFileSize = $data['filename']['size'];
        $this->csvDataLanguage = ($data['csv_value_language'] == 'fre')? 'fr': 'en';
        $this->csvFileName = $data['filename']['name'];
        $this->csvTempFileName = $data['filename']['tmp_name'];
        
        $this->recordErrorAndMessage(__("batch data upload summary"), self::MESSAGE, __('batch data upload process'), $batchUploadControl['BatchUploadControl']['name'] . (strlen($batchUploadControl['BatchUploadControl']['version'])? (' - ' . $batchUploadControl['BatchUploadControl']['version']) : ''));
        $this->recordErrorAndMessage(__("batch data upload summary"), self::MESSAGE, __("date"), $this->importDate);
        if ($this->migrationProcessVersion) {
            $this->recordErrorAndMessage(__("batch data upload summary"), self::MESSAGE, __("batch data upload process version"), $this->migrationProcessVersion);
        }
        if ($data['test_upload']) {
            $this->isUploadTest = true;
            $this->recordErrorAndMessage(__("batch data upload summary"), self::WARNING, __('test of the batch upload title'), __("test of the batch upload - no recorded/updated data"));
        }
        if ($this->csvFileName) {
            $this->recordErrorAndMessage(__("batch data upload summary"), self::MESSAGE, __('file'), $this->csvFileName);
        }
        if (! $this->csvTempFileName) {
            $validationErrors[] = __("the file for upload has not been selected", $data['script']);
        }
        if (!file_exists($this->scriptFileName)) {
            $validationErrors[] = __("the script for upload [%s] does not exist", $data['script']);
        }
        if (strtolower(pathinfo($data['filename']['name'], PATHINFO_EXTENSION)) != 'csv') {
            $validationErrors[] = __("the data file should be in csv format");
        }
        if (strlen($this->scriptFileName) > 150) {
            $validationErrors[] = __("the data file name is too long");
        }

        $uploadDir = Configure::read('uploadDirectory');
        if (! is_dir($uploadDir)) {
            if (Configure::read('debug') > 0) {
                AppController::addWarningMsg("The upload directory [$uploadDir] does not exist.");
            }
            AppController::getInstance()->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        if (empty(Configure::read('csvDirectory'))) {
            if (Configure::read('debug') > 0) {
                AppController::addWarningMsg("The csvDirectory directory should be set in core.php.");
            }
            AppController::getInstance()->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        $this->csvUploadDirectory = $uploadDir . DS . Configure::read('csvDirectory');
        if (empty($this->csvUploadDirectory) || !is_dir($this->csvUploadDirectory)) {
            mkdir($this->csvUploadDirectory);
            if (empty($this->csvUploadDirectory) || !is_dir($this->csvUploadDirectory)) {
                $validationErrors[] = __('the directory for uploading the CSV file is not set, contact your administrator');
            }
        } else {
            $permission = substr(sprintf('%o', fileperms($this->csvUploadDirectory)), -4);
            $tempFile = $this->csvUploadDirectory . DS . "temp.tmp";
            $lockFileVariable = fopen($tempFile, "a");
            fclose($lockFileVariable);
            if (!file_exists($tempFile)) {
                if ($permission != '0777') {
                    AppController::addWarningMsg(__('the permission of csv directory is not correct'));
                } else {
                    AppController::addWarningMsg(__("unable to create csv file"));
                }
            } else {
                unlink($tempFile);
            }
        }

        $this->validationErrors = $validationErrors;
    }
    
    /**
     * @param $requiredLinksPermissions
     */
    public function checkLinksPermissions($requiredLinksPermissions)
    {
        foreach ($requiredLinksPermissions as $link) {
            if (! AppController::checkLinkPermission($link)) {
                AppController::getInstance()->atimFlashError(__("you don't have permission to launch this batch data upload process"), "javascript:history.back();");
            }
        }
    }
    /**
     * @return array|null
     */
    public function importScriptList()
    {
        $allScriptsTmp = $this->find('all', [
            'conditions' => [
                'flag_active' => true
            ],
            'fields' => [
                'id',
                'name',
                'version'
            ]
        ]);
        $allScripts = [];
        foreach ($allScriptsTmp as $record) {
            $allScripts[$record['BatchUploadControl']['id']] = $record['BatchUploadControl']['name'] . (strlen($record['BatchUploadControl']['version'])? ' - ' . $record['BatchUploadControl']['version'] : '');
        }
        return $allScripts;
    }

    /**
     * @param $expectedCsvFileHeaders
     * @return bool
     */
    public function setAndTestCsvFile()
    {
        
        $expectedCsvFileHeaders = $this->getExpectedCsvFields();
        
        $validated = true;
        $this->csvFileHandle = fopen($this->csvTempFileName, 'r');
        $length = isset($this->options['csv_line_buffer_length']) ? $this->options['csv_line_buffer_length'] : 5000;
        if (!$this->csvFileHandle) {
            $this->recordErrorAndMessage(__('csv data check'), self::ERROR, "Unable to read csv file",
                "The file '{$this->csvFileName}' won't be parsed.", $this->csvFileName);
            $this->validationErrors[] = "The file '{{$this->csvFileName}}' won't be parsed.";
            $validated = false;
        } else {
//            $this->csvFileHeaders = fgetcsv($this->csvFileHandle, $length, $this->csvSeparator);
            $this->csvFileHeaders = fgets($this->csvFileHandle, $length);
            $this->csvFileHeaders = explode($this->csvSeparator, $this->csvFileHeaders);
            foreach ($this->csvFileHeaders as $key => $value) {
                $value = trim($value);
                // $value = utf8_encode($value);
                if (!empty($value)) {
                    if (preg_match_all('/[^\\p{Common}\\p{Latin}]/u', $value, $matches)) {
                        // The characters will cause a sql error when saved in database.
                        // Character can not be record as a message
                        $wrongChars = array_unique($matches[0]);
                        $this->recordErrorAndMessage(
                            __('csv data check'),
                            self::ERROR,
                            __('invalid characters in headers'),
                            __("See field label [%s] with pattern '**?**' replacing wrong character(s)", str_replace($wrongChars, '**?**', $value)));
                        $validated = false;
                    }
                    $this->csvFileHeaders[$key] = preg_replace('/[ ]+/', ' ', $value);
                }
            }
            if ($validated) {
                $this->csvFileHeaders = array_filter($this->csvFileHeaders, 'strlen');
                $csvFileHeadersKeys = array_keys($this->csvFileHeaders);
                $this->csvLineNumber = 2;
                while (($csvLineDataTmp = fgets($this->csvFileHandle, $length)) !== false) {
                    $csvLineDataTmp = explode($this->csvSeparator, $csvLineDataTmp);
                    if (!isset($csvLineDataTmp[0])){
                        continue;
                    }
                    if (array_diff($csvFileHeadersKeys, array_keys($csvLineDataTmp))) {
                        $this->recordErrorAndMessage(__('csv data check'), self::ERROR, __("the number of cells (values) of the csv line is different than the number expected based on headers line content"), __("see line [%s]", $this->csvLineNumber), $this->csvFileName);
                    } else {
                        if (! empty(array_filter($csvLineDataTmp, 'strlen'))) {
                            foreach ($this->csvFileHeaders as $key => $header) {
                                $value = trim($csvLineDataTmp[$key]);
                                // $value = removeSpecialChars(removeAccents($value));
                                // $value = utf8_encode($value);
                                if (preg_match_all('/[^\\p{Common}\\p{Latin}]/u', $value, $matches)) {
                                    // The characters will cause a sql error when saved in database.
                                    // Character can not be record as a message
                                    $wrongChars = array_unique($matches[0]);
                                    $this->recordErrorAndMessage(
                                        __('csv data check'), 
                                        self::ERROR, 
                                        __('invalid characters for the value of field "%s"', $header),
                                        __("See value [%s] with pattern '**?**' replacing wrong character(s) at line [%s]", str_replace($wrongChars, '**?**', $value), $this->csvLineNumber));
                                    $validated = false;
                                }
                                $this->csvFileContents[$this->csvLineNumber][$header] = preg_replace('/[ ]+/', ' ', $value);
                            }
                        }
                    }
                    $this->csvLineNumber ++;
                }
                if ($this->csvLineNumber > $this->maxRecordNumber && $this->maxRecordNumber != -1) {
                    $validated = false;
                    $this->recordErrorAndMessage(__('csv data check'), self::ERROR, __("too many records in file"),
                        __("The number of records [%s] is more than [%s]", $this->csvLineNumber, $this->maxRecordNumber),
                        $this->csvFileName);
                    $this->validationErrors[] = __("the number of records [%s] is more than [%s]", $this->csvLineNumber,
                        $this->maxRecordNumber);
                }
    
                $missingFields = array_diff($expectedCsvFileHeaders, $this->csvFileHeaders);
                if ($missingFields) {
                    $this->recordErrorAndMessage(__('csv data check'), self::ERROR,
                        "Wrong CSV file headers : Fields are missing in file or wrong csv separator.",
                        "Missing fields [" . implode("] + [",
                            $missingFields) . "] in file '{$this->csvFileName}' . File won't be parsed.");
                    $validated = false;
                }
                $additionalFields = array_diff($this->csvFileHeaders, $expectedCsvFileHeaders);
                if ($additionalFields) {
                    // Dont'put $additionalFields in message in case header contains wrong caracters not supported in mysql
                    // See issue https://gitlab.com/ctrnet/atim/-/issues/532
                    $this->recordErrorAndMessage(__('csv data check'), self::ERROR,
                        "Wrong CSV file headers : Additional fields are found in file or wrong csv separator. Required fields [" . implode("] + [",
                            $expectedCsvFileHeaders) . "]",
                        sizeof($additionalFields) . " additional fields detected in file '{$this->csvFileName}'." . (!empty($missingFields)? ' If the missing fields listed above are part of your file, verify that these fields are spelled correctly.' : '') . " File won't be parsed.");
                    $validated = false;
                }
            }
        }
        return $validated;
    }
    
    /**
     * Validate data one by one individually then download the csv file lines into a temporary table.
     *
     * Validation done by validateAndGetCsvValueForInsert() function:
     *   - Type of data (date, integer, varchar, etc).
     *   - Length of data (to match ATiM table fields).
     *   - Data against ATiM list values for any 'select' field.
     * @throws ImportException
     */
    public function loadCsvFileIntoTemporaryTable()
    {
        $importSummaryCategory = __('csv data check');
            
        $sortedCsvFieldsToDatabaseTableFields = $this->getCsvFieldsInfo();
        
        // Build or truncate temporary table to get csv data
        // Query not executed any time because it generates a commit automatically at this level
        $dbFieldsDefinitionsToCreateTmpTable = [];
        foreach ($this->scriptFieldsForTmpTable as $newScriptField) {
            if (!strpos(strtoupper($newScriptField['data_type']), " DEFAULT ")) {
                if (Configure::read('debug') > 0) {
                    AppController::addWarningMsg("The additionalTmpTableFieldsDefinitions['data_type'] should have a 'DEFAULT' value. See field_name = [" . $newScriptField['field_name'] . "] and data_type = [" . $newScriptField['data_type'] . "].");
                }
                AppController::getInstance()->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
            }
            $dbFieldsDefinitionsToCreateTmpTable[$newScriptField['field_name']] = $newScriptField['field_name'] . ' ' . $newScriptField['data_type'];
        }
        $uniqueCsvFields = [];
        foreach ($sortedCsvFieldsToDatabaseTableFields as $csvField => $fieldToFieldDefinition) {
            $requestedKeys = ['tmp_table_field', 'field_type', 'not_blank', 'unique_in_file', 'field_length', 'structure_domain_name_for_validation', 'str_replace', 'cell_str_replace'];
            foreach ($requestedKeys as $key) {
                if (!array_key_exists($key, $fieldToFieldDefinition)) {
                    if (Configure::read('debug') > 0) {
                        AppController::addWarningMsg("Key [$key] is missing in sortedCsvFieldsToDatabaseTableFields for CSV field [$csvField].");
                    }
                    AppController::getInstance()->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
                }
            }
            $dbField = $fieldToFieldDefinition['tmp_table_field'];
            $dbFieldType = $fieldToFieldDefinition['field_type'];
            $uniqueCsvField = $fieldToFieldDefinition['unique_in_file'];
            $dbFieldLength = $fieldToFieldDefinition['field_length'];
            if (!strlen($dbField)) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "The temporary table field name is empty! See csv field [$csvField]. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
            $fieldsWithLength = ['string', 'varchar', 'select', 'CodingIcd10Ca', 'CodingIcd10Who', 'CodingIcdo3Morpho', 'CodingIcdo3Topo', 'CodingMCode', 'integer', 'float'];
            if (in_array($dbFieldType, $fieldsWithLength) && !preg_match('/^[1-9]([0-9]){0,3}$/', $dbFieldLength)) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "The temporary table field length format is wrong or missing! See csv field [$csvField]. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
            switch ($dbFieldType) {
                case 'yes_no':
                case 'yes_no_unknown':
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`$dbField` char(1) NOT NULL DEFAULT ''";
                    break;
                case 'checkbox':
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`$dbField` tinyint(1) DEFAULT NULL";
                    break;
                case 'varchar':
                case 'string':
                case 'select':
                case 'CodingIcd10Ca':
                case 'CodingIcd10Who':
                case 'CodingIcdo3Morpho':
                case 'CodingIcdo3Topo':
                case 'CodingMCode':
                case 'CodingOtherCode':
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`$dbField` varchar($dbFieldLength) NOT NULL DEFAULT ''";
                    break;
                case 'integer':
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`$dbField` int($dbFieldLength) DEFAULT NULL";
                    break;
                case 'float':
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`$dbField` decimal(" . ($dbFieldLength + 5) . ", 5) DEFAULT NULL";
                    break;
                case 'date':
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`$dbField` date DEFAULT NULL";
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`" . $dbField . '_accuracy' . "` char(1) NOT NULL DEFAULT ''";
                    break;
                case 'datetime':
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`$dbField` datetime DEFAULT NULL";
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`" . $dbField . '_accuracy' . "` char(1) NOT NULL DEFAULT ''";
                    break;
                case 'time':
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`$dbField` time DEFAULT NULL";
                    break;
                case 'text':
                    $dbFieldsDefinitionsToCreateTmpTable[] = "`$dbField` text";
                    break;
                default:
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "The temporary table field type '$dbFieldType' is not supported! See csv field [$csvField]. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
            if ($uniqueCsvField) {
                $uniqueCsvFields[$csvField] = $dbField;
            }
        }
        $dbFieldsDefinitionsToCreateTmpTable[] = "`csv_file_name` varchar(500) NOT NULL DEFAULT ''";
        $dbFieldsDefinitionsToCreateTmpTable[] = "`csv_line` int(11) DEFAULT NULL";
        
        $this->createTempTable($dbFieldsDefinitionsToCreateTmpTable);
        
        $this->datasource->commit();
        
        // Read file and import data into the temporary table
        //---------------------------------------------------
        
        $insertStatementFields = null;
        $csvLineCounter = 0;
        $csvDataLineCounter = 0;
        
        foreach ($this->csvFileContents as $csvLineCounter => $csvLineData) {
            $csvLineData = $this->customImportClass->processLineDataBeforeTrunkValidation($csvLineData, $csvLineCounter);
            $sortedCsvLineDataToLoad = ['csv_file_name' => $this->csvFileName, 'csv_line' => $csvLineCounter];
            foreach ($sortedCsvFieldsToDatabaseTableFields as $csvField => $fieldToFieldDefinition) {
                $validatedValue = $this->validateAndGetCsvValueForInsert($csvField, $fieldToFieldDefinition,
                    $csvLineData[$csvField], $importSummaryCategory, $csvLineCounter);
                $sortedCsvLineDataToLoad = array_merge($sortedCsvLineDataToLoad, $validatedValue);
            }
            $sortedCsvLineDataToLoad = $this->customImportClass->processFormatedLineDataBeforeInsertInTemporaryTable($csvLineData, $sortedCsvLineDataToLoad, $csvLineCounter);
            $sortedCsvLineDataToLoad = array_filter($sortedCsvLineDataToLoad, function($value){return strlen($value) || $value === false || $value === true;});
            $sortedCsvLineDataToLoad = array_map(function ($v) {return ($v === true ? '1' : ($v === false ? '0' : str_replace(array_keys($this->specialCharReplaceForSql), $this->specialCharReplaceForSql, $v)));}, $sortedCsvLineDataToLoad);
            if (!empty($sortedCsvLineDataToLoad)) {
                $insertQuery = "INSERT INTO {$this->temporaryTableName} (`" .
                    implode("`, `", array_keys($sortedCsvLineDataToLoad)) .
                    "`) VALUES ('" .
                    implode("', '", $sortedCsvLineDataToLoad) . "')";
                $this->customQuery($insertQuery, true, false, ['BatchUploadControl_CSV_Line' => $csvDataLineCounter + 2]);
                $csvDataLineCounter++;
            }
        }
        
        // Validate a value is not duplicated in file
        if ($uniqueCsvFields) {
            foreach ($uniqueCsvFields as $csvField => $dbField) {
                $query = "SELECT COUNT($dbField) AS nbr_doublon, $dbField AS notUniqueValue
                    FROM {$this->temporaryTableName}
                    WHERE $dbField IS NOT NULL AND $dbField NOT LIKE ''
                    GROUP BY $dbField
                    HAVING COUNT($dbField) > 1";
                foreach ($this->getSelectQueryResult($query) as $newResult) {
                    extract($newResult);
                    $this->recordErrorAndMessage($importSummaryCategory, BatchUploadControl::ERROR, "CSV Field '$csvField' values are duplicated in file.",
                        "See value [$notUniqueValue].");
                }
            }
        }
        
        // Complete the migration summary
        //-------------------------------
        
        if ($csvDataLineCounter) {
            $this->recordErrorAndMessage(__('batch data upload summary'), BatchUploadControl::MESSAGE, "Information",
                "CSV lines counter - All lines : $csvLineCounter");
            $this->recordErrorAndMessage(__('batch data upload summary'), BatchUploadControl::MESSAGE, "Information",
                "CSV lines counter - all lines with data to process: $csvDataLineCounter");
        } else {
            $this->recordErrorAndMessage(__('batch data upload summary'), BatchUploadControl::ERROR, "Either file is empty or no line has to be defined as to be imported.",
                "Please check migration summray plus file content.");
        }
        
    }
    
    /**
     * Validate csv cell value based on field definition returned by getCsvFieldsInfo() function.
     *
     * Validation done on:
     *   - Type of data (date, integer, varchar, etc.).
     *   - Length of data (to match ATiM table fields).
     *   - Data against ATiM list values for any 'select' field.
     * @param $csvField
     * @param $fieldToFieldDefinition
     * @param $csvFieldValue
     * @param $importSummaryCategory
     * @param $csvLineNumber
     * @return array|mixed
     * @throws ImportException
     */
    public function validateAndGetCsvValueForInsert($csvField, $fieldToFieldDefinition, $csvFieldValue, $importSummaryCategory, $csvLineNumber)
    {
        $formattedCsvValue = '';
        $formattedCsvValueAccuracy = '';
        
        if ($fieldToFieldDefinition['str_replace']) {
            $csvFieldValue = str_replace($fieldToFieldDefinition['str_replace'][0], $fieldToFieldDefinition['str_replace'][1], $csvFieldValue);
        }
        if ($fieldToFieldDefinition['cell_str_replace']) {
            foreach ($fieldToFieldDefinition['cell_str_replace'][0] as $key => $value) {
                if ($csvFieldValue == $value) {
                    $csvFieldValue = $fieldToFieldDefinition['cell_str_replace'][1][$key];
                }
            }
        }
        // Validate
        switch ($fieldToFieldDefinition['field_type']) {
            case 'yes_no':
                $formattedCsvValue = strtolower($csvFieldValue);
                if (in_array($formattedCsvValue, ['oui', 'yes', '1'])) {
                    $formattedCsvValue = 'y';
                } elseif (in_array($formattedCsvValue, ['non', 'no', '0'])) {
                    $formattedCsvValue = 'n';
                }
                if (!in_array($formattedCsvValue, ['y', 'n', ''])) {
                    $this->recordErrorAndMessage($importSummaryCategory, BatchUploadControl::ERROR,
                        "CSV field '$csvField' value is not a boolean 'yes/no' value. The value won't be migrated.",
                        "See value [$csvFieldValue] at line $csvLineNumber!");
                    $formattedCsvValue = '';
                }
                break;
            case 'yes_no_unknown':
                $formattedCsvValue = strtolower($csvFieldValue);
                if (in_array($formattedCsvValue, ['oui', 'yes', '1'])) {
                    $formattedCsvValue = 'y';
                } elseif (in_array($formattedCsvValue, ['non', 'no', '0'])) {
                    $formattedCsvValue = 'n';
                } elseif (in_array($formattedCsvValue, ['unknown', 'inconnu', 'inconnue', 'unk.', 'inc.', 'unk', 'inc'])) {
                    $formattedCsvValue = 'u';
                }
                if (!in_array($formattedCsvValue, ['y', 'n', 'u', ''])) {
                    $this->recordErrorAndMessage($importSummaryCategory, BatchUploadControl::ERROR,
                        "CSV field '$csvField' value is not a 'yes/no/unknown' value. The value won't be migrated.",
                        "See value [$csvFieldValue] at line $csvLineNumber!");
                    $formattedCsvValue = '';
                }
                break;
            case 'checkbox':
                $formattedCsvValue = strtolower(($formattedCsvValue === true)? '1' : (($formattedCsvValue === false)? '0' : $formattedCsvValue));
                if (in_array($formattedCsvValue, ['oui', 'yes'])) {
                    $formattedCsvValue = '1';
                } elseif (in_array($formattedCsvValue, ['non', 'no'])) {
                    $formattedCsvValue = '0';
                }
                if (!in_array($formattedCsvValue, ['1', '0', ''])) {
                    $this->recordErrorAndMessage($importSummaryCategory, BatchUploadControl::ERROR,
                        "CSV field '$csvField' value is not a 'checkbox' value. The value won't be migrated.",
                        "See value [$csvFieldValue] at line $csvLineNumber!");
                    $formattedCsvValue = '';
                }
                break;
            case 'text':
                $formattedCsvValue = $csvFieldValue;
                break;
            case 'varchar':
            case 'string':
                $formattedCsvValueForCheck = str_replace(array_keys($this->specialCharReplaceForSql), $this->specialCharReplaceForSql, $csvFieldValue);
                // Final str_replace for insert done in loadCsvFileIntoTemporaryTable()
                $formattedCsvValue = $csvFieldValue;
                if (strlen($formattedCsvValueForCheck) > $fieldToFieldDefinition['field_length']) {
                    $this->recordErrorAndMessage($importSummaryCategory, BatchUploadControl::ERROR,
                        "CSV field '$csvField' value is too long (>" . $fieldToFieldDefinition['field_length'] . "). Value won't bey migrated.",
                        "See value [$csvFieldValue] " . ($csvFieldValue != $formattedCsvValueForCheck? "(will be migrated as [". $formattedCsvValueForCheck . "])" : "") ." at line $csvLineNumber.");
                    $formattedCsvValue = '';
                }
                break;
            case 'integer':
                $formattedCsvValue = $this->validateAndGetInteger($csvFieldValue, $importSummaryCategory,
                    "Field $csvField", "See line $csvLineNumber.");
                if (strlen($csvFieldValue) > $fieldToFieldDefinition['field_length']) {
                    $this->recordErrorAndMessage($importSummaryCategory, BatchUploadControl::ERROR,
                        "CSV field '$csvField' value is too long (>" . $fieldToFieldDefinition['field_length'] . "). Value won't bey migrated.",
                        "See value [$csvFieldValue] at line $csvLineNumber.");
                }
                break;
            case 'float':
                $formattedCsvValue = $this->validateAndGetDecimal($csvFieldValue, $importSummaryCategory, "Field $csvField", "See line $csvLineNumber.");
                if (strlen($csvFieldValue) > $fieldToFieldDefinition['field_length']) {
                    $this->recordErrorAndMessage($importSummaryCategory, BatchUploadControl::ERROR,
                        "CSV field '$csvField' value is too long (>" . $fieldToFieldDefinition['field_length'] . "). Value won't bey migrated.",
                        "See value [$csvFieldValue] at line $csvLineNumber.");
                }
                break;
            case 'date':
                list($formattedCsvValue, $formattedCsvValueAccuracy) = $this->validateAndGetDateAndAccuracy($csvFieldValue,  $importSummaryCategory, "Field $csvField", "See line $csvLineNumber.");
                break;
            case 'datetime':
                list($formattedCsvValue, $formattedCsvValueAccuracy) = $this->validateAndGetDateTimeAndAccuracy($csvFieldValue, $importSummaryCategory, "Field $csvField", "See line $csvLineNumber.");
                break;
            case 'time':
                $formattedCsvValue = $this->validateAndGetTime($csvFieldValue, $importSummaryCategory, "Field $csvField", "See line $csvLineNumber.");
                break;
            case 'CodingIcd10Ca':
            case 'CodingIcd10Who':
            case 'CodingIcdo3Morpho':
            case 'CodingIcdo3Topo':
            case 'CodingMCode':
                if (! isset($this->codingModels[$fieldToFieldDefinition['field_type']])) {
                    $this->codingModels[$fieldToFieldDefinition['field_type']] = AppModel::getInstance("CodingIcd", $fieldToFieldDefinition['field_type'], true);
                }
                $formattedCsvValue = $csvFieldValue;
                if (! $this->codingModels[$fieldToFieldDefinition['field_type']]->validateId($formattedCsvValue)) {
                    $this->recordErrorAndMessage($importSummaryCategory, BatchUploadControl::ERROR,
                        "CSV field '$csvField' value is part of the " . str_replace('Coding', '', $fieldToFieldDefinition['field_type']) . " codes list. Value won't bey migrated.",
                        "See value [$csvFieldValue] at line $csvLineNumber.");
                    $formattedCsvValue = '';
                }
                break;
            case 'select':
                // Length check not necessary because we test value vs list
                $formattedCsvValue = $this->validateAndGetStructureDomainValue($csvFieldValue,
                    $fieldToFieldDefinition['structure_domain_name_for_validation'], $importSummaryCategory,
                    "Field $csvField", "See line $csvLineNumber.");
                break;
            default:
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "The temporary table field type '" . $fieldToFieldDefinition['field_type'] . "' is not supported! See csv field [$csvField]. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
        }
        // Check not blank
        if ($fieldToFieldDefinition['not_blank']) {
            if (!strlen($csvFieldValue)) {
                $this->recordErrorAndMessage($importSummaryCategory,
                    BatchUploadControl::ERROR,
                    "CSV field '$csvField' value can not be empty.",
                    "See line $csvLineNumber!");
            } elseif (!strlen($formattedCsvValue)) {
                $this->recordErrorAndMessage($importSummaryCategory,
                    BatchUploadControl::ERROR,
                    "CSV field '$csvField' value was not empty but script erased the data (see previous errors). But value can not be empty.",
                    "See value ['$csvFieldValue'] on line $csvLineNumber!");
            }
        }
        // Return value
        if (in_array($fieldToFieldDefinition['field_type'], ['date', 'datetime'])) {
            return [
                $fieldToFieldDefinition['tmp_table_field'] => $formattedCsvValue,
                $fieldToFieldDefinition['tmp_table_field'] . '_accuracy' => $formattedCsvValueAccuracy];
        } else {
            return [$fieldToFieldDefinition['tmp_table_field'] => $formattedCsvValue];
        }
    }
    
    /**
     * Validate if the migration process can keep going based on recorded migration message. Any 'Error' will return false.
     * 
     * @return boolean False when BatchUploadControl::ERROR exists in importSummary message else True.
     */
    public function continueDataImport()
    {
        foreach ($this->importSummary as $summarySectionTitle => $data1) {
            $messageTypes = array_keys($data1);
            if (in_array(BatchUploadControl::ERROR, $messageTypes)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Set in system all expected CSV field names plus linked information to :
     *  - create field of the import temporary table.
     *  - validate the imported values based on different parameters (type, length, list of values, etc).
     *  - import csv data into the temporary table.
     *  
     *  The data structure passed as parameter should be as follows:
     *  
     *  ['csv field name' => [
     *       'tmp_table_field' => 'temporary_table_field_name',     
     *             // Field of the temporary table that will be created to keep the csv value and let custom script to manipulate it
     *       'field_type' => 'string',       
     *             // Type of the imported value ('yes_no', 'yes_no_unknown', 'checkbox', 'varchar', 'string', 'integer', 'float', 'date', 'datetime', 
     *             //   'time', 'select', 'CodingIcd10Ca', 'CodingIcd10Who', 'CodingIcdo3Morpho', 'CodingIcdo3Topo', 'CodingMCode', 'CodingOtherCode') to validate it.
     *       'not_blank' => true,       
     *             // Imported value can be empty or not to validate it.
     *       'field_length' => '250',
     *             // Imported value size to validate imported value.
     *       'structure_domain_name_for_validation' => '',
     *             // When field type is 'select', name of the linked StructureDomain to validate imported value.
     *       'str_replace' => ["é" => '2', '.' => ',']
     *             // Replace all csv cell content occurrences of the search string with the replacement string.
     *       'cell_str_replace' => ['yes' => 'y', 'no' => 'n', 'non' => 'n']
     *             // Replace all csv cell contents matching exactly the cell string with the replacement string
     *     ], ...
     *  ]
     *  
     * @param $csvFieldInfos (see array description above)
     *  
     * @throws ImportException
     */
    public function loadCsvFieldsInfo($csvFieldInfos) {
        $this->csvFieldsInfos = $csvFieldInfos;
        // TODO add validation on the array structure?
    }
    
    /**
     * Set in system information to add additional fields to the temporary table. These fields can be used bu custom migration script :
     *  - create field of the import temporary table.
     *  - validate the imported values based on different parameters (type, length, list of values, etc).
     *  - import csv data into the temporary table.
     *  
     *  The data structure passed as parameter should be as follows:
     *  
     *  [
     *     'temporary_table_field_name', 
     *     "field creation statement (script_collection_unique_key varchar(500) NOT NULL DEFAULT '')", 
     *     "default value ("''", "null", etc)], ...
     *  ]
     * 
     * @param $scriptFieldInfos (see array description above)
     * 
     * @throws ImportException
     */
    public function loadScriptFieldsForTmpTable($scriptFieldInfos) {
        $this->scriptFieldsForTmpTable = $scriptFieldInfos;
    }
    
    /**
     * @return array
     * @throws ImportException
     */
    public function getExpectedCsvFields()
    {
        return array_keys($this->getCsvFieldsInfo());
    }
    
    /**
     * Return the name of the field of the temporary table keeping the value of the csv file column passed
     * as parameter.
     * 
     * @param String $csvFieldName Name of the csv file column.
     * 
     * @return String name of the field in temporary table keeping information of the csv field name passed in argument.
     * 
     * @throws ImportException
     */
    public function getTmpTableFieldNameFromCsvHeader($csvFieldName)
    {
        $tmp = $this->getCsvFieldsInfo($csvFieldName);
        return $tmp['tmp_table_field'];
    }
    
    /**
     * @param null $csvFieldName
     * @return array|mixed
     * @throws ImportException
     */
    public function getCsvFieldsInfo($csvFieldName = null)
    {
        if ($csvFieldName) {
            if (!array_key_exists($csvFieldName, $this->csvFieldsInfos)) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "Field name '$csvFieldName' is not part of the CSV file expected. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
            return $this->csvFieldsInfos[$csvFieldName];
        }
        return $this->csvFieldsInfos;
        
    }

    /**
     * @param $errorMessages
     * @throws ImportException
     */
    public function migrationDie($errorMessages)
    {
        if (!is_array($errorMessages)) {
            $errorMessages = [$errorMessages];
        }
        foreach ($errorMessages as $msg) {
            if (empty($this->messages['migrationDie'][self::ERROR])) {
                $this->messages['migrationDie'][self::ERROR] = [__($msg)];
            } else {
                $this->messages['migrationDie'][self::ERROR][] = __($msg);
            }
        }

        if (Configure::read('debug') > 0) {
            $counter = 0;
            foreach (debug_backtrace() as $debugData) {
                $counter++;
                $debugData['file'] = empty($debugData['file']) ? "-" : $debugData['file'];
                $debugData['line'] = empty($debugData['line']) ? "-" : $debugData['line'];
                if (!isset($this->messages['migrationDie']['trace'])) {
                    $this->messages['migrationDie']['trace'] = ["$counter- Function {$debugData['function']}() [File: {$debugData['file']} - Line: {$debugData['line']}]"];
                } else {
                    $this->messages['migrationDie']['trace'][] = "$counter- Function {$debugData['function']}() [File: {$debugData['file']} - Line: {$debugData['line']}]";
                }
            }
        }
        
        $this->rollbackImport();

        throw new ImportException(json_encode($this->messages));
    }

    public function csvFileClose()
    {
        if ($this->csvFileHandle) {
            $this->csvFileContents = [];
            fclose($this->csvFileHandle);
        }
    }
    
    public function loadAtimControls()
    {
        if (empty($this->atimControls)) {
            //*** Control : sample_controls ***
            $controlsName = 'sample_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => []];
            $queryResult = $this->getSelectQueryResult("SELECT parent_sample_control_id, derivative_sample_control_id FROM parent_to_derivative_sample_controls WHERE flag_active = 1");
            $relations = [];
            foreach ($queryResult as $unit) {
                $key = $unit["parent_sample_control_id"];
                $value = $unit["derivative_sample_control_id"];
                if (!isset($relations[$key])) {
                    $relations[$key] = [];
                }
                $relations[$key][] = $value;
            }
            $activeSampleControlIds = $this->getActiveSampleControlIds($relations, "");
            $activeSampleControlIds = array_filter($activeSampleControlIds, 'strlen');
            $query = "SELECT id, sample_type, sample_category, detail_tablename FROM sample_controls WHERE id IN (" . implode(',', $activeSampleControlIds) . ")";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = $newControl['sample_type'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
            }
            
            //*** Control : aliquot_controls ***
            $controlsName = 'aliquot_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => []];
            $query = "SELECT aliquot_controls.id, sample_type, aliquot_type, aliquot_controls.detail_tablename, volume_unit
                FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = aliquot_controls.sample_control_id
                WHERE aliquot_controls.flag_active = '1' AND aliquot_controls.sample_control_id IN (" . implode(',', $activeSampleControlIds) . ")";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = $newControl['sample_type'] . '-' . $newControl['aliquot_type'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
            }           
            
            //*** Control : consent_controls ***
            $controlsName = 'consent_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => []];
            $query = "SELECT id, controls_type, detail_tablename FROM consent_controls WHERE flag_active = 1";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = $newControl['controls_type'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
            }
            
            //*** Control : event_controls ***
            $controlsName = 'event_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => []];
            $query = "SELECT id, disease_site, event_type, detail_tablename FROM event_controls WHERE flag_active = 1";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = (strlen($newControl['disease_site']) ? $newControl['disease_site'] . '-' : '') . $newControl['event_type'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
            }
            
            //*** Control : diagnosis_controls ***
            $controlsName = 'diagnosis_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => []];
            $this->atimControls[$controlsName] = ['***primary_control_ids***' => []];
            $query = "SELECT id, category, controls_type, detail_tablename FROM diagnosis_controls WHERE flag_active = 1";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = $newControl['category'] . '-' . $newControl['controls_type'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                if ($newControl['category'] == 'primary') {
                    $this->atimControls['diagnosis_controls']['***primary_control_ids***'][] = $newControl['id'];
                }
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
            }
            
            //*** Control : misc_identifier_controls ***
            $controlsName = 'misc_identifier_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => []];
            $query = "SELECT id, misc_identifier_name, flag_active, autoincrement_name, misc_identifier_format, flag_once_per_participant, flag_unique FROM misc_identifier_controls WHERE flag_active = 1";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = $newControl['misc_identifier_name'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
            }
            
            //*** Control : sop_controls ***
            $controlsName = 'sop_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => []];
            //TODO: extend not use anymore
            //$query = "SELECT id, sop_group, type, detail_tablename, extend_tablename, extend_form_alias FROM sop_controls WHERE flag_active = 1";
            $query = "SELECT id, sop_group, type, detail_tablename FROM sop_controls WHERE flag_active = 1";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = $newControl['sop_group'] . '-' . $newControl['type'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
            }
            
            //*** Control : storage_controls ***
            $controlsName = 'storage_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => []];
            $query = "SELECT id, storage_type, coord_x_title, coord_x_type, coord_x_size, coord_y_title, coord_y_type, coord_y_size, display_x_size, display_y_size , set_temperature, is_tma_block, detail_tablename, storage_type_en, storage_type_fr FROM storage_controls WHERE flag_active = 1";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = $newControl['storage_type'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
            }
            
            //*** Control : protocol_controls ***
            $controlsName = 'protocol_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => [], '***extend_id_to_detail_tablename***' => []];
            $query = "SELECT protocol_controls.id, tumour_group, type, protocol_controls.detail_tablename, protocol_extend_controls.id AS protocol_extend_control_id, protocol_extend_controls.detail_tablename AS protocol_extend_detail_tablename
                FROM protocol_controls LEFT JOIN protocol_extend_controls ON protocol_extend_controls.id = protocol_controls.protocol_extend_control_id AND protocol_extend_controls.flag_active = 1
                WHERE protocol_controls.flag_active = 1";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = $newControl['tumour_group'] . '-' . $newControl['type'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
                if ($newControl['protocol_extend_control_id']) {
                    $this->atimControls[$controlsName]['***extend_id_to_detail_tablename***'][$newControl['protocol_extend_control_id']] =  $newControl['protocol_extend_detail_tablename'];
                }
            }
            
            //*** Control : treatment_controls ***
            $controlsName = 'treatment_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => [], '***extend_id_to_detail_tablename***' => []];
            $query = "SELECT treatment_controls.id, disease_site, tx_method, treatment_controls.detail_tablename, applied_protocol_control_id, treatment_extend_controls.id AS treatment_extend_control_id, treatment_extend_controls.detail_tablename AS treatment_extend_detail_tablename
                FROM treatment_controls LEFT JOIN treatment_extend_controls ON treatment_controls.treatment_extend_control_id = treatment_extend_controls.id AND treatment_extend_controls.flag_active = 1
                WHERE treatment_controls.flag_active = 1";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = (strlen($newControl['disease_site']) ? $newControl['disease_site'] . '-' : '') . $newControl['tx_method'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
                if ($newControl['treatment_extend_control_id']) {
                    $this->atimControls[$controlsName]['***extend_id_to_detail_tablename***'][$newControl['treatment_extend_control_id']] =  $newControl['treatment_extend_detail_tablename'];
                }
            }
            
            //*** Control : inventory_action_controls ***
            $controlsName = 'inventory_action_controls';
            $this->atimControls[$controlsName] = ['***id_to_type***' => []];
            $query = "SELECT id, type, category, apply_on, sample_control_ids, aliquot_control_ids, s_ids, a_ids, detail_form_alias,
                detail_tablename, display_order, flag_active, flag_link_to_study, flag_link_to_storage, flag_test_mode, flag_form_builder, flag_active_input,  flag_batch_action
                FROM inventory_action_controls
                WHERE flag_active = 1";
            foreach ($this->getSelectQueryResult($query) as $newControl) {
                $controlType = $newControl['category'] . '-' . $newControl['type'];
                $this->atimControls[$controlsName][$controlType] = $newControl;
                $this->atimControls[$controlsName]['***id_to_type***'][$newControl['id']] = $controlType;
            }
            
            ksort($this->atimControls);
        }
    }
    
    /**
     * @param $relations
     * @param $currentCheck
     * @return array
     */
    public function getActiveSampleControlIds($relations, $currentCheck)
    {
        $activeIds = ['-1'];    //If no sample
        if (array_key_exists($currentCheck, $relations)) {
            foreach ($relations[$currentCheck] as $sampleId) {
                if ($currentCheck != $sampleId && $sampleId != 'already_parsed') {
                    $activeIds[] = $sampleId;
                    if (isset($relations[$sampleId]) && !in_array('already_parsed', $relations[$sampleId])) {
                        $relations[$sampleId][] = 'already_parsed';
                        $activeIds = array_merge($activeIds, $this->getActiveSampleControlIds($relations, $sampleId));
                    }
                }
            }
        }
        return array_unique($activeIds);
    }
    
    /**
     * Return any control table information (diagnosis_controls, aliquot_controls, etc). 
     * 
     * @param $controlTableName Name of the control table.
     * @param $controlName Type of the control (format of the type is really dependant of the controls table).
     * @return array Control information matching passed parameters
     * @throws ImportException
     */
    public function getAtimControls($controlTableName, $controlName)
    {
        if (empty($this->atimControls)) {
            $this->loadAtimControls();
        }
        if (!isset($this->atimControls[$controlTableName])) {
            $migrationDieDebug = '';
            if (Configure::read('debug') > 0) {
                $migrationDieDebug = "ERR_CONTROLS: Unknown data control '$controlTableName'.  ";
            }
            $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
        }
        if (!isset($this->atimControls[$controlTableName][$controlName])) {
            $migrationDieDebug = '';
            if (Configure::read('debug') > 0) {
                $migrationDieDebug = "ERR_CONTROLS: Unknown control name '$controlName' in data control '$controlTableName'.  ";
            }
            $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
        }
        return $this->atimControls[$controlTableName][$controlName];
    }

    /**
     * Record any migration errors, warnings or messages.
     *
     * @param string $summarySectionTitle Title of a section gathering errors or messages linked to the same task, data type, etc ('Participant creation' as an example).
     * @param string $summaryType Type of message (self::ERROR, self::WARNING or self::MESSAGE).
     * @param string $summaryTitle Message title to gather all identical errors. 
     * @param string $summaryDetails Details of the error that could include excel line number, specific imported values, etc.
     * @param string $messageDetailKey Key to not duplicate summary details for a same generated error.
     */
    public function recordErrorAndMessage($summarySectionTitle, $summaryType, $summaryTitle, $summaryDetails, $messageDetailKey = null)
    {
        if (!isset($this->importSummaryTypeCounter[$summaryType])) {
            $this->importSummaryTypeCounter[$summaryType] = 0;
        }
        $this->importSummaryTypeCounter[$summaryType]++;
        if (is_null($messageDetailKey)) {
            $this->importSummary[$summarySectionTitle][$summaryType][$summaryTitle][] = $summaryDetails;
        } else {
            $this->importSummary[$summarySectionTitle][$summaryType][$summaryTitle][$messageDetailKey] = $summaryDetails;
        }
    }
    
    public function beginImport()
    {
        $this->datasource->begin();
    }
    
    public function commitImport()
    {
        $this->datasource->commit();
    }
    
    public function rollbackImport()
    {
        $this->datasource->rollback();
    }
    
    //</editor-fold>

    //==================================================================================================================================================================================
    // SQL QUERY FUNCTIONS
    //==================================================================================================================================================================================

    /**
     * Execute an sql statement.
     *
     * @param string $query SQL statement
     * @param boolean $insert True for any $query being an INSERT statement
     * @param bool $select
     * @return multitype Id of the insert when $insert set to TRUE else the mysqli_result object
     * @throws ImportException
     */
    public function customQuery($query, $insert = false, $select = false, $options = [])
    {
        try {
            $queryRes = $this->query($query);
        } catch (Exception $e) {
            if ($e->getCode() == 'HY000' && !empty($options['BatchUploadControl_CSV_Line'])) {
                $errorMessage = $e->getMessage();
                $line = $options['BatchUploadControl_CSV_Line'];
                $start = strpos($errorMessage, "for column '") + strlen("for column '");
                $end = strrpos($errorMessage, "' at row") - $start;
                $fieldName = substr($errorMessage, $start, $end);

                $this->recordErrorAndMessage(
                    __('csv data check'),
                    self::ERROR,
                    __('invalid characters for the value of field "%s"', $fieldName),
                    __("there are some wrong characters at line [%s]", $line)
                );   
            } else {
                if (Configure::read('debug') > 0) {
                    pr('QUERY ERROR:');
                    pr($query);
                    pr(AppController::getStackTrace());
                }
                $this->migrationDie("ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
        }
        if ($insert) {
            $res = $this->query("SELECT LAST_INSERT_ID() as id");
            if (!empty($res[0][0]['id'])) {
                return $res[0][0]['id'];
            } else {
                if (Configure::read('debug') > 0) {
                    pr('QUERY ERROR:');
                    pr($query);
                    pr(AppController::getStackTrace());
                    $this->migrationDie("There is no ID for the insert query. " . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                } else {
                    $this->migrationDie("ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
            }
        } elseif ($select) {
            $queryRes = array_map(function ($item) {
                $response = [];
                if (countCustom(array_keys($item)) > 1) {
                    foreach ($item as $value) {
                        $response = array_merge($response, $value);
                    }
                } else {
                    $response = current($item);
                }
                return $response;
            }, $queryRes);
            return $queryRes;
        } else {
            return $queryRes;
        }
    }

    /**
     * Execute an sql SELECT statement and return results into an array.
     *
     * @param string $query SQL statement
     *
     * @return multitype Query results in an array
     * @throws ImportException
     */
    public function getSelectQueryResult($query)
    {
        if (!preg_match('/^[\s]*(SELECT|DESCRIBE)/i', $query)) {
            if (Configure::read('debug') > 0) {
                $this->migrationDie(["ERR_QUERY", "'SELECT' query expected", $query]);
            } else {
                $this->migrationDie("ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
        }
        $queryResult = $this->customQuery($query, false, true);

        return $queryResult;
    }

    /**
     * Record new data into ATiM table.
     *
     * @param array $tablesData Data to insert. See format above.
     * 
     * Submitted data format:
     *
     * 1- Simple Table
     * 
     * [
     *     'table_name' => [
     *          field1 => value, 
     *          field2 => value, 
     *          etc
     *     ]
     * ]
     * 
     * ex: ['participants' => [('first_name' => 'James', 'last_name' => 'Bond']]
     *
     * 2- Master/Detail Tables
     *
     * [
     *     'master_table_name' =>  [
     *          master_control_id => value,
     *          field1 => value,
     *          field2 => value,
     *          etc
     *     ],
     *     'detail_table_name' => [
     *          field1 => value, 
     *          field2 => value, 
     *          etc
     *     ]
     * ]
     *
     * ex: ['aliquot_masters' => [aliquot_control_id => 12, field1 => value, field2 => value, etc], ['ad_tubes' => [field1 => value, field2 => value, etc]]]
     *
     * 3- Sample Master/Detail Tables
     *
     * [
     *     'master_table_name' =>  [
     *          master_control_id => value,
     *          field1 => value,
     *          field2 => value,
     *          etc
     *     ],     
     *     'specimen_details/derivative_details' =>  [
     *          field1 => value,
     *          field2 => value,
     *          etc
     *     ],
     *     'detail_table_name' => [
     *          field1 => value, 
     *          field2 => value, 
     *          etc
     *     ]
     * ]
     *
     * @return string Id of the created record
     * @throws ImportException
     */
    public function customInsertRecord($tablesData)
    {
        $recordId = null;
        $controlsData = [];
        $mainTableData = [];
        $detailsTablesData = [];
        
        //**** Format submitted data ****
        
        // Rules:
        //   - Remove null or empty (strlen = 0) value from subarray.
        //   - Replace special characters ('\....) to be sql queries compliant (see $specialCharReplaceForSql)
        //   - Change boolean values true/false to tinyint 1/0
        
        foreach ($tablesData as &$subTable) {
            $subTable = array_filter($subTable, function($value){return strlen($value) || $value === false || $value === true;});
            $subTable = array_map(function ($v) {return ($v === true ? '1' : ($v === false ? '0' : str_replace(array_keys($this->specialCharReplaceForSql), $this->specialCharReplaceForSql, $v)));}, $subTable);
        }

        //**** Validate submitted data and set tables to save ****

        $subArraysCount = sizeof($tablesData);
        $subArraysNamesArr = array_keys($tablesData);
        $subArraysNamesSeparators = '<><>';
        $subArraysNamesStrg = $subArraysNamesSeparators . implode($subArraysNamesSeparators, array_keys($tablesData)) . $subArraysNamesSeparators;
        $subArraysNamesStrgForMsg = implode(' + ', array_keys($tablesData));

        if (!$subArraysCount || $subArraysCount > 3) {
            $migrationDieDebug = '';
            if (Configure::read('debug') > 0) {
                $migrationDieDebug = "Wrong tables and sub-tables passed in arguments: $subArraysNamesStrgForMsg. ";
            }
            $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            
        } elseif ($subArraysCount == 1) {
            
            // Function considers that the submitted data are attached to a 'Simple' model (different than Master/Detail model):
            //     - Check the unique submitted subarray is not a master data subarray.
            //     - Check no control_id is part of the subarray.

            $tableName = $subArraysNamesArr[0];
            if (preg_match('/_masters$/', $tableName)) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "The detail data sub-array has to be part of the submitted data for any Master/Detail model. Insertion into the master table '$tableName' failed. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
            if (preg_match('/_control_id/', implode(array_keys($tablesData[$tableName]))) && ! array_key_exists('misc_identifier_control_id', $tablesData[$tableName])) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = " The detail data sub-array has to be part of the submitted data for any Master/Detail model. Insertion into the master table '$tableName' with *_control_id field failed. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
            $mainTableData = ['name' => $tableName, 'data' => $tablesData[$tableName]];
            
        } else {
            
            // Function considers that the submitted data are attached to a 'Master/Detail' model:
            //     - Check that one of the submitted subarray is a master data subarray with control_id.
            //     - Check on controls data
            //     - Check on detail table name
            //     - Check on Specimen/Derivative Details table
            
            // 1- Check masters table and control_id field is set and exist
            if (!preg_match("/$subArraysNamesSeparators((((?!$subArraysNamesSeparators).)*)_masters)$subArraysNamesSeparators/", $subArraysNamesStrg, $matches)) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "More than one sub-array passed in arguments: The system considers submitted data as a Master/Detail model. " . 
                        "The master data sub-array has to be part of the submitted data. Insertion into the following tables failed: $subArraysNamesStrgForMsg. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
            $masterTableName = $matches[1];
            $masterControlIdFieldName = $matches[2] . '_control_id';
            $controlTableName = $matches[2] . '_controls';
            if (!preg_match("/$masterControlIdFieldName/", implode(' ', array_keys($tablesData[$masterTableName])))) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "More than one sub-array passed in arguments: The system considers submitted data as a Master/Detail model. " . 
                        "The '$masterControlIdFieldName' field is missing in the master data sub-array. Insertion into the following tables failed: $subArraysNamesStrgForMsg. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
            $controlId = $tablesData[$masterTableName][$masterControlIdFieldName];
            $controlType = '';
            $controlsData = '';
            $detailsTableName = '';
            if (! isset($this->atimControls[$controlTableName]) && preg_match('/(.*)_extend_controls$/', $controlTableName, $matches)) {
                $parentControlTableName = $matches[1] . "_controls";
                if (isset($this->atimControls[$parentControlTableName]) && isset($this->atimControls[$parentControlTableName]['***extend_id_to_detail_tablename***'][$controlId])) {
                    $detailsTableName = $this->atimControls[$parentControlTableName]['***extend_id_to_detail_tablename***'][$controlId];
                } else {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "More than one sub-array passed in arguments: The system considers submitted data as a Master/Detail model. " .
                            "The '$masterControlIdFieldName' value [$controlId] does not exist or is not active in ATiM. Insertion into the master table '$masterTableName' failed. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
            } elseif (!isset($this->atimControls[$controlTableName]['***id_to_type***'][$controlId])) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "More than one sub-array passed in arguments: The system considers submitted data as a Master/Detail model. " .
                        "The '$masterControlIdFieldName' value [$controlId] does not exist or is not active in ATiM. Insertion into the master table '$masterTableName' failed. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            } else {
                $controlType = $this->atimControls[$controlTableName]['***id_to_type***'][$controlId];
                $controlsData = $this->atimControls[$controlTableName][$controlType];
                $detailsTableName = $controlsData['detail_tablename'];
            }
            $mainTableData = ['name' => $masterTableName, 'data' => $tablesData[$masterTableName]];
            unset($tablesData[$masterTableName]);
            
            // 2- Check detail table is the right one based on control_id
            if (!isset($tablesData[$detailsTableName])) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "More than one sub-array passed in arguments: The system considers submitted data as a Master/Detail model. " .
                        "Missing '$detailsTableName' table data for control '$controlType' for data passed in arguments. Insertion into the following tables failed: $subArraysNamesStrgForMsg.";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
            $detailsTablesData[] = ['name' => $detailsTableName, 'data' => $tablesData[$detailsTableName]];
            unset($tablesData[$detailsTableName]);
            
            // 3- Check specimen_details or derivative_details sub-array is passed in arguments (for sample only) and is the right one
            if ($masterTableName == 'sample_masters') {
                if (sizeof($tablesData) != 1) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "More than one sub-array passed in arguments: The system considers submitted data as a Master/Detail model. " .
                            "The data sub-arrays for a sample records should be sample_masters sub-array plus sample_details sub-array plus either specimen_details or derivative_details sub-array. Insertion into the following tables failed: $subArraysNamesStrgForMsg. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
                $specOrDerivDetailsTableName = array_keys($tablesData);
                $specOrDerivDetailsTableName = array_shift($specOrDerivDetailsTableName);
                $expectedSpecOrDerivDetailsTableName = $controlsData['sample_category'] . "_details";
                if (! in_array($specOrDerivDetailsTableName, array('specimen_details', 'derivative_details'))) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "More than one sub-array passed in arguments: The system considers submitted data as a Master/Detail model. " .
                            "The data sub-arrays for a sample records should be sample_masters sub-array plus sample_details sub-array plus either specimen or derivative _details sub-array. Insertion into the following tables failed: $subArraysNamesStrgForMsg. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                } elseif ($specOrDerivDetailsTableName != $expectedSpecOrDerivDetailsTableName) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "More than one sub-array passed in arguments: The system considers submitted data as a Master/Detail model. " .
                            "The data sub-arrays for a " . $controlsData['sample_category'] . " sample record should be sample_masters sub-array plus sample_details sub-array plus $expectedSpecOrDerivDetailsTableName sub-array. Insertion into the following tables failed: $subArraysNamesStrgForMsg. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
                $detailsTablesData[] = ['name' => $specOrDerivDetailsTableName, 'data' => $tablesData[$specOrDerivDetailsTableName]];
            } else {
                if (sizeof($tablesData) != 0) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "The data sub-arrays for a non sample record should be masters sub-array plus details sub-array. Insertion into the following tables failed: $subArraysNamesStrgForMsg. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
            }
            unset($tablesData);
        }
        $migrationDieDebug = '';
        if (array_key_exists('id', $mainTableData['data'])) {
            if (Configure::read('debug') > 0) {
                $migrationDieDebug = "Model primary key 'id' can not be part of the data sent to the customInsert() function. Insertion into the following table '" . $mainTableData['name'] . "' failed. ";
            }
            $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
        }
        
        //**** Complete missing (system) data plus last validation based on model ****
        
        $dataToUpdateAfterRecords = [];
        if ($mainTableData['name'] == 'participants') {
            // Manage field(s):
            //    - participants.last_modification
            if (! array_key_exists('last_modification', $mainTableData['data'])) {
                $mainTableData['data']['last_modification'] = $this->importDate;
            }
        } elseif ($mainTableData['name'] == 'diagnosis_masters') {
            // Manage field(s):
            //    - diagnosis_masters.primary_id
            //    - diagnosis_masters.parent_id
            unset($mainTableData['data']['primary_id']);
            if (in_array($controlsData['category'], ['primary', 'nonneoplastic'])) {
                if (isset($mainTableData['data']['parent_id']) && ! empty($mainTableData['data']['parent_id'])) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "Wrong parent id value for a primary or nonneoplastic diagnosis. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
                unset($mainTableData['data']['parent_id']);
            } else {
                if (! isset($mainTableData['data']['parent_id']) || empty($mainTableData['data']['parent_id'])) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "Missing parent id value for a secondary or progression diagnosis. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
            }
            //TODO: Should we check that the parent_id exists? Kind of additional nice to have but will slow down the import.
        } elseif ($mainTableData['name'] == 'sample_masters') {
            // Manage field(s):
            //    - sample_masters.initial_specimen_sample_type
            //    - sample_masters.initial_specimen_sample_id
            //    - sample_masters.parent_sample_type
            //    - sample_masters.parent_id (required for derivative)
            unset($mainTableData['data']['initial_specimen_sample_type']);
            unset($mainTableData['data']['initial_specimen_sample_id']);
            unset($mainTableData['data']['parent_sample_type']);
            if ($controlsData['sample_category'] == 'specimen') {
                if (isset($mainTableData['data']['parent_id']) && ! empty($mainTableData['data']['parent_id'])) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "Wrong parent id value for a specimen. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
                unset($mainTableData['data']['parent_id']);
            } else {
                if (! isset($mainTableData['data']['parent_id']) || empty($mainTableData['data']['parent_id'])) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "Missing parent id value for a derivative. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
            }
            if (! isset($mainTableData['data']['sample_code'])) {
                $dataToUpdateAfterRecords[] = 'sample_code';
            }
            //TODO: Should we check that the parent_id exists? Kind of additional nice to have but will slow down the import.
        } elseif ($mainTableData['name'] == 'inventory_action_masters') {
            // Manage field(s):
            //    - inventory_action_masters.initial_specimen_sample_id
            //    - inventory_action_masters.sample_master_id
            if (isset($mainTableData['data']['aliquot_master_id']) && ! empty($mainTableData['data']['aliquot_master_id'])) {
                $linekdSampleIds = $this->getSelectQueryResult("SELECT sample_master_id, initial_specimen_sample_id 
                    FROM aliquot_masters INNER JOIN sample_masters ON sample_masters.id = aliquot_masters.sample_master_id
                    WHERE aliquot_masters.id = {$mainTableData['data']['aliquot_master_id']}
                    AND aliquot_masters.deleted != 1
                    AND sample_masters.deleted != 1");
                if (! $linekdSampleIds) {
                    // No aliquot error
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "Aliquot Id attached to the InventoryAction does not match an existing aliquot. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                } else {
                    if (isset($mainTableData['data']['sample_master_id']) && ! empty($mainTableData['data']['sample_master_id'])) {
                        if ($mainTableData['data']['sample_master_id'] != $linekdSampleIds[0]['sample_master_id']) {
                            // Error wrong sample_master_id
                            $migrationDieDebug = '';
                            if (Configure::read('debug') > 0) {
                                $migrationDieDebug = "Submitted sample_master_id does not match sample_master_id of the aliquot attached to the InventoryAction. ";
                            }
                            $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                        }
                    } else {
                        $mainTableData['data']['sample_master_id'] = $linekdSampleIds[0]['sample_master_id'];
                    }
                    if (isset($mainTableData['data']['initial_specimen_sample_id']) && ! empty($mainTableData['data']['initial_specimen_sample_id'])) {
                        if ($mainTableData['data']['initial_specimen_sample_id'] != $linekdSampleIds[0]['initial_specimen_sample_id']) {
                            // Error wrong initial_specimen_sample_id
                            $migrationDieDebug = '';
                            if (Configure::read('debug') > 0) {
                                $migrationDieDebug = "Submitted initial_specimen_sample_id does not match initial_specimen_sample_id of the sample attached to the InventoryAction. ";
                            }
                            $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                        }
                    } else {
                        $mainTableData['data']['initial_specimen_sample_id'] = $linekdSampleIds[0]['initial_specimen_sample_id'];
                    }
                }
            } elseif (isset($mainTableData['data']['sample_master_id']) && ! empty($mainTableData['data']['sample_master_id'])) {
                $linekdSampleIds = $this->getSelectQueryResult("SELECT initial_specimen_sample_id
                    FROM sample_masters
                    WHERE id = {$mainTableData['data']['sample_master_id']}
                    AND deleted != 1");
                if (! $linekdSampleIds) {
                    // No aliquot error
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "Sample Id attached to the InventoryAction does not match an existing sample. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                } else {
                    if (isset($mainTableData['data']['initial_specimen_sample_id']) && ! empty($mainTableData['data']['initial_specimen_sample_id'])) {
                        if ($mainTableData['data']['initial_specimen_sample_id'] != $linekdSampleIds[0]['initial_specimen_sample_id']) {
                            // Error wrong initial_specimen_sample_id
                            $migrationDieDebug = '';
                            if (Configure::read('debug') > 0) {
                                $migrationDieDebug = "Submitted initial_specimen_sample_id does not match initial_specimen_sample_id of the sample attached to the InventoryAction. ";
                            }
                            $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                        }
                    } else {
                        $mainTableData['data']['initial_specimen_sample_id'] = $linekdSampleIds[0]['initial_specimen_sample_id'];
                    }
                }
            } else {
                // Error missing information
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "Both aliquot_master_id and sample_master_id are missing for the InventoryAction. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
        } elseif ($mainTableData['name'] == 'aliquot_masters') {
            // Manage field(s):
            //    - sample_masters.in_stock
            //    - sample_masters.storage_master_id
            //    - sample_masters.storage_coord_x
            //    - sample_masters.storage_coord_y
            if (! array_key_exists('storage_master_id', $mainTableData['data']) || empty($mainTableData['data']['storage_master_id'])) {
                unset($mainTableData['data']['storage_coord_x']);
                unset($mainTableData['data']['storage_coord_y']);
            } else {
                // Storage master id completed
                if (! array_key_exists('in_stock', $mainTableData['data']) || $mainTableData['data']['in_stock'] == 'no') {
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "No storage allowed for a non in stock aliquot or in stock value is missing. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
            }
        } elseif ($mainTableData['name'] == 'storage_masters') {
            // Manage field(s):
            //    - storage_masters.parent_id
            //    - storage_masters.lft
            //    - storage_masters.rght
            //    - storage_masters.code
            //    - storage_masters.selection_label
            //    - storage_masters.temperature
            //    - storage_masters.temp_unit
            unset($mainTableData['data']['lft']);
            unset($mainTableData['data']['rght']);
            if (! isset($mainTableData['data']['short_label']) || ! strlen($mainTableData['data']['short_label'])) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "Missing short label for storage. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
            }
            if (! isset($mainTableData['data']['code'])) {
                $dataToUpdateAfterRecords[] = 'code';
            }
            if (! isset($mainTableData['data']['selection_label'])) {
                $dataToUpdateAfterRecords[] = 'selection_label';
            }
            if (! isset($mainTableData['data']['temperature'])) {
                $dataToUpdateAfterRecords[] = 'temperature';
            }
            if (! isset($mainTableData['data']['temp_unit'])) {
                $dataToUpdateAfterRecords[] = 'temp_unit';
            }
            //TODO: Should we check that the parent_id exists? Kind of additional nice to have but will slow down the import.
            //TODO: Should we check that the parent is not a TMA block? Kind of additional nice to have but will slow down the import.
        }
        
        // **** Record main table data ****
        
        $mainTableData['data'] = array_merge($mainTableData['data'], [
            "created" => $this->importDate,
            "created_by" => $this->importedBy,
            "modified" => "$this->importDate",
            "modified_by" => $this->importedBy
        ]);
        $query = "INSERT INTO `" . $mainTableData['name'] . "` (`" . implode("`, `", array_keys($mainTableData['data'])) . "`) VALUES ('" . implode("', '", array_values($mainTableData['data'])) . "')";
        $recordId = $this->customQuery($query, true);
        
        if ($mainTableData['name'] == 'diagnosis_masters') {
            if (in_array($controlsData['category'], ['primary', 'nonneoplastic'])) {
                // Manage field(s):
                //    - diagnosis_masters.primary_id
                //    - diagnosis_masters.parent_id
                if ($controlsData['category'] == 'primary') {
                    $query = "UPDATE diagnosis_masters SET primary_id=id WHERE id = $recordId;";
                } else {
                    $query = "UPDATE diagnosis_masters DiagnosisMasters, diagnosis_masters ParentDiagnosisMasters
                            SET DiagnosisMasters.primary_id = DiagnosisMasters.id
                            WHERE DiagnosisMasters.id = $recordId
                            AND DiagnosisMasters.parent_id = ParentDiagnosisMasters.id;";
                }
                $this->customQuery($query);
            }
        } elseif ($mainTableData['name'] == 'sample_masters') {
            // Update field(s):
            //    - sample_masters.initial_specimen_sample_type
            //    - sample_masters.initial_specimen_sample_id
            //    - sample_masters.parent_sample_type
            //    - sample_masters.sample_code (if required)
            $sampleCodeUpdate = (in_array('sample_code', $dataToUpdateAfterRecords))? ", SampleMaster.sample_code = SampleMaster.id" : "";
            if ($controlsData['sample_category'] == 'specimen') {
                $sampleType = $controlsData['sample_type'];
                $query = "UPDATE sample_masters SampleMaster 
                    SET SampleMaster.initial_specimen_sample_id = SampleMaster.id, 
                    SampleMaster.initial_specimen_sample_type = '$sampleType' 
                    $sampleCodeUpdate 
                    WHERE SampleMaster.id = $recordId;";
            } else {
                $query = "UPDATE sample_masters SampleMaster, sample_masters ParentSampleMaster, sample_controls ParentSampleControl
                    SET SampleMaster.initial_specimen_sample_id = ParentSampleMaster.initial_specimen_sample_id, 
                    SampleMaster.initial_specimen_sample_type = ParentSampleMaster.initial_specimen_sample_type, 
                    SampleMaster.parent_sample_type = ParentSampleControl.sample_type
                    $sampleCodeUpdate 
                    WHERE SampleMaster.id = $recordId
                    AND SampleMaster.parent_id = ParentSampleMaster.id
                    AND ParentSampleMaster.deleted <> 1
                    AND ParentSampleMaster.sample_control_id = ParentSampleControl.id;";                
            }
            $this->customQuery($query);
        } elseif ($mainTableData['name'] == 'storage_masters') {
            // Update field(s):
            //    - storage_masters.lft
            //    - storage_masters.rght
            //    - storage_masters.selection_label
            //    - storage_masters.temperature
            //    - storage_masters.temp_unit
            $codeUpdate = (in_array('code', $dataToUpdateAfterRecords))? ", StorageMaster.code = StorageMaster.id" : "";
            if (isset($mainTableData['data']['parent_id']) && ($mainTableData['data']['parent_id'])) {
                $selectionLabelUpdate = (in_array('selection_label', $dataToUpdateAfterRecords))? ", StorageMaster.selection_label = CONCAT(ParentStorageMaster.selection_label,'-',StorageMaster.short_label)" : "";
                if (in_array('selection_label', $dataToUpdateAfterRecords)) {
                    $lengthQuery = "SELECT LENGTH(CONCAT(ParentStorageMaster.selection_label, '-', StorageMaster.short_label)) AS length_res
                        FROM storage_masters StorageMaster, storage_masters ParentStorageMaster
                        WHERE StorageMaster.id = $recordId
                        AND ParentStorageMaster.id = StorageMaster.parent_id
                        AND ParentStorageMaster.deleted <> 1;";
                    $lengthRes = $this->getSelectQueryResult($lengthQuery);
                    if ($lengthRes && $lengthRes[0]['length_res'] > 60) {
                        $migrationDieDebug = '';
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "Generated Storage Selection Label will be longer than 60. " .
                                "Update of the selection_label of the following stroage_master_id failed: $recordId. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                }
                $temperatureUpdate = (in_array('temperature', $dataToUpdateAfterRecords))? ", StorageMaster.temperature = ParentStorageMaster.temperature" : "";
                $temperatureUniteUpdate = (in_array('temp_unit', $dataToUpdateAfterRecords))? ", StorageMaster.temp_unit = ParentStorageMaster.temp_unit" : "";
                $query = "SELECT StorageMaster.rght AS rght FROM storage_masters AS StorageMaster WHERE StorageMaster.deleted <> 1 AND StorageMaster.id = " . $mainTableData['data']['parent_id'];
                $parentRght = $this->getSelectQueryResult($query);
                if (!($parentRght && $parentRght[0]['rght'])) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "Missing parent rght value in ATiM. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
                $parentRght = (int)($parentRght[0]['rght']);
                $query = "UPDATE storage_masters StorageMaster, storage_masters ParentStorageMaster
                    SET StorageMaster.lft = ($parentRght),
                    StorageMaster.rght = ($parentRght + 1)
                    $codeUpdate
                    $selectionLabelUpdate
                    $temperatureUpdate
                    $temperatureUniteUpdate
                    WHERE StorageMaster.id = $recordId
                    AND ParentStorageMaster.id = StorageMaster.parent_id
                    AND ParentStorageMaster.deleted <> 1;";
                $this->customQuery($query);
                $query = "UPDATE storage_masters AS StorageMaster
                    SET StorageMaster.lft = StorageMaster.lft + 2
                    WHERE StorageMaster.lft >= $parentRght AND NOT (StorageMaster.id = $recordId);";
                $this->customQuery($query);
                $query = "UPDATE storage_masters AS StorageMaster
                    SET StorageMaster.rght = StorageMaster.rght + 2
                    WHERE StorageMaster.rght >= $parentRght AND NOT (StorageMaster.id = $recordId);";
                $this->customQuery($query);
            } else {
                $selectionLabelUpdate = (in_array('selection_label', $dataToUpdateAfterRecords))? ", StorageMaster.selection_label = StorageMaster.short_label" : "";
                $query = "SELECT MAX(StorageMaster.rght) AS rght FROM storage_masters AS StorageMaster WHERE StorageMaster.deleted <> 1 AND StorageMaster.id <> $recordId"; 
                $lastRght = $this->getSelectQueryResult($query);
                $lastRght = (int)(($lastRght && $lastRght[0]['rght'])? $lastRght[0]['rght'] : 0);
                $query = "UPDATE storage_masters StorageMaster
                    SET StorageMaster.lft = ($lastRght + 1),
                    StorageMaster.rght = ($lastRght + 2)
                    $codeUpdate
                    $selectionLabelUpdate
                    WHERE StorageMaster.id = $recordId;";
                $this->customQuery($query);
            }
        }
        
        //**** Details tables record ****
            
        $tmpDetailTablename = null;
        if ($detailsTablesData) {
            $foreignKey = str_replace('_masters', '_master_id', $mainTableData['name']);
            foreach ($detailsTablesData as $detailTable) {
                $detailTable['data'] = array_merge($detailTable['data'], [$foreignKey => $recordId]);
                $query = "INSERT INTO `" . $detailTable['name'] . "` (`" . implode("`, `",
                        array_keys($detailTable['data'])) . "`) VALUES ('" . implode("', '",
                            array_values($detailTable['data'])) . "')";
                $this->customQuery($query, true);
                if (!in_array($detailTable['name'], ['specimen_details', 'derivative_details'])) {
                    $tmpDetailTablename = $detailTable['name'];
                }
            }
        }
            
        // **** Keep updated tables in memory to populate revs tables****
        
        $this->addToModifiedDatabaseTablesList($mainTableData['name'], $tmpDetailTablename);
        
        return $recordId;
    }

    /**
     * Update an ATim table record.
     * 
     * @param string $id Id of the record to update
     * @param array $tablesData Data to update (see format above)
     * Submitted data format:
     *
     * 1- Simple Table
     * 
     * [
     *     'table_name' => [
     *          field1 => value, 
     *          field2 => value, 
     *          etc
     *     ]
     * ]
     * 
     * ex: ['participants' => [('first_name' => 'James', 'last_name' => 'Bond']]
     *
     * 2- Master/Detail Tables, etc (details_table_name sub array is not mandatory for upldate but the control_id should be part of the master data for control)
     *
     * [
     *     'master_table_name' =>  [
     *          field1 => value,
     *          field2 => value,
     *          etc
     *     ],
     *     'detail_table_name' => [
     *          field1 => value, 
     *          field2 => value, 
     *          etc
     *     ]
     * ]
     *
     * ex: ['aliquot_masters' => [field1 => value, field2 => value, etc], ['ad_tubes' => [field1 => value, field2 => value, etc]]]
     *
     * @throws ImportException
     */
    public function updateTableData($id, $tablesData)
    {
        if ($tablesData) {
            
            $noData = true;
            foreach ($tablesData as $key => $subArray) {
                if (! empty($subArray)) $noData = false;
            }
            if ($noData) return;
                        
            $mainTableData = [];
            $masterTableName = null;
            $detailsTablesData = [];
            $detailsTableName = null;
            
            //**** Format submited data ****
            
            // The flush of empty field should not be done in case we want to erase a field value
            
            //**** Validate submitted data and set tables to save ****
            
            $subArraysCount = sizeof($tablesData);
            $subArraysNamesArr = array_keys($tablesData);
            $subArraysNamesSeparators = '<><>';
            $subArraysNamesStrg = $subArraysNamesSeparators . implode($subArraysNamesSeparators, array_keys($tablesData)) . $subArraysNamesSeparators;
            $subArraysNamesStrgForMsg = implode(' + ', array_keys($tablesData));
            
            if (!$subArraysCount || $subArraysCount > 3) {
                $migrationDieDebug = '';
                if (Configure::read('debug') > 0) {
                    $migrationDieDebug = "Wrong tables and sub-tables passed in arguments: $subArraysNamesStrgForMsg. ";
                }
                $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                
            } elseif ($subArraysCount == 1) {
                
                // Function considers that the submitted data are attached:
                //     - To a 'Simple' model (different from Master/Detail model).
                //     - Or a Master/Detail model and just the master table is submitted
                
                $tableName = $subArraysNamesArr[0];
                $mainTableData = ['name' => $tableName, 'data' => $tablesData[$tableName]];
                if (preg_match("/^.*_masters$/", $tableName)) {
                    $masterTableName = $tableName;
                }
                unset($tablesData);
                
            } else {
            
                // Function considers that the submitted data are attached to a 'Master/Detail' model:
                //     - Check that one of the submitted subarray is a master data subarray 
                
                if (!preg_match("/$subArraysNamesSeparators((((?!$subArraysNamesSeparators).)*)_masters)$subArraysNamesSeparators/", $subArraysNamesStrg, $matches)) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "More than one sub-array passed in arguments: The system considers submitted data as a Master/Detail model. " .
                            "The master data sub-array has to be part of the submitted data. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
                $masterTableName = $matches[1];
                $mainTableData = ['name' => $masterTableName, 'data' => $tablesData[$masterTableName]];
                unset($tablesData[$masterTableName]);
                
                foreach ($tablesData as $tableName => $detailsTableData) {
                    $detailsTablesData[] = ['name' => $tableName, 'data' => $detailsTableData];
                }
                unset($tablesData);
            }
            
            if ($masterTableName) {
                
                // Fonction considers that the submitted data are attached to a 'Master/Detail' model:
                //     - Check that a master data subarray contains the control_id.
                $masterControlIdFieldName = str_replace('_masters', '_control_id', $masterTableName);
                $controlTableName = str_replace('_masters', '_controls', $masterTableName);
                if (!preg_match("/$masterControlIdFieldName/", implode(' ', array_keys($mainTableData['data'])))) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "More than one sub-array or a master table passed in arguments: The system considers submitted data as a Master/Detail model. " .
                            "The '$masterControlIdFieldName' field is missing in the master data sub-array. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
                $controlId = $mainTableData['data'][$masterControlIdFieldName];
                
                //     - Check on controls data
                $controlType = '';
                $controlsData = '';
                $detailsTableName = '';
                if (! isset($this->atimControls[$controlTableName]) && preg_match('/(.*)_extend_controls$/', $controlTableName, $matches)) {
                    $parentControlTableName = $matches[1] . "_controls";
                    if (isset($this->atimControls[$parentControlTableName]) && isset($this->atimControls[$parentControlTableName]['***extend_id_to_detail_tablename***'][$controlId])) {
                        $detailsTableName = $this->atimControls[$parentControlTableName]['***extend_id_to_detail_tablename***'][$controlId];
                    } else {
                        $migrationDieDebug = '';
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "More than one sub-array or a master table passed in arguments: The system considers submitted data as a Master/Detail model. " .
                                "The '$masterControlIdFieldName' value [$controlId] does not exist or is not active in ATiM. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                } elseif (!isset($this->atimControls[$controlTableName]['***id_to_type***'][$controlId])) {
                    $migrationDieDebug = '';
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "More than one sub-array or a master table passed in arguments: The system considers submitted data as a Master/Detail model. " .
                            "The '$masterControlIdFieldName' value [$controlId] does not exist or is not active in ATiM. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                } else {
                    $controlType = $this->atimControls[$controlTableName]['***id_to_type***'][$controlId];
                    $controlsData = $this->atimControls[$controlTableName][$controlType];
                    $detailsTableName = $controlsData['detail_tablename'];
                }
                
                //     - Check on detail table name
                //     - Check on Specimen/Derivative Details table
                $allowedOtherTables = [$detailsTableName];
                $errorMsg = '';
                if ($masterTableName == 'sample_masters') {
                    $expectedSpecOrDerivDetailsTableName = $controlsData['sample_category'] . "_details";
                    $allowedOtherTables[] = $expectedSpecOrDerivDetailsTableName;
                    $errorMsg = " and/or '$expectedSpecOrDerivDetailsTableName' sub-array";
                }
                
                foreach ($detailsTablesData as $tmpDetailTableInfo) {
                    if (! in_array($tmpDetailTableInfo['name'], $allowedOtherTables)) {
                        $migrationDieDebug = '';
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "More than one sub-array or a master table passed in arguments: The system considers submitted data as a Master/Detail model. " .
                                "The data sub-arrays for a $controlType record should be '$masterTableName' masters sub-array plus '$detailsTableName' details sub-array$errorMsg. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                }
            }
                    
            //**** Complete missing (system) data plus last validation based on model ****
            
            if ($mainTableData['name'] == 'participants') {
                // Manage field(s):
                //    - participants.participant_identifier
                //    - participants.last_modification
                foreach(['participant_identifier'] as $fieldToTest) {
                    if (array_key_exists($fieldToTest, $mainTableData['data'])) {
                        //TODO: Should be allowed/managed by script?
                        $migrationDieDebug = '';
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "Participant $fieldToTest can not be updated. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                }
                if (! array_key_exists('last_modification', $mainTableData['data'])) {
                    $mainTableData['data']['last_modification'] = $this->importDate;
                }
            } elseif ($mainTableData['name'] == 'diagnosis_masters') {
                // Manage field(s):
                //    - diagnosis_masters.primary_id
                //    - diagnosis_masters.parent_id
                foreach(['primary_id', 'parent_id'] as $fieldToTest) {
                    if (array_key_exists($fieldToTest, $mainTableData['data'])) {
                        //TODO: Should be allowed/managed by script?
                        $migrationDieDebug = '';
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "Diagnosis $fieldToTest can not be updated. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                }
            } elseif ($mainTableData['name'] == 'sample_masters') {
                // Manage field(s):
                //    - sample_masters.initial_specimen_sample_type
                //    - sample_masters.initial_specimen_sample_id
                //    - sample_masters.parent_sample_type
                //    - sample_masters.parent_id (required for derivative)
                foreach(['initial_specimen_sample_type', 'initial_specimen_sample_id', 'parent_id', 'parent_sample_type', 'sample_code'] as $fieldToTest) {
                    if (array_key_exists($fieldToTest, $mainTableData['data'])) {
                        //TODO: Should be allowed/managed by script (at least sample_code)?
                        $migrationDieDebug = '';
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "Sample $fieldToTest can not be updated. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                }
            } elseif ($mainTableData['name'] == 'inventory_action_masters') {
                // Manage field(s):
                //    - inventory_action_masters.initial_specimen_sample_id
                //    - inventory_action_masters.sample_master_id
                foreach(['initial_specimen_sample_id'/*, 'sample_master_id'*/] as $fieldToTest) {
                    if (array_key_exists($fieldToTest, $mainTableData['data'])) {
                        //TODO: Should be allowed/managed by script (at least sample_code)?
                        $migrationDieDebug = '';
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "InvetoryAction $fieldToTest can not be updated. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                }
            } elseif ($mainTableData['name'] == 'aliquot_masters') {
                // Manage field(s):
                //    - sample_masters.in_stock
                //    - sample_masters.storage_master_id
                //    - sample_masters.storage_coord_x
                //    - sample_masters.storage_coord_y
                foreach(['barcode'] as $fieldToTest) {
                    if (array_key_exists($fieldToTest, $mainTableData['data'])) {
                        //TODO: Should be allowed/managed by script?
                        $migrationDieDebug = '';
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "Aliquot $fieldToTest can not be updated. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                }
                if (! array_key_exists('storage_master_id', $mainTableData['data']) || empty($mainTableData['data']['storage_master_id'])) {
                    $mainTableData['data']['storage_master_id'] = null;
                    $mainTableData['data']['storage_coord_x'] = '';
                    $mainTableData['data']['storage_coord_y'] = '';
                } else {
                    // Storage master id completed
                    if (! array_key_exists('in_stock', $mainTableData['data']) || $mainTableData['data']['in_stock'] == 'no') {
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "No storage allowed for a non in stock aliquot or in stock value is missing. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                }
            } elseif ($mainTableData['name'] == 'storage_masters') {
                // Manage field(s):
                //    - storage_masters.parent_id
                //    - storage_masters.lft
                //    - storage_masters.rght
                //    - storage_masters.code
                //    - storage_masters.selection_label
                //    - storage_masters.temperature
                //    - storage_masters.temp_unit
                foreach(['lft', 'rght', 'short_label', 'code', 'selection_label', 'temperature', 'temp_unit'] as $fieldToTest) {
                    if (array_key_exists($fieldToTest, $mainTableData['data'])) {
                        //TODO: Should be allowed/managed by script? Do we allow position update? Impact on other storage (temperature and lft rgth)
                        $migrationDieDebug = '';
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "Aliquot $fieldToTest can not be updated or case not supported yet by script. Update of the following tables failed: $subArraysNamesStrgForMsg. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                }
            }
            
            // **** Record main table data ****
            
            $tableName = $mainTableData['name'];
            $tableData = $mainTableData['data'];
            $setSqlStrings = [];
            foreach (array_merge($tableData, ['modified' => $this->importDate, 'modified_by' => $this->importedBy]) as $key => $value) {
                    if (is_null($value)) {
                        $setSqlStrings[] = "`$key` = NULL";
                    } elseif ($value === true) {
                        $setSqlStrings[] = "`$key` = '1'";
                    } elseif ($value === false) {
                        $setSqlStrings[] = "`$key` = '0'";
                    } else {
                        $value = str_replace(array_keys($this->specialCharReplaceForSql), $this->specialCharReplaceForSql, $value);
                        $setSqlStrings[] = "`$key` = '$value'";
                    }
            }
            $query = "UPDATE `$tableName` SET " . implode(', ', $setSqlStrings) . " WHERE `id` = $id;";
            $this->customQuery($query);
            
            //**** Details tables record ****
            
            $tmpDetailTablename = null;
            if ($detailsTablesData) {
                $foreignKey = str_replace('_masters', '_master_id', $mainTableData['name']);
                foreach ($detailsTablesData as $detailTable) {
                    $tableName = $detailTable['name'];
                    if (!empty($detailTable['data'])) {
                        $tableData = array_merge($detailTable['data'], [$foreignKey => $id]);
                        $setSqlStrings = [];
                        foreach ($tableData as $key => $value) {
                            if (is_null($value)) {
                                $setSqlStrings[] = "`$key` = NULL";
                            } elseif ($value === true) {
                                $setSqlStrings[] = "`$key` = '1'";
                            } elseif ($value === false) {
                                $setSqlStrings[] = "`$key` = '0'";
                            } else {
                                $value = str_replace(array_keys($this->specialCharReplaceForSql), $this->specialCharReplaceForSql, $value);
                                $setSqlStrings[] = "`$key` = '$value'";
                            }
                        }
                        $query = "UPDATE `$tableName` SET " . implode(', ',
                            $setSqlStrings) . " WHERE `$foreignKey` = $id;";
                        $this->customQuery($query); 
                    }  
                }
            }
            
            // **** Keep updated tables in memory to populate revs tables****
            
            $this->addToModifiedDatabaseTablesList($mainTableData['name'], $detailsTableName);
        }
    }

    /**
     * @param $mainTableName
     * @param $detailTableName
     */
    public function addToModifiedDatabaseTablesList($mainTableName, $detailTableName)
    {
        $key = $mainTableName . '-' . (is_null($detailTableName) ? '' : $detailTableName);
        $this->modifiedDatabaseTablesList[$key] = [$mainTableName, $detailTableName];
    }

    /**
     * Insert into revs table data created or updated .
     * When no parameter is set, the system will insert any record created or modified by
     * following functions:
     *        - customInsertRecord()
     *        - updateTableData()
     *
     * @param null $mainTablename
     * @param null $detailTablename
     * @throws ImportException
     */
    public function insertIntoRevsBasedOnModifiedValues($mainTablename = null, $detailTablename = null)
    {
        $tablesSetsToUpdate = $this->modifiedDatabaseTablesList;
        if (!is_null($mainTablename)) {
            $key = $mainTablename . '-' . (is_null($detailTablename) ? '' : $detailTablename);
            $tablesSetsToUpdate = [$key => [$mainTablename, $detailTablename]];
        }

        //Check masters model alone
        $initialTablesSetsToUpdate = $tablesSetsToUpdate;
        foreach ($initialTablesSetsToUpdate as $key => $tmp) {
            if (preg_match('/^([a-z]+_masters)\-$/', $key, $matches)) {
                $masterTableName = $matches[1];
                $controlTableName = str_replace('_masters', '_controls', $masterTableName);
                if (!isset($this->atimControls[$controlTableName])) {
                    $migrationDieDebug = "";
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "[$masterTableName] [$controlTableName]. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
                foreach ($this->atimControls[$controlTableName] as $controlData) {
                    if (is_array($controlData) && isset($controlData['detail_tablename'])) {
                        $detailTableName = $controlData['detail_tablename'];
                        $newKey = "$masterTableName-$detailTableName";
                        if (!isset($tablesSetsToUpdate[$newKey])) {
                            $tablesSetsToUpdate[$newKey] = ["$masterTableName", "$detailTableName"];
                        }
                    }
                }
                unset($tablesSetsToUpdate[$key]);
            }
        }
        
        //Insert Into Revs
        $insertQueries = [];
        foreach ($tablesSetsToUpdate as $newTablesSet) {
            list($mainTablename, $detailTablename) = $newTablesSet;
            if (!$detailTablename) {
                // *** CLASSICAL MODEL ***
                $query = "DESCRIBE $mainTablename;";
                $results = $this->getSelectQueryResult($query);

                $tableFields = [];
                foreach ($results as $row) {
                    if (!in_array($row['Field'], ['created', 'created_by', 'modified', 'modified_by', 'deleted'])) {
                        $tableFields[] = $row['Field'];
                    }
                }
                $sourceTableFields = (empty($tableFields) ? '' : '`' . implode('`, `',
                            $tableFields) . '`, ') . "`modified_by`, `modified`";
                $revsTableFields = (empty($tableFields) ? '' : '`' . implode('`, `',
                            $tableFields) . '`, ') . '`modified_by`, `version_created`';
                $insertQueries[] = "INSERT INTO `{$mainTablename}_revs` ($revsTableFields)
                    (SELECT $sourceTableFields FROM `$mainTablename` WHERE `modified_by` = '{$this->importedBy}' AND `modified` = '{$this->importDate}');";
            } else {
                // *** MASTER DETAIL MODEL ***
                if (!preg_match('/^.+\_masters$/', $mainTablename)) {
                    $migrationDieDebug = "";
                    if (Configure::read('debug') > 0) {
                        $migrationDieDebug = "'{$mainTablename}' is not a 'Master' table of a MasterDetail model. ";
                    }
                    $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                }
                $foreignKey = str_replace('_masters', '_master_id', $mainTablename);
                //Master table
                $query = "DESCRIBE $mainTablename;";
                $results = $this->getSelectQueryResult($query);

                $tableFields = [];
                foreach ($results as $row) {
                    if (!in_array($row['Field'], ['created', 'created_by', 'modified', 'modified_by', 'deleted'])) {
                        $tableFields[] = $row['Field'];
                    }
                }
                $sourceTableFields = (empty($tableFields) ? '' : "$mainTablename.`" . implode("`, {$mainTablename}.`",
                            $tableFields) . '`, ') . "{$mainTablename}.`modified_by`, {$mainTablename}.`modified`";
                $revsTableFields = (empty($tableFields) ? '' : '`' . implode('`, `',
                            $tableFields) . '`, ') . '`modified_by`, `version_created`';
                $insertQueries[] = "INSERT INTO `{$mainTablename}_revs` ($revsTableFields)
                    (SELECT $sourceTableFields FROM `{$mainTablename}` INNER JOIN `{$detailTablename}` ON `{$mainTablename}`.`id` = `$detailTablename`.`$foreignKey` WHERE `{$mainTablename}`.`modified_by` = '{$this->importedBy}' AND `{$mainTablename}`.`modified` = '{$this->importDate}');";
                //Detail table
                $allDetailTablenames = ($mainTablename != 'sample_masters') ? [$detailTablename] : [$detailTablename, 'specimen_details', 'derivative_details'];
                foreach ($allDetailTablenames as $detailTablename) {
                    $query = "DESCRIBE $detailTablename;";
                    $results = $this->getSelectQueryResult($query);

                    $tableFields = [];
                    foreach ($results as $row) {
                        $tableFields[] = $row['Field'];
                    }
                    if (!in_array($foreignKey, $tableFields)) {
                        $migrationDieDebug = "";
                        if (Configure::read('debug') > 0) {
                            $migrationDieDebug = "Foreign Key '$foreignKey' defined based on 'Master' table name '$mainTablename' is not a field of the 'Detail' table '$detailTablename'. ";
                        }
                        $this->migrationDie($migrationDieDebug . "ERR/Line_" . __LINE__ . "/action_" . __FUNCTION__ . ".");
                    }
                    $sourceTableFields = (empty($tableFields) ? '' : "{$detailTablename}.`" . implode("`, {$detailTablename}.`",
                                $tableFields) . '`, ') . "{$mainTablename}.`modified`";
                    $revsTableFields = (empty($tableFields) ? '' : '`' . implode('`, `',
                                $tableFields) . '`, ') . '`version_created`';
                    $insertQueries[] = "INSERT INTO `" . $detailTablename . "_revs` ($revsTableFields)
                        (SELECT $sourceTableFields FROM `$mainTablename` INNER JOIN `$detailTablename` ON {$mainTablename}.`id` = `$detailTablename`.`$foreignKey` WHERE `$mainTablename`.`modified_by` = '$this->importedBy' AND `$mainTablename`.`modified` = '$this->importDate');";
                }
            }
        }
        foreach (array_unique($insertQueries) as $query) {
            $this->customQuery($query, true);

        }
    }
        
    //==================================================================================================================================================================================
    // DATA VALIDATIONS FUNCTIONS
    //==================================================================================================================================================================================
    
    /**
     * Validate a value is a value of a system list or a custom drop down list and return
     * the formatted value as it exists into database (when cases are different).
     *
     * @param unknown $value Value to validate
     * @param unknown $domainName Domain name of the list
     * @param unknown $summarySectionTitle Section title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryTitleAddIn Additional information to add to summary title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryDetailsAddIn Additional information to add to summary detail if an error is generated by the function (See $this->recordErrorAndMessage() description)
     *
     * @return string Formatted value to use to match value in database.
     * @throws ImportException
     */
    public function validateAndGetStructureDomainValue($value, $domainName, $summarySectionTitle, $summaryTitleAddIn = '', $summaryDetailsAddIn = '')
    {
        if (!array_key_exists($domainName, $this->domainsValues)) {
            $this->domainsValues[$domainName] = [];
            $domainDataResults = $this->getSelectQueryResult("SELECT id, source FROM structure_value_domains WHERE domain_name = '$domainName'");
            if (!empty($domainDataResults)) {
                if (!$this->structureValueDomainModel) {
                    $this->structureValueDomainModel = AppModel::getInstance("", "StructureValueDomain", true);
                }
                foreach ($this->structureValueDomainModel->getDropDown($domainName) as $listKey => $listValue) {
                    $this->domainsValues[$domainName][strtolower($listValue)] = $listKey;
                }           
            } else {
                $this->recordErrorAndMessage($summarySectionTitle, self::ERROR,
                    "Wrong '$domainName'" . (empty($summaryTitleAddIn) ? '' : ' - ' . $summaryTitleAddIn),
                    "The '$domainName' Structure Domain (defined as domain of value '$value') does not exist. The value will be set to null." . (empty($summaryDetailsAddIn) ? '' : " [$summaryDetailsAddIn]"));
                $value = '';
            }
        }
        if (strlen($value)) {
            if (array_key_exists(strtolower($value), $this->domainsValues[$domainName])) {
                $value = $this->domainsValues[$domainName][strtolower($value)];    //To set the right case
            } else {
                $domainNameValues = "Allowed Values : [" . implode("] & [",
                        array_keys($this->domainsValues[$domainName])) . "]";
                $strLimit = 300;
                if (strlen($domainNameValues) > $strLimit) {
                    $domainNameValues = substr($domainNameValues, 0, $strLimit) . '...';
                }
                $this->recordErrorAndMessage($summarySectionTitle, self::ERROR,
                    "Wrong '$domainName' Value" . (empty($summaryTitleAddIn) ? '' : ' - ' . $summaryTitleAddIn) . " $domainNameValues.",
                    "Value '$value' is not a value of the '$domainName' Structure Domain. The value will be set to null." . (empty($summaryDetailsAddIn) ? '' : " [$summaryDetailsAddIn]"));
                $value = '';
            }
        }
        return $value;
    }

    /**
     * Test excel value is a date with format yyyy-MM-dd and return the formatted date for database and the accuracy.
     *
     * @param string $date Value of the excel date
     * @param unknown $summarySectionTitle Section title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryTitleAddIn Additional information to add to summary title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryDetailsAddIn Additional information to add to summary detail if an error is generated by the function (See $this->recordErrorAndMessage() description)
     *
     * @return array array($formattedDate, $accuracy)
     */
    public function validateAndGetDateAndAccuracy($date, $summarySectionTitle, $summaryTitleAddIn, $summaryDetailsAddIn)
    {
        if (is_array($this->unknownMnHrDayMonthPatterns)) {
            $this->unknownMnHrDayMonthPatterns = "((" . implode(')|(', $this->unknownMnHrDayMonthPatterns) . "))";
            $this->unknownMnHrDayMonthPatterns = str_replace(array('.', '-', '*', '?', '+'), array('\.', '\-', '\*', '\?', '\+'), $this->unknownMnHrDayMonthPatterns);
        }
        
        $date = str_replace(' ', '', $date);
        $result = null;
        if (! strlen($date) ) {
            $result = ['', ''];
        }         
        elseif (preg_match('/^((19|20)([0-9]{2}))\-(' . $this->unknownMnHrDayMonthPatterns . ')\-(' . $this->unknownMnHrDayMonthPatterns . ')$/', $date, $matches)) {
            $result = [$matches[1] . '-01-01', 'm'];
        }
        elseif (preg_match('/^((19|20)([0-9]{2})\-([01][0-9]))\-(' . $this->unknownMnHrDayMonthPatterns . ')$/', $date, $matches)) {
            $result = [$matches[1] . '-01', 'd'];
        }  
        elseif (preg_match('/^(19|20)([0-9]{2})\-([01][0-9])\-([0-3][0-9])$/', $date, $matches)) {
            $result = [$date, 'c'];
        } elseif (preg_match('/^(19|20)([0-9]{2})\-([01][0-9])$/', $date, $matches)) {
            $result = [$date . '-01', 'd'];
        } elseif (preg_match('/^((19|20)([0-9]{2}))$/', $date, $matches)) {
            $result = [$matches[1] . '-01-01', 'm'];
        }
        $wrongDate = false;
        if (! is_null($result) && $result[0]) {
            list($year, $month, $day) = explode('-', $result[0]);
            if (!checkdate($month, $day, $year)) {
                $result = null;
                $wrongDate = true;
            }
        }
        if (is_null($result)) {
            if ($wrongDate) {
                $this->recordErrorAndMessage($summarySectionTitle, self::ERROR,
                    'Date Error' . (empty($summaryTitleAddIn) ? '' : ' - ' . $summaryTitleAddIn),
                    "The date '$date' does not exist in calendar! The date will be set to null." . (empty($summaryDetailsAddIn) ? '' : " [$summaryDetailsAddIn]"));
            } else {
                $this->recordErrorAndMessage($summarySectionTitle, self::ERROR,
                    'Date Format Error' . (empty($summaryTitleAddIn) ? '' : ' - ' . $summaryTitleAddIn),
                    "Format of the date '$date' is not supported (different than yyyy-MM-dd)! The date will be set to null." . (empty($summaryDetailsAddIn) ? '' : " [$summaryDetailsAddIn]"));
            }
            return ['', ''];
        } else {
            return $result;
        }
    }

    /**
     * Test excel values are component of a datetime following format 'yyyy-MM-dd HH:mm:ss' or 'yyyy-MM-dd HH:mm'and return the formatted datetime for database and the accuracy.
     *
     * @param string $dateTime Value of the excel date time
     * @param unknown $summarySectionTitle Section title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryTitleAddIn Additional information to add to summary title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryDetailsAddIn Additional information to add to summary detail if an error is generated by the function (See $this->recordErrorAndMessage() description)
     *
     * @return array array($formattedDatetime, $accuracy)
     * @throws ImportException
     */
    public function validateAndGetDateTimeAndAccuracy($dateTime, $summarySectionTitle, $summaryTitleAddIn, $summaryDetailsAddIn)
    {        
        if (is_array($this->unknownMnHrDayMonthPatterns)) {
            $this->unknownMnHrDayMonthPatterns = "((" . implode(')|(', $this->unknownMnHrDayMonthPatterns) . "))";
            $this->unknownMnHrDayMonthPatterns = str_replace(array('.', '-', '*', '?', '+'), array('\.', '\-', '\*', '\?', '\+'), $this->unknownMnHrDayMonthPatterns);
        }
        
        $date = explode(' ', $dateTime);
        $time = '';
        if(sizeof($date) == 2) {
            list($date, $time) = $date;
        } else {
            $date = $dateTime;
        }
        $date = str_replace(' ', '', $date);
        $time = str_replace(' ', '', $time);
        //** Get Date **
        list($formattedDate, $formattedDateAccuracy) = $this->validateAndGetDateAndAccuracy($date, $summarySectionTitle,
            $summaryTitleAddIn, $summaryDetailsAddIn);
        if (!$formattedDate) {
            if (! strlen($date) && strlen($time)) {
                $this->recordErrorAndMessage($summarySectionTitle, self::ERROR,
                    'DateTime Format Error: Date Is Missing' . (empty($summaryTitleAddIn) ? '' : ' - ' . $summaryTitleAddIn),
                    "Format of the datetime '$dateTime' is not supported! The datetime will be set to null." . (empty($summaryDetailsAddIn) ? '' : " [$summaryDetailsAddIn]"));
            }
            return ['', ''];
        } else {
            //Combine date and time
            if (! strlen($time)) {
                return [$formattedDate . ' 00:00', str_replace('c', 'h', $formattedDateAccuracy)];
            } else {
                if ($formattedDateAccuracy != 'c') {
                    $this->recordErrorAndMessage($summarySectionTitle, self::ERROR,
                        'Time Set for an Inaccurate Date' . (empty($summaryTitleAddIn) ? '' : ' - ' . $summaryTitleAddIn),
                        "Date and time are set but date is inaccurate. The datetime will be set to null." . (empty($summaryDetailsAddIn) ? '' : " [$summaryDetailsAddIn]"));
                    return ['', ''];
                } elseif (preg_match('/^(' . $this->unknownMnHrDayMonthPatterns . '):(' . $this->unknownMnHrDayMonthPatterns . ')(:' . $this->unknownMnHrDayMonthPatterns . '){0,1}$/', $time, $matches)) {
                    return [$formattedDate . ' 01:01:01', 'h'];
                } elseif (preg_match('/^((0{0,1}[0-9])|((1[0-9])|(2[0-3]))):(' . $this->unknownMnHrDayMonthPatterns . ')(:' . $this->unknownMnHrDayMonthPatterns . '){0,1}$/', $time, $matches)) {
                    return [$formattedDate . ' ' . ((strlen($matches[1]) != 1) ? $matches[1] : '0' . $matches[1]) . ':01:01', 'i'];
                } elseif (preg_match('/^((0{0,1}[0-9])|((1[0-9])|(2[0-3]))):([0-5][0-9])(:' . $this->unknownMnHrDayMonthPatterns . '){0,1}$/', $time, $matches)) {
                    return [$formattedDate . ' ' . ((strlen($matches[1]) != 1) ? $matches[1] : '0' . $matches[1]) . ':' . $matches[6] . ':01', 'c'];
                } elseif (preg_match('/^((0{0,1}[0-9])|((1[0-9])|(2[0-3]))):([0-5][0-9])(:[0-5][0-9]){0,1}$/', $time, $matches)) {
                    return [$formattedDate . ' ' . ((strlen($matches[1]) != 1) ? $time : '0' . $time), 'c'];
                } else {
                    $this->recordErrorAndMessage($summarySectionTitle, self::ERROR,
                        'Time Format Error' . (empty($summaryTitleAddIn) ? '' : ' - ' . $summaryTitleAddIn),
                        "Format of the datetime '$date $time' is not supported! The datetime will be set to null." . (empty($summaryDetailsAddIn) ? '' : " [$summaryDetailsAddIn]"));
                    return ['', ''];
                }
            }
        }
    }

    /**
     * Test excel values are time following format 'HH:mm:ss' or 'HH:mm' and return the formatted time for database and the accuracy.
     *
     * @param string $time Value of the excel time
     * @param unknown $summarySectionTitle Section title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryTitleAddIn Additional information to add to summary title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryDetailsAddIn Additional information to add to summary detail if an error is generated by the function (See $this->recordErrorAndMessage() description)
     *
     * @return string $formattedTime
     */
    public function validateAndGetTime($time, $summarySectionTitle, $summaryTitleAddIn, $summaryDetailsAddIn)
    {
        $time = str_replace(' ', '', $time);
        if (! strlen($time)) {
            return '';
        } else {
            if (preg_match('/^((0{0,1}[0-9])|((1[0-9])|(2[0-3]))):[0-5][0-9]$/', $time, $matches)) {
                return (strlen($matches[1]) == 1) ? $time : '0' . $time;
            } elseif (preg_match('/^((0{0,1}[0-9])|((1[0-9])|(2[0-3]))):([0-5][0-9]):[0-5][0-9]$/', $time, $matches)) {
                return ((strlen($matches[1]) == 1) ? $time : '0' . $time) . $matches[6];
            } else {
                $this->recordErrorAndMessage($summarySectionTitle, self::ERROR,
                    'Time Format Error' . (empty($summaryTitleAddIn) ? '' : ' - ' . $summaryTitleAddIn),
                    "Format of time '$time' is not supported! The time will be set to null." . (empty($summaryDetailsAddIn) ? '' : " [$summaryDetailsAddIn]"));
                return '';
            }
        }
    }

    /**
     * Test excel value is a decimal
     *
     * @param string $decimalValue Exel value
     * @param unknown $summarySectionTitle Section title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryTitleAddIn Additional information to add to summary title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryDetailsAddIn Additional information to add to summary detail if an error is generated by the function (See $this->recordErrorAndMessage() description)
     *
     * @return string $formattedValue
     */
    public function validateAndGetDecimal($decimalValue, $summarySectionTitle, $summaryTitleAddIn, $summaryDetailsAddIn)
    {
        $decimalValue = str_replace([' ', ','], ['', '.'], $decimalValue);
        if (strlen($decimalValue)) {
            if (preg_match('/^([\-]){0,1}[0-9]+([\.,][0-9]+){0,1}$/', $decimalValue)) {
                return $decimalValue;
            } elseif (preg_match('/^([\.,][0-9]+){0,1}$/', $decimalValue)) {
                return '0' . $decimalValue;
            } else{
                $this->recordErrorAndMessage($summarySectionTitle, self::ERROR,
                    "Wrong Decimal Format" . (empty($summaryTitleAddIn) ? '' : ' - ' . $summaryTitleAddIn),
                    "Format of decimal '$decimalValue' is not supported! The value will be set to null." . (empty($summaryDetailsAddIn) ? '' : " [$summaryDetailsAddIn]"));
                return '';
            }
        } else {
            return '';
        }
    }

    /**
     * Test excel value is an integer
     *
     * @param string $integerValue Exel value
     * @param unknown $summarySectionTitle Section title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryTitleAddIn Additional information to add to summary title if an error is generated by the function (See $this->recordErrorAndMessage() description)
     * @param string $summaryDetailsAddIn Additional information to add to summary detail if an error is generated by the function (See $this->recordErrorAndMessage() description)
     *
     * @return string $formattedValue
     */
    public function validateAndGetInteger($integerValue, $summarySectionTitle, $summaryTitleAddIn, $summaryDetailsAddIn)
    {
        $integerValue = str_replace([' '], [''], $integerValue);
        if (strlen($integerValue)) {
            if (preg_match('/^([\-]){0,1}[0-9]+$/', $integerValue)) {
                return $integerValue;
            } else {
                $this->recordErrorAndMessage($summarySectionTitle, self::ERROR,
                    "Wrong Integer Format" . (empty($summaryTitleAddIn) ? '' : ' - ' . $summaryTitleAddIn),
                    "Format of integer '$integerValue' is not supported! The value will be set to null." . (empty($summaryDetailsAddIn) ? '' : " [$summaryDetailsAddIn]"));
                return '';
            }
        } else {
            return '';
        }
    }

    //</editor-fold>
}

/**
 * Class ImportException
 */
class ImportException extends Exception
{

}