<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class FormBuilder
 */
class FormBuilder extends AdministrateAppModel
{
    private $i18n = array();
    private $prefixes = array();
    private $structureAlias = "";
    private $structureMasterAlias = "";
    public $labelFB = "";
    public $pluginFB = "";
    public $modelFB = "";
    public $masterFB = "";
    public $defaultAliasFB = "";
    public $optionsFB = array();

    /**
     *
     * @param array $variables
     * @return array|bool
     */
    public function listOfFormsSummary($variables = array())
    {
        $return = false;
        if (isset($variables['FormBuilder.id'])) {
            $result = $this->find('first', array(
                'conditions' => array(
                    'FormBuilder.id' => $variables['FormBuilder.id']
                )
            ));
            $return = array(
                'menu' => array(
                    null,
                    __($result['FormBuilder']['label'])
                ),
                'title' => null,
                'data' => null,
                'structure alias' => null
            );
        }
        return $return;
    }

    /**
     *
     * @param array $variables
     * @return array|bool
     */
    public function formSummary($variables = array())
    {
        $return = false;
        if (isset($variables['Form.name']) && $variables['Form.name']) {
            $return = array(
                'menu' => array(
                    null,
                    $variables['Form.name']
                ),
                'title' => null,
                'data' => null,
                'structure alias' => null
            );
        }
        return $return;
    }

    public function getDataFromAlias($data, $model, $option = array())
    {
        $detailResult = (!empty($data["detail"]))?$this->getData($data["detail"], $model, $option):array('common'=> array(), 'fb'=> array(), 'validationData' => array(), 'valueDomainData' => array(), 'settings' => array());
        $masterResult = (!empty($data["master"]))?$this->getData($data["master"], $model, $option):array('common'=> array(), 'fb'=> array(), 'validationData' => array(), 'valueDomainData' => array(), 'settings' => array());
        $result = array(
            "masterProd" => $masterResult['common'],
            "masterTest" => $masterResult['fb'],
            "detailProd" => $detailResult['common'],
            "detailTest" => $detailResult['fb'],
            "valueDomainData" => json_encode($this->mergeArray($masterResult['valueDomainData'], $detailResult['valueDomainData'])),
            "validationData" => json_encode($this->mergeArray($masterResult['validationData'], $detailResult['validationData'])),
            "settings" => json_encode($this->mergeArray($masterResult['settings'], $detailResult['settings']))
        );
        if (!empty($data['oldDetail'])){
            $result['detailOldProd'] = $this->getData($data["oldDetail"], $model, $option)['common'];
        }
        if (!empty($detailResult['fbDeletable'])){
            $result['fbDeletable'] = $detailResult['fbDeletable'];
        }
        return $result;
    }

    private function mergeArray()
    {
        $return = array();
        $args = func_get_args();
        for ($i=0; $i<func_num_args(); $i++){
            if (is_array($args[$i])){
                foreach ($args[$i] as $val) {
                    array_push($return, $val);
                }
            }else{
                array_push($return, $args[$i]);
            }
        }
        return $return;
    }

    private function checkModel($modelMD, $model)
    {
        $ok = false;
        if ($modelMD == $model){
            $ok = true;
        } elseif (str_replace(array('Master', 'Detail'), array('', ''), $modelMD) == str_replace(array('Master', 'Detail', 'Control'), array('', '', ''), $model)){
            $ok = true;
        }elseif ($modelMD == "SampleMaster" && ($model == "DerivativeDetail" || $model == "ViewSample" || $model == "SpecimenDetail")){
            $ok = true;
        }
        return $ok;
    }

    private function getValidationData($d)
    {
        $response = array();
        if (!empty($d['StructureValidation'])){
            foreach ($d['StructureValidation'] as $validation) {
                $matchRange = preg_match("/(range),(\-?[0-9\.]+),(\-?[0-9\.]+)/", $validation['rule'], $range);
                $matchBetween = preg_match("/(between),(\-?[0-9]+),(\-?[0-9]+)/", $validation['rule'], $between);
                if ($matchBetween){
                    if ($between[2]!="" && $between[3]!=""){
                        $response[] = array(
                            'name' => 'data[FunctionManagement][between_from]',
                            'value' => $between[2]
                        );
                        $response[] = array(
                            'name' => 'data[FunctionManagement][between_to]',
                            'value' => $between[3]
                        );
                    }
                }elseif ($validation['rule'] == 'isUnique'){
                    $response[] = array(
                        'name' => 'data[FunctionManagement][is_unique]',
                        'value' => '1'
                    );
                }elseif ($validation['rule'] == 'notBlank'){
                    $response[] = array(
                        'name' => 'data[FunctionManagement][not_blank]',
                        'value' => '1'
                    );
                }elseif ($matchRange){
                    if ($range[2]!="" && $range[3]!=""){
                        $response[] = array(
                            'name' => 'data[FunctionManagement][range_from]',
                            'value' => (round($range[2]) == $range[2])?$range[2]+1:round($range[2])
                        );
                        $response[] = array(
                            'name' => 'data[FunctionManagement][range_to]',
                            'value' => (round($range[3]) == $range[3])?$range[3]-1:round($range[3])
                        );
                    }
                }else{

                }
            }
        }
        $response = (empty($response)) ? "" : $response;
        return $response;
    }

    private function getValueDomain($d)
    {
        $response = array("value" => "", "id" => "0");
        if (!empty($d['StructureValueDomain'])){
            if (!empty($d['StructureValueDomain']['source'])){
                $match = preg_match("/(StructurePermissibleValuesCustom::getCustomDropdown\()'(.+)'(\))/", $d['StructureValueDomain']['source'], $a);
                if ($match && countCustom($a) == 4 && !empty($a[2])){
                    $response = array(
                        "value" => str_replace("'", "", $a[2]),
                        "id" => $d["StructureValueDomain"]["id"],
                        "multi-select" => (strpos($d['setting'], "class=atim-multiple-choice") !== false ? 1 : 0),
                    );
                }
            }
        }
        return $response;
    }

    public function getSettings($data, $index = true)
    {
        $response = '';
        if (!empty($data['setting'])) {
            $arrayData = [];
            if (preg_match("/class=pasteDisabled/", $data['setting'])) {
                if ($index) {
                    $arrayData['can_paste'] = 'no';
                    $response = $this->normalised(['settings' => $arrayData], 'settingsIndex');
                } else {
                    $response = ['can_paste' => 'no'];
                }
            }
        }
        return $response;
    }

    public function getData($data, $model, $option = array())
    {
        $option += array('notCheckModel' => false);
        extract($option);
        $result = array('common' => array(), 'fb' => array(), 'fbDeletable' => array(), 'validationData' => array(), 'settings' => array(), 'valueDomainData' => array(), 'OtherValidation'=> array());
        $dataTemp = array();
        if (isset($data[0]["structure"]["Sfs"])){
            foreach ($data as $d){
                if (isset($d["structure"]["Sfs"])){
                    $dataTemp = array_merge($dataTemp, $d["structure"]["Sfs"]);
                }
            }
        }elseif (isset($data["structure"]["Sfs"])){
            $dataTemp = $data["structure"]["Sfs"];
        }
        foreach ($dataTemp as $d) {
            if ($d['type'] == 'hidden'){
                continue;
            }
            if ($this->checkModel($model, $d["model"]) || $notCheckModel){
                $tempFields = array();
                $tempFields["Structure"]["id"]=$d["structure_id"];
                $tempFields["Structure"]["alias"]=$d["structure_alias"];

                $tempFields["StructureField"]["id"]=$d["structure_field_id"];
                $tempFields["StructureField"]["field"]=$d["field"];
                $tempFields["StructureField"]["structure_value_domain"]=$d["structure_value_domain"];
                $tempFields["StructureField"]["structure_value_domain_name"]=$d["structure_value_domain_name"];
                $multipleChoice = strpos($d['setting'], "class=atim-multiple-choice") !== false ? ___('(multiple choice)') : "";
                $tempFields["FunctionManagement"]["is_structure_value_domain"] = $this->getListName($d["structure_value_domain"]) . $multipleChoice;
                $tempFields["StructureField"]["flag_confidential"]=$d["flag_confidential"];
                $tempFields["StructureField"]["setting"]=$d["setting"];
                $tempFields["FunctionManagement"]['settings'] = $this->getSettings($d);

//                if (empty($d["language_label"]) && !$notCheckModel){
//                    $tempFields["StructureField"]["language_label"]=___($d["language_tag"]);
//                }else{
//                    $tempFields["StructureField"]["language_label"]=($notCheckModel)?$d["language_label"]:___($d["language_label"]);
//                }
                $tempFields['StructureField']['language_label'] = ($notCheckModel) ? $d['language_label'] : ___($d['language_label']);
                $tempFields['StructureField']['language_tag'] = ($notCheckModel) ? $d['language_tag'] : ___($d['language_tag']);

                $tempFields["StructureField"]["language_help"]=($notCheckModel)?$d["language_help"]:___($d["language_help"]);
                $tempFields["StructureField"]["type"]=$d["type"];
                $tempFields["StructureField"]["flag_form_builder"]=$d["flag_form_builder"];
                $tempFields["StructureField"]["flag_test_mode"]=$d["flag_test_mode"];
                if ($notCheckModel){
                    $tempFields["StructureField"]["tablename"]=$d["tablename"];
                    $tempFields["StructureField"]["plugin"]=$d["plugin"];
                    $tempFields["StructureField"]["model"]=$d["model"];
                    $tempFields["StructureField"]["language_tag"]=$d["language_tag"];
                    $tempFields["StructureField"]["setting"]=$d["setting"];
                    $tempFields["FunctionManagement"]['settings'] = $this->getSettings($d);
                    $tempFields["StructureField"]["default"]=$d["default"];
                    $tempFields["StructureField"]["flag_copy_from_form_builder"]=$d["flag_copy_from_form_builder"];

                    $tempFields['StructureFormat']['setting']=$d['setting'];
                    $tempFields['StructureFormat']['default']=$d['default'];
                    $tempFields['StructureFormat']['flag_add_readonly']=$d['flag_add_readonly'];
                    $tempFields['StructureFormat']['flag_edit_readonly']=$d['flag_edit_readonly'];
                    $tempFields['StructureFormat']['flag_search_readonly']=$d['flag_search_readonly'];
                    $tempFields['StructureFormat']['flag_addgrid']=$d['flag_addgrid'];
                    $tempFields['StructureFormat']['flag_addgrid_readonly']=$d['flag_addgrid_readonly'];
                    $tempFields['StructureFormat']['flag_editgrid']=$d['flag_editgrid'];
                    $tempFields['StructureFormat']['flag_editgrid_readonly']=$d['flag_editgrid_readonly'];
                    $tempFields['StructureFormat']['flag_summary']=$d['flag_summary'];
                    $tempFields['StructureFormat']['flag_batchedit']=$d['flag_batchedit'];
                    $tempFields['StructureFormat']['flag_batchedit_readonly']=$d['flag_batchedit_readonly'];
                    $tempFields['StructureFormat']['flag_float']=$d['flag_float'];
                    $tempFields['StructureFormat']['margin']=$d['margin'];

                }

                $tempFields["StructureFormat"]["id"]=$d["structure_format_id"];
                $tempFields["StructureFormat"]["flag_add"]=$d["flag_add"];
                $tempFields["StructureFormat"]["flag_edit"]=$d["flag_edit"];
                $tempFields["StructureFormat"]["flag_search"]=$d["flag_search"];
                $tempFields["StructureFormat"]["flag_index"]=$d["flag_index"];
                $tempFields["StructureFormat"]["flag_detail"]=$d["flag_detail"];
                $tempFields["StructureFormat"]["display_column"]=$d["display_column"];
                $tempFields["StructureFormat"]["display_order"]=$d["display_order"];
                $tempFields["StructureFormat"]["language_heading"]=!empty(__($d["language_heading"])) ? __($d["language_heading"]) : '';
               if($notCheckModel){
                   if (!empty($d['StructureValidation'])){
                        $tempFields['validationData'] = $d['StructureValidation'];
                   }
                    $result['fb'][] = $tempFields;
                }elseif(!empty($d["flag_test_mode"]) && empty($d["flag_copy_from_form_builder"])){
                    $tempFields["FunctionManagement"]["fb_has_validation"] = $this->normalised($this->getValidationData($d), 'validationIndex');
                    $result['fbDeletable'][] = $tempFields;
                }elseif(empty($d["flag_test_mode"])){
                    $tempFields["FunctionManagement"]["fb_has_validation"] = $this->normalised($this->getValidationData($d), 'validationIndex');
                   $result['common'][] = $tempFields;
               }else{
                   $result["settings"][] = $this->getSettings($d, false);
                    $result["valueDomainData"][] = $this->getValueDomain($d);
                    $result["validationData"][] = $this->getValidationData($d);
                    $tempFields["FunctionManagement"]["fb_has_validation"] = $this->normalised($this->getValidationData($d), 'validationIndex');
                    $result['fb'][] = $tempFields;
                }
            }else{
                if (Configure::read('debug') != 0) {
                    AppController::addWarningMsg("FomrBuider.getData(): Model (structure_field) removed form the structure fields list (structure_alias = [{$d['structure_alias']}],  structure_field_id = [{$d['structure_field_id']}], model = [{$d['model']}], field = [{$d['field']}]).");
                }
            }
        }
        return $result;
    }

    public function checkValidation($type)
    {
        $validation = null;
        if (isset($type)){
            if (in_array($type, array('date', 'datetime', 'time'))){
                $validation = array("is_unique", "not_blank");
            }
            if (in_array($type, array('float1','float2','float5', 'integer', 'integer_positive'))){
                $validation = array("is_unique", "not_blank", "range_from");
            }
            if (in_array($type, array('input', 'input_short', 'input_medium', 'textarea'))){
                $validation = array("is_unique", "not_blank", "between_from", "alpha_numeric");
            }
            if (in_array($type, array('yes_no', 'y_n_u', 'select', 'multi-select', 'validateIcd10WhoCode', 'validateIcdo3MorphoCode', 'validateIcdo3TopoCode'))){
                $validation = array("not_blank");
            }
            if (in_array($type, array('checkbox'))){
                $validation = null;
            }

        }
        return $validation;
    }

    public function categorizedIdByStatus($data, $newdata)
    {
        $ids = array(
            'add'=> array(),
            'update'=> array(),
            'delete'=> array()
        );
        $ids1 = array();
        $ids2 = array();


    }

    public function normalised($data, $type)
    {
        $response = "";
        if($type == "validation"){
            if (isset($data["FunctionManagement"]['is_unique'])){
                $response = ($data["FunctionManagement"]['is_unique'])?$response . __("unique") . " & " : $response;
            }
            if (isset($data["FunctionManagement"]['not_blank'])){
                $response = ($data["FunctionManagement"]['not_blank'])?$response . __("required") . " & " : $response;
            }
            if (isset($data["FunctionManagement"]['between_from']) && $data["FunctionManagement"]['between_from']!=""){
                $response .= __("length from %s to %s", $data["FunctionManagement"]['between_from'], $data["FunctionManagement"]['between_to']) . " & ";
            }
            if (isset($data["FunctionManagement"]['range_from']) && $data["FunctionManagement"]['range_from']!=""){
                $response .= __("value from %s to %s", $data["FunctionManagement"]['range_from'], $data["FunctionManagement"]['range_to']) . " & ";
            }
        }elseif($type == "valuedomain"){
            $response = "Test-Yaser";
        }elseif($type == 'settings'){
            if (!empty($data['settings'])){
                foreach ($data['settings'] as $k => $v){
                    $response .= __($k) . ": " . __($v) . " & ";
                }
            }
        }elseif($type == 'settingsIndex'){
            if (!empty($data['settings'])){
                $response = "The settings of each fields are |||";
                foreach ($data['settings'] as $k => $v){
                    $response .= __($k) . ": " . __($v) . " |||-";
                }
                return $response;
            }
        }elseif($type == 'validationIndex'){
            if (!empty($data)){
                $response = "The validation rules for formbuilder |||";
                foreach ($data as $k => $rule){
                    if ($rule['name'] == 'data[FunctionManagement][is_unique]'){
                        $response .= __("unique") . " |||-" ;
                    }elseif($rule['name'] == 'data[FunctionManagement][not_blank]'){
                        $response .= __("required") . " |||-" ;
                    }elseif($rule['name'] == 'data[FunctionManagement][between_from]'){
                        $response .= __("length from %s to %s", $rule["value"], $data[$k+1]['value']) . " |||-";
                        unset($data[$k+1]);
                    }elseif($rule['name'] == 'data[FunctionManagement][range_from]'){
                        $response .= __("value from %s to %s", $rule["value"], $data[$k+1]['value']) . " |||-";
                        unset($data[$k+1]);
                    }
                }
            }
            return $response;
        }

        if (!empty($response)){
            $response = substr($response, 0, strlen($response)-3);
        }
        $response = json_encode(array("text" => $response, "title" => str_replace(" & ", "\n", $response)));
        return $response;
    }

    private function addToI18n(&$words="", $prefix = "")
    {
        if (empty($prefix)){
            $prefix = $this->structureAlias . " || ";
        }
        $this->prefixes = $this->prefixes + array($prefix);
        $lang = (Configure::read('Config.language')=='eng')?'en':'fr';
        $langAbsent = (Configure::read('Config.language')!='eng')?'en':'fr';
        $i18nModel = new Model(array(
            'table' => 'i18n',
            'name' => 0
        ));

        if (!empty($words)){
            if (!is_array($words)){
                $wordsArray = array($words);
            }else{
                $wordsArray = $words;
            }
            foreach ($wordsArray as &$word){
                if (!empty($word)){
                    $word = str_replace(array("\t", "\n", "\r"), array(" ", " ", ""), $word);
                    $id = $prefix . substr($word, 0, 100);
                    $add = true;
                    foreach ($this->i18n as $w) {
                        if ($w['id'] == $id){
                            $add = false;
                            break;
                        }
                    }

                    $wordsI18n = $i18nModel->find("all", array('conditions' => array(
                        "BINARY(".$lang.")" => $word
                    )));

                    $wordI18n = array();
                    foreach ($wordsI18n as $w) {
                        if (!empty($w[0][$langAbsent])){
                            $wordI18n = $w;
//                            if (strtolower(trim($w[0][$langAbsent])) == strtolower(trim($word))){
//                                $wordI18n = $w;
//                            }
                            break;
                        }
                    }

                    if (empty($wordI18n) && !empty($wordsI18n)){
                        $wordI18n = $wordsI18n[0];
                    }

                    if (!empty($wordI18n)){
                        $wordAbsent = $wordI18n[0][$langAbsent];
                        $word = $wordI18n[0][$lang];
                    }else{
                        $wordAbsent = $word . "_". $langAbsent;
                    }
                    $pageId = ($this->modelFB == 'MiscIdentifierControl') ? "MiscIdentifier" : "";
                    if ($add) {
                        $this->i18n[] = array(
                            "id" => $id,
                            $lang => $word,
                            $langAbsent => $wordAbsent,
                            "page_id" => $pageId

                        );
                    }
                    $word = $id;
                }
            }

            if (!is_array($words)){
                $words = $wordsArray[0];
            }else{
                $words = $wordsArray;
            }

        }
    }

    public function getLastInsertID()
    {
        return $this->_insertID;
    }

    private function getListName($structureValueDomainId)
    {
        $structureValueDemainName = "";
        $structureValueDomainModelInstance = AppModel::getInstance("", "StructureValueDomain");
        $data = $structureValueDomainModelInstance->findById($structureValueDomainId);

        if (!empty($data)){
            if (empty($data["source"])){
                $structureValueDemainName = __("fixed list");
            }else{
                preg_match("/(StructurePermissibleValuesCustom::getCustomDropdown\()(.+)(\))/", $data["source"], $a);
                if (countCustom($a) == 4) {

                    $sPVCC = AppModel::getInstance("", "StructurePermissibleValuesCustomControl");
                    $sPVCCLink = $sPVCC->getLink($data);

                    $link = 'http' . (isset($_SERVER['HTTPS']) ? 's' : '') . '://' .$_SERVER['HTTP_HOST'].AppController::getInstance()->request->webroot. $sPVCCLink["link"];
                    $a = $sPVCCLink["text"];
                    $title = __("form_builder_for customising the (%s) list click here", $a);
                    $linkTag = "<a href = '$link' target='_blank' title = '$title'>$a</a>";
                    $structureValueDemainName = $linkTag;
                }else{
                    $structureValueDemainName = __("functional list");
                }
            }

        }
        return $structureValueDemainName;
    }

    private function normalisedErrorMessages($options)
    {
        extract($options);
        $ve = (!empty($validationErrors)) ? "<br>" . implode("<br>", Hash::extract($validationErrors, "{s}.{n}")) : "";
        AppController::getInstance()->atimFlashError(__($message, $model, $class, $line, $ve), "javascript:history.back();");
    }

    public function normalisedAndSave($data = null, $validate = false, $fieldList = array())
    {
        $models = $data["models"];

        $modelInstance = AppModel::getInstance($models["main"]["plugin"], $models["main"]["model"]);
        $master = $models["main"]["master"];

        $structureModelInstance = AppModel::getInstance('', 'Structure');
        $structureFieldModelInstance = AppModel::getInstance("", "StructureField");
        $structureFormatModelInstance = AppModel::getInstance("", "StructureFormat");
        $structureValidationModelInstance = AppModel::getInstance("", "StructureValidation");
        $structureValueDomainModelInstance = AppModel::getInstance("", "StructureValueDomain");
        $structurePermissibleValuesCustomControlModelInstance = AppModel::getInstance("", "StructurePermissibleValuesCustomControl");

        $options = $data["options"];
        $relatedData = $data["others"]['FormBuilder'];
        $modelName = $modelInstance->name;
        $this->structureAlias = ($master == 'MiscIdentifier') ? getPrefix() . $relatedData['default_alias'] : $data[$modelName]['detail_form_alias'];
        $structureValidationData = $data['StructureValidation'];
        if (!empty($master)){
            $this->addToI18n($data[$data["models"]["main"]["model"]][$modelInstance->controlType]);

            $data[$modelName]['flag_form_builder'] = 1;
            $modelInstance->addWritableField();
            $saveResult = $modelInstance->save($data);
            if (!$saveResult){
                $this->normalisedErrorMessages([
                    'validationErrors' => $modelInstance->validationErrors,
                    'line' => __LINE__,
                    'class' => __CLASS__,
                    'message' => "error in saving %s (%s, %s) %s",
                    'model' => $modelName
                ]);
            }else{
                $this->_insertID = $saveResult[$modelInstance->name]['id'];
            }
        }

        if ($master == 'MiscIdentifier'){
            $this->saveI18n();
            return;
        }

        if (!empty($master)){
            $data['Structure'] = array(
                'alias' => $data[$modelName]['detail_form_alias'],
                'description' => $data[$modelName]['detail_form_alias']
            );
            $structureModelInstance->addWritableField(array("alias", "description"));
            $saveResult = $structureModelInstance->save($data, true);
            $structureId = "";
            if (!$saveResult){
                $this->normalisedErrorMessages([
                    'validationErrors' => $modelInstance->validationErrors,
                    'line' => __LINE__,
                    'class' => __CLASS__,
                    'message' => "error in saving %s (%s, %s) %s",
                    'model' => $structureModelInstance->name
                ]);
            }else{
                $structureId = $saveResult['Structure']['id'];
            }
        }else{
            $s = $structureModelInstance->find('first', array(
                'conditions' => array(
                    'alias' => $data[$modelName]['alias']
                ),
                'recursive' => 0
            ));
            $structureId = $s['structure']['Structure']['id'];
        }


        $commonPrefix = $options["prefix-common"];


        $tableName = $data[$modelName]['detail_tablename'];
        $index=0;
        $sfid=array();

        foreach ($data[$commonPrefix] as $key => $value) {
            $functionManagementData = $value['FunctionManagement'];
            $structureFieldData = $value["StructureField"];
            $structureFieldData["plugin"] = $relatedData["plugin"];
            $structureFieldData["model"] = !empty($relatedData["detail"])?$relatedData["detail"]:$relatedData["model"];
            $structureFieldData["tablename"] = $tableName;
            $structureFieldData["language_tag"] = "";
            $structureFieldData["flag_form_builder"] = "1";
            $structureFieldData["flag_test_mode"] = "1";
            $structureFieldData["flag_copy_from_form_builder"] = "1";
            $structureFieldData["default"] = "";
            $structureFieldData["setting"] = (isset($structureFieldData["setting"])) ? $structureFieldData["setting"] : "";

            $svdId = $structureFieldData["structure_value_domain"];
            if ($structureFieldData["type"] == "select"){
                if (empty($svdId)){
                    $testSpvcc = $structurePermissibleValuesCustomControlModelInstance->find("first", array('conditions'=>array(
                        'name' => $structureFieldData["structure_value_domain_value"]
                    )));
                    if (empty($testSpvcc)){
                        $spvccData = array(
                            'name' => $structureFieldData["structure_value_domain_value"],
                            'flag_active' => 1,
                            'values_max_length' => 255,
                            'category' => $this->structureAlias,
                            'values_used_as_input_counter' => 0,
                            'values_counter' => 0
                        );

                        $spvccData = array("StructurePermissibleValuesCustomControl" => $spvccData);
                        $structurePermissibleValuesCustomControlModelInstance->addWritableField(array("name", "flag_active", "values_max_length", "category", "values_used_as_input_counter", "values_counter"));
                        $structurePermissibleValuesCustomControlModelInstance->id = null;
                        $saveResult = $structurePermissibleValuesCustomControlModelInstance->save($spvccData);
                        if (!$saveResult){
                            $this->normalisedErrorMessages([
                                'validationErrors' => $modelInstance->validationErrors,
                                'line' => __LINE__,
                                'class' => __CLASS__,
                                'message' => "error in saving %s (%s, %s) %s",
                                'model' => $structurePermissibleValuesCustomControlModelInstance->name,
                            ]);
                        }
                    }
                }
                $svdData = array(
                    'domain_name' => $this->structureAlias."_".$structureFieldData["structure_value_domain_value"]."_".$index,
                    'category' => $this->structureAlias,
                    'source' => "StructurePermissibleValuesCustom::getCustomDropdown('".$structureFieldData["structure_value_domain_value"]."')"
                );

                $svdData = array("StructureValueDomain" => $svdData);
                $structureValueDomainModelInstance->addWritableField(array("domain_name", "category", "source"));
                $structureValueDomainModelInstance->id = null;
                $saveResult = $structureValueDomainModelInstance->save($svdData);

                if (!$saveResult){
                    $this->normalisedErrorMessages([
                        'validationErrors' => $modelInstance->validationErrors,
                        'line' => __LINE__,
                        'class' => __CLASS__,
                        'message' => "error in saving %s (%s, %s) %s",
                        'model' => $structureValueDomainModelInstance->name
                    ]);
                }else{
                    $svdId = $saveResult['StructureValueDomain']['id'];
                }
            }
            $structureFieldModelInstance->id = null;
            $structureFieldData["structure_value_domain"] = $svdId;
            unset($structureFieldData['id']);
            $structureFieldData = array("StructureField" => $structureFieldData, "FunctionManagement" => $functionManagementData);
            $structureFieldModelInstance->data = $structureFieldData;
            $structureFieldModelInstance->addWritableField();

            $this->addToI18n($structureFieldData ['StructureField']['language_label']);
            $this->addToI18n($structureFieldData ['StructureField']['language_help']);

            $structureFieldModelInstance->id = null;
            $saveResult = $structureFieldModelInstance->save($structureFieldData, true);

            if (!$saveResult){
                $this->normalisedErrorMessages([
                    'validationErrors' => $modelInstance->validationErrors,
                    'line' => __LINE__,
                    'class' => __CLASS__,
                    'message' => "error in saving %s (%s, %s) %s",
                    'model' => $structureFieldModelInstance->name
                ]);
           }

            $sfid[$index] = $saveResult["StructureField"]["id"];
            $structureFormatData = $value["StructureFormat"];
            unset($structureFormatData["id"]);
            $structureFormatData["structure_id"] = $structureId;
            $structureFormatData["structure_field_id"] = $sfid[$index];
            $structureFormatData["language_label"] = "";
            $structureFormatData["language_tag"] = "";
            $structureFormatData["language_help"] = "";
            $structureFormatData["flag_addgrid"]=$structureFormatData["flag_add"];
            $structureFormatData["flag_editgrid"]=$structureFormatData["flag_edit"];
            $structureFormatData = array("StructureFormat" => $structureFormatData, "FunctionManagement" => $functionManagementData);
            $structureFormatModelInstance->data = $structureFormatData;
            $structureFormatModelInstance->addWritableField(array("structure_id", "structure_field_id", "language_label", "language_tag", "language_help", "setting"));

            $this->addToI18n($structureFormatData ['StructureFormat']['language_heading']);

            $structureFormatModelInstance->id = null;
            $saveResult = $structureFormatModelInstance->save($structureFormatData);

            if (!$saveResult){
                $this->normalisedErrorMessages([
                    'validationErrors' => $modelInstance->validationErrors,
                    'line' => __LINE__,
                    'class' => __CLASS__,
                    'message' => "error in saving %s (%s, %s) %s",
                    'model' => $structureFormatModelInstance->name
                ]);
            }
            $index++;
        }

        foreach ($structureValidationData as $key => $value) {
            $value["structure_field_id"] = $sfid[$value["structure_field_id"]];
            $value = array("StructureValidation" => $value);
            $this->addToI18n($value['StructureValidation']['language_message']);
            $structureValidationModelInstance->addWritableField(array("structure_field_id", "rule", "on_action", "language_message"));
            $structureValidationModelInstance->id = null;
            $structureValidationModelInstance->save($value);
        }

        $this->saveI18n();
    }

    private function diffNewOldData($newData, $data)
    {
        $status = array(
            "add" => array(),
            "update" => array(),
            "delete" => array()
        );
        $response = array(
            'masterTest' => $status,
            'detailTest' => $status,
            'fbDeletable' => $status,
            "i18n" => array()
        );
        $data["StructureValidation"] = json_decode($data["validationData"], 1);
        $data["StructureValueDomainData"] = json_decode($data["valueDomainData"], 1);
        unset ($data["masterProd"]);
        unset ($data["detailProd"]);
        $index = 0;
        $indexOld = 0;


        foreach (array("masterTest", "detailTest", "fbDeletable") as $key){
            if (!isset($newData[$key])){
                $newData[$key] = array();
            }
            if (!isset($data[$key])){
                $data[$key] = array();
            }
            $prefix = getPrefix();
            if ($key == 'detailTest' || $key == 'fbDeletable'){
                if (isset($newData[$newData['models']['main']['model']]['detail_form_alias']) && !empty($newData[$newData['models']['main']['model']]['detail_form_alias'])){
                    $prefix = $newData[$newData['models']['main']['model']]['detail_form_alias'] . " || ";
                }else{
                    $prefix = $prefix . $newData['others']['FormBuilder']['default_alias'] . " || ";
                }
            }else{
                $prefix = $prefix . $newData['others']['FormBuilder']['default_alias'] . " || ";
            }
            foreach ($newData[$key] as $line){
                if (empty($line["StructureField"]["id"]) && empty($line["StructureFormat"]["id"])){
                    $i = countCustom($response[$key]["add"]);
                    $response[$key]["add"][$i] = $line;
                    $response[$key]["add"][$i]["StructureValueDomain"] = $line["StructureField"]["structure_value_domain"];
                    $response[$key]["add"][$i]["StructureValidation"] = array();
                    for ($counter = 0; $counter < countCustom($newData["StructureValidation"]); $counter++){
                        if ($newData["StructureValidation"][$counter]["structure_field_id"] == $index){
                            $response[$key]["add"][$i]["StructureValidation"][] = $newData["StructureValidation"][$counter];
                            $this->addToI18n($response[$key]["add"][$i]["StructureValidation"][countCustom($response[$key]["add"][$i]["StructureValidation"])-1]['language_message'], $prefix);
                        }
                    }
                }else{
                    $i = countCustom($response[$key]["update"]);
                    $response[$key]["update"][$i] = $line;

                    foreach ($data[$key] as $tempLine){
                        if ($line["StructureField"]["id"] == $tempLine["StructureField"]["id"]){
                            $response[$key]["update"][$i]["StructureField"]["structure_value_domain_old"] = $tempLine["StructureField"]["structure_value_domain"];
                            break;
                        }
                    }

                    $response[$key]["update"][$i]["StructureValidation"] = array();
                    for ($counter = 0; $counter < countCustom($newData["StructureValidation"]); $counter++){
                        if ($newData["StructureValidation"][$counter]["structure_field_id"] == $index){
                            $response[$key]["update"][$i]["StructureValidation"][] = $newData["StructureValidation"][$counter];
                            $this->addToI18n($response[$key]["update"][$i]["StructureValidation"][countCustom($response[$key]["update"][$i]["StructureValidation"])-1]['language_message'], $prefix);
                        }
                    }
                }
                $index++;
            }

            foreach ($data[$key] as $oldLine){
                $exists = false;
                foreach ($newData[$key] as $line){
                    if ($oldLine["StructureField"]["id"] == $line["StructureField"]["id"]){
                        $exists = true;
                        break;
                    }
                }
                if (!$exists){
                    $i = countCustom($response[$key]["delete"]);
                    $response[$key]["delete"][$i] = $oldLine;
                    if ($key != "fbDeletable"){
                        $response[$key]["delete"][$i]["StructureValueDomain"] = $data["StructureValueDomainData"][$indexOld];
                        $response[$key]["delete"][$i]["StructureValidation"] = $data["StructureValidation"][$indexOld];
                    }

                    $this->prefixes = $this->prefixes + array($prefix);
                }
                $indexOld++;
            }
        }

        foreach (array("masterTest", "detailTest", "fbDeletable") as $key) {
            $prefix = getPrefix();
            $prefix = (in_array($key, array('detailTest', 'fbDeletable'))!==false) ? "" : $prefix . $newData['others']['FormBuilder']['default_alias'] . " || ";
            foreach (array("update", "add") as $k) {
                foreach ($response[$key][$k] as &$field) {
                        $this->addToI18n($field['StructureField']['language_tag'], $prefix);
                        $this->addToI18n($field['StructureField']['language_label'], $prefix);
                        $this->addToI18n($field['StructureField']['language_help'], $prefix);
                        $this->addToI18n($field['StructureFormat']['language_heading'], $prefix);
                }
            }
        }
        return $response;
    }

    private function updateI18n()
    {
        $i18nModel = new Model(array(
            'table' => 'i18n',
            'name' => 0
        ));

        $pageId = ($this->modelFB == 'MiscIdentifierControl') ? "MiscIdentifier" : "";
        $currentI18n = array();
        foreach ($this->prefixes as $prefix){
            $currentI18n [] = $i18nModel->find('list', array(
                'conditions' => array(
                    'id like ' => "$prefix%",
                    'page_id' => $pageId
            )));
        }
        $currentI18nIds = (empty($currentI18n))?array():array_keys($currentI18n[0]);
        $newI18nIds = (empty($this->i18n))?array():array_values(array_map(function ($a) { return $a['id']; }, $this->i18n));
        $idsToDelete = array_diff($currentI18nIds,$newI18nIds);
        $idsToAdd = array_diff($newI18nIds, $currentI18nIds);
        foreach ($this->i18n as $key=>$value) {
            if (in_array($value['id'], $idsToAdd)===false){
                unset($this->i18n[$key]);
            }
        }
        foreach ($this->prefixes as $prefix){
            $i18nModel->deleteAll(array(
                'id' => $idsToDelete,
                'page_id' => $pageId
            ));
        }
        $this->saveI18n();

    }

    private function updateTestMasterAndDetail($diffNewOldData, $relatedData, $models)
    {
        $modelInstance = AppModel::getInstance($models["plugin"], $models["model"]);

        $structureModelInstance = AppModel::getInstance('', 'Structure');
        $structureFieldModelInstance = AppModel::getInstance("", "StructureField");
        $structureFormatModelInstance = AppModel::getInstance("", "StructureFormat");
        $structureValidationModelInstance = AppModel::getInstance("", "StructureValidation");
        $structureValueDomainModelInstance = AppModel::getInstance("", "StructureValueDomain");
        $structurePermissibleValuesCustomControlModelInstance = AppModel::getInstance("", "StructurePermissibleValuesCustomControl");
        $structureValueDomainsPermissibleValueModelInstance = AppModel::getInstance("", "structureValueDomainsPermissibleValue");

        $structureMasterId = $structureModelInstance->find("first", array("conditions" => array(
            "alias" => $this->structureMasterAlias
        )));
        $structureMasterId = $structureMasterId["structure"]["Structure"]["id"];

        $structureId = $structureModelInstance->find("first", array("conditions" => array(
            "alias" => $this->structureAlias
        )));

        if (!empty($structureId['structure'])){
            $structureId = $structureId["structure"]["Structure"]["id"];
        }elseif (!empty($this->structureAlias)){
            $structureModelInstance->addWritableField(array("alias", "description"));
            $saveResult = $structureModelInstance->save(array('Structure' => array(
                'alias' => $this->structureAlias,
                'description' => $this->structureAlias
            )));
            $structureId = $saveResult['Structure']['id'];
        }

        if (isset($diffNewOldData['fbDeletable']) && is_array($diffNewOldData['fbDeletable'])){
            $data = $diffNewOldData['fbDeletable'];

            foreach ($data['delete'] as $val){
                $structureFormatModelInstance->delete($val['StructureFormat']['id']);
                $structureValidationModelInstance->deleteAll(array('StructureValidation.structure_field_id' => $val['StructureField']['id']));
                $structureFieldModelInstance->delete($val['StructureField']['id']);
                if (isset($val['StructureField']['structure_value_domain']) && !empty($val['StructureField']['structure_value_domain'])){
                    $svdId = $val['StructureField']['structure_value_domain'];
                    $svdData = $structureValueDomainModelInstance->find("first", array(
                        'conditions' => array(
                            'StructureValueDomain.id' => $svdId
                        ),
                        'callbacks' => false
                    ));

                    if (strpos($svdData['StructureValueDomain']['source'], "StructurePermissibleValuesCustom::getCustomDropdown('") !== false) {
                        $structureValueDomainModelInstance->delete($svdId, false);
                    }
                }
            }

        }

        foreach (array("detailTest", "masterTest") as $md){
            $data = $diffNewOldData[$md];
            foreach ($data['delete'] as $val){
                $structureFormatModelInstance->delete($val['StructureFormat']['id']);
                $structureValidationModelInstance->deleteAll(array('StructureValidation.structure_field_id' => $val['StructureField']['id']));
                $structureFieldModelInstance->delete($val['StructureField']['id']);
                if (isset($val['StructureValueDomain']['id']) && !empty($val['StructureValueDomain']['id'])){
                    $structureValueDomainModelInstance->delete($val['StructureValueDomain']['id'], false);
                }
            }

            foreach (array('update', 'add') as $action) {
                foreach ($data[$action] as $field) {
                    $structureFieldData = $field['StructureField'];
                    $structureFormatData = $field['StructureFormat'];
                    $structureValidationData = $field['StructureValidation'];
                    $functionManagementData = $field['FunctionManagement'];
                    $svdId = $structureFieldData["structure_value_domain"];
                    $svdOldId = (isset($structureFieldData["structure_value_domain_old"])) ? $structureFieldData["structure_value_domain_old"] : "";

                    $addSVDCondition = ($action != 'update' || $svdOldId != $svdId || (empty($svdId) && empty($svdOldId)));
                    if ($structureFieldData['type'] == 'select') {
                        if ($addSVDCondition) {
                            if (empty($structureFieldData['structure_value_domain'])) {
                                $testSpvcc = $structurePermissibleValuesCustomControlModelInstance->find("first", array('conditions'=>array(
                                    'name' => $structureFieldData["structure_value_domain_value"]
                                )));
                                if (empty($testSpvcc)){
                                    $spvccData = array(
                                        'name' => $structureFieldData["structure_value_domain_value"],
                                        'flag_active' => 1,
                                        'values_max_length' => 255,
                                        'category' => ($md == 'detailTest') ? $this->structureAlias : $this->structureMasterAlias,
                                        'values_used_as_input_counter' => 0,
                                        'values_counter' => 0
                                    );

                                    $spvccData = array("StructurePermissibleValuesCustomControl" => $spvccData);
                                    $structurePermissibleValuesCustomControlModelInstance->addWritableField(array("name", "flag_active", "values_max_length", "category", "values_used_as_input_counter", "values_counter"));
                                    $structurePermissibleValuesCustomControlModelInstance->id = null;
                                    $saveResult = $structurePermissibleValuesCustomControlModelInstance->save($spvccData);
                                    if (!$saveResult) {
                                        $this->normalisedErrorMessages([
                                            'validationErrors' => $modelInstance->validationErrors,
                                            'line' => __LINE__,
                                            'class' => __CLASS__,
                                            'message' => "error in saving %s (%s, %s) %s",
                                            'model' => $structurePermissibleValuesCustomControlModelInstance->name
                                        ]);
                                    }
                                }

                            }
                            $svdData = array(
                                'domain_name' => (($md == 'detailTest') ? $this->structureAlias : $this->structureMasterAlias) . "_" . $structureFieldData["structure_value_domain_value"] . "_" . rand(20, 100000),
                                'category' => ($md == 'detailTest') ? $this->structureAlias : $this->structureMasterAlias,
                                'source' => "StructurePermissibleValuesCustom::getCustomDropdown('" . $structureFieldData["structure_value_domain_value"] . "')"
                            );
                            $svdData = array("StructureValueDomain" => $svdData);
                            $structureValueDomainModelInstance->addWritableField(array("domain_name", "category", "source"));
                            $structureValueDomainModelInstance->id = null;

                            $saveResult = $structureValueDomainModelInstance->save($svdData);

                            if (!$saveResult) {
                                $this->normalisedErrorMessages([
                                    'validationErrors' => $modelInstance->validationErrors,
                                    'line' => __LINE__,
                                    'class' => __CLASS__,
                                    'message' => "error in saving %s (%s, %s) %s",
                                    'model' => $structureValueDomainModelInstance->name
                                ]);
                            } else {
                                $svdId = $saveResult['StructureValueDomain']['id'];
                            }
                        }
                    }

                    if ($action == 'update'){
                        $structureFieldModelInstance->id =$structureFieldData['id'];
                    }else{
                        unset($structureFieldData['id']);
                        $structureFieldModelInstance->id = null;
                    }

                    $structureFieldData["structure_value_domain"] = $svdId;
                    $structureFieldData["plugin"] = $relatedData["plugin"];
                    if (empty($relatedData["detail"])){
                        $structureFieldData["model"] = $relatedData["model"];
                    }elseif($md == 'detailTest'){
                        $structureFieldData["model"] = $relatedData["detail"];
                    }else{
                        $structureFieldData["model"] = $relatedData["master"];
                    }

                    $structureFieldData["tablename"] = ($md == 'detailTest') ? $relatedData["control"]["detail_tablename"] : $relatedData["master_table"];
                    $structureFieldData["language_tag"] = "";
                    $structureFieldData["flag_form_builder"] = "1";
                    $structureFieldData["flag_test_mode"] = "1";
                    $structureFieldData["flag_copy_from_form_builder"] = 1;

                    $structureFieldData["default"] = "";
                    $structureFieldData["setting"] = (isset($structureFieldData["setting"])) ? $structureFieldData["setting"] : "";

                    $structureFieldData = array("StructureField" => $structureFieldData, "FunctionManagement" => $functionManagementData);

                    $structureFieldModelInstance->addWritableField();
                    $structureFieldModelInstance->data = $structureFieldData;
                    $saveResult = $structureFieldModelInstance->save($structureFieldData, true);
                    if (!$saveResult) {
                        $this->normalisedErrorMessages([
                            'validationErrors' => $modelInstance->validationErrors,
                            'line' => __LINE__,
                            'class' => __CLASS__,
                            'message' => "error in saving %s (%s, %s) %s",
                            'model' => $structureFieldModelInstance->name
                        ]);
                    }
                    if ($addSVDCondition){
                        $structureValueDomainModelInstance->delete($svdOldId, false);
                    }

                    $structureFieldData['id'] = $saveResult["StructureField"]["id"];

                    $structureFormatData["structure_id"] = ($md == 'detailTest') ? $structureId : $structureMasterId;
                    $structureFormatData["structure_field_id"] = $structureFieldData['id'];
                    $structureFormatData["language_label"] = "";
                    $structureFormatData["language_tag"] = "";
                    $structureFormatData["language_help"] = "";
                    $structureFormatData["flag_addgrid"]=$structureFormatData["flag_add"];
                    $structureFormatData["flag_editgrid"]=$structureFormatData["flag_edit"];
                    if ($action == 'update'){
                        $structureFormatModelInstance->id = $structureFormatData['id'];
                    }else{
                        $structureFormatModelInstance->id = null;
                        unset($structureFormatData['id']);
                    }
                    $structureFormatData = array("StructureFormat" => $structureFormatData, "FunctionManagement" => $functionManagementData);
                    $structureFormatModelInstance->data = $structureFormatData;
                    $structureFormatModelInstance->addWritableField(array("structure_id", "structure_field_id", "language_label", "language_tag", "language_help", "setting"));

                    $saveResult = $structureFormatModelInstance->save($structureFormatData);

                    if (!$saveResult) {
                        $this->normalisedErrorMessages([
                            'validationErrors' => $modelInstance->validationErrors,
                            'line' => __LINE__,
                            'class' => __CLASS__,
                            'message' => "error in saving %s (%s, %s) %s",
                            'model' => $structureFormatModelInstance->name
                        ]);
                    }

                    $structureValidationModelInstance->deleteAll(array('StructureValidation.structure_field_id' => $structureFieldData['id']));
                    foreach ($structureValidationData as $value) {
                        $value["structure_field_id"] = $structureFieldData['id'];
                        $value = array("StructureValidation" => $value);
                        $structureValidationModelInstance->addWritableField(array("structure_field_id", "rule", "on_action", "language_message"));
                        $structureValidationModelInstance->id = null;
                        $structureValidationModelInstance->save($value);
                    }

                }
            }
        }

    }

    private function modifyTheMD($diffNewOldData, $relatedData, $models)
    {
        $this->updateI18n();
        $this->updateTestMasterAndDetail($diffNewOldData, $relatedData, $models);

    }

    public function normalisedAndEdit($newData, $data , $validate = false, $fieldList = array())
    {
        $models = $newData["models"]["main"];
        $master = $models["master"];
        $modelInstance = AppModel::getInstance($models["plugin"], $models["model"]);
        $relatedData = $newData["others"]['FormBuilder'];
        if (empty($master)){
            $this->structureAlias = $relatedData['default_alias'];
        }elseif($master == 'MiscIdentifier'){
            $this->structureAlias = getPrefix() . $relatedData['default_alias'];
            $modelData = $newData[$models["model"]];
            $relatedData["control"] = $newData[$relatedData["model"]];
        }else{
            $modelData = $newData[$models["model"]];
            $relatedData["control"] = $newData[$relatedData["model"]];
            $this->structureAlias = $newData[$models['model']]['detail_form_alias'];
        }

        $this->structureMasterAlias = $relatedData["default_alias"];

        $diffNewOldData = $this->diffNewOldData($newData, $data);
        if (!empty($master)){
            $tempModelData = $newData[$models['model']];
            if ($tempModelData['flag_test_mode']){
                $this->addToI18n($tempModelData[$modelInstance->controlType]);
                $modelData[$modelInstance->controlType]=$tempModelData[$modelInstance->controlType];
                $modelInstance->id = $modelData['id'];
                $modelInstance->addWritableField();
                unset($modelData['form_alias']);
                unset($modelData[$modelInstance->controlType.'_id']);
                $modelInstance->save($modelData);
            }elseif (isset($tempModelData['detail_old_form_alias']) && !empty($tempModelData['detail_old_form_alias'])){
                $tempModelData["detail_form_alias"] = implode(",", array_merge(explode(",", $tempModelData["detail_old_form_alias"]), array($tempModelData['detail_form_alias'])));

                $modelInstance->id = $tempModelData['id'];
                unset($tempModelData["form_alias"]);
                unset($tempModelData["detail_old_form_alias"]);
                $modelInstance->addWritableField();
                if (empty($tempModelData['flag_form_builder'])){
                    $tempModelData[$modelInstance->controlType] = $tempModelData[$modelInstance->controlType.'_id'];
                }
                unset($tempModelData[$modelInstance->controlType.'_id']);
                $modelInstance->save($tempModelData);
            }elseif (empty($data['control'][$models['model']]['detail_form_alias']) && !empty($tempModelData["detail_form_alias"])){
                $modelInstance->id = $tempModelData['id'];
                $modelInstance->addWritableField();
                $modelInstance->save($tempModelData);
            }
        }

        if ($master == 'MiscIdentifier'){
            $this->updateI18n();
            return;
        }
        $this->modifyTheMD($diffNewOldData, $relatedData, $models);
    }

    private function saveI18n($data = array())
    {
        $i18nModel = new Model(array(
            'table' => 'i18n',
            'name' => 0
        ));

        if (empty($data) || !array($data)) {
            $data = $this->i18n;
        }

        foreach ($data as $word) {
            $i18nModel->id = null;
            $i18nModel->save(array(
                'id' => $word["id"],
                'en' => $word["en"],
                'fr' => $word["fr"],
                'page_id' => !empty($word["page_id"])?$word["page_id"]:""
            ));
        }
    }

    public function addFieldsToTable($data)
    {
        $model = $data["models"]["main"]["model"];
        $master = $data["models"]["main"]["master"];

        $detailTableName = !empty($master)?$data['control'][$model]["detail_tablename"]:"";

        $relatedData = $data["others"]['FormBuilder'];
        $refrenceTable = $relatedData["master_table"];

        $queryMaster = "ALTER TABLE `{$refrenceTable}` \n\t";
        $queryMasterRev = "ALTER TABLE `{$refrenceTable}_revs` \n\t";

        $queryDetail = "ALTER TABLE `{$detailTableName}` \n\t";
        $queryDetailRev = "ALTER TABLE `{$detailTableName}_revs` \n\t";

        $fk = Inflector::underscore($relatedData["master"]) ."_id";


        $currentFieldQueryDetail = array();
        $currentFieldQueryMaster = array();
        foreach (array('detailTest', 'masterTest') as $key) {
            foreach ($data[$key] as $field){
                $fieldName = $field['StructureField']['field'];
                $type = $field['StructureField']['type'];
                if ($type == 'checkbox'){
                    $tempQuery = "ADD " . "`$fieldName` tinyint";
                }elseif ($type=='date'){
                    $tempQuery = "ADD " . "`$fieldName` date ," . "ADD " . $fieldName . "_accuracy char(1) NOT NULL DEFAULT ''";
                }elseif ($type=='datetime'){
                    $tempQuery = "ADD " . "`$fieldName` datetime,\n\t";
                    $tempQuery .= "ADD " . $fieldName . "_accuracy char(1) NOT NULL DEFAULT ''";
                }elseif ($type=='float1'){
                    $tempQuery = "ADD " . "`$fieldName` decimal(11, 1)";
                }elseif ($type=='float2'){
                    $tempQuery = "ADD " . "`$fieldName` decimal(12, 2)";
                }elseif ($type=='float5'){
                    $tempQuery = "ADD " . "`$fieldName` decimal(15, 5)";
                }elseif ($type=='input'){
                    $tempQuery = "ADD " . "`$fieldName` varchar(1000) COLLATE 'latin1_swedish_ci' ";
                }elseif ($type=='input_short'){
                    $tempQuery = "ADD " . "`$fieldName` varchar(50) COLLATE 'latin1_swedish_ci' ";
                }elseif ($type=='input_medium'){
                    $tempQuery = "ADD " . "`$fieldName` varchar(200) COLLATE 'latin1_swedish_ci' ";
                }elseif ($type=='integer'){
                    $tempQuery = "ADD " . "`$fieldName` int";
                }elseif ($type=='select'){
                    $tempQuery = "ADD " . "`$fieldName` varchar(255) COLLATE 'latin1_swedish_ci' ";
                }elseif ($type=='multi-select'){
                    $tempQuery = "ADD " . "`$fieldName` varchar(2000) COLLATE 'latin1_swedish_ci' ";
                }elseif ($type=='textarea'){
                    $tempQuery = "ADD " . "`$fieldName` text";
                }elseif ($type=='time'){
                    $tempQuery = "ADD " . "`$fieldName` time";
                }elseif ($type=='yes_no' || $type=='y_n_u'){
                    $tempQuery = "ADD " . "`$fieldName` char(1)";
                }elseif (in_array($type, array("validateIcd10WhoCode", "validateIcdo3MorphoCode", "validateIcdo3TopoCode"))!==false){
                    $tempQuery = "ADD " . "`$fieldName` varchar(1000) COLLATE 'latin1_swedish_ci' ";
                }
                if ($key == 'detailTest'){
                    $currentFieldQueryDetail[] = $tempQuery;
                }else{
                    $currentFieldQueryMaster[] = $tempQuery;
                }
            }
        }

        $queryDetail .= implode(" ,\n\t", $currentFieldQueryDetail) . " ;\n\t";
        $queryDetailRev .= implode(" ,\n\t", $currentFieldQueryDetail) . " ;\n\t";

        $queryMaster .= implode(" ,\n\t", $currentFieldQueryMaster) . " ;\n\t";
        $queryMasterRev .= implode(" ,\n\t", $currentFieldQueryMaster) . " ;\n\t";

        if (!empty($data['detailTest'])){
            $this->query($queryDetail);
            $this->query($queryDetailRev);
        }
        if (!empty($data['masterTest'])){
            $this->query($queryMaster);
            $this->query($queryMasterRev);
        }
    }

    public function createTable($data)
    {

        $model = $data["models"]["main"]["model"];
        $detailTableName =  (isset($data['control'][$model]["detail_tablename"]))?$data['control'][$model]["detail_tablename"]:"";
        $masterTableName =  $data['others']['FormBuilder']["master_table"];
        $relatedData = $data["others"]['FormBuilder'];

        $refrenceTable = $relatedData["master_table"];
        $fk = Inflector::underscore($relatedData["master"]) ."_id";

        if (!empty($detailTableName)){

            $query = "CREATE TABLE IF NOT EXISTS `{$detailTableName}` (\n\t"
                . "`{$fk}` int NOT NULL PRIMARY KEY, \n\t";

            $queryRevs = "CREATE TABLE IF NOT EXISTS `{$detailTableName}_revs` (\n\t"
                . "`{$fk}` int NOT NULL, \n\t";

            $index = 0;
            $currentFieldQuery = array();
            foreach (array('fbDeletable', 'detailTest') as $key){
                if (isset($data[$key]) && !empty($data[$key])){
                    foreach ($data[$key] as $field){
                        $fieldName = $field['StructureField']['field'];
                        $type = $field['StructureField']['type'];
                        if ($type == 'checkbox'){
                            $currentFieldQuery[] = "`$fieldName` tinyint";
                        }elseif ($type=='date'){
                            $currentFieldQuery[] = "`$fieldName` date ,$fieldName" . "_accuracy char(1) NOT NULL DEFAULT ''";
                        }elseif ($type=='datetime'){
                            $currentFieldQuery[] = "`$fieldName` datetime";
                            $currentFieldQuery[] = $fieldName . "_accuracy char(1) NOT NULL DEFAULT ''";
                        }elseif ($type=='file'){
                            $currentFieldQuery[] = "`$fieldName` varchar(255) COLLATE 'latin1_swedish_ci' ";
                        }elseif ($type=='float1'){
                            $currentFieldQuery[] = "`$fieldName` decimal(11, 1)";
                        }elseif ($type=='float2'){
                            $currentFieldQuery[] = "`$fieldName` decimal(12, 2)";
                        }elseif ($type=='float'){
                            $currentFieldQuery[] = "`$fieldName` decimal(15, 5)";
                        }elseif ($type=='float5'){
                            $currentFieldQuery[] = "`$fieldName` decimal(15, 5)";
                        }elseif ($type=='float_positive'){
                            $currentFieldQuery[] = "`$fieldName` decimal(15, 5)";
                        }elseif ($type=='input'){
                            $currentFieldQuery[] = "`$fieldName` varchar(1000) COLLATE 'latin1_swedish_ci' ";
                        }elseif ($type=='input_medium'){
                            $currentFieldQuery[] = "`$fieldName` varchar(200) COLLATE 'latin1_swedish_ci' ";
                        }elseif ($type=='input_short'){
                            $currentFieldQuery[] = "`$fieldName` varchar(50) COLLATE 'latin1_swedish_ci' ";
                        }elseif ($type=='integer'){
                            $currentFieldQuery[] = "`$fieldName` int";
                        }elseif ($type=='integer_positive'){
                            $currentFieldQuery[] = "`$fieldName` bigint";
                        }elseif ($type=='select'){
                            $currentFieldQuery[] = "`$fieldName` varchar(255) COLLATE 'latin1_swedish_ci' ";
                        }elseif ($type=='multi-select'){
                            $currentFieldQuery[] = "`$fieldName` varchar(2000) COLLATE 'latin1_swedish_ci' ";
                        }elseif ($type=='textarea'){
                            $currentFieldQuery[] = "`$fieldName` text";
                        }elseif ($type=='time'){
                            $currentFieldQuery[] = "`$fieldName` time";
                        }elseif ($type=='yes_no' || $type=='y_n_u'){
                            $currentFieldQuery[] = "`$fieldName` char(1)";
                        }elseif (in_array($type, array("validateIcd10WhoCode", "validateIcdo3MorphoCode", "validateIcdo3TopoCode"))!==false){
                            $currentFieldQuery[] = "`$fieldName` varchar(1000) COLLATE 'latin1_swedish_ci' ";
                        }
                        $index ++;
                    }
                }

            }

            if (!empty($currentFieldQuery)){
                $query .= implode(" ,\n\t", $currentFieldQuery) . " ,\n\t";

                $queryRevs .= implode(" ,\n\t", $currentFieldQuery) . " ,\n\t";
            }

            $query .= "FOREIGN KEY (`{$fk}`) REFERENCES `{$refrenceTable}` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT \n";

            $query .= ") ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';\n\n";

            $queryRevs .= "version_id int NOT NULL PRIMARY KEY AUTO_INCREMENT, \n\t"
                ."version_created datetime NOT NULL \n\t"
                .") ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';\n\n";
            try {
                $this->query($query);
                $this->query($queryRevs);
            } catch (Exception $ex){
                if (strpos($ex->getMessage(), "SQLSTATE[42000]") !== false){
                    AppController::getInstance()->atimFlashError (__('row size is too large, you need to decrease the column size by changing the type'), "javascript::back()");
                }
            }
        }

        if (!empty($data['masterTest'])){
            $query = "ALTER TABLE `{$masterTableName}` \n\t";
            $queryRevs = "ALTER TABLE `{$masterTableName}_revs` \n\t";

            $index = 0;
            $currentFieldQuery = array();

            foreach ($data['masterTest'] as $field){
                $fieldName = $field['StructureField']['field'];
                $type = $field['StructureField']['type'];
                if ($type == 'checkbox'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` tinyint";
                }elseif ($type=='date'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` date ," . "ADD " . $fieldName . "_accuracy char(1) NOT NULL DEFAULT ''";
                }elseif ($type=='datetime'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` datetime";
                    $currentFieldQuery[] = "ADD " . $fieldName . "_accuracy char(1) NOT NULL DEFAULT ''";
                }elseif ($type=='float1'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` decimal(11, 1)";
                }elseif ($type=='float2'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` decimal(12, 2)";
                }elseif ($type=='float5'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` decimal(15, 5)";
                }elseif ($type=='input'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` varchar(1000) COLLATE 'latin1_swedish_ci' ";
                }elseif ($type=='input_medium'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` varchar(200) COLLATE 'latin1_swedish_ci' ";
                }elseif ($type=='input_short'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` varchar(50) COLLATE 'latin1_swedish_ci' ";
                }elseif ($type=='integer'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` int";
                }elseif ($type=='select'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` varchar(255) COLLATE 'latin1_swedish_ci' ";
                }elseif ($type=='multi-select'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` varchar(2000) COLLATE 'latin1_swedish_ci' ";
                }elseif ($type=='textarea'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` text";
                }elseif ($type=='time'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` time";
                }elseif ($type=='yes_no' || $type=='y_n_u'){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` char(1)";
                }elseif (in_array($type, array("validateIcd10WhoCode", "validateIcdo3MorphoCode", "validateIcdo3TopoCode"))!==false){
                    $currentFieldQuery[] = "ADD " . "`$fieldName` varchar(1000) COLLATE 'latin1_swedish_ci' ";
                }
                $index ++;
            }


            $query .= implode(" ,\n\t", $currentFieldQuery) . " ;\n\t";
            $queryRevs .= implode(" ,\n\t", $currentFieldQuery) . " ;\n\t";

            try {
                $this->query($query);
                $this->query($queryRevs);
            } catch (Exception $ex){
                if (strpos($ex->getMessage(), "SQLSTATE[42000]") !== false){
                    AppController::getInstance()->atimFlashError (__('row size is too large, you need to decrease the column size by changing the type'), "javascript::back()");
                }
            }

        }


    }

    public function copy($data)
    {
        $this->saveCopyControl($data);

        $this->saveCopyStructure($data);

        $this->saveCopyFieldFormat($data);

        $this->saveI18n();

        return $data['control']['id'];
    }

    private function saveCopyControl(&$data)
    {
        foreach ($data['detailTest'] as $key => $value) {
            $data['detailTest'][$key]['StructureField']['language_label'] = ___($data['detailTest'][$key]['StructureField']['language_label']);
            $data['detailTest'][$key]['StructureField']['language_help'] = ___($data['detailTest'][$key]['StructureField']['language_help']);
            $data['detailTest'][$key]['StructureFormat']['language_heading'] = ___($data['detailTest'][$key]['StructureFormat']['language_heading']);
            if (!empty($data['detailTest'][$key]['StructureField']['language_tag'])){
                $data['detailTest'][$key]['StructureField']['language_tag'] = ___($data['detailTest'][$key]['StructureField']['language_tag']);
            }
        }

        $model = $data["FormBuilder"]['model'];
        $plugin = $data["FormBuilder"]['plugin'];
        $master = $data["FormBuilder"]["master"];

        $modelInstance = AppModel::getInstance($plugin, $model);
        $modelName = $modelInstance->getUniqueName($data['control'][$modelInstance->controlType]);

        
        $modelData = array($modelInstance->name => $data['control']);


        unset($modelData[$modelInstance->name]['id']);
        $modelData[$modelInstance->name][$modelInstance->controlType] = $modelName;
        $modelData[$modelInstance->name]['flag_form_builder'] = 1;
        $modelData[$modelInstance->name]['flag_active'] = 0;
        $modelData[$modelInstance->name]['flag_active_input'] = 1;
        unset($modelData[$modelInstance->name]['form_alias']);

        $modelInstance->setDataBeforeSaveFB($modelData);

        $modelInstance->id = null;
        $modelInstance->data = $modelData;
        $this->structureAlias = $modelData[$modelInstance->name]['detail_form_alias'];
        $this->addToI18n($modelData[$modelInstance->name][$modelInstance->controlType]);

        $modelInstance->addWritableField();
        $modelSavedData = $modelInstance->save($modelData);

        reset($modelData);
        $data['control'] = current($modelData);

        reset($modelSavedData);
        $data['control']['id'] = current($modelSavedData)['id'];

    }

    private function saveCopyStructure(&$data)
    {
        $structureModelInstance = AppModel::getInstance('', 'Structure');

        $structureData = array('Structure' => array(
            'alias' => $this->structureAlias,
            'description' => $this->structureAlias
        ));

        $structureModelInstance->id = null;

        $structureModelInstance->addWritableField(array("alias", "description"));
        $modelSavedData = $structureModelInstance->save($structureData);
        reset($modelSavedData);

        $data['Structure'] = array(
            'id' => current($modelSavedData)['id'],
            'alias' => current($modelSavedData)['alias'],
            'description' => current($modelSavedData)['alias']
        );

    }

    private function saveCopyFieldFormat(&$data)
    {
        $structureFieldModelInstance = AppModel::getInstance("", "StructureField");
        $structureFormatModelInstance = AppModel::getInstance("", "StructureFormat");
        $structureValidationModelInstance = AppModel::getInstance("", "StructureValidation");
        $structureValueDomainModelInstance = AppModel::getInstance("", "StructureValueDomain");

        $index = 0;

        foreach (array("detailTest", "detailProd") as $k) {
            foreach ($data[$k] as $field) {

                $structureFieldData = isset($field['StructureField'])?$field['StructureField']:array();
                $structureFormatData = isset($field['StructureFormat'])?$field['StructureFormat']:array();
                $structureValidationData = isset($field['validationData'])?$field['validationData']:array();

                if ($this->checkModel($structureFieldData['model'], $data['FormBuilder']['master'])){
                    $svdId = null;
                    if (!preg_match('/.+(_masters)$/', $structureFieldData['tablename'])){
                        if (!empty($structureFieldData['structure_value_domain'])){
                            $svdId = $structureFieldData['structure_value_domain'];
                            $svdData = $structureValueDomainModelInstance->find("first", array(
                                'conditions' => array(
                                    'StructureValueDomain.id' => $structureFieldData['structure_value_domain']
                                    ),
                                'callbacks' => false
                                ));

                            if (strpos($svdData['StructureValueDomain']['source'], "StructurePermissibleValuesCustom::getCustomDropdown('") !== false){
                                $source = preg_replace('/(.+\(\')(.+)(\'\))/', '$2', $svdData['StructureValueDomain']['source']);
                                $svdData = array(
                                    'domain_name' => $this->structureAlias."_".$source."_".$index,
                                    'category' => $this->structureAlias,
                                    'source' => "StructurePermissibleValuesCustom::getCustomDropdown('".$source."')"
                                );
                                $svdData = array("StructureValueDomain" => $svdData);
                                $structureValueDomainModelInstance->addWritableField(array("domain_name", "category", "source"));
                                $structureValueDomainModelInstance->id = null;
                                $saveResult = $structureValueDomainModelInstance->save($svdData);
                                $svdId = $saveResult['StructureValueDomain']['id'];
                            }
                        }
                        $structureFieldModelInstance->setDataBeforeCopyFB($structureFieldData, $index);
                        $structureFieldData['tablename'] = $this->structureAlias;
//                        $structureFieldData['model'] = str_replace('Master', 'Detail', $structureFieldData['model']);
                        $structureFieldData['structure_value_domain'] = (!empty($svdId))?$svdId:"";

                        $this->addToI18n($structureFieldData['language_label']);
                        $this->addToI18n($structureFieldData['language_tag']);
                        $this->addToI18n($structureFieldData['language_help']);

                        $structureFieldModelInstance->addWritableField();
                        $structureFieldModelInstance->id = null;
                        unset($structureFieldData['structure_value_domain_name']);
                        $structureFieldData = array('StructureField' => $structureFieldData);

                        $modelSavedData = $structureFieldModelInstance->save($structureFieldData);

                        $sfiId = $modelSavedData['StructureField']['id'];
                    }else{
                        $sfiId = $structureFieldData['id'];
                    }

                    $structureFormatData['structure_field_id'] = $sfiId;
                    $structureFormatData['structure_id'] = $data['Structure']['id'];
                    $structureFormatData['id'] = null;
                    $structureFormatModelInstance->setDataBeforeCopyFB($structureFormatData);

                    $tablename = "";
                    if (isset($structureFieldData['StructureField']['tablename'])){
                        $tablename = $structureFieldData['StructureField']['tablename'];
                    }elseif (isset($structureFieldData['tablename'])){
                        $tablename = $structureFieldData['tablename'];
                    }
                    if (!preg_match('/.+(_masters)$/', $tablename)){
                        $this->addToI18n($structureFormatData['language_heading']);
                    }
                    $structureFormatModelInstance->id = null;

                    $structureFormatData = array('StructureFormat' => $structureFormatData);
                    $structureFormatModelInstance->addWritableField();
                    $modelSavedData = $structureFormatModelInstance->save($structureFormatData);

                    foreach ($structureValidationData as $value) {
                        unset($value["id"]);
                        $value["structure_field_id"] = $sfiId;
                        $value = array("StructureValidation" => $value);

//                        $messsge = preg_replace('/(.+[0-9]{8}.+\s\|\|\s)(.+)/', '$2', $value['StructureValidation']['language_message']);
                        $messsge = ___($value['StructureValidation']['language_message']);

                        $this->addToI18n($messsge);
                        $structureValidationModelInstance->addWritableField(array("structure_field_id", "rule", "on_action", "language_message"));
                        $structureValidationModelInstance->id = null;
                        $structureValidationModelInstance->save($value);
                    }

                }else{
                    $structureFormatData['structure_field_id'] = $structureFieldData['id'];
                    $structureFormatData['structure_id'] = $data['Structure']['id'];
                    $structureFormatData['id'] = null;
                    $structureFormatModelInstance->setDataBeforeCopyFB($structureFormatData);
//                    $this->addToI18n($structureFormatData['language_heading']);
                    $structureFormatModelInstance->id = null;

                    $structureFormatData = array('StructureFormat' => $structureFormatData);
                    $structureFormatModelInstance->addWritableField();
                    $modelSavedData = $structureFormatModelInstance->save($structureFormatData);

                }

                $index ++;
            }
        }
    }

    /**
     * Tries to fetch model data.
     * If it doesn't exists, redirects to an error page.
     *
     * @param string $id The model primary key to fetch
     * @return array|null model data if it succeeds
     */
    public function getOrRedirect($id)
    {
        $this->id = $id;
        $result = $this->read();
        if ($result && !empty($result['FormBuilder']['flag_active'])) {
            $this->labelFB = isset($result['FormBuilder']['label'])?$result['FormBuilder']['label']:"";
            $this->pluginFB = isset($result['FormBuilder']['plugin'])?$result['FormBuilder']['plugin']:"";
            $this->modelFB = isset($result['FormBuilder']['model'])?$result['FormBuilder']['model']:"";
            $this->masterFB = isset($result['FormBuilder']['master'])?$result['FormBuilder']['master']:"";
            $this->defaultAliasFB = isset($result['FormBuilder']['default_alias'])?$result['FormBuilder']['default_alias']:"";
            $this->optionsFB = isset($result['FormBuilder']['options']) && !empty($result['FormBuilder']['options']) && json_decode($result['FormBuilder']['options']) !== false ? json_decode($result['FormBuilder']['options'], true) : array();
            return $result;
        }
        $bt = debug_backtrace();
        AppController::getInstance()->redirect('/Pages/err_plugin_no_data?method=' . $bt[1]['function'] . ',line=' . $bt[0]['line'], null, true);
        return null;
    }

}