<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class MenusController
 */
class MenusController extends AdministrateAppController
{

    public $uses = array(
        'Menu'
    );

    // temp beforefilter to allow permissions, ACL tables not updated yet
    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allowedActions = array(
            'index'
        );
    }

    public function index()
    {
        //TODO Validate function is not used and delete this function
        $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        exit();

        $hookLink = $this->hook();
        if ($hookLink) {
            require ($hookLink);
        }
        $this->request->data = $this->Menu->find('threaded', array(
            'conditions' => array(
                'parent_id!="3" AND parent_id!="11" AND parent_id!="18" AND parent_id!="55" AND parent_id!="70"'
            )
        ));
    }

    /**
     *
     * @param $menuId
     */
    public function detail($menuId)
    {
        //TODO Validate function is not used and delete this function
        $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        exit();

        $this->set('atimMenuVariables', array(
            'Menu.id' => $menuId
        ));
        $hookLink = $this->hook();
        if ($hookLink) {
            require ($hookLink);
        }
        $this->request->data = $this->Menu->find('first', array(
            'conditions' => array(
                'Menu.id' => $menuId
            )
        ));
    }

    /**
     *
     * @param $bankId
     */
    public function edit($bankId)
    {
        //TODO Validate function is not used and delete this function
        $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        exit();

        $this->set('atimMenuVariables', array(
            'Menu.id' => $menuId
        ));

        $hookLink = $this->hook();
        if ($hookLink) {
            require ($hookLink);
        }

        if (! empty($this->request->data)) {
            $this->Menu->id = $bankId;
            if ($this->Menu->save($this->request->data)) {
                $hookLink = $this->hook('postsave_process');
                if ($hookLink) {
                    require ($hookLink);
                }
                $this->atimFlash(__('your data has been updated'), '/Administrate/Menus/detail/' . $menuId);
            }
        } else {
            $this->request->data = $this->Menu->find('first', array(
                'conditions' => array(
                    'Menu.id' => $menuId
                )
            ));
        }
    }
}