<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class VersionsController
 */
class VersionsController extends AdministrateAppController
{

    public $uses = array(
        'Version'
    );

    public $paginate = array(
        'Version' => array(
            'order' => 'Version.version_number'
        )
    );

    public function detail()
    {
        // MANAGE DATA
        $versionData = $this->Version->find('all', array(
            'order' => array(
                'date_installed DESC',
                "id DESC"
            )
        ));
        if (empty($versionData)) {
            $this->redirect('/Pages/err_plugin_no_data?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        $this->request->data = $versionData;

        if (isset($this->passedArgs['newVersionSetup'])) {
            $this->Version->data = $this->Version->find('first', array(
                'order' => array(
                    'Version.id DESC'
                )
            ));
            $this->Version->id = $this->Version->data['Version']['id'];
            AppController::newVersionSetup();
        }
    }
}