<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class StructureFormatsController
 */
class StructureFormatsController extends AdministrateAppController
{

    public $uses = array(
        'StructureFormat'
    );

    public $paginate = array(
        'StructureFormat' => array(
            'order' => 'StructureFormat.id ASC'
        )
    );

    /**
     *
     * @param $structureId
     */
    public function listAll($structureId)
    {
        //TODO Validate function is not used and delete this function
        $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        exit();

        $this->set('atimStructure', $this->Structures->get(null, 'fields'));
        $this->set('atimMenuVariables', array(
            'Structure.id' => $structureId
        ));

        $hookLink = $this->hook();
        if ($hookLink) {
            require ($hookLink);
        }

        $this->request->data = $this->paginate($this->StructureFormat, array(
            'StructureFormat.structure_id' => $structureId
        ));
    }

    /**
     *
     * @param $structureId
     * @param $structureFormatId
     */
    public function detail($structureId, $structureFormatId)
    {
        //TODO Validate function is not used and delete this function
        $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        exit();

        $this->set('atimStructure', $this->Structures->get(null, 'fields'));
        $this->set('atimMenuVariables', array(
            'Structure.id' => $structureId,
            'StructureFormat.id' => $structureFormatId
        ));

        $hookLink = $this->hook();
        if ($hookLink) {
            require ($hookLink);
        }

        $this->request->data = $this->StructureFormat->find('first', array(
            'conditions' => array(
                'StructureFormat.id' => $structureFormatId
            )
        ));
    }

    /**
     *
     * @param $structureId
     * @param $structureFormatId
     */
    public function edit($structureId, $structureFormatId)
    {
        //TODO Validate function is not used and delete this function
        $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        exit();

        $this->set('atimStructure', $this->Structures->get(null, 'fields'));
        $this->set('atimMenuVariables', array(
            'Structure.id' => $structureId,
            'StructureFormat.id' => $structureFormatId
        ));

        $hookLink = $this->hook();
        if ($hookLink) {
            require ($hookLink);
        }

        if (! empty($this->request->data)) {
            if ($this->StructureFormat->save($this->request->data))
                $this->atimFlash(__('your data has been updated'), '/Administrate/StructureFormats/detail/' . $structureId . '/' . $structureFormatId);
        } else {
            $this->request->data = $this->StructureFormat->find('first', array(
                'conditions' => array(
                    'StructureFormat.id' => $structureFormatId
                )
            ));
        }
    }
}