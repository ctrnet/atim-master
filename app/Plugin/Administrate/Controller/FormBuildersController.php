<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

App::uses('StructuresComponent', 'Controller/Component');

/**
 * Class FormBuildersController
 */
class FormBuildersController extends AdministrateAppController
{
    public $components = array(
        "Structures",
        "Setup",
    );

    public $uses = array(
        'ClinicalAnnotation.ConsentControl',
        'ClinicalAnnotation.DiagnosisControl',
        'ClinicalAnnotation.EventControl',
        'ClinicalAnnotation.TreatmentExtendControl',
        'ClinicalAnnotation.TreatmentControl',
        'ClinicalAnnotation.MiscIdentifierControl',
        'InventoryManagement.SampleControl',
        'InventoryManagement.AliquotControl',
        'InventoryManagement.InventoryActionControl',
        'Administrate.FormBuilder',
        'Structure',
        'StructureField',
        'StructureFormat',
        'StructureValueDomain',
        'StructureValidation',
        'StructurePermissibleValuesCustomControl',
        'Protocol.ProtocolControl',
        'Datamart.Browser'
    );

    public function beforeFilter()
    {
        parent::beforeFilter();
        //Check permissions if need: for example the user have access to FB but not consent
    }

    public function index()
    {
        $this->Structures->set('form_builder_index');
        $this->Structures->set('form_builder_index_multi', 'atimStructureMulti');
        
        
        $indexData = array();
        $indexData["control"] = $this->FormBuilder->find('all', array(
            'conditions' => array(
                'flag_active' => '1',
                'alias !=' =>'',
                'detail !=' =>''
            ),
            'order' => array(
                'display_order'
            )
        ));
        $indexData["other"] = $this->FormBuilder->find('all', array(
            'conditions' => array(
                'flag_active' => '1',
                'alias !=' =>'',
                'detail =' =>''
            ),
            'order' => array(
                'display_order'
            )
        ));
        $indexData["model"] = $this->FormBuilder->find('all', array(
            'conditions' => array(
                'flag_active' => '1',
                'alias =' =>''
            ),
            'order' => array(
                'display_order'
            )
        ));
        if (!empty($indexData)) {
            foreach ($indexData as &$subIndexData) {
                foreach ($subIndexData as &$data) {
                    $data['FormBuilder']['note'] = __($data['FormBuilder']['note']);
                    $data['FormBuilder']['label'] = __($data['FormBuilder']['label']);
                }
            }
        }
        $this->set('indexData', $indexData);
        
    }

    public function listAll($modelType, $formBuilderId)
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));
        $canAdd = in_array($master, array('SampleMaster', 'AliquotMaster'))===false;

        if (isset($alias)) {
            $modelInstance = AppModel::getInstance($plugin, $model);
            $indexData = array();

            if (!empty($master)) {
                if (!isset($modelInstance->actsAs['OrderByTranslate'])){
                    $order = array(
                        $modelInstance->name.'.flag_active DESC',
                        $modelInstance->name.'.display_order',
                        $modelInstance->name.'.id'
                    );
                }else{
                    $order = array(
                        $modelInstance->name.'.flag_active DESC',
                    );
                }
                
                $this->paginate = array(
                    $modelInstance->name => array(
                        'order' => $order
                    )
                );

                $indexData = $this->paginate($modelInstance);

                $modelInstance->dataToDic($indexData);

                foreach ($indexData as &$indexDatum){
                    $indexDatum[$modelInstance->name]["flag_test_mode"] = (isset($indexDatum[$modelInstance->name]["flag_test_mode"]) && empty($indexDatum[$modelInstance->name]["flag_test_mode"])) ? 0 : 1;
                    $indexDatum[$modelInstance->name]["flag_active_input"] = (isset($indexDatum["flag_active_input"]) && empty($indexDatum[$modelInstance->name]["flag_active_input"])) ? 0 : 1;
                    $indexDatum[$modelInstance->name]["flag_active"] = (isset($indexDatum[$modelInstance->name]["flag_test_mode"]) && empty($indexDatum[$modelInstance->name]["flag_active"])) ? 0 : 1;
                    $indexDatum['FunctionManagement']['flag_test_mode'] = $indexDatum[$modelInstance->name]["flag_test_mode"] * 2 + (!empty($indexDatum[$modelInstance->name]['form_alias']) ? $this->Structures->hasTestFields($indexDatum[$modelInstance->name]['form_alias']) : 0);
                }

            }
            $data = array(
                'indexData' => $indexData,
                'model' => $model,
                'plugin' => $plugin,
                'master' => $master,
                'formBuilderId' => $formBuilderId,
                'fbSettings' => array(
                    'add' => $canAdd
                )
            );
            $this->set ('formData', $data);

            $this->Structures->set($alias);
        }
        
        // MANAGE MENU
        
        $this->setFormBuilderMenu(__FUNCTION__, $formBuilderItems);
        $this->set('atimMenuVariables', array(
            'FormBuilder.id' => $formBuilderId
        ));
        $this->set('modelType', $modelType);
    }
    
    public function detail($modelType, $formBuilderId, $controlId = null) 
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));
        $modelInstance = AppModel::getInstance($plugin, $model);
        $canCopy = in_array($master, array('SampleMaster', 'AliquotMaster'))===false;

        $result = array(
            "master" =>array(), 
            "detail" =>array()
        );
        $controlItem = null;
        if (!empty($controlId)){
            $controlItem = $modelInstance->getOrRedirectFB($controlId);
            $modelInstance->dataToDic($controlItem);
            $detailFormAliases = explode(",", $controlItem[$model]["detail_form_alias"]);
            $masterFormAliases = explode(",", $controlItem[$model]["form_alias"]);
            $detailOldFormAliases = explode(",", $controlItem[$model]["detail_old_form_alias"]);
            $masterFormAliases = array_diff($masterFormAliases, $detailOldFormAliases, $detailFormAliases);
            
            foreach ($masterFormAliases as $aliasName){
                $result["master"][] = $this->Structures->getSingleStructure($aliasName, true);
            }
            foreach ($detailFormAliases as $aliasName){
                $result["detail"][] = $this->Structures->getSingleStructure($aliasName, true);
            }
            foreach ($detailOldFormAliases as $aliasName) {
                $result["oldDetail"][] = $this->Structures->getSingleStructure($aliasName, true);
            }
            
            $this->request->data = $this->FormBuilder->getDataFromAlias($result, $master);
            $controlItem['FunctionManagement']['flag_test_mode'] += !empty(array_filter(Hash::extract($this->request->data, "{s}.{n}.StructureField.flag_test_mode"))) ? 1 : 0;

            if (!empty($this->request->data['fbDeletable'])){
                $this->request->data['detailTest'] = array_merge($this->request->data['detailTest'], $this->request->data['fbDeletable']);
                unset($this->request->data['fbDeletable']);
            }
            $this->request->data['control'] = $controlItem;

            $this->Structures->set($alias, "atimStructureForDetailControl");

        }else{
            $result['master'] = $this->Structures->getSingleStructure($defaultAlias, true);
            $this->request->data = $this->FormBuilder->getDataFromAlias($result, $model);
        }

        $data = array(
            'data' => $this->request->data,
            'model' => $model,
            'plugin' => $plugin,
            'master' => $master,
            'controlId' => $controlId,
            'formBuilderId' => $formBuilderId,
            'ControlName' => $modelInstance->getName(),
            'fbSettings' => array(
                'copy' => $canCopy
            )
        );

        if ($model !== 'MiscIdentifierControl'){
            $this->Structures->set("form_builder_master_structure", "atimStructureForControl");
        }else{
            $this->Structures->set('empty', 'atimStructureForControl');
        }
        $this->set("formData", $data);
        
        // MANAGE MENU
        
        $this->setFormBuilderMenu(__FUNCTION__, $formBuilderItems);
        $this->set('atimMenuVariables', array(
            'FormBuilder.id' => $formBuilderId,
            'Control.id' => $controlId,
            'FormBuilder.label' => __($label),
            'Form.name' => $this->getFormatedFormName($formBuilderItems, $controlItem)
        ));
        $this->set('modelType', $modelType);
    }

    public function add($modelType, $formBuilderId) 
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));

        if (empty($master)){
            $this->redirect('/Pages/err_plugin_no_data?method=' . __FUNCTION__ . ',line=' . __LINE__, null, true);
        }
        if (in_array($master, array('SampleMaster', 'AliquotMaster'))!==false){
            $this->atimFlashError(__("You are not authorized to access that location."), "javascript:history.back();");
        }

        if ($model == 'InventoryActionControl'){
            $aliquotData = $this->AliquotControl->find('list', [
                'conditions' => [
                    'AliquotControl.flag_active' => '1',
                ],'fields' => [
                    'sample_control_id'
                ]
            ]);
            $aliquotDataTemp = [];
            foreach ($aliquotData as $aId => $sId){
                $aliquotDataTemp[$sId][] = $aId;
            }
            $this->set('sampleAliquotIds', $aliquotDataTemp);
        }else{
            $this->set('sampleAliquotIds', "");
        }

        $modelInstance = AppModel::getInstance($plugin, $model);

        $this->Structures->set("form_builder_master_structure", "atimStructureForMaster");
        if ($model !== 'MiscIdentifierControl'){
            $this->Structures->set("form_builder_structure", "atimStructureForControl");
        }else{
            $this->Structures->set('empty', 'atimStructureForControl');
        }
        $result['master'] = $this->Structures->getSingleStructure($defaultAlias, true);
        $masterData = $this->FormBuilder->getDataFromAlias($result, $master);

        $this->Structures->set($alias);

        $data = $this->request->data;

        $formData = array(
            'data' => $data,
            'masterData' => $masterData,
            'model' => $model,
            'plugin' => $plugin,
            'master' => $master,
            'formBuilderId' => $formBuilderId,
            'ControlName' => $modelInstance->getName()
        );
        $options = array(
            'prefix-common' =>'common'
        );
        $this->set('formData', $formData);
        $this->set('options', $options);

        if (!empty($data)){
            $data["others"] = $formBuilderItems;
            $data["options"] = $options;
            $valid = true;
            $modelInstance->setDataBeforeSaveFB($data);
            $modelInstance->set($data);
            $valid &= $modelInstance->validates();
            if ($valid){
                $this->StructureField->setDataBeforeSaveFB($data, $options);

                $this->StructureField->data = $data;
                $errors = "";
                $valid &= $this->StructureField->validatesFormBuilder($options, $errors);
                
                $this->StructureFormat->data = $data;
                $valid &= $this->StructureFormat->validatesFormBuilder($options, $errors);
                
                $this->StructureValidation->setDataBeforeSaveFB($data, $options);
                $this->StructureValidation->data = $data;
                $valid &= $this->StructureValidation->validatesFormBuilder($options, $errors);
                
                $this->set("errorsGrid", json_encode($errors));
            }
            if ($valid){
                $data["models"] = array(
                    'main'=> array("plugin" => $plugin, "model" => $model, "master" => $master)
                );
                $this->FormBuilder->normalisedAndSave($data);
                $id = $this->FormBuilder->getLastInsertID();
                $this->atimFlash(__('your data has been saved'), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$id");
            }
        }
        
        // MANAGE MENU
        
        $this->setFormBuilderMenu(__FUNCTION__, $formBuilderItems);
        $this->set('atimMenuVariables', array(
            'FormBuilder.id' => $formBuilderId,
            'Control.id' => null,
            'FormBuilder.label' => __($label),
            'Form.name' => null
        ));
        $this->set('modelType', $modelType);
    }

    public function edit($modelType, $formBuilderId, $controlId = null) 
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));

        $modelInstance = AppModel::getInstance($plugin, $model);

        //TODO: Check it for inventory action control
        if ($model == 'InventoryActionControl'){
            $aliquotData = $this->AliquotControl->find('list', [
                'conditions' => [
                    'AliquotControl.flag_active' => '1',
                ],'fields' => [
                    'sample_control_id'
                ]
            ]);
            $aliquotDataTemp = [];
            foreach ($aliquotData as $aId => $sId){
                $aliquotDataTemp[$sId][] = $aId;
            }
            $this->set('sampleAliquotIds', $aliquotDataTemp);
        }else{
            $this->set('sampleAliquotIds', "");
        }


        $this->Structures->set('empty', 'emptyStructure');

        $data = array();
        $controlItem = null;
        if (!empty($controlId)) {
            $controlItem = $modelInstance->getOrRedirectFB($controlId);
            if ($modelInstance->name == 'MiscIdentifierControl' && empty($controlItem[$modelInstance->name]['flag_test_mode'])){
                $this->atimFlashWarning(__('form_builder_miscidentifier cannot be modified in prod mode'), "javascript::back()");
            }
            $modelInstance->dataToDic($controlItem);
            $detailFormAliases = explode(",", $controlItem[$model]["detail_form_alias"]);
            $masterFormAliases = explode(",", $controlItem[$model]["form_alias"]);
            $detailOldFormAliases = explode(",", $controlItem[$model]["detail_old_form_alias"]);
            $masterFormAliases = array_diff($masterFormAliases, $detailOldFormAliases, $detailFormAliases);

            foreach ($masterFormAliases as $aliasName) {
                $result["master"][] = $this->Structures->getSingleStructure($aliasName, true);
            }
            foreach ($detailFormAliases as $aliasName) {
                $result["detail"][] = $this->Structures->getSingleStructure($aliasName, true);
            }
            foreach ($detailOldFormAliases as $aliasName) {
                $result["oldDetail"][] = $this->Structures->getSingleStructure($aliasName, true);
            }
            $data = $this->FormBuilder->getDataFromAlias($result, $master);

            $data['control'] = $controlItem;

        } else {
            $result['master'] = $this->Structures->getSingleStructure($defaultAlias, true);
            $data = $this->FormBuilder->getDataFromAlias($result, $model);
        }

        $this->Structures->set("form_builder_deletable_structure", "atimStructureDeletable");
        $this->Structures->set("form_builder_master_structure", "atimStructureProd");
        $this->Structures->set("form_builder_structure", "atimStructureTest");

        if ($model == 'TreatmentControl' && !empty($controlItem[$model]['extended_data_import_process'])){
            $alias = "form_builder_treatment_import_process";
        }

        $this->Structures->set($alias);

        if (!empty($this->request->data)) {
            $newData = $this->request->data;
            $newData["others"] = $formBuilderItems;

            if (!isset($newData['masterTest']) || !is_array($newData['masterTest'])) {
                $newData['masterTest'] = array();
            }
            if (!isset($newData['detailTest']) || !is_array($newData['detailTest'])) {
                $newData['detailTest'] = array();
            }

            $newData["others"] = $formBuilderItems;
            $valid = true;
            if (!empty($controlId)){
                $modelInstance->setDataBeforeEditFB($newData);
//                $modelInstance->data = $newData;
                $modelInstance->set($newData);
                $valid &= $modelInstance->validates();
            }
            
            $this->StructureField->setDataBeforeEditFB($newData);

            $this->StructureField->data = $newData;
            $errors = array();

            $valid &= $this->StructureField->validatesEditFormBuilder($errors);

            $this->StructureFormat->data = $newData;
            $valid &= $this->StructureFormat->validatesEditFormBuilder($errors);

            $this->StructureValidation->setDataBeforeEditFB($newData);
            $this->StructureValidation->data = $newData;
            $valid &= $this->StructureValidation->validatesEditFormBuilder($errors);

            $this->set("errorsGrid", json_encode($errors));
            if ($valid) {
                $newData["models"] = array(
                    'main' => array("plugin" => $plugin, "model" => $model, "master" => $master)
                );
                $this->FormBuilder->normalisedAndEdit($newData, $data);
                $this->atimFlash(__('your data has been saved'), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
            }

            $newData['masterProd'] = $data['masterProd'];
            $newData['detailProd'] = $data['detailProd'];
            $newData['control'] = isset($newData[$model]) ? [$model => $newData[$model]] : (isset($data['control']) ? $data['control'] : "");

            $data = $newData;
        }
        $formData = array(
            'data' => $data,
            'model' => $model,
            'plugin' => $plugin,
            'master' => $master,
            'formBuilderId' => $formBuilderId,
            'controlId' => $controlId,
            'isTestMode' => (!empty($controlId))?current($controlItem)['flag_test_mode']:1,
            'ControlName' => $modelInstance->getName()
        );
        $this->set('formData', $formData);
        
        // MANAGE MENU
        
        $this->setFormBuilderMenu(__FUNCTION__, $formBuilderItems);
        $formatedFormName = $this->getFormatedFormName($formBuilderItems, $controlItem);
        $this->set('atimMenuVariables', array(
            'FormBuilder.id' => $formBuilderId,
            'Control.id' => $controlId,
            'FormBuilder.label' => __($label),
            'Form.name' => $formatedFormName
        ));
        $this->set('modelType', $modelType);
        $this->set('formatedFormName', $formatedFormName);
    }

    public function copyControl($modelType, $formBuilderId, $controlId) 
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));

        $modelInstance = AppModel::getInstance($plugin, $model);

        if (in_array($master, array('SampleMaster', 'AliquotMaster'))!==false){
            $this->atimFlashError(__("You are not authorized to access that location."), "javascript:history.back();");
        }

        $this->Structures->set('empty', 'emptyStructure');

        $data = array();
        $controlItem = $modelInstance->getOrRedirect($controlId);
        $detailFormAliases = explode(",", $controlItem[$model]["detail_form_alias"]);
        foreach ($detailFormAliases as $aliasName) {
            $result["detail"][] = $this->Structures->getSingleStructure($aliasName, true);
        }

        $data = $this->FormBuilder->getDataFromAlias($result, $master, array('notCheckModel' => true));
        unset($data['masterProd']);
        unset($data['masterTest']);
        $data['FormBuilder'] = $formBuilderItems['FormBuilder'];
        $data['control'] = $controlItem[$modelInstance->name];
        foreach ($data['detailProd'] as $key => $field){
            if (empty($field['StructureField']['flag_form_builder'])){
                unset($data['detailProd'][$key]);
            }
        }
        $newControlId = $this->FormBuilder->copy($data);
        $this->atimFlashConfirm(__("the copy of %s has been created", ___($data['control'][$modelInstance->controlType])), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$newControlId");
        
        // MANAGE MENU
        
        $this->setFormBuilderMenu(__FUNCTION__, $formBuilderItems);
        $this->set('atimMenuVariables', array(
            'FormBuilder.id' => $formBuilderId
        ));
    }

    public function disable($modelType, $formBuilderId, $controlId) 
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));
        $modelInstance = AppModel::getInstance($plugin, $model);
        $controlItem = $modelInstance->getOrRedirect($controlId);

        //Check if there is no Treatment related to this Treatment Precision
        if ($model == "TreatmentExtendControl"){
            $trModel = AppModel::getInstance($plugin, 'TreatmentControl');
            $trData = $trModel->find('first', [
                'conditions' => [
                    'treatment_extend_control_id' => $controlId,
                    "{$trModel->alias}.flag_active" => 1,
                    "{$trModel->alias}.flag_active_input" => 1,
                    "{$trModel->alias}.flag_test_mode" => 0,
                ],
            ]);
            if (!empty($trData)){
                $this->atimFlashError(__('there are some active treatments related to this treatment precision'), 'javascript::back()');
            }
        }

        $controlItem[$modelInstance->name]['flag_active_input'] = 0;
        $modelInstance->addWritableField();
        $a = $modelInstance->save($controlItem);
        $this->atimFlashConfirm(__("the control has been disabled"), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
    }

    public function enable($modelType, $formBuilderId, $controlId) 
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));
        $modelInstance = AppModel::getInstance($plugin, $model);
        $controlItem = $modelInstance->getOrRedirect($controlId);

        //Check if the related Treatment Precision exists or is active
        if ($model == "TreatmentControl" && !empty($controlItem[$model]['treatment_extend_control_id'])){
            $trExInstance = AppModel::getInstance($plugin, 'TreatmentExtendControl');
            $trExData = $trExInstance->find("first", [
                'conditions' => [
                    "{$trExInstance->alias}.id" => $controlItem[$model]['treatment_extend_control_id'],
                    "{$trExInstance->alias}.flag_active" => 1,
                    "{$trExInstance->alias}.flag_active_input" => 1,
                    "{$trExInstance->alias}.flag_test_mode" => 0,
                ]
            ]);
            if (empty($trExData)) {
                $this->atimFlashError(__('the treatment precision related to this treatment not exist or inactive'), "javascript::back()");
            }
        }

        $controlItem[$modelInstance->name]['flag_active_input'] = 1;
        $modelInstance->addWritableField();
        $a = $modelInstance->save($controlItem);
        $this->atimFlashConfirm(__("the control has been enabled"), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
    }

    public function deactivate($modelType, $formBuilderId, $controlId)
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));
        $modelInstance = AppModel::getInstance($plugin, $model);
        $controlItem = $modelInstance->getOrRedirect($controlId);

        //Check if there is no Treatment related to this Treatment Precision
        if ($model == "TreatmentExtendControl"){
            $trModel = AppModel::getInstance($plugin, 'TreatmentControl');
            $trData = $trModel->find('first', [
                'conditions' => [
                    'treatment_extend_control_id' => $controlId,
                    "{$trModel->alias}.flag_active" => 1,
                    "{$trModel->alias}.flag_test_mode" => 0,
                ],
            ]);
            if (!empty($trData)){
                $this->atimFlashError(__('there are some active treatments related to this treatment precision'), 'javascript::back()');
            }
        }

        $masterModelInstance = AppModel::getInstance($plugin, $master);
        $hasData = $masterModelInstance->find('first', [
            'conditions' => [
                $masterModelInstance->alias . '.' . Inflector::underscore($model) . "_id" => $controlId,
                $masterModelInstance->alias . '.deleted' => ['1', '0'],
            ],
        ]);
        if (empty($hasData)){
            $controlItem[$modelInstance->name]['flag_active'] = 0;
            $modelInstance->addWritableField(['flag_active']);
            $a = $modelInstance->save($controlItem);
            
            $this->Setup->clearCache();
            $this->Setup->automaticATiMCustomisation();
            
            $this->atimFlashConfirm(__("the control has been inactive"), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
        }else{
            $this->atimFlashError(__("there are some data related to this form"), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
        }
    }

    public function activate($modelType, $formBuilderId, $controlId)
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));
        $modelInstance = AppModel::getInstance($plugin, $model);
        $controlItem = $modelInstance->getOrRedirect($controlId);

        //Check if the related Treatment Precision exists or is active
        if ($model == "TreatmentControl" && !empty($controlItem[$model]['treatment_extend_control_id'])){
            $trExInstance = AppModel::getInstance($plugin, 'TreatmentExtendControl');
            $trExData = $trExInstance->find("first", [
                'conditions' => [
                    "{$trExInstance->alias}.id" => $controlItem[$model]['treatment_extend_control_id'],
                    "{$trExInstance->alias}.flag_active" => 1,
                    "{$trExInstance->alias}.flag_test_mode" => 0,
                ]
            ]);
            if (empty($trExData)) {
                $this->atimFlashError(__('the treatment precision related to this treatment not exist or inactive'), "javascript::back()");
            }
        }

        if ($controlItem[$model]['flag_test_mode']){
            $this->redirect('/Pages/err_plugin_system_error?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        $controlItem[$modelInstance->name]['flag_active'] = 1;
        $modelInstance->addWritableField(['flag_active']);
        $a = $modelInstance->save($controlItem);
        
        $this->Setup->clearCache();
        $this->Setup->automaticATiMCustomisation();
        
        $this->atimFlashConfirm(__("the control has been active"), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
    }

    public function delete($modelType, $formBuilderId, $controlId)
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));

        $modelInstance = AppModel::getInstance($plugin, $model);

        $relatedData = $formBuilderItems['FormBuilder'];

        $this->Structures->set('empty', 'emptyStructure');

        $data = array();
        $controlItem = $modelInstance->getOrRedirectFB($controlId);

        if (!$controlItem[$model]['flag_test_mode']){
            $this->atimFlashError(__("the control can not be deleted because its in production mode"), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
        }else{
            
        }
        $modelInstance->dataToDic($controlItem);
        $detailFormAliases = explode(",", $controlItem[$model]["detail_form_alias"]);
        $masterFormAliases = explode(",", $controlItem[$model]["form_alias"]);
        $masterFormAliases = array_diff($masterFormAliases, $detailFormAliases);

        foreach ($detailFormAliases as $aliasName) {
            $result["detail"][] = $this->Structures->getSingleStructure($aliasName, true);
        }
        $data = $this->FormBuilder->getDataFromAlias($result, $master);
        $data['control'] = $controlItem;
        unset($data['masterProd']);
        unset($data['masterTest']);
        unset($data['detailProd']);

        $data['valueDomainData'] = json_decode($data['valueDomainData'], 1);

        $i18nModel = new Model(array(
            'table' => 'i18n',
            'name' => 0
        ));
        if ($relatedData['master'] != 'MiscIdentifier'){
            $i18nModel->deleteAll(array(
                'id like ' => $data['control'][$model]["detail_form_alias"] . " || %"
            ));
        }else{
            $i18nModel->deleteAll(array(
                'id like ' => getPrefix() . $relatedData['default_alias']." || %",
                'page_id' => 'MiscIdentifier'
            ));
        }

        if (isset($data['detailTest']) && is_array($data['detailTest'])){
            foreach ($data['detailTest'] as $key=>$val){
                $this->StructureFormat->delete($val['StructureFormat']['id']);
                $this->StructureValidation->deleteAll(array('StructureValidation.structure_field_id' => $val['StructureField']['id']));
                $this->StructureField->delete($val['StructureField']['id']);
                if (isset($data['valueDomainData'][$key]['id']) && !empty($data['valueDomainData'][$key]['id'])){
                    $this->StructureValueDomain->delete($data['valueDomainData'][$key]['id'], false);
                }
            }
        }

        if (isset($data['fbDeletable']) && is_array($data['fbDeletable'])){
            foreach ($data['fbDeletable'] as $key=>$val){
                $this->StructureFormat->delete($val['StructureFormat']['id']);
                $this->StructureValidation->deleteAll(array('StructureValidation.structure_field_id' => $val['StructureField']['id']));
                $this->StructureField->delete($val['StructureField']['id']);
                if (isset($val['StructureField']['structure_value_domain']) && !empty($val['StructureField']['structure_value_domain'])){
                    $svdId = $val['StructureField']['structure_value_domain'];
                    $svdData = $this->StructureValueDomain->find("first", array(
                        'conditions' => array(
                            'StructureValueDomain.id' => $svdId
                        ),
                        'callbacks' => false
                    ));

                    if (strpos($svdData['StructureValueDomain']['source'], "StructurePermissibleValuesCustom::getCustomDropdown('") !== false) {
                        $this->StructureValueDomain->delete($svdId, false);
                    }
                }
            }
        }

        if ($formBuilderItems['FormBuilder']['master'] != 'MiscIdentifier'){
            $structureId = $this->Structure->find('first', array(
                'conditions' => array(
                    'Structure.alias' => $data['control'][$model]["detail_form_alias"]
                ),
                'recursive' => 0
            ));
            $structureId = $structureId['structure']['Structure']['id'];
            $this->StructureFormat->deleteAll(array('StructureFormat.structure_id' => $structureId));

            $this->Structure->delete($structureId);
        }


        $modelInstance->delete($data['control'][$model]["id"]);
        
        $this->atimFlashConfirm(__("the control is deleted"), "/Administrate/FormBuilders/listAll/$modelType/$formBuilderId");
    }

    public function testToProd($modelType, $formBuilderId, $controlId = null) 
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));

        $modelInstance = AppModel::getInstance($plugin, $model);

        $this->Structures->set('empty', 'emptyStructure');

        $data = array();
        if (!empty($controlId)) {
            $controlItem = $modelInstance->getOrRedirectFB($controlId);

            // Check id Treatment precision related to the Treatment exists and is Active
            if ($model == 'TreatmentControl' && !empty($controlItem[$model]['treatment_extend_control_id'])){
                $trExInstance = AppModel::getInstance($plugin, 'TreatmentExtendControl');
                $trExData = $trExInstance->find("first", [
                    'conditions' => [
                        "{$trExInstance->alias}.id" => $controlItem[$model]['treatment_extend_control_id'],
                        "{$trExInstance->alias}.flag_active" => 1,
                        "{$trExInstance->alias}.flag_active_input" => 1,
                        "{$trExInstance->alias}.flag_test_mode" => 0,
                    ]
                ]);
                if (empty($trExData)) {
                    $this->atimFlashError(__('the treatment precision related to this treatment not exist or inactive'), "javascript::back()");
                }
            }

            $modelInstance->dataToDic($controlItem);
            $detailFormAliases = explode(",", $controlItem[$model]["detail_form_alias"]);
            $masterFormAliases = explode(",", $controlItem[$model]["form_alias"]);
            $masterFormAliases = array_diff($masterFormAliases, $detailFormAliases);

            foreach ($masterFormAliases as $aliasName) {
                $result["master"][] = $this->Structures->getSingleStructure($aliasName, true);
            }
            foreach ($detailFormAliases as $aliasName) {
                $result["detail"][] = $this->Structures->getSingleStructure($aliasName, true);
            }
            $data = $this->FormBuilder->getDataFromAlias($result, $master);

            $data['control'] = $controlItem;
        } else {
            $result['master'] = $this->Structures->getSingleStructure($defaultAlias, true);
            $data = $this->FormBuilder->getDataFromAlias($result, $model);
            $data['control'][$formBuilderItems['FormBuilder']['model']]['detail_form_alias'] = $formBuilderItems['FormBuilder']['default_alias'];
        }
        unset($data['masterProd']);
        unset($data['detailProd']);
        $i18nModel = new Model(array(
            'table' => 'i18n',
            'name' => 'i18n'
        ));

        if ($this->FormBuilder->modelFB == 'MiscIdentifierControl'){
            $i18nWords = $i18nModel->find('all', array(
                'conditions' => array(
                    "id like " => getPrefix(). $this->FormBuilder->defaultAliasFB . " || %",
                    "page_id" => $this->FormBuilder->masterFB
                )
            ));
        }else{
            $i18nWords = $i18nModel->find('all', array(
                'conditions' => array(
                    "id like " => getPrefix(). current($data['control'])['detail_form_alias'] . " || %",
                    "page_id" => ""
                )
            ));
        }

        if (!empty($i18nWords)){
            $lang = (Configure::read('Config.language') == 'eng') ? 'en' : 'fr';
            $i18nWords = array_map(function ($item) use ($lang){
                return(array($item['i18n'][$lang] => $item['i18n']['id']));
            }, $i18nWords);
            $words = array();
            foreach ($i18nWords as $k => $word){
                $words[key($word)] = current($word);
            }

        }

        $data["others"] = $formBuilderItems;
        $data["models"] = array(
            'main' => array("plugin" => $plugin, "model" => $model, "master" => $master)
        );
        $testToProdConditions = !empty($data['fbDeletable']) || !empty($data['detailTest']) || !empty($data['masterTest']) || !empty($controlItem[$modelInstance->name]['flag_test_mode']) || !empty($i18nWords);

        if (!$testToProdConditions) {
            $this->atimFlashError(__("there is no field for save in this form"), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
        } else {

            if (!empty($this->FormBuilder->optionsFB['copyTo'])){
                $structureId = $this->Structure->find('all', array(
                    'conditions' => array(
                        'alias' => $this->FormBuilder->optionsFB['copyTo']
                    ),
                    'recursive' => -1,
                    'fields' => array(
                        'id'
                    ),
                    'formBuilder' => true
                ));
                if (empty($structureId)){
                    $this->atimFlashError(__("there is an error in the model options"), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
                }else{
                    $structureIds = array_map(function($item){return $item['Structure']['id'];}, $structureId);
                }
            }

            if (isset($controlItem[$model]['flag_test_mode']) && $controlItem[$model]['flag_test_mode']){
                $this->FormBuilder->createTable($data);
            }elseif($this->FormBuilder->modelFB != 'MiscIdentifierControl'){
                $this->FormBuilder->addFieldsToTable($data);
            }
            
            foreach (array("detailTest", "masterTest", "fbDeletable") as $mode) {
                if (isset($data[$mode])){
                    foreach ($data[$mode] as $field) {
                        $this->StructureField->updateAll(array("flag_test_mode" => "0"), array("StructureField.id" => $field['StructureField']["id"]));
                    }
                }
            }

            if (!empty($this->FormBuilder->optionsFB['copyTo']) && isset($data['masterTest'])){
                foreach ($structureIds as $structureId){
                    foreach ($data['masterTest'] as $field){
                        $structureFormatData = $field['StructureFormat'];
                        unset($structureFormatData["id"]);
                        $structureFormatData['language_heading'] = !empty($words[$structureFormatData['language_heading']])?$words[$structureFormatData['language_heading']]:$structureFormatData['language_heading'];
                        $structureFormatData['structure_field_id'] = $field['StructureField']['id'];
                        $structureFormatData['structure_id'] = $structureId;
                        $structureFormatData["language_label"] = "";
                        $structureFormatData["language_tag"] = "";
                        $structureFormatData["language_help"] = "";
                        $structureFormatData["flag_addgrid"] = $structureFormatData["flag_add"];
                        $structureFormatData["flag_editgrid"] = $structureFormatData["flag_edit"];

                        $structureFormatData = array('StructureFormat' => $structureFormatData);
                        $this->StructureFormat->id = null;
                        $this->StructureFormat->addWritableField();

                        $this->StructureFormat->save($structureFormatData);
                    }
                }

            }

            $prefix = getPrefix() . $data['others']['FormBuilder']['default_alias'] . " || ";

            $i18nModel->updateAll(array("page_id" => "'FormBuilder'"), array("id like " => current($data['control'])['detail_form_alias'] . " || %"));
            $i18nModel->updateAll(array("page_id" => "'FormBuilder'"), array("id like " => "$prefix%"));

            if (!empty($controlItem[$modelInstance->name]['flag_form_builder'])) {
                $modelInstance->updateAll(array("flag_test_mode" => "0", "flag_active" => "1"), array($modelInstance->name.".id" => $controlId));
            }

            $this->Setup->clearCache();
            $this->Setup->automaticATiMCustomisation();

            $this->FormBuilder->getDataSource()->flushMethodCache();

            $actionControlModel = AppModel::getInstance('InventoryManagement', 'InventoryActionControl');
            $actionControlModel->actionControlInit();

            $this->atimFlashConfirm(__("the form has been created and is useable from now"), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
        }
    }

    public function preview($formBuilderId, $controlId = null) 
    {
        if ($this->request->is('ajax')){
            $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
            extract(AppController::convertArrayKeyFromSnakeToCamel($formBuilderItems['FormBuilder']));
            $this->set('formBuilderItems', $formBuilderItems['FormBuilder']);
            if (!empty($controlId)){
                $modelInstance = AppModel::getInstance($plugin, $model);
                $controlItem = $modelInstance->getOrRedirectFB($controlId);
                $aliases = $controlItem[$model]['form_alias'];
                if (!empty($controlItem[$model]['flag_link_to_study'])){
                    $aliases .= ",inventory_action_study";
                }

                $this->Structures->set($aliases, 'previewAlias', array('test_mode' => true));
                $this->set('formatedFormName', $this->getFormatedFormName($formBuilderItems, $controlItem));
            }else{
                $this->Structures->set($formBuilderItems['FormBuilder']['default_alias'], 'previewAlias', array('test_mode' => true));
                $this->set('formatedFormName', $this->getFormatedFormName($formBuilderItems, null));
            }
        }else{
            $this->atimFlashError(__('You are not authorized to access that location.'), "/Menus");
        }
    }

    public function autoCompleteDropDownList()
    {
        $term = (isset($_GET['term']))?str_replace(array("\\",'%','_'), array("\\\\",'\%','\_'), $_GET['term']):"";
        $result = $this->StructurePermissibleValuesCustomControl->find('all', array(
            'conditions' => array(
                "StructurePermissibleValuesCustomControl.name like" => "%$term%",
            ),
            'fields' => array(
                "StructurePermissibleValuesCustomControl.name",
                "StructurePermissibleValuesCustomControl.id"
            )
        ));
        $result = $this->StructurePermissibleValuesCustomControl->normalized($result);
        $this->set("result", $result);
    }

    public function valueDomain()
    {
        if ($this->request->is('ajax')){
            $this->Structures->set("form_builder_value_domain", "formBuilderValueDomain");
        }else{
            $this->atimFlashError(__('You are not authorized to access that location.'), "/Menus");
        }
    }
    
    public function addValidation($type)
    {
        if ($this->request->is('ajax')){
            $activeValidations = $this->FormBuilder->checkValidation($type);
            $this->Structures->set("form_builder_validation", "formBuilderValidationStructure");
            $this->set("activeValidations", $activeValidations);
            $this->set("type", $type);
        }else{
            $this->atimFlashError(__('You are not authorized to access that location.'), "/Menus");
        }
    }
    
    public function normalised($type)
    {
        $this->autoRender = false ;
        return $this->FormBuilder->normalised($this->request->data, $type);
    }
    
    public function dictionary($modelType, $formBuilderId, $controlId = null)
    {
        $formBuilderItems = $this->FormBuilder->getOrRedirect($formBuilderId);
        $model = $formBuilderItems['FormBuilder']['model'];
        $plugin = $formBuilderItems['FormBuilder']['plugin'];

        $modelInstance = AppModel::getInstance($plugin, $model);

        if (!empty($controlId)) {
            $controlItem = $modelInstance->getOrRedirectFB($controlId);
            $detailFormAlias = $controlItem[$model]["detail_form_alias"];
            $prefix = $detailFormAlias . " || ";
        } else {
            $detailFormAlias = $formBuilderItems["FormBuilder"]["default_alias"];
            $prefix = getPrefix() . $detailFormAlias . " || ";
        }

        $this->Structures->set("form_builder_i18n", "form_builder_i18n");
        
        $i18nModel = new Model(array(
            'table' => 'i18n',
            'name' => 0
        ));

        $i18n = $i18nModel->find("all", array(
            'conditions' => array(
                'OR' => [
                    'AND' => [
                        'id LIKE' => "$prefix%",
                        'page_id' => "",
                    ],
                    'page_id' => "MiscIdentifier",
                ]
            ),
        ));

        $i18nFB = array();
        foreach ($i18n as $value) {
            $i18nFB[] = array("FormBuilder" => $value[0]);
        }
        if (empty($i18n)) {
            $this->set("noData", true);
        } else {
            $this->set("noData", false);
        }
        if (!empty($this->request->data)) {
            $data = $this->request->data;
            foreach ($data as $k => $word) {
                $i18nModel->id = $word['FormBuilder']['id'];

                if (empty($word['FormBuilder']['en'])) {
                    $word['FormBuilder']['en'] = $i18nFB[$k]['FormBuilder']['en'];
                }
                if (empty($word['FormBuilder']['fr'])) {
                    $word['FormBuilder']['fr'] = $i18nFB[$k]['FormBuilder']['fr'];
                }
                $i18nModel->save(array(
                    'id' => $word['FormBuilder']['id'],
                    'en' => $word['FormBuilder']['en'],
                    'fr' => $word['FormBuilder']['fr']
                ));
            }
            if (!empty($controlId)) {
                $this->atimFlash(__('dictionary data saved successfully'), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId");
            } else {
                $this->atimFlash(__('dictionary data saved successfully'), "/Administrate/FormBuilders/detail/$modelType/$formBuilderId");
            }
        } else {
            if ($this->request->is('ajax')){
                $this->set("i18n", $i18nFB);
                $this->set("formBuilderId", $formBuilderId);
                $this->set("controlId", $controlId);
            }else{
                $this->atimFlashError(__('You are not authorized to access that location.'), "/Menus");
            }
        }

        $this->set('modelType', $modelType);
    }

    public function setFormBuilderMenu($controllerFunction, $formBuilderItems)
    {
        $controllerFunction = str_replace(array('add', 'edit'), array('listAll', 'detail'), $controllerFunction);
        $modelType = '';
        if (! $formBuilderItems['FormBuilder']['alias']) {
            $modelType = 'simple_model';
        } elseif ($formBuilderItems['FormBuilder']['detail']) {
            $modelType = 'master_detail_model';
        } else {
            $modelType = 'other_model';
        }
        $atimMenuToCleanUp = $this->Menus->get("/Administrate/FormBuilders/$controllerFunction/$modelType");
        foreach ($atimMenuToCleanUp as $mainMenuKey => $atimSubMenu) {
            foreach ($atimSubMenu as $subMenuKey => $atimSubMenuData) {
                if (isset($atimSubMenuData['Menu']['use_link']) && preg_match('/^\/Administrate\/FormBuilders\/((detail)|(listAll))\/([a-z_]+)\//', $atimSubMenuData['Menu']['use_link'], $matches)) {
                    if ($matches[4] != $modelType) {
                        unset($atimMenuToCleanUp[$mainMenuKey][$subMenuKey]);
                    }
                }
            }
        }
        $this->set('atimMenu', $atimMenuToCleanUp);
    }
    
    public function getFormatedFormName($formBuilderItems, $controlItem = null) {
        if (!isset($controlItem)) {
            // Simple model
            return __($formBuilderItems['FormBuilder']['label']);  
        } else {
            if (array_key_exists('MiscIdentifierControl', $controlItem)) {
                return __($controlItem['MiscIdentifierControl']['misc_identifier_name']);
            } else {
                $controlItemKeys = array_keys($controlItem);
                $controlName = array_shift($controlItemKeys);
                $ctrlFields = array_shift($controlItem);
                return $this->Browser->getTranslatedDatabrowserLabelFromCtrlData($controlName, $ctrlFields);
            }
        }
        return '-';
    }

    public function settings()
    {
        if ($this->request->is('ajax')){
            $this->Structures->set("form_builder_settings", "formBuilderSettingStructure");
        }else{
            $this->atimFlashError(__('You are not authorized to access that location.'), "/Menus");
        }
    }
}