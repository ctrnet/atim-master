<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

class InventoryConfigurationsController extends AppController
{

    public $uses = [
        "InventoryManagement.SampleControl",
        "InventoryManagement.SampleMaster",
        "InventoryManagement.AliquotControl",
        "InventoryManagement.AliquotMaster",
        "InventoryManagement.ParentToDerivativeSampleControl",
    ];

    public function beforeFilter()
    {
        parent::beforeFilter();
    }

    public function edit($sampleControlId)
    {
        $data = $this->SampleControl->getSampleTypeAndDerivatedOrRedirect($sampleControlId)[0];

        $this->set('sampleControlId', $sampleControlId);

        $this->Structures->set('sample_controls_management', 'atimStructure');

        $derivated = [];
        $toDerivate = [];

        if (!empty($data[0]['derivative_sample_id'])){
            foreach (explode(",", $data[0]['derivative_sample_id']) as $id) {
                $sampleControlData = $this->SampleMaster->find('first', [
                    'conditions' => [
                        'SampleMaster.sample_control_id' => $id,
                        'SampleMaster.parent_sample_type' => $data['SampleControl']['sample_type'],
                    ],
                ]);

                if (!empty($sampleControlData)) {
                    $derivated [] = $sampleControlData['SampleControl']['sample_type'];
                } else {
                    $toDerivate [] = $this->SampleControl->findById($id)['SampleControl']['sample_type'];
                }
            }
        }

        $aliquotTypeList = $this->AliquotControl->getPermissibleAliquotsArray($sampleControlId, false);
        $activatedAliquotTypes = [];
        $activatedAliquotTypesWithoutData = [];
        $inactivatedAliquotTypes = [];
        $aliquotIds = [
            'activated' => [],
            'activated_without_data' => [],
            'inactivated' => [],
        ];
        foreach ($aliquotTypeList as $aliquotType) {
            if ($aliquotType['AliquotControl']['flag_active']) {
                $found = $this->AliquotMaster->find("first", [
                    'conditions' => [
                        'AliquotMaster.aliquot_control_id' => $aliquotType['AliquotControl']['id']
                    ],
                ]);
                if (!empty($found)) {
                    $activatedAliquotTypes[$aliquotType['AliquotControl']['aliquot_type']] = $aliquotType['AliquotControl']['aliquot_type'];
                    $aliquotIds['activated'][] = $aliquotType['AliquotControl']['id'];
                } else {
                    $activatedAliquotTypesWithoutData[$aliquotType['AliquotControl']['aliquot_type']] = $aliquotType['AliquotControl']['aliquot_type'];
                    $aliquotIds['activated_without_data'][] = $aliquotType['AliquotControl']['id'];
                }
            } else {
                $inactivatedAliquotTypes[$aliquotType['AliquotControl']['aliquot_type']] = $aliquotType['AliquotControl']['aliquot_type'];
                $aliquotIds['inactivated'][] = $aliquotType['AliquotControl']['id'];
            }
        }

        $data =
            [
                "SampleControl" => [
                    "id" => $data['SampleControl']['sample_control_id'],
                    "sample_type" => $data['SampleControl']['sample_type'],
                    "sample_category" => $data['SampleControl']['category'],
                ], "FunctionManagement" => [
                "to_derivate" => implode("|||", $toDerivate),
                "derivatived" => implode("|||", $derivated),
                "parent" => str_replace(",", "|||", $data[0]['parent_sample_type']),
                "aliquot_type_read_only" => implode("|||", $activatedAliquotTypes),
                "aliquot_type" => implode("|||", $activatedAliquotTypesWithoutData),
            ],
            ];

        // Define the Aliquot custom list
        $dropdownOptions = ['FunctionManagement.aliquot_type' => array_map("___", array_merge($activatedAliquotTypesWithoutData, $inactivatedAliquotTypes))];
        $this->Structures->setDropdownOptions($dropdownOptions, 'dropdownOptions');

        if (empty($this->request->data)) {
            $this->request->data = $data;
        } else {
            $submittedDataValidates = true;

            $hookLink = $this->hook('presave_process');
            if ($hookLink) {
                require($hookLink);
            }

            if ($submittedDataValidates) {

                // Aliquot type validation
                $aliquotIds['add'] = [];
                if (!empty($this->request->data["FunctionManagement"]["aliquot_type"])) {
                    $aliquotControldata = $this->AliquotControl->find('all', [
                        'conditions' => [
                            'AliquotControl.aliquot_type' => $this->request->data["FunctionManagement"]["aliquot_type"],
                            'AliquotControl.sample_control_id' => $sampleControlId
                        ],
                    ]);

                    $aliquotTypes = Hash::extract($aliquotControldata, "{n}.AliquotControl.aliquot_type");
                    $aliquotIds['add'] = Hash::extract($aliquotControldata, "{n}.AliquotControl.id");

                    // Check if the aliquots are not from the unchangables or the list modified in front end
                    $problematicAliquotTypes = implode(", ", array_map("___", array_unique(array_merge(array_diff($aliquotTypes, $this->request->data["FunctionManagement"]["aliquot_type"]), array_diff($this->request->data["FunctionManagement"]["aliquot_type"], $aliquotTypes)))));
                    if (!empty($problematicAliquotTypes)) {
                        $this->AliquotControl->validationErrors['aliquot_type'][] = $problematicAliquotTypes;
                        $submittedDataValidates = false;
                    }

                }
                $shouldActivateAliquotIds = array_intersect($aliquotIds['add'], $aliquotIds['inactivated']);
                $shouldInactivateAliquotIds = array_diff($aliquotIds['activated_without_data'], $aliquotIds['add']);

                //Sample Control validateion
                $this->request->data['FunctionManagement']['to_derivate'] = !empty($this->request->data['FunctionManagement']['to_derivate']) ? $this->request->data['FunctionManagement']['to_derivate'] : [];
                $sampleControlData = $this->SampleControl->find('all', [
                    'conditions' => [
                        'SampleControl.sample_type' => $this->request->data['FunctionManagement']['to_derivate']
                    ]
                ]);

                $tmpSampleTypes1 = Hash::extract($sampleControlData, '{n}.SampleControl.sample_type');
                $tmpSampleTypes2 = $this->request->data['FunctionManagement']['to_derivate'];

                $problematicSampleTypes = implode(", ", array_map("___", array_unique(array_merge(array_diff($tmpSampleTypes1, $tmpSampleTypes2), array_diff($tmpSampleTypes2, $tmpSampleTypes1)))));
                if (!empty($problematicSampleTypes)) {
                    $this->SampleControl->validationErrors['to_derivate'][] = $problematicSampleTypes;
                    $submittedDataValidates = false;
                }
                // Should not select from the derivated with data
                $intersectionAliquotType = implode(", ", array_map("___", array_unique(array_intersect($derivated, $this->request->data['FunctionManagement']['to_derivate']))));
                if (!empty($intersectionAliquotType)) {
                    $this->SampleControl->validationErrors['to_derivate'][] = __('the sample types have already derivated for %s [%s]', ___($data['SampleControl']['sample_type']),$intersectionAliquotType);
                    $submittedDataValidates = false;
                }

                $shouldRemove = array_diff($toDerivate, $this->request->data['FunctionManagement']['to_derivate']);
                $shouldAdd = array_diff($this->request->data['FunctionManagement']['to_derivate'], $toDerivate);
                $allSampleTypeToConsider = array_values(array_unique(array_merge($derivated, $toDerivate, $this->request->data['FunctionManagement']['to_derivate'])));

                $listSelectedSubPossibleDerivatedTypeConditions = [
                    'SampleControl.id' => $sampleControlId,
                    'SampleControl2.sample_type' => $allSampleTypeToConsider
                ];

                $joins =
                    [
                        [
                            'table' => 'sample_controls',
                            'alias' => 'SampleControl2',
                        ], [
                            'table' => 'parent_to_derivative_sample_controls',
                            'alias' => 'ParentToDerivativeSampleControl',
                            'type' => 'LEFT',
                            'conditions' => [
                                "SampleControl.id = ParentToDerivativeSampleControl.parent_sample_control_id",
                                "SampleControl2.id = ParentToDerivativeSampleControl.derivative_sample_control_id",
                            ]
                        ]
                    ];

                $fields =
                    [
                        'SampleControl.id',
                        'SampleControl.sample_type',
                        'SampleControl2.id',
                        'SampleControl2.sample_type',
                        'ParentToDerivativeSampleControl.id',
                        'ParentToDerivativeSampleControl.flag_active',
                    ];

                $order = 'SampleControl.id, SampleControl2.id';

                $patenrToDerivativeData = $this->SampleControl->find('all', [
                    'conditions' => $listSelectedSubPossibleDerivatedTypeConditions,
                    'joins' => $joins,
                    'fields' => $fields,
                    'order' => $order,
                ]);

                $shouldRemoveSampleIds = [];
                $shouldAddSampleIds = [];
                $shouldUpdateSampleIds = [];
                foreach ($patenrToDerivativeData as $patenrToDerivativeDatum) {
                    if (in_array($patenrToDerivativeDatum['SampleControl2']['sample_type'], $shouldRemove)) {
                        $shouldRemoveSampleIds[] = $patenrToDerivativeDatum['ParentToDerivativeSampleControl']['id'];
                    } elseif (in_array($patenrToDerivativeDatum['SampleControl2']['sample_type'], $shouldAdd)) {
                        if (!empty($patenrToDerivativeDatum['ParentToDerivativeSampleControl']['id']) && empty($patenrToDerivativeDatum['ParentToDerivativeSampleControl']['flag_active'])){
                            $shouldUpdateSampleIds[] = $patenrToDerivativeDatum['ParentToDerivativeSampleControl']['id'];
                        }elseif(empty($patenrToDerivativeDatum['ParentToDerivativeSampleControl']['id'])){
                            $shouldAddSampleIds[] = [
                                $patenrToDerivativeDatum['SampleControl']['id'],
                                $patenrToDerivativeDatum['SampleControl2']['id']
                            ];
                        }
                    }
                }

                if ($submittedDataValidates){
                    $submittedDataValidates = $this->AliquotControl->updateAll(
                        [
                            'AliquotControl.flag_active' => 1
                        ], [
                            'AliquotControl.id' => $shouldActivateAliquotIds
                        ]
                    );

                    if ($submittedDataValidates){
                        $submittedDataValidates = $this->AliquotControl->updateAll(
                            [
                                'AliquotControl.flag_active' => 0
                            ], [
                                'AliquotControl.id' => $shouldInactivateAliquotIds
                            ]
                        );
                    }

                    $parentToDerivativeSampleControlModel = new Model(array(
                        'table' => 'parent_to_derivative_sample_controls',
                        'name' => 'ParentToDerivativeSampleControl'
                    ));
                    if ($submittedDataValidates && !empty($shouldRemoveSampleIds)){
                        $submittedDataValidates = $parentToDerivativeSampleControlModel->updateAll(
                            [
                                'flag_active' => 0
                            ],[
                                'ParentToDerivativeSampleControl.id' => $shouldRemoveSampleIds
                            ]
                        );
                    }

                    if ($submittedDataValidates && !empty($shouldUpdateSampleIds)){
                        $submittedDataValidates = $parentToDerivativeSampleControlModel->updateAll(
                            [
                                'flag_active' => 1
                            ],[
                                'ParentToDerivativeSampleControl.id' => $shouldUpdateSampleIds
                            ]
                        );
                    }

                    if ($submittedDataValidates && !empty($shouldAddSampleIds)){
                        $parentToDerivativeSampleControlData = [];
                        foreach ($shouldAddSampleIds as $shouldAddSampleId)
                        {
                            $parentToDerivativeSampleControlData[] = [
                                'ParentToDerivativeSampleControl' => [
                                    'parent_sample_control_id' => $shouldAddSampleId[0],
                                    'derivative_sample_control_id' => $shouldAddSampleId[1],
                                    'flag_active' => 1,
                                ],
                            ];
                        }

                        $submittedDataValidates = $parentToDerivativeSampleControlModel->saveAll($parentToDerivativeSampleControlData);
                    }
                }

                $hookLink = $this->hook('postsave_process');
                if ($hookLink) {
                    require ($hookLink);
                }
                if ($submittedDataValidates) {
                    $this->atimFlash(__('your data has been updated'), '/Administrate/InventoryConfigurations/detail/' . $sampleControlId);
                }
            }
        }
    }

    public function detail($sampleControlId)
    {
        $data = $this->SampleControl->getSampleTypeAndDerivatedOrRedirect($sampleControlId)[0];
        $this->request->data =
        [
            "SampleControl" => [
                "id" => $data['SampleControl']['sample_control_id'],
                "sample_type" => __($data['SampleControl']['sample_type']),
                "sample_category" => __($data['SampleControl']['category']),
            ], "FunctionManagement" => [
                "derivate" => implode("<br>", array_map("__", explode(",", $data[0]['derivative_sample_type']))),
                "parent" => implode("<br>", array_map("__", explode(",", $data[0]['parent_sample_type']))),
                "aliquot_type" => implode("<br>", array_map("__", explode(",", $data[0]['aliquot_type']))),
                "specimen_active" => ($data['SampleControl']['category'] == 'derivative' ? __('n/a') : (strpos($data['0']['flag_active'], "1") !== false ? __('yes') : __('no'))),
            ],
        ];

        $activateSpecimenButton = false;
        if ($data['SampleControl']['category'] == 'specimen'){
            $queryActive = "
                SELECT flag_active 
                FROM  parent_to_derivative_sample_controls
                WHERE parent_sample_control_id IS NULL 
                AND derivative_sample_control_id = {$data['SampleControl']['sample_control_id']}";
            $queryActiveRes = $this->SampleControl->query($queryActive);
            if ($queryActiveRes) {
                if ($queryActiveRes['0']['parent_to_derivative_sample_controls']['flag_active']){
                    $query = "
                        SELECT SM.`id` FROM `sample_masters` SM WHERE 
                              SM.`sample_control_id` = '$sampleControlId'
                        LIMIT 1;
                    ";
    
                    if (empty($this->SampleControl->query($query))){
                        $activateSpecimenButton = 'deactivate';
                    }
                } else {
                    $activateSpecimenButton = 'activate';
                }
            }
        }
        $this->set("activateSpecimenButton", $activateSpecimenButton);
        $this->Structures->set('sample_controls_management', 'atimStructure');

        $hookLink = $this->hook('format');
        if ($hookLink) {
            require($hookLink);
        }

    }

    /**
     *
     * @param string $searchId
     */
    public function search($searchId = '')
    {
        // CUSTOM CODE: Hook for search_handler
        $hookLink = $this->hook('pre_search');
        if ($hookLink) {
            require($hookLink);
        }

        // CUSTOM CODE: FORMAT DISPLAY DATA

        $hookLink = $this->hook('format');
        if ($hookLink) {
            require($hookLink);
        }

        $this->Structures->set('sample_controls_management', 'atimStructure');

        $this->render('search');

    }

    public function index()
    {

        $parentToDerivativeSampleControlModel = new Model(array(
            'table' => 'parent_to_derivative_sample_controls',
            'name' => 'ParentToDerivativeSampleControl'
        ));

        $conditions = [
        ];
        if (!empty($this->request->data)) {
            $sampleTypes = !empty($this->request->data['SampleControl']['sample_type']) ? array_filter($this->request->data['SampleControl']['sample_type']) : [];
            $sampleCategory = !empty($this->request->data['SampleControl']['sample_category']) ? array_filter($this->request->data['SampleControl']['sample_category']) : [];
            $derivatived = !empty($this->request->data['FunctionManagement']['derivate']) ? array_filter($this->request->data['FunctionManagement']['derivate']) : [];
            $parent = !empty($this->request->data['FunctionManagement']['parent']) ? array_filter($this->request->data['FunctionManagement']['parent']) : [];
            $aliquotType = !empty($this->request->data['FunctionManagement']['aliquot_type']) ? array_filter($this->request->data['FunctionManagement']['aliquot_type']) : [];
            if (!empty($sampleTypes)) {
                $conditions['SampleControl.sample_type'] = $sampleTypes;
            }
            if (!empty($sampleCategory)) {
                $conditions['SampleControl.sample_category'] = $sampleCategory;
            }
            if (!empty($derivatived)) {
                $conditions['ParentSample.sample_type'] = $derivatived;
            }
            if (!empty($parent)) {
                $conditions['SampleControlDerivate.sample_type'] = $parent;
            }
            if (!empty($aliquotType)) {
                $conditions['AliquotControl.aliquot_type'] = $aliquotType;
            }
        }

        $joins =
            [
                [
                    'table' => 'parent_to_derivative_sample_controls',
                    'alias' => 'ParentToDerivativeSampleControl2',
                    'type' => 'LEFT',
                    'conditions' => [
                        'ParentToDerivativeSampleControl.derivative_sample_control_id = ParentToDerivativeSampleControl2.parent_sample_control_id'
                    ],
                ], [
                'table' => 'sample_controls',
                'alias' => 'SampleControl',
                'type' => 'LEFT',
                'conditions' => [
                    'ParentToDerivativeSampleControl.derivative_sample_control_id = SampleControl.id'
                ],
            ], [
                'table' => 'sample_controls',
                'alias' => 'SampleControlDerivate',
                'type' => 'LEFT',
                'conditions' => [
                    'ParentToDerivativeSampleControl.parent_sample_control_id = SampleControlDerivate.id'
                ],
            ], [
                'table' => 'sample_controls',
                'alias' => 'ParentSample',
                'type' => 'LEFT',
                'conditions' => [
                    'ParentToDerivativeSampleControl2.derivative_sample_control_id = ParentSample.id'
                ],
            ], [
                'table' => 'aliquot_controls',
                'alias' => 'AliquotControl',
                'type' => 'LEFT',
                'conditions' => [
                    'AliquotControl.sample_control_id = SampleControl.id'
                ],
            ],
            ];

        $fields =
            [
                "SampleControl.id AS sample_control_id",
                "SampleControl.sample_type AS sample_type",
                "SampleControl.sample_category AS category",
                "GROUP_CONCAT(DISTINCT SampleControlDerivate.sample_type) AS parent_sample_type",
                "GROUP_CONCAT(ParentToDerivativeSampleControl.flag_active) flag_active",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (ParentToDerivativeSampleControl2.flag_active = 1 , ParentSample.sample_type, -1)), ',-1', ''), '-1,', ''), '-1', '') AS derivative_sample_type",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (ParentToDerivativeSampleControl2.flag_active = 0 , ParentSample.sample_type, -1)), ',-1', ''), '-1,', ''), '-1', '') AS derivative_sample_type_inactive",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (ParentToDerivativeSampleControl2.flag_active = 1 , ParentSample.id, -1)), ',-1', ''), '-1,', ''), '-1', '') AS derivative_sample_id",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (ParentToDerivativeSampleControl2.flag_active = 0 , ParentSample.id, -1)), ',-1', ''), '-1,', ''), '-1', '') AS derivative_sample_id_inactive",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (AliquotControl.flag_active = 1, AliquotControl.aliquot_type, -1)), ',-1', ''), '-1,', ''), '-1', '') AS aliquot_type",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (AliquotControl.flag_active = 0, AliquotControl.aliquot_type, -1)), ',-1', ''), '-1,', ''), '-1', '') AS aliquot_type_inactive ",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (AliquotControl.flag_active = 1, AliquotControl.id, -1)), ',-1', ''), '-1,', ''), '-1', '') AS aliquot_id",
                "REPLACE(REPLACE(REPLACE(GROUP_CONCAT(DISTINCT if (AliquotControl.flag_active = 0, AliquotControl.id, -1)), ',-1', ''), '-1,', ''), '-1', '') AS aliquot_id_inactive ",
            ];

        $order =
            [
                "sample_control_id"
            ];

        $group =
            [
                "sample_type",
                "category",
            ];

        $data = $parentToDerivativeSampleControlModel->find('all', [
            'conditions' => $conditions,
            'joins' => $joins,
            'fields' => $fields,
            'group' => $group,
            'order' => $order,
        ]);

        $this->request->data = [];
        
        foreach ($data as $datum) {
            $this->request->data[] = [
                "SampleControl" => [
                    "id" => $datum['SampleControl']['sample_control_id'],
                    "sample_type" => __($datum['SampleControl']['sample_type']),
                    "sample_category" => __($datum['SampleControl']['category']),
                ], "FunctionManagement" => [
                    "derivate" => implode("  /  ", array_map("__", explode(",", $datum[0]['derivative_sample_type']))),
                    "parent" => implode("  /  ", array_map("__", explode(",", $datum[0]['parent_sample_type']))),
                    "aliquot_type" => implode("  /  ", array_map("__", explode(",", $datum[0]['aliquot_type']))),
                    "specimen_active" => ($datum['SampleControl']['category'] == 'derivative' ? __('n/a') : (strpos($datum['0']['flag_active'], "1") !== false ? __('yes') : __('no'))),
                ],
            ];
        }

        usort($this->request->data, function ($a, $b){
            return strcmp(strtolower($a['SampleControl']['sample_type']), strtolower($b['SampleControl']['sample_type']));
        });

        $this->Structures->set('sample_controls_management', 'atimStructure');
    }

    public function deactivate($controlId)
    {
        $sampleControlData = $this->SampleControl->getSampleTypeAndDerivatedOrRedirect($controlId, false);

        if ($sampleControlData[0]['SampleControl']['sample_category'] == 'derivative') {
            $this->redirect('/Pages/err_plugin_no_data?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        $query = "
                    SELECT SM.`id` FROM `sample_masters` SM WHERE 
                          SM.`sample_control_id` = '$controlId'
                    LIMIT 1;
                ";

        if (empty($this->SampleControl->query($query))) {
            $sampleControlData[0]['ParentToDerivativeSampleControl']['flag_active'] = false;
            $saveStatus = $this->ParentToDerivativeSampleControl->save([
                'ParentToDerivativeSampleControl' => $sampleControlData[0]['ParentToDerivativeSampleControl']
            ]);

            if ($saveStatus){
                $this->atimFlashConfirm(__('the sample (%s) is deactivated', __($sampleControlData[0]['SampleControl']['sample_type'])), "/Administrate/InventoryConfigurations/detail/$controlId");
            } else {
                $this->atimFlashWarning(__('cannot change the status of the sample (%s)', __($sampleControlData[0]['SampleControl']['sample_type'])), "/Administrate/InventoryConfigurations/detail/$controlId");
            }
        } else {
            $this->atimFlashError(__('this sample (%s) has already some derivatives', __($sampleControlData[0]['SampleControl']['sample_type'])), "/Administrate/InventoryConfigurations/detail/$controlId");
        }
    }

    public function activate($controlId)
    {
        $sampleControlData = $this->SampleControl->getSampleTypeAndDerivatedOrRedirect($controlId, false);
        if ($sampleControlData[0]['SampleControl']['sample_category'] == 'derivative') {
            $this->redirect('/Pages/err_plugin_no_data?method=' . __METHOD__ . ',line=' . __LINE__, null, true);
        }
        $sampleControlData[0]['ParentToDerivativeSampleControl']['flag_active'] = true;
        $saveStatus = $this->ParentToDerivativeSampleControl->save([
            'ParentToDerivativeSampleControl' => $sampleControlData[0]['ParentToDerivativeSampleControl']
        ]);

        if ($saveStatus){
            $this->atimFlashConfirm(__('the sample (%s) is activated', __($sampleControlData[0]['SampleControl']['sample_type'])), "/Administrate/InventoryConfigurations/detail/$controlId");
        } else {
            $this->atimFlashWarning(__('cannot change the status of the sample (%s)', __($sampleControlData[0]['SampleControl']['sample_type'])), "/Administrate/InventoryConfigurations/detail/$controlId");
        }

    }

}