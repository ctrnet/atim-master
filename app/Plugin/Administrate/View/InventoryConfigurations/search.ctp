<?php
/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

AppController::addWarningMsg(__('inventory_configurations_warning_message'));

$settings = array(
    'return' => true
);
$settings['header'] = [
    'title' => __('search type') . ': ' . __('sample type'),
    'description' => '<br>' .
        __('inventory_configurations_specimen_activation_message') . '<br><br>' .
        __('inventory_configurations_derivative_activation_message') . '<br><br>' .
        __('inventory_configurations_aliquot_activation_message'),
];
// Set form structure and option
$finalOptions = array(
    'type' => 'search',
    'links' => array(
        'top' => '/Administrate/InventoryConfigurations/index/'
    ),
    'settings' => $settings
);

// CUSTOM CODE
$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}

// BUILD FORM
$form = $this->Structures->build($atimStructure, $finalOptions);
echo $form;