<?php

$options = [
    'settings' => [
        'header' => __('inventory configuration'),
    ],
    'links' =>[
        'top' => "/Administrate/InventoryConfigurations/edit/$sampleControlId",
        'bottom' => [
            'cancel' => "/Administrate/InventoryConfigurations/detail/$sampleControlId",
        ],
    ],
    'dropdown_options' => $dropdownOptions,
];

$this->Structures->build($atimStructure, $options);