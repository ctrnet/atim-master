<?php

$options = [
    'settings' => [
        'header' => __('inventory configuration'),
    ],
    'links' =>[
        'bottom' => [
            'cancel' => '/Administrate/InventoryConfigurations/search/',
            'edit' => '/Administrate/InventoryConfigurations/edit/' . $this->request->data['SampleControl']['id'],
        ],
    ],
];

if(!empty($activateSpecimenButton)){
    $sampleType = $this->request->data['SampleControl']['sample_type'];
    $controlId = $this->request->data['SampleControl']['id'];
    if ($activateSpecimenButton == 'deactivate'){
        $options['links']['bottom']['deactivate'] = [
            "link" => "/Administrate/InventoryConfigurations/deactivate/$controlId",
            "icon" => "is-disable",
            'title' => __('deactivate the sample (%s)', $sampleType),
        ];
    } elseif ($activateSpecimenButton == 'activate') {
        $options['links']['bottom']['activate'] = [
            'link' => "/Administrate/InventoryConfigurations/activate/$controlId",
            "icon" => "confirm",
            'title' => __('activate the sample (%s)', $sampleType),
        ];

    }
}

$this->Structures->build($atimStructure, $options);