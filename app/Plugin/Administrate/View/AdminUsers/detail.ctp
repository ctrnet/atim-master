<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

if ($_SESSION['Auth']['User']['id'] == $atimMenuVariables['User.id']) {
    $structureLinks = array(
        'bottom' => array(
            'edit' => '/Administrate/AdminUsers/edit/' . $atimMenuVariables['Group.id'] . '/%%User.id%%'
        )
    );
} else {
    $structureLinks = array(
        'bottom' => array(
            'edit' => '/Administrate/AdminUsers/edit/' . $atimMenuVariables['Group.id'] . '/%%User.id%%',
            'change group' => array(
                'link' => '/Administrate/AdminUsers/changeGroup/' . $atimMenuVariables['Group.id'] . '/%%User.id%%',
                'icon' => 'users'
            ),
            'delete' => '/Administrate/AdminUsers/delete/' . $atimMenuVariables['Group.id'] . '/%%User.id%%'
        )
    );
}

if ($isSuperAdmin || $currentUser){
    unset($structureLinks['bottom']['change group']);
}

if ($isSuperAdminTFA) {
    $structureLinks['bottom']['regenerate the tfa configuration key'] = [
        'link' => 'javascript:generateNewSecretKey(%%User.id%%)',
        'icon' => 'refresh',
        'title' => __('generate the new qr code help message'),
    ];
}

if (!empty($superAdminStatus)) {
    if ($superAdminStatus == 'inactive') {
        $structureLinks['bottom']['set the user as tfa super admin'] = [
            'link' => "javascript:changeSuperAdminStatus(%%User.id%%, '$superAdminStatus')",
            'icon' => 'confirm',
            'title' => __('set the user as tfa super admin help message'),
        ];
    } elseif ($superAdminStatus == 'active') {
        $structureLinks['bottom']['remove tfa super admin role'] = [
            'link' => "javascript:changeSuperAdminStatus(%%User.id%%, '$superAdminStatus')",
            'icon' => 'is-disable',
            'title' => __('remove tfa super admin role help message'),
        ];
    }
}

$this->Structures->build($atimStructure, array(
    'links' => $structureLinks,
    'settings' => [
        'no_sanitization' => [
            'FunctionManagement' => ['qrcode']
        ]
    ]
));
?>

<script>
    var REGENERATE_SECRET_CODE_CONFORMATION = '<?= ___("do you want to generate a new secret code for the user?") ?>';
    var THE_SECRET_KEY_IS_REGENERATED = '<?= ___("the secret key is regenerated") ?>';
    var SOME_PROBLEM_HAPPEND = '<?= ___("there are some problems in creating a new secret key") ?>';
    var SET_THE_SUPER_ADMIN_MESSAGE = '<?= ___("do you want to set this user as tfa super admin?") ?>';
    var REMOVE_THE_SUPER_ADMIN_MESSAGE = '<?= ___("do you want to remove the tfa super admin permission from this user?") ?>';

    function changeSuperAdminStatus(id, status) {
        var popupMessage;
        if (status == 'active') {
            popupMessage = REMOVE_THE_SUPER_ADMIN_MESSAGE;
        } else if (status == 'inactive') {
            popupMessage = SET_THE_SUPER_ADMIN_MESSAGE;
        }

        if ($("#superAdminConfirmDualogPopup").length == 0) {
            var yes_action = function () {
                $("#superAdminConfirmDualogPopup").popup('close');
                $.get(root_url + "Users/changeTheSuperAdminStatus/" + id + "/" + status + "/", function (data) {
                    location.reload();
                });

                return false;
            };
            var no_action = function () {
                $("#superAdminConfirmDualogPopup").popup('close');
                return false;
            };

            buildConfirmDialog('superAdminConfirmDualogPopup', popupMessage, [
                {
                    label: STR_YES,
                    action: yes_action,
                    icon: "detail",
                }, {
                    label: STR_NO,
                    action: no_action,
                    icon: "delete noPrompt",
                }
            ]);
        }
        $("#superAdminConfirmDualogPopup").popup();

    }

    function generateNewSecretKey(id) {

        if ($("#regenerateConfirmDualogPopup").length == 0) {
            var yes_action = function () {
                $("#regenerateConfirmDualogPopup").popup('close');
                $.get(root_url + "Users/regenerateSecretKey/" + id + "/", function (data) {
                    data = normaliseData(data);
                    if (data == "ok") {
                        alert(THE_SECRET_KEY_IS_REGENERATED);
                    } else if (data == "nok") {
                        alert(SOME_PROBLEM_HAPPEND);
                    }
                });

                return false;
            };
            var no_action = function () {
                $("#regenerateConfirmDualogPopup").popup('close');
                return false;
            };

            buildConfirmDialog('regenerateConfirmDualogPopup', REGENERATE_SECRET_CODE_CONFORMATION, [
                {
                    label: STR_YES,
                    action: yes_action,
                    icon: "detail",
                }, {
                    label: STR_NO,
                    action: no_action,
                    icon: "delete noPrompt",
                }
            ]);
        }
        $("#regenerateConfirmDualogPopup").popup();
    }
</script>