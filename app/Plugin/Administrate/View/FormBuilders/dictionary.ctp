<?php
if ($noData){
    ob_clean();
    echo "nok";
    return;
}
$this->request->data = $i18n;

$structureOptions = array(
    'links' => array(
        'top' => "/Administrate/FormBuilders/dictionary/$modelType/$formBuilderId/$controlId",
        'bottom' => array("cancel" => "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId"),
    ),
    'type' => 'editgrid',
    'settings' =>array(
        'header' => __("control detail")
    )
);

$this->Structures->build($formBuilderI18n, $structureOptions);