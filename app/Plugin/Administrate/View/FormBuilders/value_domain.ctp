<?php
$structureOptions = [
    'links' => [
        'top' => "/Administrate/FormBuilders/valueDomain",
    ],
    'type' => 'edit',
    'settings' => [
        'header' => __("custom drop down list name"),
    ],
];
$structureOptions['override']['StructureValueDomain.domain_name'] = !empty($this->request->data['value']) ? $this->request->data['value'] : "";
$structureOptions['override']['StructureValueDomain.flag_multi_select'] = (!empty($this->request->data['multi-select']) && ($this->request->data['multi-select'] === 1 || $this->request->data['multi-select'] === '1' || $this->request->data['multi-select'] === 'true')) ? 1 : 0;
$this->Structures->build($formBuilderValueDomain, $structureOptions);