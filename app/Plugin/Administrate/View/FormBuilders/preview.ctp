<?php
$structureOptions = array(
    'links' => array(
        'top' => "/Administrate/FormBuilders/index",
    ),
    'type' => 'add',
    'settings' =>array(
        'header' => __("preview of the %s form", $formatedFormName),
        'form_bottom' => false,
        'actions' => false
    )
);

$this->Structures->build($previewAlias, $structureOptions);