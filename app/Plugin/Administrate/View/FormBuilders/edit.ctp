<?php
extract($formData);
$structureLinks = array(
    'top' => "/Administrate/FormBuilders/edit/$modelType/$formBuilderId",
    'bottom' => array(
        'cancel' => "/Administrate/FormBuilders/detail/$modelType/$formBuilderId",
    )
);
$dataKeys = array("masterProd", "masterTest");
if (!empty($master)){
    $dataKeys = array("masterProd", "masterTest", "detailOldProd", "detailProd", "fbDeletable", "detailTest");
    $structureLinks = array(
        'top' => "/Administrate/FormBuilders/edit/$modelType/$formBuilderId/$controlId",
        'bottom' => array(
            'cancel' => "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/$controlId"
        )
    );
    $this->request->data = $data['control'];
    $structureOptions = array(
        'type' => 'detail',
        'settings' =>array(
            'header' => __("form information"),
            'form_bottom' => false,
            'actions' => false
        )
    );
    if ($isTestMode){
        $structureOptions['type'] = 'edit';
        $structureOptions['links'] = array(
            'top' => $structureLinks
        );
    }
    $this->Structures->build($atimStructure, $structureOptions);
}
$dataKeys = ($model == 'MiscIdentifierControl')?array():$dataKeys;

foreach ($dataKeys as $index){
    if (empty($data[$index]) && in_array($index, array("masterProd", "detailOldProd", "detailProd", "fbDeletable"))!==false){
        continue;
    }
    $this->request->data = $data[$index];
    
    $tmpTitle = '';
    $tmpDescription = '';
    switch($index) {
        case 'masterTest':
            $tmpTitle = __(!empty($master)? 'fb_master_model_fields_in_test' : 'fb_simple_model_fields_in_test');
            $tmpDescription = !empty($master)? __('fb_master_model_fields_in_test_description_[%s]', $atimMenuVariables['FormBuilder.label']) :  __('These fields will be shown on all the %s forms', $atimMenuVariables['FormBuilder.label']);
            break;
        case 'detailTest':
            $tmpTitle = __('fb_detail_model_fields_in_test');
            $tmpDescription = __('fb_detail_model_fields_in_test_description_[%s]', $atimMenuVariables['Form.name']);
            break;
    }
    $structureOptions = array(
        'links' => array(
            'top' => $structureLinks
        ),
        'type' => 'editgrid',
        'settings' =>array(
            'header' => array(
                'title' => $tmpTitle,
                'description' => $tmpDescription
            ), 
            'data_miss_warn' => false,
            'actions' => false,
            'form_bottom' => false,
            'name_prefix' => $index,
            'prefix_included' => true,
            'class' => $index,
            'no_sanitization' => [
                'FunctionManagement' => ['is_structure_value_domain']
            ]
        )
    );
    $atimStructureFB = $atimStructureProd;
    if (substr($index, -4, 4)=='Test'){
        $structureOptions['settings']['add_fields'] = true;
        $structureOptions['settings']['del_fields'] = true;
        $structureOptions["settings"]['name_prefix'] = $index;
        $structureOptions["settings"]['prefix_included'] = true;
        $atimStructureFB = $atimStructureTest;
        if (empty($data[$index])){
            $structureOptions['type'] = 'addgrid';
            $structureOptions['settings']['add_gride_empty'] = true;
        }
    }elseif($index == 'fbDeletable'){
        $atimStructureFB = $atimStructureDeletable;
        $structureOptions['settings']['add_fields'] = false;
        $structureOptions['settings']['del_fields'] = true;
        $structureOptions['type'] = 'editgrid';
    }else{
        $tmpTitle = '';
        $tmpDescription = '';
        switch($index) {
            case 'masterProd':
                $tmpTitle = __(!empty($master)? 'fb_master_model_fields_in_prod' : 'fb_simple_model_fields_in_prod');
                $tmpDescription = !empty($master)? __('fb_master_model_fields_in_prod_description_[%s]', $atimMenuVariables['FormBuilder.label']) : __('fields shown on all the %s forms', $atimMenuVariables['FormBuilder.label']);
                break;
            case 'detailOldProd':
                $tmpTitle = __('fb_detail_model_old_fields_in_prod');
                $tmpDescription = __('fb_detail_model_old_fields_in_prod_description_[%s]', $atimMenuVariables['Form.name']);
                break;
            case 'detailProd':
                $tmpTitle = __('fb_detail_model_fields_in_prod');
                $tmpDescription = __('fb_detail_model_fields_in_prod_description_[%s]', $atimMenuVariables['Form.name']);
                break;
        }
        $structureOptions = array(
            'type' => 'index',
            'settings' =>array(
                'header' => array(
                    'title' => $tmpTitle,
                    'description' =>  $tmpDescription,
                ),
                'pagination' => 0,
                'actions' => false,
                'data_miss_warn'=> false,
                'no_sanitization' => [
                    'FunctionManagement' => ['is_structure_value_domain']
                ]
            )
        );
    }
    
    if (isset($options["prefix-common"])){
        $structureOptions["settings"]['name_prefix'] = $options["prefix-common"];
        $structureOptions["settings"]['prefix_included'] = true;
    }

    $this->Structures->build($atimStructureFB, $structureOptions);
}

$emptyStructureOptions['links'] = $structureLinks;
$emptyStructureOptions['settings']['actions'] = true;
$this->Structures->build($emptyStructure, $emptyStructureOptions);

$this->request->data["validationData"] = $data['validationData'];
$this->request->data["valueDomainData"] = $data['valueDomainData'];
$this->request->data["settings"] = $data['settings'];
?>

<style>
    input[type=checkbox]{
        display: inline-block;
        /*width: 100%;*/
    }
</style>

<script>
    var formBuilderScriptsActive = true;
    var i18n = Array();
    var typeValue = "";
    var warningValidationMessage = "<?php echo __("check the validations rules after changing the data type");?>";
    var warningValueDomainMessage = "<?php echo __("please select the list items");?>";
//    var confirmMessage = "<?php  echo __("do you want to update the labels?");?>";
//    var alertMessage = "<?php echo __("the labels are already exist and unchangeable");?>";
    var addValidationMessage = "<?php echo __("click to add the validation rules");?>";
    var addValueDomainMessage = "<?php echo __("click to add/modify list");?>";
    var errorToFromMesage = "<?php echo __("should complete both or none of them");?>";
    var validationData = '<?php echo (isset($this->request->data['validationData'])?$this->request->data['validationData']:""); ?>';
    var valueDomainData = '<?php echo (isset($this->request->data['valueDomainData'])?$this->request->data['valueDomainData']:""); ?>';
    var fbSettingsData = '<?php echo (isset($this->request->data['settings'])?$this->request->data['settings']:""); ?>';
    var fbErrorsGrid = '<?php echo (isset($errorsGrid)?$errorsGrid:""); ?>';
    var addFieldToMaster = "<?php echo __("are you sure to add a field to master?"); ?>";

    var copyStr = "<?php echo(__("copy", null)); ?>";
    var pasteStr = "<?php echo(__("paste")); ?>";
    var copyingStr = "<?php echo(__("copying")); ?>";
    var pasteOnAllLinesStr = "<?php echo(__("paste on all lines")); ?>";
    var copyControl = true;
    var noResultMessage = "<?php echo(___("no result")); ?>";
    var placeHolderMessage = "<?php echo(___("select an option")); ?>";
    var multiplechoiceMessage = "<?php echo(___("(multiple choice)")); ?>";

    var confirmChangeApplyOnOptionMessage = "<?php echo __('change this option maybe will reset the selected samples and aliquots. do you want to continue?') ?>";
    var sampleMasterIds = JSON.parse('<?php echo json_encode($sampleAliquotIds) ?>');

</script>

<?php
echo $this->Html->script('form-builder');