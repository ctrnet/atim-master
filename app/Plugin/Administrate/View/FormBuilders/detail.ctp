<?php
extract($formData);
$controlId = (empty($controlId))?null:$controlId;

$structureLinks = array(
    'bottom' => array(
        'edit' => "/Administrate/FormBuilders/edit/$modelType/$formBuilderId/$controlId",
        'preview' => array(
            'link' => "/Administrate/FormBuilders/preview/$formBuilderId/$controlId",
            'icon' => 'enabled',
            'title' => __('see the preview of the form')
        ),
        'copy' => "/Administrate/FormBuilders/copyControl/$modelType/$formBuilderId/$controlId",
        'read only mode' => array(
            "link" => "/Administrate/FormBuilders/disable/$modelType/$formBuilderId/$controlId",
            "icon" => "is-disable",
            'title' => __('make disable the control')
        ),
        'read/write mode' => array(
            'link' => "/Administrate/FormBuilders/enable/$modelType/$formBuilderId/$controlId",
            "icon" => "confirm",
            'title' => __('make enable the control')
        ),
        'deactivate' => array(
            "link" => "/Administrate/FormBuilders/deactivate/$modelType/$formBuilderId/$controlId",
            "icon" => "is-disable",
            'title' => __('deactivate the control')
        ),
        'activate' => array(
            'link' => "/Administrate/FormBuilders/activate/$modelType/$formBuilderId/$controlId",
            "icon" => "confirm",
            'title' => __('activate the control')
        ),
        'delete' => "/Administrate/FormBuilders/delete/$modelType/$formBuilderId/$controlId",
        'dictionary' => array(
            'link' => "/Administrate/FormBuilders/dictionary/$modelType/$formBuilderId/$controlId",
            'icon' => "lab_book",
            'title' => __('add the new words in to en & fr dic')
        ),
        'testToProd' => array(
            'link' => "/Administrate/FormBuilders/testToProd/$modelType/$formBuilderId/$controlId",
            'icon' => "reports_download",
            'title' => __('change the mode from test to production mode')
        )
    ),
    'ajax' => array(
        'bottom' => array(
            'preview' => array(
                'json' => array(
                    'callback' => 'formBuilderPreview'
                )
            ),
            'dictionary' => array(
                'json' => array(
                    'callback' => 'formBuilderDictionary'
                )
            ),
            'testToProd' => array(
                'json' => array(
                    'callback' => 'formBuilderTestToProd'
                )
            ),
            'read only mode' => array(
                'json' => array(
                    'callback' => 'formBuilderDisable'
                )
            ),
            'read/write mode' => array(
                'json' => array(
                    'callback' => 'formBuilderDisable'
                )
            ),
            'deactivate' => array(
                'json' => array(
                    'callback' => 'formBuilderActivateDeactivate'
                )
            ),
            'activate' => array(
                'json' => array(
                    'callback' => 'formBuilderActivateDeactivate'
                )
            ),
            'copy' => array(
                'json' => array(
                    'callback' => 'formBuilderCopy'
                )
            ),
        )
    )
);

if (!$controlId){
    unset($structureLinks['bottom']['copy']);
    unset($structureLinks['bottom']['read only mode']);
    unset($structureLinks['bottom']['read/write mode']);
    unset($structureLinks['bottom']['delete']);
    unset($structureLinks['bottom']['activate']);
    unset($structureLinks['bottom']['deactivate']);
    unset($structureLinks['ajax']['bottom']['read only mode']);
}
if (isset($fbSettings['copy']) && empty($fbSettings['copy'])){
    unset($structureLinks['bottom']['copy']);
}

if ($model == 'MiscIdentifierControl'){
    unset($structureLinks['bottom']['copy']);
    unset($structureLinks['bottom']['preview']);
}

if($controlId){
    if ($data['control'][$model]['flag_active_input'] == 0){
        unset($structureLinks['ajax']['bottom']['read only mode']);
        unset($structureLinks['bottom']['read only mode']);
    }else{
        unset($structureLinks['ajax']['bottom']['read/write mode']);
        unset($structureLinks['bottom']['read/write mode']);
    }
    if($data['control'][$model]['flag_test_mode']){
        unset($structureLinks['ajax']['bottom']['deactivate']);
        unset($structureLinks['bottom']['deactivate']);
        unset($structureLinks['ajax']['bottom']['activate']);
        unset($structureLinks['bottom']['activate']);
    }

    if ($data['control'][$model]['flag_active'] == 0){
        unset($structureLinks['ajax']['bottom']['deactivate']);
        unset($structureLinks['bottom']['deactivate']);
    }else{
        unset($structureLinks['ajax']['bottom']['activate']);
        unset($structureLinks['bottom']['activate']);
    }
    if ($model == 'AliquotControl' || $model == 'SampleControl'){
        unset($structureLinks['ajax']['bottom']['copy']);
        unset($structureLinks['bottom']['copy']);
        unset($structureLinks['ajax']['bottom']['read only mode']);
        unset($structureLinks['bottom']['read only mode']);
        unset($structureLinks['ajax']['bottom']['read/write mode']);
        unset($structureLinks['bottom']['read/write mode']);
        unset($structureLinks['ajax']['bottom']['delete']);
        unset($structureLinks['bottom']['delete']);
//        unset($structureLinks['ajax']['bottom']['activate']);
//        unset($structureLinks['bottom']['activate']);
        unset($structureLinks['ajax']['bottom']['deactivate']);
        unset($structureLinks['bottom']['deactivate']);
    }

}

$actions = [];
$found = false;
foreach (["detailTest", "detailProd", "detailOldProd", "masterTest", "masterProd", "control"] as $value){
    $actions[$value] = !empty($data[$value]) && !$found;
    $found = !empty($data[$value]) || $found;
}

if (!empty($data["control"])){
    $this->request->data = $data["control"];
    $action = $actions["control"];
    $structureOptions = array(
        'type' => 'detail',
        'settings' =>array(
            'header' => __("form information"),
            'pagination' => 0,
            'actions' => $action,
            'no_sanitization' => [
                'FunctionManagement' => ['is_structure_value_domain']
            ]
        )
    );

    $this->Structures->build($atimStructureForDetailControl, $structureOptions);
}

if (!empty($data["masterProd"])){
    $this->request->data = $data["masterProd"];
    $action = $actions["masterProd"];
    $structureOptions = array(
        'type' => 'index',
        'settings' =>array(
            'header' => array(
                'title' => $controlId? __('fb_master_model_fields_in_prod') : __('fb_simple_model_fields_in_prod'),
                'description' => $controlId? __('fb_master_model_fields_in_prod_description_[%s]', $atimMenuVariables['FormBuilder.label']) : __('fields shown on all the %s forms', $atimMenuVariables['FormBuilder.label'])
            ),
            'pagination' => 0,
            'data_miss_warn'=> false,
            'actions' => $action,
            'no_sanitization' => [
                'FunctionManagement' => ['is_structure_value_domain']
            ]

        )
    );

    $this->Structures->build($atimStructureForControl, $structureOptions);
}

if (!empty($data["masterTest"])){
    $this->request->data = $data["masterTest"];
    $action = $actions["masterTest"];
    $structureOptions = array(
        'type' => 'index',
        'settings' =>array(
            'header' => array(
                'title' => $controlId? __('fb_master_model_fields_in_test') : __('fb_simple_model_fields_in_test'),
                'description' => $controlId? __('fb_master_model_fields_in_test_description_[%s]', $atimMenuVariables['FormBuilder.label']) : __('These fields will be shown on all the %s forms', $atimMenuVariables['FormBuilder.label'])
            ),
            'pagination' => 0,
            'data_miss_warn'=> false,
            'actions' => $action,
            'no_sanitization' => [
                'FunctionManagement' => ['is_structure_value_domain']
            ]

        )
    );

    $this->Structures->build($atimStructureForControl, $structureOptions);
}

if (!empty($data["detailOldProd"])){
    $this->request->data = $data["detailOldProd"];
    $action = $actions["detailOldProd"];
    $structureOptions = array(
        'type' => 'index',
        'settings' =>array(
            'header' => array(
                'title' => __('fb_detail_model_old_fields_in_prod'),
                'description' => __('fb_detail_model_old_fields_in_prod_description_[%s]', $atimMenuVariables['Form.name'])
            ),
            'pagination' => 0,
            'data_miss_warn'=> false,
            'actions' => $action,
            'no_sanitization' => [
                'FunctionManagement' => ['is_structure_value_domain']
            ]

        )
    );

    $this->Structures->build($atimStructureForControl, $structureOptions);
}

if (!empty($data["detailProd"])){
    $this->request->data = $data["detailProd"];
    $action = $actions["detailProd"];
    $structureOptions = array(
        'type' => 'index',
        'settings' =>array(
            'header' => array(
                'title' => __('fb_detail_model_fields_in_prod'),
                'description' => __('fb_detail_model_fields_in_prod_description_[%s]', $atimMenuVariables['Form.name'])
            ),
            'pagination' => 0,
            'data_miss_warn'=> false,
            'actions' => $action,
            'no_sanitization' => [
                'FunctionManagement' => ['is_structure_value_domain']
            ]

        )
    );

    $this->Structures->build($atimStructureForControl, $structureOptions);
}

if (!empty($data["detailTest"])){
    $this->request->data = $data["detailTest"];
    $action = $actions["detailTest"];
    $structureOptions = array(
        'type' => 'index',
        'settings' =>array(
            'header' => array(
                'title' => __('fb_detail_model_fields_in_test'),
                'description' => __('fb_detail_model_fields_in_test_description_[%s]', $atimMenuVariables['Form.name'])
            ),
            'pagination' => 0,
            'data_miss_warn'=> false,
            'actions' => $action,
            'no_sanitization' => [
                'FunctionManagement' => ['is_structure_value_domain']
            ]

        )
    );

    $this->Structures->build($atimStructureForControl, $structureOptions);
}

$structureOptions = array(
    'links' => $structureLinks,
    'type' => 'index',
        'settings' =>array(
            'pagination' => 0,
            'no_sanitization' => [
                'FunctionManagement' => ['is_structure_value_domain']
            ]

        )
);
$this->Structures->build([], $structureOptions);
?>
<script>
    var formBuilderScriptsActive = true;
    var disableLinkMessage = "<?php echo __("the links are disabled in the preview window"); ?>";
    var noDataDictionary = "<?php echo __("there is no data dictionary"); ?>";
//    var noTestToProdData = "<?php echo __("there is no field for save in this form"); ?>";
    var noTestToProdConfirmation = "<?php echo __("are you sure to submit the structure fields change don in FB?"); ?>";
    var disableTheControl = "<?php echo __("are you sure to make disable the control"); ?>";
    var enableTheControl = "<?php echo __("are you sure to make enable the control"); ?>";
    var deactivateTheControl = "<?php echo __("are you sure to activate the control"); ?>";
    var activateTheControl = "<?php echo __("are you sure to deactivate the control"); ?>";
    var copyTheControl = "<?php echo __("are you sure to make a copy of the control"); ?>";


    var i18n = Array();
    var typeValue = "";
    var warningValidationMessage = "<?php echo __("check the validations rules after changing the data type");?>";
    var warningValueDomainMessage = "<?php echo __("please select the list items");?>";
    var addValidationMessage = "<?php echo __("click to add the validation rules");?>";
    var addValueDomainMessage = "<?php echo __("click to add/modify list");?>";
    var fbSettingsData = '<?php echo (isset($this->request->data['settings'])?$this->request->data['settings']:""); ?>';
    var errorToFromMesage = "<?php echo __("should complete both or none of them");?>";
    var validationData = "";
    var valueDomainData = "";
    var fbErrorsGrid = "";
    var addFieldToMaster = "<?php echo __("are you sure to add a field to master?"); ?>";

    var copyStr = "<?php echo(__("copy", null)); ?>";
    var pasteStr = "<?php echo(__("paste")); ?>";
    var copyingStr = "<?php echo(__("copying")); ?>";
    var pasteOnAllLinesStr = "<?php echo(__("paste on all lines")); ?>";
    var copyControl = true;
    var noResultMessage = "<?php echo(___("no result")); ?>";
    var placeHolderMessage = "<?php echo(___("select an option")); ?>";
</script>
<?php
echo $this->Html->script('form-builder');