<?php
extract($formData);
$this->request->data = $data;
$header = $model;

if (!empty($master)){
    $header = __("control detail");
    $structureOptions = array(
        'links' => array(
            'top' => "/Administrate/FormBuilders/add/$modelType/$formBuilderId",
        ),
        'type' => 'add',
        'settings' =>array(
            'header' => __("form information"),
            'form_bottom' => false,
            'actions' => false
        )
    );

    $this->Structures->build($atimStructure, $structureOptions);
}

if (!empty($masterData['masterProd'])){
    $this->request->data = $masterData['masterProd'];
    $structureOptions = array(
        'type' => 'editgrid',
        'settings' => array(
            'header' => array(
                'title' => __('fb_master_model_fields_in_prod'),
                'description' => __('fb_master_model_fields_in_prod_description_[%s]', $atimMenuVariables['FormBuilder.label'])
            ),
            // 'add_fields' => true,
            // 'del_fields' => true,
            'data_miss_warn' => false,
            'actions' => false,
            'no_sanitization' => [
                'FunctionManagement' => ['is_structure_value_domain']
            ]
        )
    );
    $this->Structures->build($atimStructureForMaster, $structureOptions);
}
if ($model != 'MiscIdentifierControl'){
    $this->request->data = $data;
    $structureLinks = array(
        'top' => "/Administrate/FormBuilders/add/$modelType/$formBuilderId",
        'bottom' => array(
            'cancel' => (!empty($master))?"/Administrate/FormBuilders/listAll/$modelType/$formBuilderId":"/Administrate/FormBuilders/index"
        )
    );

    $structureOptions = array(
        'links' => $structureLinks,
        'type' => 'addgrid',
        'settings' =>array(
            'header' => array(
                'title' => __('fb_detail_model_fields_of_new_form'),
                'description' => __('fb_detail_model_fields_of_new_form_description')
            ),
            'add_fields' => true,
            'del_fields' => true,
            'class' => "common"
        )
    );
    if (isset($options["prefix-common"])){
        $structureOptions["settings"]['name_prefix'] = $options["prefix-common"];
        $structureOptions["settings"]['prefix_included'] = true;
    }
    $this->Structures->build($atimStructureForControl, $structureOptions);
}else{
    $structureLinks = array(
        'top' => "/Administrate/FormBuilders/add/$modelType/$formBuilderId",
        'bottom' => array(
            'cancel' => (!empty($master))?"/Administrate/FormBuilders/listAll/$modelType/$formBuilderId":"/Administrate/FormBuilders/index"
        )
    );

    $structureOptions = array(
        'links' => $structureLinks,
        'type' => 'add',
    );
    $this->Structures->build($atimStructureForControl, $structureOptions);
}
?>

<script>
    var formBuilderScriptsActive = true;
    var i18n = Array();
    var typeValue = "";
    var warningValidationMessage = "<?php echo __("check the validations rules after changing the data type");?>";
    var warningValueDomainMessage = "<?php echo __("please select the list items");?>";
//    var confirmMessage = "<?php  echo __("do you want to update the labels?");?>";
//    var alertMessage = "<?php echo __("the labels are already exist and unchangeable");?>";
    var addValidationMessage = "<?php echo __("click to add the validation rules");?>";
    var addValueDomainMessage = "<?php echo __("click to add/modify list");?>";
    var errorToFromMesage = "<?php echo __("should complete both or none of them");?>";
    var validationData = '<?php echo (isset($this->request->data['validationData'])?$this->request->data['validationData']:""); ?>';
    var valueDomainData = '<?php echo (isset($this->request->data['valueDomainData'])?$this->request->data['valueDomainData']:""); ?>';
    var fbSettingsData = '<?php echo (isset($this->request->data['settings'])?$this->request->data['settings']:""); ?>';
    var fbErrorsGrid = '<?php echo (isset($errorsGrid)?$errorsGrid:""); ?>';

    var copyStr = "<?php echo(__("copy", null)); ?>";
    var pasteStr = "<?php echo(__("paste")); ?>";
    var copyingStr = "<?php echo(__("copying")); ?>";
    var pasteOnAllLinesStr = "<?php echo(__("paste on all lines")); ?>";
    var copyControl = true;
    var noResultMessage = "<?php echo(___("no result")); ?>";
    var placeHolderMessage = "<?php echo(___("select an option")); ?>";
    var multiplechoiceMessage = "<?php echo(___("(multiple choice)")); ?>";

    var confirmChangeApplyOnOptionMessage = "<?php echo __('change this option maybe will reset the selected samples and aliquots. do you want to continue?') ?>";
    var sampleMasterIds = JSON.parse('<?php echo json_encode($sampleAliquotIds) ?>');

</script>

<?php
echo $this->Html->script('form-builder');
?>
<style>
    input[type=checkbox]{
        display: inline-block;
        width: 100%;
    }
</style>