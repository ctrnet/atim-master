<?php
$this->request->data = $indexData['model'];
$structureLinks = array(
    'index' => array(
        'form detail' => array(
            'link' => '/Administrate/FormBuilders/detail/simple_model/' . '%%FormBuilder.id%%',
            'icon' => 'detail'
        )
    )
);

$structureOptions = array(
    'links' => $structureLinks,
    'type' => 'index',
    'settings' => array(
        'header' =>  [
            'title' => __("form builder simple_model header") . ' : ' . __('list of forms'),
            'description' => __('form builder simple_model description'),
        ],
        'pagination' => 0,
        'actions' => false
    )
);

$this->Structures->build($atimStructure, $structureOptions);

$this->request->data = $indexData['control'];
$structureLinks = array(
    'index' => array(
        'list of forms' => array(
            'link' => '/Administrate/FormBuilders/listAll/master_detail_model/' . '/%%FormBuilder.id%%',
            'icon' => 'detail'
        )
    )
);

$structureOptions = array(
    'links' => $structureLinks,
    'type' => 'index',
    'settings' => array(
        'header' =>  [
            'title' => __("form builder master_detail_model header") . ' : ' . __('types of forms'),
            'description' => __("form builder master_detail_model description"),
        ],        
        'pagination' => 0,
        'actions' => false
    )
);

$this->Structures->build($atimStructureMulti, $structureOptions);

$this->request->data = $indexData['other'];
$structureLinks = array(
    'index' => array(
        'list of forms' => array(
            'link' => '/Administrate/FormBuilders/listAll/other_model/' . '/%%FormBuilder.id%%',
            'icon' => 'detail'
        )
    )
);

$structureOptions = array(
    'links' => $structureLinks,
    'type' => 'index',
    'settings' => array(
        'header' => [
            'title' => __("form builder other_model header") . ' : ' . __('types of forms'),
            'description' => __("form builder other_model description"),
        ],  
        'pagination' => 0,
        'actions' => true
    )
);

$this->Structures->build($atimStructureMulti, $structureOptions);