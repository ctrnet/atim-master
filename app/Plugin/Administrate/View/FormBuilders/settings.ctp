<?php
$structureOptions = array(
    'links' => array(
        'top' => "/Administrate/FormBuilders/add/1/1",
    ),
    'type' => 'edit',
    'settings' =>array(
        'header' => __("the field settings"),
    )
);

$this->Structures->build($formBuilderSettingStructure, $structureOptions);