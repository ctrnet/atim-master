<?php

$data = $formData["data"];
$formBuilderId = $formData["formBuilderId"];
$controlId = $formData["controlId"];
$model = $formData["model"];

$structureLinks = array(
    'bottom' => array(
        'back' => "/Administrate/FormBuilders/listAll/$modelType/$formBuilderId",
        'edit' => "/Administrate/FormBuilders/edit/$modelType/$formBuilderId/$controlId",
        'clone' => "/Administrate/FormBuilders/copyControl/$modelType/$formBuilderId/$controlId",
        'disable' => "/Administrate/FormBuilders/disable/$modelType/$formBuilderId/$controlId",
        'dictionary' => "/Administrate/FormBuilders/dictionary/$modelType/$formBuilderId/$controlId",
        'delete' => "/Administrate/FormBuilders/delete/$modelType/$formBuilderId/$controlId",
    )
);
$this->request->data = $data["control"];

$structureOptions = array(
    'type' => 'index',
    'data' => $data["master"],
    'settings' =>array(
        'header' => __("not editable fields"), 
        'pagination' => 0
    )
);

$this->Structures->build($atimStructureForControl, $structureOptions);