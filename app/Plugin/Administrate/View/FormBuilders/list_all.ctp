<?php
extract($formData);
$this->request->data = $indexData;
$structureLinks = array(
    'index' => array(
        'detail' => "/Administrate/FormBuilders/detail/$modelType/$formBuilderId/%%$model.id%%"
    ),
    'bottom' => array(
        'add' => "/Administrate/FormBuilders/add/$modelType/$formBuilderId"
    )
);
if (isset($fbSettings['add']) && empty($fbSettings['add'])){
    unset($structureLinks['bottom']['add']);
}
$structureOptions = array(
    'links' => $structureLinks,
    'type' => 'index',
    'settings' =>array(
        'header' => __('list of forms'),
        'pagination' => 1
    )
);
$this->Structures->build($atimStructure, $structureOptions);
echo $this->Html->script('form-builder');
?>
<script>
    var disableLinkMessage = "<?php echo __("the links are disabled in the preview window"); ?>";
</script>