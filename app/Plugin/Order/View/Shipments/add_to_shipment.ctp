<?php
$structureLinks = array(
    'top' => '/Order/Shipments/addToShipment/' . $atimMenuVariables['Order.id'] . '/' . $atimMenuVariables['Shipment.id'] . "/$orderLineId",
    'bottom' => array(
        'cancel' => '/Order/Shipments/detail/' . $atimMenuVariables['Order.id'] . '/' . $atimMenuVariables['Shipment.id'] . '/'
    ),
    'checklist' => array(
        'OrderItem.id][' => '%%OrderItem.id%%'
    )
);

$structureSettings = array(
    'pagination' => true,
    'header' => __('add items to shipment', null),
    'form_inputs' => false,
    'confirmation_msg' => __('multi_entry_form_confirmation_msg')
);

$finalOptions = array(
    'type' => 'index',
    'links' => $structureLinks,
    'settings' => $structureSettings,    
    'extras' => '
		<input type="hidden" name="data[max_input_vars_validation]" value="1" class = "max_input_vars_validation" />'
);

if (isset($languageHeading)) {
    $finalOptions['settings']['language_heading'] = $languageHeading;
}

$finalAtimStructure = $atimStructure;

// CUSTOM CODE
$hookLink = $this->Structures->hook();

// BUILD FORM

$this->Structures->build($finalAtimStructure, $finalOptions);

?>
<script>
    var dataLimit=<?php echo $dataLimit;?>;
    var dataIndex="<?php echo "OrderItem";?>";
    var controller="<?php echo "Shipments";?>";
    var action="<?php echo "addToShipment";?>";
    var compressAllFormData = '<?php echo $this->Structures->compressPostData(); ?>';
</script>