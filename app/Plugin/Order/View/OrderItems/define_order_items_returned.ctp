<?php
$structureLinks = array(
    'top' => '/Order/OrderItems/defineOrderItemsReturned/' . $orderId . '/' . $orderLineId . '/' . $shipmentId . '/',
    'bottom' => array(
        'cancel' => $urlToCancel
    )
);

$finalOptions = array(
    'type' => 'editgrid',
    'links' => $structureLinks,
    'settings' => array(
        'pagination' => false,
        'header' => __('order items'),
        'paste_disabled_fields' => array(
            'OrderItem.order_item_shipping_label',
            'OrderItem.date_added',
            'OrderItem.added_by'
        ),
        'confirmation_msg' => __('multi_entry_form_confirmation_msg')
    ),
    'extras' => '
		<input type="hidden" name="data[url_to_cancel]" value="' . $urlToCancel . '"/><input type="hidden" name="data[order_item_ids]" value="' . $orderItemIds . '"/>
		<input type="hidden" name="data[max_input_vars_validation]" value="1" class = "max_input_vars_validation" />'
);

$finalAtimStructure = $atimStructure;

// CUSTOM CODE
$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}

// BUILD FORM
$this->Structures->build($finalAtimStructure, $finalOptions);

?>
<script type="text/javascript">
var copyControl = true;
var compressAllFormData = '<?php echo $this->Structures->compressPostData(); ?>';
</script>