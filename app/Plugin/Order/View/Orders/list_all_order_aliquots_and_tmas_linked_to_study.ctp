<?php
$structureLinks = array();

$structureLinks['index'] = array(
    'items details' => array(
        'link' => '%%Generated.item_detail_link%%/',
        'icon' => 'detail'
    )
);

$structureOverride = array();

$finalAtimStructure = $atimStructure;
$finalOptions = array(
    'type' => 'index',
    'links' => $structureLinks,
    'override' => $structureOverride
);

// CUSTOM CODE
$hookLink = $this->Structures->hook();
if ($hookLink) {
    require ($hookLink);
}

// BUILD FORM
$this->Structures->build($finalAtimStructure, $finalOptions);