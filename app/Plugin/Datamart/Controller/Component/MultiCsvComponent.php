<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
 * @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

class MultiCsvComponent extends Component
{

    private  $controller;
    private static $limit;
    private static $page;
    private static $limitPerQuery = 5000;
    private static $pageNumber = 1;

    public function initialize(Controller $controller)
    {
        parent::initialize($controller);
        $this->controller = $controller;

        $this->initiateThePaginationParameters();
    }

    private function initiateThePaginationParameters()
    {
        self::$limit = self::$limitPerQuery;
        self::$page = self::$pageNumber;
    }

    public function downLoadMultipleCSV($data)
    {
        if (!empty($data['multiCSV'])) {
            $data = $data['multiCSV'];
        }
        $filenames = [];
        $time = date("Y-m-d h\hi-s");
        $baseDir = APP . "tmp" . DS . "csv" . DS;
        if (!is_dir($baseDir)){
            mkdir($baseDir, 0777, true);
            if (!is_dir($baseDir)){
                $this->controller->atimFlashError(__('can not create the csv directory, contact the administrator'), 'javascript::back()');
            }
        }

        $directory = $baseDir . $time . DS;
        foreach ($data as $datum) {
            extract($datum);
            $filename = $directory . $filename;
            $filenames[] = $filename;
            
            $this->controller->set("directory", $directory);
            $this->controller->set("csvHeader", $filename);
            $this->controller->set("filename", $filename);
            $this->controller->set("saveInFile", true);
            $this->controller->Structures->set($alias, "resultStructure");

            $this->controller->request->data = $datum['data'];
            $this->controller->render('../Csv/csv');

        }

        $filenames = implode("|||", $filenames);
        $this->controller->set("filenames", $filenames);

        $this->controller->redirect("/Datamart/Reports/getFileContent?directory=$directory");
    }

    public function getDataChunk($data, $conditions)
    {
        extract($data);
        $conditions += [
            'conditions' => [
                $model->alias . '.' . $model->primaryKey => $ids
            ],
            'page' => self::$page,
            'limit' => self::$limit,
        ];
        $tmpData = $model->find('all', $conditions);
        if (!empty($tmpData)) {
            self::$page++;
        } else {
            $this->initiateThePaginationParameters();
        }
        return $tmpData;
    }

    public function checkOptions($report)
    {
        if (!empty($report['Report']['options'])){
            $options = array_map('trim', explode(",", $report['Report']['options']));
            foreach ($options as $option){
                $option = array_map('trim', explode("=", $option));
                if (strtolower($option[0]) == 'multi-csv' && strtolower($option[1]) == 'true'){
                    $this->controller->set('reportPopUp', "/Datamart/Reports/manageReport/" . $report['Report']['id']);
                    $report['Report']['multiCSV'] = true;
                }
            }
        }
        return $report;
    }
}