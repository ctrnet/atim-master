<?php

/**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

/**
 * Class BrowsingIndex
 */
class BrowsingIndex extends DatamartAppModel
{

    public $useTable = "datamart_browsing_indexes";

    public $belongsTo = array(
        'BrowsingResult' => array(
            'className' => 'BrowsingResult',
            'foreignKey' => 'root_node_id'
        )
    );

    public $tmpBrowsingLimit = 5;

    public function summary($variables = array())
    {
        $return = false;

        if (isset($variables['BrowsingIndex.id'])) {
            $result = $this->find('first', array(
                'conditions' => array(
                    'BrowsingIndex.id' => $variables['BrowsingIndex.id']
                )
            ));

            $userModel = $this->getInstance('', 'User');
            $userData = $userModel->findById($result['BrowsingIndex']['created_by']);

            $firstLastName = [];
            if (! empty($userData['User']['first_name'])) {
                $firstLastName[] = $userData['User']['first_name'];
            }

            if (! empty($userData['User']['last_name'])) {
                $firstLastName[] = $userData['User']['last_name'];
            }

            if (empty($firstLastName)) {
                $firstLastName[] = $userData['User']['username'];
            }
            unset($userData);
            $firstLastName = implode(" ", $firstLastName);

            $result['Generated']['created_by_data'] = $firstLastName;

            $return['data'] = $result;
            $return['menu'] = [
                null,
                __("browsing #") . $result['BrowsingIndex']['id']
            ];
            $return['title'] = [
                null,
                __("browsing #") . $result['BrowsingIndex']['id']
            ];

            $return['structure alias'] = 'datamart_browsing_indexes';
        }

        return $return;
    }
}