<?php
 /**
 *
 * ATiM - Advanced Tissue Management Application
 * Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 *
 * Licensed under GNU General Public License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Canadian Tissue Repository Network <info@ctrnet.ca>
 * @copyright     Copyright (c) Canadian Tissue Repository Network (http://www.ctrnet.ca)
 * @link          http://www.ctrnet.ca
 * @since         ATiM v 2
* @license       http://www.gnu.org/licenses  GNU General Public License Version 3
 */

$allFields = !empty(AppController::getInstance()->csvConfig['type']) && AppController::getInstance()->csvConfig['type'] == 'all';
 // used by csv/csv and browser/csv
$content = $this->Structures->build($resultStructure, array(
    'type' => 'csv',
    'settings' => array(
        'csv_header' => $csvHeader,
        'all_fields' => $allFields,
        'filename' => !empty($filename) ? $filename : "",
        'saveInFile' => !empty($saveInFile) ? true : false,
    )
));

if (!empty($saveInFile) && !empty($filename)) {
    if (!is_dir($directory)){
        mkdir($directory);
    }
    file_put_contents($filename, $content, FILE_APPEND);
}else{
    echo $content;
}
ob_flush();