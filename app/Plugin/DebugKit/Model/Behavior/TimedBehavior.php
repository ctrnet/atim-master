<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         DebugKit 1.3
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('DebugTimer', 'DebugKit.Lib');

/**
 * Class TimedBehavior
 *
 * @since         DebugKit 1.3
 */
class TimedBehavior extends ModelBehavior {

/**
 * Behavior settings
 *
 * @var array
 */
	public $settings = array();

/**
 * Default setting values
 *
 * @var array
 */
	protected $_defaults = array();

/**
 * Setup the behavior and import required classes.
 *
 * @param \Model|object $model Model using the behavior
 * @param array $settings Settings to override for model.
 * @return void
 */
	public function setup(Model $model, $settings = null) {
		if (is_array($settings)) {
			$this->settings[$model->alias] = array_merge($this->_defaults, $settings);
		} else {
			$this->settings[$model->alias] = $this->_defaults;
		}
	}

/**
 * beforeFind, starts a timer for a find operation.
 *
 * @param Model $model The model.
 * @param array $queryData Array of query data (not modified)
 * @return bool true
 */
	public function beforeFind(Model $model, $queryData) {
		DebugTimer::start($model->alias . '_find', $model->alias . '->find()');
		return true;
	}

/**
 * afterFind, stops a timer for a find operation.
 *
 * @param Model $model The mdoel.
 * @param array $results Array of results
 * @param bool $primary Whether this model is being queried directly (vs. being queried as an association)
 * @return bool true.
 */
	public function afterFind(Model $model, $results, $primary = false) {
		DebugTimer::stop($model->alias . '_find');
		return true;
	}

/**
 * beforeSave, starts a time before a save is initiated.
 *
 * @param Model $model The model.
 * @param array $options The options.
 * @return bool Always true.
 */
	public function beforeSave(Model $model, $options = array()) {
		DebugTimer::start($model->alias . '_save', $model->alias . '->save()');
		return true;
	}

/**
 * afterSave, stop the timer started from a save.
 *
 * @param \Model $model The model.
 * @param string $created True if this save created a new record.
 * @param array $options The options.
 * @return bool Always true.
 */
	public function afterSave(Model $model, $created, $options = array()) {
		DebugTimer::stop($model->alias . '_save');
		return true;
	}

}