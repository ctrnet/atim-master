<?php
/**
 * View Variables Panel Element
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         DebugKit 1.1
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<?php
if (!empty($_SESSION['hooks'])) {
    echo "<h2 id = 'hookHeader'>" . ___('Hooks') . "</h2>";
    $output = "<table class = 'ATiM-debug-hook-table'><tr><th>" . __('hook file name') . "</th><th>" . __('exists') . "</th></th></tr>";
    foreach ($_SESSION['hooks'] as &$hook) {
        $output .= "<tr><td>" . $hook[0] . "</td><td>" . ($hook[1] ? "Y" : "N") . "</td></tr>";
        unset($hook);
    }
    $output .= "</table>";
    echo $output;
    $_SESSION['hooks'] = array();
}
?>
<h2 id = 'structure_header'> <?php echo ___('Structure Alias'); ?></h2>
<?php echo $this->Structures->getStructureAlias();