# The Script file to load the demo data for the Linux users.

WHITE='\033[1;37m'
CYAN='\033[1;36m'
YELLOW='\033[1;33m'
RED='\033[0;31m'
GREEN='\033[0;32m'

clear
echo -e "${YELLOW}"

printf "==================================================================\n"
printf "==                   Password Salt Changer                      ==\n"
printf "==================================================================\n"

printf "Database Name(default 'atimdev28z' if not completed)? "
read databaseName
if [ "$databaseName" = "" ];
then
	databaseName="atimdev28z"
fi

printf "Database username (default 'root' if not completed)? "
read databaseUserName
if [ "$databaseUserName" = "" ];
then
	databaseUserName="root"
fi

printf "Database Password? "
read password
if [ "$password" = "" ];
then
	password=""
else
	password="-p $password"
fi

pass="-ppassword"


printf "Database Host (localhost)? "
read host
if [ "$host" = "" ];
then
	host="localhost"
fi


printf "Database Port# (3306)? "
read port
if [ "$port" = "" ];
then
	port="3306"
fi


printf "The salt phrase to encrypt the password (ANewSaltPhraseToEncode)? "
read salt
if [ "$salt" = "" ];
then
	salt="ANewSaltPhraseToEncode"
fi


printf "Enter your ATiM username (administrator)? "
read atimusername
if [ "$atimusername" = "" ];
then
	atimusername="administrator"
fi


echo "php ModifyTheSalt.php -u ${databaseUserName} ${password} -host ${host} -d ${databaseName} -port ${port} -salt ${salt} -atimusername ${atimusername}"
php ModifyTheSalt.php -u $databaseUserName $password -host $host -d $databaseName -port $port -salt $salt -atimusername $atimusername

echo "Please execute SQL queries of the '\scripts\Modify The Salt Password\encryptPassScript.sql' in the SQL database."
echo "Please find the temporary passwords of your users in '\scripts\Modify The Salt Password\users.csv'."

read -n 1 -p "Press any key to continue . . ."
echo""