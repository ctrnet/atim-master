CLS
@ECHO off
COLOR F
ECHO ==================================================================
ECHO ==                   Password Salt Changer                      ==
ECHO ==================================================================
ECHO -

SET databaseUserName=root
SET host=localhost
SET password=
SET port=3306

set /p databaseName=Database Name (default 'atimdev28z' if not completed)?
if "%databaseName%" == "" (
	SET databaseName=atimdev28z
)

set /p databaseUserName=Database databaseUserName (default 'root' if not completed)?
if "%databaseUserName%" == "" (
	SET databaseUserName=root
)

set /p password=Database password?
if NOT "%password%" == "" (
	SET password=-p %password%
)

set /p host=Database Host (localhost)?
if "%host%" == "" (
	SET host=localhost
)

set /p port=Database Port# (3306)?
if "%port%" == "" (
	SET port=3306
)

set /p salt=The salt phrase to encrypt the password (ANewSaltPhraseToEncode)?
if "%salt%" == "" (
	SET salt=ANewSaltPhraseToEncode
)

set /p atimusername=Enter your ATiM username (administrator)?
if "%atimusername%" == "" (
	SET atimusername=administrator
)

@echo on

php ModifyTheSalt.php -u %databaseUserName% %password% -host %host% -d %databaseName% -port %port% -salt %salt% -atimusername %atimusername%

@echo off

echo.
echo.
echo Please execute SQL queries of the '\scripts\Modify The Salt Password\encryptPassScript.sql' in the SQL database.
echo Please find the temporary passwords of your users in '\scripts\Modify The Salt Password\users.csv'.
echo.

PAUSE
