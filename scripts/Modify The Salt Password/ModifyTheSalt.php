<?php

use libs\ArgvParser;

include_once "./libs/ArgvParser.php";

class ModifyTheSalt
{
    private $db;

    private $config = [
        'mysql_database' => 'atimdev28z',
        'mysql_host' => 'localhost',
        'mysql_user' => 'root',
        'mysql_pwd' => '',
        'mysql_port' => 3306,
        'salt' => 'ANewSaltPhraseToEncode',
        'atimusername' => 'administrator',
    ];

    private $result = [];

    public function __construct()
    {
        $this->parseArgs();
        echo "\n\nDatabase Configuration: \n" . json_encode($this->config, JSON_PRETTY_PRINT);
        $this->getConnection();
    }

    private function parseArgs()
    {
        $argvParser = new ArgvParser();
        $args = array_change_key_case($argvParser->parseConfigs($argv), CASE_LOWER);

        if (!empty($args['h']) || !empty($args['help'])) {
            print_r("php .\InventoryUseEventMigration.php -u username(root) -p password(none) -d database -port portNumber(3306) -host hostname(localhost)");
            exit(-1);
        } else {
            if (!empty($args['u']) || !empty($args['user'])) {
                $this->config['mysql_user'] = empty($args['u']) ? $args['user'] : $args['u'];
            }

            $this->config['mysql_host'] = empty($args['host']) ? $this->config['mysql_host'] : $args['host'];
            $this->config['mysql_port'] = empty($args['port']) ? $this->config['mysql_port'] : $args['port'];

            if (!empty($args['p']) || !empty($args['password'])) {
                $this->config['mysql_pwd'] = empty($args['p']) ? $args['password'] : $args['p'];
            }

            if (!empty($args['d']) || !empty($args['database'])) {
                $this->config['mysql_database'] = empty($args['d']) ? $args['database'] : $args['d'];
            } elseif (isset($this->config['mysql_database'])) {
            } else {
                $this->config['error'][] = "Database name is required, you can pass it by -d option";
            }

            if (!empty($args['s']) || !empty($args['salt'])) {
                $this->config['salt'] = empty($args['s']) ? $args['salt'] : $args['s'];
            } elseif (isset($this->config['salt'])) {
            } else {
                $this->config['error'][] = "Salt is required, you can pass it by -s option";
            }

            if (!empty($args['atimusername'])) {
                $this->config['atimusername'] = $args['atimusername'];
            } elseif (isset($this->config['atimusername'])) {
            } else {
                $this->config['error'][] = "ATiM user name is required, you can pass it by -atimusername option";
            }
        }
        if (!empty($this->config['error'])) {
            echo("\n\nError: \n\t" . implode("\n\t", $this->config['error']) . "\n");
            exit(-1);
        }
    }

    private function getConnection()
    {
        $this->db = @new mysqli($this->config['mysql_host'], $this->config['mysql_user'], $this->config['mysql_pwd']);

        if ($this->db->connect_errno) {
            die('Connect Error: ' . $this->db->connect_errno . ": " . $this->db->connect_error);
        }
        if (!$this->db->set_charset("utf8")) {
            die("We failed");
        }

        if ($this->config['mysql_database'] != NULL) {
            if (!$this->db->select_db($this->config['mysql_database'])) {
                die($this->db->error);
            }
        }
    }

    public function __destruct()
    {
    }

    public function changeTheSalt()
    {
        $users = $this->selectUsers();
        $this->updateThePasswords($users);
        $this->finalization();
    }

    private function selectUsers($options = [])
    {
        $options += [
            'flag_active' => 1,
            'deleted' => 0,
        ];

        $conditions = http_build_query($options, " = ", " AND ") . ";";
        $users = [];
        try {
            $query = "SELECT * FROM `users` WHERE $conditions";
            $stmt = $this->db->prepare($query) or die("Query ($query) failed at line " . __LINE__ . ' ' . $query . ' ' . $this->db->error);
            $stmt->execute();
            $res = $stmt->get_result();
            $row = $res->fetch_assoc();
            if ($row) {
                while ($row) {
                    $users[$row['id']] = $row;
                    $row = $res->fetch_assoc();
                }
            }
        } catch (Exception $e) {
            $this->result['Errors'][] = $e->getMessage();
        }
        return $users;
    }

    private function updateThePasswords($users)
    {
        $modifyby = false;
        foreach ($users as $user) {
            if ($user['username'] == $this->config['atimusername']){
                $modifyby = $user['id'];
            }
        }

        if (!$modifyby){
            $this->result['Errors'][] = "The " . $this->config['atimusername'] . " Username does not existe in the ATiM.";
            return;
        }

        $this->result['csv'] = ["username;first name;last name;email;password"];
        foreach ($users as $user) {
            $id = $user['id'];
            $username = $user['username'];
            $firstName = $user['first_name'];
            $lastName = $user['last_name'];
            $email = $user['email'];
            $password = $this->generateRandomString(10);
            $encryptedPassword = sha1($this->config['salt'] . $password);

            $this->result['update'][] = "UPDATE `users` SET `password` = '$encryptedPassword', `force_password_reset` = '1', `modified_by` = '$modifyby', `modified` = NOW(), `password_modified` = NOW() WHERE `id` = '$id'";
            $this->result['csv'][] = "$username;$firstName;$lastName;$email;$password";
        }
    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()_+';
        $charactersLength = strlen($characters);
        do {
            $passwordIsOK = false;
            $randomString = '';
            $specialChars = $upperCase = $lowerCase = $letter = false;
            for ($i = 0; $i < $length; $i++) {
                $char = $characters[rand(0, $charactersLength - 1)];
                if (strpos("!@#$%^&*()_+", $char) !== false) {
                    $specialChars = true;
                } elseif (strpos("ABCDEFGHIJKLMNOPQRSTUVWXYZ", $char) !== false) {
                    $upperCase = true;
                } elseif (strpos("abcdefghijklmnopqrstuvwxyz", $char) !== false) {
                    $lowerCase = true;
                } elseif (strpos("0123456789", $char) !== false) {
                    $letter = true;
                }
                $randomString .= $char;
            }
            $passwordIsOK = $specialChars && $upperCase && $lowerCase && $letter;
        } while (!$passwordIsOK);

        return $randomString;
    }

    private function finalization()
    {
        $scriptFilename = "encryptPassScript.sql";
        $csvFilename = "users.csv";

        if(!empty($this->result['Errors'])){
            echo ("\n\nErrors:\n\n\t" . implode("\n\t", $this->result['Errors']) . "\n\n");
        }else{
            file_put_contents($scriptFilename, implode(";\n", $this->result['update']) . ";");
            file_put_contents($csvFilename, implode("\n", $this->result['csv']));
        }
    }

}

$modifyTheSalt = new ModifyTheSalt();
$modifyTheSalt->changeTheSalt();