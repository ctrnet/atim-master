-- ----------------------------------------------------------------------------------------------------------------------------------
-- ATiM Database Upgrade Script
-- Version: 2.7.3
--
-- For more information:
--    ./app/scripts/v2.7.0/ReadMe.txt
--    https://atim-software.ca/
-- ----------------------------------------------------------------------------------------------------------------------------------

SET @user_id = (SELECT id FROM users WHERE username = 'system');
SET @user_id = (SELECT IFNULL(@user_id, 1));

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Create the user_cookies table for manage multi-logins
-- ----------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS `user_cookies`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `session_id` VARCHAR (100) NULL,
  `flag_active` tinyint(1) NOT NULL DEFAULT 1,
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_user_cookies_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #119: Wrong header in MiscIdentifier Result view
--  https://gitlab.com/ctrnet/atim/-/issues/119
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE structure_formats
SET
  `flag_override_label`='1',
  `language_label`='identifier name'
WHERE
  structure_id=(SELECT id FROM structures WHERE alias='miscidentifiers_for_participant_search') AND
  structure_field_id=(SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND
  `tablename`='misc_identifier_controls' AND
  `field`='misc_identifier_name' AND
  `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list') AND `flag_confidential`='0');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue 124 (eventum): i18n correction(s)
-- ----------------------------------------------------------------------------------------------------------------------------------
REPLACE INTO i18n (`id`,`en`,`fr`)
VALUES
('cell count', 'Cell Count', 'Nombre de cellules');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #124: Language Selection : Add EN|FR link on the top header
--  https://gitlab.com/ctrnet/atim/-/issues/124
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `i18n` (`id`, `en`, `fr`) VALUES
("switch language", "Français", "Passer au français"),
("english tag title", "Current language is English", "Change the language to English"),
("french tag title", "Changer la langue en français", "La langue actuelle est le français");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #75: Add a message if the user try to save an special character
--  https://gitlab.com/ctrnet/atim/-/issues/75
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `i18n` (`id`, `en`, `fr`) VALUES
('invalid characters for the field "%s"', 'Invalid characters for the field "%s"', 'Caractères non valides pour le champ "%s"');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #73: Add an installation flag
--  https://gitlab.com/ctrnet/atim/-/issues/73
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `i18n` (`id`, `en`, `fr`) VALUES
('the atim is on maintenance for now.',
'ATiM is under maintenance at the moment. Please contact your administartor for any details.',
"ATiM est actuellement en maintenance. Veuillez contacter votre administrateur pour plus de détails.");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #131: Give permission to administrator group to explore any BatchSets and DataBrowser results.
--  https://gitlab.com/ctrnet/atim/-/issues/131
-- ----------------------------------------------------------------------------------------------------------------------------------

ALTER TABLE `datamart_browsing_indexes`
ADD `locked` tinyint(1) NOT NULL DEFAULT '1' AFTER `temporary`,
ADD `sharing_status` varchar(50) COLLATE 'latin1_swedish_ci' NULL DEFAULT 'user' AFTER `locked`;

REPLACE INTO `i18n` (`id`, `en`, `fr`) VALUES
('edit browse', 'Edit Browse', 'Modifier Parcourir'),
('delete browse', 'Delete Browse', 'Supprimer Parcourir'),
('edit data research accessibility', 'Edit data research accessibility', "Modifier l'accessibilité de la recherche de données"),
('delete the research', 'Delete the research\n(not to lose any data)', 'Supprimer la recherche\n(ne pas perdre de données)');

UPDATE structure_formats SET `display_order`='8' WHERE structure_id=(SELECT id FROM structures WHERE alias='querytool_batch_set') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='BatchSet' AND `tablename`='datamart_batch_sets' AND `field`='locked' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #134: Remove the validation for field1 in collections alias
--  https://gitlab.com/ctrnet/atim/-/issues/134
-- ----------------------------------------------------------------------------------------------------------------------------------

DELETE FROM `structure_validations` WHERE structure_field_id = (SELECT id FROM `structure_fields` WHERE  `model` = 'Generated'  AND `field` = 'field1'  AND `plugin` = ''  AND `structure_value_domain` IS NULL);

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #126: Annotation > Protocol and Study are not hidden by newVersionSetUp() function
--  https://gitlab.com/ctrnet/atim/-/issues/126
--
--	Issue #18: New Version Setup : Check 1st sub menu of 'Clinical Annotation > Annotation'
--  https://gitlab.com/ctrnet/atim/-/issues/18
--
--	Issue #131: Give permission to administrator group to explore any BatchSets and DataBrowser results.
--  https://gitlab.com/ctrnet/atim/-/issues/131
--
--	Issue #149: Change the upload button text correspond the language
--  https://gitlab.com/ctrnet/atim/-/issues/149
--
--	Issue #164: Add multi select functionnality to ATiM
--  https://gitlab.com/ctrnet/atim/-/issues/164
--
--	Issue #166: Wring french message when changing aliquots positions in boxes from csv
--  https://gitlab.com/ctrnet/atim/-/issues/166
--
--	Issue #168: AddGrid and is unique validation
--  https://gitlab.com/ctrnet/atim/-/issues/168
--
--	Issue #141: Search from treatment to collection in databrowser : Shortest link vs Tx to Participant To Collection
--  https://gitlab.com/ctrnet/atim/-/issues/141
--
--  Change sub menu links and i18n
-- ----------------------------------------------------------------------------------------------------------------------------------
REPLACE INTO i18n (id, en, fr)
VALUES
("following menus have been inactivated : %s.",
"Following menus have been inactivated (change core variable 'newVersionSetupConfigureMenus' to false if you want disable this automatic configuration): %s.",
"Les menus suivants ont été désactivés (changez la variable du core 'newVersionSetupConfigureMenus' à false si vous souhaitez désactiver cette configuration automatique): %s."),
("following menus have been activated : %s.",
"Following menus have been activated (change core variable 'newVersionSetupConfigureMenus' to false if you want disable this automatic configuration): %s.",
"Les menus suivants ont été activés (changez la variable du core 'newVersionSetupConfigureMenus' à false si vous souhaitez désactiver cette configuration automatique): %s."),
("sharing status", "Sharing status", "Statut de partage"),
("shared browsing", "Shared browsing", "Navigation partagée"),
("shared browsing trees", "Shared browsing trees", "Arbres de navigation partagés"),
("shared browsing data is read-only", "Shared browsing data is read-only", "Les données de navigation partagées sont en lecture seule"),
("choose file", "Choose File", "Choisir un fichier"),
("no file chosen", "No file chosen", "Aucun fichier choisi"),
("keep", "Keep", "Garder"),
("the search owner does not allow you to expand their searches", "The search owner does not allow you to expand their searches.", "Le propriétaire de la recherche ne vous permet pas d''étendre ses recherches."),
("select some options", "Select Some Options", "Sélectionnez quelques options"),
("no result for", "No Result", "Pas de résultat"),
("number of aliquots analyzed = %d, validated = %d, warning = %d, error = %d", "Number of aliquots : Analyzed = %d, Validated = %d, With warning = %d, With error = %d", "Nombre d'aliquotes: Analysées =%d, validées =%d, avec avertissement =%d, avec erreur =%d"),
("replace", "Replace", "Remplacer"),
("you can not submit value '%s' for field '%s' twice", "You can not submit twice value '%s' for field '%s'", "Vous ne pouvez pas soumettre deux fois la valeur '%s' pour le champ '%s'"),
("you can not record %s twice", "You can not record %s twice", "Vous ne pouvez enregistrer le %s deux fois!"),
("search via an intermediate node %s description text", "Do you want to search via the intermediate node %s ?", "Voulez-vous effectuer une recherche via le nœud intermédiaire %s ?"),
("search via an intermediate node", "Search Via an Intermediate Node", "Recherche Via un Nœud Intermédiaire"),
("'datamart_browsing_controls' has been updated %s", "The table 'datamart_browsing_controls' has been updated %s", "La table 'datamart_browsing_controls' a été mise à jour %s"),
("the relation between 'Participant' and '%s' is set", "The relation between 'Participant' and '%s' is set", "la relation entre 'Participant' et '%s' est définie");

UPDATE menus SET use_link = '/ClinicalAnnotation/EventMasters/listall/study/%%Participant.id%%' WHERE use_link LIKE '/ClinicalAnnotation/EventMasters/listall/Study/%%Participant.id%%';
UPDATE menus SET use_link = '/ClinicalAnnotation/EventMasters/listall/protocol/%%Participant.id%%' WHERE use_link LIKE '/ClinicalAnnotation/EventMasters/listall/Protocol/%%Participant.id%%';

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #131: Give permission to administrator group to explore any BatchSets and DataBrowser results.
--  https://gitlab.com/ctrnet/atim/-/issues/131
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
('Datamart', 'BrowsingIndex', 'datamart_browsing_indexes', 'sharing_status', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='batch_sets_sharing_status') , '0', '', '', '', 'sharing status', ''),
('Datamart', 'BrowsingIndex', 'datamart_browsing_indexes', 'locked', 'checkbox',  NULL , '0', '', '', '', 'locked', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
((SELECT id FROM structures WHERE alias='datamart_browsing_indexes'),
(SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='sharing_status' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='batch_sets_sharing_status')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='sharing status' AND `language_tag`=''),
'1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='datamart_browsing_indexes'),
(SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='locked' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='locked' AND `language_tag`=''),
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO `structure_validations` (`structure_field_id`, `rule`)
VALUES
((SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='sharing_status' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='batch_sets_sharing_status')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='sharing status' AND `language_tag`=''), 'notBlank');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #205: Arrange the position of the column in `datamart_browsing_indexes` alias
--  https://gitlab.com/ctrnet/atim/-/issues/205
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE structure_formats SET `display_order`='5' WHERE structure_id=(SELECT id FROM structures WHERE alias='datamart_browsing_indexes') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='BrowsingResult' AND `tablename`='' AND `field`='browsing_structures_id' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='display_name_from_datamasrtstructure') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='10' WHERE structure_id=(SELECT id FROM structures WHERE alias='datamart_browsing_indexes') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='created' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='15' WHERE structure_id=(SELECT id FROM structures WHERE alias='datamart_browsing_indexes') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='sharing_status' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='batch_sets_sharing_status') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='20' WHERE structure_id=(SELECT id FROM structures WHERE alias='datamart_browsing_indexes') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='locked' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='25' WHERE structure_id=(SELECT id FROM structures WHERE alias='datamart_browsing_indexes') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #152: Databrowser: No error message when a treatment node gathers more than one treatment for at least 
--  one participant and we search on collection pre-treatment
--  https://gitlab.com/ctrnet/atim/-/issues/152
-- ----------------------------------------------------------------------------------------------------------------------------------

-- Change displayed message

INSERT INTO i18n (id,en,fr) 
VALUES
('a special parameter could not be applied because one or many %s selected are linked to more than one %s listed in the previously linked node - relation should be one %s per %s',
'A special parameter could not be applied because one or many selected %s are linked to more than one %s listed in the previous node. Relation should be one %s per %s.',
"Un paramètre spécial n'a pas pu être appliqué car un ou plusieurs %s sélectionnés sont liés à plus d'un %s répertorié dans le noeud précédent. La relation doit être de un %s par %s.");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #162: Databrowser: Unable to search diagnosis (or treatment or event) pre or post collection in databrowser
--  https://gitlab.com/ctrnet/atim/-/issues/162
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('adv_diagnosis_date', 'open', '', 'ClinicalAnnotation.DiagnosisMaster::getBrowsingAdvSearch(\'dx_date\')');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'DiagnosisMaster', 'diagnosis_masters', 'dx_date', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='adv_diagnosis_date') , '0', 'noCtrl=', '', '', 'dx_date', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='dx_adv_search'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='dx_date' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='adv_diagnosis_date')  AND `flag_confidential`='0' AND `setting`='noCtrl=' AND `default`='' AND `language_help`='' AND `language_label`='dx_date' AND `language_tag`=''), 
'1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('adv_event_date', 'open', '', 'ClinicalAnnotation.EventMaster::getBrowsingAdvSearch(\'event_date\')');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventMaster', 'event_masters', 'event_date', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='adv_event_date') , '0', 'noCtrl=', '', '', 'date', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='event_adv_search'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_date' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='adv_event_date')), 
'1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('adv_treatment_start_date', 'open', '', 'ClinicalAnnotation.TreatmentMaster::getBrowsingAdvSearch(\'start_date\')'),
('adv_treatment_finish_date', 'open', '', 'ClinicalAnnotation.TreatmentMaster::getBrowsingAdvSearch(\'finish_date\')');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'TreatmentMaster', 'treatment_masters', 'start_date', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='adv_treatment_start_date') , '0', 'noCtrl=', '', '', 'date/start date', ''),
('ClinicalAnnotation', 'TreatmentMaster', 'treatment_masters', 'finish_date', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='adv_treatment_finish_date') , '0', 'noCtrl=', '', '', 'finish date', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='tx_adv_search'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='start_date' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='adv_treatment_start_date')), 
'1', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='tx_adv_search'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='finish_date' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='adv_treatment_finish_date')), 
'1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #163: Databrowser: Be able to search on treatment with the most recent or oldest finish date
--  https://gitlab.com/ctrnet/atim/-/issues/163
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO i18n (id,en,fr) 
VALUES
('keep entries with the most recent finish date per participant', 'Keep entries with the most recent finish date per participant', "Conserver les entrées avec la date de fin la plus récente par participant"),
('keep entries with the oldest finish date per participant', 'Keep entries with the oldest finish date per participant', "Conserver les entrées avec la date de fin la plus ancienne par participant");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #133: Compress the post data
--  https://gitlab.com/ctrnet/atim/-/issues/133
-- ----------------------------------------------------------------------------------------------------------------------------------

-- Allow more than 20 children aliquots per realiquoted parent

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('nbr of children by default can not be bigger than %d', 'The number of children aliquots by default can not be bigger than %d!', "Le nombre d'aliquots enfants par défaut ne peut pas être supérieur à %d!");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #102: Batch Set display when the number of elements are too big.
--  https://gitlab.com/ctrnet/atim/-/issues/102
-- ----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `datamart_batch_sets` ADD `id_csv` longtext COLLATE 'latin1_swedish_ci' NULL;

INSERT IGNORE INTO i18n (`id`, `en`, `fr`) VALUES
("the batchset contains too many elements", "The batchset contains too many elements to display the content of this one.", "Le lot de données contient trop d'éléments pour afficher le contenu de celui-ci."),
("for any action you take, all matches of the current set will be used", "For any action you take, all matches of the current set will be used", "Peu importe l'action que vous sélectionnez , tous les résultats de l'ensemble présent seront utilisés");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #170: Check the external links like DataBrowswer, Diagnisis and etc.
--  https://gitlab.com/ctrnet/atim/-/issues/170
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE `external_links` SET `link` = 'https://atim-software.ca/en/data-browser/' WHERE `name` = 'databrowser_help';
UPDATE `external_links` SET `link` = 'https://atim-software.ca/en/how-to-edit-an-existing-group/' WHERE `name` = 'permissions_help';
UPDATE `external_links` SET `link` = 'https://atim-software.ca/en/inventory-management/' WHERE `name` = 'inventory_elements_defintions';
UPDATE `external_links` SET `link` = 'https://atim-software.ca/en/use-the-diagnosis-section/' WHERE `name` = 'diagnosis_module_wiki';

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #174: Derivative search result: Duplicated fields Parent Sample & Initial Specimen
--  https://gitlab.com/ctrnet/atim/-/issues/174
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE structure_formats SET `flag_index`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='derivatives') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='SampleMaster' AND `tablename`='sample_masters' AND `field`='initial_specimen_sample_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type') AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='derivatives') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='SampleMaster' AND `tablename`='sample_masters' AND `field`='parent_sample_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_type') AND `flag_confidential`='0');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #72: Simplify system to let creation of non tumoral diagnosis.
--  https://gitlab.com/ctrnet/atim/-/issues/72
-- ----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `diagnosis_controls`
CHANGE `category` `category` enum('primary','secondary - distant','progression - locoregional','remission','recurrence - locoregional','nonneoplastic') COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT 'primary' AFTER `id`;

DELETE FROM i18n WHERE id IN ('add diagnosis event (progression, treatment, event, clinical history)');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('nonneoplastic', 'Nonneoplastic', 'Non-néoplasique'),
('add diagnosis event (progression, treatment, event, clinical history)', "Complete Clinical File", "Compléter dossier clinique"),
("select a type of diagnostic clinical file record to add", "Select type of clinical file data to add", "Sélectionner le type de données du dossier clinique à ajouter"),
('add type of diagnosis', 'Add Diagnosis', 'Ajouter Diagnostic');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #167: Create new sample type 'PDX'. 
-- PDX is similar to Xenograft, but is a tissue that is used to be inserted in animals like mouse.  
-- https://gitlab.com/ctrnet/atim/-/issues/167
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Create new sample_control for PDX
INSERT INTO sample_controls (sample_type, sample_category, detail_form_alias, detail_tablename, databrowser_label)
VALUES ('pdx', 'derivative', 'sd_der_pdxs,derivatives', 'sd_der_pdx', 'pdx');
-- Create new tables in db
CREATE TABLE `sd_der_pdx` (
  `sample_master_id` int(11) NOT NULL,
  species varchar(50),
  implantation_site varchar(50),
  laterality varchar(30),
  KEY `FK_sd_der_pdx_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_der_pdx_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_der_pdx_revs` (
  `sample_master_id` int(11) NOT NULL,
  species varchar(50),
  implantation_site varchar(50),
  laterality varchar(30),
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `ad_pdx_slides` (
  `aliquot_master_id` int(11) NOT NULL,
  KEY `FK_ad_pdx_slides_aliquot_masters` (`aliquot_master_id`),
  CONSTRAINT `FK_ad_pdx_slides_aliquot_masters` FOREIGN KEY (`aliquot_master_id`) REFERENCES `aliquot_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `ad_pdx_slides_revs` (
  `aliquot_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `ad_pdx_cores` (
  `aliquot_master_id` int(11) NOT NULL,
  KEY `FK_ad_pdx_cores_aliquot_masters` (`aliquot_master_id`),
  CONSTRAINT `FK_ad_pdx_cores_aliquot_masters` FOREIGN KEY (`aliquot_master_id`) REFERENCES `aliquot_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `ad_pdx_cores_revs` (
  `aliquot_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- Insert PDX in parent_to_derivative_sample_controls
INSERT INTO parent_to_derivative_sample_controls (parent_sample_control_id,derivative_sample_control_id,flag_active)
VALUES ((SELECT id FROM sample_controls WHERE sample_type = 'tissue'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx'), 0);
INSERT INTO parent_to_derivative_sample_controls (parent_sample_control_id,derivative_sample_control_id,flag_active)
VALUES
((SELECT id FROM sample_controls WHERE sample_type = 'pdx'), (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), 0),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx'), (SELECT id FROM sample_controls WHERE sample_type = 'protein'), 0),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx'), 0);
-- Insert new aliquot controls for aliquots of sample type PDX
INSERT INTO aliquot_controls (sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, databrowser_label) 
VALUES 
((SELECT id FROM sample_controls WHERE sample_type='pdx'), 'tube', null, 'ad_der_pdx_tubes', 'ad_tubes', null, 0, '', 'pdx|tube'),
((SELECT id FROM sample_controls WHERE sample_type='pdx'), 'block', null, 'ad_der_pdx_blocks', 'ad_blocks', null, 0, '', 'pdx|block'),
((SELECT id FROM sample_controls WHERE sample_type='pdx'), 'slide', null, 'ad_der_pdx_slides', 'ad_pdx_slides', null, 0, '', 'pdx|slide'),
((SELECT id FROM sample_controls WHERE sample_type='pdx'), 'core', null, 'ad_der_pdx_cores', 'ad_pdx_cores', null, 0, '', 'pdx|core');

INSERT INTO realiquoting_controls (parent_aliquot_control_id, child_aliquot_control_id, flag_active) 
VALUES 
((SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_tubes'),(SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_tubes'), '0'),
((SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_tubes'),(SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_blocks'), '0'),
((SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_tubes'),(SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_slides'), '0'),
((SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_blocks'),(SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_blocks'), '0'),
((SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_blocks'),(SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_tubes'), '0'),
((SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_blocks'),(SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_slides'), '0'),
((SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_blocks'),(SELECT id FROM aliquot_controls WHERE detail_form_alias = 'ad_der_pdx_cores'), '0');
-- Structures for pdx
INSERT INTO structures (alias) VALUES ('sd_der_pdxs'),('ad_der_pdx_tubes'), ('ad_der_pdx_blocks'), ('ad_der_pdx_slides'), ('ad_der_pdx_cores');

INSERT INTO structure_value_domains (domain_name, source) 
VALUES 
('pdx_species', "StructurePermissibleValuesCustom::getCustomDropdown('PDX Species')"),
('pdx_implantation_sites', "StructurePermissibleValuesCustom::getCustomDropdown('PDX Implantation Sites')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
('PDX Species', 1, 50, 'inventory'),
('PDX Implantation Sites', 1, 50, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'PDX Species');
INSERT INTO `structure_permissible_values_customs` (`value`, `en`, `fr`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`)
VALUES
('mouse', 'Mouse',  'Souris', '1', @control_id, NOW(), NOW(), 1, 1);
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'PDX Implantation Sites');
INSERT INTO `structure_permissible_values_customs` (`value`, `en`, `fr`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`)
VALUES
('liver', 'Liver',  'Foie', '1', @control_id, NOW(), NOW(), 1, 1),
('mammary fat pad', 'Mammary Fat Pad',  'Tissu graisseux mammaire', '1', @control_id, NOW(), NOW(), 1, 1),
('flank', 'Flank',  'Flanc', '1', @control_id, NOW(), NOW(), 1, 1);
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) VALUES
('InventoryManagement', 'SampleDetail', '', 'species', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='pdx_species') , '0', '', '', '', 'species', ''), 
('InventoryManagement', 'SampleDetail', '', 'implantation_site', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='pdx_implantation_sites') , '0', '', '', '', 'implantation site', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='sd_der_pdxs'), (SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='species' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='pdx_species')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='species' AND `language_tag`=''), '1', '444', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='sd_der_pdxs'), (SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='implantation_site' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='pdx_implantation_sites')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='implantation site' AND `language_tag`=''), '1', '445', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='sd_der_pdxs'), (SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='laterality' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='tissue_laterality')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='laterality' AND `language_tag`=''), '1', '446', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ad_der_pdx_tubes'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='coll_to_stor_spent_time_msg' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='inv_coll_to_stor_spent_time_msg_defintion' AND `language_label`='collection to storage spent time' AND `language_tag`=''), 
'1', '59', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_tubes'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='coll_to_stor_spent_time_msg' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '59', '', '0', '1', 'collection to storage spent time (min)', '0', '', '0', '', '1', 'integer_positive', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_tubes'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='creat_to_stor_spent_time_msg' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='inv_creat_to_stor_spent_time_msg_defintion' AND `language_label`='creation to storage spent time' AND `language_tag`=''), 
'1', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_tubes'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='creat_to_stor_spent_time_msg' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '60', '', '0', '1', 'creation to storage spent time (min)', '0', '', '0', '', '1', 'integer_positive', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_tubes'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='lot_number' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='' AND `language_label`='lot number' AND `language_tag`=''), 
'1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ad_der_pdx_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='coll_to_stor_spent_time_msg' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='inv_coll_to_stor_spent_time_msg_defintion' AND `language_label`='collection to storage spent time' AND `language_tag`=''), 
'1', '59', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='coll_to_stor_spent_time_msg' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '59', '', '0', '1', 'collection to storage spent time (min)', '0', '', '0', '', '1', 'integer_positive', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='creat_to_stor_spent_time_msg' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='inv_creat_to_stor_spent_time_msg_defintion' AND `language_label`='creation to storage spent time' AND `language_tag`=''), 
'1', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='creat_to_stor_spent_time_msg' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '60', '', '0', '1', 'creation to storage spent time (min)', '0', '', '0', '', '1', 'integer_positive', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='block_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='block_type')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='block type' AND `language_tag`=''), 
'1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ad_der_pdx_cores'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='coll_to_stor_spent_time_msg' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='inv_coll_to_stor_spent_time_msg_defintion' AND `language_label`='collection to storage spent time' AND `language_tag`=''), 
'1', '59', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_cores'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='coll_to_stor_spent_time_msg' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '59', '', '0', '1', 'collection to storage spent time (min)', '0', '', '0', '', '1', 'integer_positive', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_cores'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='creat_to_stor_spent_time_msg' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='inv_creat_to_stor_spent_time_msg_defintion' AND `language_label`='creation to storage spent time' AND `language_tag`=''), 
'1', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_cores'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='creat_to_stor_spent_time_msg' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '60', '', '0', '1', 'creation to storage spent time (min)', '0', '', '0', '', '1', 'integer_positive', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_pdx_cores'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='block_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='block_type')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='block type' AND `language_tag`=''), 
'1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ad_der_pdx_slides'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='immunochemistry' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='' AND `language_label`='immunochemistry code' AND `language_tag`=''), 
'1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');

-- i18n
INSERT INTO i18n (id, en, fr) VALUES ('pdx', 'PDX', 'PDX');

-- Created pdx derivative (tissue, blood, dna, rna, etc)
-- -----------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE IF NOT EXISTS sd_pdx_blood_cells (
  sample_master_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS sd_pdx_blood_cells_revs (
  sample_master_id int(11) NOT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `sd_pdx_blood_cells`
  ADD CONSTRAINT FK_sd_pdx_blood_cells_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);

CREATE TABLE IF NOT EXISTS sd_pdx_dnas (
  sample_master_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS sd_pdx_dnas_revs (
  sample_master_id int(11) NOT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `sd_pdx_dnas`
  ADD CONSTRAINT FK_sd_pdx_dnas_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);

CREATE TABLE IF NOT EXISTS sd_pdx_pbmcs (
  sample_master_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS sd_pdx_pbmcs_revs (
  sample_master_id int(11) NOT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `sd_pdx_pbmcs`
  ADD CONSTRAINT FK_sd_pdx_pbmcs_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);

CREATE TABLE IF NOT EXISTS sd_pdx_plasmas (
  sample_master_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS sd_pdx_plasmas_revs (
  sample_master_id int(11) NOT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `sd_pdx_plasmas`
  ADD CONSTRAINT FK_sd_pdx_plasmas_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);

CREATE TABLE IF NOT EXISTS sd_pdx_rnas (
  sample_master_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS sd_pdx_rnas_revs (
  sample_master_id int(11) NOT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `sd_pdx_rnas`
  ADD CONSTRAINT FK_sd_pdx_rnas_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);

CREATE TABLE IF NOT EXISTS sd_pdx_serums (
  sample_master_id int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS sd_pdx_serums_revs (
  sample_master_id int(11) NOT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `sd_pdx_serums`
  ADD CONSTRAINT FK_sd_pdx_serums_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);

CREATE TABLE IF NOT EXISTS sd_pdx_bloods (
  sample_master_id int(11) NOT NULL,
  blood_type varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS sd_pdx_bloods_revs (
  sample_master_id int(11) NOT NULL,
  blood_type varchar(30) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `sd_pdx_bloods`
  ADD CONSTRAINT FK_sd_pdx_bloods_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);

CREATE TABLE IF NOT EXISTS sd_pdx_tissues (
  sample_master_id int(11) NOT NULL,
  tissue_source varchar(50) DEFAULT NULL,
  tissue_laterality varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS sd_pdx_tissues_revs (
  sample_master_id int(11) NOT NULL,
  tissue_source varchar(50) DEFAULT NULL,
  tissue_laterality varchar(30) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;
ALTER TABLE `sd_pdx_tissues`
  ADD CONSTRAINT FK_sd_pdx_tissues_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);

INSERT INTO `sample_controls` (`sample_type`, `sample_category`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`) VALUES
('pdx-blood', 'derivative', 'sd_pdx_bloods,derivatives', 'sd_pdx_bloods', 0, 'pdx-blood'),
('pdx-tissue', 'derivative', 'sd_pdx_tissues,derivatives', 'sd_pdx_tissues', 0, 'pdx-tissue'),
('pdx-blood cell', 'derivative', 'derivatives', 'sd_pdx_blood_cells', 0, 'pdx-blood cell'),
('pdx-pbmc', 'derivative', 'derivatives', 'sd_pdx_pbmcs', 0, 'pdx-pbmc'),
('pdx-plasma', 'derivative', 'derivatives', 'sd_pdx_plasmas', 0, 'pdx-plasma'),
('pdx-serum', 'derivative', 'derivatives', 'sd_pdx_serums', 0, 'pdx-serum'),
('pdx-dna', 'derivative', 'derivatives', 'sd_pdx_dnas', 0, 'pdx-dna'),
('pdx-rna', 'derivative', 'derivatives', 'sd_pdx_rnas', 0, 'pdx-rna');

INSERT INTO structures(`alias`) VALUES ('sd_pdx_tissues');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) VALUES
('InventoryManagement', 'SampleDetail', '', 'tissue_source', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='tissue_source_list') , '0', '', '', '', 'tissue source', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='sd_pdx_tissues'), (SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='tissue_source' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='tissue_source_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='tissue source' AND `language_tag`=''), '1', '441', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='sd_pdx_tissues'), (SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='tissue_laterality' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='tissue_laterality')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='laterality' AND `language_tag`=''), '1', '444', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');

INSERT INTO structures(`alias`) VALUES ('sd_pdx_bloods');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='sd_pdx_bloods'), (SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='blood_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='blood_type')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='blood tube type' AND `language_tag`=''), '1', '441', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');

INSERT INTO `parent_to_derivative_sample_controls` (`parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) VALUES
((SELECT id FROM sample_controls WHERE sample_type = 'pdx'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-blood'), 0, NULL),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-tissue'), 0, NULL),

((SELECT id FROM sample_controls WHERE sample_type = 'pdx-blood'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-blood cell'), 0, NULL),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-blood'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-pbmc'), 0, NULL),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-blood'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-plasma'), 0, NULL),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-blood'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-serum'), 0, NULL),

((SELECT id FROM sample_controls WHERE sample_type = 'pdx-tissue'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-dna'), 0, NULL),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-pbmc'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-dna'), 0, NULL),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-blood cell'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-dna'), 0, NULL),

((SELECT id FROM sample_controls WHERE sample_type = 'pdx-tissue'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-rna'), 0, NULL),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-pbmc'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-rna'), 0, NULL),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-blood cell'), (SELECT id FROM sample_controls WHERE sample_type = 'pdx-rna'), 0, NULL);

INSERT INTO `aliquot_controls` (`sample_control_id`, `aliquot_type`, `aliquot_type_precision`, `detail_form_alias`, `detail_tablename`, `volume_unit`, `flag_active`, `comment`, `display_order`, `databrowser_label`) VALUES
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-tissue'), 'tube', '', '', 'ad_tubes', NULL, 0, 'Specimen tube', 0, 'pdx-tissue|tube'),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-tissue'), 'block', NULL, 'ad_pdx_tiss_blocks', 'ad_blocks', NULL, 0, 'Tissue block', 0, 'pdx-tissue|block'),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-tissue'), 'slide', '', 'ad_pdx_tiss_slides', 'ad_tissue_slides', NULL, 0, 'Tissue slide', 0, 'pdx-tissue|slide'),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-tissue'), 'core', '', '', 'ad_tissue_cores', NULL, 0, 'Tissue core', 0, 'pdx-tissue|core'),

((SELECT id FROM sample_controls WHERE sample_type = 'pdx-blood'), 'tube', '(ml)', 'ad_pdx_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, 'Derivative tube requiring volume in ml', 0, 'pdx-blood|tube'),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-plasma'), 'tube', '(ml)', 'ad_pdx_tubes_incl_ml_vol,ad_hemolysis', 'ad_tubes', 'ml', 0, 'Derivative tube requiring volume in ml', 0, 'pdx-plasma|tube'),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-serum'), 'tube', '(ml)', 'ad_pdx_tubes_incl_ml_vol,ad_hemolysis', 'ad_tubes', 'ml', 0, 'Derivative tube requiring volume in ml', 0, 'pdx-serum|tube'),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-blood cell'), 'tube', '', 'ad_pdx_cell_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, 'Derivative tube requiring volume in ml specific for cells', 0, 'pdx-blood cell|tube'),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-pbmc'), 'tube', '', 'ad_pdx_cell_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, 'Derivative tube requiring volume in ml specific for cells', 0, 'pdx-pbmc|tube'),

((SELECT id FROM sample_controls WHERE sample_type = 'pdx-dna'), 'tube', '(ul + conc)', 'ad_pdx_tubes_incl_ul_vol_and_conc', 'ad_tubes', 'ul', 0, 'Derivative tube requiring volume in ul and concentration', 0, 'pdx-dna|tube'),
((SELECT id FROM sample_controls WHERE sample_type = 'pdx-rna'), 'tube', '(ul + conc)', 'ad_pdx_tubes_incl_ul_vol_and_conc', 'ad_tubes', 'ul', 0, 'Derivative tube requiring volume in ul and concentration', 0, 'pdx-rna|tube');

INSERT INTO structures(`alias`) VALUES ('ad_pdx_tiss_blocks');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='ad_pdx_tiss_blocks'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='block_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='block_type')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='block type' AND `language_tag`=''), '1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0');

INSERT INTO structures(`alias`) VALUES ('ad_pdx_tiss_slides');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='ad_pdx_tiss_slides'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='immunochemistry' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='' AND `language_label`='immunochemistry code' AND `language_tag`=''), '1', '71', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0');

INSERT INTO structures(`alias`) VALUES ('ad_pdx_tubes_incl_ml_vol');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='ad_pdx_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='current_volume' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='current volume' AND `language_tag`=''), '1', '71', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), '1', '72', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='initial_volume' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='initial volume' AND `language_tag`=''), '1', '73', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), '1', '74', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '1', '1', '1', '0', '0', '1', '1', '1', '1', '0', '0', '0', '1', '0', '0');

INSERT INTO structures(`alias`) VALUES ('ad_pdx_cell_tubes_incl_ml_vol');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='ad_pdx_cell_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='current_volume' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='current volume' AND `language_tag`=''), '1', '71', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_cell_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), '1', '72', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_cell_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='initial_volume' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='initial volume' AND `language_tag`=''), '1', '73', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_cell_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), '1', '74', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '1', '1', '1', '0', '0', '1', '1', '1', '1', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_cell_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='cell_count' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='cell count' AND `language_tag`=''), '1', '75', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_cell_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='cell_count_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='cell_count_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), '1', '76', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_cell_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='concentration' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='aliquot concentration' AND `language_tag`=''), '1', '77', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_cell_tubes_incl_ml_vol'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='ad_cell_tubes' AND `field`='concentration_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='cell_concentration_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), '1', '78', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0');

INSERT INTO structures(`alias`) VALUES ('ad_pdx_tubes_incl_ul_vol_and_conc');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='ad_pdx_tubes_incl_ul_vol_and_conc'), (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='current_volume' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='current volume' AND `language_tag`=''), '1', '71', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_tubes_incl_ul_vol_and_conc'), (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), '1', '72', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_tubes_incl_ul_vol_and_conc'), (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='initial_volume' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='initial volume' AND `language_tag`=''), '1', '73', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_tubes_incl_ul_vol_and_conc'), (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), '1', '74', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '1', '1', '1', '0', '0', '1', '1', '1', '1', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_tubes_incl_ul_vol_and_conc'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='concentration' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='aliquot concentration' AND `language_tag`=''), '1', '75', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_pdx_tubes_incl_ul_vol_and_conc'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='ad_tubes' AND `field`='concentration_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='concentration_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), '1', '76', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0');

INSERT INTO `realiquoting_controls` (`parent_aliquot_control_id`, `child_aliquot_control_id`, `flag_active`, `lab_book_control_id`) VALUES
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|tube'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|block'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|slide'), 0, NULL),

((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|block'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|block'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|block'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|slide'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|block'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|core'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|block'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-tissue|tube'), 0, NULL),

((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-blood|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-blood|tube'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-plasma|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-plasma|tube'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-serum|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-serum|tube'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-blood cell|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-blood cell|tube'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-pbmc|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-pbmc|tube'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-dna|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-dna|tube'), 0, NULL),
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-rna|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-rna|tube'), 0, NULL);

INSERT INTO structure_value_domains (domain_name, source) 
VALUES 
('pdx_tissue_source_list', "StructurePermissibleValuesCustom::getCustomDropdown('PDX Tissues Sources')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
('PDX Tissues Sources', 1, 50, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'PDX Tissues Sources');
INSERT INTO `structure_permissible_values_customs` (`value`, `en`, `fr`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`)
VALUES
('liver', 'Liver',  'Foie', '1', @control_id, NOW(), NOW(), 1, 1),
('lung', 'Lung',  'Poumon', '1', @control_id, NOW(), NOW(), 1, 1),
('kidney', 'Kidney',  'Rein', '1', @control_id, NOW(), NOW(), 1, 1),
('pancreas', 'Pancreas',  'Pancréas', '1', @control_id, NOW(), NOW(), 1, 1),
('ovary', 'Ovary',  'Ovaire', '1', @control_id, NOW(), NOW(), 1, 1),
('intestine', 'Intestine',  'Intestin', '1', @control_id, NOW(), NOW(), 1, 1),
('heart', 'Heart',  'Coeur', '1', @control_id, NOW(), NOW(), 1, 1),
('diaphragm', 'Diaphragm',  'Diaphragme', '1', @control_id, NOW(), NOW(), 1, 1),
('subcutaneous tumor', 'Subcutaneous Tumor',  'Tumeur sous-cutanée', '1', @control_id, NOW(), NOW(), 1, 1);
UPDATE structure_fields SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='pdx_tissue_source_list')  WHERE model='SampleDetail' AND tablename='' AND field='tissue_source' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='tissue_source_list');

INSERT INTO i18n (id,en,fr)
VALUES
('pdx-blood', 'PDX-Blood', 'PDX-Sang'),
('pdx-blood cell', 'PDX-Blood Cells', 'PDX-Cellules de sang'),
('pdx-dna', 'PDX-DNA', 'PDX-ADN'),
('pdx-pbmc', 'PDX-PBMC', 'PDX-PBMC'),
('pdx-plasma', 'PDX-Plasma', 'PDX-Plasma'),
('pdx-rna', 'PDX-RNA', 'PDX-ARN'),
('pdx-serum', 'PDX-Serum', 'PDX-Sérum'),
('pdx-tissue', 'PDX-Tissue', 'PDX-Tissu');

-- PDX buffy coat

INSERT INTO sample_controls(sample_type, sample_category, detail_form_alias, detail_tablename, databrowser_label)
VALUES
('pdx-buffy coat', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_pdx_buffy_coats', 'pdx-buffy coat');
INSERT INTO parent_to_derivative_sample_controls (parent_sample_control_id,derivative_sample_control_id , flag_active)
VALUES
((SELECT id FROM sample_controls WHERE sample_type LIKE 'pdx-blood'), (SELECT id FROM sample_controls WHERE sample_type LIKE 'pdx-buffy coat'),0);
INSERT INTO aliquot_controls (sample_control_id, aliquot_type, detail_form_alias, detail_tablename, flag_active, databrowser_label, volume_unit)
VALUES
((SELECT id FROM sample_controls WHERE sample_type LIKE 'pdx-buffy coat'), 'tube', 'ad_der_cell_tubes_incl_ml_vol', 'ad_tubes', '0', 'pdx-buffy coat|tube', 'ml');
INSERT INTO realiquoting_controls (parent_aliquot_control_id, child_aliquot_control_id, flag_active)
VALUES
((SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-buffy coat|tube'), (SELECT id FROM aliquot_controls WHERE databrowser_label = 'pdx-buffy coat|tube'), '0');
CREATE TABLE `sd_der_pdx_buffy_coats` (
  `sample_master_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_der_pdx_buffy_coats_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL,
  `version_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE `sd_der_pdx_buffy_coats`
  ADD KEY `FK_sd_der_pdx_buffy_coats_sample_masters` (`sample_master_id`);
ALTER TABLE `sd_der_pdx_buffy_coats_revs`
  ADD PRIMARY KEY (`version_id`);
ALTER TABLE `sd_der_pdx_buffy_coats_revs`
  MODIFY `version_id` int(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `sd_der_pdx_buffy_coats`
  ADD CONSTRAINT `FK_sd_der_pdx_buffy_coats_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`);
INSERT IGNORE INTO i18n (id,en,fr)
VALUES  
('pdx-buffy coat', 'PDX-Buffy Coat', 'PDX-Puffy Coat');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #77 : Clarify the message if there is no data related to a field
-- Updated StructutreHelper.php : AppController::addWarningMsg
-- https://gitlab.com/ctrnet/atim/-/issues/77
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO i18n (id, en, fr) VALUES ('missing reference key [%s], value not included in list of accepted values for field [%s]', 'missing reference key [%s], value not included in list of accepted values for field [%s]', 'clé de référence manquante [%s], valeur non incluse dans la liste des valeurs acceptées pour le champ [%s]');
INSERT INTO i18n (id, en, fr) VALUES ('no data for [%s.%s], the field probably does not exist in database table', 'no data for [%s.%s], the field probably does not exist in database table', 'pas de données pour [%s.%s], le champ n\’existe probablement pas dans la table de la base de données');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #157: Create new sample type 'viral rna'. 
-- https://gitlab.com/ctrnet/atim/-/issues/157
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `sample_controls` (`id`, `sample_type`, `sample_category`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`) VALUES
(null, 'viral rna', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_viral_rnas', 0, 'viral rna');

CREATE TABLE IF NOT EXISTS `sd_der_viral_rnas` (
  `sample_master_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `sd_der_viral_rnas_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
ALTER TABLE `sd_der_viral_rnas`
  ADD CONSTRAINT `FK_sd_der_viral_rnas_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('viral rna','Viral RNA','ARN viral');
INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cheek swab'), (SELECT id FROM sample_controls WHERE sample_type = 'viral rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'tissue'), (SELECT id FROM sample_controls WHERE sample_type = 'viral rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'blood'), (SELECT id FROM sample_controls WHERE sample_type = 'viral rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'plasma'), (SELECT id FROM sample_controls WHERE sample_type = 'viral rna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'serum'), (SELECT id FROM sample_controls WHERE sample_type = 'viral rna'), 0, NULL);
INSERT INTO `aliquot_controls` (`id`, `sample_control_id`, `aliquot_type`, `aliquot_type_precision`, `detail_form_alias`, `detail_tablename`, `volume_unit`, `flag_active`, `comment`, `display_order`, `databrowser_label`) VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'viral rna'), 'tube', '(ul + conc)', 'ad_der_tubes_incl_ul_vol_and_conc', 'ad_tubes', 'ul', 0, 'Derivative tube requiring volume in ul and concentration', 0, 'viral rna|tube');
INSERT INTO `realiquoting_controls` (`id`, `parent_aliquot_control_id`, `child_aliquot_control_id`, `flag_active`, `lab_book_control_id`) VALUES
(null, (SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'viral rna'), 
(SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'viral rna'), 0, NULL);       

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #114: Create new sample type 'red blood cell'. 
-- https://gitlab.com/ctrnet/atim/-/issues/114
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'red blood cells', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_red_blood_cells', 0, 'red blood cells');
CREATE TABLE IF NOT EXISTS sd_der_red_blood_cells (
sample_master_id int(11) NOT NULL,
KEY FK_sd_der_red_blood_cells_sample_masters (sample_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS sd_der_red_blood_cells_revs (
sample_master_id int(11) NOT NULL,
version_id int(11) NOT NULL AUTO_INCREMENT,
version_created datetime NOT NULL,
PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
ALTER TABLE sd_der_red_blood_cells
ADD CONSTRAINT FK_sd_der_red_blood_cells_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'blood'), (SELECT id FROM sample_controls WHERE sample_type = 'red blood cells'), 0, NULL);
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label) VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'red blood cells'), 'tube', '', 'ad_der_tubes_incl_ul_vol_and_conc', 'ad_tubes', 'ul', 0, '', 0, 'red blood cells|tube');
INSERT INTO realiquoting_controls (id, parent_aliquot_control_id, child_aliquot_control_id, flag_active, lab_book_control_id) VALUES
(null, (SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'red blood cells'),
(SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'red blood cells'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('red blood cells', 'Red Blood Cells', 'Globules rouges');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #95: Add Plasma and Buffy Coat derivatvie from Bone Marrow 
-- https://gitlab.com/ctrnet/atim/-/issues/95
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone marrow'), (SELECT id FROM sample_controls WHERE sample_type = 'buffy coat'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone marrow'), (SELECT id FROM sample_controls WHERE sample_type = 'plasma'), 0, NULL);

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #154: Bone marrow mononuclear cells (BMMC)  
-- https://gitlab.com/ctrnet/atim/-/issues/154
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `sample_controls` (`id`, `sample_type`, `sample_category`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`) VALUES
(null, 'bmmc', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_bmmcs', 0, 'mononuclear cells');

CREATE TABLE IF NOT EXISTS `sd_der_bmmcs` (
  `sample_master_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `sd_der_bmmcs_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
ALTER TABLE `sd_der_bmmcs`
  ADD CONSTRAINT `FK_sd_der_bmmcs_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`);

INSERT IGNORE INTO i18n (id,en,fr) VALUES ('bmmc','BMMC','BMMC');
INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone marrow'), (SELECT id FROM sample_controls WHERE sample_type = 'bmmc'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bmmc'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bmmc'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL);
INSERT INTO `aliquot_controls` (`id`, `sample_control_id`, `aliquot_type`, `aliquot_type_precision`, `detail_form_alias`, `detail_tablename`, `volume_unit`, `flag_active`, `comment`, `display_order`, `databrowser_label`) VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bmmc'), 'tube', '', 'ad_der_cell_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 'bmmc|tube');
INSERT INTO `realiquoting_controls` (`id`, `parent_aliquot_control_id`, `child_aliquot_control_id`, `flag_active`, `lab_book_control_id`) VALUES
(null, (SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'bmmc'), 
(SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'bmmc'), 0, NULL);     

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #121: White blood cells  
-- https://gitlab.com/ctrnet/atim/-/issues/121
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO sample_controls
(id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label) VALUES
(null, 'white blood cells', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_white_blood_cells', 0, 'white blood cells');
CREATE TABLE IF NOT EXISTS sd_der_white_blood_cells (
sample_master_id int(11) NOT NULL,
KEY FK_sd_der_white_blood_cells_sample_masters (sample_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS sd_der_white_blood_cells_revs (
sample_master_id int(11) NOT NULL,
version_id int(11) NOT NULL AUTO_INCREMENT,
version_created datetime NOT NULL,
PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
ALTER TABLE sd_der_white_blood_cells
ADD CONSTRAINT FK_sd_der_white_blood_cells_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'blood'), (SELECT id FROM sample_controls WHERE sample_type = 'white blood cells'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'white blood cells'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'white blood cells'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL);
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label) VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'white blood cells'), 'tube', '', 'ad_der_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 'white blood cells|tube');
INSERT INTO realiquoting_controls (id, parent_aliquot_control_id, child_aliquot_control_id, flag_active, lab_book_control_id) VALUES
(null, (SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'white blood cells'),
(SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'white blood cells'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('white blood cells', 'White Blood Cells', 'Cellules blanches du sang');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #120: Create Bronchial Washing  
-- https://gitlab.com/ctrnet/atim/-/issues/120
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'bronchial washing', 'specimen', 'sd_spe_bronchial_washings,specimens', 'sd_spe_bronchial_washings', 0, 'bronchial washing');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, null, (SELECT id FROM sample_controls WHERE sample_type = 'bronchial washing'), 0, NULL);
DROP TABLE IF EXISTS sd_spe_bronchial_washings;
CREATE TABLE IF NOT EXISTS sd_spe_bronchial_washings (
sample_master_id int(11) NOT NULL,
collected_volume decimal(10,5) DEFAULT NULL,
collected_volume_unit varchar(20) DEFAULT NULL,
KEY FK_sd_spe_bronchial_washings_sample_masters (sample_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS sd_spe_bronchial_washings_revs;
CREATE TABLE IF NOT EXISTS sd_spe_bronchial_washings_revs (
sample_master_id int(11) NOT NULL,
collected_volume decimal(10,5) DEFAULT NULL,
collected_volume_unit varchar(20) DEFAULT NULL,
version_id int(11) NOT NULL AUTO_INCREMENT,
version_created datetime NOT NULL,
PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE sd_spe_bronchial_washings
ADD CONSTRAINT FK_sd_spe_bronchial_washings_sample_masters FOREIGN KEY (sample_master_id) REFERENCES sample_masters (id);
INSERT INTO structures(alias) VALUES ('sd_spe_bronchial_washings');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='sd_spe_bronchial_washings'),
(SELECT id FROM structure_fields WHERE model='SampleDetail' AND tablename='' AND field='collected_volume' AND type='float_positive' AND structure_value_domain  IS NULL),
'1', '442', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='sd_spe_bronchial_washings'),
(SELECT id FROM structure_fields WHERE model='SampleDetail' AND tablename='' AND field='collected_volume_unit' AND type='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='sample_volume_unit')  ),
'1', '443', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '1', '0', '0');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label) VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bronchial washing'), 'tube', '', 'ad_spec_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 'bronchial washing|tube');
INSERT INTO realiquoting_controls (id, parent_aliquot_control_id, child_aliquot_control_id, flag_active, lab_book_control_id) VALUES
(null, (SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'bronchial washing'),
(SELECT aliquot_controls.id FROM aliquot_controls INNER JOIN sample_controls ON sample_controls.id = sample_control_id AND sample_type = 'bronchial washing'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('bronchial washing', 'Bronchial Washing', 'Lavage bonchique');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #143: Add icd-10-CA codes help message and change these linked to icd-10-WHO to clarify  
-- https://gitlab.com/ctrnet/atim/-/issues/143
-- ----------------------------------------------------------------------------------------------------------------------------------

REPLACE INTO i18n (id,en,fr)
VALUES
("help_dx_icd10_code_who", "The disease or condition as represented by a code (ICD-10-WHO codes from the 2009 Version of Stats Canada).", "La maladie ou la condition représentée par un code (ICD-10-WHO codes de la version 2009 de 'Stats Canada')."),
("help_icd_10_code_who", "ICD-10-WHO codes from the 2009 Version of Stats Canada", "ICD-10-WHO codes de la version 2009 de 'Stats Canada'."),
("help_family_history_dx_icd10_code_who", "The disease or condition as represented by a code (ICD-10-WHO codes from the 2009 Version of Stats Canada).", "La maladie ou la condition représentée par un code (ICD-10-WHO codes de la version 2009 de 'Stats Canada')."),
("help_cause_of_death_icd10_code_who", "The disease or injury which initiated the train of morbid events leading directly to a person's death or the circumstances of the accident or violence which produced the fatal injury, as represented by a code (ICD-10-WHO codes from the 2009 Version of Stats Canada).", "La maladie ou la blessure qui a initié la série d'événements de morbidité, menant directement au décès de la personne ou les circonstances de l'accident ou violence ayant produit une blessure fatale, telle que représentée par un code (ICD-10-WHO codes de la version 2009 de 'Stats Canada')."),
("help_2nd_cause_of_death_icd10_code_who", "Any secondary disease, injury, circumstance of accident or violence which may have contributed to the person's death as represented by a code (ICD-10-WHO codes from the 2009 Version of Stats Canada).", "N'importe quelle maladie secondaire, blessure, circonstance d'accident ou violence qui peut avoir contribué à  la mort de la personne, représenté par un code (ICD-10-WHO codes de la version 2009 de 'Stats Canada').");

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("help_dx_icd10_code_ca", 
"The disease or condition as represented by a code (ICD-10-CA codes from version ICD10CA_Code_Eng_Desc2010_V2_0 of the ICIS/CIHI).", 
"La maladie ou la condition représentée par un code (ICD-10-CA codes de la version ICD10CA_Code_Fra_Desc2010_V2_0 de 'ICIS/CIHI')."),
("help_cause_of_death_icd10_code_ca", 
"The disease or injury which initiated the train of morbid events leading directly to a person's death or the circumstances of the accident or violence which produced the fatal injury, as represented by a code (ICD-10-CA codes from version ICD10CA_Code_Eng_Desc2010_V2_0 of the ICIS/CIHI).", 
"La maladie ou la blessure qui a initié la série d'événements de morbidité, menant directement au décès de la personne ou les circonstances de l'accident ou violence ayant produit une blessure fatale, telle que représentée par un code (ICD-10-CA codes de la version ICD10CA_Code_Fra_Desc2010_V2_0 de 'ICIS/CIHI')."),
("help_2nd_cause_of_death_icd10_code_ca", 
"Any secondary disease, injury, circumstance of accident or violence which may have contributed to the person's death as represented by a code (ICD-10-CA codes from version ICD10CA_Code_Eng_Desc2010_V2_0 of the ICIS/CIHI).", 
"N'importe quelle maladie secondaire, blessure, circonstance d'accident ou violence qui peut avoir contribué à  la mort de la personne, représenté par un code (ICD-10-CA codes de la version ICD10CA_Code_Fra_Desc2010_V2_0 de 'ICIS/CIHI')."),
("help_family_history_dx_icd10_code_ca", 
"The disease or condition as represented by a code (ICD-10-CA codes from version ICD10CA_Code_Eng_Desc2010_V2_0 of the ICIS/CIHI).", 
"La maladie ou la condition représentée par un code (ICD-10-CA codes de la version ICD10CA_Code_Fra_Desc2010_V2_0 de 'ICIS/CIHI').");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #34: Duplicated field to remove : DerivativeDetail.creation_datetime
-- https://gitlab.com/ctrnet/atim/-/issues/34
-- ----------------------------------------------------------------------------------------------------------------------------------

-- SELECT * FROM structure_fields WHERE `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='creation_datetime' AND `type`='datetime' AND `structure_value_domain` IS NULL AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='inv_creation_datetime_defintion' AND `language_label`='creation date' AND `language_tag`=''
-- 
-- select structure_alias,structure_field_id,plugin,model,tablename,field,structure_value_domain_name 
-- from view_structure_formats_simplified 
-- where `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='creation_datetime';

SET @first_structure_field_id = (
  SELECT id 
  FROM structure_fields 
  WHERE `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='creation_datetime' AND `type`='datetime' AND `structure_value_domain` IS NULL AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='inv_creation_datetime_defintion' AND `language_label`='creation date' AND `language_tag`=''
  ORDER BY id ASC LIMIT 0 ,1); 

SET @second_structure_field_id = (
  SELECT id 
  FROM structure_fields 
  WHERE `model`='DerivativeDetail' AND `tablename`='derivative_details' AND `field`='creation_datetime' AND `type`='datetime' AND `structure_value_domain` IS NULL AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='inv_creation_datetime_defintion' AND `language_label`='creation date' AND `language_tag`=''
  AND id > @first_structure_field_id
  ORDER BY id ASC LIMIT 0 ,1); 

UPDATE structure_validations SET structure_field_id = @first_structure_field_id WHERE structure_field_id = IFNULL(@second_structure_field_id, @first_structure_field_id);
UPDATE structure_formats SET structure_field_id = @first_structure_field_id WHERE structure_field_id = IFNULL(@second_structure_field_id, @first_structure_field_id);

DELETE FROM structure_fields WHERE id = @second_structure_field_id;

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #129: Check the unique fields for Structure_fields table and remove duplicated one
-- https://gitlab.com/ctrnet/atim/-/issues/129
-- ----------------------------------------------------------------------------------------------------------------------------------

-- SELECT * FROM (
--   select count(id) c, group_concat(id), field, type, model, tablename, structure_value_domain FROM structure_fields GROUP BY field, 
--   type, model, tablename, structure_value_domain ORDER BY c DESC
-- ) res WHERE res.c > 1

-- Participant.last_name

SET @first_structure_field_id = (
  SELECT id 
  FROM structure_fields 
  WHERE `model`='Participant' AND `tablename`='participants' AND `field`='last_name' AND `type`='input' AND `structure_value_domain` IS NULL
  ORDER BY id ASC LIMIT 0 ,1); 

SET @second_structure_field_id = (
  SELECT id 
  FROM structure_fields 
  WHERE `model`='Participant' AND `tablename`='participants' AND `field`='last_name' AND `type`='input' AND `structure_value_domain` IS NULL
  AND id > @first_structure_field_id
  ORDER BY id ASC LIMIT 0 ,1); 

UPDATE structure_validations SET structure_field_id = @first_structure_field_id WHERE structure_field_id = IFNULL(@second_structure_field_id, @first_structure_field_id);
UPDATE structure_formats SET structure_field_id = @first_structure_field_id WHERE structure_field_id = IFNULL(@second_structure_field_id, @first_structure_field_id);

DELETE FROM structure_fields WHERE id = @second_structure_field_id;

-- Participant.first_name

SET @first_structure_field_id = (
  SELECT id 
  FROM structure_fields 
  WHERE `model`='Participant' AND `tablename`='participants' AND `field`='first_name' AND `type`='input' AND `structure_value_domain` IS NULL
  ORDER BY id ASC LIMIT 0 ,1); 

SET @second_structure_field_id = (
  SELECT id 
  FROM structure_fields 
  WHERE `model`='Participant' AND `tablename`='participants' AND `field`='first_name' AND `type`='input' AND `structure_value_domain` IS NULL
  AND id > @first_structure_field_id
  ORDER BY id ASC LIMIT 0 ,1); 

UPDATE structure_validations SET structure_field_id = @first_structure_field_id WHERE structure_field_id = IFNULL(@second_structure_field_id, @first_structure_field_id);
UPDATE structure_formats SET structure_field_id = @first_structure_field_id WHERE structure_field_id = IFNULL(@second_structure_field_id, @first_structure_field_id);

DELETE FROM structure_fields WHERE id = @second_structure_field_id;

UPDATE structure_formats SET `flag_override_label`='1', `language_label`='name' WHERE structure_id=(SELECT id FROM structures WHERE alias='number_of_elements_per_participant') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='first_name' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='1');

-- InventoryManagement.custom.event (structure : custom_aliquot_storage_history)

UPDATE structure_fields SET field = 'storage_event' WHERE plugin = 'InventoryManagement' AND `model`='custom' AND `tablename`='' AND `field`='event' AND `type`='input' AND `structure_value_domain` IS NULL;

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #206: Make sop code + version unique instead SOP code only
-- https://gitlab.com/ctrnet/atim/-/issues/206
-- ----------------------------------------------------------------------------------------------------------------------------------

DELETE FROM structure_validations
WHERE structure_field_id=(SELECT id FROM structure_fields WHERE model='SopMaster' AND tablename='sop_masters' AND field='code' AND structure_value_domain  IS NULL  AND flag_confidential='0')
AND rule = 'isUnique';
INSERT IGNORE INTO i18n (id,en,fr)
VALUES ('the combination of the code and version must be unique', "The combination of the code and version must be unique", "La combinaison du code et de la version doit être unique");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #132: Remove realiquoting controls
-- https://gitlab.com/ctrnet/atim/-/issues/132
-- ----------------------------------------------------------------------------------------------------------------------------------

-- Keep on ly records with lab_book_control_id IS NOT NULL.
-- Table realiquoting_controls should not be used any more. The table is not deleted in v2.7.3 to allow
-- the support of labbook for a short period of time until we replace lab book by a new feature.

DELETE FROM realiquoting_controls WHERE lab_book_control_id IS NULL;

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #194: Date of detail model record with no table_name definition generate a warning message
-- https://gitlab.com/ctrnet/atim/-/issues/194
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id,en,fr)
VALUES ('cannot load model for field [%s] with id %s. date accuracy feature can not be used on this field because field tablename is probably not set. check and set field tablename.',
"System cannot load model for field [%s] with id %s. The date accuracy feature can not be used on this field because field table name is probably not set. check and set the tablename field.",
"Impossible de charger le modèle pour le champ [% s] avec l'id % s. La fonction de date approcimative ne peut pas être utilisée sur ce champ car le nom de la table lié au champ n'est probablement pas défini. Vérifier et définir le nom de la table du champ."),
("date accuracy feature can not be used on following field(s) because field tablename is probably not set. check and set field tablename for: <br>- %s",
"The date accuracy feature can not be used on following field(s) because tablename field is not probably set. Check and set tablename field for: <br>- %s",
"La fonction de date approcimative ne peut pas être utilisée sur ce(s) champ(s) car le nom de la table lié au(x) champ(s) n'est probablement pas défini. Vérifier et définir le nom de la table pour: <br>- %s"),
("%s field [%s] in form structure '%s.'", "The (%s) field [%s] in form structure '%s.'", "Le champ (%s) [%s] dans le formulaire '%s.");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #203: Unable to open sample tree view and sample detail form when too many aliquots are linked to this sample
-- https://gitlab.com/ctrnet/atim/-/issues/203
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('message_for_collection_tree_view');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'Generated', '', 'collection_tree_view_item_summary', 'input',  NULL , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='message_for_collection_tree_view'), 
(SELECT id FROM structure_fields WHERE `model`='Generated' AND `tablename`='' AND `field`='collection_tree_view_item_summary' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'0', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES ('there are too many aliquots for display (%s)', 'There are too many aliquots for display (%s).', "Il y a trop de aliquotes pour l'affichage (%s).");

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Hide participant chronology time column in trunk version
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE structure_formats 
SET `flag_index`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='chronology') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='custom' AND `tablename`='' AND `field`='time' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Issue #123: Create nasopharyngeal swab
-- https://gitlab.com/ctrnet/atim/-/issues/123
-- &
-- Issue #156: Change cheek swab to urt (upper respiratory tract) and cheek swab (as sb covid)
-- https://gitlab.com/ctrnet/atim/-/issues/156
-- ----------------------------------------------------------------------------------------------------------------------------------

ALTER TABLE sd_spe_cheek_swabs RENAME TO sd_spe_urt_cheek_swabs;
ALTER TABLE sd_spe_cheek_swabs_revs RENAME TO sd_spe_urt_cheek_swabs_revs;

UPDATE aliquot_controls 
SET databrowser_label = 'urt or cheek swab|tube' 
WHERE sample_control_id = (SELECT id FROM sample_controls WHERE sample_type = 'cheek swab')
AND aliquot_type = 'tube';

UPDATE sample_controls 
SET sample_type = 'urt or cheek swab', 
detail_form_alias = CONCAT(detail_form_alias, ',sd_spe_urt_cheek_swabs'), 
detail_tablename = 'sd_spe_urt_cheek_swabs', 
databrowser_label = 'urt or cheek swab' 
WHERE sample_type = 'cheek swab';

UPDATE sample_masters SET initial_specimen_sample_type = 'urt or cheek swab' WHERE initial_specimen_sample_type = 'cheek swab';
UPDATE sample_masters SET parent_sample_type = 'urt or cheek swab' WHERE parent_sample_type = 'cheek swab';

INSERT INTO structures(`alias`) VALUES ('sd_spe_urt_cheek_swabs');
INSERT INTO i18n (id,en,fr) VALUES ('urt or cheek swab', 'URT or Cheek Swab', 'Ecouvillon des VRS ou joue');

-- collection site

ALTER TABLE sd_spe_urt_cheek_swabs ADD COLUMN collection_site varchar(250) DEFAULT NULL;
ALTER TABLE sd_spe_urt_cheek_swabs_revs ADD COLUMN collection_site varchar(250) DEFAULT NULL;
UPDATE sd_spe_urt_cheek_swabs SET collection_site = 'cheek';
UPDATE sd_spe_urt_cheek_swabs_revs SET collection_site = 'cheek';
INSERT INTO structure_value_domains (domain_name, source) VALUES ('urt_cheek_swab_collection_sites', 'StructurePermissibleValuesCustom::getCustomDropdown(\'URT or Cheek Swab Sites\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('URT or Cheek Swab Sites', 1, 100, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'URT or Cheek Swab Sites');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("unknown", "Unknown", "Inconnu", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cheek", "Cheek", "Joue", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("tongue", "Tongue", "Langue", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("nasopharynx", "Nasopharynx", "Nasopharynx", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("oropharynx", "Oropharynx", "Oropharynx", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("larynx", "Larynx", "Larynx", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("laryngopharynx", "Laryngo-Pharynx", "Laryngo-pharynx", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("deep nasal", "Deep nasal", "Nasal profond", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("throat", "Throat", "Gorge", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("oral cavity", "Oral Cavity", "Cavité buccale", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("nasal cavity", "Nasal Cavity", "Cavité nasale", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "Autre", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'SampleDetail', 'sd_spe_urt_cheek_swabs', 'collection_site', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='urt_cheek_swab_collection_sites') , '0', 'class=atim-multiple-choice', '', 'help_urt_cheek_swab_collection_sites', 'collection site', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sd_spe_urt_cheek_swabs'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_urt_cheek_swabs' AND `field`='collection_site'), 
'1', '441', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sample_masters_for_collection_tree_view'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_urt_cheek_swabs' AND `field`='collection_site'), 
'0', '2', '', '0', '1', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', (SELECT flag_active FROM sample_controls INNER JOIN parent_to_derivative_sample_controls ON sample_controls.id = derivative_sample_control_id WHERE sample_type = 'urt or cheek swab'), '0', '0', '0');
INSERT INTO `structure_validations` (`id`, `structure_field_id`, `rule`, `on_action`, `language_message`) 
VALUES 
(NULL, 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_urt_cheek_swabs' AND `field`='collection_site'), 'notBlank', '', '');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES 
('collection site', 'collection site', 'Zone de prélèvement'),
('help_urt_cheek_swab_collection_sites', 'Site(s) of the collection by swab in the upper respiratory tract or cheek.', 'Site(s) de prélèvement par écouvillon dans les voies respiratoires supérieures ou sur la joue.');

-- swab transport medium

ALTER TABLE sd_spe_urt_cheek_swabs ADD COLUMN transport_medium varchar(100) DEFAULT NULL;
ALTER TABLE sd_spe_urt_cheek_swabs_revs ADD COLUMN transport_medium varchar(100) DEFAULT NULL;
INSERT INTO structure_value_domains (domain_name, source) VALUES ('urt_cheek_swab_transport_mediums', 'StructurePermissibleValuesCustom::getCustomDropdown(\'URT or Cheek Swab Transport Mediums\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('URT or Cheek Swab Transport Mediums', 1, 100, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'URT or Cheek Swab Transport Mediums');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("unknown", "Unknown", "Inconnu", "101", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("utm", "UTM", "UTM", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "Autre", "100", "1", @control_id, NOW(), NOW(), @user_id, @user_id);
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'SampleDetail', 'sd_spe_urt_cheek_swabs', 'transport_medium', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='urt_cheek_swab_transport_mediums') , '0', '', '', '', 'transport medium', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sd_spe_urt_cheek_swabs'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_urt_cheek_swabs' AND `field`='transport_medium'), 
'1', '442', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES 
('transport medium', 'Transport Medium', 'Milieu de transport ');

-- ----------------------------------------------------------------------------------------------------------------------------------
--	issue (https://gitlab.com/ctrnet/atim/-/issues/227): Data Browser - Buttons label and summary
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('new browsing/search', 'New Browsing/Search', 'Nouvelle navigation/recherche'),
('display browsing tree', 'Display Browsing', 'Afficher la navigation'),
('delete browsing', 'Delete Browsing', 'Supprimer la navigation'),
('save browsing', 'Save Browsing', 'Sauvegarder la navigation'),
('edit accessibility', 'Edit Accessibility', 'Modifier l''accessibilité'),
('databrowser unused parents msg', 'Records of the previous node unlinked to displayed records', "Enregistrements du noeud précédent dissociés des enregistrements affichés"),
('edit browsing', 'Edit browsing', 'Modifier la navigation');

REPLACE INTO i18n (id,en,fr)
VALUES
('unused parents', 'Unused Parents', 'Parents non utilisés'),
('save browsing steps', 'Save Browsing Steps', 'Sauvegarder les étapes de navigation'),
('shared browsing trees', 'Shared Browsing', 'Navigations partagées'),
('saved browsing trees', 'Saved Browsing', 'Navigations enregistrées');

UPDATE structure_formats SET `flag_summary`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='datamart_browsing_indexes') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='sharing_status' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='batch_sets_sharing_status') AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_summary`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='datamart_browsing_indexes') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='locked' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', 'Generated', '', 'created_by_data', 'input',  NULL , '0', '', '', '', 'created by', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='datamart_browsing_indexes'), 
(SELECT id FROM structure_fields WHERE `model`='Generated' AND `tablename`='' AND `field`='created_by_data' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='created by' AND `language_tag`=''), 
'1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', 'BrowsingIndex', 'datamart_browsing_indexes', 'id', 'input',  NULL , '0', '', '', '', 'browsing #', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='datamart_browsing_indexes'), 
(SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='id' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='browsing #' AND `language_tag`=''), 
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
INSERT IGNORE INTO i18n (id,en,fr) VALUES ("browsing #", "Browsing #", "Navigation #");
UPDATE structure_formats SET `flag_edit`='0', `flag_edit_readonly`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='datamart_browsing_indexes') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='BrowsingIndex' AND `tablename`='datamart_browsing_indexes' AND `field`='id' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

UPDATE `menus` SET `use_summary` = 'Datamart.BrowsingIndex::summary' WHERE `language_title` = 'browsing' AND `use_link` = '/Datamart/Browser/index';
-- ----------------------------------------------------------------------------------------------------------------------------------
--	issue (https://gitlab.com/ctrnet/atim/-/issues/222): Change ID length for `missing_translations` table
-- ----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `missing_translations` CHANGE `id` `id` varchar(1023) COLLATE 'latin1_swedish_ci' NOT NULL FIRST;

-- ----------------------------------------------------------------------------------------------------------------------------------
--	issue (https://gitlab.com/ctrnet/atim/-/issues/217): Misspelled Column Name
-- ----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `datamart_reports` CHANGE `limit_access_from_datamart_structrue_function` `limit_access_from_datamart_structure_function` tinyint(1) NOT NULL DEFAULT '0';

-- ----------------------------------------------------------------------------------------------------------------------------------
--	missing i18n for newVersionSetUp()
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (id,en,fr)
VALUES 	
("following automatic customisations have been executed : %s", 
"The following automatic customisations have been executed : %s", 
"Les customizations automatiques suivantes ont été éxecutées: %s"),
("datamart_browsing_controls has been updated to let people to search on %s.", 
"The table 'datamart_browsing_controls' has ben updated to let people to search on model '%s' with the Databrowser",
"La table 'datamart_browsing_controls' a été mise à jour pour permettre les recherches sur le model '%s' dans le navigateur de données."),
("datamart_browsing_controls has been updated to not let people to search on %s.", 
"The table 'datamart_browsing_controls' has been updated to not let people to search on model '%s' with the Databrowser",
"La table 'datamart_browsing_controls' a été mise à jour pour ne pas permettre les recherches sur le modlès '%s' dans le navigateur de données."),            
("datamart_browsing_controls has been updated to let people to search on following models : %s",
"The table 'datamart_browsing_controls' has been updated to let people to search with the Databrowser on following models: %s",
"La table 'datamart_browsing_controls' a été mise à jour pour permettre les recherches sur les modeles suivants dans le navigateur de données: %s"), 
("datamart_browsing_controls has been updated to not let people to search on following models : %s",
"The table 'datamart_browsing_controls' has been updated to not let people to search with the Databrowser on following models: %s",
"La table 'datamart_browsing_controls' a été mise à jour pour ne pas permettre les recherches sur les modeles suivants dans le navigateur de données: %s"),
("letNewVersionSetupCustomisePartiallyATiM_active_message", 
"<FONT COLOR='red'><b>The ATiM automatic customisation process has been activated. Part of the menus, displayed fields and databrowser models links have been activated automatically or not based on 'Controls' tables records, 'Core' variables and 'Databrowser' configurations:
<br>- Review details attached to 'AUTOMATIC CUSTOMISATION' comments in AppController.newVersionSetUp() for more information. 
<br>- Validate automatic customisation done on your local installation.
<br>- Change core variable 'letNewVersionSetupCustomisePartiallyATiM' to 'false' to inactivate the feature.</b></FONT>", 
"<FONT COLOR='red'><b>Le processus de personnalisation automatique d'ATiM a été activé. Une partie des menus, des champs affichés et des liens dans le navigateur de données ont été automatiquement activés ou non en fonction des enregistrements des tables 'Controls', des variables 'Core' et des configurations du 'Databrowser':
<br> - Regardez les détails attachés aux commentaires 'PERSONNALISATION AUTOMATIQUE' dans 'AppController.newVersionSetUp()' pour plus d'information.
<br> - Validez la personnalisation automatique effectuée sur votre installation locale.
<br> - Changez la variable du core «letNewVersionSetupCustomisePartiallyATiM» à «false» pour desactiver la fonctionnalité.</b></FONT>"),
("letNewVersionSetupCustomisePartiallyATiM_not_active_message", 
"<FONT COLOR='red'><b>The ATiM automatic customisation process has not been activated. When active, part of the menus, displayed fields and databrowser models links can been automatically activated or not based on 'Controls' tables records, 'Core' variables and 'Databrowser' configurations:
<br>- Review details attached to 'AUTOMATIC CUSTOMISATION' comments in AppController.newVersionSetUp() for more information. 
<br>- Change core variable 'letNewVersionSetupCustomisePartiallyATiM' to 'true' to activate the feature.</b></FONT>", 
"<FONT COLOR='red'><b>Le processus de personnalisation automatique d'ATiM n'a pas été activé. Si la fonctionnalité est activée, une partie des menus, des champs affichés et des liens dans le navigateur de données peuvent être automatiquement activés ou non en fonction des enregistrements des tables 'Controls', des variables 'Core' et des configurations du 'Databrowser':
<br> - Regardez les détails attachés aux commentaires 'PERSONNALISATION AUTOMATIQUE' dans 'AppController.newVersionSetUp()' pour plus d'information.
<br> - Validez la personnalisation automatique effectuée sur votre installation locale.
<br> - Changez la variable du core «letNewVersionSetupCustomisePartiallyATiM» par «true» pour activer la fonctionnalité.</b></FONT>"),
("structure 'clinicalcollectionlinks' has been updated to not let people to view '%s' data.",
"The structure 'clinicalcollectionlinks' has been updated to hide %s data.",
"La structure 'clinicalcollectionlinks' a été mise à jour pour cacher les données du model '%s%'.");

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("launch of 'add to order' actions have been updated based on the core variable 'order_item_type_config'.",
"The activation of the 'Add to Order' actions have been updated based on the core variable 'order_item_type_config'.",
"L'activation des actions 'Ajout à la commande' a été mises à jour en fonction de la valeur de la variable'order_item_type_config'.");

-- ----------------------------------------------------------------------------------------------------------------------------------
--	ATiM version
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;
INSERT INTO `versions` (version_number, date_installed, trunk_build_number, branch_build_number)
VALUES
('2.7.3', NOW(),'to complete when install ATiM','to complete when install ATiM');
