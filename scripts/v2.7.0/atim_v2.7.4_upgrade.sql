-- ----------------------------------------------------------------------------------------------------------------------------------
-- ATiM Database Upgrade Script
-- Version: 2.7.4
--
-- For more information:
--    ./app/scripts/v2.7.0/ReadMe.txt
--    https://atim-software.ca/
-- ----------------------------------------------------------------------------------------------------------------------------------

SET @user_id = (SELECT id FROM users WHERE username = 'system');
SET @user_id = (SELECT IFNULL(@user_id, 1));


-- <editor-fold desc="Import Batch Tool">

  -- <editor-fold desc="Modify missing_translations table">
ALTER TABLE `missing_translations` CHANGE `id` `id` varchar(1023) COLLATE 'latin1_swedish_ci' NOT NULL FIRST;
  -- </editor-fold>

  -- <editor-fold desc="Create menu">
INSERT INTO menus (id, parent_id, is_root, display_order, language_title, language_description, use_link, use_summary, flag_active, flag_submenu)
VALUES ("tool_CAN_101", "core_CAN_33", 1, 100, "batch data upload", "", "/Tools/Imports/", 'Tools.BatchUpload::summary', 1, 1);
  -- </editor-fold>

  -- <editor-fold desc="Create tables">

  -- <editor-fold desc="Create batch_upload_controls to save the imports">
CREATE TABLE `batch_upload_controls` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(50) NOT NULL,
  `version` varchar(10) NOT NULL,
  `script` varchar(255) NOT NULL,
  `description` text NULL,
  `template_csv` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL,
  `max_csv_lines_number` int(11) NOT NULL DEFAULT '-1',
  `flag_active` tinyint NOT NULL DEFAULT '1',
  `flag_manipulate_confidential_data` tinyint(1) DEFAULT 1,
  CONSTRAINT unique_upload_ctrls UNIQUE (`name`,`version`)
) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';

  -- </editor-fold>

  -- <editor-fold desc="Create batch_upload_messages to save the import messages">

CREATE TABLE `batch_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `batch_upload_control_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `upload_datetime` datetime NOT NULL,
  `csv_file_name` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL,
  CONSTRAINT `FK_batch_upload_control_batch_uploads` FOREIGN KEY (`batch_upload_control_id`) REFERENCES `batch_upload_controls` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `FK_batch_upload_control_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION
) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';

  -- </editor-fold>

  -- <editor-fold desc="Create batch_upload_messages to save the import messages">

DROP TABLE IF EXISTS `batch_upload_messages`;
CREATE TABLE `batch_upload_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `batch_upload_id` int(11) NOT NULL,
  `section_title` varchar(255) NOT NULL,
  `title` varchar(2500) NOT NULL,
  `message_type` varchar(30) NOT NULL,
  `message` varchar(2500) NOT NULL,
  CONSTRAINT `FK_batch_upload_messages_users` FOREIGN KEY (`batch_upload_id`) REFERENCES `batch_uploads` (`id`) ON DELETE NO ACTION

) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';

  -- </editor-fold>

  -- <editor-fold desc="Create temprary table import_list">

CREATE TABLE `import_lists` (
  `batch_upload_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
   `user` varchar(100) COLLATE 'latin1_swedish_ci' NOT NULL,
  `upload_datetime` datetime NOT NULL,
  `csv_file_name` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL,
  `status` varchar(50) COLLATE 'latin1_swedish_ci' NOT NULL,
  `batch_upload_control_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE 'latin1_swedish_ci' NOT NULL,
  `version` varchar(10) DEFAULT NULL,
  `script` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL,
  `description` text COLLATE 'latin2_bin' NULL,
  `max_csv_lines_number` int(11) NULL DEFAULT '-1',
  `flag_active` varchar(50) NULL DEFAULT '1',
  FOREIGN KEY (`batch_upload_id`) REFERENCES `batch_uploads` (`id`),
  FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  FOREIGN KEY (`batch_upload_control_id`) REFERENCES `batch_upload_controls` (`id`)
) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';

  -- </editor-fold>

  -- </editor-fold>

  -- <editor-fold desc="Create structures">

  -- <editor-fold desc="Create the import select main page structure">

INSERT INTO structures(`alias`) VALUES ('import_select_page');

INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('import script type', '', '', 'Tools.BatchUploadControl::importScriptList');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
('Core', 'FunctionManagement', '', 'filename', 'file',  NULL , '0', 'class=upload', '', 'batch data upload description', 'select the CSV file', ''),
('Core', 'FunctionManagement', '', 'script', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='import script type') , '0', '', '', '', 'upload type', ''),
('Core', 'FunctionManagement', '', 'define_csv_separator', 'input',  NULL , '0', 'size=3,maxlength=1', '', 'help_define_csv_separator', 'define_csv_separator', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
((SELECT id FROM structures WHERE alias='import_select_page'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='filename' AND `type`='file' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=upload' AND `default`='' AND `language_help`='batch data upload description' AND `language_label`='select the CSV file' AND `language_tag`=''),
'1', '1', 'batch data upload', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='import_select_page'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='script' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='import script type')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='upload type' AND `language_tag`=''),
'1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='import_select_page'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='define_csv_separator' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3,maxlength=1' AND `default`='' AND `language_help`='help_define_csv_separator' AND `language_label`='define_csv_separator' AND `language_tag`=''),
'1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

INSERT INTO `structure_validations` (`structure_field_id`, `rule`) VALUES
((SELECT id FROM structure_fields WHERE `field`='filename' AND `type`='file' AND `model`='FunctionManagement' AND `tablename`='' AND `structure_value_domain` IS NULL ), 'notBlank'),
((SELECT id FROM structure_fields WHERE `field`='script' AND `type`='select' AND `model`='FunctionManagement' AND `tablename`='' AND `structure_value_domain` = (SELECT id FROM structure_value_domains WHERE domain_name = 'import script type')), 'notBlank');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Core', 'FunctionManagement', '', 'csv_value_language', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='lang') , '0', '', '', '', 'csv value language', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='import_select_page'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='csv_value_language' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='lang')), 
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
UPDATE structure_fields SET  `default`='eng' WHERE model='FunctionManagement' AND tablename='' AND field='csv_value_language' AND `type`='select' 
AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='lang');
INSERT INTO `structure_validations` (`structure_field_id`, `rule`) VALUES
((SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='csv_value_language' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='lang')), 'notBlank');
INSERT IGNORE INTO i18n (id, en, fr) VALUES
("csv value language", "Language (data only)", "Langue (données uniquement)");

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Core', 'FunctionManagement', '', 'test_upload', 'checkbox',  NULL , '0', '', '', '', 'test upload in batch only', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='import_select_page'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='test_upload' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`=''), 
'1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT IGNORE INTO i18n (id, en, fr) VALUES
("test upload in batch only", "Test (no data creation/update)", "Test (aucun enregistrement/modification de données)"),
("test of the batch upload title", "File Import Test", "Test d'importation du fichier"),
("test of the batch upload - no recorded/updated data", "File import has been executed in test mode. No data have been recorded or updated.", "L'import de fichier a été exécuté en mode test. Aucune donnée n'a été enregistrée ou mise à jour.");

  -- </editor-fold>

  -- <editor-fold desc="Create the import_select_message_list for the message list page">

INSERT INTO structures(`alias`) VALUES ('import_select_message_list');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
('Core', 'BatchUploadMessage', '', 'section_title', 'input',  NULL , '0', '', '', '', 'section title', ''),
('Core', 'BatchUploadMessage', '', 'message_type', 'input',  NULL , '0', '', '', '', 'type', ''),
('Core', 'BatchUploadMessage', '', 'title', 'input',  NULL , '0', '', '', '', 'title', ''),
('Core', 'BatchUploadMessage', '', 'message', 'input',  NULL , '0', '', '', '', 'message', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
((SELECT id FROM structures WHERE alias='import_select_message_list'),
(SELECT id FROM structure_fields WHERE `model`='BatchUploadMessage' AND `tablename`='' AND `field`='section_title' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='section title' AND `language_tag`=''),
'1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='import_select_message_list'),
(SELECT id FROM structure_fields WHERE `model`='BatchUploadMessage' AND `tablename`='' AND `field`='message_type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='type' AND `language_tag`=''),
'1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='import_select_message_list'),
(SELECT id FROM structure_fields WHERE `model`='BatchUploadMessage' AND `tablename`='' AND `field`='title' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='title' AND `language_tag`=''),
'1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='import_select_message_list'),
(SELECT id FROM structure_fields WHERE `model`='BatchUploadMessage' AND `tablename`='' AND `field`='message' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='message' AND `language_tag`=''),
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');

INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("import_message_types", "", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("error", "error");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="import_message_types"), (SELECT id FROM structure_permissible_values WHERE value="error" AND language_alias="error"), "1", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("warning", "warning");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="import_message_types"), (SELECT id FROM structure_permissible_values WHERE value="warning" AND language_alias="warning"), "2", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("message", "message");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="import_message_types"), (SELECT id FROM structure_permissible_values WHERE value="message" AND language_alias="message"), "3", "1");
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='import_message_types')  WHERE model='BatchUploadMessage' AND tablename='' AND field='message_type' AND `type`='input' AND structure_value_domain  IS NULL ;


  -- </editor-fold>

  -- <editor-fold desc="Create the Alias import_list to show the list of the batch imports">

INSERT INTO structures(`alias`) VALUES ('import_list');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
('Tools', 'ImportList', 'batch_upload_controls', 'name', 'input',  NULL , '0', '', '', '', 'upload type', ''),
('Tools', 'ImportList', 'batch_uploads', 'upload_datetime', 'input',  NULL , '0', '', '', '', 'batch import date', ''),
('Tools', 'ImportList', 'batch_uploads', 'status', 'input',  NULL , '0', '', '', '', 'status', ''),
('Tools', 'ImportList', 'batch_uploads', 'user_id', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='users_list') , '0', '', '', '', 'user', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
((SELECT id FROM structures WHERE alias='import_list'),
(SELECT id FROM structure_fields WHERE `model`='ImportList' AND `tablename`='batch_upload_controls' AND `field`='name' AND `type`='input'),
'1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'),
((SELECT id FROM structures WHERE alias='import_list'),
(SELECT id FROM structure_fields WHERE `model`='ImportList' AND `tablename`='batch_uploads' AND `field`='upload_datetime' AND `type`='input'),
'1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'),
((SELECT id FROM structures WHERE alias='import_list'),
(SELECT id FROM structure_fields WHERE `model`='ImportList' AND `tablename`='batch_uploads' AND `field`='status' AND `type`='input'),
'1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'),
((SELECT id FROM structures WHERE alias='import_list'), 
(SELECT id FROM structure_fields WHERE `model`='ImportList' AND `tablename`='batch_uploads' AND `field`='user_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='users_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='user' AND `language_tag`=''), 
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0');

INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("import_status", "", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("upload_failed", "upload_failed");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="import_status"), (SELECT id FROM structure_permissible_values WHERE value="upload_failed" AND language_alias="upload_failed"), "1", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("upload_success", "upload_success");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="import_status"), (SELECT id FROM structure_permissible_values WHERE value="upload_success" AND language_alias="upload_success"), "2", "1");
INSERT IGNORE INTO i18n (id, en, fr) VALUES
("upload_failed", "Failed", "Échec"),
("upload_success", "Success", "Succès");
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='import_status')  WHERE model='ImportList' AND tablename='batch_uploads' AND field='status' AND `type`='input' AND structure_value_domain  IS NULL ;

INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("upload_test_failed", "upload_test_failed"),("upload_test_success", "upload_test_success");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) 
VALUES
((SELECT id FROM structure_value_domains WHERE domain_name="import_status"), 
(SELECT id FROM structure_permissible_values WHERE value="upload_test_success" AND language_alias="upload_test_success"), "2", "1"),
 ((SELECT id FROM structure_value_domains WHERE domain_name="import_status"), 
(SELECT id FROM structure_permissible_values WHERE value="upload_test_failed" AND language_alias="upload_test_failed"), "2", "1");
INSERT IGNORE INTO i18n (id, en, fr) VALUES
("upload_test_failed", "Test Only (Failed)", "Test seulement (Échec)"),
("upload_test_success", "Test Only (Success)", "Test seulement (Succès)");

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
('Tools', 'ImportList', 'batch_uploads', 'csv_file_name', 'input',  NULL , '0', '', '', '', 'file', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
((SELECT id FROM structures WHERE alias='import_list'),
(SELECT id FROM structure_fields WHERE `model`='ImportList' AND `tablename`='batch_uploads' AND `field`='csv_file_name' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='file' AND `language_tag`=''),
'1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Tools', 'ImportList', 'batch_upload_controls', 'version', 'input',  NULL , '0', 'size=3', '', '', '', 'version');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='import_list'), 
(SELECT id FROM structure_fields WHERE `model`='ImportList' AND `tablename`='batch_upload_controls' AND `field`='version' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='version'), 
'1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0');

INSERT IGNORE INTO i18n (id, en, fr) VALUES ('version', 'Version', 'Version');
  -- </editor-fold>

  -- </editor-fold>

  -- <editor-fold desc="Populate i18n menu">

INSERT IGNORE INTO i18n (id, en, fr) VALUES
("download the upload report", "Download the Report", "Téléchargez le rapport"),
("display the upload report", "Display the Report", "Affichez le rapport"),
("batch data upload", "Batch Data Upload", "Téléchargement données par lots"),
("batch data upload summary", "Upload Summary", "Résumé du téléchargement"),
("batch data upload process version", "Batch Data Upload Version", "Version téléchargement données par lots"),
("batch data upload process", "Batch Data Upload", "Téléchargement données par lots"),
("upload type", "Upload Type", "Type de téléchargement"),
("select the CSV file", "Select CSV File", "Sélectionner fichier CSV"),
("the data file name is too long", "The data file name is too long.", "Le nom du fichier de données est trop long.");
INSERT IGNORE INTO i18n (id, en, fr) VALUES
("section title", "Section Title", "Section titre"),
("batch data upload description", "Batch Data Upload Description", "Description téléchargement données par lots"),
("unable to create csv file", "Unable to create csv file.", "Impossible de créer le fichier csv."),
("the permission of csv directory is not correct",
"The permission on csv directory is not correct.",
"Les permissions sur le répertoire csv ne sont pas correctes.");
INSERT IGNORE INTO i18n (id, en, fr) VALUES
("the directory for uploading the CSV file is not set, contact your administrator",
"The directory for uploading the CSV file is not set, contact your administrator.",
"Le répertoire de téléchargement du fichier CSV n'est pas défini, contactez votre administrateur.");
INSERT IGNORE INTO i18n (id, en, fr) VALUES
("the data file should be in csv format",
"The data file should be in csv format.",
"Le fichier de données doit être au format csv"),
("the file [%s] does not exist.","The file [%s] does not exist.", "Le fichier [%s] n'existe pas."),
("the script for upload [%s] does not exist","The script for upload [%s] does not exist, contact your administrator.", "Le code pour le téléchargement [%s] n'existe pas, contactez votre administrateur."),
("the file for upload has not been selected","The file for upload has not been selected.", "Le fichier pour le téléchargement n'a pas été sélectionné."),

("error in saving csv file on the server, contact administrator","Error in saving csv file on the server, contact administrator.", "Erreur lors de l'enregistrement du fichier csv sur le serveur, contactez l'administrateur."),
("too many records in file","Too many records in file.", "Trop d'enregistrements dans le fichier."),
("there are some errors in batch data upload", "There are some errors in data upload.", "Il y a des erreurs dans le téléchargement des données."),
("the batch data upload could be done successfully", "The batch data upload could be done successfully.", "Le téléchargement des données par lots pourrait être effectué avec succés."),
("the batch data upload done successfully", "The batch data upload done successfully.", "Le téléchargement des données par lots a été effectué avec succés."),
("the number of records [%s] is more than [%s]","The number of records [%s] is more than [%s].", "le nombre d'enregistrements [%s] est supérieur é [%s].");
INSERT IGNORE INTO i18n (id, en, fr) VALUES
("the number of cells (values) of the csv line is different than the number expected based on headers line content",
"The number of cells (or values) of a CSV line is different than the number expected based on headers line content.",
"Le nombre de cellules (ou valeurs) d'une ligne CSV est différent du nombre attendu selon le contenu de la ligne d'en-tétes"),
("see line [%s]", "See ligne [%s].", "Voire ligne [%s]."),
("batch import date", "Date", "Date"),
("batch message list", "Batch Message List", ""),
("batch name", "batch name", ""),
("batch upload summary", "Import", "Téléchargement"),
('csv data check', 'CSV Data Check', 'Vérification des données CSV');
INSERT IGNORE INTO i18n (id, en, fr) VALUES
("successful uploads list", "Successful Uploads List", "Liste téléchargements réussis"),
("the list of all successful uploads", "List of all successful uploads.", "Liste de tous les téléchargements réussis."),
("batch name", "batch name", ""),
("batch import date", "Upload Date", "Date téléchargement"),
("the last uploads list", "Last Uploads List", "Liste derniers téléchargements"),
('the list of the last uploads per each control', "The list of the last uploads per upload Type.", "Liste des derniers téléchargements par type de téléchargement"),
("batch import date", "Date", "Date"),
("download csv file", "Download the CSV file", "Téléchargez le fichier CSV"),
("download templates", "Download CSV Templates", "Télécharger des modèles CSV"),
("new upload", "New Upload", "Nouveau téléchargement");

DELETE FROM i18n WHERE id = 'Error';
INSERT IGNORE INTO i18n (id,en,fr) VALUEs
('error', 'Error', 'Erreur'),
("you don't have permission to launch this batch data upload process", 
"You don't have permission(s) to launch this batch data upload process.", 
"Vous n'avez pas les permissions requises pour exécuter ce téléchargement données par lots."),
('the csv data file is not saved on the server', 'The csv data file is not saved on the server - contact administrator.', 'Le fichier de données csv n''est pas enregistré sur le serveur - contactez l''administrateur.');

INSERT IGNORE INTO i18n (id,en,fr) VALUES
("you are not allowed to access summary data of the batch upload", "You are not allowed to access summary data of the batch upload!", "Vous n'êtes pas autorisé à ccéder aux données du téléchargement.'");
  -- </editor-fold>

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/225): Keep the file name in upload fields if there is any validation error">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/225): Keep the file name in upload fields if there is any validation error
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO i18n (`id`, `en`, `fr`) VALUES
("resolve the errors in forms and try to upload %s file for %s", "Resolve the errors in forms and try to upload %s file for %s.", "Résolvez les erreurs dans les formulaires et essayez de télécharger le fichier %s pour %s."),
("resolve the errors in forms and try to replace file for %s", "Resolve the errors in forms and try to replace file for %s.", "Résolvez les erreurs dans les formulaires et essayez de remplacer le fichier pour %s."),
("resolve the errors in forms and try to delete file for %s", "Resolve the errors in forms and try to delete file for %s.", "Résolvez les erreurs dans les formulaires et essayez de supprimer le fichier pour %s.");
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/231): stat failed for upload directory">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/231): stat failed for upload directory
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (`id`, `en`, `fr`) VALUES
("the upload directory does not exist.", "The upload directory does not exist.", "Le répertoire de téléchargement n'existe pas.");
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/296): NewVersionSetUp(): Clean up "Current Volume clean up" section to apply rules defined in the issue">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/296): NewVersionSetUp(): Clean up "Current Volume clean up" section to apply rules defined in the issue
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO i18n (`id`, `en`, `fr`) VALUES
('the current volume of %s aliquot(s) has been updated to initial volume because the current volume was set to null but an initial volume is defined.',
"The current volumes of %s aliquot(s) have been updated by copying the initial volumes because the current volumes were not defined but an initial volume existed.",
"Les volumes courants de %s aliquotes ont été mis à jour par copie des volume initiaux car les volume courrants n'étaient pas définis mais un volume initial existait."),
('the current volume of %s aliquot(s) has been erased because the current volume was set but no initial volume is defined.',
"The current volumes of %s aliquot(s) have been erased because the current volumes were set but no initial volume is defined.",
"Les volumes courants de %s aliquotes ont été effacés car les volumes courrants étaient définis mais aucun volume initial existait."),
('the current volume of %s aliquot(s) has been set to 0.',
'The current volumes of %s aliquot(s) have been set to 0.',
"Les volumes courants de %s aliquotes ont été mis à jour à une valeure egale à 0."),
('the current volume of %s aliquot(s) has been updated to initial volume minus used volumes.',
'The current volumes of %s aliquot(s) have been updated to initial volume minus used volumes.',
"Les volume courants de %s aliquotes ont été mis à jour à une valeure egale au volume initial moins le volume utilisé"),
("a initial volume is set for %s aliquots attached to aliquot type (aliquot_controls) with no volume unit - only aliquot with volume unit can have a volume in atim - please correct",
"An initial volume is set for %s aliquots attached to aliquot type (aliquot_controls) with no volume unit. Only aliquots with volume unit can have a volume in atim. Please correct data into ATiM.",
"Un volume initial est défini pour %s aliquotes attachés à un type d'aliquote (aliquot_controls) sans unité de volume. Seuls les aliquotes avec unité de volume peuvent avoir un volume dans ATiM. Veuillez corriger les données dans ATiM."),
("a used volume is set for %s aliquots attached to aliquot type (aliquot_controls) with no volume unit - only aliquot with volume unit can have a use definition with a volume in atim - please correct",
"A used volume is set for %s aliquots attached to aliquot type (aliquot_controls) with no volume unit. Only aliquots with volume unit can have a use definition with a used volume in atim. Please correct data into ATiM.",
"Un volume utilisé est défini pour %s aliquotes attachés à un type d'aliquote (aliquot_controls) sans unité de volume. Seuls les aliquotes avec unité de volume peuvent avoir une utilisation avec un volume utilisé dans ATiM. Veuillez corriger les données dans ATiM.");
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/244): Add notes to index view when flag_add = '1'">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/244): Add notes to index view when flag_add = '1'
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='participants') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='diagnosismasters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='collection_notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='sample_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='SampleMaster' AND `tablename`='sample_masters' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='aliquot_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='consent_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ConsentMaster' AND `tablename`='consent_masters' AND `field`='reason_denied' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='consent_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ConsentMaster' AND `tablename`='consent_masters' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
-- </editor-fold>


-- <editor-fold desc="Add/correct i18n records">

-- ----------------------------------------------------------------------------------------------------------------------------------
--	i18n clean up
-- ----------------------------------------------------------------------------------------------------------------------------------
REPLACE INTO i18n (id,en,fr)
VALUES
('treatment extend', 'Treatment Precision', 'Précision de traitement'),
('treatment extends', 'Treatment Precisions', 'Précisions de traitement'),
('treatments extends', 'Treatments Precisions', 'Précisions de traitements');

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/244): Add notes to index view when flag_add = '1'">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/244): Add notes to index view when flag_add = '1'
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='participants') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='diagnosismasters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='collection_notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='sample_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='SampleMaster' AND `tablename`='sample_masters' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='aliquot_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='consent_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ConsentMaster' AND `tablename`='consent_masters' AND `field`='reason_denied' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_index`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='consent_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ConsentMaster' AND `tablename`='consent_masters' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/247): Create cfDNA sample as in charm project">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/247): Create cfDNA sample as in charm project"
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'cfdna', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_dnas', 0, 'cfdna');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'plasma'), (SELECT id FROM sample_controls WHERE sample_type = 'cfdna'),0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'urine'), (SELECT id FROM sample_controls WHERE sample_type = 'cfdna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'serum'), (SELECT id FROM sample_controls WHERE sample_type = 'cfdna'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('cfdna', 'cfDNA', 'cfDNA');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cfDNA'), 'tube', '', 'ad_der_tubes_incl_ul_vol_and_conc', 'ad_tubes', 'ul', 0, '', 0, 'cfdna|tube');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/255): Wrong field type (integer instead checkbox) in CAP Report (v2016) - Colon/Rectum (Resection)">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/255): 
--	               Wrong field type (integer instead checkbox) in CAP Report (v2016) - Colon/Rectum (Resection)
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE structure_fields SET  `type`='checkbox' WHERE model='EventDetail' AND tablename='ed_cap_report_16_colon_resections' AND field='path_nstage_nbr_of_lymph_nodes_examined_no_determined' AND `type`='integer_positive' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='checkbox' WHERE model='EventDetail' AND tablename='ed_cap_report_16_colon_resections' AND field='path_nstage_nbr_of_lymph_nodes_involved_no_determined' AND `type`='integer_positive' AND structure_value_domain  IS NULL ;

ALTER TABLE ed_cap_report_16_colon_resections 
  MODIFY COLUMN path_nstage_nbr_of_lymph_nodes_examined_no_determined tinyint(1) DEFAULT '0',
  MODIFY COLUMN path_nstage_nbr_of_lymph_nodes_involved_no_determined tinyint(1) DEFAULT '0';
ALTER TABLE ed_cap_report_16_colon_resections_revs 
  MODIFY COLUMN path_nstage_nbr_of_lymph_nodes_examined_no_determined tinyint(1) DEFAULT '0',
  MODIFY COLUMN path_nstage_nbr_of_lymph_nodes_involved_no_determined tinyint(1) DEFAULT '0';

UPDATE ed_cap_report_16_colon_resections SET path_nstage_nbr_of_lymph_nodes_examined_no_determined = '1' WHERE path_nstage_nbr_of_lymph_nodes_examined_no_determined != '0';
UPDATE ed_cap_report_16_colon_resections_revs SET path_nstage_nbr_of_lymph_nodes_examined_no_determined = '1' WHERE path_nstage_nbr_of_lymph_nodes_examined_no_determined != '0';

UPDATE ed_cap_report_16_colon_resections SET path_nstage_nbr_of_lymph_nodes_involved_no_determined = '1' WHERE path_nstage_nbr_of_lymph_nodes_involved_no_determined != '0';
UPDATE ed_cap_report_16_colon_resections_revs SET path_nstage_nbr_of_lymph_nodes_involved_no_determined = '1' WHERE path_nstage_nbr_of_lymph_nodes_involved_no_determined != '0';
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/329): Remove unique_key constraint on storage_masters.code, use StorageMaster.id as Storage System Code, plus clean up ViewStorageMaster to remove unnecessary fields">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/329): 
--	   Remove unique_key constraint on storage_masters.code, use StorageMaster.id as Storage System Code, plus clean up 
--	   ViewStorageMaster to remove unnecessary fields
-- ----------------------------------------------------------------------------------------------------------------------------------


ALTER TABLE storage_masters DROP INDEX unique_code;

-- storagemasters structure
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('StorageLayout', 'StorageMaster', 'storage_masters', 'id', 'input',  NULL , '0', 'size=30', '', 'storage_code_help', 'storage code', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='storagemasters'), 
(SELECT id FROM structure_fields WHERE `model`='StorageMaster' AND `tablename`='storage_masters' AND `field`='id' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='storage_code_help' AND `language_label`='storage code' AND `language_tag`=''), 
'1', '100', 'system data', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
UPDATE structure_formats SET `flag_edit`='0', `flag_edit_readonly`='0', `flag_search`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='storagemasters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StorageMaster' AND `tablename`='storage_masters' AND `field`='code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- children_storages structure
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='children_storages'), 
(SELECT id FROM structure_fields WHERE `model`='StorageMaster' AND `tablename`='storage_masters' AND `field`='id' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='storage_code_help' AND `language_label`='storage code' AND `language_tag`=''), 
'0', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
UPDATE structure_formats SET `flag_edit`='0', `flag_edit_readonly`='0', `flag_search`='0', `flag_search_readonly`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='children_storages') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StorageMaster' AND `tablename`='storage_masters' AND `field`='code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- report_list_all_storages_criteria_and_result structure
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='report_list_all_storages_criteria_and_result'), 
(SELECT id FROM structure_fields WHERE `model`='StorageMaster' AND `tablename`='storage_masters' AND `field`='id' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='storage_code_help' AND `language_label`='storage code' AND `language_tag`=''), 
'0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
UPDATE structure_formats SET `flag_index`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='report_list_all_storages_criteria_and_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StorageMaster' AND `tablename`='storage_masters' AND `field`='code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- view_storage_masters structure
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('StorageLayout', 'ViewStorageMaster', 'view_storage_masters', 'id', 'input',  NULL , '0', 'size=30', '', 'storage_code_help', 'storage code', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='view_storage_masters'), 
(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='id' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='storage_code_help' AND `language_label`='storage code' AND `language_tag`=''), 
'1', '100', 'system data', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
UPDATE structure_formats SET `flag_search`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_storage_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- report_list_all_storages_criteria_and_result structure
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='report_list_all_storages_criteria_and_result'), 
(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='id' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='storage_code_help' AND `language_label`='storage code' AND `language_tag`=''), 
'0', '103', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
UPDATE structure_formats SET `flag_index`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='report_list_all_storages_criteria_and_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- view_storage_masters_unclassified_list structure
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='view_storage_masters_unclassified_list'), 
(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='id' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='storage_code_help' AND `language_label`='storage code' AND `language_tag`=''), 
'1', '100', 'system data', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
UPDATE structure_formats SET `flag_search`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_storage_masters_unclassified_list') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewStorageMaster' AND `tablename`='view_storage_masters' AND `field`='code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- non_tma_block_storages structure
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('StorageLayout', 'NonTmaBlockStorage', 'view_storage_masters', 'id', 'input',  NULL , '0', 'size=30', '', 'storage_code_help', 'storage code', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='non_tma_block_storages'), 
(SELECT id FROM structure_fields WHERE `model`='NonTmaBlockStorage' AND `tablename`='view_storage_masters' AND `field`='id' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='storage_code_help' AND `language_label`='storage code' AND `language_tag`=''), 
'1', '100', 'system data', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
UPDATE structure_formats SET `flag_search`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='non_tma_block_storages') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='NonTmaBlockStorage' AND `tablename`='view_storage_masters' AND `field`='code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- tma_blocks structure
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('StorageLayout', 'TmaBlock', '', 'id', 'input',  NULL , '0', 'size=30', '', 'storage_code_help', 'storage code', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='tma_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='TmaBlock' AND `tablename`='' AND `field`='id' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='storage_code_help' AND `language_label`='storage code' AND `language_tag`=''), 
'1', '100', 'system data', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
UPDATE structure_formats SET `flag_search`='0', `flag_index`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='tma_blocks') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='TmaBlock' AND `tablename`='' AND `field`='code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- </editor-fold>

-- ----------------------------------------------------------------------------------------------------------------------------------
--	ATiM version
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;
INSERT INTO `versions` (version_number, date_installed, trunk_build_number, branch_build_number)
VALUES
('2.7.4', NOW(),'to complete when install ATiM','to complete when install ATiM');
