-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- Jewish General Hospital : Lymphoma Histological Transformation Diagnosis
-- ................................................................................
-- Histological transformation diagnosis forms developped to capture data of 
-- lymphoma histological transformation for the ATiM of Lymphoma bank at the 
-- Lady Davis Institute (Jewish General Hospital).
-- --------------------------------------------------------------------------------

INSERT INTO `diagnosis_controls` (`id`, `category`, `controls_type`, `flag_active`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`, `flag_compare_with_cap`) 
VALUES
(null, 'progression - locoregional', 'jgh lymphoma - histological transformation', 1, 'jgh_lymphoma_dxd_histological_transformation', 'jgh_lymphoma_dxd_histological_transformations', 0, 'progression - locoregional|histological transformation', 0);

DROP TABLE IF EXISTS `jgh_lymphoma_dxd_histological_transformations`;
CREATE TABLE IF NOT EXISTS `jgh_lymphoma_dxd_histological_transformations` (
  `diagnosis_master_id` int(11) NOT NULL,
  `lymphoma_type` varchar(250) NOT NULL DEFAULT '',
  `type_of_transformation` varchar(20) DEFAULT '',
  `hyper_ca2plus` char(1) DEFAULT '',
  `hyper_ca2plus_value` decimal(7,2) DEFAULT NULL,
  `unusual_site` char(1) DEFAULT '',
  `unusual_site_value` varchar(50) DEFAULT '',
  `ldh_increased_more_than_2xlimit` char(1) DEFAULT '',
  `ldh_value` decimal(7,2) DEFAULT NULL,
  `discordant_nodal_growth` char(1) DEFAULT '',
  `new_b_symptoms` text,
  `path_nbr` varchar(100) DEFAULT '',
  `path_date` date DEFAULT NULL,
  `ecog` decimal(7,2) DEFAULT NULL,
  `ens` decimal(7,2) DEFAULT NULL,
  `nhl_stage_nbr` varchar(6) DEFAULT NULL,
  `nhl_stage_alpha` varchar(6) DEFAULT NULL,
  KEY `diagnosis_master_id` (`diagnosis_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `jgh_lymphoma_dxd_histological_transformations_revs`;
CREATE TABLE IF NOT EXISTS `jgh_lymphoma_dxd_histological_transformations_revs` (
  `diagnosis_master_id` int(11) NOT NULL,
  `lymphoma_type` varchar(250) NOT NULL DEFAULT '',
  `type_of_transformation` varchar(20) DEFAULT '',
  `hyper_ca2plus` char(1) DEFAULT '',
  `hyper_ca2plus_value` decimal(7,2) DEFAULT NULL,
  `unusual_site` char(1) DEFAULT '',
  `unusual_site_value` varchar(50) DEFAULT '',
  `ldh_increased_more_than_2xlimit` char(1) DEFAULT '',
  `ldh_value` decimal(7,2) DEFAULT NULL,
  `discordant_nodal_growth` char(1) DEFAULT '',
  `new_b_symptoms` text,
  `path_nbr` varchar(100) DEFAULT '',
  `path_date` date DEFAULT NULL,
  `ecog` decimal(7,2) DEFAULT NULL,
  `ens` decimal(7,2) DEFAULT NULL,
  `nhl_stage_nbr` varchar(6) DEFAULT NULL,
  `nhl_stage_alpha` varchar(6) DEFAULT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=223 DEFAULT CHARSET=latin1;

ALTER TABLE `jgh_lymphoma_dxd_histological_transformations`
  ADD CONSTRAINT `FK_jgh_lymphoma_dxd_histo_transformations_diagnosis_masters` FOREIGN KEY (`diagnosis_master_id`) REFERENCES `diagnosis_masters` (`id`);

-- INSERT INTO structure_value_domains (domain_name, override, category, source)
-- VALUES
-- ('jgh_lymphoma_lymphoma_type_list', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'JGH Lymphoma: Lymphoma Types\')');
-- INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
-- VALUES
-- ('JGH Lymphoma: Lymphoma Types', 1, 250, 'clinical - diagnosis');
-- SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'JGH Lymphoma: Lymphoma Types');
-- INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
-- VALUES
-- ("Acute promyelocytic leukemi", "Acute promyelocytic leukemi", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Adult T-cell leukemia/lymphoma", "Adult T-cell leukemia/lymphoma", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("ALCL", "Anaplastic large-cell lymphoma", "Lymphome anaplasique à grandes cellules", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("ALL", "Acute Lymphoblastic Leukemia", "Leucémie aiguë lymphoblastique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("AML", "Acute Myelogenous Leukemia", "Leucémie myéloïde aiguë", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Amyloidosis", "Amyloidosis", "Amyloidosis", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Angioimmunoblastic T-cell lymphoma (ATL)", "Angioimmunoblastic T-cell lymphoma", "Lymphadénopathie angio-immunoblastique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Aplastic Anemia", "Aplastic Anemia", "Anémie Aplastique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("B-cell lymphoma, unclassifiable", "B-cell lymphoma, unclassifiable", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("BL", "Burkitt\'s Lymphoma", "Lymphome de Burkitt", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Blastic plasmacytoid dendritic cell neoplasm", "Blastic plasmacytoid dendritic cell neoplasm", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Castleman\'s Disease", "Castleman\'s Disease", "Maladie de Castleman", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("CLL", "Chronic Lymphocytic Leukemia", "Leucémie lymphoblastique chronique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("CLL / SLL", "Chronic lymphocytic leukemia / Small lymphocytic lymphoma", "Leucémie lymphoïde chronique / Lymphome à petits lymphocytes", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("CML", "Chronic Myelogenous Leukemia", "Leucémie myéloïde chronique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("CMML", "Chronic Myelomonocytic Leukemia", "Leucémie myélomonocytaire chronique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Composite Lymphoma", "Composite Lymphoma", "Lymphome composite", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("CTCL", "Cutaneous T-cell lymphoma", "Lymphome cutané à cellules T", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("DLBCL", "Diffuse Large B Cell Lymphoma", "Lymphome diffus à grande cellules B", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Double-hit Diffuse Large B Cell Lymphoma", "Double-hit Diffuse Large B Cell Lymphoma", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Essential Thrombocytosis", "Essential Thrombocytosis", "Thrombocytémie essentielle", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("FL", "Follicular Lymphoma", "Lymphome folliculaire", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Haemophagocytic Lymphohistiocytosis", "Haemophagocytic Lymphohistiocytosis", "Lymphohistiocytose hémophagocytaire", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("HCL", "Hairy Cell Leukemia", "Leucémie à tricholeucocytes", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("HD", "Hodgkin\'s Disease", "Lymphome hodgkinien", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("MALT lymphoma", "MALT lymphoma", "Lymphome du MALT", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("MC", "Mantle Cell Lymphoma", "Lymphome des cellules du manteau", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("MDS", "Myelodysplastic Syndrome", "Syndrome myélodisplastique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("MGUS", "Monoclonal gammopathy of undetermined significance", "Gammapathie monoclonale de signification indéterminée", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("MM", "Multiple Myeloma", "Myélome multiple", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("MZL", "Marginal Zone Lymphoma", "Lymphome de la zone marginale", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("Plasmablastic lymphoma", "Plasmablastic lymphoma", "lymphome plasmablastique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("PMBCL", "Primary mediastinal B-cell lymphoma", "Lymphome médiastinal primitif à grandes cellules B", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("PTCL", "Peripheral T Cell Lymphoma", "PTCL", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("SLL", "Small Lymphocytic Leukemia", "SLL", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("T-cell large granular lymphocyte leukemia", "T-cell large granular lymphocyte leukemia", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("T-lymphoblastic leukemia/lymphoma", "T-lymphoblastic leukemia/lymphoma", "Lymphome ou leucémie lymphoblastique à précurseurs T", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("T-PPL", "T-cell prolymphocytic leukemia", "Leucémie à prolymphocytes T", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
-- ("WD", "Lymphoplasmacytic lymphoma/Waldenström\'s Disease", "Lymphoplasmacytic lymphoma/Maladie de Waldenström", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("jgh_lymphoma_dx_histo_transf_definition_source", "open", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias)
VALUES
("clinical", "clinical"),
("patho", "patho");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) 
VALUES 
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_dx_histo_transf_definition_source"), (SELECT id FROM structure_permissible_values WHERE value="clinical" AND language_alias="clinical"), "1", "1"),
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_dx_histo_transf_definition_source"), (SELECT id FROM structure_permissible_values WHERE value="patho" AND language_alias="patho"), "2", "1");

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('jgh_lymphoma_dx_histo_transf_unusual_site', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'JGH Lymphoma: Histo Transformation Unusual Sites\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('JGH Lymphoma: Histo Transformation Unusual Sites', 1, 50, 'clinical - diagnosis');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'JGH Lymphoma: Histo Transformation Unusual Sites');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("bone", "Bone", "Os", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("liver", "Liver", "Foie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Lungs", "Lungs", "Poumons", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Spleen", "Spleen", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("unusual site cns", "CNS", "CNS", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("unusual site ms", "MS", "MS", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

-- INSERT INTO structure_value_domains (domain_name, override, category, source) 
-- VALUES 
-- ("jgh_lymphoma_nhl_stage_nbr", "open", "", NULL);
-- INSERT IGNORE INTO structure_permissible_values (value, language_alias) 
-- VALUES
-- ("I", "I"),
-- ("II", "II"),
-- ("III", "III"),
-- ("IV", "IV");
-- INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) 
-- VALUES 
-- ((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_nbr"), (SELECT id FROM structure_permissible_values WHERE value="I" AND language_alias="I"), "1", "1"),
-- ((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_nbr"), (SELECT id FROM structure_permissible_values WHERE value="II" AND language_alias="II"), "2", "1"),
-- ((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_nbr"), (SELECT id FROM structure_permissible_values WHERE value="III" AND language_alias="III"), "3", "1"),
-- ((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_nbr"), (SELECT id FROM structure_permissible_values WHERE value="IV" AND language_alias="IV"), "4", "1");

-- INSERT INTO structure_value_domains (domain_name, override, category, source) 
-- VALUES 
-- ("jgh_lymphoma_nhl_stage_alpha", "open", "", NULL);
-- INSERT IGNORE INTO structure_permissible_values (value, language_alias) 
-- VALUES
-- ("A", "A"),
-- ("B", "B"),
-- ("E", "E");
-- INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) 
-- VALUES 
-- ((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_alpha"), (SELECT id FROM structure_permissible_values WHERE value="A" AND language_alias="A"), "1", "1"),
-- ((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_alpha"), (SELECT id FROM structure_permissible_values WHERE value="B" AND language_alias="B"), "2", "1"),
-- ((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_alpha"), (SELECT id FROM structure_permissible_values WHERE value="E" AND language_alias="E"), "3", "1");

INSERT INTO structures(`alias`) VALUES ('jgh_lymphoma_dxd_histological_transformation');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'lymphoma_type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_lymphoma_type_list') , '0', '', '', '', 'lymphoma type', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'type_of_transformation', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_dx_histo_transf_definition_source') , '0', '', '', '', 'type of transformation', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'path_nbr', 'input',  NULL , '0', 'size=20', '', '', 'path nbr', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'path_date', 'date',  NULL , '0', '', '', '', 'path date', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'hyper_ca2plus', 'yes_no',  NULL , '0', '', '', '', 'hyper ca2plus', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'hyper_ca2plus_value', 'float',  NULL , '0', '', '', '', '', 'hyper ca2plus value'), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'unusual_site', 'yes_no',  NULL , '0', '', '', '', 'unusual site', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'unusual_site_value', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_dx_histo_transf_unusual_site') , '0', '', '', '', '', 'unusual site value'), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'ldh_increased_more_than_2xlimit', 'yes_no',  NULL , '0', '', '', '', 'ldh increased more than 2xlimit', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'ldh_value', 'float',  NULL , '0', '', '', '', '', 'ldh value'), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'discordant_nodal_growth', 'yes_no',  NULL , '0', '', '', '', 'discordant nodal growth', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'new_b_symptoms', 'textarea',  NULL , '0', 'rows=3,cols=30', '', '', 'new b symptoms', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'ecog', 'float',  NULL , '0', 'size=5', '', '', 'ecog', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'ens', 'float',  NULL , '0', 'size=5', '', '', 'ens', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'nhl_stage_nbr', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_nbr') , '0', '', '', '', 'nhl stage', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_histological_transformations', 'nhl_stage_alpha', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_alpha') , '0', '', '', '', '', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='lymphoma_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_lymphoma_type_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='lymphoma type' AND `language_tag`=''), 
'1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='type_of_transformation' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_dx_histo_transf_definition_source')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='type of transformation' AND `language_tag`=''), 
'1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='path_nbr' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='path nbr' AND `language_tag`=''), 
'1', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='path_date' AND `type`='date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='path date' AND `language_tag`=''), 
'1', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='hyper_ca2plus' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='hyper ca2plus' AND `language_tag`=''), 
'2', '20', 'values', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='hyper_ca2plus_value' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='hyper ca2plus value'), 
'2', '21', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='unusual_site' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='unusual site' AND `language_tag`=''), 
'2', '22', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='unusual_site_value' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_dx_histo_transf_unusual_site')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='unusual site value'), 
'2', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='ldh_increased_more_than_2xlimit' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='ldh increased more than 2xlimit' AND `language_tag`=''), 
'2', '24', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='ldh_value' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='ldh value'), 
'2', '25', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='discordant_nodal_growth' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='discordant nodal growth' AND `language_tag`=''), 
'2', '26', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='new_b_symptoms' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='rows=3,cols=30' AND `default`='' AND `language_help`='' AND `language_label`='new b symptoms' AND `language_tag`=''), 
'2', '27', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='ecog' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='ecog' AND `language_tag`=''), 
'2', '51', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='ens' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='ens' AND `language_tag`=''), 
'2', '52', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='nhl_stage_nbr' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_nbr')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='nhl stage' AND `language_tag`=''), 
'2', '53', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_histological_transformation'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_histological_transformations' AND `field`='nhl_stage_alpha' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_alpha')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '54', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

REPLACE INTO i18n (id,en,fr)
VALUES 
("jgh lymphoma - histological transformation", "Histological Transformation (JGH Lymphoma)", "Transformation histologique (JGH Lymphome)"),
("discordant nodal growth", "Discordant Nodal Growth", ""),
("ecog", "ECOG", "ECOG"),
("ens", "ENS", "ENS"),
("hyper ca2plus", "Hyper Ca<sup>2+</sup>", ""),
("hyper ca2plus value", "Ca<sup>2+</sup> Value", ""),
("ldh increased more than 2xlimit", "Increased LDH > 2 x Limit", ""),
("ldh value", "LDH Value", ""),
("lymphoma type", "Lymphoma Type", ""),
("new b symptoms", "New B Symptoms", ""),
("nhl stage", "NHL", ""),
("path date", "Path Date", ""),
("path nbr", "Path #", ""),
("type of transformation", "Transformation Defined", ""),
("unusual site", "Unusual Site", ""),
("unusual site value", "Site Value", ""),
("values", "Values", ""),
("clinical", "Clinical", ""),
("patho", "Patho", "");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;