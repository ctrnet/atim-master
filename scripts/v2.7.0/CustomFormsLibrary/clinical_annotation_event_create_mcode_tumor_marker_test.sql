-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- mCode ((Minimal Common Oncology Data Elements) initiative : 
--   Create outcome form based on 'Labs/Vitals' group
--   and 'Tumor Marker Test' profile fields  
--   of the mCODE™ DataDictionnary Version 0.9.1.
-- ................................................................................

--   List based on :
--    - test
--       mCODE™ Value Set Name : TumorMarkerTestVS
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, databrowser_label, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, '', 'lab', 'mcode - tumor marker test', 1, 'mcode_ed_lab_tumor_marker_test', 'mcode_ed_lab_tumor_marker_tests', 0, 'lab|mcode - tumor marker test', 0, 1, 1);

DROP TABLE IF EXISTS mcode_ed_lab_tumor_marker_tests;
CREATE TABLE IF NOT EXISTS mcode_ed_lab_tumor_marker_tests (
  event_master_id int(11) NOT NULL,
  test varchar(50) default NULL,
  pos_neg varchar(50) default NULL,
  value decimal(6,2) DEFAULT NULL,
  value_unit varchar(20) default NULL,
  res_text varchar(30) default NULL,
  KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS mcode_ed_lab_tumor_marker_tests_revs;
CREATE TABLE IF NOT EXISTS mcode_ed_lab_tumor_marker_tests_revs (
  event_master_id int(11) NOT NULL,
  test varchar(50) default NULL,
  pos_neg varchar(50) default NULL,
  value decimal(6,2) DEFAULT NULL,
  value_unit varchar(20) default NULL,
  res_text varchar(30) default NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE mcode_ed_lab_tumor_marker_tests
  ADD CONSTRAINT mcode_ed_lab_tumor_marker_tests_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);

INSERT INTO structures(`alias`) VALUES ('mcode_ed_lab_tumor_marker_test');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_tumor_marker_tests', 'test', 'autocomplete',  NULL , '0', 'size=10,url=/CodingIcd/CodingMCodes/autocomplete/TumorMarkerTestVS,tool=/CodingIcd/CodingMCodes/tool/TumorMarkerTestVS', '', 'help_mcode_tumor_marker_test', 'test', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_ed_lab_tumor_marker_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_tumor_marker_tests' AND `field`='test'), 
'1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_lab_tumor_marker_test'),
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=1', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_validations (structure_field_id, rule, language_message)
VALUES
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_tumor_marker_tests' AND `field`='test' AND `type`='autocomplete'),
'validateMcodeCode,TumorMarkerTestVS', 'invalid mcode code'),
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_tumor_marker_tests' AND `field`='test' AND `type`='autocomplete'),
'notBlank', '');

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('ctrnet_test_units_for_mcode_tumor_marker_test', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CTRNet & mCode : Tumor Marker Test Units\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CTRNet & mCode : Tumor Marker Test Units', 1, 50, 'clinical - event');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CTRNet & mCode : Tumor Marker Test Units');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("IU/ml", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ng/ml", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("pg/ml", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("u/l", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("U/ml", "", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_tumor_marker_tests', 'pos_neg', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_PositiveNegativeVS') , '0', '', '', '', 'positive/negative', ''), 
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_tumor_marker_tests', 'value', 'float',  NULL , '0', 'size=4', '', '', 'value', ''), 
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_tumor_marker_tests', 'value_unit', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_test_units_for_mcode_tumor_marker_test') , '0', '', '', '', '', ''), 
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_lab_tumor_marker_tests', 'res_text', 'input',  NULL , '0', 'size=8', '', '', 'result in text format', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_ed_lab_tumor_marker_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_tumor_marker_tests' AND `field`='pos_neg' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_PositiveNegativeVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='positive/negative' AND `language_tag`=''), 
'1', '10', 'result', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_lab_tumor_marker_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_tumor_marker_tests' AND `field`='value' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=4' AND `default`='' AND `language_help`='' AND `language_label`='value' AND `language_tag`=''), 
'1', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_lab_tumor_marker_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_tumor_marker_tests' AND `field`='value_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_test_units_for_mcode_tumor_marker_test')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'1', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_lab_tumor_marker_test'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_lab_tumor_marker_tests' AND `field`='res_text' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=8' AND `default`='' AND `language_help`='' AND `language_label`='result in text format' AND `language_tag`=''), 
'1', '13', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

UPDATE structure_formats SET `language_heading`='summary' WHERE structure_id=(SELECT id FROM structures WHERE alias='mcode_ed_lab_tumor_marker_test') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('mcode - tumor marker test', 'Tumor Marker (mCODE™)', 'Marqueur de tumeur (mCODE™)'),
('positive/negative', 'Positive/Negative', 'Positif/Négatif'),
('result in text format', 'Value (txt)', 'Valeur (txt)'),
('help_mcode_tumor_marker_test',
"The code representing the tumor marker test as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the TumorMarkerTestVS Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code correspondant au test du marqueur de tumeur tel que défini par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes TumorMarkerTestVS de la version 0.9.1 du dictionnaire de données mCode.");
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;