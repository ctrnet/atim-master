-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- Display fields missing in search and index view
-- ................................................................................
-- For Blood, Tissue, Plasma, PBMC, etc
-- --------------------------------------------------------------------------------

-- Blood

UPDATE structure_formats 
SET `flag_search`='1', `flag_index`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='sd_spe_bloods') 
AND structure_field_id IN (
  SELECT id 
  FROM structure_fields 
  WHERE `field` IN ('collected_tube_nbr','collected_volume','collected_volume_unit')
);

-- Tissue

UPDATE structure_formats 
SET `flag_search`='1', `flag_index`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='sd_spe_tissues') 
AND structure_field_id IN (
  SELECT id 
  FROM structure_fields 
  WHERE `field` IN ('pathology_reception_datetime','tissue_size','tissue_size_unit','tissue_weight','tissue_weight_unit')
);

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;