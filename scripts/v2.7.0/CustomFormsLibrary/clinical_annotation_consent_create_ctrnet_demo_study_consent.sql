-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- CTRNet Demo : 'Study Consent'
-- ................................................................................
-- Let user to create specific consents a participant has to signed to be part of 
-- a study.
-- --------------------------------------------------------------------------------

INSERT INTO `consent_controls` (`id`, `controls_type`, `flag_active`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`, `flag_link_to_study`) 
VALUES
(null, 'study consent', 1, 'consent_masters_study', 'cd_nationals', 0, 'study consent', '1');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
('study consent', 'Sudy Consent', 'Consentement d''étude');

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;