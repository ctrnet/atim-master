-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CTRNet Demo : Hormono Therapy
-- ................................................................................
-- Form developped to capture data of hormonotherapy.
-- --------------------------------------------------------------------------------

INSERT INTO treatment_extend_controls (id, detail_tablename, detail_form_alias, flag_active, `type`, databrowser_label) VALUES
(null, 'ctrnet_demo_txe_hormono_therapies', 'ctrnet_demo_txe_hormono_therapies', 1, 'ctrnet demo - hormonotherapy drug', 'ctrnet demo - hormonotherapy drug');

INSERT INTO treatment_controls (id, tx_method, disease_site, flag_active, detail_tablename, detail_form_alias, display_order, applied_protocol_control_id, extended_data_import_process, databrowser_label, flag_use_for_ccl, treatment_extend_control_id, use_addgrid, use_detail_form_for_index) 
VALUES
(null, 'ctrnet demo - hormonotherapy', '', 1, 'ctrnet_demo_txd_hormono_therapies', 'ctrnet_demo_txd_hormono_therapies', 0, NULL, NULL, 'ctrnet demo - hormonotherapy', 0, (SELECT id FROM treatment_extend_controls WHERE type = 'ctrnet demo - hormonotherapy drug'), 0, 0);

DROP TABLE IF EXISTS ctrnet_demo_txd_hormono_therapies;
CREATE TABLE IF NOT EXISTS ctrnet_demo_txd_hormono_therapies (
  treatment_master_id int(11) NOT NULL,
  KEY tx_master_id (treatment_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS ctrnet_demo_txd_hormono_therapies_revs;
CREATE TABLE IF NOT EXISTS ctrnet_demo_txd_hormono_therapies_revs (
  treatment_master_id int(11) NOT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS ctrnet_demo_txe_hormono_therapies;
CREATE TABLE IF NOT EXISTS ctrnet_demo_txe_hormono_therapies (
  dose varchar(50) DEFAULT NULL,
  method varchar(50) DEFAULT NULL,
  treatment_extend_master_id int(11) NOT NULL,
  KEY FK_ctrnet_demo_txe_hormono_therapies_treatment_extend_masters (treatment_extend_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS ctrnet_demo_txe_hormono_therapies_revs;
CREATE TABLE IF NOT EXISTS ctrnet_demo_txe_hormono_therapies_revs (
  dose varchar(50) DEFAULT NULL,
  method varchar(50) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  treatment_extend_master_id int(11) NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE ctrnet_demo_txd_hormono_therapies
  ADD CONSTRAINT ctrnet_demo_txd_hormono_therapies_ibfk_1 FOREIGN KEY (treatment_master_id) REFERENCES treatment_masters (id);

ALTER TABLE ctrnet_demo_txe_hormono_therapies
  ADD CONSTRAINT FK_ctrnet_demo_txe_hormono_therapies_treatment_extend_masters FOREIGN KEY (treatment_extend_master_id) REFERENCES treatment_extend_masters (id);

INSERT INTO structures(`alias`) VALUES ('ctrnet_demo_txd_hormono_therapies');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ctrnet_demo_txd_hormono_therapies'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='tx_intent' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='intent')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_tx_intent' AND `language_label`='intent' AND `language_tag`=''), 
'1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_txd_hormono_therapies'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='finish_date' AND `type`='date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_finish_date' AND `language_label`='finish date' AND `language_tag`=''), 
'1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_txd_hormono_therapies'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='information_source' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='information_source')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_information_source' AND `language_label`='information source' AND `language_tag`=''), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_txd_hormono_therapies'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='notes' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='rows=3,cols=30' AND `default`='' AND `language_help`='help_notes' AND `language_label`='notes' AND `language_tag`=''), 
'1', '99', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structures(`alias`) VALUES ('ctrnet_demo_txe_hormono_therapies');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'TreatmentExtendDetail', 'ctrnet_demo_txe_hormono_therapies', 'method', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='chemotherapy_method') , '0', '', '', 'help_method', 'method', ''), 
('ClinicalAnnotation', 'TreatmentExtendDetail', 'ctrnet_demo_txe_hormono_therapies', 'dose', 'input',  NULL , '0', 'size=10', '', 'help_dose', 'dose', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ctrnet_demo_txe_hormono_therapies'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_treatment_drug_id' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='url=/Drug/Drugs/autocompleteDrug' AND `default`='' AND `language_help`='' AND `language_label`='drug' AND `language_tag`=''), 
'1', '1', 'drugs', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_txe_hormono_therapies'), 
(SELECT id FROM structure_fields WHERE `model`='Drug' AND `tablename`='drugs' AND `field`='generic_name' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '1', 'drugs', '0', '1', 'drug', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_txe_hormono_therapies'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='ctrnet_demo_txe_hormono_therapies' AND `field`='method' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='chemotherapy_method')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_method' AND `language_label`='method' AND `language_tag`=''), 
'1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '1', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_txe_hormono_therapies'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='ctrnet_demo_txe_hormono_therapies' AND `field`='dose' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='help_dose' AND `language_label`='dose' AND `language_tag`=''), 
'1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '1', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("ctrnet demo - hormonotherapy drug", "Hormonotherapy - Drug (CTRNet Demo)", "Hormono thérapie - Molécule (CTRNet Demo)"),
("ctrnet demo - hormonotherapy", "Hormonotherapy (CTRNet Demo)", "Hormono thérapie (CTRNet Demo)");
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;