-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- mCode ((Minimal Common Oncology Data Elements) initiative : 
--   Create performance status form based on 'Patient' group 
--   'ECOG Performance Status' profile fields of the mCODE™ Data 
--   Dictionnary Version 0.9.1.
-- ................................................................................

--   List based on :
--    - ECOGP
--       mCODE™ Value Set Name : ECOGPerformanceStatusVS
--    - Karnofsky
--       mCODE™ Value Set Name : KarnofskyPerformanceStatusVS
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, databrowser_label, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, '', 'clinical', 'mcode - performance status', 1, 'mcode_ed_clinical_performance_status', 'mcode_ed_clinical_performance_status', 0, 'clinical|mcode - performance status', 0, 1, 1);

DROP TABLE IF EXISTS mcode_ed_clinical_performance_status;
CREATE TABLE IF NOT EXISTS mcode_ed_clinical_performance_status (
  event_master_id int(11) NOT NULL,
  mcode_ecog_performance_status varchar(50) default NULL,
  mcode_karnofsky_performance_status varchar(50) default NULL,
  KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS mcode_ed_clinical_performance_status_revs;
CREATE TABLE IF NOT EXISTS mcode_ed_clinical_performance_status_revs (
  event_master_id int(11) NOT NULL,
  mcode_ecog_performance_status varchar(50) default NULL,
  mcode_karnofsky_performance_status varchar(50) default NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE mcode_ed_clinical_performance_status
  ADD CONSTRAINT mcode_ed_clinical_performance_status_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);

INSERT INTO structures(`alias`) VALUES ('mcode_ed_clinical_performance_status');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_clinical_performance_status', 'mcode_ecog_performance_status', 'autocomplete',  NULL , '0', 'size=10,url=/CodingIcd/CodingMCodes/autocomplete/ECOGPerformanceStatusVS,tool=/CodingIcd/CodingMCodes/tool/ECOGPerformanceStatusVS', '', 'help_mcode_ecog_performance_status', 'ecog performance status', ''), 
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_clinical_performance_status', 'mcode_karnofsky_performance_status', 'autocomplete',  NULL , '0', 'size=10,url=/CodingIcd/CodingMCodes/autocomplete/KarnofskyPerformanceStatusVS,tool=/CodingIcd/CodingMCodes/tool/KarnofskyPerformanceStatusVS', '', 'help_mcode_karnofsky_performance_status', 'karnofsky performance status', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_ed_clinical_performance_status'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_clinical_performance_status' AND `field`='mcode_ecog_performance_status'), 
'1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_clinical_performance_status'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_clinical_performance_status' AND `field`='mcode_karnofsky_performance_status'), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_clinical_performance_status'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '100', 'summary', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=1', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_validations (structure_field_id, rule, language_message)
VALUES
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_clinical_performance_status' AND `field`='mcode_ecog_performance_status' AND `type`='autocomplete'),
'validateMcodeCode,ECOGPerformanceStatusVS', 'invalid mcode code'),
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_clinical_performance_status' AND `field`='mcode_karnofsky_performance_status' AND `type`='autocomplete'),
'validateMcodeCode,KarnofskyPerformanceStatusVS', 'invalid mcode code');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('mcode - performance status', 'Performance Status (mCODE™)', 'Statut de performance (mCODE™)'),

('ecog performance status', 'ECOG', 'ECOG'),
('help_mcode_ecog_performance_status',
"The code representing the ECOG (Eastern Cooperative Oncology Group) Performance Status as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the ECOGPerformanceStatus Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code définissant le 'statut de performance ECOG (Eastern Cooperative Oncology Group)' tel que défini par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes ECOGPerformanceStatus de la version 0.9.1 du dictionnaire de données mCode."),

('karnofsky performance status', 'Karnofsky', 'Karnofsky'),
('help_mcode_karnofsky_performance_status',
"The code representing the karnofsky (Eastern Cooperative Oncology Group) Performance Status as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the KarnofskyPerformanceStatus Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code définissant le 'statut de performance karnofsky (Eastern Cooperative Oncology Group)' tel que défini par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes KarnofskyPerformanceStatus de la version 0.9.1 du dictionnaire de données mCode.");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;