<?php

function writeSyntax($message = ""){
	echo $message;
	echo "\n\nThis script will write all the old user_logs data into a file in current directory and delete them from the user_logs table.\n\n";
	echo "\n\nSyntax: \n\n\tphp ".basename($_SERVER["SCRIPT_FILENAME"])." -d database_name -u username [-p password] -s[server-name]-n: port_number  [-D date] [--delete=YES] \n\n";
	echo "\n-d: The name of the database. \n-u: The username to access to the database.\n-p: The password related to the username.";
	echo "\n-s: The name of the database server (default localhost).\n-n: The port number(default 3306).";
	echo "\n-D: The date until that all logs will be erased(default until 6 month before and format yyyy-mm-dd).\n--delete: Determine if you want to delete the data from table (By default NO).\n\n";
	exit;
}

	$options = getopt("u:p:d:D:s:n:", array("delete:"));
	if ($argc == 1){
		writeSyntax();
	}
	
	if (empty($options['u'])){
		writeSyntax("\nUsername is required");
	}
	
	if (empty($options['d'])){
		writeSyntax("\nDatabase is required");
	}
	
	$database = $options["d"];
	$username = $options["u"];
	$delete = empty($options["delete"])?"NO":$options["delete"];
	$port = empty($options["n"])?"3306":$options["n"];
	$serverName = empty($options["s"])?"localhost":$options["s"];
	$password = empty($options["p"])?"":$options["p"];
	$date = empty($options["D"])?date("Y-m-d", strtotime("-6 months")):$options["D"];
	$filename = "userLogs".time().'.csv';

	
	try {
		$conn = new PDO("mysql:host=$serverName;port=$port;dbname=$database", $username, $password);
		
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$conn->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, false);
		
		$stmt = $conn->prepare("SELECT COUNT(*) Counter FROM user_logs where visited < '$date'"); 
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$counter = (int)$row['Counter'];
		$counter;
		
		$stmt = $conn->prepare("SELECT * FROM user_logs where visited < '$date'"); 
		
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if (!empty($row)){
			file_put_contents($filename, implode(";", array_keys($row))."\n", FILE_APPEND);
		}

		$i=0;
		$data = array();
		$buffer = 10000;
		while(!empty($row)){
			$i++;
			if ($i % $buffer == $buffer -1){
				file_put_contents($filename, implode("\n", $data)."\n", FILE_APPEND);
				$data = array();
			}
			$data[] = implode(";", $row);
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
		}
		file_put_contents($filename, implode("\n", $data)."\n", FILE_APPEND);
		echo "\nThe data has been registered in $filename file.";
		if ($delete == "YES"){
			$stmt = $conn->prepare("DELETE FROM user_logs where visited < '$date'"); 
			$stmt->execute();
			$stmt = $conn->prepare("OPTIMIZE TABLE user_logs"); 
			$stmt->execute();
			echo "\nThe data has been removed from user_logs table";
		}
		
		$conn = null;
		$stmt = null;
		
	}
	catch(PDOException $e){
		echo "Connection failed: " . $e->getMessage();
    }