-- ----------------------------------------------------------------------------------------------------------------------------------
-- ATiM Database Upgrade Script
-- Version: 2.8.0
--
-- For more information:
--    ./app/scripts/v2.8.0/ReadMe.txt
--    http://wiki.atim-software.ca username/pass: ATiM/ATiMWiKi
-- ----------------------------------------------------------------------------------------------------------------------------------

SET @user_id = (SELECT id FROM users WHERE username = 'system');
SET @user_id = (SELECT IFNULL(@user_id, 1));

-- <editor-fold defaultstate="collapsed" desc="ATiM Inventory Use/Event MySQL scripts">

    -- <editor-fold defaultstate="collapsed" desc="Create Control/Master Inventory Action tables">

        -- <editor-fold defaultstate="collapsed" desc="Create the Inventory_Action_Controls table">

CREATE TABLE `inventory_action_controls` (
                                             `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                             `type` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL COMMENT 'The name shown for the user',
                                             `category` varchar(100) COLLATE 'latin1_swedish_ci' NOT NULL COMMENT 'The way for distinguish the different types of inventory actions like Quality Control, Pathology report and Aliquot Internal Use or etc.',
                                             `apply_on` set('aliquots','aliquots related to samples','samples','samples and all related aliquots','samples and some related aliquots','samples and aliquots not necessarily related') NOT NULL COMMENT 'Define if test applicable on the samples or aliquots',
                                             `sample_control_ids` longtext NULL,
                                             `aliquot_control_ids` longtext NULL,
                                             `s_ids` longtext NULL COMMENT 'This is an auto-generate column save all sample control ids related to each inventory action control',
                                             `a_ids` longtext NULL COMMENT 'This is an auto-generate column save all aliquot control ids related to each inventory action control',
                                             `detail_form_alias` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '',
                                             `detail_tablename` varchar(255) COLLATE 'latin2_bin' NOT NULL,
                                             `display_order` int(11) NOT NULL DEFAULT '0',
                                             `databrowser_label` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '',
                                             `flag_active` tinyint(1) NOT NULL DEFAULT '1',
                                             `flag_link_to_study` tinyint(1) NOT NULL DEFAULT '0',
                                             `flag_link_to_storage` tinyint(1) NOT NULL DEFAULT '0',
                                             `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
                                             `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
                                             `flag_active_input` tinyint(1) NOT NULL DEFAULT '1',
                                             `flag_batch_action` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';

        -- </editor-fold>

        -- <editor-fold defaultstate="collapsed" desc="Create the Inventory_Action_Masters table">

CREATE TABLE `inventory_action_masters` (
                                            `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                            `old_id` varchar(25) NULL,
                                            `inventory_action_control_id` int(11) NOT NULL,
                                            `sample_master_id` int(11) NOT NULL,
                                            `aliquot_master_id` int(11) NULL,
                                            `initial_specimen_sample_id` int(11) NOT NULL,
                                            `title` varchar(100) DEFAULT NULL,
                                            `type` varchar(100) DEFAULT NULL,
                                            `study_summary_id` int(11) NULL,
                                            `person` varchar(50) COLLATE 'latin1_swedish_ci' NULL,
                                            `date` datetime DEFAULT NULL,
                                            `date_accuracy` char(1) NOT NULL DEFAULT '',
                                            `used_volume` decimal(10,5) DEFAULT NULL,
                                            `notes` text,
                                            `created` datetime NULL,
                                            `created_by` int(11) NOT NULL,
                                            `modified` datetime NULL,
                                            `modified_by` int(11) NOT NULL,
                                            `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
                                            FOREIGN KEY (`inventory_action_control_id`) REFERENCES `inventory_action_controls` (`id`) ON DELETE NO ACTION,
                                            FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`) ON DELETE NO ACTION,
                                            FOREIGN KEY (`aliquot_master_id`) REFERENCES `aliquot_masters` (`id`) ON DELETE NO ACTION,
                                            FOREIGN KEY (`initial_specimen_sample_id`) REFERENCES `sample_masters` (`id`) ON DELETE NO ACTION,
                                            FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION,
                                            FOREIGN KEY (`study_summary_id`) REFERENCES `study_summaries` (`id`) ON DELETE NO ACTION,
                                            FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION,
                                            INDEX `inventory_action_master_title` (`title`),
                                            INDEX `inventory_action_master_person` (`person`),
                                            INDEX `inventory_action_master_old_id` (`person`)
) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';

DROP TABLE IF EXISTS `inventory_action_masters_revs`;
CREATE TABLE IF NOT EXISTS `inventory_action_masters_revs` (
    `id` int(11) NOT NULL,
    `old_id` varchar(25) NULL,
    `inventory_action_control_id` int(11) NOT NULL,
    `sample_master_id` int(11) NOT NULL,
    `aliquot_master_id` int(11) NULL,
    `initial_specimen_sample_id` int(11) NOT NULL,
    `title` varchar(100) DEFAULT NULL,
    `type` varchar(100) DEFAULT NULL,
    `study_summary_id` int(11) NULL,
    `person` varchar(50) COLLATE 'latin1_swedish_ci' NULL,
    `date` datetime DEFAULT NULL,
    `date_accuracy` char(1) NOT NULL DEFAULT '',
    `used_volume` decimal(10,5) DEFAULT NULL,
    `notes` text,
    `modified_by` int(10) UNSIGNED NOT NULL,
    `version_id` int(11) NOT NULL AUTO_INCREMENT,
    `version_created` datetime NOT NULL,
    PRIMARY KEY (`version_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

        -- </editor-fold>

    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Initializing the Inventory Action Control table">

-- INSERT INTO `inventory_action_controls` (`type`, `category`, `apply_on`, `sample_control_ids`, `aliquot_control_ids`, `s_ids`, `a_ids`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`, `flag_active`, `flag_link_to_study`, `flag_link_to_storage`, `flag_test_mode`, `flag_form_builder`, `flag_active_input`) VALUES
-- ('aliquot_internal_uses', 'aliquot uses and events	', 'aliquots', NULL, 'all', NULL, NULL, 'action_aliquotinternaluses', 'ac_aliquot_internal_uses', '0', 'aliquot uses and events', '1', '1', '1', '0', '0', '1'),
-- ('quality control', 'quality control', 'samples and aliquots not necessarily related', 'all', 'all', NULL, NULL, 'action_qualityctrls', 'ac_quality_ctrls', '0', 'quality controls', '1', '0', '0', '0', '0', '1');

    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Define the master Structures">

-- <editor-fold defaultstate="collapsed" desc="Create the inventory_action_masters structure">

INSERT INTO structures(`alias`) VALUES ('inventory_action_masters');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'Generated', '', 'parent_info', 'hidden',  NULL , '0', '', '', '', 'parent information', ''),
    ('Core', 'FunctionManagement', '', 'aliquot_master_name', 'input',  NULL , '0', '', '', '', 'aliquot_master_name_for_inventory_action', ''),
    ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'date', 'datetime',  NULL , '0', '', '', 'the date that the inventory action done on aliquot/sample', 'Date', ''),
    ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'notes', 'textarea',  NULL , '0', '', '', '', 'notes', ''),
    ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'created', 'datetime',  NULL , '0', '', '', 'help_created', 'created (into the system)', ''),
    ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'inventory_action_control_id', 'hidden',  NULL , '0', '', '', '', 'inventory action control id', ''),
    ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'sample_master_id', 'hidden',  NULL , '0', '', '', '', 'sample master id', ''),
    ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'aliquot_master_id', 'hidden',  NULL , '0', '', '', '', 'aliquot master id', ''),
    ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'initial_specimen_sample_id', 'hidden',  NULL , '0', '', '', '', 'initial specimen sample id', ''),
    ('Study', 'InventoryActionMaster', 'inventory_action_masters', 'study_summary_id', 'hidden',  NULL , '0', '', '', '', 'study summary id', '');
-- ('Core', 'FunctionManagement', '', 'inventory_action_control_name', 'input',  NULL , '0', '', '', '', 'inventory action control name', '');
-- ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'person', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_staff') , '0', '', '', '', 'done by', ''),
-- ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'title', 'input',  NULL , '0', '', '', 'number or identifier assigned to your inventory action', 'inventory action title', '');


INSERT IGNORE INTO i18n (id,en,fr) VALUE ('aliquot_master_name_for_inventory_action', "Aliquot (if applicable)", "Aliquote (si applicable)");

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='Generated' AND `tablename`='' AND `field`='parent_info' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='parent information' AND `language_tag`=''),
     '0', '1', '', '0', '0', '', '0', '', '0',  '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='aliquot_master_name' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='aliquot_master_name_for_inventory_action' AND `language_tag`=''),
     '0', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='date' AND `type`='datetime' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='the date that the inventory action done on aliquot/sample' AND `language_label`='Date' AND `language_tag`=''),
     '0', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='notes' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='notes' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='inventory_action_control_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='inventory action control id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='sample_master_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='sample master id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='aliquot_master_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='aliquot master id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='initial_specimen_sample_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='initial specimen sample id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='study_summary_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='study summary id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='CopyCtrl' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='copy control' AND `language_tag`=''),
     '100', '10000', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'person', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_staff') , '0', '', '', '', 'inventory_action_person_in_charge', ''),
    ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'title', 'input',  NULL , '0', '', '-', '', 'inventory_action_title', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='person' AND `type`='select'),
     '0', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='title' AND `type`='input'),
     '0', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '0', '0');

INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES
    ((SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='title' AND `type`='input'), 'notBlank', NULL);

UPDATE structure_formats SET `flag_summary`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='inventory_action_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='inventory_action_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- NL INSERT INTO structure_value_domains (domain_name, override, category, source)
-- NL VALUES
-- NL ('inventory_action_categories', 'open', '', NULL);

-- NL INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
-- NL VALUES
-- NL ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'type', 'input',  NULL , '0', '', '', '', 'inventory action type', ''),
-- NL ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'category', 'select',  (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_categories') , '0', '', '', '', 'inventory action category', '');

-- NL INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
-- NL VALUES
-- NL ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
-- NL (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='type' AND `type`='input'),
-- NL '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '0', '0'),
-- NL ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
-- NL (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='category' AND `type`='select'),
-- NL '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '0', '0');

    -- </editor-fold>

-- <editor-fold defaultstate="collapsed" desc="Create the inventory_action_study Structure">

INSERT INTO structures(`alias`) VALUES ('inventory_action_study');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_study'),
     (SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '0', '5', '', '0', '1', 'study / project', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_study'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_aliquot_internal_use_study_summary_id' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='url=/Study/StudySummaries/autocompleteStudy' AND `default`='' AND `language_help`='' AND `language_label`='study / project' AND `language_tag`=''),
     '0', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0');

    -- </editor-fold>

-- <editor-fold defaultstate="collapsed" desc="Create the inventory_action_volume structure">

INSERT INTO structures(`alias`) VALUES ('inventory_action_volume');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'used_volume', 'float_positive',  NULL , '0', 'size=5', '', '', 'aliquot used volume', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_volume'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='used_volume' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='aliquot used volume' AND `language_tag`=''),
     '0', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_volume'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''),
     '0', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '1', '1', '1', '0', '0', '1', '1', '1', '1', '0', '0', '1', '1', '1', '0');

    -- </editor-fold>

    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Create the inventory action 'apply on' list">

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES ("inventory_action_apply_on_types", "", "", NULL);
INSERT IGNORE INTO structure_permissible_values
(value, language_alias)
VALUES
("samples", "inv. action categ. : samples"),
("samples and all related aliquots", "inv. action categ. : samples and all related aliquots"),
("samples and some related aliquots", "inv. action categ. : samples and some related aliquots"),
("samples and aliquots not necessarily related", "inv. action categ. : samples and aliquots not necessarily related"),
("aliquots", "inv. action categ. : aliquots"),
("aliquots related to samples", "inv. action categ. : aliquots related to samples");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active)
VALUES
    ((SELECT id FROM structure_value_domains WHERE domain_name="inventory_action_apply_on_types"),
     (SELECT id FROM structure_permissible_values WHERE value= "samples" AND language_alias= "inv. action categ. : samples"), "0", "1"),
    ((SELECT id FROM structure_value_domains WHERE domain_name="inventory_action_apply_on_types"),
     (SELECT id FROM structure_permissible_values WHERE value= "samples and all related aliquots" AND language_alias= "inv. action categ. : samples and all related aliquots"), "1", "1"),
    ((SELECT id FROM structure_value_domains WHERE domain_name="inventory_action_apply_on_types"),
     (SELECT id FROM structure_permissible_values WHERE value= "samples and some related aliquots" AND language_alias= "inv. action categ. : samples and some related aliquots"), "2", "1"),
    ((SELECT id FROM structure_value_domains WHERE domain_name="inventory_action_apply_on_types"),
     (SELECT id FROM structure_permissible_values WHERE value= "samples and aliquots not necessarily related" AND language_alias= "inv. action categ. : samples and aliquots not necessarily related"), "3", "1"),
    ((SELECT id FROM structure_value_domains WHERE domain_name="inventory_action_apply_on_types"),
     (SELECT id FROM structure_permissible_values WHERE value= "aliquots" AND language_alias="inv. action categ. : aliquots" ), "4", "1"),
    ((SELECT id FROM structure_value_domains WHERE domain_name="inventory_action_apply_on_types"),
     (SELECT id FROM structure_permissible_values WHERE value= "aliquots related to samples" AND language_alias= "inv. action categ. : aliquots related to samples"), "5", "1");

INSERT INTO i18n (`id`, `en`, `fr`)
VALUES
    ("inv. action categ. : samples", "Selected samples types only", "Types d'échantillons sélectionnées uniquement"),
    ("inv. action categ. : samples and all related aliquots", "Selected samples types or all their related aliquots types", "Types d'échantillons sélectionnés ou tous leurs types d'aliquotes associés"),
    ("inv. action categ. : samples and some related aliquots", "Selected samples types or some of their related aliquots types", "Types d'échantillons sélectionnés ou certains de leurs types d'aliquotes associés"),
    ("inv. action categ. : samples and aliquots not necessarily related", "Selected samples types ou selected aliquots types (types not necessarily related)", "Types d'échantillons sélectionnés ou types d'aliquotes sélectionnés (types non nécessairement associés)"),
    ("inv. action categ. : aliquots", "Selected aliquots types only", "Types d'aliquotes sélectionnés seulement"),
    ("inv. action categ. : aliquots related to samples", "Only to all the aliquots types related to the selected samples types", "Uniquement à tous les types d'aliquotes associés aux types d'échantillons sélectionnés");

-- INSERT INTO i18n (`id`, `en`, `fr`)
-- VALUES
-- created from
--   ("inv. action categ. : apply on help message", "Apply on help message", "Apply on help message"),


    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Define the detail Structures">

--   -- <editor-fold defaultstate="collapsed" desc="Create the action_qualityctrls structure">
--
-- INSERT INTO structures(`alias`) VALUES ('action_qualityctrls');
--
-- INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
-- VALUES
-- ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='quality_control_type') , '0', '', '', '', 'qc type', ''),
-- ('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'qc_type_precision', 'input',  NULL , '0', '', '', '', '', 'qc type precision'),
-- ('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'tool', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_qc_tool') , '0', '', '', '', 'qc tool', ''),
-- ('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'score', 'input',  NULL , '0', 'size=5', '', '', 'qc score', ''),
-- ('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'unit', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='quality_control_unit') , '0', '', '', '', '', ''),
-- ('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'conclusion', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='quality_control_conclusion') , '0', '', '', '', 'qc conclusion', ''),
-- ('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'qc_code', 'input',  NULL , '0', 'size=10', '', 'qc_code_help', 'qc code', '');
--
-- INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
-- VALUES
-- ((SELECT id FROM structures WHERE alias='action_qualityctrls'),
-- (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='quality_control_type')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='qc type' AND `language_tag`=''),
-- '1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
-- ((SELECT id FROM structures WHERE alias='action_qualityctrls'),
-- (SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='qc_type_precision' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='qc type precision'),
-- '1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
-- ((SELECT id FROM structures WHERE alias='action_qualityctrls'),
-- (SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='tool' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_qc_tool')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='qc tool' AND `language_tag`=''),
-- '1', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
-- ((SELECT id FROM structures WHERE alias='action_qualityctrls'),
-- (SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='score' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='qc score' AND `language_tag`=''),
-- '1', '21', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
-- ((SELECT id FROM structures WHERE alias='action_qualityctrls'),
-- (SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='quality_control_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''),
-- '1', '22', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
-- ((SELECT id FROM structures WHERE alias='action_qualityctrls'),
-- (SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='conclusion' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='quality_control_conclusion')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='qc conclusion' AND `language_tag`=''),
-- '1', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
-- ((SELECT id FROM structures WHERE alias='action_qualityctrls'),
-- (SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='qc_code' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='qc_code_help' AND `language_label`='qc code' AND `language_tag`=''),
-- '1', '100', 'system data', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
--
--       -- </editor-fold>
--
--   -- <editor-fold defaultstate="collapsed" desc="Create the aliquot internal user structure">
--
-- INSERT INTO structures(`alias`) VALUES ('action_aliquotinternaluses');
--
-- INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
-- VALUES
-- ('InventoryManagement', 'InventoryActionMaster', 'inventory_action_masters', 'type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='aliquot_internal_use_type') , '0', '', '', '', 'type', ''),
-- ('InventoryManagement', 'InventoryActionDetail', 'ac_aliquot_internal_uses', 'duration', 'integer_positive',  NULL , '0', 'size=6', '', '', 'duration', ''),
-- ('InventoryManagement', 'InventoryActionDetail', 'ac_aliquot_internal_uses', 'duration_unit', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='duration_unit') , '0', '', '', '', '', '');
--
-- INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
-- VALUES
-- ((SELECT id FROM structures WHERE alias='action_aliquotinternaluses'),
-- (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_internal_use_type')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='type' AND `language_tag`=''),
-- '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
-- ((SELECT id FROM structures WHERE alias='action_aliquotinternaluses'),
-- (SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_aliquot_internal_uses' AND `field`='duration' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=6' AND `default`='' AND `language_help`='' AND `language_label`='duration' AND `language_tag`=''),
-- '1', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
-- ((SELECT id FROM structures WHERE alias='action_aliquotinternaluses'),
-- (SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_aliquot_internal_uses' AND `field`='duration_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='duration_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''),
-- '1', '13', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');
--
--
-- INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES
-- ((SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_internal_use_type')), 'notBlank', NULL);
--
--       -- </editor-fold>

-- <editor-fold defaultstate="collapsed" desc="Create the intermediate Batch inventory action page">

INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('inventory_action_control_common_list', '', '', 'InventoryManagement.InventoryActionControl::listCommonInventoryAction');


INSERT INTO structures(`alias`) VALUES ('inventory_action_selection_type');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('', '0', '', 'aliquotSampleIds', 'hidden',  NULL , '0', '', '', '', '', ''),
    ('', '0', '', 'inventory_action_control_id', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_control_common_list') , '0', '', '', '', 'select inventory action type', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_selection_type'),
     (SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_control_common_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='select inventory action type' AND `language_tag`=''),
     '0', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_selection_type'),
     (SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquotSampleIds' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''),
     '0', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0');


INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES
    ((SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_control_common_list')), 'notBlank', NULL);

    -- </editor-fold>

    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Create Detail Tables">

-- <editor-fold defaultstate="collapsed" desc="Create Internal Aliquot Use Table">

-- CREATE TABLE `ac_aliquot_internal_uses` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `inventory_action_master_id` int(11) NOT NULL,
--   `duration` int(6) DEFAULT NULL,
--   `duration_unit` varchar(25) DEFAULT NULL,
--   PRIMARY KEY (`id`),
--   KEY `FK_ac_aliquot_internal_uses_inventory_action_master_id` (`inventory_action_master_id`),
--   CONSTRAINT `FK_ac_aliquot_internal_uses_inventory_action_master_id` FOREIGN KEY (`inventory_action_master_id`) REFERENCES `inventory_action_masters` (`id`)
--
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    -- </editor-fold>

-- <editor-fold defaultstate="collapsed" desc="Create Quality Control Table">
-- CREATE TABLE `ac_quality_ctrls` (
--   `id` int(11) NOT NULL AUTO_INCREMENT,
--   `inventory_action_master_id` int(11) NOT NULL,
--   `qc_code` varchar(20) DEFAULT NULL,
--   `qc_type_precision` varchar(250) DEFAULT NULL,
--   `tool` varchar(30) DEFAULT NULL,
--   `score` varchar(30) DEFAULT NULL,
--   `unit` varchar(30) DEFAULT NULL,
--   `conclusion` varchar(30) DEFAULT NULL,
--   PRIMARY KEY (`id`),
--   UNIQUE KEY `unique_qc_code` (`qc_code`),
--   KEY `FK_ac_quality_ctrls_inventory_action_master_id` (`inventory_action_master_id`)
-- ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    -- </editor-fold>

    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Add Inventory Action to Data Browser">

INSERT INTO `datamart_structures` (`plugin`, `model`, `structure_id`, `adv_search_structure_alias`, `display_name`, `control_master_model`, `index_link`, `batch_edit_link`)
VALUES ('InventoryManagement', 'InventoryActionMaster', (SELECT id FROM `structures` WHERE alias = 'inventory_action_masters'), NULL, 'inventory action', 'InventoryActionMaster', '/InventoryManagement/InventoryActionMasters/detail/%%InventoryActionMaster.id%%/', '');

INSERT INTO `datamart_browsing_controls` (`id1`, `id2`, `flag_active_1_to_2`, `flag_active_2_to_1`, `use_field`) VALUES
                                                                                                                     ((SELECT id FROM `datamart_structures` where model = 'InventoryActionMaster' and plugin = 'InventoryManagement'), (SELECT id FROM `datamart_structures` where model = 'ViewSample' and plugin = 'InventoryManagement'), '1', '1', 'sample_master_id'),
                                                                                                                     ((SELECT id FROM `datamart_structures` where model = 'InventoryActionMaster' and plugin = 'InventoryManagement'), (SELECT id FROM `datamart_structures` where model = 'ViewAliquot' and plugin = 'InventoryManagement'), '1', '1', 'aliquot_master_id');

INSERT INTO `datamart_structure_functions` (`datamart_structure_id`, `label`, `link`, `flag_active`, `ref_single_fct_link`) VALUES
       ((SELECT `id` FROM `datamart_structures` WHERE `model` = 'ViewAliquot'), 'create inventory action (aliquot specific)', '/InventoryManagement/InventoryActionMasters/selectActionTypePage/', '1', ''),
       ((SELECT `id` FROM `datamart_structures` WHERE `model` = 'ViewSample'), 'create inventory action (sample specific)', '/InventoryManagement/InventoryActionMasters/selectActionTypePage/', '1', '');


INSERT INTO `datamart_browsing_controls` (`id1`, `id2`, `flag_active_1_to_2`, `flag_active_2_to_1`, `use_field`) VALUES
    ((SELECT id FROM `datamart_structures` where model = 'InventoryActionMaster' and plugin = 'InventoryManagement'), (SELECT id FROM `datamart_structures` where model = 'StudySummary ' and plugin = 'Study'), '1', '1', 'study_summary_id');

    -- </editor-fold>

    -- <editor-fold desc="Disable AIU, QC and Pathology review in databrowser">
UPDATE datamart_browsing_controls set flag_active_1_to_2 = 0, flag_active_2_to_1 = 0 WHERE (id1 = (SELECT id FROM `datamart_structures` where model = 'ViewAliquotUse' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'ViewAliquot' and plugin = 'InventoryManagement')) OR (id1 = (SELECT id FROM `datamart_structures` where model = 'ViewAliquot' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'ViewAliquotUse' and plugin = 'InventoryManagement'));
UPDATE datamart_browsing_controls set flag_active_1_to_2 = 0, flag_active_2_to_1 = 0 WHERE (id1 = (SELECT id FROM `datamart_structures` where model = 'ViewAliquotUse' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'StudySummary' and plugin = 'Study')) OR (id1 = (SELECT id FROM `datamart_structures` where model = 'StudySummary' and plugin = 'Study') AND id2 =(SELECT id FROM `datamart_structures` where model = 'ViewAliquotUse' and plugin = 'InventoryManagement'));
UPDATE datamart_browsing_controls set flag_active_1_to_2 = 0, flag_active_2_to_1 = 0 WHERE (id1 = (SELECT id FROM `datamart_structures` where model = 'AliquotReviewMaster' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'ViewAliquot' and plugin = 'InventoryManagement')) OR (id1 = (SELECT id FROM `datamart_structures` where model = 'ViewAliquot' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'AliquotReviewMaster' and plugin = 'InventoryManagement'));
UPDATE datamart_browsing_controls set flag_active_1_to_2 = 0, flag_active_2_to_1 = 0 WHERE (id1 = (SELECT id FROM `datamart_structures` where model = 'AliquotReviewMaster' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'SpecimenReviewMaster' and plugin = 'InventoryManagement')) OR (id1 = (SELECT id FROM `datamart_structures` where model = 'SpecimenReviewMaster' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'AliquotReviewMaster' and plugin = 'InventoryManagement'));
UPDATE datamart_browsing_controls set flag_active_1_to_2 = 0, flag_active_2_to_1 = 0 WHERE (id1 = (SELECT id FROM `datamart_structures` where model = 'SpecimenReviewMaster' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'ViewSample' and plugin = 'InventoryManagement')) OR (id1 = (SELECT id FROM `datamart_structures` where model = 'ViewSample' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'SpecimenReviewMaster' and plugin = 'InventoryManagement'));
UPDATE datamart_browsing_controls set flag_active_1_to_2 = 0, flag_active_2_to_1 = 0 WHERE (id1 = (SELECT id FROM `datamart_structures` where model = 'QualityCtrl' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'ViewAliquot' and plugin = 'InventoryManagement')) OR (id1 = (SELECT id FROM `datamart_structures` where model = 'ViewAliquot' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'QualityCtrl' and plugin = 'InventoryManagement'));
UPDATE datamart_browsing_controls set flag_active_1_to_2 = 0, flag_active_2_to_1 = 0 WHERE (id1 = (SELECT id FROM `datamart_structures` where model = 'QualityCtrl' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'ViewSample' and plugin = 'InventoryManagement')) OR (id1 = (SELECT id FROM `datamart_structures` where model = 'ViewSample' and plugin = 'InventoryManagement') AND id2 =(SELECT id FROM `datamart_structures` where model = 'QualityCtrl' and plugin = 'InventoryManagement'));
    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Some Dictionary modifications">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Add some words in dictionary (i18n) for the actions' tables
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO i18n (id, en, fr) values
('realiquotings, derivatives creations and orders', "Realiquotings, Derivatives and Orders", "Réaliquotages, Dérivés et Commandes"),
('aliquot_master_name_for_inventory_action', 'Aliquot (if applicable)', 'Aliquot (si applicable)'),
('add inventory actions', "Add Events/Annotations ", "Céer événements/annotations"),
('inventory action data exists for the deleted aliquot',
"Your data cannot be deleted! Inventory events/annotations exist for the deleted aliquot.",
"Vos données ne peuvent être supprimées! Des événements/annotations d'inventaire existent pour votre aliquote."),
('inventory action exists for the deleted sample',
"Your data cannot be deleted! Inventory events/annotations exist for the deleted sample.",
"Vos données ne peuvent être supprimées! Des événements/annotations d'inventaire existent pour votre échantillon."),
('aliquot inventory actions', 'Aliquot Events/Annotations', 'Événements/annotations d''aliquots'),
('inventory_action_person_in_charge', "Executed/Created By", "Réalisé/Créé par"),
('inventory_action_title', "Title", "Titre"),
('there are some problem in inventory_action_controls table (id = %s, column = %s)', "There are some problems in inventory_action_controls table (id = %s, column = %s)", "Il y a quelques problèmes dans la table inventory_action_controls (id =%s, colonne =% )"),
('date that the action done on aliquot/sample', "The date that the inventory event/annotation happened on Aliquot/Sample", "La date à laquelle l'utilisations/événement d'inventaire s'est produite sur l'aliquote/l'échantillon"),
('parent information', "Aliquot/Sample info", "L'information de Aliquote/Échantillon"),
('aliquot', "Aliquot", "Aliquote"),
('aliquot_internal_uses', "Aliquots Internal Use", "Utilisation interne d'aliquotes"),
('inventory action lists', "Inventory Events/Annotations' List", "Liste des événements/annotations d'inventaire"),
('inventory action control name', "Inventory Event/Annotation Type", "Type d'événement/annotation d'inventaire"),
('error in parent aliquot current volume', "The current volume for the parent aliquot is not correct", "Le volume actuel de l'aliquote parent n'est pas correct"),
('the used volume is more than the current aliquot volume', "The total of used volume is more than the current aliquot volume", "Le total du volume utilisé est supérieur au volume d'aliquote actuel"),
('create inventory action (aliquot specific)', "Create Event/Annotation (Aliquot specific)", "Créer un(e) événement/annotation (spécifique à l'aliquote)"),
('create inventory action (sample specific)', "Create Event/Annotation (Sample specific)", "Créer un(e) événement/annotation (spécifique à l'échantillon)"),
('inventory action type selection', "Inventory Event/Annotation Type Selection", "Sélection du type d'événement/annotation d'inventaire"),
('select inventory action type', "Select inventory event/annotation type", "Sélectionnez le type d'événement/annotation d'inventaire"),
('you must select an inventory action type', "You must select an inventory event/annotation type!", "Vous devez sélectionner un type d'événement/annotation d'inventaire!"),
('you must select an inventory action type', "You must select an inventory event/annotation type!", "Vous devez sélectionner un type d'événement/annotation d'inventaire!"),
('there is no selected %s', "There is no selected %s", "Il n'y a pas de %s sélectionné"),
('number or identifier assigned to your action', "Number or identifier assigned to your action", "Numéro ou identifiant attribué à votre action"),
('inventory action title', "Inventory Event/Annotation Title", "Titre de l'événement/annotation d'inventaire"),
('done by', "Done by", "Fait par"),
('this control has been removed and can not be usable anymore.', "This control has been replaced by 'Inventory Action (Event/Annotation) Master' and can not be usable anymore.",
"Ce contrôle a été remplacé par ''Inventory Action (Event/Annotation) Master'' et ne peut plus être utilisé."),
('link', "Link", "Lien"),
('add action process', "Select the related inventory event/annotation from the list", "Sélectionnez l'événement/annotation d'inventaire associé(e) dans la liste"),
('you must select an action type', "You need to select an inventory event/annotation type", "Vous devez sélectionner un type d'événement/annotation d'inventaire"),
('inventory action', "Inventory Event/Annotation", "Événement/annotation d'inventaire"),
('inventory actions', "Inventory Events/Annotations ", "Événements/annotations d'inventaire"),
("the date that the inventory use/event done on aliquot/sample", "The date that the inventory event/annotation done on aliquot/sample", "La date à laquelle l'action d'inventaire a été effectuée sur l'aliquote/l'échantillon"),
('no uses/events history exists', "No Events/Annotations .", "Aucune événement/annotation."),
('sample', "Sample", "Échantillon");


-- ('', "", ""),

-- ----------------------------------------------------------------------------------------------------------------------------------
--	Modify the CTRNet abbreviation phrase and also the footer links texts
-- ----------------------------------------------------------------------------------------------------------------------------------
REPLACE INTO i18n (id, en, fr) values
("about_body", "The Canadian Tissue Repository Network (CTRNet) is a translational cancer research resource, funded by Canadian Institutes of Health Research, that furthers Canadian health research by linking cancer researchers with provincial tumour banks.", "Le réseau Canadien de Banques de Tissues (RCBT) est une ressource en recherche translationnelle en cancer, subventionnée par les Instituts de recherche en santé du Canada, qui aide la recherche en santé au Canada en reliant entre eux les chercheurs en onc"),
("core_ctrnet", "Canadian Tissue Repository Network", "Réseau Canadien de Banque de Tissues"),
("credits_body", "ATiM is an open-source project development by leading tissue banks across Canada. For more information on our development team, questions, comments or suggestions please visit our websites at <a href = 'http://www.ctrnet.ca' target='_blank'>CTRNet</a> and <a href = 'http://www.atim-software.ca' target='_blank'>ATiM</a>", "ATiM est un logiciel développé par les plus importantes banques de tissus à travers le Canada. Pour de plus amples informations sur notre équipe de développement, des questions, commentaires ou suggestions, veuillez consulter nos sites web à <a href = 'http://www.ctrnet.ca' target='_blank'>CTRNet</a> et <a href = 'http://www.atim-software.ca' target='_blank'>ATiM</a>");
    -- </editor-fold>

    -- <editor-fold desc="Disable the Path Review and Quality Control on the Menus">
UPDATE menus SET flag_active=false WHERE
    (id = 'inv_CAN_224' AND language_title = 'quality controls') OR
    (id = 'inv_CAN_2241' AND language_title = 'details') OR
    (id = 'inv_CAN_2224' AND language_title = 'quality controls') OR
    (id = 'inv_CAN_22241' AND language_title = 'details') OR
    (id = 'inv_CAN_225' AND language_title = 'specimen review');
    -- </editor-fold>

    -- <editor-fold desc="Create structure inventory_action_batch_process_aliq_storage_and_in_stock_details to apply all storage changes">
INSERT INTO structures(`alias`) VALUES ('inventory_action_batch_process_aliq_storage_and_in_stock_details');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'FunctionManagement', '', 'remove_from_storage', 'checkbox',  NULL , '0', '', '', '', '', 'remove storage data');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_batch_process_aliq_storage_and_in_stock_details'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='in_stock' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_values')  AND `flag_confidential`='0'),
     '1', '400', '', '0', '1', '', '1', 'aliquot in stock detail - new value', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_batch_process_aliq_storage_and_in_stock_details'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='in_stock_detail' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_detail')  AND `flag_confidential`='0'),
     '1', '500', '', '0', '1', '', '1', 'detail', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_batch_process_aliq_storage_and_in_stock_details'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_in_stock_detail' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '1', '501', '', '0', '0', '', '1', 'or erase detail', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_batch_process_aliq_storage_and_in_stock_details'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='recorded_storage_selection_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '1', '701', '', '0', '1', '', '1', 'new storage selection label', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_batch_process_aliq_storage_and_in_stock_details'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_from_storage' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='remove storage data'),
     '1', '702', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
    -- </editor-fold>

    -- <editor-fold desc="Add action 'number of elements per participant' for datamart structure 'InventoryActionMaster' ">

SET @control_id = (SELECT id FROM datamart_reports WHERE name = 'number of elements per participant');
INSERT INTO `datamart_structure_functions` (`datamart_structure_id`, `label`, `link`, `flag_active`)
    (SELECT id, 'number of elements per participant', CONCAT('/Datamart/Reports/manageReport\/', @control_id), 1
     FROM datamart_structures WHERE model IN ('InventoryActionMaster'));
    -- </editor-fold>

    -- <editor-fold desc="Remove any records in datamart for models 'SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl'">
-- Note: These lins are commented and put after 2.8B to consider all the tables with the relation to datamart_structures  issue (https://gitlab.com/ctrnet/atim/-/issues/423): Delete QC, SR and AR from datamart structure

-- DELETE FROM datamart_structure_functions WHERE datamart_structure_id In (SELECT id FROM `datamart_structures` where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl'));
-- DELETE FROM datamart_browsing_controls WHERE id1 In (SELECT id FROM `datamart_structures` where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl'));
-- DELETE FROM datamart_browsing_controls WHERE id2 In (SELECT id FROM `datamart_structures` where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl'));
-- DELETE FROM datamart_structures where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl');
    -- </editor-fold>

    -- <editor-fold desc="Issue (https://gitlab.com/ctrnet/atim/-/issues/275): Creation of Inv Use/Event in batch and other invetory actions: Make parent forms, errors, etc consistent for all batch actions">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #275: Creation of Inv Use/Event in batch and other invetory actions: Make parent forms, errors, etc consistent for all batch actions.
--  https://gitlab.com/ctrnet/atim/-/issues/275
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='used_aliq_in_stock_details'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '1', '9000', '', '0', '0', '', '0', '', '0', '', '1', 'hidden', '1', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');


    -- </editor-fold>

    -- <editor-fold desc="Issue (https://gitlab.com/ctrnet/atim/-/issues/289): Inventory Use/Event Creation In Batch - Apply to all">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #260: Inventory Action and Data Types Relationship Diagram (Databrowser).
--  https://gitlab.com/ctrnet/atim/-/issues/260
-- ----------------------------------------------------------------------------------------------------------------------------------
DELETE FROM `datamart_browsing_controls`
WHERE
            `id1` = (SELECT id FROM `datamart_structures` WHERE `model` = 'StudySummary') AND `id2` = (SELECT id FROM `datamart_structures` WHERE `model` = 'ViewAliquotUse') OR
            `id1` = (SELECT id FROM `datamart_structures` WHERE `model` = 'ViewAliquotUse') AND `id2` = (SELECT id FROM `datamart_structures` WHERE `model` = 'StudySummary') ;

DELETE FROM `datamart_browsing_controls`
WHERE
            `id1` = (SELECT id FROM `datamart_structures` WHERE `model` = 'ViewAliquot') AND `id2` = (SELECT id FROM `datamart_structures` WHERE `model` = 'ViewAliquotUse') OR
            `id1` = (SELECT id FROM `datamart_structures` WHERE `model` = 'ViewAliquotUse') AND `id2` = (SELECT id FROM `datamart_structures` WHERE `model` = 'ViewAliquot') ;


INSERT INTO `datamart_browsing_controls` (`id1`, `id2`, `flag_active_1_to_2`, `flag_active_2_to_1`, `use_field`) VALUES
                                                                                                                     ((SELECT id FROM `datamart_structures` where model = 'InventoryActionMaster'), (SELECT id FROM `datamart_structures` where model = 'ViewAliquotUse'), '1', '1', 'sample_master_id'),
                                                                                                                     ((SELECT id FROM `datamart_structures` where model = 'ViewAliquotUse'), (SELECT id FROM `datamart_structures` where model = 'StudySummary'), '1', '1', 'study_summary_id'),
                                                                                                                     ((SELECT id FROM `datamart_structures` where model = 'ViewAliquotUse'), (SELECT id FROM `datamart_structures` where model = 'ViewAliquot'), '1', '1', 'aliquot_master_id');


UPDATE `datamart_structures` SET `display_name` = 'aliquot use' WHERE `model` = 'ViewAliquotUse';

INSERT IGNORE INTO i18n (`id`, `en`, `fr`) VALUES
("aliquot use", "Aliquot Use", "Utilisation d'aliquote");

-- </editor-fold>

    -- <editor-fold desc="Issue (https://gitlab.com/ctrnet/atim/-/issues/314): Inventory Use/Event listAll() - Add specific sub-menu">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #314: Inventory Use/Event listAll() - Add specific sub-menu
--  https://gitlab.com/ctrnet/atim/-/issues/314
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `menus` (`id`, `parent_id`, `is_root`, `display_order`, `language_title`, `language_description`, `use_link`, `use_summary`, `flag_active`, `flag_submenu`) 
VALUES 
( 'inv_CAN_2239', 'inv_CAN_221', '0', '2', 'inventory action history listall - aliquot', NULL, '/InventoryManagement/InventoryActionMasters/listAll/specimenaliquot/%%AliquotMaster.id%%', 'InventoryManagement.AliquotMaster::summary', '1', '1'),
( 'inv_CAN_22239b', 'inv_CAN_2221', '0', '2', 'inventory action history listall - aliquot', NULL, '/InventoryManagement/InventoryActionMasters/listAll/samplealiquot/%%AliquotMaster.id%%', 'InventoryManagement.AliquotMaster::summary', '1', '1');

INSERT INTO `menus` (`id`, `parent_id`, `is_root`, `display_order`, `language_title`, `language_description`, `use_link`, `use_summary`, `flag_active`, `flag_submenu`) 
VALUES 
( 'inv_CAN_22238', 'inv_CAN_222', '0', '2', 'inventory action history listall - sample', NULL, '/InventoryManagement/InventoryActionMasters/listAll/sample/%%SampleMaster.id%%', 'InventoryManagement.SampleMaster::derivativeSummary', '1', '1');

INSERT INTO `menus` (`id`, `parent_id`, `is_root`, `display_order`, `language_title`, `language_description`, `use_link`, `use_summary`, `flag_active`, `flag_submenu`) 
VALUES 
( 'inv_CAN_22237', 'inv_CAN_1', '0', '2', 'inventory action history listall - specimen', NULL, '/InventoryManagement/InventoryActionMasters/listAll/specimen/%%SampleMaster.initial_specimen_sample_id%%', 'InventoryManagement.SampleMaster::specimenSummary', '1', '1');

UPDATE menus SET display_order = 3 WHERE id = 'inv_CAN_222' AND language_title = 'listall derivatives';

REPLACE INTO i18n (id,en,fr)
VALUES
('inventory action history listall - specimen', 'Events/Annotations', 'Événements/annotations'),
('inventory action history listall - sample', 'Events/Annotations', 'Événements/annotations'),
('inventory action history listall - aliquot', 'Events/Annotations', 'Événements/annotations');

-- </editor-fold>

    -- <editor-fold desc="Issue (https://gitlab.com/ctrnet/atim/-/issues/260): Inventory Action and Data Types Relationship Diagram (Databrowser).">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #260: Inventory Action and Data Types Relationship Diagram (Databrowser).

--  https://gitlab.com/ctrnet/atim/-/issues/260
-- ----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `datamart_structures` ADD `options` varchar(255) COLLATE 'latin1_swedish_ci' DEFAULT NULL;
UPDATE `datamart_structures` SET `options` = '{"stopBranching": true}' WHERE `model` = 'ViewAliquotUse';
    -- </editor-fold>

    -- <editor-fold desc="Issue (https://gitlab.com/ctrnet/atim/-/issues/289): Inventory Use/Event Creation In Batch - Apply to all">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #289: Inventory Use/Event Creation In Batch - Apply to all
--  https://gitlab.com/ctrnet/atim/-/issues/289
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('inventory_action_selection_type_hidden');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_selection_type_hidden'),
     (SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action_control_id' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_control_common_list')  AND `flag_confidential`='0'),
     '1', '10', '', '0', '0', '', '0', '', '0', '', '1', 'hidden', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_selection_type_hidden'),
     (SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquotSampleIds' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''),
     '1', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0');


INSERT INTO `datamart_structure_functions` (`datamart_structure_id`, `label`, `link`, `flag_active`) VALUES
((SELECT `id` FROM `datamart_structures` WHERE `model` = 'ViewAliquot'), 'create use/event (applied to all)', '/InventoryManagement/InventoryActionMasters/selectActionTypePage/true', 1),
((SELECT `id` FROM `datamart_structures` WHERE `model` = 'ViewSample'), 'create use/event (applied to all)', '/InventoryManagement/InventoryActionMasters/selectActionTypePage/true', 1);
    -- </editor-fold>

    -- <editor-fold desc="used_aliq_in_stock_details structure update">

UPDATE structure_formats SET `flag_batchedit`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='in_stock' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_values') AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_batchedit`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='in_stock_detail' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_detail') AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_batchedit`='0' WHERE structure_id IN (SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_from_storage' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='collection_id' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='sample_master_id' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2' WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_from_storage' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode' AND `language_label`='barcode' AND `language_tag`='' AND `type`='input' AND `setting`='size=30' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='aliquot_type' AND `language_label`='aliquot type' AND `language_tag`='' AND `type`='select' AND `setting`='' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type') AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='StorageLayout' AND `model`='StorageMaster' AND `tablename`='storage_masters' AND `field`='selection_label' AND `language_label`='storage' AND `language_tag`='' AND `type`='input' AND `setting`='size=20' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='stor_selection_label_defintion' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='storage_coord_x' AND `language_label`='position into storage' AND `language_tag`='' AND `type`='input' AND `setting`='size=4' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='storage_coord_y' AND `language_label`='' AND `language_tag`='' AND `type`='input' AND `setting`='size=4' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='used_aliq_in_stock_details') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='aliquot_label' AND `language_label`='aliquot label' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'AliquotControl', 'aliquot_controls', 'volume_unit', 'hidden',  NULL , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='used_aliq_in_stock_details'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''),
     '1', '9000', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

REPLACE INTO i18n (id,en,fr)
VALUES
('aliquot in stock detail - new value', "Used Aliquot : Stock Detail - New 'In Stock' Value", "Aliquote utilisée : Détail du stock - Nouvelle donnée 'En stock'"),
('fields values of the section below will be applied to all other sections if entered and will replace sections fields values',
'The values of the fields in the section below will be applied to each of the similar fields in the following sections if entered!',
'Les valeurs des champs de la section ci-dessous seront appliquées à chacun des champs similaires des sections suivantes si elles sont saisies!');
INSERT INTO i18n (id,en,fr)
VALUES
('data to be applied to all aliquots used -- fields values of the section below will be applied to all other sections if entered and will replace sections fields values',
"Data to be applied to all used aliquots (if entered, the values of the fields in the section below will be applied to each of the similar fields in the following sections).",
"Données à appliquer à toutes les aliquotes utilisées (si saisies, les valeurs des champs de la section ci-dessous seront appliquées à chacun des champs similaires des sections suivantes).");

INSERT INTO i18n (id,en,fr)
VALUES
    ("batch creation process", "Batch Creation Process", "Processus de création par lot"),
    ("children aliquot creation", "Children Aliquots Creation", "Création aliquotes 'enfants'"),
    ("children aliquot selection", "Children Aliquots Selection", "Sélection aliquotes 'enfants'");
INSERT INTO i18n (id,en,fr)
VALUES
    ('inventory action selection', 'Inventory Event/Annotation Creation Process', "Processus de création d'événements/annotations d'inventaire"),
    ('inventory action creation process', 'Inventory Event/Annotation Creation Process', "Processus de création d'événements/annotations d'inventaire");

    -- </editor-fold>

    -- <editor-fold desc="new structure used_parent_label_for_displays">

INSERT INTO structures(`alias`) VALUES ('used_parent_label_for_displays');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'Generated', '', 'used_parent_label_for_display_title', 'hidden',  NULL , '0', '', '', '', '', ''),
    ('InventoryManagement', 'Generated', '', 'used_parent_label_for_display_description', 'hidden',  NULL , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='used_parent_label_for_displays'),
     (SELECT id FROM structure_fields WHERE `model`='Generated' AND `tablename`='' AND `field`='used_parent_label_for_display_title' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''),
     '9', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='used_parent_label_for_displays'),
     (SELECT id FROM structure_fields WHERE `model`='Generated' AND `tablename`='' AND `field`='used_parent_label_for_display_description' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''),
     '9', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

            -- </editor-fold>

    -- <editor-fold desc="remove structrue used_aliq_in_stock_detail_volume & batch_process_view_sample_joined_to_collection">

DELETE FROM structure_formats WHERE structure_id = (SELECT id FROM structures WHERE alias='used_aliq_in_stock_detail_volume');
DELETE FROM structures WHERE alias='used_aliq_in_stock_detail_volume';

DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='batch_process_view_sample_joined_to_collection');
DELETE FROM structures WHERE alias='batch_process_view_sample_joined_to_collection';

            -- </editor-fold>
        
-- </editor-fold>

-- <editor-fold defaultstate="collapsed" desc="ATiM FormBuilder MySQL Scripts">

    -- <editor-fold defaultstate="collapsed" desc="Add the new words in the dictionary">

-- -------------------------------------------------------------------------------------
-- The dictionary
-- -------------------------------------------------------------------------------------

INSERT IGNORE INTO
    i18n (id,en,fr)
VALUES
  ('control / form type to display to user', 'Form/Data type to display to user', "Nom du formulaire/type de données à afficher à l'utilisateur"),
  ('inv. action categ. : apply on help message',
  "Precisions:
		<ul>
            <li><b>Selected samples types only: </b>The event/annotation can only be created for sample type(s) defined in the 'Sample Type' field below.</li>
            <li><b>Selected samples types or all their related aliquots types: </b>The event/annotation can only be created for either sample type(s) defined in the 'Sample Type' field below or all aliquot type(s) that could be created for a sample type defined in the 'Sample Type' field below.</li>
            <li><b>Selected samples types or some of their related aliquots types: </b>The event/annotation can only be created for either sample type(s) defined in the 'Sample Type' field below  or a specific subset of aliquot type(s)  defined in the 'Aliquot Type' field and limited to type that could be created for sample type(s) defined in the 'Sample Type' field below.</li>
            <li><b>Selected samples types ou selected aliquots types (types not necessarily related): </b>The event/annotation can only be created for either sample type(s) defined in the 'Sample Type' field below or aliquot types defined in the 'Aliquot Type' field below. Relation between types in both fields is not mandatory.</li>
            <li><b>Selected aliquots types only: </b>The event/annotation can only be created for aliquot type(s) defined in the 'Aliquot Type' field below.</li>
            <li><b>Only to all the aliquots types related to the selected samples types: </b>The event/annotation can only be created for all aliquot type(s) that could be created for the sample type(s) defined in the 'Sample Type' field below.</li>
        </ul>",
  "Précisions:
            <li><b>Types d'échantillons sélectionnées uniquement: </b>L'événement/annotation ne peut être créé que pour le(s) type(s) d'échantillon définis dans le champ 'Type d'échantillon' ci-dessous.</li>
            <li><b>Types d'échantillons sélectionnés ou tous leurs types d'aliquotes associés: </b>L'événement/annotation ne peut être créé que pour le(s) type(s) d'échantillon défini(s) dans le champ 'Type d'échantillon' ci-dessous ou pour tous les types d'aliquote qui pourraient être créés pour un type d'échantillon défini dans le champ 'Type d'échantillon' ci-dessous.</li>
            <li><b>Types d'échantillons sélectionnés ou certains de leurs types d'aliquotes associés: </b>L'événement/annotation ne peut être créé que pour le(s) type(s) d'échantillon défini(s) dans le champ 'Type d'échantillon' ci-dessous ou pour un sous ensemble de types d'aliquote définis dans le champ 'Type d'aliquote' et limité aux types qui pourraient être créés pour un des types d'échantillon définis dans le champ 'Type d'échantillon' ci-dessous.</li>
            <li><b>Types d'échantillons sélectionnés ou types d'aliquotes sélectionnés (types non nécessairement associés): </b>L'événement/annotation ne peut être créé que pour le(s) type(s) d'échantillon défini(s) dans le champ 'Type d'échantillon' ci-dessous ou pour le(s) type(s) d'aliquote défini(s) dans le champ 'Type d'aliquote' ci-dessous. La relation entre les types dans les deux champs n'est pas obligatoire.</li>
            <li><b>Types d'aliquotes sélectionnés seulement: </b>L'événement/annotation ne peut être créé que pour le(s) type(s) d'aliquote défini(s) dans le champ 'Type d'aliquote' ci-dessous.</li>
            <li><b>Uniquement à tous les types d'aliquotes associés aux types d'échantillons sélectionnés: </b>L'événement/annotation ne peut être créé que pour tous les types d'aliquote qui pourrai(en)t être créé(s) pour le(s) type(s) d'échantillon défini(s) dans le champ 'Type d'échantillon' ci-dessous</li>
 </li>"),
 ('autocomplete', 'Auto-Complete', 'Auto-complétion'),
  ('participant contact', 'Participant Contact', 'Contact du participant'),
  ('participant message', 'Participant Message', 'Message du participant'),
  ('form', 'Form', 'Formulaire'),
  ('form type', 'Form Type', 'Type de formulaire'),
  ('form detail', 'Form Detail', 'Détails du formulaire'),
  ('form types', 'Form Types', 'Types de formulaire'),
  ('form builder', 'Forms Builder', 'Constructeur de formulaires'),
  ('form builder description', 'A tool to manage ATiM forms and fields (creation, deletion, change).', 'Un outil pour gérer les formulaires de ATiM et les champs (creation, suppression, modification).'),
  ('consent fb_control_description', "Modify existing consent forms structures or create a new ones.", "Modifiez la structure des formulaires de consentements existants ou créez-en de nouveaux."),
  ('diagnosis fb_control_description', "Modify existing diagnosis forms structures or create a new ones.", "Modifiez la structure des formulaires de diagnostics existants ou créez-en de nouveaux."),
  ('sample fb_control_description', "Modify existing sample forms structures or create a new ones.", "Modifiez la structure des formulaires d'échantillons existants ou créez-en de nouveaux."),
  ('form builder index alias', 'The form builder index fields.', "Les champs d\'index du constructeur de formulaire."),
  ('participant fb_model_description', "Modify participant profile form structure.", "Modifer la structure du formuliare du profile des participants."),
  ('participant contact fb_model_description',"Modify participant contact form structure.", "Modifer la structure du formuliare de contact des participants."),
  ('participant message fb_model_description', "Modify participant message form structure.", "Modifer la structure du formuliare de message des participants."),
  ('treatment fb_control_description', "Modify existing treatment forms structures or create a new ones.", "Modifiez la structure des formulaires de traitement existants ou créez-en de nouveaux."),
  ('treatment extend fb_control_description', "Modify existing treatment precision forms structures or create a new ones.", "Modifiez la structure des formulaires de précision des traitements existants ou créez-en de nouveaux."),
  ('event fb_control_description', "Modify existing clinical annotation forms structures or create a new ones.", "Modifiez la structure des formulaires d'annotation clinique existants ou créez-en de nouveaux."),
  ("inventory_use_event fb_control_description", "Modify existing inventory event/annotation forms structures or create a new ones.", "Modifiez la structure des formulaires d'évenement/annotation d'inventaire existants ou créez-en de nouveaux."),
  ('aliquot fb_control_description', "Modify existing aliquot forms structures or create a new ones.", "Modifiez la structure des formulaires d'échantillon existants ou créez-en de nouveaux."),
  ('quality control fb_model_description', "Modify quality control form structure.", "Modifer la structure du formuliare de contrôle de qualité."),
  ('aliquot internal use fb_model_description', "Modify aliquot event/annotation form structure.", "Modifer la structure du formuliare d'évenement/annotation d'aliquote."),
  ('aliquot internal use', "Aliquot Event/Annotation", "Événement/annotation d'aliquote"),
  ('study fb_model_description', "Modify study form structure.", "Modifer la structure du formuliare d'étude."),
  ('collections fb_model_description', "Modify collection form structure.", "Modifer la structure du formuliare de collection."),
  ('reproductive history contact fb_model_description', "Modify reproductive history form structure.", "Modifer la structure du formuliare d'historique gynécologique."),
  ('sop fb_control_description', "Modify existing SOP forms structures or create a new ones.", "Modifiez la structure des formulaires de SOP existants ou créez-en de nouveaux."),
  ('family history fb_model_description',  "Modify family history form structure.", "Modifer la structure du formuliare d'historique familiale."),
    ('consent type', "Consent name", "nom du consentement"),
  ('is_unique', "Unique Value", "Unique valeur"),
  ('is_unique_help', "Check this box if the value is unique. <br>Example: Social Insurance Number.", "Cochez cette case si la valeur est unique. <br> Exemple: Numéro d'Assurance Sociale."),
  ('not_blank', "Required field", "Champs requis"),
  ('not_blank_help', "Check this Box if the value is required. <br>Example: Participant Name.", "Cochez cette case si la valeur est requise. <br> Exemple: Nom de Participant."),
  ('range_heading', "Range Number", "Intervalle"),
  ('range_from', "From", "De"),
  ('range_to', "To", "À"),
  ('range_help', "For the numbers value if there is a boundry limitation. <br>Example: <ul><li>Body Temperature from: 33, to:42</li><li>Number of children is positive: from: 0 to: blank</li></ul>", "Pour la valeur des nombres s'il y a une limite.<br>Exemple:<ul><li>Température corporelle de: 33 à 42</li><li>Le nombre d'enfants est positif: de: 0 à: blanc</li></ul>"),
  ('between_heading', "The text length limit", "La limite de longueur du texte"),
  ('between_from', "From", "De"),
  ('between_to', "To", "À"),
  ('between_help', "The text length limit<br>Example: first name", "La limite de longueur de texte <br> Exemple: prénom"),
  ('alpha_numeric_help', "Should contains the Numeric and the Alphabetic combination", "Devrait contenir la combinaison Numérique et Alphabétique"),
  ('error-the %s should be between %s and %s', "The %s should be between %s and %s.", "Le %s doit être compris entre %s et %s."),
  ('error-the %s length should be between %s and %s', "The %s length should be between %s and %s.", "La longueur de %s doit être comprise entre %s et %s."),
  ('validation rules', "Validation Rules", "Règles de validation"),
  ('flag_confidential_help', "Any value of a confidential field won't be searchable or displayed for group of users having no access to confidential information (field like first name, date of birth, etc.).", "Aucune valeur d'un champ confidentiel ne pourra être recherchée ou affichée pour un groupe d'utilisateurs n'ayant pas accès à des informations confidentielles (champ tel que prénom, date de naissance, etc.)."),
  ('general information', 'Field', 'Champ'),
  ('language_label_help', 'This field will be shown in the form as label.', "Ce champ sera affiché dans le formulaire en tant qu'étiquette."),
  ('language_field_type_help',
     "<b>The data type for the current field.</b>
        <ul>
            <li><b>Textbox: </b>for the short texts</li>
            <li><b>Textarea: </b>for the long texts like comments</li>
            <li><b>List: </b>for the lists of values fix or custom (any new created select field will be a custom drop down list</li>
            <li><b>Multi select list: </b>for the multiple lists of values</li>
            <li><b>Integer: </b>for the integer numbers</li>
            <li><b>Decimal number: </b>for the decimal fields (fraction with e-1, e-2 and e-5 as allowed decimals)</li>
            <li><b>Checkbox: </b>to permit the user to take a binary choice</li>
            <li><b>Yes/No</b>set of two check boxes 'yes' and 'no' to let user to select one of them or uncheck both</li>
            <li><b>Yes/No/Unknown</b>set of two check boxes 'yes', 'no' and 'unknown' to let user to select one of them or uncheck all</li>
            <li><b>Date: </b>for the date field (no time information)</li>
            <li><b>Date/Time: </b>for the date plus time field</li>
            <li><b>Time: </b>for hour and minute fields</li>
            <li><b>ICD-10: </b>to let user to search and select code from the International Statistical Classification of Diseases and Related Health Problems (ICD)</li>
            <li><b>ICD-O-3 Morpho: </b>to let user to search and select a morphologic code from International Classification of Diseases for Oncology, Third Edition (ICD-O-3)</li>
            <li><b>ICD-O-3 Topo: </b>to let user to search and select a topographic code from International Classification of Diseases for Oncology, Third Edition (ICD-O-3)</li>
        </ul>",
     "<b> Le type de données du champ actuel. </b>
        <ul>
            <li><b>Zone de texte: </b>pour la du texte court</li>
            <li><b>Zone de texte: </b>pour les textes longs comme les commentaires</li>
            <li><b>List: </b>pour les listes de valeurs fixes ou personnalisées (tout nouveau champ liste créé sera une liste personnalisée</li>
            <li><b>Liste de sélection multiple: </b>pour les listes de valeurs</li>
            <li><b>Entier: </b>pour les nombres entiers/li>
            <li><b>Nombre décimal: </b>pour les nombres décimaux (fraction avec e-1, e-2 et e-5 comme valeur décimales autorisées).</li>
            <li><b>Case à cocher: </b>pour permettre aux utilisateurs de faire un choix binaire</li>
            <li><b>Oui/Non </b>ensemble de deux cases à cocher 'oui' et 'non' pour permettre à l'utilisateur de sélectionner l'une d'entre elles ou de décocher les deux</li>
            <li><b>Oui/Non/Inconnu </b>ensemble de trois cases à cocher 'oui', 'non' et 'inconnu' pour permettre à l'utilisateur de sélectionner l'une d'entre elles ou de décocher les trois</li>
            <li><b>Date: </b> pour les dates sans heure et minute</li>
            <li><b>Date/heure: </b> pour les dates incluant les heures et minutes</li>
            <li><b>heure: </b> pour les heures et minutes</li>
            <li><b>CIM-10: </b>pour permettre à l'utilisateur de rechercher et de sélectionner un code dans la Classification statistique internationale des maladies et des problèmes de santé connexes (CIM)</li>
            <li><b>Morpho CIM-O-3: </b>pour permettre à l'utilisateur de rechercher et de sélectionner un code morphologique dans la Classification internationale des maladies pour l'oncologie, troisième édition</li>
            <li><b>Topo CIM-O-3: pour permettre à l'utilisateur de rechercher et de sélectionner un code topographique dans la Classification internationale des maladies pour l'oncologie, troisième édition</b></li>
        </ul> "
    ),
  ('language_help_help', 'The description that will be appeared as help.', "La description qui sera présentée à titre d'aide."),
  ('flag_add_help', 'Whether the field will be appeared in add forms', "Si le champ sera affiché sous le formulaire de saisie"),
  ('display according to form type', "Field'S Presence According to the Form Display Mode", "Présence du champ selon le mode d'affichage formulaire"),
  ('flag_edit_help', 'Whether the field will be appeared in edit forms', "Si le champ sera affiché sous le formulaire de modification"),
  ('flag_search_help', 'Whether the field will be appeared in search forms', "Si le champ sera affiché sous le formulaire de modification"),
  ('flag_index_help', 'Whether the field will be appeared in index forms', "Si le champ sera affiché sous le formulaire d'indexe"),
  ('flag_detail_help', 'Whether the field will be appeared in detail forms', "Si le champ sera affiché sous le formulaire de detail"),
  ('display_column_help', 'The column to show the current field', "La colonne pour afficher le champ actuel"),
  ('display_order_help', 'The row to show the current field', "La ligne pour afficher le champ actuel"),
  ('language_heading_help', 'Blue header displayed in forms to separate set of fields in sub-forms.', "En-tête bleu affiché dans les formulaires pour séparer un ensemble de champs dans des sous-formulaires."),
  ('layout format', 'Form Layout', 'Mise en page du formulaire'),
  -- 	('editable fields', '', ''),
  -- 	('not editable fields', '', ''),
  ('has_validation_help', 'The specific rules for validating the data entered in the field.', 'Les règles spécifiques de validation des données saisies dans le champ.'),
  ('has_validation', 'Validation Rules', 'Règles de validation'),
  ('is_structure_value_domain_help', 'If the selected field type is \'List\', the definition of the \'Custom Drop Down List\' is required. A new one can be created or a list previously defined can be re-used.',
  'Si le type de champ sélectionné est \'Liste\', la définition de la \'Liste personnalisée\' est requise. Un nouvelle liste peut être créée ou une liste précédemment définie peut être réutilisée.'),
  ('is_structure_value_domain', 'Values List Name', 'Nom de la liste de valeurs'),
  ('field type [%s] is unknown', 'Field type [%s] is unknown.', 'Champ de type [%s] est inconnu.'),
  ('click to add the validation rules', 'Click to add the validation rules.', 'Cliquez pour ajouter les règles de validation.'),
  ('value/domain is required', 'Value/Domain is required.', 'La liste de valeurs est requis.'),
  ('length from %s to %s', 'The length is from %s to %s', 'La taille est entre %s et %s'),
  ('value from %s to %s', 'The value is between %s and %s', 'La valeur est entre %s et %s'),
  ('required', 'Required', 'Requis'),
  ('unique', 'Unique', 'Unique'),
  ('there is no validation rule for %s datatype', 'There is no validation rule for %s data type.', "Il n'y a pas de règle de validation pour le type de données%s."),
  ('list name', 'List Name', 'Nom de la liste'),
  ('please select the list items', 'Please select the list items.', 'Veuillez sélectionner les éléments de la liste.'),
  ('here', 'here', 'ici'),
  ('for customising the <b>%s</b> list click <b>%s</b>', 'For customising the "<b>%s</b>" list click <b>%s</b>', 'Pour personnaliser la "<b>%s</b>" liste, cliquez <b>%s</b>'),
  ('list_name_help', 'You can type the name of an existing list or create a new one.', "Vous pouvez taper le nom d'une liste existante ou en créer une nouvelle."),
  ('other models', 'Other Models', 'Autres modèles'),
  ('manage form', 'Manage Form', 'Gérer le formulaire'),
  ('list of forms', 'List of Forms', 'Liste des formulaires'),
  ('flag_confidential', 'Confidential', 'Confidentiel'),
  ('language_label', 'Field Label', 'Étiquette de champ'),
  ('language_help', 'Help Message (Description of the field)', "Message d'aide (description du champ)"),
  ('field type', 'Field Type', 'Type de champ'),
  ('flag_add', 'Add', 'Création'),
  ('flag_edit', 'Edit', 'Modification'),
  ('flag_search', 'Search', 'Recherche'),
  ('flag_index', 'Index', 'Index'),
  ('flag_detail', 'Detail', 'Détail'),
  ('display_column', 'Column', 'Colonne'),
  ('display_order', 'Position (Line)', 'Position (ligne)'),
  ('control information %s', "\'%s\' Control Information", "Informations de contrôle \'%s\'"),
  ('control detail', 'Detail Fields', 'Champs de détail'),
  ('click to add the validation rules', 'Click to add the validation rules.', 'Cliquez pour ajouter les règles de validation.'),
  ('language_heading', 'Heading Title', 'Titre de la section'),
  ('checkbox', 'Checkbox', 'Case à cocher'),
  ('date', 'Date', 'Date'),
  ('datetime', 'Date/Time', 'Date/Heure'),
  ('input', 'Text box', 'Boite de texte'),
  ('integer-type', 'Integer', 'Entier'),
  ('integer_positive', 'Integer positive', 'Entier positif'),
  ('float1', 'Decimal (e-1)', 'Décimal (e-1)'),
  ('float2', 'Decimal (e-2)', 'Décimal (e-2)'),
  ('float5', 'Decimal (e-5)', 'Décimal (e-5)'),
  ('yes no unknown', 'Yes/No/Unknown', 'Oui/No/Inconnu'),
  ('select', 'List', 'Liste'),
  ('textarea', 'Text area', 'Zone de texte'),
  ('time', 'Time', 'Temps'),
  ('click to add/modify list', 'Click to add/modify the list', 'Cliquez pour ajouter/modifier la liste'),
  ('check the validations rules after changing the data type', 'Please check the validations rules, because the data type has been changed.', 'Veuillez vérifier les règles de validation, car le type de données a été modifié.'),
  ('should complete both or none of them', 'Should complete both or none of them.', "Devrait compléter les deux ou aucun d'entre eux."),
  ('validateIcdo3TopoCode', 'ICD-O-3 Topo', 'ICD-O-3 Topo'),
  ('validateIcdo3MorphoCode', 'ICD-O-3 Morpho', 'ICD-O-3 Morpho'),
  ('validateIcd10WhoCode', 'ICD-10', 'ICD-10'),
  ('icd_0_3_topography_categories', 'ICD-O-3 Topo Categoris', 'ICD-O-3 Topo Catégories'),
  ('yes_no', 'Yes/No', 'Oui/Non'),
  ('preview', 'Preview', 'Aperçu'),
  ('see the preview of the form', 'A preview of the add form', 'Un aperçu du formulaire ajouter'),
  ('preview of the %s form', '\'%s\' Form Preview', 'Aperçu du formulaire \'%s\''),
  ('dictionary', 'Dictionary', 'Dictionaire'),

  ('fb_simple_model_fields_in_prod', 'Fields in Production', 'Champs en production'),
  ('fb_simple_model_fields_in_test', 'Fields in Test', 'Champs en test'),

  ('fb_master_model_fields_in_prod', 'Common Fields in Production', 'Champs communs en production'),
  ('fb_master_model_fields_in_prod_description_[%s]', 'Common fields of any ''%s'' form and already used in production.', 'Champs communs à tous les formulaires de ''%s'' et déjà utilisés en production.'),

  ('fb_master_model_fields_in_test', 'Common Fields in Test', 'Champs communs en test'),
  ('fb_master_model_fields_in_test_description_[%s]', 'Fields in test (not used in production) which will be attached to all ''%s'' forms when they will be made available in production.', 'Champs en test (non utilisés en production) qui seront attachés à tous les formulaires ''%s'' lorsqu''ils seront rendus disponibles en production.'),

  ('fb_detail_model_old_fields_in_prod', "Specific (Unchangeable) Fields in Production.", "Champs spécifiques (et non modifiables) en production."),
  ('fb_detail_model_old_fields_in_prod_description_[%s]',
     "Specific fields of '%s' forms, unchangeable and already used in production ('Unchangeable' when the fields are attached to a new form created by copying a form because the fields were not created by the 'Form builder').",
     "Champs spécifiques aux formulaires '%s', non modifiables et déjà utilisés en production ('Non modifiables' lorsque les champs sont attachés à un nouveau formulaire créé par copie de formulaire car les champs n'ont pas été créés par le 'Constructeur de formulaires')."),

  ('fb_detail_model_fields_in_prod', 'Specific Fields in Production', 'Champs spécifiques en production'),
  ('fb_detail_model_fields_in_prod_description_[%s]', 'Specific fields of ''%s'' forms and already used in production.', 'Champs spécifiques aux formulaires ''%s'' et déjà utilisés en production.'),

  ('fb_detail_model_fields_in_test', 'Specific fields in Test', 'Champs spécifiques en test'),
  ('fb_detail_model_fields_in_test_description_[%s]', 'Specific fields in test (not used in production) which will only be attached to ''%s'' forms when they will be made available in production.', 'Champs spécifiques en test (non utilisés en production) qui seront uniquement attachés aux formulaires ''%s'' lorsqu''ils seront rendus disponibles en production.'),

  ('fb_detail_model_fields_of_new_form', 'Specific fields of the new form', 'Champs spécifiques du nouveau formulaire'),
  ('fb_detail_model_fields_of_new_form_description', 'Add new specific fields which will only be attached to the created form when it will be made available in production.', 'Créer des champs spécifiques qui seront uniquement attachés aux formulaires ''%s'' lorsqu''ils seront rendus disponibles en production.'),

  ('add the new words in to en & fr dic', 'Add the new words into English and French dictionary.', 'Ajoutez les nouveaux mots dans les dictionnaires anglais et français.'),
  ('the links are disabled in the preview window', 'The links are disabled in the preview window.', "Les liens sont désactivés dans la fenêtre d'aperçu."),
  ('change the mode from test to production mode', 'Change the mode from Test to Production.', 'Changez le mode de test à production.'),
  ('testToProd', 'Test to Prod', 'De Teste à Prod'),
  ('types of forms', 'Types of Forms', 'Types de formulaires'),
  ('form builder simple model form', 'Simple Form', 'Formulaire simple'),
  ('form builder master detail model forms', 'Multi-Forms', 'Multi-formulaires'),
  ('form builder master detail model form', 'Form', 'Formulaire'),
  ('form builder other model forms', 'Other Forms', 'Autres formulaires'),
  ('form builder other model form', 'Form', 'Formulaire'),
  ("form information", "Form Information", "Information du formulaire"),
  ("form builder other_model header", 'Other Forms', 'Autres formulaires'),
  ("form builder master_detail_model header", 'Multi-Forms', 'Multi-formulaires'),
  ('form builder simple_model header', 'Simple Forms', 'Formulaires simples'),
  ('consent type is required', 'Consent type is required.', 'Le type de consentement est requis.'),
  ('consent type should be unique', 'Consent type should be unique.', 'Le type de consentement doit être unique.'),
  ('field type is required', 'Field type is required.', 'Le type de champ est obligatoire.'),
  ('field label is required', 'Field label is required.', "L'étiquette de champ est requise."),
  ('dictionary data saved successfully', "Dictionary data saved successfully.", "Les données du dictionnaire ont été enregistrées avec succès."),
  ('.', ".", "."),
  (',', ",", ","),
  ('the control is deleted', "The control has been deleted.", "Le contrôle a été supprimé."),
  ('the control can not be deleted because its in production mode', "The control can not be deleted because it's in production mode.", "Le contrôle ne peut pas être supprimé car il est en mode de production."),
  ('there is no data dictionary', "There is no word in this form to add to the dictionary.", "Il n'y a pas de mot dans ce formulaire pour ajouter au dictionnaire."),
  ('there is no field for save in this form', "There is no new field to make available in production to users in this form.", "Il n'y a pas de nouveau champ à rendre accessible en production aux utilisateurs dans ce formulaire."),
  ('are you sure to submit the structure fields change don in FB?',
	"You are about to commit a new form or some form structure update(s) and make it/them available in production. Do you want to continue? Note that the changes:<br><ul class = 'listWithBullet'><li>Will affect ATiM froms accessible to users in production.</li><li>Are not reversible without an ATiM developper.</li><li>Will affect all forms of the same type when change are on common fields.</li></ul>",
	"Vous êtes sur le point de valider un nouveau formulaire ou une ou plusieurs mises à jour de la structure du formulaire et de les rendre disponibles en production. Voulez-vous continuer? Notez que les modifications:<br><ul class = 'listWithBullet'><li>Affecteront les formulaires ATiM accessibles aux utilisateurs en production.</li><li>Ne sont pas réversibles sans un développeur ATiM.</li><li>Affecteront tous les formulaires du même type lorsque les modifications concernent des champs communs.</li></ul>"),
  ('are you sure to add a field to master?', "In this part the fields will be added in all forms.", "Dans cette partie, les champs seront ajoutés dans tous les formulaires."),
  ('the form has been created and is useable from now', "The form has been created and is useable from now.", "Le formulaire a été créé et est utilisable à partir de maintenant."),
  ('consent', "Consent", "Consentment"),
  ('these fields will be shown on all the %s forms', "These fields will only be displayed in the '%s' forms when they will be made available in production.", "Ces champs seront affichés uniquement dans les formulaires '%s' lorsqu'ils seront rendus disponibles en production.."),
  ('fields shown on all the %s forms', "Fields only displayed in the '%s' forms and already used in production.", "Champs affichés uniquement dans les formulaires '%s' et déjà utilisés en production."),
  ('test mode', "Test mode", "Mode teste"),
  ('test mode help', "The forms in the test mode are draft.", "Les formulaires en mode test sont brouillons."),
  ('are you sure to make disable the control', "Are you sure to make disable the control?", "Êtes-vous sûr de faire désactiver le contrôle?"),
  ('are you sure to make enable the control', "Are you sure to make enable the form?", "Êtes-vous sûr de faire activer le formulaire?"),
  ('disable', "Disable", "Désactiver"),
  ('enable', "Enable", "Active"),
  ('flag active', "Active", "Actif"),
  ('flag active as input', "Active for input", "Actif pour l'entrée"),
  ('help_flag_active_fb', "Determines whether the control is active or not.", "Détermine si le contrôle est actif."),
  ('help_flag_active_input_fb', "Determines whether the control can be used as an input for new data.", "Détermine si le contrôle peut être utilisé pour la saisie de nouvelles données."),
  ('the control has been enabled', "The control has been enabled.", "Le contrôle a été activé."),
  ('the control has been disabled', "The control has been disabled.", "Le contrôle a été désactivé."),
  ('make disable the control', "Make disable the control.", "Faire désactiver le contrôle."),
  ('make enable the control', "Make enable the control.", "Faire activer le contrôle."),
  ('the copy of %s has been created', "The copy of %s has been created.", "La copie de %s a été créée."),
  ('fbDeletable', "The specific fields that are just deletable.", "Les champs spécifiques qui sont simplement supprimables."),
  ('are you sure to make a copy of the control', "Are you sure to make a copy of the form?", "Etes-vous sûr de vouloir faire une copie du formulaire?"),
  ('no data for [%s.%s]', 'No data for [%s.%s].', 'Aucune donnée pour [%s.%s]'),
  ('no result', "No Result: ", "Aucun résultat: "),
  ('hidden', "Hidden", "Caché"),
  ('aliquot internal uses', "Aliquot internal uses", "Aliquote des utilisations internes"),
  ('select an option', "Select an option", "Choisir une option"),
  ("inventory action type", "Event/Annotation  Type", "Type d'évenement/annotation"),
  ("it will be appear in data browser", "It will be appear in data browser", "Il apparaîtra dans le navigateur de données"),
  ("apply on", "Apply on", "Appliquer sur"),
  ("linked to a study", "Study Link", "Lien à étude"),
  ("inv action: link to storage", "Aliquot Storage Update Option", "Option mise à jour entreposage (aliquote)"),
  ("please select some aliquots", "Please select some Aliquots", "Veuillez sélectionner quelques Aliquotes"),
  ("please select some sample types", "Please select some Sample Types", "Veuillez sélectionner quelques types d'échantillon"),
  ("please select some sample or aliquot types", "Please select some Samples or Aliquots", "Veuillez sélectionner quelques types d'échantillons ou aliquotes"),
  ("change this option maybe will reset the selected samples and aliquots. do you want to continue?", "Changing this option may reset the selected samples or aliquots. Do you want to continue?", "La modification de cette option peut réinitialiser les échantillons ou les aliquotes sélectionnés.Voulez-vous continuer?"),
  ("multi-select-list", "Multi select list", "Liste de sélection multiple"),
  ("multi select", "Multiple Select", "Sélection Multiple"),
  ("multi_select_help", "Multiple / Single Select", "Sélection Multiple / Unique"),
  ("error in saving %s (%s, %s) %s", "Error in saving %s (%s, %s) %s", "Erreur lors de la sauvegarde %s (%s,%s) %s"),
  ("inventory action category", "Event/Annotation Category", "Catégorie d'évenement/annotation"),
  ("fixed list", "Fixed list", "Liste fixe"),
  ("functional list", "List generated by functions", "Liste généré par fonctions"),
  ("this type has already existed", "This type has already existed", "Ce type a déjà existé"),
  ("form_builder_for customising the (%s) list click here", "For customising the (%s) list click here", "Pour personnaliser la liste (%s), cliquez ici"),
  ("form_builder_miscidentifier cannot be modified in prod mode", "Identifier cannot be modified in prod mode", "Identifiant non modifiable en mode production"),
  ("(multiple choice)", " (Multiple choice)", " (Choix multiples)"),
  ("are you sure to activate the control", "Are you sure to activate the control?", "Êtes-vous sûr d''activer le contrôle ?"),
  ("are you sure to deactivate the control", "Are you sure to deactivate the control?", "Êtes-vous sûr de désactiver le contrôle ?"),
  ("the control has been active", "The control has been activated.", "Le contrôle a été activé."),
  ("the control has been inactive", "The control has been deactivated.", "Le contrôle a été désactivé."),
  ("deactivate the control", "Deactivate the control", "Désactiver le champ"),
  ("activate the control", "Activate the control", "Activer le contrôle"),
  ("ia_help_flag_action_in_batch", "This let you to apply this event on batch on many Aliquots/Samples in one form.", "Cela vous permet d''appliquer cet événement par lot sur de nombreux aliquotes/échantillons sous une seule forme."),
  ("flag action in batch", "Apply on Batch", "Appliquer par lot"),
  ("fb_ the form cannot be used in batch action and have unique field at the same time", "The form cannot be used in a batch action and has a unique field at the same time.", "Le formulaire ne peut pas être utilisé dans une action par lots et possède en même temps un champ unique."),
  ("inventory action category", "Event/Annotation Category", "Catégorie d'évenement/annotation");
-- ("", "", ""),

INSERT IGNORE INTO
    i18n (id,en,fr)
VALUES
  ("inv. action help: sp ctlr ids", "See 'Apply On' field help.", "Voire aide du champ 'Appliquer sur'."),
  ("inv. action help: al ctlr ids", "See 'Apply On' field help.", "Voire aide du champ 'Appliquer sur'."),
  ("inv. action help: link to study", "When checked the study fields will be added to the form structure and will let users to link the created inventory event/annotation to a study.",
       "Si coché, les champs d'étude seront ajoutés à la structure du formulaire et permettront aux utilisateurs de lier l'événement/annotation d'inventaire créé à une étude."),
  ("inv. action help: link to storage", "When checked, the user will be able to defined directly in the form the new storage information of the aliquot attached to the new inventory event/annotation created from the aliquot. Limited to aliquots only.",
     "Si cochée, l'utilisateur pourra directement dans le fomrulaire définir les nouvelles informations d'entreposage de l'aliquote attaché à la nouvelle événement/annotation d'inventaire créé à partir de l'aliquote. Limité aux aliquotes uniquement.");


-- </editor-fold>

    -- <editor-fold desc="Add FB to administrator Menu">
-- -------------------------------------------------------------------------------------
-- Add FB to administrator Menu
-- -------------------------------------------------------------------------------------
DELETE FROM menus where use_link like '/Administrate/FormBuilders%';

INSERT INTO `menus`
(`id`, `parent_id`, `is_root`, `display_order`, `language_title`, `language_description`, `use_link`, `use_summary`, `flag_active`, `flag_submenu`)
VALUES
    ('core_CAN_41_10', 'core_CAN_41', '0', '8', 'form builder', NULL, '/Administrate/FormBuilders/index', '', '1', '1'),
    ('core_CAN_41_101', 'core_CAN_41_10', 0, 1, 'form builder simple model form', NULL, '/Administrate/FormBuilders/detail/simple_model/%%FormBuilder.id%%/', 'Administrate.FormBuilder::formSummary', 1, 1),
    ('core_CAN_41_102', 'core_CAN_41_10', 0, 1, 'form builder master detail model forms', NULL, '/Administrate/FormBuilders/listAll/master_detail_model/%%FormBuilder.id%%/', 'Administrate.FormBuilder::listOfFormsSummary', 1, 1),
    ('core_CAN_41_1021', 'core_CAN_41_102', 0, 1, 'form builder master detail model form', NULL, '/Administrate/FormBuilders/detail/master_detail_model/%%FormBuilder.id%%/%%Control.id%%', 'Administrate.FormBuilder::formSummary', 1, 1),
    ('core_CAN_41_103', 'core_CAN_41_10', 0, 1, 'form builder other model forms', NULL, '/Administrate/FormBuilders/listAll/other_model/%%FormBuilder.id%%/', 'Administrate.FormBuilder::listOfFormsSummary', 1, 1),
    ('core_CAN_41_1031', 'core_CAN_41_103', 0, 1, 'form builder other model form', NULL, '/Administrate/FormBuilders/detail/other_model/%%FormBuilder.id%%/%%Control.id%%', 'Administrate.FormBuilder::formSummary', 1, 1);
    -- </editor-fold>

    -- <editor-fold desc="Create & Initialise the form_builders table">
    -- <editor-fold desc="Create the form_builders table which keep the list of models can be customized">
-- -------------------------------------------------------------------------------------
--	Create the form_builders table which keep the list of models can be customized
-- -------------------------------------------------------------------------------------

CREATE TABLE `form_builders` (
                                 `id` int(4) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
                                 `label` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL,
                                 `plugin` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL,
                                 `model` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '',
                                 `master` varchar(255) COLLATE 'latin1_swedish_ci' NULL,
                                 `detail` varchar(255) COLLATE 'latin1_swedish_ci' NULL DEFAULT '',
                                 `master_table` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL,
                                 `table` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '',
                                 `alias` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '',
                                 `default_alias` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL,
                                 `note` text COLLATE 'latin1_swedish_ci' NULL,
                                 `options` varchar(1023) NULL DEFAULT '',
                                 `flag_active` tinyint(1) NOT NULL DEFAULT '1',
                                 `display_order` int(11) NOT NULL DEFAULT '0'
) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';
    -- </editor-fold>

    -- <editor-fold desc="initializing the form_builders table">

INSERT INTO `form_builders` (`label`, `plugin`, `model`, `master`, `detail`, `master_table`, `table`, `alias`, `default_alias`, `note`, `flag_active`, `display_order`) VALUES
                                                                                                                                                                            ('consent', 'ClinicalAnnotation', 'ConsentControl', 'ConsentMaster', 'ConsentDetail', 'consent_masters', 'consent_controls', 'form_builder_consent', 'consent_masters', 'consent fb_control_description', 1, 1),
                                                                                                                                                                            ('diagnosis', 'ClinicalAnnotation', 'DiagnosisControl', 'DiagnosisMaster', 'DiagnosisDetail', 'diagnosis_masters', 'diagnosis_controls', 'form_builder_diagnosis', 'diagnosismasters', 'diagnosis fb_control_description', 1, 2),
                                                                                                                                                                            ('clinical event', 'ClinicalAnnotation', 'EventControl', 'EventMaster', 'EventDetail', 'event_masters', 'event_controls', 'form_builder_event', 'eventmasters', 'event fb_control_description', 1, 3),
                                                                                                                                                                            ('treatment', 'ClinicalAnnotation', 'TreatmentControl', 'TreatmentMaster', 'TreatmentDetail', 'treatment_masters', 'treatment_controls', 'form_builder_treatment', 'treatmentmasters', 'treatment fb_control_description', 1, 4),
                                                                                                                                                                            ('treatment precision', 'ClinicalAnnotation', 'TreatmentExtendControl', 'TreatmentExtendMaster', 'TreatmentExtendDetail', 'treatment_extend_masters', 'treatment_extend_controls', 'form_builder_treatment_extend', 'treatment_extend_masters', 'treatment extend fb_control_description', 1, 5),
                                                                                                                                                                            ('sample', 'InventoryManagement','SampleControl', 'SampleMaster', 'SampleDetail', 'sample_masters', 'sample_controls', 'form_builder_sample', 'sample_masters', 'sample fb_control_description', 1, 6),
                                                                                                                                                                            ('aliquot', 'InventoryManagement', 'AliquotControl', 'AliquotMaster', 'AliquotDetail', 'aliquot_masters', 'aliquot_controls', 'form_builder_aliquot', 'aliquot_masters', 'aliquot fb_control_description', 1, 7),
-- ('specimen review name', 'InventoryManagement', 'SpecimenReviewControl', 'SpecimenReviewMaster', 'SpecimenReviewDetail', 'specimen_review_masters', 'specimen_review_controls', 'form_builder_specimen_review', 'specimen_review_masters', 'specimen review fb_control_description', 1, 8),
-- ('aliquot review', 'InventoryManagement', 'AliquotReviewControl', 'AliquotReviewMaster', 'AliquotReviewDetail', 'aliquot_review_masters', 'aliquot_review_controls', 'form_builder_aliquot_review', 'aliquot_review_masters', 'aliquot review fb_control_description', 1, 9),
                                                                                                                                                                            ('protocol', 'Protocol', 'ProtocolControl', 'ProtocolMaster', 'protocol_masters', 'protocol_controls', 'protocol_controls', 'form_builder_protocol', 'protocolmasters', 'protocol fb_control_description', 0, 10),
-- ('sop', 'Sop', 'SopControl', 'SopMaster', 'SopDetail', 'sop_masters', 'sop_controls', 'form_builder_sop', 'sopmasters', 'sop fb_control_description', 1, 11),
                                                                                                                                                                            ('A', 'protocol_extend_controls', 'protocol_extend_controls', NULL, '', 'protocol_extend_controls', '', 'protocol_extend_controls', 'protocol_extend_controls', NULL, 0, 0),
                                                                                                                                                                            ('A', 'sop_controls', 'sop_controls', NULL, '', 'sop_controls', '', 'sop_controls', 'sop_controls', NULL, 0, 0),
                                                                                                                                                                            ('misc identifiers', 'ClinicalAnnotation', 'MiscIdentifierControl', 'MiscIdentifier', '', 'misc_identifiers', 'misc_identifier_controls', 'form_builder_misc_identifier', 'form_builder_misc_identifier', 'misc identifier fb_control_description', 1, 14),
                                                                                                                                                                            ('participant', 'ClinicalAnnotation', 'Participant', '', '', 'participants', '', '', 'participants', 'participant fb_model_description', 1, 1),
                                                                                                                                                                            ('participant contact', 'ClinicalAnnotation', 'ParticipantContact', '', '', 'participant_contacts', '', '', 'participantcontacts', 'participant contact fb_model_description', 1, 2),
                                                                                                                                                                            ('family history', 'ClinicalAnnotation', 'FamilyHistory', '', '', 'family_histories', '', '', 'familyhistories', 'family history fb_model_description', 1, 3),
                                                                                                                                                                            ('participant message', 'ClinicalAnnotation', 'ParticipantMessage', '', '', 'participant_messages', '', '', 'participantmessages', 'participant message fb_model_description', 1, 4),
                                                                                                                                                                            ('reproductive history', 'ClinicalAnnotation', 'ReproductiveHistory', '', '', 'reproductive_histories', '', '', 'reproductivehistories', 'reproductive history contact fb_model_description', 1, 5),
                                                                                                                                                                            ('study', 'Study', 'StudySummary', '', '', 'study_summaries', '', '', 'studysummaries', 'study fb_model_description', 1, 6),
-- ('quality control', 'InventoryManagement', 'QualityCtrl', '', '', 'quality_ctrls', '', '', 'qualityctrls', 'quality control fb_model_description', 1, 14),
                                                                                                                                                                            ('collection', 'InventoryManagement', 'Collection', '', '', 'collections', '', '', 'collections', 'collections fb_model_description', 1, 7),
-- ('aliquot internal use', 'InventoryManagement', 'AliquotInternalUse', '', '', 'aliquot_internal_uses', '', '', 'aliquotinternaluses', 'aliquot internal use fb_model_description', 1, 15),
                                                                                                                                                                            ('inventory action', 'InventoryManagement', 'InventoryActionControl', 'InventoryActionMaster', 'InventoryActionDetail', 'inventory_action_masters', 'inventory_action_controls', 'form_builder_inventory_action', 'inventory_action_masters', 'inventory_use_event fb_control_description', 1, 16);


UPDATE `form_builders` SET `options` = '{"copyTo":["view_collection", "linked_collections"]}' WHERE `model` = 'Collection';
    -- </editor-fold>
    -- </editor-fold>

    -- <editor-fold desc="Create form_builder_index alias">
-- -------------------------------------------------------------------------------------
--	Create form_builder_index alias
-- -------------------------------------------------------------------------------------
INSERT INTO structures(`alias`) VALUES ('form_builder_index');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('Administrate', 'FormBuilder', 'form_builders', 'label', 'input',  NULL, '0', '', '', '', 'form', ''),
    ('Administrate', 'FormBuilder', 'form_builders', 'note', 'textarea',  NULL, '0', '', '', '', 'note', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_index'),
     (SELECT id FROM structure_fields WHERE `model`='FormBuilder' AND `tablename`='form_builders' AND `field`='label' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='form' AND `language_tag`=''),
     '0', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_index'),
     (SELECT id FROM structure_fields WHERE `model`='FormBuilder' AND `tablename`='form_builders' AND `field`='note' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='note' AND `language_tag`=''),
     '0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');


-- -------------------------------------------------------------------------------------
--	Value domain field type
-- -------------------------------------------------------------------------------------

INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("form_builder_type_list", "", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("input", "input");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="input" AND language_alias="input"), "5", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("textarea", "textarea");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="textarea" AND language_alias="textarea"), "10", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("select", "select");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="select" AND language_alias="select"), "12", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("integer", "integer-type");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="integer" AND language_alias="integer-type"), "15", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("float1", "float1");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="float1" AND language_alias="float1"), "20", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("float2", "float2");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="float2" AND language_alias="float2"), "25", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("float5", "float5");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="float5" AND language_alias="float5"), "30", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("checkbox", "checkbox");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="checkbox" AND language_alias="checkbox"), "35", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("yes_no", "yes_no");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="yes_no" AND language_alias="yes_no"), "40", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("y_n_u", "yes no unknown");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="y_n_u" AND language_alias="yes no unknown"), "45", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("date", "date");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="date" AND language_alias="date"), "50", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("datetime", "datetime");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="datetime" AND language_alias="datetime"), "55", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("time", "time");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="time" AND language_alias="time"), "60", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("validateIcd10WhoCode", "validateIcd10WhoCode");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="validateIcd10WhoCode" AND language_alias="validateIcd10WhoCode"), "65", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("validateIcdo3MorphoCode", "validateIcdo3MorphoCode");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="validateIcdo3MorphoCode" AND language_alias="validateIcdo3MorphoCode"), "70", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("validateIcdo3TopoCode", "validateIcdo3TopoCode");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="validateIcdo3TopoCode" AND language_alias="validateIcdo3TopoCode"), "75", "1");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value=" " AND language_alias=" "), "1", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("hidden", "hidden");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active, use_as_input) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="hidden" AND language_alias="hidden"), "100", "1", "0");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("float", "float");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active, use_as_input) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="float" AND language_alias="float"), "101", "1", "0");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("password", "password");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active, use_as_input) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="password" AND language_alias="password"), "102", "1", "0");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("integer_positive", "integer_positive");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active, use_as_input) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="integer_positive" AND language_alias="integer_positive"), "103", "1", "0");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("float_positive", "float_positive");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active, use_as_input) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="float_positive" AND language_alias="float_positive"), "104", "1", "0");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("display", "display");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active, use_as_input) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="display" AND language_alias="display"), "105", "1", "0");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("autocomplete", "autocomplete");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active, use_as_input) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="autocomplete" AND language_alias="autocomplete"), "106", "1", "0");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("input-readonly", "input-readonly");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active, use_as_input) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="input-readonly" AND language_alias="input-readonly"), "107", "1", "0");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("button", "button");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active, use_as_input) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="button" AND language_alias="button"), "108", "1", "0");




    -- </editor-fold>

    -- <editor-fold desc="Form builder control alias ">
-- -------------------------------------------------------------------------------------
--	Form builder control alias
-- -------------------------------------------------------------------------------------
INSERT INTO structures(`alias`) VALUES ('form_builder_structure');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) VALUES
-- ('Administrate', 'Structure', 'structures', 'id', 'hidden',  NULL, '0', '', '', '', 'id', ''),
('Administrate', 'StructureField', 'structure_fields', 'language_label', 'input',  NULL, '0', 'class=pasteDisabled', '', 'language_label_help', 'language_label', ''),
('Administrate', 'StructureField', 'structure_fields', 'id', 'hidden',  NULL, '0', '', '', '', 'id', ''),
('Administrate', 'StructureField', 'structure_fields', 'setting', 'hidden',  NULL , '0', '', '', '', 'setting', ''),
('Administrate', 'StructureField', 'structure_fields', 'flag_copy_from_form_builder', 'hidden',  NULL, '0', '', '', '', 'flag_copy_from_form_builder', ''),
('Administrate', 'StructureField', 'structure_fields', 'type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='form_builder_type_list'), '0', 'class=fb_type_select,class=pasteDisabled', '', 'language_field_type_help', 'field type', ''),
('Administrate', 'StructureField', 'structure_fields', 'flag_confidential', 'checkbox',  NULL, '0', 'class=pasteDisabled', '', 'flag_confidential_help', 'flag_confidential', ''),
('Core', 'FunctionManagement', '', 'is_structure_value_domain', 'button',  NULL, '0', 'class=fb_is_structure_value_domain_input', '', 'is_structure_value_domain_help', 'is_structure_value_domain', ''),
('Administrate', 'StructureFormat', 'structure_formats', 'flag_add', 'checkbox',  NULL, '0', '', '1', 'flag_add_help', 'flag_add', ''),
('Administrate', 'StructureFormat', 'structure_formats', 'id', 'hidden',  NULL, '0', '', '', '', 'id', ''),
('Administrate', 'StructureFormat', 'structure_formats', 'flag_edit', 'checkbox',  NULL, '0', '', '1', 'flag_edit_help', 'flag_edit', ''),
('Administrate', 'StructureFormat', 'structure_formats', 'flag_search', 'checkbox',  NULL, '0', '', '1', 'flag_search_help', 'flag_search', ''),
('Administrate', 'StructureFormat', 'structure_formats', 'flag_index', 'checkbox',  NULL, '0', '', '1', 'flag_index_help', 'flag_index', ''),
('Administrate', 'StructureFormat', 'structure_formats', 'flag_detail', 'checkbox',  NULL, '0', '', '1', 'flag_detail_help', 'flag_detail', ''),
('Administrate', 'StructureFormat', 'structure_formats', 'language_heading', 'input',  NULL, '0', 'class=pasteDisabled', '', 'language_heading_help', 'language_heading', ''),
('Administrate', 'StructureFormat', 'structure_formats', 'display_column', 'integer_positive',  NULL, '0', 'class=pasteDisabled', '1', 'display_column_help', 'display_column', ''),
('Administrate', 'StructureFormat', 'structure_formats', 'display_order', 'integer_positive',  NULL, '0', 'class=pasteDisabled', '1', 'display_order_help', 'display_order', ''),
('Administrate', 'StructureField', 'structure_fields', 'language_help', 'textarea',  NULL, '0', 'class=pasteDisabled', '', 'language_help_help', 'language_help', ''),
('Core', 'FunctionManagement', '', 'fb_has_validation', 'button',  NULL, '0', 'class=fb_has_validation', '', 'has_validation_help', 'has_validation', '');



INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES
-- ((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='Structure' AND `tablename`='structures' AND `field`='id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='id' AND `language_tag`=''), '1', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='language_label' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='language_label_help' AND `language_label`='language_label' AND `language_tag`=''), '1', '10', 'general information', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '1'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='id' AND `language_tag`=''), '1', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='flag_copy_from_form_builder' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='flag_copy_from_form_builder' AND `language_tag`=''), '1', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='setting' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='setting' AND `language_tag`=''), '1', '13', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='form_builder_type_list')  AND `flag_confidential`='0' AND `setting`='class=fb_type_select,class=pasteDisabled' AND `default`='' AND `language_help`='language_field_type_help' AND `language_label`='field type' AND `language_tag`=''), '1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='flag_confidential' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='flag_confidential_help' AND `language_label`='flag_confidential' AND `language_tag`=''), '1', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='is_structure_value_domain' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=fb_is_structure_value_domain_input' AND `default`='' AND `language_help`='is_structure_value_domain_help' AND `language_label`='is_structure_value_domain' AND `language_tag`=''), '1', '45', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_add' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_add_help' AND `language_label`='flag_add' AND `language_tag`=''), '1', '50', 'display according to form type', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='id' AND `language_tag`=''), '1', '55', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_edit' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_edit_help' AND `language_label`='flag_edit' AND `language_tag`=''), '1', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_search' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_search_help' AND `language_label`='flag_search' AND `language_tag`=''), '1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_index' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_index_help' AND `language_label`='flag_index' AND `language_tag`=''), '1', '80', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_detail' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_detail_help' AND `language_label`='flag_detail' AND `language_tag`=''), '1', '90', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='language_heading' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='language_heading_help' AND `language_label`='language_heading' AND `language_tag`=''), '1', '100', 'layout format', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='display_column' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='1' AND `language_help`='display_column_help' AND `language_label`='display_column' AND `language_tag`=''), '1', '110', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='display_order' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='1' AND `language_help`='display_order_help' AND `language_label`='display_order' AND `language_tag`=''), '1', '120', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='language_help' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='language_help_help' AND `language_label`='language_help' AND `language_tag`=''), '1', '125', 'other', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='fb_has_validation' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=fb_has_validation' AND `default`='' AND `language_help`='has_validation_help' AND `language_label`='has_validation' AND `language_tag`=''), '1', '130', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='form_builder_structure'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='CopyCtrl' AND `type`='checkbox' AND `structure_value_domain` IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='copy control' AND `language_tag`=''), '10', '10000', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0');

-- -------------------------------------------------------------------------------------
--	Validation rules
-- -------------------------------------------------------------------------------------

INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES
                                                                                           ((SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='language_label' AND `type` = 'input' AND `structure_value_domain`  IS NULL AND `language_label` = 'language_label'),'notBlank', 'field label is required'),
                                                                                           ((SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='form_builder_type_list')),'notBlank', 'field type is required'),
                                                                                           ((SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='is_structure_value_domain' AND `type`='button' AND `structure_value_domain`  IS NULL ),'notBlank', 'value/domain is required');
    -- </editor-fold>

    -- <editor-fold desc="Create some aliases">

-- -------------------------------------------------------------------------------------
--	Form Builder Validation alias
-- -------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('form_builder_validation');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) VALUES
                                                                                                                                                                                                          ('Core', 'FunctionManagement', '', 'is_unique', 'checkbox',  NULL, '0', '', '', 'is_unique_help', 'is_unique', ''),
                                                                                                                                                                                                          ('Core', 'FunctionManagement', '', 'not_blank', 'checkbox',  NULL, '0', '', '', 'not_blank_help', 'not_blank', ''),
                                                                                                                                                                                                          ('Core', 'FunctionManagement', '', 'range_from', 'integer_positive',  NULL, '0', '', '', '', 'range_from', ''),
                                                                                                                                                                                                          ('Core', 'FunctionManagement', '', 'range_to', 'integer_positive',  NULL, '0', '', '', 'range_help', '', 'range_to'),
                                                                                                                                                                                                          ('Core', 'FunctionManagement', '', 'between_from', 'integer_positive',  NULL, '0', '', '', '', 'between_from', ''),
                                                                                                                                                                                                          ('Core', 'FunctionManagement', '', 'between_to', 'integer_positive',  NULL, '0', '', '', 'between_help', '', 'between_to');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_validation'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='is_unique' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='is_unique_help' AND `language_label`='is_unique' AND `language_tag`=''), '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_validation'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='not_blank' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='not_blank_help' AND `language_label`='not_blank' AND `language_tag`=''), '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_validation'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='range_from' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='range_from' AND `language_tag`=''), '1', '3', 'range_heading', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_validation'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='range_to' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='range_help' AND `language_label`='' AND `language_tag`='range_to'), '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_validation'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='between_from' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='between_from' AND `language_tag`=''), '1', '5', 'between_heading', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_validation'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='between_to' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='between_help' AND `language_label`='' AND `language_tag`='between_to'), '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- -------------------------------------------------------------------------------------
--	Form Builder Value Domain alias
-- -------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('form_builder_value_domain');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) VALUES
                                                                                                                                                                                                          ('Administrate', 'StructureValueDomain', 'structure_value_domains', 'domain_name', 'autocomplete',  NULL, '0', 'url=/Administrate/FormBuilders/autocompleteDropDownList,class=form-builder-autocomplete', '', 'list_name_help', 'list name', ''),
                                                                                                                                                                                                          ('Administrate', 'StructureValueDomain', '', 'flag_multi_select', 'checkbox',  NULL , '0', 'class=is-multi-select', '', 'multi_select_help', 'multi select', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_value_domain'), (SELECT id FROM structure_fields WHERE `model`='StructureValueDomain' AND `tablename`='structure_value_domains' AND `field`='domain_name' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='url=/Administrate/FormBuilders/autocompleteDropDownList,class=form-builder-autocomplete' AND `default`='' AND `language_help`='list_name_help' AND `language_label`='list name' AND `language_tag`=''), '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_value_domain'), (SELECT id FROM structure_fields WHERE `model`='StructureValueDomain' AND `tablename`='' AND `field`='flag_multi_select' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=is-multi-select' AND `default`='' AND `language_help`='multi_select_help' AND `language_label`='multi select' AND `language_tag`=''), '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0');


INSERT IGNORE INTO
    i18n (id,en,fr)
VALUES
    ('custom drop down list name', 'Custom Drop Down List Name', 'Nom de la liste personnalisée');

-- -------------------------------------------------------------------------------------
--	change the length of the category column
-- -------------------------------------------------------------------------------------

ALTER TABLE `structure_permissible_values_custom_controls`
    CHANGE `category` `category` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT 'undefined' AFTER `values_max_length`;
    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="All Controllers">

        -- <editor-fold defaultstate="collapsed" desc="Clicical Collections">

            -- <editor-fold defaultstate="collapsed" desc="Consent Control Control">
-- -------------------------------------------------------------------------------------
--	Add some columns to Consent_controlls
-- -------------------------------------------------------------------------------------

ALTER TABLE `consent_controls`
    ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_active_input` tinyint(1) NOT NULL DEFAULT '1',
CHANGE `databrowser_label` `databrowser_label` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '' AFTER `display_order`,
CHANGE `controls_type` `controls_type` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `id`;

-- -------------------------------------------------------------------------------------
--	Create form_builder_consent alias
-- -------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('form_builder_consent');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('ClinicalAnnotation', 'ConsentControl', 'consent_controls', 'controls_type', 'input',  NULL, '0', 'class=control-name', '', '', 'consent type', ''),
    ('ClinicalAnnotation', 'ConsentControl', 'consent_controls', 'flag_active', 'checkbox',  NULL, '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('ClinicalAnnotation', 'ConsentControl', 'consent_controls', 'flag_active_input', 'checkbox',  NULL, '0', '', '', 'help_flag_active_input_fb', 'flag active as input', ''),
    ('ClinicalAnnotation', 'ConsentControl', 'consent_controls', 'flag_test_mode', 'checkbox',  NULL, '0', '', '', 'test mode help', 'test mode', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_consent'),
     (SELECT id FROM structure_fields WHERE `model`='ConsentControl' AND `tablename`='consent_controls' AND `field`='controls_type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=control-name' AND `default`='' AND `language_help`='' AND `language_label`='consent type' AND `language_tag`=''),
     '1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_consent'),
     (SELECT id FROM structure_fields WHERE `model`='ConsentControl' AND `tablename`='consent_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '30', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_consent'),
     (SELECT id FROM structure_fields WHERE `model`='ConsentControl' AND `tablename`='consent_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_consent'),
     (SELECT id FROM structure_fields WHERE `model`='ConsentControl' AND `tablename`='consent_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');


INSERT INTO structure_validations (`structure_field_id`, `rule`, `language_message`)
VALUES
-- ((SELECT id FROM structure_fields WHERE `model`='ConsentControl' AND `tablename`='consent_controls' AND `field`='controls_type' AND `type` = 'input' AND structure_value_domain IS NULL), 'isUnique', 'consent type should be unique'),
((SELECT id FROM structure_fields WHERE `model`='ConsentControl' AND `tablename`='consent_controls' AND `field`='controls_type' AND `type` = 'input' AND structure_value_domain IS NULL), 'notBlank', 'consent type is required');

            -- </editor-fold>

            -- <editor-fold defaultstate="collapsed" desc="Diagnosis Control">
-- -------------------------------------------------------------------------------------
--	Add some columns to Diagnosis_controlls
-- -------------------------------------------------------------------------------------
ALTER TABLE `diagnosis_controls`
    ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_active_input` tinyint(1) NOT NULL DEFAULT '1',
CHANGE `databrowser_label` `databrowser_label` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '' AFTER `display_order`,
CHANGE `controls_type` `controls_type` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `id`;

-- -------------------------------------------------------------------------------------
--	Create form_builder_diagnosis alias
-- -------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('form_builder_diagnosis');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('ClinicalAnnotation', 'DiagnosisControl', 'diagnosis_controls', 'controls_type', 'input',  NULL, '0', 'class=control-name', '', '', 'diagnosis type', ''),
    ('ClinicalAnnotation', 'DiagnosisControl', 'diagnosis_controls', 'databrowser_label', 'input',  NULL, '0', '', '', 'databrowser label help', 'databrowser label', ''),
    ('ClinicalAnnotation', 'DiagnosisControl', 'diagnosis_controls', 'flag_active', 'checkbox',  NULL, '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('ClinicalAnnotation', 'DiagnosisControl', 'diagnosis_controls', 'display_order', 'integer',  NULL, '0', '', '', '', 'display order', ''),
    ('ClinicalAnnotation', 'DiagnosisControl', 'diagnosis_controls', 'flag_test_mode', 'checkbox',  NULL, '0', '', '', 'test mode help', 'test mode', ''),
    ('ClinicalAnnotation', 'DiagnosisControl', 'diagnosis_controls', 'flag_active_input', 'checkbox',  NULL, '0', '', '', 'help_flag_active_input_fb', 'flag active as input', ''),
    ('ClinicalAnnotation', 'DiagnosisControl', 'diagnosis_controls', 'flag_compare_with_cap', 'checkbox',  NULL, '0', '', '', '', 'flag_compare_with_cap', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_diagnosis'),
     (SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='controls_type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=control-name' AND `default`='' AND `language_help`='' AND `language_label`='diagnosis type' AND `language_tag`=''),
     '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_diagnosis'),
     (SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='databrowser_label' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='databrowser label help' AND `language_label`='databrowser label' AND `language_tag`=''),
     '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_diagnosis'),
     (SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='category' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='diagnosis_category')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='category' AND `language_tag`=''),
     '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '1', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_diagnosis'),
     (SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_diagnosis'),
     (SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='display_order' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='display order' AND `language_tag`=''),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_diagnosis'),
     (SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_diagnosis'),
     (SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_diagnosis'),
     (SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='flag_compare_with_cap' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='flag_compare_with_cap' AND `language_tag`=''),
     '1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');



INSERT INTO structure_validations (`structure_field_id`, `rule`, `language_message`)
VALUES
    ((SELECT id FROM structure_fields WHERE `type` = 'input' AND `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='controls_type' AND `structure_value_domain` IS NULL), 'notBlank', ''),
-- ((SELECT id FROM structure_fields WHERE `type` = 'input' AND  `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='controls_type' AND `structure_value_domain` IS NULL), 'isUnique', ''),
    ((SELECT id FROM structure_fields WHERE `type` = 'select' AND `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='category' AND `structure_value_domain` = (SELECT id FROM structure_value_domains WHERE domain_name='diagnosis_category')), 'notBlank', '');

-- -------------------------------------------------------------------------------------
--	i18N for Diagnosis
-- -------------------------------------------------------------------------------------

INSERT IGNORE INTO
    i18n (id,en,fr)
VALUES
    ('dx-category', "Diagnosis Category", "Catégorie de diagnostic"),
    ('diagnosis type', "Diagnosis name", "Nom du diagnostic");

--     ('', "", ""),

            -- </editor-fold>

            -- <editor-fold defaultstate="collapsed" desc="Event Control">

-- -------------------------------------------------------------------------------------
--	Add some columns to Event_controlls
-- -------------------------------------------------------------------------------------

ALTER TABLE `event_controls`
    ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_active_input` tinyint(1) NOT NULL DEFAULT '1',
CHANGE `databrowser_label` `databrowser_label` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '' AFTER `display_order`,
CHANGE `event_type` `event_type` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `id`;

-- -------------------------------------------------------------------------------------
--	Create form_builder_consent alias
-- -------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('form_builder_event');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'event_type', 'input',  NULL, '0', 'class=control-name', '', '', 'event_type_label', ''),
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'disease_site', 'input',  NULL, '0', '', '', 'disease_site_help', 'disease_site_label', ''),
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'display_order', 'integer',  NULL, '0', '', '', '', 'display order', ''),
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'flag_use_for_ccl', 'checkbox',  NULL, '0', '', '', 'help_flag_use_for_ccl', 'flag_use_for_ccl_label', ''),
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'flag_test_mode', 'checkbox',  NULL, '0', '', '', 'test mode help', 'test mode', ''),
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'flag_active', 'checkbox',  NULL, '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'flag_active_input', 'checkbox',  NULL, '0', '', '', 'help_flag_active_input_fb', 'flag active as input', ''),
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'databrowser_label', 'input',  NULL, '0', '', '', 'databrowser label help', 'databrowser label', ''),
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'use_addgrid', 'checkbox',  NULL, '0', '', '', 'help_use_addgrid', 'use_addgrid_label', ''),
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'use_detail_form_for_index', 'checkbox',  NULL, '0', '', '', 'help_use_detail_form_for_index', 'use_detail_form_for_index_label', '');


INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='event_type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=control-name' AND `default`='' AND `language_help`='' AND `language_label`='event_type_label' AND `language_tag`=''),
     '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='event_group' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='event_group_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='event_group' AND `language_tag`=''),
     '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '1', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='disease_site' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='disease_site_help' AND `language_label`='disease_site_label' AND `language_tag`=''),
     '1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='display_order' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='display order' AND `language_tag`=''),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='flag_use_for_ccl' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_use_for_ccl' AND `language_label`='flag_use_for_ccl_label' AND `language_tag`=''),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '75', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '80', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='databrowser_label' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='databrowser label help' AND `language_label`='databrowser label' AND `language_tag`=''),
     '1', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='use_addgrid' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_use_addgrid' AND `language_label`='use_addgrid_label' AND `language_tag`=''),
     '1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_event'),
     (SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='use_detail_form_for_index' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_use_detail_form_for_index' AND `language_label`='use_detail_form_for_index_label' AND `language_tag`=''),
     '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');




INSERT INTO structure_validations (`structure_field_id`, `rule`, `language_message`)
VALUES
    ((SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='event_type' AND `type`='input' AND `structure_value_domain`  IS NULL), 'notBlank', ''),
-- ((SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='event_type' AND `type`='input' AND `structure_value_domain`  IS NULL), 'isUnique', ''),
    ((SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='event_group' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='event_group_list')), 'notBlank', '');

UPDATE `structure_fields` SET `language_help` = 'help_event_group' WHERE
        `model`='EventControl' AND `tablename`='event_controls' AND `field`='event_group' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='event_group_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='event_group' AND `language_tag`='';
-- -------------------------------------------------------------------------------------
--	i18N for Event
-- -------------------------------------------------------------------------------------

INSERT IGNORE INTO
i18n (id,en,fr)
VALUES
('event_type_label', "Event name", "Nom de l'événement"),
('event_type_help', "Name of the clinical event that will let user identify the event.", "Nom de l'événement clinique qui permettra à l'utilisateur d'identifier l'événement."),
('event_group_label', "Event Group", "Groupe de l'évenement"),
('help_event_group', "Category of the event that will let the system classify all the events of the participants per group (clinical, lab, lifestyle, etc).", "Catégorie de l'événement qui permettra au système de classer tous les événements des participants par groupe (clinique, laboratoire, mode de vie, etc.)."),
('disease_site_label', "Disease/Site", "Maladie/Site"),
('disease_site_help', "Optional. Defines if the event form has been created specifically for one type of disease (breast - CT Scan, Lung - CT Scan, etc) meaning it gather fields related to the clinical data clinicans track for this disease.", "Optionnel. Définit si le formulaire d’événement a été créé spécifiquement pour un type de maladie (scanner mammaire, scanner pulmonaire, scanner pulmonaire, etc.), ce qui signifie qu’il regroupe les champs liés aux données cliniques suivies par les cliniciens pour cette maladie."),
('flag_use_for_ccl_label', "Linkable to collection", "Pouvant être lié à la collection"),
('help_flag_use_for_ccl', "When user can link a collection to a specific clinical event (a pathology report linked to a tissue collection, etc.), this option will let to add (or not) the event in the list of selectable events during the collection creation.", "Lorsque l'utilisateur peut lier une collection à un événement clinique spécifique (rapport de pathologie lié à une collection de tissus, etc.), cette option permet d'ajouter (ou non) l'événement dans la liste des événements sélectionnables lors de la création de la collection."),
('use_addgrid_label', "Batch Creation", "Création en lot"),
('help_use_addgrid', "Will let user to create more than one event for the participant in one click.", "Permettra à l'utilisateur de créer plus d'un événement pour le participant en un clic."),
('clinical event', "Clinical Event", "Événement clinique"),
('use_detail_form_for_index_label', "Display in separated list", "Afficher dans liste séparée"),
('help_use_detail_form_for_index', "When user is showing all the clinical events of a participant, all the events with this type will be displayed in a separated list with all the fields of the form and removed from the list with all other events with a limited set of fields (common to all form types).", "Lorsque l'utilisateur affiche tous les événements cliniques d'un participant, tous les événements de ce type sont affichés dans une liste séparée avec tous les champs du formulaire et supprimés de la liste avec tous les autres événements avec un ensemble limité de champs (communs à tous les types de formulaire).");

            -- </editor-fold>

            -- <editor-fold defaultstate="collapsed" desc="Treatment Control">

-- -------------------------------------------------------------------------------------
--	Add some columns to Treatment_controlls
-- -------------------------------------------------------------------------------------

ALTER TABLE `treatment_controls`
    ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_active_input` tinyint(1) NOT NULL DEFAULT '1',
CHANGE `databrowser_label` `databrowser_label` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '' AFTER `display_order`,
CHANGE `tx_method` `tx_method` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `id`;

-- -------------------------------------------------------------------------------------
--	Create Functional value domain for treatment_extend_control_id & protocol_id
-- -------------------------------------------------------------------------------------
INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('tx_precision_id', 'open', '', 'ClinicalAnnotation.TreatmentExtendControl::getPrecisionIdValues');
INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('protocol id', 'open', '', 'Protocol.ProtocolControl::getProtocolIdPermissibleValues');

-- -------------------------------------------------------------------------------------
--	Create form_builder_treatment alias
-- -------------------------------------------------------------------------------------
INSERT INTO structures(`alias`) VALUES ('form_builder_treatment');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'tx_method', 'input',  NULL , '0', 'class=control-name', '', '', 'treatment_method_label', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'disease_site', 'input',  NULL , '0', '', '', 'disease_site_help', 'disease_site_label', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'applied_protocol_control_id', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='protocol id') , '0', '', '', 'applied_protocol_control_id_help', 'applied_protocol_control_id_label', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'display_order', 'integer',  NULL , '0', '', '', '', 'display order', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'treatment_extend_control_id', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='tx_precision_id') , '0', '', '', 'treatment_extend_control_id_help', 'treatment_extend_control_id_label', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'flag_use_for_ccl', 'checkbox',  NULL , '0', '', '', 'help_flag_use_for_ccl', 'flag_use_for_ccl_label', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'use_addgrid', 'checkbox',  NULL , '0', '', '', 'help_use_addgrid', 'use_addgrid_label', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'use_detail_form_for_index', 'checkbox',  NULL , '0', '', '', 'help_use_detail_form_for_index', 'use_detail_form_for_index_label', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'flag_test_mode', 'checkbox',  NULL , '0', '', '', 'test mode help', 'test mode', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'flag_active', 'checkbox',  NULL , '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'flag_active_input', 'checkbox',  NULL , '0', '', '', 'help_flag_active_input_fb', 'flag active as input', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'databrowser_label', 'input',  NULL , '0', '', '', 'databrowser label help', 'databrowser label', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='tx_method' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=control-name' AND `default`='' AND `language_help`='' AND `language_label`='treatment_method_label' AND `language_tag`=''),
     '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='disease_site' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='disease_site_help' AND `language_label`='disease_site_label' AND `language_tag`=''),
     '1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='applied_protocol_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='protocol id')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='applied_protocol_control_id_help' AND `language_label`='applied_protocol_control_id_label' AND `language_tag`=''),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='display_order' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='display order' AND `language_tag`=''),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='treatment_extend_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='tx_precision_id')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='treatment_extend_control_id_help' AND `language_label`='treatment_extend_control_id_label' AND `language_tag`=''),
     '1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_use_for_ccl' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_use_for_ccl' AND `language_label`='flag_use_for_ccl_label' AND `language_tag`=''),
     '1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='use_addgrid' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_use_addgrid' AND `language_label`='use_addgrid_label' AND `language_tag`=''),
     '1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='use_detail_form_for_index' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_use_detail_form_for_index' AND `language_label`='use_detail_form_for_index_label' AND `language_tag`=''),
     '1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '75', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '80', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='databrowser_label' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='databrowser label help' AND `language_label`='databrowser label' AND `language_tag`=''),
     '1', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

INSERT INTO structure_validations (`structure_field_id`, `rule`, `language_message`)
VALUES
    ((SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='tx_method' AND `type`='input' AND `structure_value_domain`  IS NULL), 'notBlank', '');

-- -------------------------------------------------------------------------------------
--	i18N for Treatment
-- -------------------------------------------------------------------------------------

INSERT IGNORE INTO
    i18n (id,en,fr)
VALUES
    ('treatment_method_label', "Treatment Method", "Méthode de Traitement"),
    ('applied_protocol_control_id_label', "Treatment Protocol", "Protocole de Traitement"),
    ('treatment_extend_control_id_help', "Optional, Determine the name of the treatment precision that is related to this treatment.<br><b>Note: </b>The treatment precision should be already defined.", "Facultatif, Déterminez le nom du traitement de précision associée à ce traitement.<br><b>Remarque: </b> Le traitement de précision doit déjà être définie."),
    ('applied_protocol_control_id_help', "Optional, Determine the name of the protocol that is related to this treatment.<br><b>Note: </b>The protocol should be allready defined.", "Facultatif, Déterminez le nom de protocol associée à ce traitement.<br><b>Remarque: </b> Le protocol doit déjà être définie."),
    ('treatment_extend_control_id_label', "Treatment Precision", "Traitement de précision");

--     ('', "", ""),
            -- </editor-fold>

            -- <editor-fold defaultstate="collapsed" desc="Treatment Extend Control">
-- -------------------------------------------------------------------------------------
--	Add some columns to treatment_extend_controls
-- -------------------------------------------------------------------------------------

ALTER TABLE `treatment_extend_controls`
    ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_active_input` tinyint(1) NOT NULL DEFAULT '1',
CHANGE `databrowser_label` `databrowser_label` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '',
CHANGE `type` `type` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `id`;

-- -------------------------------------------------------------------------------------
--	Create form_builder_treatment_extend alias
-- -------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('form_builder_treatment_extend');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('ClinicalAnnotation', 'TreatmentExtendControl', 'treatment_extend_controls', 'type', 'input',  NULL, '0', 'class=control-name', '', '', 'treatment_extend_control_type', ''),
    ('ClinicalAnnotation', 'TreatmentExtendControl', 'treatment_extend_controls', 'flag_test_mode', 'checkbox',  NULL, '0', '', '', 'test mode help', 'test mode', ''),
    ('ClinicalAnnotation', 'TreatmentExtendControl', 'treatment_extend_controls', 'flag_active', 'checkbox',  NULL, '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('ClinicalAnnotation', 'TreatmentExtendControl', 'treatment_extend_controls', 'flag_active_input', 'checkbox',  NULL, '0', '', '', 'help_flag_active_input_fb', 'flag active as input', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_extend'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentExtendControl' AND `tablename`='treatment_extend_controls' AND `field`='type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=control-name' AND `default`='' AND `language_help`='' AND `language_label`='treatment_extend_control_type' AND `language_tag`=''),
     '1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_extend'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentExtendControl' AND `tablename`='treatment_extend_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_extend'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentExtendControl' AND `tablename`='treatment_extend_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '30', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_extend'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentExtendControl' AND `tablename`='treatment_extend_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');


INSERT INTO structure_validations (`structure_field_id`, `rule`, `language_message`)
VALUES
-- ((SELECT id FROM structure_fields WHERE `model`='TreatmentExtendControl' AND `tablename`='treatment_extend_controls' AND `field`='type' AND `type` = 'input' AND structure_value_domain IS NULL), 'isUnique', ''),
((SELECT id FROM structure_fields WHERE `model`='TreatmentExtendControl' AND `tablename`='treatment_extend_controls' AND `field`='type' AND `type` = 'input' AND structure_value_domain IS NULL), 'notBlank', '');

-- -------------------------------------------------------------------------------------
--	i18N for Treatment Extend
-- -------------------------------------------------------------------------------------

INSERT IGNORE INTO
i18n (id,en,fr)
VALUES
('treatment extend', "Treatment Precision", "Traitement de Précision"),
('treatment_extend_control_type', "Treatment precision name", "Nom de traitement du précision");

-- ('', "", ""),


            -- </editor-fold>

            -- <editor-fold defaultstate="collapsed" desc="Misc identifier">
ALTER TABLE `misc_identifier_controls`
    ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_active_input` tinyint(1) NOT NULL DEFAULT '1',
CHANGE `misc_identifier_name` `misc_identifier_name` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `id`;

-- -------------------------------------------------------------------------------------
--	Create form_builder_misc_identifier alias
-- -------------------------------------------------------------------------------------
INSERT INTO structures(`alias`) VALUES ('form_builder_misc_identifier');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('ClinicalAnnotation', 'MiscIdentifierControl', 'misc_identifier_controls', 'flag_test_mode', 'checkbox',  NULL , '0', '', '', 'test mode help', 'test mode', ''),
    ('ClinicalAnnotation', 'MiscIdentifierControl', 'misc_identifier_controls', 'flag_active', 'checkbox',  NULL , '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('ClinicalAnnotation', 'MiscIdentifierControl', 'misc_identifier_controls', 'flag_active_input', 'checkbox',  NULL , '0', '', '', 'help_flag_active_input_fb', 'flag active as input', ''),
    ('ClinicalAnnotation', 'MiscIdentifierControl', 'misc_identifier_controls', 'flag_once_per_participant', 'checkbox',  NULL , '0', '', '', '', 'flag_once_per_participant_label', ''),
    ('ClinicalAnnotation', 'MiscIdentifierControl', 'misc_identifier_controls', 'flag_confidential', 'checkbox',  NULL , '0', '', '', '', 'flag_confidential_label', ''),
    ('ClinicalAnnotation', 'MiscIdentifierControl', 'misc_identifier_controls', 'flag_unique', 'checkbox',  NULL , '0', '', '', '', 'flag_confidential_flag_unique', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_misc_identifier'),
     (SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_misc_identifier'),
     (SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_misc_identifier'),
     (SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_misc_identifier'),
     (SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='misc_identifier_name' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '1', '1', '', '0', '1', 'misc_identifier_name_label', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_misc_identifier'),
     (SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='flag_once_per_participant' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='flag_once_per_participant_label' AND `language_tag`=''),
     '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_misc_identifier'),
     (SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='flag_confidential' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='flag_confidential_label' AND `language_tag`=''),
     '1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_misc_identifier'),
     (SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='flag_unique' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='flag_confidential_flag_unique' AND `language_tag`=''),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');


INSERT INTO structure_validations (`structure_field_id`, `rule`, `language_message`)
VALUES
    ((SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='misc_identifier_name' AND `type` = 'input' AND structure_value_domain IS NULL), 'notBlank', '');

-- -------------------------------------------------------------------------------------
--	i18N for Misc Identifier
-- -------------------------------------------------------------------------------------
INSERT IGNORE INTO
i18n (id,en,fr)
VALUES
('misc_identifier_name_label', "Identifier name", "Nom de l'identifiant"),
('flag_once_per_participant_label', "One per participant", "Un par participant"),
('flag_confidential_label', "Confidential", "Confidentiel"),
('flag_confidential_flag_unique', "Unique", "Unique"),
('misc identifier fb_control_description', "Create new participant identifier or modify the properties of existing ones.", "Créez un nouvel identifiant de participant ou modifiez les propriétés des identifiants existants.");

            -- </editor-fold>

        -- </editor-fold>

        -- <editor-fold defaultstate="collapsed" desc="Inventory">
            -- <editor-fold defaultstate="collapsed" desc="Sample">
-- -------------------------------------------------------------------------------------
--	Add some columns to sample_controls
-- -------------------------------------------------------------------------------------

ALTER TABLE `sample_controls`
    ADD `flag_active` tinyint(1) NOT NULL DEFAULT '1',
ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_active_input` tinyint(1) NOT NULL DEFAULT '1',
CHANGE `sample_type` `sample_type` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL AFTER `id`;


-- -------------------------------------------------------------------------------------
--	Create structure_value_domains sample_type
-- -------------------------------------------------------------------------------------
INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('sample_type_fb', 'open', '', 'InventoryManagement.SampleControl::getAllSampleTypePermissibleValues');

-- -------------------------------------------------------------------------------------
--	Create form_builder_sample alias
-- -------------------------------------------------------------------------------------
INSERT INTO structures(`alias`) VALUES ('form_builder_sample');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'SampleControl', 'sample_controls', 'sample_type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='sample_type_fb') , '0', 'class=control-name', '', '', 'sample type label', ''),
    ('InventoryManagement', 'SampleControl', 'sample_controls', 'flag_test_mode', 'checkbox',  NULL , '0', '', '', 'test mode help', 'test mode', ''),
    ('InventoryManagement', 'SampleControl', 'sample_controls', 'flag_active', 'checkbox',  NULL , '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('InventoryManagement', 'SampleControl', 'sample_controls', 'flag_active_input', 'checkbox',  NULL , '0', '', '', 'help_flag_active_input_fb', 'flag active as input', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_sample'),
     (SELECT id FROM structure_fields WHERE `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='sample_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_type_fb')  AND `flag_confidential`='0' AND `setting`='class=control-name' AND `default`='' AND `language_help`='' AND `language_label`='sample type label' AND `language_tag`=''),
     '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_sample'),
     (SELECT id FROM structure_fields WHERE `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='sample_category' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_category')  AND `flag_confidential`='0'),
     '1', '2', '', '0', '1', 'inv_sample_category_defintion_label', '0', '', '1', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_sample'),
     (SELECT id FROM structure_fields WHERE `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_sample'),
     (SELECT id FROM structure_fields WHERE `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_sample'),
     (SELECT id FROM structure_fields WHERE `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');


-- -------------------------------------------------------------------------------------
--	i18N for Sample Control
-- -------------------------------------------------------------------------------------

INSERT IGNORE INTO
i18n (id,en,fr)
VALUES
('inv_sample_category_defintion_label', "Sample Category", "Catégorie d'échantillon"),
('sample type label', "Sample Type", "Type d'échantillon");

-- ('', "", ""),
            -- </editor-fold>

            -- <editor-fold defaultstate="collapsed" desc="Aliquot">
-- -------------------------------------------------------------------------------------
--	Add some columns to ali1uot_controls
-- -------------------------------------------------------------------------------------

ALTER TABLE `aliquot_controls`
    ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_active_input` tinyint(1) NOT NULL DEFAULT '1';

-- -------------------------------------------------------------------------------------
--	Create Sample Id's dropdown list
-- -------------------------------------------------------------------------------------
INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('sample_typeId_fb', 'open', '', 'InventoryManagement.SampleControl::getAllSampleIdPermissibleValues');

-- -------------------------------------------------------------------------------------
--	Create form_builder_aliquot alias
-- -------------------------------------------------------------------------------------
INSERT INTO structures(`alias`) VALUES ('form_builder_aliquot');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'AliquotControl', 'sample_controls', 'flag_test_mode', 'checkbox',  NULL , '0', '', '', 'test mode help', 'test mode', ''),
    ('InventoryManagement', 'AliquotControl', 'sample_controls', 'flag_active', 'checkbox',  NULL , '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('InventoryManagement', 'AliquotControl', 'aliquot_controls', 'flag_active_input', 'checkbox',  NULL , '0', '', '', 'help_flag_active_input_fb', 'flag active as input', ''),
    ('InventoryManagement', 'AliquotControl', 'aliquot_controls', 'sample_control_id', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='sample_typeId_fb') , '0', '', '', 'sample type help', 'sample type', ''),
    ('InventoryManagement', 'AliquotControl', 'aliquot_controls', 'comment', 'textarea',  NULL , '0', '', '', '', 'note', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='sample_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='sample_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='aliquot_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type')  AND `flag_confidential`='0'),
     '1', '1', '', '0', '0', '', '0', '', '1', 'aliquot type label help', '0', '', '1', 'class=control-name', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='sample_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_typeId_fb')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='sample type help' AND `language_label`='sample type' AND `language_tag`=''),
     '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0'),
     '1', '3', '', '0', '1', 'volume unit', '0', '', '1', 'volume unit help', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='comment' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='note' AND `language_tag`=''),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');




-- -------------------------------------------------------------------------------------
--	i18N for Sample Control
-- -------------------------------------------------------------------------------------

-- INSERT IGNORE INTO
-- i18n (id,en,fr)
-- VALUES
-- ('volume unit help', "", ""),
-- ('aliquot type label help', "", ""),
-- ('sample type help', "", "");

-- ('', "", ""),
            -- </editor-fold>

            -- <editor-fold defaultstate="collapsed" desc="SpecimenReview">
-- -- -------------------------------------------------------------------------------------
-- --	Add some columns to specimen_review_controls
-- -- -------------------------------------------------------------------------------------

ALTER TABLE `specimen_review_controls`
    ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_active_input` tinyint(1) NOT NULL DEFAULT '1',
ADD `display_order` int(11) NOT NULL DEFAULT '0' AFTER `detail_tablename`,
CHANGE `review_type` `review_type` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL;


-- -- -------------------------------------------------------------------------------------
-- --	Create Functional list for aliquot_review_controls
-- -- -------------------------------------------------------------------------------------
INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('aliquots_review_id_type', 'open', '', 'InventoryManagement.AliquotReviewControl::getAliquotReviewsControlIdType');
-- -- -------------------------------------------------------------------------------------
-- --	Create structure_value_domains sample_type
-- -- -------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('form_builder_specimen_review');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'SpecimenReviewControl', 'specimen_review_controls', 'review_type', 'input',  NULL , '0', 'class=control-name', '', '', 'specimen_review_type_label', ''),
    ('InventoryManagement', 'SpecimenReviewControl', 'specimen_review_controls', 'display_order', 'integer',  NULL , '0', '', '', '', 'display order', ''),
    ('InventoryManagement', 'SpecimenReviewControl', 'specimen_review_controls', 'sample_control_id', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type_from_id') , '0', '', '', 'sample_control_id_help', 'sample_control_id_label', ''),
    ('InventoryManagement', 'SpecimenReviewControl', 'specimen_review_controls', 'aliquot_review_control_id', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='aliquots_review_id_type') , '0', '', '', 'aliquot_review_control_id_help', 'aliquot_review_control_id_label', ''),
    ('InventoryManagement', 'SpecimenReviewControl', 'specimen_review_controls', 'flag_test_mode', 'checkbox',  NULL , '0', '', '', 'test mode help', 'test mode', ''),
    ('InventoryManagement', 'SpecimenReviewControl', 'specimen_review_controls', 'flag_active', 'checkbox',  NULL , '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('InventoryManagement', 'SpecimenReviewControl', 'specimen_review_controls', 'flag_active_input', 'checkbox',  NULL , '0', '', '', 'help_flag_active_input_fb', 'flag active as input', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_specimen_review'),
     (SELECT id FROM structure_fields WHERE `model`='SpecimenReviewControl' AND `tablename`='specimen_review_controls' AND `field`='review_type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=control-name' AND `default`='' AND `language_help`='' AND `language_label`='specimen_review_type_label' AND `language_tag`=''),
     '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_specimen_review'),
     (SELECT id FROM structure_fields WHERE `model`='SpecimenReviewControl' AND `tablename`='specimen_review_controls' AND `field`='display_order' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='display order' AND `language_tag`=''),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_specimen_review'),
     (SELECT id FROM structure_fields WHERE `model`='SpecimenReviewControl' AND `tablename`='specimen_review_controls' AND `field`='sample_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type_from_id')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='sample_control_id_help' AND `language_label`='sample_control_id_label' AND `language_tag`=''),
     '1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_specimen_review'),
     (SELECT id FROM structure_fields WHERE `model`='SpecimenReviewControl' AND `tablename`='specimen_review_controls' AND `field`='aliquot_review_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquots_review_id_type')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='aliquot_review_control_id_help' AND `language_label`='aliquot_review_control_id_label' AND `language_tag`=''),
     '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_specimen_review'),
     (SELECT id FROM structure_fields WHERE `model`='SpecimenReviewControl' AND `tablename`='specimen_review_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_specimen_review'),
     (SELECT id FROM structure_fields WHERE `model`='SpecimenReviewControl' AND `tablename`='specimen_review_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '75', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_specimen_review'),
     (SELECT id FROM structure_fields WHERE `model`='SpecimenReviewControl' AND `tablename`='specimen_review_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '80', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

-- -------------------------------------------------------------------------------------
--	Specimen review Validation
-- -------------------------------------------------------------------------------------
INSERT INTO structure_validations (`structure_field_id`, `rule`, `language_message`)
VALUES
    ((SELECT id FROM structure_fields WHERE `model`='SpecimenReviewControl' AND `tablename`='specimen_review_controls' AND `field`='review_type' AND `type`='input' AND `structure_value_domain`  IS NULL), 'notBlank', ''),
    ((SELECT id FROM structure_fields WHERE `model`='SpecimenReviewControl' AND `tablename`='specimen_review_controls' AND `field`='sample_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type_from_id')), 'notBlank', '');

-- -------------------------------------------------------------------------------------
--	i18N for Specimen review
-- -------------------------------------------------------------------------------------
INSERT IGNORE INTO
i18n (id,en,fr)
VALUES
('specimen review name', "Specimen Review", "Rapport de Spécimen"),
('specimen_review_type_label', "Specimen review name", "Nom de rapport du spécimen"),
('aliquot_review_control_id_label', "Aliquot Review", "Rapport d'aliquotes"),
('aliquot_review_control_id_help', "Optional, Determine the aliquot review name related to this specimen review.<br><b>Note: </b>The aliquot review should be already defined.", "Facultatif, Déterminez le nom de la révision de partie aliquote associé à cette révision de spécimen.<br><b>Remarque:</b>La révision de partie aliquote doit déjà être définie."),
('sample_control_id_label', "Sample", "Sample"),
('specimen review fb_control_description', "specimen review fb_control_description", "specimen review fb_control_description"),
('sample_control_id_help', "Determine the Sample name related to this specimen review.<br><b>Note: </b>The Sample should be already defined.", "Déterminez le nom de l'échantillon associé à cet examen de spécimen.<br><b> Remarque: </b>L'échantillon doit déjà être défini.");

-- -- ('', "", ""),
            -- </editor-fold>

            -- <editor-fold defaultstate="collapsed" desc="AliquotReview">
-- -- -------------------------------------------------------------------------------------
-- --	Add some columns to aliquot_review_controls
-- -- -------------------------------------------------------------------------------------

ALTER TABLE `aliquot_review_controls`
    ADD `display_order` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_active_input` tinyint(1) NOT NULL DEFAULT '1',
CHANGE `review_type` `review_type` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL;

--
-- -- -------------------------------------------------------------------------------------
-- --	Create alias form_builder_aliquot_review
-- -- -------------------------------------------------------------------------------------
INSERT INTO structures(`alias`) VALUES ('form_builder_aliquot_review');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'AliquotReviewControl', 'aliquot_review_controls', 'flag_test_mode', 'checkbox',  NULL , '0', '', '', 'test mode help', 'test mode', ''),
    ('InventoryManagement', 'AliquotReviewControl', 'aliquot_review_controls', 'flag_active', 'checkbox',  NULL , '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('InventoryManagement', 'AliquotReviewControl', 'aliquot_review_controls', 'flag_active_input', 'checkbox',  NULL , '0', '', '', 'help_flag_active_input_fb', 'flag active as input', ''),
    ('InventoryManagement', 'AliquotReviewControl', 'aliquot_review_controls', 'aliquot_type_restriction', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type') , '0', 'class=atim-multiple-choice', '', 'aliquot_type_restriction_help_fb', 'aliquot_type_restriction', ''),
    ('InventoryManagement', 'AliquotReviewControl', 'aliquot_review_controls', 'review_type', 'input',  NULL , '0', 'class=control-name', '', '', 'aliqout_review_type', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot_review'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotReviewControl' AND `tablename`='aliquot_review_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot_review'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotReviewControl' AND `tablename`='aliquot_review_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot_review'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotReviewControl' AND `tablename`='aliquot_review_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot_review'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotReviewControl' AND `tablename`='aliquot_review_controls' AND `field`='aliquot_type_restriction' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='aliquot_type_restriction_help_fb' AND `language_label`='aliquot_type_restriction' AND `language_tag`=''),
     '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_aliquot_review'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotReviewControl' AND `tablename`='aliquot_review_controls' AND `field`='review_type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=control-name' AND `default`='' AND `language_help`='' AND `language_label`='aliqout_review_type' AND `language_tag`=''),
     '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');




INSERT INTO structure_validations (`structure_field_id`, `rule`, `language_message`)
VALUES
    ((SELECT id FROM structure_fields WHERE `model`='AliquotReviewControl' AND `tablename`='aliquot_review_controls' AND `field`='review_type' AND `type`='input' AND `structure_value_domain`  IS NULL), 'notBlank', ''),
    ((SELECT id FROM structure_fields WHERE `model`='AliquotReviewControl' AND `tablename`='aliquot_review_controls' AND `field`='aliquot_type_restriction' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type')), 'notBlank', '');

-- -------------------------------------------------------------------------------------
--	i18N for Specimen review
-- -------------------------------------------------------------------------------------
INSERT IGNORE INTO
i18n (id,en,fr)
VALUES
('aliqout_review_type', "Aliqout review name", "Nom d'examen en aliquotes"),
('aliquot_type_restriction', "Aliquot Type", "Type d'aliquote"),
('aliquot review fb_control_description', "aliquot review fb_control_description", "aliquot review fb_control_description");
-- ('', "", ""),


            -- </editor-fold>

            -- <editor-fold defaultstate="collapsed" desc="Inventory Action">

              -- <editor-fold desc="Add list for Inventory Action Category type">
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
    ('Inventory Event/Annotation Categories', 1, 255, '');

SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Inventory Event/Annotation Categories');

SET @user_id = 2;

INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`)
VALUES
    ("quality control", "Quality Control", "Control de qualité", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
    ("aliquot_internal_uses", "Aliquots Internal Use", "Utilisation interne d''aliquotes", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
    ("specimen review", "Aliquot/Specimen Review", "Rapport de Spécimen/Aliquote", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

UPDATE structure_permissible_values_custom_controls
SET values_max_length = 255, values_used_as_input_counter = 3, values_counter = 3 WHERE name = 'inventory_action_categories';

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
    ('inventory_action_categories', 'open', '', "StructurePermissibleValuesCustom::getCustomDropdown('Inventory Event/Annotation Categories')");


INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`)
VALUES
    ('specimen_sample_type_from_id_all', 'open', '', 'InventoryManagement.SampleControl::getSpecimenSampleTypePermissibleValuesFromIdAll');

    -- </editor-fold>

    -- <editor-fold desc="Add list for Inventory Action Aliquot types concat with Sample">

INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('aliquot_type_list_concat_sample', 'open', '', 'InventoryManagement.AliquotControl::getAliquotTypesPermissibleValues');

    -- </editor-fold>

    -- <editor-fold desc="Create the form_builder_inventory_action Alias">
INSERT INTO structures(`alias`) VALUES ('form_builder_inventory_action');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'type', 'input',  NULL , '0', 'class=control-name', '', 'control / form type to display to user', 'inventory action type', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'category', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_categories') , '0', '', '', '', 'inventory action category', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'apply_on', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_apply_on_types') , '0', '', '', 'inv. action categ. : apply on help message', 'apply on', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'sample_control_ids', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type_from_id_all') , '0', 'class=atim-multiple-choice,class=samples-control_ids', '', '', 'sample type', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'aliquot_control_ids', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type_list_concat_sample') , '0', 'class=atim-multiple-choice,class=aliquots-control_ids', '', '', 'aliquot type', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'flag_test_mode', 'checkbox',  NULL , '0', '', '', 'test mode help', 'test mode', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'flag_active', 'checkbox',  NULL , '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'flag_active_input', 'checkbox',  NULL , '0', '', '', 'help_flag_active_input_fb', 'flag active as input', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'flag_link_to_study', 'checkbox',  NULL , '0', '', '', 'test mode help', 'linked to a study', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'flag_link_to_storage', 'checkbox',  NULL , '0', '', '', '', 'inv action: link to storage', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'flag_batch_action', 'checkbox',  NULL , '0', '', '', 'ia_help_flag_action_in_batch', 'flag action in batch', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=control-name' AND `default`='' AND `language_help`='control / form type to display to user' AND `language_label`='inventory action type' AND `language_tag`=''),
     '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='category' AND `type`='select' ),
     '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='apply_on'),
     '1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='sample_control_ids'),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='aliquot_control_ids'),
     '1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='flag_test_mode'),
     '1', '50', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='flag_active'),
     '1', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='flag_active_input'),
     '1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='flag_link_to_study' AND `type`='checkbox' ),
     '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='flag_link_to_storage' AND `type`='checkbox'),
     '1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_inventory_action'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='flag_batch_action' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='ia_help_flag_action_in_batch' AND `language_label`='flag action in batch' AND `language_tag`=''),
     '1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

UPDATE structure_fields SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type_from_id_all') ,  `language_help`='inv. action help: sp ctlr ids' WHERE model='InventoryActionControl' AND tablename='inventory_action_controls' AND field='sample_control_ids' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='specimen_sample_type_from_id_all');
UPDATE structure_fields SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type_list_concat_sample') ,  `language_help`='inv. action help: al ctlr ids' WHERE model='InventoryActionControl' AND tablename='inventory_action_controls' AND field='aliquot_control_ids' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type_list_concat_sample');
UPDATE structure_fields SET  `language_help`='inv. action help: link to study' WHERE model='InventoryActionControl' AND tablename='inventory_action_controls' AND field='flag_link_to_study' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_help`='inv. action help: link to storage' WHERE model='InventoryActionControl' AND tablename='inventory_action_controls' AND field='flag_link_to_storage' AND `type`='checkbox' AND structure_value_domain  IS NULL ;

    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Validation">
INSERT INTO structure_validations (`structure_field_id`, `rule`, `language_message`)
VALUES
    ((SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='type' AND `type` = 'input' ), 'notBlank', ''),
    ((SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='category' AND `type` = 'select'), 'notBlank', ''),
    ((SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='apply_on' AND `type` = 'select'), 'notBlank', '');

    -- </editor-fold>

    -- <editor-fold desc="Update inventory_action_masters structure alias: add controls info to form and re-order fields">

INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`)
VALUES
    ('inventory_action_types', 'open', '', 'InventoryManagement.InventoryActionControl::getActiveTypesPermissibleValues');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_types') , '0', '', '', '', 'inventory action type', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='type' AND `type`='select'),
     '0', '-1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='category' AND `type`='select' ),
     '0', '-2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0');



UPDATE structure_formats SET `display_order`='2' WHERE structure_id=(SELECT id FROM structures WHERE alias='inventory_action_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='3' WHERE structure_id=(SELECT id FROM structures WHERE alias='inventory_action_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='4' WHERE structure_id=(SELECT id FROM structures WHERE alias='inventory_action_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='aliquot_master_name' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

UPDATE structure_formats SET `display_order`='9' WHERE structure_id=(SELECT id FROM structures WHERE alias='inventory_action_study') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='9' WHERE structure_id=(SELECT id FROM structures WHERE alias='inventory_action_study') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_aliquot_internal_use_study_summary_id' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

UPDATE structure_formats SET `display_order`='6' WHERE structure_id=(SELECT id FROM structures WHERE alias='inventory_action_volume') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='used_volume' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='7' WHERE structure_id=(SELECT id FROM structures WHERE alias='inventory_action_volume') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit') AND `flag_confidential`='0');


    -- </editor-fold>


    -- </editor-fold>

        -- </editor-fold>

    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Add the new word to Dictionary Form">

-- -------------------------------------------------------------------------------------
--	form_builder_i18n Structure
-- -------------------------------------------------------------------------------------
INSERT INTO structures(`alias`) VALUES ('form_builder_i18n');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) VALUES
                                                                                                                                                                                                          ('Administrate', 'FormBuilder', 'i18n', 'en', 'input',  NULL, '0', 'size=100', '', '', 'English', ''),
                                                                                                                                                                                                          ('Administrate', 'FormBuilder', 'i18n', 'id', 'hidden',  NULL, '0', '', '', '', 'id', ''),
                                                                                                                                                                                                          ('Administrate', 'FormBuilder', 'i18n', 'fr', 'input',  NULL, '0', 'size=100', '', '', 'French', ''),
                                                                                                                                                                                                          ('Administrate', 'FormBuilder', 'i18n', 'page_id', 'hidden',  NULL, '0', '', '', '', 'page_id', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_i18n'), (SELECT id FROM structure_fields WHERE `model`='FormBuilder' AND `tablename`='i18n' AND `field`='en' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=100' AND `default`='' AND `language_help`='' AND `language_label`='English' AND `language_tag`=''), '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_i18n'), (SELECT id FROM structure_fields WHERE `model`='FormBuilder' AND `tablename`='i18n' AND `field`='id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='id' AND `language_tag`=''), '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_i18n'), (SELECT id FROM structure_fields WHERE `model`='FormBuilder' AND `tablename`='i18n' AND `field`='fr' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=100' AND `default`='' AND `language_help`='' AND `language_label`='French' AND `language_tag`=''), '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_i18n'), (SELECT id FROM structure_fields WHERE `model`='FormBuilder' AND `tablename`='i18n' AND `field`='page_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='page_id' AND `language_tag`=''), '1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0');

    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Create form_builder_master_structure alias">
-- -------------------------------------------------------------------------------------
--	Add the form_builder_master_structure structure for the master fields.
-- -------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('form_builder_master_structure');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='language_label' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='language_label_help' AND `language_label`='language_label' AND `language_tag`=''), '1', '10', 'general information', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '1'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='form_builder_type_list')  AND `flag_confidential`='0' AND `setting`='class=fb_type_select,class=pasteDisabled' AND `default`='' AND `language_help`='language_field_type_help' AND `language_label`='field type' AND `language_tag`=''), '1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='language_help' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='language_help_help' AND `language_label`='language_help' AND `language_tag`=''), '1', '130', 'other', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='flag_confidential' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='flag_confidential_help' AND `language_label`='flag_confidential' AND `language_tag`=''), '1', '30', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='is_structure_value_domain' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=fb_is_structure_value_domain_input' AND `default`='' AND `language_help`='is_structure_value_domain_help' AND `language_label`='is_structure_value_domain' AND `language_tag`=''), '1', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_add' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_add_help' AND `language_label`='flag_add' AND `language_tag`=''), '1', '50', 'display according to form type', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_edit' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_edit_help' AND `language_label`='flag_edit' AND `language_tag`=''), '1', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_search' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_search_help' AND `language_label`='flag_search' AND `language_tag`=''), '1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_index' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_index_help' AND `language_label`='flag_index' AND `language_tag`=''), '1', '80', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_detail' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_detail_help' AND `language_label`='flag_detail' AND `language_tag`=''), '1', '90', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='language_heading' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='language_heading_help' AND `language_label`='language_heading' AND `language_tag`=''), '1', '100', 'layout format', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='display_column' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='1' AND `language_help`='display_column_help' AND `language_label`='display_column' AND `language_tag`=''), '1', '110', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='display_order' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='1' AND `language_help`='display_order_help' AND `language_label`='display_order' AND `language_tag`=''), '1', '120', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '1', '1', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='fb_has_validation' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=fb_has_validation' AND `default`='' AND `language_help`='has_validation_help' AND `language_label`='has_validation' AND `language_tag`=''), '1', '140', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           ((SELECT id FROM structures WHERE alias='form_builder_master_structure'), (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='CopyCtrl' AND `type`='checkbox' AND `structure_value_domain` IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='copy control' AND `language_tag`=''), '10', '10000', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Create form_builder_deletable_structure alias">

-- -------------------------------------------------------------------------------------
--	Add the form_builder_deletable_structure structure for the copied fields from a form created before formbuilder.
-- -------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('form_builder_deletable_structure');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='language_help' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='language_help_help' AND `language_label`='language_help' AND `language_tag`=''),
     '1', '125', 'other', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='form_builder_type_list')  AND `flag_confidential`='0' AND `setting`='class=fb_type_select,class=pasteDisabled' AND `default`='' AND `language_help`='language_field_type_help' AND `language_label`='field type' AND `language_tag`=''),
     '1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='display_column' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='1' AND `language_help`='display_column_help' AND `language_label`='display_column' AND `language_tag`=''),
     '1', '110', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='display_order' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='1' AND `language_help`='display_order_help' AND `language_label`='display_order' AND `language_tag`=''),
     '1', '120', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='language_label' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='language_label_help' AND `language_label`='language_label' AND `language_tag`=''),
     '1', '10', 'general information', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '1'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='language_heading' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='language_heading_help' AND `language_label`='language_heading' AND `language_tag`=''),
     '1', '100', 'layout format', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='id' AND `language_tag`=''),
     '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='flag_copy_from_form_builder' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='flag_copy_from_form_builder' AND `language_tag`=''),
     '1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='id' AND `language_tag`=''),
     '1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='flag_confidential' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=pasteDisabled' AND `default`='' AND `language_help`='flag_confidential_help' AND `language_label`='flag_confidential' AND `language_tag`=''),
     '1', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_add' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_add_help' AND `language_label`='flag_add' AND `language_tag`=''),
     '1', '50', 'display according to form type', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_edit' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_edit_help' AND `language_label`='flag_edit' AND `language_tag`=''),
     '1', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_search' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_search_help' AND `language_label`='flag_search' AND `language_tag`=''),
     '1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_index' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_index_help' AND `language_label`='flag_index' AND `language_tag`=''),
     '1', '80', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureFormat' AND `tablename`='structure_formats' AND `field`='flag_detail' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='1' AND `language_help`='flag_detail_help' AND `language_label`='flag_detail' AND `language_tag`=''),
     '1', '90', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='is_structure_value_domain' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=fb_is_structure_value_domain_input' AND `default`='' AND `language_help`='is_structure_value_domain_help' AND `language_label`='is_structure_value_domain' AND `language_tag`=''),
     '1', '45', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='fb_has_validation' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=fb_has_validation' AND `default`='' AND `language_help`='has_validation_help' AND `language_label`='has_validation' AND `language_tag`=''),
     '1', '130', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

    -- </editor-fold>

    -- <editor-fold defaultstate="collapsed" desc="Some table modifications">

-- -------------------------------------------------------------------------------------
--	Add the display_order column to treatment_extend_controls
-- -------------------------------------------------------------------------------------
ALTER TABLE `treatment_extend_controls`
    ADD `display_order` int(11) NOT NULL DEFAULT '0';
-- -------------------------------------------------------------------------------------
--	Add two column to structure_fields
-- -------------------------------------------------------------------------------------

ALTER TABLE `structure_fields`
    ADD `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
ADD `flag_copy_from_form_builder` tinyint(1) NOT NULL DEFAULT '0';

-- -------------------------------------------------------------------------------------
--	Change the view_structure_formats_simplified and add 2 fields
-- -------------------------------------------------------------------------------------
DROP VIEW IF EXISTS `view_structure_formats_simplified`;
CREATE VIEW `view_structure_formats_simplified` AS
select
    CONCAT (`sfo`.`id`,`sfi`.`id`) AS `id`,
    `str`.`alias` AS `structure_alias`,
    `sfo`.`id` AS `structure_format_id`,
    `sfi`.`id` AS `structure_field_id`,
    `sfo`.`structure_id` AS `structure_id`,
    `sfi`.`plugin` AS `plugin`,
    `sfi`.`model` AS `model`,
    `sfi`.`tablename` AS `tablename`,
    `sfi`.`field` AS `field`,
    `sfi`.`structure_value_domain` AS `structure_value_domain`,
    `svd`.`domain_name` AS `structure_value_domain_name`,
    `sfi`.`flag_confidential` AS `flag_confidential`,
    if((`sfo`.`flag_override_label` = '1'),`sfo`.`language_label`,`sfi`.`language_label`) AS `language_label`,
    if((`sfo`.`flag_override_tag` = '1'),`sfo`.`language_tag`,`sfi`.`language_tag`) AS `language_tag`,
    if((`sfo`.`flag_override_help` = '1'),`sfo`.`language_help`,`sfi`.`language_help`) AS `language_help`,
    if((`sfo`.`flag_override_type` = '1'),`sfo`.`type`,`sfi`.`type`) AS `type`,
    if((`sfo`.`flag_override_setting` = '1'),`sfo`.`setting`,`sfi`.`setting`) AS `setting`,
    if((`sfo`.`flag_override_default` = '1'),`sfo`.`default`,`sfi`.`default`) AS `default`,
    sfi.sortable,
    `sfo`.`flag_add` AS `flag_add`,`sfo`.`flag_add_readonly` AS `flag_add_readonly`,
    `sfo`.`flag_edit` AS `flag_edit`,`sfo`.`flag_edit_readonly` AS `flag_edit_readonly`,
    `sfo`.`flag_search` AS `flag_search`,`sfo`.`flag_search_readonly` AS `flag_search_readonly`,
    `sfo`.`flag_addgrid` AS `flag_addgrid`,`sfo`.`flag_addgrid_readonly` AS `flag_addgrid_readonly`,
    `sfo`.`flag_editgrid` AS `flag_editgrid`,`sfo`.`flag_editgrid_readonly` AS `flag_editgrid_readonly`,
    `sfo`.`flag_batchedit` AS `flag_batchedit`,`sfo`.`flag_batchedit_readonly` AS `flag_batchedit_readonly`,
    `sfo`.`flag_index` AS `flag_index`,`sfo`.`flag_detail` AS `flag_detail`,`sfo`.`flag_summary` AS `flag_summary`,
    `sfo`.`flag_float` AS `flag_float`,
    `sfo`.`display_column` AS `display_column`,
    `sfo`.`display_order` AS `display_order`,
    `sfo`.`language_heading` AS `language_heading`,
    `sfi`.`flag_form_builder` AS `flag_form_builder`,
    `sfi`.`flag_copy_from_form_builder` AS `flag_copy_from_form_builder`,
    `sfi`.`flag_test_mode` AS `flag_test_mode`,
    `sfo`.`margin` AS `margin`
from (((`structure_formats` `sfo` join `structure_fields` `sfi` on((`sfo`.`structure_field_id` = `sfi`.`id`)))
    join `structures` `str` on((`str`.`id` = `sfo`.`structure_id`)))
         left join `structure_value_domains` `svd` on((`svd`.`id` = `sfi`.`structure_value_domain`)));
UPDATE structure_fields SET sortable = '0' WHERE model IN ('0', 'FunctionManagement', 'Generated', 'GeneratedParentAliquot', 'GeneratedParentSample');

    -- </editor-fold>

    -- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/285): Category populated by FB is not useful/readable for user">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Issue #285: Category populated by FB is not useful/readable for user
--  https://gitlab.com/ctrnet/atim/-/issues/285
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE structure_formats SET `flag_index`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='administrate_dropdowns') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StructurePermissibleValuesCustomControl' AND `tablename`='structure_permissible_values_custom_controls' AND `field`='category' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='permissible_values_custom_categories') AND `flag_confidential`='0');
    -- </editor-fold>

-- </editor-fold>


-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/349): Change the listall action name to camelCase">
-- Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/349): Change the listall action name to camelCase
UPDATE menus SET use_link = REPLACE (use_link, 'listall', 'listAll') WHERE use_link LIKE '%listall%';
-- </editor-fold>


-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/366): Add an appropriate popup to select a position in from the storage">
-- Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/366): Add an appropriate popup to select a position in from the storage
INSERT IGNORE INTO `i18n` (id, en, fr) VALUES
('please select a suggested value from the list.', 'Please select a suggested value from the list.', 'Veuillez sélectionner une valeur proposée dans la liste.');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/379): New Data Types Relation Ship Diagram">
-- Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/379): New Data Types Relation Ship Diagram
UPDATE `datamart_browsing_controls` SET `flag_active_1_to_2` = '1', `flag_active_2_to_1` = '1'
WHERE `id1` = (SELECT id FROM `datamart_structures` WHERE `model` = 'InventoryActionMaster') AND
      `id2` IN (SELECT id FROM `datamart_structures` WHERE `model` IN ('ViewSample', 'ViewAliquotUse'));

DELETE FROM `datamart_browsing_controls`
WHERE `id1` = (SELECT id FROM `datamart_structures` WHERE `model` = 'InventoryActionMaster') AND
      `id2` IN (SELECT id FROM `datamart_structures` WHERE `model` IN ('ViewAliquotUse'));

INSERT IGNORE INTO `i18n` (id, en, fr) VALUES
('aliquot use foot note', '* Aliquot Use consists of certain events performed on the aliquots, such as realiquoting, ordering, derivation creation and annotation/events', '* Usage des aliquotes consiste en certains événements effectués sur les aliquotes, tels que la réaliquote, la commande, la création de dérivation et annotation/événements');
-- </editor-fold>

-- <editor-fold desc="Add correction on i18n">
REPLACE INTO `i18n` (id, en, fr) VALUES
('event/annotation selection', "Event/Annotation Type Selection", "Sélection type d'événement/annotation"),
("add uses/events", "Add Events/Annotations", "Ajouter événements/annotations"),
("aliquot internal use code", "Event/Annotation Defintion", "Définition de l'événement/annotation"),
("aliquot use foot note", 
"* Aliquot Use consists of certain events performed on the aliquots, such as realiquoting, ordering, derivatives creation and annotations/events.",
"* L'utilisation des aliquotes consiste en certains événements effectués sur les aliquotes, tels que du réaliquotage, l'ajout à des commandes, la création de dérivatés et la création d'annotations/événements."),
("aliquot use/event", "Aliquot Event/Annotation", "Utilisation/événement d'aliquote"),
("aliquot uses and events", "Aliquot Events/Annotations", "Événements/annotations d'aliquote"),
("create use/event (applied to all)", "Create Event/Annotation (applied to all)", "Créer événement/annotation (applicable à tous)"),
("create uses/events (aliquot specific)", "Create Events/Annotations (aliquot specific)", "Créer événements/annotations (aliquote spécifique)"),
("use and/or event", "Event/Annotation", "Utilisation/événement"),
("use/event creation", "Event/Annotation Creation", "Création événement/annotation"),
("uses and events", "Events/Annotations", "Événements/annotations"),
("you are about to create an use/event for %d aliquot(s)", "You are about to create an event/annotation for %d aliquot(s)", "Vous êtes sur le point de créer un(e) événement/annotation pour %d aliquotes."),
("you are about to create an use/event for %s aliquot(s) contained into %s",
"You are about to create an events/annotation for %s aliquot(s) contained into %s.",
"Vous êtes sur le point de créer un(e) événement/annotation pour %d aliquotes contenues dans %s."),
('no inventory event/annotation type can be selected for the selected items and the launched action',
"No inventory event/annotation type can be selected for the processed items and the launched action.",
"Aucun événement/annotation d'inventaire ne peut être choisi pour les éléments traités et l'action lancée"),
("events/annotation types", "Events/Annotations Types", "Types des événements/annotations"),
("event_annotation_listall_specimen_header_string",
"Events/Annotations List (specimen, derivatives and aliquots)",
"Événements/annotations (spécimen, dérivés et aliquotes)"),
('event_annotation_listall_specimen_description_string',
"Events/annotations list of the specimen, its derivatives and all their aliquots. To access detailed list of a specific event/annotation type, use the filter option below.", 
"Liste d''événements/annotations du spécimen, ses dérivés et toutes leurs aliquotes. Pour accéder à la liste détaillée d'un type d'événement/d'annotation spécifique, utilisez l'option de filtre ci-dessous."),
('event_annotation_listall_derivative_description_string',
"Events/annotations list of the derivative and its aliquots. To access detailed list of a specific event/annotation type, use the filter option below.", 
"Liste d''événements/annotations du dérivé et toutes ses aliquotes. Pour accéder à la liste détaillée d'un type d'événement/d'annotation spécifique, utilisez l'option de filtre ci-dessous."),
('summary_lower_case', 'summary', 'résumé'), 
("for more details and access the all specimen events annotations list click %s",
'List limited to specimen''s events/annotations and generic data only. For more details and access the all events/annotations list of the specimen, its derivatives and their aliquots, click %s.',
'Liste limitée aux événements/annotations du spécimen et aux données génériques seulement. Pour plus de détails et accéder à toute la liste d''événements/annotations du spécimen, ses dérivés et leurs aliquotes), cliquez %s.'),
("for more details and access the all sample events annotations list click %s",
'List limited to derivative''s events/annotations and generic data only. For more details and access the all events/annotations detailed list of the derivative and its aliquots, click %s.',
'Liste limitée aux événements/annotations du dérivé et aux données génériques seulement. Pour plus de détails et accéder à toute la liste détaillée d''événements/annotations du dérivé et ses aliquotes, cliquez %s.'),
('for more details and access the all aliquot events annotations list click %s',
'List limited to aliquot''s events/annotations and generic data only. For more details and access the all events/annotations list including storage history, realiquotings, derivatives, etc., click %s.',
'Liste limitée aux événements/annotations de l''aliquot et aux données génériques seulement. Pour plus de détails et accéder à toute la liste d''événements/annotations incluant l''historique d''entreposage, les réaliquotages, les dérivés, etc., cliquez %s.'),
('at least one event/annotation has to be created per parent',
'At least one event/annotation has to be created per selected item.',
'Au moins un(e) événements/annotation doit être créé par élement sélectionné.'),
('(limited to specimen)', '(list limited to specimen)', '(listelimité au spécimen)'),
('see detail options in data to apply to all', 
"See the 'stock detail' options in 'Data to be applied to all' section.",
"Voir les options du 'détail du stock' dans la section 'Données à appliquer à tous'."),
('you are about to create event/annotation for %d element(s)', 'You are about to create a same event/annotation for %d item(s).', "Vous êtes sur le point de créer un(e) même événement/annotation pour %d élément(s).");

-- </editor-fold>

-- <editor-fold desc="Removed addAliquotInternalUse() and addInternalUseToManyAliquots() from datamart_structure_functions (issue #380).">
DELETE FROM datamart_structure_functions
WHERE LINK IN (
'/InventoryManagement/AliquotMasters/addAliquotInternalUse/',
'/InventoryManagement/QualityCtrls/add/',
'/InventoryManagement/AliquotMasters/addInternalUseToManyAliquots');
-- </editor-fold>



-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/401): SpecimenReview, QualityCtrl, etc still in acos table">
DELETE FROM `acos` WHERE `parent_id` IN (SELECT `id` FROM (SELECT `id` FROM `acos` WHERE `alias` = 'QualityCtrls') c);
DELETE FROM `acos` WHERE `alias` = 'QualityCtrls';
DELETE FROM `acos` WHERE `parent_id` IN (SELECT `id` FROM (SELECT `id` FROM `acos` WHERE `alias` =  'AliquotMasters') c) AND `alias` LIKE '%internaluse%';
DELETE FROM `acos` WHERE `parent_id` IN (SELECT `id` FROM (SELECT `id` FROM `acos` WHERE `alias` = 'SpecimenReviews') c);
DELETE FROM `acos` WHERE `alias` = 'SpecimenReviews';
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/314): Inventory Use/Event listAll() - Add specific sub-menu">

INSERT INTO structures(`alias`) VALUES ('inventory_action_sample_types');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'SampleControl', 'sample_controls', 'sample_type', 'input',  NULL , '0', '', '', '', 'sample type', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_sample_types'),
     (SELECT id FROM structure_fields WHERE `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='sample_type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='sample type' AND `language_tag`=''),
     '0', '-1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
-- </editor-fold>

-- --------------------------------------------------------------------------------------------------
-- New changing after the 2.8.B tag from 2021-11-15
-- --------------------------------------------------------------------------------------------------

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/411): Renamed AliquotMastersController.listallUses to AliquotMastersController.listAllUses">
UPDATE `menus` SET `use_link` = REPLACE (`use_link`, 'listallUses', 'listAllUses') WHERE `use_link` LIKE '%listallUses%';
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/414): convert to camelCase all the actions">
UPDATE `structure_fields` SET `setting` = REPLACE (`setting`, 'autocomplete', 'autoComplete') WHERE `setting` LIKE '%autocomplete%';
UPDATE `structure_formats` SET `setting` = REPLACE (`setting`, 'autocomplete', 'autoComplete') WHERE `setting` LIKE '%autocomplete%' AND `flag_override_setting` = '1';
UPDATE `acos` SET `alias` = REPLACE (`alias`, 'autocomplete', 'autoComplete') WHERE `alias` LIKE '%autocomplete%';
-- </editor-fold>



-- <editor-fold desc="Add Note to the add Grid form">
UPDATE structure_formats SET `flag_addgrid`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='inventory_action_masters') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='notes' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/423): Delete QC, SR and AR from datamart structure">
DELETE FROM `datamart_batch_ids` WHERE `datamart_batch_ids`.set_id IN(
    SELECT id FROM `datamart_batch_sets` WHERE `datamart_batch_sets`.datamart_structure_id IN (
        SELECT id FROM datamart_structures where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl')
    )
);

DELETE FROM `datamart_batch_sets` WHERE `datamart_batch_sets`.datamart_structure_id IN (
    SELECT id FROM datamart_structures where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl')
);

DELETE FROM `datamart_saved_browsing_steps` WHERE `datamart_saved_browsing_steps`.`datamart_saved_browsing_index_id` IN(
    SELECT id FROM `datamart_saved_browsing_indexes` WHERE `datamart_saved_browsing_indexes`.starting_datamart_structure_id IN (
        SELECT id FROM datamart_structures where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl')
    )
);

DELETE FROM `datamart_saved_browsing_steps` WHERE `datamart_saved_browsing_steps`.`datamart_structure_id` IN(
    SELECT id FROM datamart_structures where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl')
);

DELETE FROM `datamart_saved_browsing_indexes` WHERE `datamart_saved_browsing_indexes`.`starting_datamart_structure_id` IN(
    SELECT id FROM datamart_structures where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl')
);

DELETE FROM `template_nodes` WHERE `template_nodes`.`datamart_structure_id` IN(
    SELECT id FROM datamart_structures where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl')
);

DELETE FROM `datamart_structure_functions` WHERE `datamart_structure_functions`.datamart_structure_id IN (
    SELECT id FROM datamart_structures where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl')
);

UPDATE `datamart_reports` SET `flag_active` = 0, associated_datamart_structure_id = NULL WHERE `associated_datamart_structure_id` IN (
    SELECT id FROM datamart_structures where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl')
);

DELETE FROM datamart_browsing_controls WHERE id1 In (
    SELECT id FROM `datamart_structures` where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl')
);

DELETE FROM datamart_browsing_controls WHERE id2 In (
    SELECT id FROM `datamart_structures` where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl')
);

DELETE FROM datamart_structures where model IN ('SpecimenReviewMaster', 'AliquotReviewMaster', 'QualityCtrl');
-- </editor-fold>


-- <editor-fold desc="Allow link between collection and participant identifier">

-- -------------------------------------------------------------------------------------
--	Link Misc Identifier to collection
-- -------------------------------------------------------------------------------------

-- -------------------------------------------------------
-- Update Database tables :
--     1- ADD flag_use_for_ccl misc_identifier_controls
--     2- ADD misc_identifier_id in collections 
-- --------------------------------------------------------
ALTER TABLE misc_identifier_controls ADD COLUMN `flag_use_for_ccl` tinyint(1) NOT NULL DEFAULT '0';
ALTER TABLE collections ADD COLUMN misc_identifier_id int(11) DEFAULT NULL;
ALTER TABLE collections ADD CONSTRAINT `collections_ibfk_6` FOREIGN KEY (`misc_identifier_id`) REFERENCES `misc_identifiers` (`id`);
ALTER TABLE collections_revs ADD COLUMN misc_identifier_id int(11) DEFAULT NULL;

-- ---------------------------------------------------------------------
-- datamart_browsing_controls : link collection to miscidentifiers
-- ----------------------------------------------------------------------
INSERT INTO datamart_browsing_controls (id1, id2, flag_active_1_to_2, flag_active_2_to_1, use_field)
VALUES
((SELECT id FROM `datamart_structures` WHERE model = 'ViewCollection'),
(SELECT id FROM `datamart_structures` WHERE model = 'MiscIdentifier'),
'1', '1', 'misc_identifier_id');
 
-- -----------------------------------------------------
-- Function structure value domains : identifier_list  
-- -----------------------------------------------------   
INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('identifier_list', 'open', '', 'ClinicalAnnotation.EventControl::getEventDiseaseSitePermissibleValues');

-- ----------------------------------------------------------
-- Structure clinicalcollectionlinks    
-- 			Add misc_identifier_name and identifier_value
-- ----------------------------------------------------------
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='clinicalcollectionlinks'), 
(SELECT id FROM structure_fields WHERE `model`='MiscIdentifier' AND `tablename`='misc_identifiers' AND `field`='identifier_value' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '510', '', '0', '1', 'value', '0', '', '0', '', '0', '', '1', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='clinicalcollectionlinks'), 
(SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='misc_identifier_name' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='name' AND `language_tag`=''), 
'1', '500', 'Identifier', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');

-- --------------------------------------------------------
-- view_collection
-- Add misc_identifier_name, identifier_value and study
-- --------------------------------------------------------
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'ViewCollection', '', 'misc_identifier_name', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list') , '0', '', '', '', 'miscidentifier', ''),
('InventoryManagement', 'ViewCollection', '', 'identifier_value', 'input',  NULL , '0', 'size=20', '', '', 'identifier value', ''), 
('InventoryManagement', 'ViewCollection', '', 'title', 'input',  NULL , '0', 'size=20', '', '', 'study_title', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='view_collection'), 
(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='misc_identifier_name' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='miscidentifier' AND `language_tag`=''), 
'0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'),
((SELECT id FROM structures WHERE alias='view_collection'), 
(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='identifier_value' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='identifier value' AND `language_tag`=''), 
'0', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='view_collection'), 
(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='title' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='study_title' AND `language_tag`=''), 
'0', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
-- Update order display in view_collection
UPDATE structure_fields SET  `setting`='size=30,class=file range' WHERE model='ViewCollection' AND tablename='' AND field='participant_identifier' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `flag_override_setting`='0', `setting`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='5' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='bank_id' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='banks') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='6' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='collection_site' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='custom_collection_site') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='7' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='collection_datetime' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_fields SET  `language_label`='study' WHERE model='ViewCollection' AND tablename='' AND field='title' AND `type`='input' AND structure_value_domain  IS NULL ;

-- ------------------------------------------------------
-- view_sample_joined_to_collection
--       Add misc_identifier_name and identifier_value
-- ------------------------------------------------------
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'ViewSample', '', 'misc_identifier_name', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list') , '0', '', '', '', 'miscidentifier', ''), 
('InventoryManagement', 'ViewSample', '', 'identifier_value', 'input',  NULL , '0', 'size=30', '', '', 'identifier value', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='view_sample_joined_to_collection'), 
(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='misc_identifier_name' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='miscidentifier' AND `language_tag`=''), 
'0', '-2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='view_sample_joined_to_collection'), 
(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='identifier_value' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='' AND `language_label`='identifier value' AND `language_tag`=''), 
'0', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
UPDATE structure_formats SET `display_order`='-6' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='acquisition_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='-4' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- -----------------------------------------------------
-- view_aliquot_joined_to_sample_and_collection
--        Add misc_identifier_name, identifier_value
-- ------------------------------------------------------
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'ViewAliquot', '', 'misc_identifier_name', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list') , '0', '', '', '', 'miscidentifier', ''), 
('InventoryManagement', 'ViewAliquot', '', 'identifier_value', 'input',  NULL , '0', 'size=30', '', '', 'identifier value', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='misc_identifier_name' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='miscidentifier' AND `language_tag`=''), 
'0', '-2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'), 
((SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='identifier_value' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='' AND `language_label`='identifier value' AND `language_tag`=''), 
'0', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0');
UPDATE structure_fields SET  `setting`='size=30,class=file range' WHERE model='ViewAliquot' AND tablename='' AND field='participant_identifier' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `display_order`='-6' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='acquisition_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='-4', `flag_override_setting`='0', `setting`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT INTO i18n (id, en, fr)
VALUES 
('miscidentifier', 'Identifier', 'Identifiant'),
('error_fk_misc_identifier_linked_collection', 'Your data cannot be deleted! This Identifier is linked to a collection.', 'Vos données ne peuvent être supprimées! Cet identifiant est attaché à une collection.');

UPDATE structure_fields SET  `language_label`='collection_misc_identifier_name' WHERE model='ViewCollection' AND tablename='' AND field='misc_identifier_name' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list');
UPDATE structure_fields SET  `setting`='size=15,class=file range',  `language_label`='' WHERE model='ViewCollection' AND tablename='' AND field='identifier_value' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `setting`='size=10,placeholder=Study/Etude',  `language_label`='' WHERE model='ViewCollection' AND tablename='' AND field='title' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `display_order`='4' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='identifier_value' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='3' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

UPDATE structure_fields SET  `language_label`='collection_misc_identifier_name' WHERE model='ViewSample' AND tablename='' AND field='misc_identifier_name' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list');
UPDATE structure_fields SET  `setting`='size=15,class=file range',  `language_label`='collection_misc_identifier_name' WHERE model='ViewSample' AND tablename='' AND field='identifier_value' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `flag_edit`='0', `flag_edit_readonly`='0', `flag_search`='0', `flag_index`='0', `flag_detail`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='misc_identifier_name' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list') AND `flag_confidential`='0');

UPDATE structure_fields SET  `language_label`='collection_misc_identifier_name' WHERE model='ViewAliquot' AND tablename='' AND field='misc_identifier_name' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list');
UPDATE structure_fields SET  `setting`='size=15,class=file range',  `language_label`='collection_misc_identifier_name' WHERE model='ViewAliquot' AND tablename='' AND field='identifier_value' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `flag_search`='0', `flag_index`='0', `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='misc_identifier_name' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list') AND `flag_confidential`='0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('collection_misc_identifier_name', 'Collection Participant Id', 'Id de collection du participant');

UPDATE structure_formats SET `display_order`='90', `language_heading`='collection_misc_identifier_name' WHERE structure_id=(SELECT id FROM structures WHERE alias='clinicalcollectionlinks') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='misc_identifier_name' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='91', `flag_override_label`='0', `language_label`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='clinicalcollectionlinks') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='MiscIdentifier' AND `tablename`='misc_identifiers' AND `field`='identifier_value' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'MiscIdentifierControl', 'misc_identifier_controls', 'flag_use_for_ccl', 'checkbox',  NULL , '0', '', '', '', 'flag use for ccl', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='form_builder_misc_identifier'), 
(SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='flag_use_for_ccl' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='flag use for ccl' AND `language_tag`=''), 
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');
UPDATE structure_formats SET `display_order`='2' WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_misc_identifier') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='flag_unique' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('flag use for ccl', "Linkable to collection", "Pouvant être lié à la collection");

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'MiscIdentifierControl', 'misc_identifier_controls', 'flag_link_to_study', 'checkbox',  NULL , '0', '', '', '', 'flag use for study', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='form_builder_misc_identifier'), 
(SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='flag_link_to_study' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='flag use for study' AND `language_tag`=''), 
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('flag use for study', "Linked to study (required)", "Lié à une étude (requis)");

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/432): Make MiscIdentifier unique per study when flag_unique = 1 and flag_link_to_study = 1">

ALTER TABLE misc_identifiers DROP INDEX misc_identifier_control_id;
UPDATE structure_fields SET  `language_help`='help_form_builder_misc_identifier_flag_unique' WHERE model='MiscIdentifierControl' AND tablename='misc_identifier_controls' AND field='flag_unique' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('help_form_builder_misc_identifier_flag_unique', 
"When checked, a value can only be assigned once to this type of identifier. Exception exists when the type of identifier is a study identifier. In this exception, a value can only be attributed once to this type of identifier in a given study but more than once when the studies are different.", 
"Lorsque coché, une valeur ne peut être attribuée qu'une seule fois à ce type d'identifiant. Une exception existe lorsque le type d'identifiant est un identifiant d'étude. Dans ce cas précis, une valeur ne pourra être attribuée qu'une seule fois à ce type d'identifiant dans une étude donnée mais plus d'une fois lorsque les études sont différentes.");

-- </editor-fold>

-- <editor-fold desc="Changed message for is unique constraint assigned to misc identifier type">

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('this value can be assigned once to both this identifier and the study',
"This value can only be assigned once to this identifier for the study.",
"Cette valeur ne peut être attribuée qu'une seule fois à cet identifiant pour l'étude."),
('this value can be assigned once to this identifier type',
"This value can only be assigned once to this identifier.",
"Cette valeur ne peut être attribuée qu'une seule fois à cet identifiant.");

-- </editor-fold>

-- <editor-fold desc="Issue (https://gitlab.com/ctrnet/atim/-/issues/433): Add feature to backup aliquot storage information then re-apply position to selected aliquot">

ALTER TABLE `datamart_structure_functions` ADD `options` varchar(255) COLLATE 'latin1_swedish_ci' NULL;

INSERT INTO `datamart_structure_functions` (`datamart_structure_id`, `label`, `link`, `flag_active`, `ref_single_fct_link`, `options`) VALUES
    ((SELECT `id` FROM `datamart_structures` WHERE `model` = 'ViewAliquot'), 'reserve the aliquots positions', '/InventoryManagement/AliquotMasters/memoriseAliquotPositions/', '1', '', 'confirm=multi_entry_form_confirmation_msg'),
    ((SELECT `id` FROM `datamart_structures` WHERE `model` = 'ViewAliquot'), 'restore the aliquots positions', '/InventoryManagement/AliquotMasters/restoreAliquotPositions/', '1', '', 'confirm=multi_entry_form_confirmation_msg');

INSERT IGNORE INTO i18n (`id`, `en`, `fr`) VALUES
	('memorized positions can not be restored', "Memorized aliquot positions cannot be restored.", "Les positions des aliquotes mémorisées ne peuvent pas être restaurées."),
    ('not available aliquots - memorized positions can not be restored', 'Aliquots not available. Memorized aliquot positions cannot be restored.', 'Aliquotes non disponibles. Les positions des aliquotes mémorisées ne peuvent pas être restaurées.'),
	('not available aliquots - position can not be memorized', 'Aliquots not available. Aliquot positions cannot be memorized.', 'Aliquotes non disponibles. Les positions des aliquotes ne peuvent pas être mémorisées.'),
    ('no storage id - position can not be memorized', 'Aliquots with no position. Aliquot positions cannot be memorized.', 'Aliquotes non entreposés. Les positions des aliquotes ne peuvent pas être mémorisées.'),
    ('the storage does not exist anymore - memorized positions can not be restored',
    'Storage does not exists anymore. Memorized aliquot positions cannot be restored.', 'L''entreposage n''existe plus. Les positions des aliquotes mémorisées ne peuvent pas être restaurées.'),
	('the storage selection label changed - memorized positions can not be restored', 
    'The storage selection label change. Memorized aliquot positions cannot be restored.', 'Le label de sélection de l''entreposage a changé. Les positions des aliquotes mémorisées ne peuvent pas être restaurées.'),
    ("reserve the aliquots positions", "Memorise the aliquots positions", "Mémoriser les positions des aliquotes"),
    ("'no storage id for the selected aliquot to memorize'", "These aliquots are not stored.", "Ces aliquotes ne sont pas entreposés."),
    ("not available aliquots", "Aliquots are not available.", "Ces aliquotes ne sont pas disponibles."),
    ("aliquot positions cannot be memorized", "Aliquot positions cannot be memorized.", "Les positions des aliquotes ne peuvent pas être mémorisées."),
    ("error backuping the aliquot positions data - contact administrator", "Error in backing up some aliquot positions, Contact administrator", "Erreur lors de la sauvegarde de certaines positions aliquotes, contactez l''administrateur"),
    ("some aliquot backup positions deleted (%s)", "Some aliquot backup positions deleted (%s)", "Certaines positions de sauvegarde aliquotes supprimées (%s)"),
    ("the aliquot positions not backed up", "The aliquot positions are not in memory. Previous positions can not be reset.", "Les positions des aliquotes ne sont pas en mémoire. Les positions précédentes ne peuvent pas être réinitialisées."),
    ("the storage is not exist", "The storage is not exist", "Le stockage n'existe pas"),
    ("the storage selection label is not the same", "The storage selection label is not the same with backed up data", "L''étiquette de sélection de stockage n''est pas la même avec les données sauvegardées"),
    ("error restoring the aliquot positions data - contact administrator", "Error in restoring some aliquot positions - Contact administrator", "Erreur lors de la restauration de certaines positions aliquotes - Contacter l''administrateur"),
    ("the count parameter is not an array [%s]", "The count parameter is not an array [%s]", "Le paramètre count n''est pas un tableau [%s]"),
    ("you have set more than one element in storage for these aliquots (%s)", "You have set more than one element in storage for these aliquots (%s)", "Vous avez défini plus d''un élément en stockage pour ces aliquotes (%s)"),
    ("restore the aliquots positions", "Restore the aliquots positions", "Restaurer les positions des aliquotes");
-- ("", "", ""),

CREATE TABLE `aliquot_storage_memories`
(
    `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `aliquot_master_id` int(11) NOT NULL,
    `storage_master_id` int(11) NOT NULL,
    `storage_coord_x`   varchar(11) NULL,
    `storage_coord_y`   varchar(11) NULL,
    `selection_label` varchar(60) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '',
    `deleted` tinyint(3) unsigned NOT NULL DEFAULT 0,
    FOREIGN KEY (`aliquot_master_id`) REFERENCES `aliquot_masters` (`id`),
    FOREIGN KEY (`storage_master_id`) REFERENCES `storage_masters` (`id`)
) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/408): InventoryManagement/InventoryActionMasters/listAll/ : sort on sample type odes not work for specimen">
UPDATE structure_fields SET  `model`='InventoryActionMaster' WHERE model='SampleControl' AND tablename='sample_controls' AND field='sample_type' AND `type`='input' AND structure_value_domain  IS NULL ;
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/452): Add Barcode search on the Main Menu">

INSERT INTO `menus` (`id`, `parent_id`, `is_root`, `display_order`, `language_title`, `language_description`, `use_link`, `use_summary`, `flag_active`, `flag_submenu`) VALUES
    ('inv_CAN_9',  'inv_CAN', '1', '4', 'barcode search', '', 'javascript:searchAliquotBtBarcode("/InventoryManagement/AliquotMasters/barcodeSearch/")', '', '1', '1');

INSERT IGNORE INTO i18n (id,en,fr) VALUES
('barcode search', "Search Aliquot by Barcode", "Recherche d''aliquote par code-barres"),
('the query returned too many results. %s', "The query returned too many results and we are showing you %s first records. Try refining the search parameters.", "La requête a renvoyé trop de résultats et nous vous montrons %s premiers enregistrements. Essayez d''affiner les paramètres de recherche.");

INSERT INTO structures(`alias`) VALUES ('aliquot_barcode_search');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='aliquot_barcode_search'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='barcode' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- </editor-fold>

-- <editor-fold desc="I18n clean up">

DELETE FROM i18n WHERE id IN ('all (participant, consent, diagnosis and treatment/annotation)');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES 
("float", "Float", "Valeur décimale"),
("float_positive", "Float Positive", "Valeur décimale positive"),
("display", "Display", "Affichage"),
("input-readonly", "Read Only Text Field", "Champ texte lecture seule"),
("y_n_u", "Yes/No/Unknown", "Oui/Non/Inconnu"),
("button", "Button", "Bouton"),
('this identifier has already been created for this participant and the study',
"his identifier has already been created for this participant and the study and can not be created more than once!",
"Cet identifiant a déjà été créé pour le participant et l'étude et ne peut être créé plus d'une fois!"),
('previous or current aliquot position is currently memorized : %s.', 'The previous (or current) aliquot position is memorized in ATiM : %s.', "La position précédente (ou actuelle) de l'aliquote est mémorisée dans ATiM: %s."),
('all (participant, consent, diagnosis and treatment/annotation)', 
"All linked records (Participant, Consent, Diagnosis, etc.)", 
"Toutes les données attachées (participant, Consentement, Diagnotic, etc.)");

-- </editor-fold>


-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/453): Add aliquot memorized positions in report">
INSERT INTO `datamart_reports` (`name`, `description`, `form_alias_for_search`, `form_alias_for_results`, `form_type_for_results`, `function`, `flag_active`, `associated_datamart_structure_id`, `limit_access_from_datamart_structure_function`) VALUES
("memorized aliquot positions", "find the aliquots with memorized positioned", "view_aliquot_joined_to_sample_and_collection", "view_aliquot_joined_to_sample_and_collection,aliquot_memorized_position_report", "index", "aliquotMemorizedpositionReport", "1", (SELECT `id` FROM `datamart_structures` WHERE `model` = 'ViewAliquot'), "0");

INSERT IGNORE INTO i18n (id,en,fr) VALUES
("aliquot_memorized_position_header",  "Memorized aliquot positions", "Positions d''aliquotes mémorisées"),
("aliquot_memorized_position",  "Positions", "Positions"),
('memorized aliquot positions', "Memorized aliquot positions", "Positions d''aliquotes mémorisées"),
('note aliquot memory', "Memorized aliquot positions Note", "Positions d''aliquotes mémorisées Remarque"),
('find the aliquots with memorized positioned', "Find the aliquots with memorized positioned", "Retrouver les aliquotes avec les positions mémorisées");

INSERT INTO structures(`alias`) VALUES ('aliquot_memorized_position_report');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'AliquotMaster', '', 'note_aliquot_memory', 'input',  NULL , '0', '', '', '', 'aliquot_memorized_position', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='aliquot_memorized_position_report'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='' AND `field`='note_aliquot_memory' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='aliquot_memorized_position' AND `language_tag`=''),
     '-1', '28', 'aliquot_memorized_position_header', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');

SET @control_id = (SELECT id FROM datamart_reports WHERE name = 'memorized aliquot positions');
INSERT INTO `datamart_structure_functions` (`datamart_structure_id`, `label`, `link`, `flag_active`) 
(SELECT id, 'view memorized aliquot positions', CONCAT('/Datamart/Reports/manageReport\/', @control_id), 1
FROM datamart_structures WHERE model IN ('ViewAliquot'));

INSERT IGNORE INTO i18n (id,en,fr) VALUES
('view memorized aliquot positions', "View memorized aliquot positions", "Afficher positions d''aliquotes mémorisées");

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/455): Delete in batch the aliquot memorized positions">
INSERT INTO `datamart_structure_functions` (`datamart_structure_id`, `label`, `link`, `flag_active`, `ref_single_fct_link`, `options`) VALUE
((SELECT `id` FROM `datamart_structures` WHERE `model` = 'ViewAliquot'), 'delete aliquot memorized positions', '/InventoryManagement/AliquotMasters/deleteAliquotMemorizedPositions', '1', '', 'confirm=multi_entry_form_confirmation_msg');

INSERT IGNORE INTO i18n (id,en,fr) VALUES
('delete aliquot memorized positions', "Delete aliquot memorized positions", "Supprimer les positions aliquotes mémorisées");
-- </editor-fold>

-- <editor-fold desc="Created New Sample Type : Rectal swab (https://gitlab.com/ctrnet/atim/-/issues/443)">
INSERT INTO `sample_controls` (`id`, `sample_type`, `sample_category`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`) VALUES
(null, 'rectal swab', 'specimen', 'specimens,sd_spe_rectal_swabs', 'sd_spe_rectal_swabs', 0, 'rectal swab');

CREATE TABLE IF NOT EXISTS `sd_spe_rectal_swabs` (
  `sample_master_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `sd_spe_rectal_swabs_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
ALTER TABLE `sd_spe_rectal_swabs`
  ADD CONSTRAINT `FK_sd_spe_rectal_swabs_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`);

INSERT INTO structures(`alias`) VALUES ('sd_spe_rectal_swabs');

INSERT IGNORE INTO i18n (id,en,fr) VALUES ('rectal swab','Rectal Swab','Frottis rectal');
INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, NULL, (SELECT id FROM sample_controls WHERE sample_type = 'rectal swab'), 0, NULL);
INSERT INTO `aliquot_controls` (`id`, `sample_control_id`, `aliquot_type`, `aliquot_type_precision`, `detail_form_alias`, `detail_tablename`, `volume_unit`, `flag_active`, `comment`, `display_order`, `databrowser_label`) VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'rectal swab'), 'tube', '', 'ad_spec_tubes', 'ad_tubes', '', 0, '', 0, 'rectal swab|tube');

ALTER TABLE sd_spe_rectal_swabs ADD COLUMN transport_medium varchar(100) DEFAULT NULL;
ALTER TABLE sd_spe_rectal_swabs_revs ADD COLUMN transport_medium varchar(100) DEFAULT NULL;
INSERT INTO structure_value_domains (domain_name, source) VALUES ('rectal_swab_transport_mediums', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Rectal Swab Transport Mediums\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('Rectal Swab Transport Mediums', 1, 100, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Rectal Swab Transport Mediums');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("unknown", "Unknown", "Inconnu", "101", "1", @control_id, NOW(), NOW(), @user_id, @user_id);
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'SampleDetail', 'sd_spe_rectal_swabs', 'transport_medium', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='rectal_swab_transport_mediums') , '0', '', '', '', 'transport medium', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sd_spe_rectal_swabs'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_rectal_swabs' AND `field`='transport_medium'), 
'1', '442', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES 
('transport medium', 'Transport Medium', 'Milieu de transport ');
-- </editor-fold>


-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/473): Form mode and active status display and management">
REPLACE INTO i18n (`id`, `en`, `fr`) VALUES
('are you sure to make enable the control', "Are you sure to change the data access for this form from Read-only to Read/Write mode?", "Êtes-vous sûr de changer l'accès aux données pour ce formulaire du mode Lecture seule au mode Lecture/Écriture?"),
('are you sure to make disable the control', "Are you sure to change the data access for this form from Read/Write to Read-only mode?", "Êtes-vous sûr de changer l'accès aux données pour ce formulaire du mode Lecture/Écriture au mode Lecture seule?"),
('make disable the control', "Change the form access mode from Read/Write to Read-only", "Changer le mode d'accès au formulaire de Lecture/Écriture à Lecture seule"),
('make enable the control', "Change the form access mode from Read-only to Read/Write", "Changer le mode d'accès au formulaire de Lecture seule à Lecture/Écriture"),
('all tested', "All Tested", "Tout testé"),
('form or fields in test', "Form or Fields in test", "Formulaire ou champs en test"),
('form is active', "Active", "Actif"),
('form is disabled', "Disabled", "Désactivé"),
('read/write', "Read/Write", "Lecture/écriture"),
('read-only', "Read Only", "Lecture seulement"),
('test mode', "Test Status", "Statut du test"),
('flag active', "Form Status", "État du formulaire"),
('flag active as input', "Data Access Mode", "Mode d'accès aux données"),
('are you sure to activate the control', "Are you sure to activate the form?", "Êtes-vous sûr d'activer le formulaire?"),
('are you sure to deactivate the control', "Are you sure to deactivate the form?", "Êtes-vous sûr désactiver le formulaire?"),
('deactivate the control', "Deavtivate the Form", "Désactiver le formulaire"),
('activate the control', "Activate the Form", "Activer le formulaire"),
('read only mode', "Read Only Mode", "Mode lecture seule"),
('read/write mode', "Read/Write Mode", "Mode lecture/écriture"),
('there are some data related to this form', "There are some data related to this form", "Il y a des données liées à ce formulaire");
-- </editor-fold>

-- <editor-fold desc="Created New Sample Type : Skin swab">
INSERT INTO `sample_controls` (`id`, `sample_type`, `sample_category`, `detail_form_alias`, `detail_tablename`, `display_order`, `databrowser_label`) VALUES
(null, 'skin swab', 'specimen', 'sd_spe_skin_swabs,specimens', 'sd_spe_skin_swabs', 0, 'skin swab');

CREATE TABLE IF NOT EXISTS `sd_spe_skin_swabs` (
  `sample_master_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE IF NOT EXISTS `sd_spe_skin_swabs_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
ALTER TABLE `sd_spe_skin_swabs`
  ADD CONSTRAINT `FK_sd_spe_skin_swabs_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`);

INSERT INTO structures(`alias`) VALUES ('sd_spe_skin_swabs');

INSERT IGNORE INTO i18n (id,en,fr) VALUES ('skin swab','Skin Swab','Frottis de peau');
INSERT INTO `parent_to_derivative_sample_controls` (`id`, `parent_sample_control_id`, `derivative_sample_control_id`, `flag_active`, `lab_book_control_id`) 
VALUES
(null, NULL, (SELECT id FROM sample_controls WHERE sample_type = 'skin swab'), 0, NULL);
INSERT INTO `aliquot_controls` (`id`, `sample_control_id`, `aliquot_type`, `aliquot_type_precision`, `detail_form_alias`, `detail_tablename`, `volume_unit`, `flag_active`, `comment`, `display_order`, `databrowser_label`) VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'skin swab'), 'tube', '', 'ad_spec_tubes', 'ad_tubes', '', 0, '', 0, 'skin swab|tube');

ALTER TABLE sd_spe_skin_swabs ADD COLUMN transport_medium varchar(100) DEFAULT NULL;
ALTER TABLE sd_spe_skin_swabs_revs ADD COLUMN transport_medium varchar(100) DEFAULT NULL;
INSERT INTO structure_value_domains (domain_name, source) VALUES ('skin_swab_transport_mediums', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Skin Swab Transport Mediums\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('Skin Swab Transport Mediums', 1, 100, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Skin Swab Transport Mediums');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("unknown", "Unknown", "Inconnu", "101", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "Autre", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'SampleDetail', 'sd_spe_skin_swabs', 'transport_medium', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='skin_swab_transport_mediums') , '0', '', '', '', 'transport medium', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sd_spe_skin_swabs'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_skin_swabs' AND `field`='transport_medium'), 
'1', '442', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES 
('transport medium', 'Transport Medium', 'Milieu de transport ');

ALTER TABLE sd_spe_skin_swabs ADD COLUMN collection_site varchar(250) DEFAULT NULL;
ALTER TABLE sd_spe_skin_swabs_revs ADD COLUMN collection_site varchar(250) DEFAULT NULL;
UPDATE sd_spe_skin_swabs SET collection_site = 'cheek';
UPDATE sd_spe_skin_swabs_revs SET collection_site = 'cheek';
INSERT INTO structure_value_domains (domain_name, source) VALUES ('skin_swab_collection_sites', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Skin Swab Sites\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('Skin Swab Sites', 1, 100, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Skin Swab Sites');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("unknown", "Unknown", "Inconnu", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "Autre", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'SampleDetail', 'sd_spe_skin_swabs', 'collection_site', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='skin_swab_collection_sites') , '0', 'class=atim-multiple-choice', '', 'help_skin_swab_collection_sites', 'collection site', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sd_spe_skin_swabs'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_skin_swabs' AND `field`='collection_site'), 
'1', '441', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sample_masters_for_collection_tree_view'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_skin_swabs' AND `field`='collection_site'), 
'0', '2', '', '0', '1', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
INSERT INTO `structure_validations` (`id`, `structure_field_id`, `rule`, `on_action`, `language_message`) 
VALUES 
(NULL, 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_skin_swabs' AND `field`='collection_site'), 'notBlank', '', '');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES 
('collection site', 'collection site', 'Zone de prélèvement'),
('help_skin_swab_collection_sites', 'Skin zones of the smears for the skin swab collection.', 'Zones cutanées des frottis pour la collecte des écouvillons cutanés.');

-- </editor-fold>

-- <editor-fold desc="Created New Sample Type : Breast Milk + Supernatants + Cells">
INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'breast milk', 'specimen', 'specimens', 'sd_spe_breast_milks', 0, 'breast milk');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, null, (SELECT id FROM sample_controls WHERE sample_type = 'breast milk'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('breast milk', 'Breast Milk', 'Lait maternel');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'breast milk'), 'tube', '', 'ad_spec_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 'breast milk|tube');

CREATE TABLE `sd_spe_breast_milks` (
  `sample_master_id` int(11) NOT NULL,
  KEY `FK_sd_spe_breast_milks_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_spe_breast_milks_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_spe_breast_milks_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'breast milk supernatant', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_breast_milk_supernatants', 0, 'breast milk supernatant'),
(null, 'breast milk cell', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_breast_milk_cells', 0, 'breast milk cell');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'breast milk'), (SELECT id FROM sample_controls WHERE sample_type = 'breast milk supernatant'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'breast milk'), (SELECT id FROM sample_controls WHERE sample_type = 'breast milk cell'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('breast milk supernatant', 'Breast Milk Supernatant', 'Surnageant de lait maternel'), ('breast milk cell', 'Breast Milk Cell', 'Cellule lait maternel');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'breast milk supernatant'), 'tube', '', 'ad_der_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 'breast milk supernatant|tube'),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'breast milk cell'), 'tube', '', 'ad_der_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 'breast milk cell|tube');

CREATE TABLE `sd_der_breast_milk_supernatants` (
  `sample_master_id` int(11) NOT NULL,
  KEY `FK_sd_der_breast_milk_supernatants_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_der_breast_milk_supernatants_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_der_breast_milk_supernatants_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `sd_der_breast_milk_cells` (
  `sample_master_id` int(11) NOT NULL,
  KEY `FK_sd_der_breast_milk_cells_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_der_breast_milk_cells_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_der_breast_milk_cells_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- </editor-fold>

-- <editor-fold desc="Created New Sample Type : Amniotic Fluid">
INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'amniotic fluid', 'specimen', 'specimens', 'sd_spe_amniotic_fluids', 0, 'amniotic fluid');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, null, (SELECT id FROM sample_controls WHERE sample_type = 'amniotic fluid'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('amniotic fluid', 'Amniotic Fluid', 'Liquide amniotique');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'amniotic fluid'), 'tube', '', 'ad_spec_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 'amniotic fluid|tube');

CREATE TABLE `sd_spe_amniotic_fluids` (
  `sample_master_id` int(11) NOT NULL,
  KEY `FK_sd_spe_amniotic_fluids_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_spe_amniotic_fluids_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_spe_amniotic_fluids_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- </editor-fold>

-- <editor-fold desc="Created New Sample Type : Cell culture supernatant">
INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'cell culture supernatant', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_cell_culture_supernatants', 0, 'cell culture supernatant');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), (SELECT id FROM sample_controls WHERE sample_type = 'cell culture supernatant'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('cell culture supernatant', 'Cell Culture Supernatant', 'Surnageant culture cellulaire');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cell culture supernatant'), 'tube', '', 'ad_der_tubes_incl_ul_vol', 'ad_tubes', 'ul', 0, '', 0, 'cell culture supernatant|tube');

CREATE TABLE `sd_der_cell_culture_supernatants` (
  `sample_master_id` int(11) NOT NULL,
  KEY `FK_sd_der_cell_culture_supernatants_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_der_cell_culture_supernatants_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_der_cell_culture_supernatants_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- </editor-fold>

-- <editor-fold desc="Created New Sample Type : Immortalized cell line">
INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'immortalized cell line', 'derivative', 'sd_der_immortalized_cell_lines,sd_undetailed_derivatives,derivatives', 'sd_der_immortalized_cell_lines', 0, 'immortalized cell line');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), (SELECT id FROM sample_controls WHERE sample_type = 'immortalized cell line'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('immortalized cell line', 'Immortalized Cell Line', 'Lignée cellulaire immortelle');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'immortalized cell line'), 'tube', '', 'ad_der_cell_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 'immortalized cell line|tube');

CREATE TABLE `sd_der_immortalized_cell_lines` (
  `sample_master_id` int(11) NOT NULL,
  `cell_passage_number` int(6) DEFAULT NULL,
  KEY `FK_sd_der_immortalized_cell_lines_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_der_immortalized_cell_lines_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_der_immortalized_cell_lines_revs` (
  `sample_master_id` int(11) NOT NULL,
  `cell_passage_number` int(6) DEFAULT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

SET @cell_line_sample_control_id = (SELECT id FROM sample_controls WHERE sample_type = 'immortalized cell line');
SET @cell_cult_sample_control_id = (SELECT id FROM sample_controls WHERE sample_type = 'cell culture');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
(SELECT null, parent_sample_control_id, @cell_line_sample_control_id, 0, null 
FROM parent_to_derivative_sample_controls 
WHERE derivative_sample_control_id = @cell_cult_sample_control_id AND parent_sample_control_id != @cell_cult_sample_control_id);
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
(SELECT null, @cell_line_sample_control_id, derivative_sample_control_id, 0, null 
FROM parent_to_derivative_sample_controls 
WHERE parent_sample_control_id = @cell_cult_sample_control_id AND derivative_sample_control_id != @cell_cult_sample_control_id);

INSERT INTO structures(`alias`) VALUES ('sd_der_immortalized_cell_lines');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sd_der_immortalized_cell_lines'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='cell_passage_number' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='cell pasage number' AND `language_tag`=''), 
'1', '447', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');
-- </editor-fold>

-- <editor-fold desc="Created New Sample Type : T cells">
INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 't cell', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_t_cells', 0, 't cell');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'blood'), (SELECT id FROM sample_controls WHERE sample_type = 't cell'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('t cell', 'T Cell', 'Cellule T');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 't cell'), 'tube', '', 'ad_der_cell_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 't cell|tube');

CREATE TABLE `sd_der_t_cells` (
  `sample_master_id` int(11) NOT NULL,
  KEY `FK_sd_der_t_cells_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_der_t_cells_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_der_t_cells_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 't cell'), (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 't cell'), (SELECT id FROM sample_controls WHERE sample_type = 'cell lysate'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 't cell'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 't cell'), (SELECT id FROM sample_controls WHERE sample_type = 'immortalized cell line'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 't cell'), (SELECT id FROM sample_controls WHERE sample_type = 'protein'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 't cell'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL); 
-- </editor-fold>

-- <editor-fold desc="Created New Sample Type : Cord Blood Derivatives">
SET @blood_sample_control_id = (SELECT id FROM sample_controls WHERE sample_type = 'blood');
SET @cord_blood_sample_control_id = (SELECT id FROM sample_controls WHERE sample_type = 'cord blood');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
(SELECT null, @cord_blood_sample_control_id, derivative_sample_control_id, 0, null FROM parent_to_derivative_sample_controls WHERE parent_sample_control_id = @blood_sample_control_id);

INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'stem cell', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_stem_cells', 0, 'stem cell');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cord blood'), (SELECT id FROM sample_controls WHERE sample_type = 'stem cell'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('stem cell', 'Stem Cell', 'Cellule souche');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'stem cell'), 'tube', '', 'ad_der_cell_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 'stem cell|tube');

CREATE TABLE `sd_der_stem_cells` (
  `sample_master_id` int(11) NOT NULL,
  KEY `FK_sd_der_stem_cells_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_der_stem_cells_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_der_stem_cells_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- </editor-fold>

-- <editor-fold desc="Add blood details fields to cord blood detail form (https://gitlab.com/ctrnet/atim/-/issues/482)">
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sd_spe_cord_bloods'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='blood_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='blood_type')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='blood tube type' AND `language_tag`=''), 
'1', '441', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='sd_spe_cord_bloods'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='collected_tube_nbr' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='collected tubes nbr' AND `language_tag`=''), 
'1', '442', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='sd_spe_cord_bloods'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='collected_volume' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='collected volume' AND `language_tag`=''), 
'1', '443', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='sd_spe_cord_bloods'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='' AND `field`='collected_volume_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_volume_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'1', '444', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

ALTER TABLE sd_spe_cord_bloods
  	ADD COLUMN `blood_type` varchar(30) DEFAULT NULL,
  	ADD COLUMN `collected_tube_nbr` int(4) DEFAULT NULL,
   	ADD COLUMN `collected_volume` decimal(10,5) DEFAULT NULL,
  	ADD COLUMN `collected_volume_unit` varchar(20) DEFAULT NULL;

ALTER TABLE sd_spe_cord_bloods_revs
  	ADD COLUMN `blood_type` varchar(30) DEFAULT NULL,
  	ADD COLUMN `collected_tube_nbr` int(4) DEFAULT NULL,
   	ADD COLUMN `collected_volume` decimal(10,5) DEFAULT NULL,
  	ADD COLUMN `collected_volume_unit` varchar(20) DEFAULT NULL;
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/470): Treatment Form Duplication & treatment_controls.extended_data_import_process">
INSERT INTO structures(`alias`) VALUES ('form_builder_treatment_import_process');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='tx_method' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=control-name' AND `default`='' AND `language_help`='' AND `language_label`='treatment_method_label' AND `language_tag`=''),
     '1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='disease_site' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='disease_site_help' AND `language_label`='disease_site_label' AND `language_tag`=''),
     '1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='applied_protocol_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='protocol id')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='applied_protocol_control_id_help' AND `language_label`='applied_protocol_control_id_label' AND `language_tag`=''),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '1', '0', '0', '1', '1', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='display_order' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='display order' AND `language_tag`=''),
     '1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='treatment_extend_control_id' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='tx_precision_id')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='treatment_extend_control_id_help' AND `language_label`='treatment_extend_control_id_label' AND `language_tag`=''),
     '1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '1', '0', '0', '1', '1', '1', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_use_for_ccl' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_use_for_ccl' AND `language_label`='flag_use_for_ccl_label' AND `language_tag`=''),
     '1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='use_addgrid' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_use_addgrid' AND `language_label`='use_addgrid_label' AND `language_tag`=''),
     '1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='use_detail_form_for_index' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_use_detail_form_for_index' AND `language_label`='use_detail_form_for_index_label' AND `language_tag`=''),
     '1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_test_mode' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='test mode help' AND `language_label`='test mode' AND `language_tag`=''),
     '1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_active' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_fb' AND `language_label`='flag active' AND `language_tag`=''),
     '1', '75', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_active_input' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_flag_active_input_fb' AND `language_label`='flag active as input' AND `language_tag`=''),
     '1', '80', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_treatment_import_process'),
     (SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='databrowser_label' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='databrowser label help' AND `language_label`='databrowser label' AND `language_tag`=''),
     '1', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
-- </editor-fold>

-- <editor-fold desc="Cord Blood Tubes : Missing volume/units fields (https://gitlab.com/ctrnet/atim/-/issues/483)">
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ad_spec_tubes_incl_ul_vol_and_conc'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='coll_to_stor_spent_time_msg' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='inv_coll_to_stor_spent_time_msg_defintion' AND `language_label`='collection to storage spent time' AND `language_tag`=''), 
'1', '59', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_spec_tubes_incl_ul_vol_and_conc'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='coll_to_stor_spent_time_msg' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '59', '', '0', '1', 'collection to storage spent time (min)', '0', '', '0', '', '1', 'integer_positive', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_spec_tubes_incl_ul_vol_and_conc'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='creat_to_stor_spent_time_msg' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='inv_creat_to_stor_spent_time_msg_defintion' AND `language_label`='creation to storage spent time' AND `language_tag`=''), 
'1', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_spec_tubes_incl_ul_vol_and_conc'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='creat_to_stor_spent_time_msg' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '60', '', '0', '1', 'creation to storage spent time (min)', '0', '', '0', '', '1', 'integer_positive', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_spec_tubes_incl_ul_vol_and_conc'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='lot_number' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='' AND `language_label`='lot number' AND `language_tag`=''), 
'1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_spec_tubes_incl_ul_vol_and_conc'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='current_volume' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='current volume' AND `language_tag`=''), 
'1', '71', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_spec_tubes_incl_ul_vol_and_conc'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'1', '72', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ad_spec_tubes_incl_ul_vol_and_conc'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='volume_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'1', '74', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '1', '1', '1', '0', '0', '1', '1', '1', '1', '0', '0', '0', '1', '0', '0');
-- </editor-fold>

-- <editor-fold desc="Vaginal Swab Medium Transport">
ALTER TABLE sd_spe_vaginal_swabs ADD COLUMN transport_medium varchar(100) DEFAULT NULL;
ALTER TABLE sd_spe_vaginal_swabs_revs ADD COLUMN transport_medium varchar(100) DEFAULT NULL;
UPDATE sample_controls SET detail_form_alias = CONCAT(detail_form_alias, ',sd_spe_vaginal_swabs') WHERE sample_type = 'vaginal swab';
INSERT INTO structures(`alias`) VALUES ('sd_spe_vaginal_swabs');
INSERT INTO structure_value_domains (domain_name, source) VALUES ('vaginal_swab_transport_mediums', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Vaginal Swab Transport Mediums\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('Vaginal Swab Transport Mediums', 1, 100, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Vaginal Swab Transport Mediums');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("unknown", "Unknown", "Inconnu", "101", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "Autre", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'SampleDetail', 'sd_spe_vaginal_swabs', 'transport_medium', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='vaginal_swab_transport_mediums') , '0', '', '', '', 'transport medium', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sd_spe_vaginal_swabs'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_vaginal_swabs' AND `field`='transport_medium'), 
'1', '442', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');
-- </editor-fold>

-- <editor-fold desc="Replace sample precision fields (blood_type, collection_sites) in collection tree view structure (sample_masters_for_collection_tree_view) by generated data field ">
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'Generated', '', 'sample_precision_for_tree_view', 'input',  NULL , '0', '', '', '', '-', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sample_masters_for_collection_tree_view'), 
(SELECT id FROM structure_fields WHERE `model`='Generated' AND `tablename`='' AND `field`='sample_precision_for_tree_view' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='-' AND `language_tag`=''), 
'0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='sample_masters_for_collection_tree_view') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='SampleDetail' AND `tablename`='' AND `field`='blood_type' AND `language_label`='blood tube type' AND `language_tag`='' AND `type`='select' AND `setting`='' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='blood_type') AND `language_help`='' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='sample_masters_for_collection_tree_view') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='SampleDetail' AND `tablename`='sd_spe_urt_cheek_swabs' AND `field`='collection_site' AND `language_label`='collection site' AND `language_tag`='' AND `type`='select' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='urt_cheek_swab_collection_sites') AND `language_help`='help_urt_cheek_swab_collection_sites' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='sample_masters_for_collection_tree_view') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='InventoryManagement' AND `model`='SampleDetail' AND `tablename`='sd_spe_skin_swabs' AND `field`='collection_site' AND `language_label`='collection site' AND `language_tag`='' AND `type`='select' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='skin_swab_collection_sites') AND `language_help`='help_skin_swab_collection_sites' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');
-- </editor-fold>

-- <editor-fold desc="Simplify the management of the clinicalcollectionlinks fields to display according to the allowed linkable object treatment, event, ....">
INSERT INTO structures(`alias`) 
VALUES 
('clinicalcollectionlinks_misc_id'), 
('clinicalcollectionlinks_treatment'), 
('clinicalcollectionlinks_diagnosis'), 
('clinicalcollectionlinks_event'), 
('clinicalcollectionlinks_consent');
SET @structure_id = (SELECT id FROM structures WHERE alias='clinicalcollectionlinks');

SET @new_structure_id = (SELECT id FROM structures WHERE alias='clinicalcollectionlinks_misc_id');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
(SELECT @new_structure_id, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`
FROM structure_formats WHERE structure_id = @structure_id
AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model` IN ('MiscIdentifier', 'MiscIdentifierControl')));

SET @new_structure_id = (SELECT id FROM structures WHERE alias='clinicalcollectionlinks_treatment');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
(SELECT @new_structure_id, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`
FROM structure_formats WHERE structure_id = @structure_id
AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model` IN ('TreatmentControl', 'TreatmentMaster', 'TreatmentDetail')));

SET @new_structure_id = (SELECT id FROM structures WHERE alias='clinicalcollectionlinks_diagnosis');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
(SELECT @new_structure_id, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`
FROM structure_formats WHERE structure_id = @structure_id
AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model` IN ('DiagnosisControl', 'DiagnosisMaster', 'DiagnosisDetail')));

SET @new_structure_id = (SELECT id FROM structures WHERE alias='clinicalcollectionlinks_consent');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
(SELECT @new_structure_id, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`
FROM structure_formats WHERE structure_id = @structure_id
AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model` IN ('ConsentControl', 'ConsentMaster', 'ConsentDetail')));

SET @structure_id = (SELECT id FROM structures WHERE alias='clinicalcollectionlinks');
SET @new_structure_id = (SELECT id FROM structures WHERE alias='clinicalcollectionlinks_event');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
(SELECT @new_structure_id, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`
FROM structure_formats WHERE structure_id = @structure_id
AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model` IN ('EventControl', 'EventMaster', 'EventDetail')));

DELETE FROM structure_formats
WHERE structure_id =  @structure_id
AND structure_field_id IN (SELECT id FROM structure_fields WHERE `model` IN (
'ConsentControl', 'ConsentMaster', 'ConsentDetail',
'MiscIdentifier', 'MiscIdentifierControl',
'DiagnosisControl', 'DiagnosisMaster', 'DiagnosisDetail',
'TreatmentControl', 'TreatmentMaster', 'TreatmentDetail',
'EventControl', 'EventMaster', 'EventDetail'
)); 
-- </editor-fold>

-- <editor-fold desc="New version set up function revision">
INSERT IGNORE INTO
i18n (id,en,fr)
VALUES 	
("structures miscidentifiers and miscidentifiers_for_participant_search have been updated to let people to create Study Participant Identifier and search on this field.",
"The structures 'miscidentifiers' and 'miscidentifiers_for_participant_search' have been updated to display 'Participant Study Identifier' and 'Study' fields.",
"Les structures 'miscidentifiers' et 'miscidentifiers_for_participant_search' ont été mises à jour pour afficher les champs 'Identifiant d'étude du participant' et 'Étude'.");
INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
("structures miscidentifiers and miscidentifiers_for_participant_search have been updated to not let people to create Study Participant Identifier and search on this field.",
"The structures 'miscidentifiers' and 'miscidentifiers_for_participant_search' have been updated to hide 'Participant Study Identifier' and 'Study' fields.",
"Les structures 'miscidentifiers' et 'miscidentifiers_for_participant_search' ont été mises à jour pour cacher les champs 'Identifiant d'étude du participant' et 'Étude'.");

INSERT IGNORE INTO
i18n (id,en,fr)
VALUES 	
("participant study identifier fields have been displayed in structures view_collection, view_sample_joined_to_collection and view_aliquot_joined_to_sample_and_collection.",
"The 'Participant Study Identifier' structure fields have been added to 'view_collection', 'view_sample_joined_to_collection, and 'view_aliquot_joined_to_sample_and_collection' structures.",
"Les champs 'Identifiant d'étude du participant' ont été ajoutés aux structures 'view_collection', 'view_sample_joined_to_collection, et 'view_aliquot_joined_to_sample_and_collection'."),

("participant study identifier fields have been hidden in structures view_collection, view_sample_joined_to_collection and view_aliquot_joined_to_sample_and_collection.",
"The 'Participant Study Identifier' structure fields have been removed from 'view_collection', 'view_sample_joined_to_collection, and 'view_aliquot_joined_to_sample_and_collection' structures.",
"Les champs 'Identifiant d'étude du participant' ont été supprimés des structures 'view_collection', 'view_sample_joined_to_collection, et 'view_aliquot_joined_to_sample_and_collection'.");

DELETE FROM i18n WHERE id IN ("structure 'consent_masters' and 'datamart_browsing_controls' have been updated to let people to create 'Study Consent' and search on this field.",
"structure 'consent_masters' and 'datamart_browsing_controls' have been updated to not let people to create 'Study Consent' and search on this field.");
REPLACE INTO
i18n (id,en,fr)
VALUES 	
("structure 'consent_masters' and 'datamart_browsing_controls' have been updated to let people to create 'Study Consent' and search on this field.",
"The structure 'consent_masters' has been updated to display 'Consent Study' fields.",
"La structure 'consent_masters' a été mise à jour pour afficher les champs des 'Consentements d'étude'.");
INSERT IGNORE INTO
i18n (id,en,fr)
VALUES 	
("structure 'consent_masters' and 'datamart_browsing_controls' have been updated to not let people to create 'Study Consent' and search on this field.",
"The structure 'consent_masters' has been updated to hide 'Consent Study' fields.",
"La structure 'consent_masters' a été mise à jour pour ne pas afficher les champs des 'Consentements d'étude'.");
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/491): Form Builder: Consent - Add option to link new form to study as it is for misc identifier forms.">

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('ClinicalAnnotation', 'ConsentControl', 'consent_controls', 'flag_link_to_study', 'checkbox',  NULL , '0', '', '', '', 'flag use for study', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_consent'),
     (SELECT id FROM structure_fields WHERE `model`='ConsentControl' AND `tablename`='consent_controls' AND `field`='flag_link_to_study' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='flag use for study' AND `language_tag`=''),
     '1', '15', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/490): Form Builder: Index view vs detail view - Status value not consistent">

    -- <editor-fold desc="Create the Form Builder Test Status List">
INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("Form Builder Test Status", "", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("3", "form and fields in test");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="Form Builder Test Status"), (SELECT id FROM structure_permissible_values WHERE value="3" AND language_alias="form and fields in test"), "1", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("0", "all tested");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="Form Builder Test Status"), (SELECT id FROM structure_permissible_values WHERE value="0" AND language_alias="all tested"), "4", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("1", "field in test");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="Form Builder Test Status"), (SELECT id FROM structure_permissible_values WHERE value="1" AND language_alias="field in test"), "3", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("2", "form in test");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="Form Builder Test Status"), (SELECT id FROM structure_permissible_values WHERE value="2" AND language_alias="form in test"), "2", "1");
    -- </editor-fold>

    -- <editor-fold desc="Create the Form Builder Form Status List">
INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("Form Builder Form Status", "", "", NULL);
INSERT IGNORE INTO structure_permissible_values (`value`, language_alias) VALUES("1", "form is active");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="Form Builder Form Status"), (SELECT id FROM structure_permissible_values WHERE `value`="1" AND language_alias="form is active"), "1", "1");
INSERT IGNORE INTO structure_permissible_values (`value`, language_alias) VALUES("0", "form is disabled");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="Form Builder Form Status"), (SELECT id FROM structure_permissible_values WHERE `value`="0" AND language_alias="form is disabled"), "2", "1");
-- </editor-fold>

    -- <editor-fold desc="Create the Form Builder Data Access Mode List">
INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("Form Builder Data Access Mode", "", "", NULL);
INSERT IGNORE INTO structure_permissible_values (`value`, language_alias) VALUES("1", "read/write");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="Form Builder Data Access Mode"), (SELECT id FROM structure_permissible_values WHERE `value`="1" AND language_alias="read/write"), "1", "1");
INSERT IGNORE INTO structure_permissible_values (`value`, language_alias) VALUES("0", "read-only");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="Form Builder Data Access Mode"), (SELECT id FROM structure_permissible_values WHERE `value`="0" AND language_alias="read-only"), "2", "1");
    -- </editor-fold>

    -- <editor-fold desc="Update ConsentControl">
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')  WHERE model='ConsentControl' AND tablename='consent_controls' AND field='flag_test_mode' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Form Status')  WHERE model='ConsentControl' AND tablename='consent_controls' AND field='flag_active' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Data Access Mode')  WHERE model='ConsentControl' AND tablename='consent_controls' AND field='flag_active_input' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `model`='FunctionManagement',  `tablename`='',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')  WHERE model='ConsentControl' AND tablename='consent_controls' AND field='flag_test_mode' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status');
-- </editor-fold>

    -- <editor-fold desc="Update AliquotControl">
UPDATE structure_fields SET  `tablename`='aliquot_controls',  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')  WHERE model='AliquotControl' AND tablename='sample_controls' AND field='flag_test_mode' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `tablename`='aliquot_controls',  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Form Status')  WHERE model='AliquotControl' AND tablename='sample_controls' AND field='flag_active' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Data Access Mode')  WHERE model='AliquotControl' AND tablename='aliquot_controls' AND field='flag_active_input' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='flag_test_mode' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_aliquot') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotControl' AND `tablename`='aliquot_controls' AND `field`='flag_test_mode' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status') AND `flag_confidential`='0');
    -- </editor-fold>

    -- <editor-fold desc="Update InventoryActionControl">
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')  WHERE model='InventoryActionControl' AND tablename='inventory_action_controls' AND field='flag_test_mode' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Form Status')  WHERE model='InventoryActionControl' AND tablename='inventory_action_controls' AND field='flag_active' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Data Access Mode')  WHERE model='InventoryActionControl' AND tablename='inventory_action_controls' AND field='flag_active_input' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='flag_test_mode' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_inventory_action') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='flag_test_mode' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status') AND `flag_confidential`='0');
    -- </editor-fold>

    -- <editor-fold desc="Update SampleControl">
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')  WHERE model='SampleControl' AND tablename='sample_controls' AND field='flag_test_mode' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Form Status')  WHERE model='SampleControl' AND tablename='sample_controls' AND field='flag_active' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Data Access Mode')  WHERE model='SampleControl' AND tablename='sample_controls' AND field='flag_active_input' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='flag_test_mode' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_sample') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='flag_test_mode' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status') AND `flag_confidential`='0');
    -- </editor-fold>

    -- <editor-fold desc="Update TreatmentExtendControl">
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')  WHERE model='TreatmentExtendControl' AND tablename='treatment_extend_controls' AND field='flag_test_mode' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Form Status')  WHERE model='TreatmentExtendControl' AND tablename='treatment_extend_controls' AND field='flag_active' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Data Access Mode')  WHERE model='TreatmentExtendControl' AND tablename='treatment_extend_controls' AND field='flag_active_input' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='flag_test_mode' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_treatment_extend') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='TreatmentExtendControl' AND `tablename`='treatment_extend_controls' AND `field`='flag_test_mode' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status') AND `flag_confidential`='0');
    -- </editor-fold>

    -- <editor-fold desc="Update TreatmentControl">
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'flag_test_mode', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status') , '0', '', '', 'test mode help', 'test mode', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'flag_active', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Form Status') , '0', '', '', 'help_flag_active_fb', 'flag active', ''),
    ('ClinicalAnnotation', 'TreatmentControl', 'treatment_controls', 'flag_active_input', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Data Access Mode') , '0', '', '', 'help_flag_active_input_fb', 'flag active as input', '');
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_test_mode' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status') ) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_treatment') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_test_mode' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_active' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Form Status') ) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_treatment') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_active' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_active_input' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Data Access Mode') ) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_treatment') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_active_input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='flag_test_mode' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_treatment') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='flag_test_mode' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status') AND `flag_confidential`='0');
    -- </editor-fold>

    -- <editor-fold desc="Update EventControl">
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')  WHERE model='EventControl' AND tablename='event_controls' AND field='flag_test_mode' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Form Status')  WHERE model='EventControl' AND tablename='event_controls' AND field='flag_active' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Data Access Mode')  WHERE model='EventControl' AND tablename='event_controls' AND field='flag_active_input' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `flag_override_setting`='0', `setting`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_event') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='event_group' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='event_group_list') AND `flag_confidential`='0');
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='flag_test_mode' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_event') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='flag_test_mode' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status') AND `flag_confidential`='0');
    -- </editor-fold>

    -- <editor-fold desc="Update DiagnosisControl">
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')  WHERE model='DiagnosisControl' AND tablename='diagnosis_controls' AND field='flag_test_mode' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Form Status')  WHERE model='DiagnosisControl' AND tablename='diagnosis_controls' AND field='flag_active' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Data Access Mode')  WHERE model='DiagnosisControl' AND tablename='diagnosis_controls' AND field='flag_active_input' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `flag_override_setting`='0', `setting`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_diagnosis') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='category' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='diagnosis_category') AND `flag_confidential`='0');
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='flag_test_mode' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_diagnosis') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='flag_test_mode' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status') AND `flag_confidential`='0');
-- </editor-fold>

    -- <editor-fold desc="Update MiscIdentifierControl">
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')  WHERE model='MiscIdentifierControl' AND tablename='misc_identifier_controls' AND field='flag_test_mode' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Form Status')  WHERE model='MiscIdentifierControl' AND tablename='misc_identifier_controls' AND field='flag_active' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `type`='select',  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Data Access Mode')  WHERE model='MiscIdentifierControl' AND tablename='misc_identifier_controls' AND field='flag_active_input' AND `type`='checkbox' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='flag_test_mode' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status')) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_misc_identifier') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='MiscIdentifierControl' AND `tablename`='misc_identifier_controls' AND `field`='flag_test_mode' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='Form Builder Test Status') AND `flag_confidential`='0');
    -- </editor-fold>

-- </editor-fold>


-- <editor-fold desc="Re-order MiscIdentifiier in inventory views"
UPDATE structure_fields SET `language_label`='',  `language_tag`='collection_misc_identifier_type' WHERE model='ViewCollection' AND tablename='' AND field='misc_identifier_name' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list');
UPDATE structure_fields SET  `language_tag`='collection_misc_identifier_study_title' WHERE model='ViewCollection' AND tablename='' AND field='title' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_label`='collection_misc_identifier' WHERE model='ViewCollection' AND tablename='' AND field='identifier_value' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `display_order`='3' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='misc_identifier_name' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='4' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='2' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='identifier_value' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

UPDATE structure_fields SET `language_label`='',  `language_tag`='collection_misc_identifier_type' WHERE model='ViewSample' AND tablename='' AND field='misc_identifier_name' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list');
UPDATE structure_fields SET  `language_label`='collection_misc_identifier' WHERE model='ViewSample' AND tablename='' AND field='identifier_value' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `display_order`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='misc_identifier_name' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='-1' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_sample_joined_to_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewSample' AND `tablename`='' AND `field`='identifier_value' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

UPDATE structure_fields SET `language_label`='',  `language_tag`='collection_misc_identifier_type' WHERE model='ViewAliquot' AND tablename='' AND field='misc_identifier_name' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list');
UPDATE structure_fields SET  `language_label`='collection_misc_identifier' WHERE model='ViewAliquot' AND tablename='' AND field='identifier_value' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `display_order`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='misc_identifier_name' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='-1' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='identifier_value' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT IGNORE INTO i18n (id,en,fr) VALUE 
("collection_misc_identifier_study_title", "-", "-"),
("collection_misc_identifier", "Participant's Collection Identifier", "Identification de collect. du partic."),
("collection_misc_identifier_type", "Type", "Type");
-- </editor-fold>

-- <editor-fold desc="Missing i18n / i18n correction"

UPDATE structure_fields SET language_label = 'flag action in batch - apply to all'
WHERE model = 'InventoryActionControl' AND field = 'flag_batch_action' AND type = 'checkbox';


UPDATE structure_fields SET language_label = 'flag action in batch - apply to all'
WHERE model = 'InventoryActionControl' AND field = 'flag_batch_action' AND type = 'checkbox';

DELETE FROM i18n WHERE id IN (
'clinical',
"ia_help_flag_action_in_batch", 
'flag action in batch - apply to all',
"fb_ the form cannot be used in batch action and have unique field at the same time", 
'treatment_extend_control_id_help', 
'applied_protocol_control_id_help', 
'treatment_extend_control_id_label',
'treatment extend', 
'treatment_extend_control_type',
'please select a suggested value from the list.'
);

REPLACE INTO
i18n (id,en,fr)
VALUES 	
('clinical', 'Clinical', 'Clinique'),

("ia_help_flag_action_in_batch", 
  "Allows you to create the same event/annotation for several aliquots/samples by completing a form that will be duplicated for each of the selected aliquots/samples.",
  "Permet de créer un même événement/annotation pour plusieurs aliquotes/échantillons en complétant un formulaire qui sera dupliqué pour chacun des aliquotes/échantillons sélectionnés."),

('flag action in batch - apply to all', "Batch creation by (one form) duplication", "Création par lot par duplication (d'un formulaire)"),
("fb_ the form cannot be used in batch action and have unique field at the same time", 
  "A form with a field defined as unique cannot be used in a batch action.", 
  "Le formulaire ne peut pas être utilisé dans une action par lots et possèder en même temps un champ unique."),

('treatment_extend_control_id_help', 
	"Optional! Determine the name of the treatment precision form that is related to this treatment.<br><b>Note: </b>The treatment precision form must have been created previously..", 
    "Facultatif! Déterminez le type/nom du formulaire de précision du traitement associé à ce traitement.<br><b>Remarque: </b> Le formulaire de précision du traitement doit avoir été créé précédement."),
('applied_protocol_control_id_help', 
	"Optional, Determine the name of the protocol that is related to this treatment.<br><b>Note: </b>The protocol should be allready defined.", 
	"Facultatif, Déterminez le nom de protocol associée à ce traitement.<br><b>Remarque: </b> Le protocol doit déjà être définie."),
('treatment_extend_control_id_label', "Treatment Precision", "Précision du traitement"),

('treatment extend', "Treatment Precision", "Précision du traitement"),
('treatment_extend_control_type', "Type", "Type"),

('please select a suggested value from the list.', 
	"Please select a suggested storage 'selection label' value from the dynamic list to make the position selection function to work.",
	"Veuillez sélectionner une valeur suggérée de 'label de sélection' d'entreposage dans la liste dynamique pour que la fonction de sélection de position fonctionne.");

DELETE FROM i18n WHERE id IN (
'treatment_method_label',
'applied_protocol_control_id_label',
'treatment_extend_control_id_help',
'applied_protocol_control_id_help',
'treatment_extend_control_id_label',
'diagnosis type',
'event_type_label',
'event_group',
'event_form_type',
'inventory action category',
'inventory action type'
);
REPLACE INTO
i18n (id,en,fr)
VALUES 	
('inventory action category', 'Category', 'Catégorie'),
('inventory action type', "Type", "Type"),
('event_form_type', "Type", "Type"),
('event_group', 'Category', 'Catégorie'),
('event_type_label', "Type", "Type"),
('diagnosis type', "Type", "Type"),
('treatment_method_label', "Type", "Type"),
('applied_protocol_control_id_label', "Protocol Type (to apply)", "Type de protocole (à appliquer)"),
 ('treatment_extend_control_id_help', 
    "Optional! Determine the name of the treatment precision form that is related to this treatment.<br><b>Note: </b>The treatment precision form must have been created previously..", 
    "Facultatif! Déterminez le type/nom du formulaire de précision du traitement associé à ce traitement.<br><b>Remarque: </b> Le formulaire de précision du traitement doit avoir été créé précédement."),
('applied_protocol_control_id_help', "Optional, Determine the name of the protocol that is related to this treatment.<br><b>Note: </b>The protocol should be allready defined.", "Facultatif, Déterminez le nom de protocol associée à ce traitement.<br><b>Remarque: </b> Le protocol doit déjà être définie."),
('treatment_extend_control_id_label', "Treatment Precision (form)", "Précision du traitement (formulaire)");

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('field in test', 'Field(s) In Test', 'Champ(s) en test'),
('form in test', 'Form In Test', 'Formulaire en test'), 
('form and fields in test', 'Form And Fields In Test', 'Formulaire et champs en test');

DELETE FROM i18n WHERE id IN ('test mode help', 'help_flag_active_fb', 'help_flag_active_input_fb');				

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('help_flag_active_fb', 
"Determines whether the control is active or not meaning available to the participants for data input or only research (according to access mode).", 
"Détermine si le contrôle est actif ou non, c'est-à-dire disponible aux participants pour la saisie de données ou uniquement la recherche de données (selon le mode d'accès)"),
('help_flag_active_input_fb', 
"Determines whether the control can be used as an input for new data or only for research (read only).", 
"Détermine si le contrôle peut être utilisé pour la saisie de nouvelles données ou uniquement la recherche (lecture seule)."),
('test mode help', 
"Defines if the entire form is in test (never moved from test to prod) or if new fields have been attached to the form previously moved in production and requiring a new validation (move from test to prod) to be part of the form for user data input.", 
"Définit si tout le formulaire est en test (jamais déplacé de test en prod) ou si de nouveaux champs ont été attachés au formulaire précédemment déplacé en production et nécessitant une nouvelle validation (passage de test en prod) pour faire partie du formulaire de saisie des données utilisateur.");
-- </editor-fold>

-- <editor-fold desc="Changed event_controls / treatment_controls field disease_site from text to drop down list"
UPDATE structure_formats 
SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='disease_site' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='event_disease_site_list')) 
WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_event') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='disease_site' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
DELETE FROM structure_validations WHERE structure_field_id IN (SELECT id FROM structure_fields WHERE (model='EventControl' AND tablename='event_controls' AND field='disease_site' AND `type`='input' AND structure_value_domain IS NULL ));
DELETE FROM structure_fields WHERE (model='EventControl' AND tablename='event_controls' AND field='disease_site' AND `type`='input' AND structure_value_domain IS NULL );
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='disease_site' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='tx_disease_site_list') ) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_treatment') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='disease_site' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_label`='1', `language_label`='disease_site_label', `flag_override_tag`='1', `language_tag`='', `flag_override_help`='1', `language_help`='disease_site_help' WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_event') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='disease_site' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='event_disease_site_list') AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_label`='1', `language_label`='disease_site_label', `flag_override_help`='1', `language_help`='disease_site_help' WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_treatment') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='disease_site' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='tx_disease_site_list') AND `flag_confidential`='0');

INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('Treatment/Clinical Event Diseases/Sites', 1, 50, '');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Treatment/Clinical Event Diseases/Sites');
SET @user_id = 2;
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`)
(SELECT DISTINCT disease_site,en,fr, '1', '1', @control_id, NOW(), NOW(), @user_id, @user_id
FROM (
SELECT disease_site FROM event_controls
UNION  
SELECT disease_site FROM treatment_controls
)RES LEFT JOIN i18n
ON RES.disease_site = i18n.id);

SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Treatment/Clinical Event Diseases/Sites');
SET @user_id = 2;
DELETE FROM structure_permissible_values_customs 
WHERE control_id = @control_id
AND value IN ('breast', 'small intestine', 'bile duct', 'pancreas', 'liver', 'gallbladder', 'colon/rectum', 'ampulla of vater', 'general', 'prostate');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`)
VALUES
("breast", "Breast", "Sein", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("small intestine", "Small Intestine", "Intestin grêle", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("bile duct", "Bile Duct", "Voie biliaire", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("pancreas", "Pancreas", "Pancréas", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("liver", "Liver", "Foie", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("gallbladder", "Gallbladder", "Vésicule biliaire", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("colon/rectum", "Colon/Rectum", "Côlon/rectum", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ampulla of vater", "Ampulla of Vater", "Ampoule de Vater", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("general", "General", "Général", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("prostate", "Prostate", "Prostate", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

UPDATE structure_value_domains SET source = "StructurePermissibleValuesCustom::getCustomDropdown('Treatment/Clinical Event Diseases/Sites')"
WHERE domain_name IN ('event_disease_site_list', 'tx_disease_site_list');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/494): FromBuiilder - Treatment & TreatmentExtend">
REPLACE INTO i18n (id,en,fr)
VALUES
('the treatment precision related to this treatment not exist or inactive', 'The Treatment Precision related to this Treatment not exist or inactive', "La précision de traitement liée à ce traitement n'existe pas ou est inactive"),
('there are some active treatments related to this treatment precision', 'There are some active treatments related to this treatment precision', "Il existe des traitements actifs liés à cette précision de traitement");
-- </editor-fold>

-- <editor-fold desc="Change study management in consent form to display study field or not based on ConsentControl.flag_link_to_study field"
UPDATE structure_formats 
SET `flag_search`='0' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='consent_masters_study') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

UPDATE structure_formats 
SET `flag_override_setting`='1', `setting`='size=40,placeholder=Study/Etude (if/si applicable)', 
`flag_search`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='consent_masters') 
AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

UPDATE consent_controls SET detail_form_alias = REPLACE(detail_form_alias, 'consent_masters_study,', '');
UPDATE consent_controls SET detail_form_alias = REPLACE(detail_form_alias, ',consent_masters_study', '');
UPDATE consent_controls SET detail_form_alias = REPLACE(detail_form_alias, 'consent_masters_study', '');
-- </editor-fold>

-- <editor-fold desc="Change study management in inventory action form to display study field or not based on flag_link_to_study field in inventory_action_masters structure"
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='inventory_action_masters'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '9', '', '0', '1', 'study / project', '0', '', '0', '', '0', '', '1', 'size=40,placeholder=Study/Etude (if/si applicable)', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
INSERT IGNORE INTO
i18n (id,en,fr)
VALUES 	
("structure 'inventory_action_masters' has been updated to let people to search on study field.",
"The structure 'inventory_action_masters' has been updated to display 'Study' field.",
"La structures 'inventory_action_masters' a été mise à jour pour afficher le champs 'Étude'.");
INSERT IGNORE INTO 
i18n (id,en,fr)
VALUES 	
("structure 'inventory_action_masters' has been updated to not let people to search on study field.",
"The structure 'inventory_action_masters' has been updated to hide 'Study' field.",
"La structures 'inventory_action_masters' a été mise à jour pour cacher le champs 'Étude'.");
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/498): Unable to add Diagnosis in FB if there is any Dx active in db">
INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("diagnosis_category_fix_list", "", "", NULL);
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="diagnosis_category_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="primary" AND language_alias="primary"), "1", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("secondary - distant", "secondary - distant");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="diagnosis_category_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="secondary - distant" AND language_alias="secondary - distant"), "2", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("progression - locoregional", "progression - locoregional");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="diagnosis_category_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="progression - locoregional" AND language_alias="progression - locoregional"), "3", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("recurrence - locoregional", "recurrence - locoregional");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="diagnosis_category_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="recurrence - locoregional" AND language_alias="recurrence - locoregional"), "4", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("nonneoplastic", "nonneoplastic");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="diagnosis_category_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="nonneoplastic" AND language_alias="nonneoplastic"), "5", "1");

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('ClinicalAnnotation', 'DiagnosisControl', 'diagnosis_controls', 'category', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='diagnosis_category_fix_list') , '0', '', '', '', 'category', '');
INSERT INTO structure_validations (`structure_field_id`, `rule`, `on_action`, `language_message`)
    (SELECT (SELECT id FROM structure_fields WHERE model='DiagnosisControl' AND tablename='diagnosis_controls' AND field='category' AND `type`='select' AND structure_value_domain=(SELECT id FROM structure_value_domains WHERE domain_name='diagnosis_category_fix_list') ), `rule`, `on_action`, `language_message` FROM structure_validations WHERE structure_field_id=(SELECT id FROM structure_fields WHERE model='DiagnosisControl' AND tablename='diagnosis_controls' AND field='category' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='diagnosis_category'))) ;
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='category' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='diagnosis_category_fix_list') ) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_diagnosis') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='category' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='diagnosis_category') AND `flag_confidential`='0');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/499): Unable to add Event in FB if there is any Event active in db">
INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("event_group_fix_list", "", "", NULL);
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="event_group_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="clinical" AND language_alias="clinical"), "1", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("lab", "lab");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="event_group_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="lab" AND language_alias="lab"), "2", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("lifestyle", "lifestyle");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="event_group_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="lifestyle" AND language_alias="lifestyle"), "3", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("study", "clin_study");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="event_group_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="study" AND language_alias="clin_study"), "5", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("screening", "screening");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="event_group_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="screening" AND language_alias="screening"), "6", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("protocol", "protocol");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="event_group_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="protocol" AND language_alias="protocol"), "7", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("adverse_events", "adverse events");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="event_group_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="adverse_events" AND language_alias="adverse events"), "8", "1");


INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('ClinicalAnnotation', 'EventControl', 'event_controls', 'event_group', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='event_group_fix_list') , '0', '', '', 'help_event_group', 'event_group', '');
INSERT INTO structure_validations (`structure_field_id`, `rule`, `on_action`, `language_message`)
    (SELECT (SELECT id FROM structure_fields WHERE model='EventControl' AND tablename='event_controls' AND field='event_group' AND `type`='select' AND structure_value_domain=(SELECT id FROM structure_value_domains WHERE domain_name='event_group_fix_list') ), `rule`, `on_action`, `language_message` FROM structure_validations WHERE structure_field_id=(SELECT id FROM structure_fields WHERE model='EventControl' AND tablename='event_controls' AND field='event_group' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='event_group_list'))) ;
UPDATE structure_formats SET `structure_field_id`=(SELECT `id` FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='event_group' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='event_group_fix_list') ) WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_event') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='EventControl' AND `tablename`='event_controls' AND `field`='event_group' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='event_group_list') AND `flag_confidential`='0');

INSERT IGNORE INTO i18n (id,en,fr) VALUES
("adverse events", "Adverse events", "Événements indésirables");
-- </editor-fold>


-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/509): SQL Error for the Research (CTRNet Trunk) form">
ALTER TABLE `ed_all_study_researches` CHANGE `file_path` `file_path` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '' ;
ALTER TABLE `ed_all_study_researches_revs` CHANGE `file_path` `file_path` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '';
-- </editor-fold>

-- --------------------------------------------------------------------------------------------------
-- New changing after the 2.8.G tag from 2022-02-25
-- --------------------------------------------------------------------------------------------------

-- <editor-fold desc="">
-- INSERT INTO `versions` (version_number, date_installed, trunk_build_number, branch_build_number)
-- VALUES
-- ('2.8.0', NOW(),'2758cb5e','n/a');
-- </editor-fold>


UPDATE`menus` SET `use_link` = 'javascript:searchAliquotBtBarcode("InventoryManagement/AliquotMasters/barcodeSearch/")' WHERE `use_link` = 'javascript:searchAliquotBtBarcode("/InventoryManagement/AliquotMasters/barcodeSearch/")';

-- --------------------------------------------------------------------------------------------------
-- Tag 280
-- --------------------------------------------------------------------------------------------------

INSERT INTO `versions` (version_number, date_installed, trunk_build_number, branch_build_number)
VALUES
('2.8.0', NOW(),'7b42e751','n/a');
