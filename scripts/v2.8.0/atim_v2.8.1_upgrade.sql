-- ----------------------------------------------------------------------------------------------------------------------------------
-- ATiM Database Upgrade Script
-- Version: 2.8.1
--
-- For more information:
--    ./app/scripts/v2.8.0/ReadMe.md
--    http://wiki.atim-software.ca username/pass: ATiM/ATiMWiKi
-- ----------------------------------------------------------------------------------------------------------------------------------

REPLACE INTO i18n (id,en,fr)
VALUES
('inventory_action_title', 'Title/Main Info', 'Titre/Info générale');

INSERT IGNORE INTO `i18n` (`id`, `en`, `fr`) VALUES
("the tables reindexed", "Some tables are reindexed.", "Certaines tables sont réindexées.");

INSERT INTO `versions` (version_number, date_installed, trunk_build_number, branch_build_number)
VALUES
('2.8.1', NOW(),'todo','n/a');