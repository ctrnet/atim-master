-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.8.2
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Update Participant - Diagnosis - Consent Report
-- --------------------------------------------------------------------------------

UPDATE `datamart_reports`
SET `flag_active` = '1'
WHERE `name` = "export diagnosis and consent of participant"
  AND `description` = "export_diagnosis_and_consent_of_participant"
  AND `options` = "multi-csv=true";


UPDATE `datamart_structure_functions`
SET `flag_active` = '1'
WHERE `label` = "export diagnosis and consent of participant"
  AND `options` = "multi-csv=true";

UPDATE versions SET permissions_regenerated = 0;
