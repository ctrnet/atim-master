-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- Aliquot Tissue Tube : Storage Method and Solution
-- ................................................................................
-- Add 'Storage Mehtod' and 'Storage Solution' fields to the tissue tube form.
-- --------------------------------------------------------------------------------

ALTER TABLE ad_tubes
  ADD COLUMN ctrnet_demo_storage_method VARCHAR(100) DEFAULT NULL,
  ADD COLUMN ctrnet_demo_storage_solution VARCHAR(100) DEFAULT NULL;
ALTER TABLE ad_tubes_revs
  ADD COLUMN ctrnet_demo_storage_method VARCHAR(100) DEFAULT NULL,
  ADD COLUMN ctrnet_demo_storage_solution VARCHAR(100) DEFAULT NULL;
UPDATE aliquot_controls 
SET detail_form_alias = CONCAT(detail_form_alias, ',ctrnet_demo_ad_tissue_tubes') 
WHERE sample_control_id = (SELECT id FROM sample_controls WHERE sample_type = 'tissue') 
AND aliquot_type = 'tube';
INSERT INTO structures(`alias`) VALUES ('ctrnet_demo_ad_tissue_tubes');
INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("ctrnet_demo_tissue_tube_storage_methods", "", "", CONCAT("StructurePermissibleValuesCustom::getCustomDropdown(\'Tissue Tube Storage Methods\')")),
("ctrnet_demo_tissue_tube_storage_solutions", "", "", CONCAT("StructurePermissibleValuesCustom::getCustomDropdown(\'Tissue Tube Storage Solutions\')"));
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Tissue Tube Storage Methods", 1, 50, 'inventory'),
("Tissue Tube Storage Solutions", 1, 50, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Tissue Tube Storage Methods");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`)
VALUES
('flash freeze','Flash Freeze', "", '1', @control_id, @user_id, NOW(),@user_id, NOW());
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Tissue Tube Storage Solutions");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`)
VALUES
('DMSO','', "", '1', @control_id, @user_id, NOW(),@user_id, NOW()),
('OCT','', "", '1', @control_id, @user_id, NOW(),@user_id, NOW()),
('isopentane','Isopentane', "", '1', @control_id, @user_id, NOW(),@user_id, NOW()),
('DMSO + serum','DMSO + Serum', "", '1', @control_id, @user_id, NOW(),@user_id, NOW()),
('isopentane + OCT','Isopentane + OCT', "", '1', @control_id, @user_id, NOW(),@user_id, NOW()),
('RNA later','RnaLater', "", '1', @control_id, @user_id, NOW(),@user_id, NOW()),
('saline solution','Saline Solution', "", '1', @control_id, @user_id, NOW(),@user_id, NOW()),
('none','None', "Aucune", '1', @control_id, @user_id, NOW(),@user_id, NOW());
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) VALUES
('InventoryManagement', 'AliquotDetail', 'ad_tubes', 'ctrnet_demo_storage_method', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_tissue_tube_storage_methods') , '0', '', '', '', 'storage method', ''), 
('InventoryManagement', 'AliquotDetail', 'ad_tubes', 'ctrnet_demo_storage_solution', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_tissue_tube_storage_solutions') , '0', '', '', '', 'storage solution', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ad_tissue_tubes'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='ad_tubes' AND `field`='ctrnet_demo_storage_method' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_tissue_tube_storage_methods')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='storage method' AND `language_tag`=''), '1', '76', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ad_tissue_tubes'), (SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='ad_tubes' AND `field`='ctrnet_demo_storage_solution' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_tissue_tube_storage_solutions')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='storage solution' AND `language_tag`=''), '1', '76', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT INTO i18n (id,en,fr) 
VALUES
('storage method', 'Storage Method', "Méthode d'entreposage"),
('storage solution', 'Storage Solution', "Solution d'entreposage");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;