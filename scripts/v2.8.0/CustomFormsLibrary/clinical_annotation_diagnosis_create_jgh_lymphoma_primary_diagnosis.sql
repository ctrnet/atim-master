-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- Jewish General Hospital : Lymphoma Primary Diagnosis
-- ................................................................................
-- Primary diagnosis forms developped to capture data of lymphoma primary cancer 
-- for the ATiM of Lymphoma bank at the Lady Davis Institute (Jewish 
-- General Hospital).
-- --------------------------------------------------------------------------------

INSERT INTO `diagnosis_controls` (`id`, `category`, `controls_type`, `flag_active`, `detail_form_alias`, `detail_tablename`, `display_order`) 
VALUES
(null, 'primary', 'jgh lymphoma - lymphoma diagnosis', 1, 'jgh_lymphoma_dxd_lymphoma', 'jgh_lymphoma_dxd_lymphomas', 0);

DROP TABLE IF EXISTS `jgh_lymphoma_dxd_lymphomas`;
CREATE TABLE IF NOT EXISTS `jgh_lymphoma_dxd_lymphomas` (
  `diagnosis_master_id` int(11) NOT NULL,
  `lymphoma_type` varchar(250) NOT NULL DEFAULT '',
  `primary_hematologist` varchar(50) DEFAULT '',
  `baseline_history_desc` text,
  `baseline_b_symptoms` char(1) DEFAULT '',
  `baseline_b_symp_fever` char(1) DEFAULT '',
  `baseline_b_symp_night_sweating` char(1) DEFAULT '',
  `baseline_b_symp_weight_loss` char(1) DEFAULT '',
  `baseline_ecog` int(3) DEFAULT NULL,
  `nhl_stage_nbr` varchar(6) DEFAULT '',
  `nhl_stage_alpha` varchar(6) DEFAULT '',
  `cll_rai_stage` varchar(20) DEFAULT '',
  `prognosis_ipi` decimal(7,2) DEFAULT NULL,
  `prognosis_flipi` decimal(7,2) DEFAULT NULL,
  `prognosis_hd` decimal(7,2) DEFAULT NULL,
  `prognosis_mipi` decimal(7,2) DEFAULT NULL,
  `pulmonary_comorbidity` char(1) DEFAULT '',
  `cardiac_comorbidity` char(1) DEFAULT '',
  `renal_comorbidity` char(1) DEFAULT '',
  `hepatic_comorbidity` char(1) DEFAULT '',
  `cns_comorbidity` char(1) DEFAULT '',
  `other_comorbidity` char(1) DEFAULT '',
  `comorbidities_precision` text,
  `baseline_ldh` decimal(7,2) DEFAULT NULL,
  `baseline_ens` decimal(7,2) DEFAULT NULL,
  `ipi_relapse_age` int(5) DEFAULT NULL,
  `ipi_relapse_ecog` decimal(7,2) DEFAULT NULL,
  `ipi_relapse_ldh` decimal(7,2) DEFAULT NULL,
  `ipi_relapse_ens` decimal(7,2) DEFAULT NULL,
  `ipi_relapse_nhl_stage_nbr` varchar(6) DEFAULT NULL,
  `ipi_relapse_nhl_stage_alpha` varchar(6) DEFAULT NULL,
  KEY `diagnosis_master_id` (`diagnosis_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `jgh_lymphoma_dxd_lymphomas_revs`;
CREATE TABLE IF NOT EXISTS `jgh_lymphoma_dxd_lymphomas_revs` (
  `diagnosis_master_id` int(11) NOT NULL,
  `lymphoma_type` varchar(250) NOT NULL DEFAULT '',
  `primary_hematologist` varchar(50) DEFAULT '',
  `baseline_history_desc` text,
  `baseline_b_symptoms` char(1) DEFAULT '',
  `baseline_b_symp_fever` char(1) DEFAULT '',
  `baseline_b_symp_night_sweating` char(1) DEFAULT '',
  `baseline_b_symp_weight_loss` char(1) DEFAULT '',
  `baseline_ecog` int(3) DEFAULT NULL,
  `nhl_stage_nbr` varchar(6) DEFAULT '',
  `nhl_stage_alpha` varchar(6) DEFAULT '',
  `cll_rai_stage` varchar(20) DEFAULT '',
  `prognosis_ipi` decimal(7,2) DEFAULT NULL,
  `prognosis_flipi` decimal(7,2) DEFAULT NULL,
  `prognosis_hd` decimal(7,2) DEFAULT NULL,
  `prognosis_mipi` decimal(7,2) DEFAULT NULL,
  `pulmonary_comorbidity` char(1) DEFAULT '',
  `cardiac_comorbidity` char(1) DEFAULT '',
  `renal_comorbidity` char(1) DEFAULT '',
  `hepatic_comorbidity` char(1) DEFAULT '',
  `cns_comorbidity` char(1) DEFAULT '',
  `other_comorbidity` char(1) DEFAULT '',
  `comorbidities_precision` text,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  `baseline_ldh` decimal(7,2) DEFAULT NULL,
  `baseline_ens` decimal(7,2) DEFAULT NULL,
  `ipi_relapse_age` int(5) DEFAULT NULL,
  `ipi_relapse_ecog` decimal(7,2) DEFAULT NULL,
  `ipi_relapse_ldh` decimal(7,2) DEFAULT NULL,
  `ipi_relapse_ens` decimal(7,2) DEFAULT NULL,
  `ipi_relapse_nhl_stage_nbr` varchar(6) DEFAULT NULL,
  `ipi_relapse_nhl_stage_alpha` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3739 DEFAULT CHARSET=latin1;

ALTER TABLE `jgh_lymphoma_dxd_lymphomas`
  ADD CONSTRAINT `FK_jgh_lymphoma_dxd_lymphomas_diagnosis_masters` FOREIGN KEY (`diagnosis_master_id`) REFERENCES `diagnosis_masters` (`id`);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('jgh_lymphoma_lymphoma_type_list', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'JGH Lymphoma: Lymphoma Types\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('JGH Lymphoma: Lymphoma Types', 1, 250, 'clinical - diagnosis');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'JGH Lymphoma: Lymphoma Types');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("Acute promyelocytic leukemi", "Acute promyelocytic leukemi", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Adult T-cell leukemia/lymphoma", "Adult T-cell leukemia/lymphoma", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("ALCL", "Anaplastic large-cell lymphoma", "Lymphome anaplasique à grandes cellules", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("ALL", "Acute Lymphoblastic Leukemia", "Leucémie aiguë lymphoblastique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("AML", "Acute Myelogenous Leukemia", "Leucémie myéloïde aiguë", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Amyloidosis", "Amyloidosis", "Amyloidosis", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Angioimmunoblastic T-cell lymphoma (ATL)", "Angioimmunoblastic T-cell lymphoma", "Lymphadénopathie angio-immunoblastique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Aplastic Anemia", "Aplastic Anemia", "Anémie Aplastique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("B-cell lymphoma, unclassifiable", "B-cell lymphoma, unclassifiable", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("BL", "Burkitt\'s Lymphoma", "Lymphome de Burkitt", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Blastic plasmacytoid dendritic cell neoplasm", "Blastic plasmacytoid dendritic cell neoplasm", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Castleman\'s Disease", "Castleman\'s Disease", "Maladie de Castleman", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("CLL", "Chronic Lymphocytic Leukemia", "Leucémie lymphoblastique chronique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("CLL / SLL", "Chronic lymphocytic leukemia / Small lymphocytic lymphoma", "Leucémie lymphoïde chronique / Lymphome à petits lymphocytes", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("CML", "Chronic Myelogenous Leukemia", "Leucémie myéloïde chronique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("CMML", "Chronic Myelomonocytic Leukemia", "Leucémie myélomonocytaire chronique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Composite Lymphoma", "Composite Lymphoma", "Lymphome composite", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("CTCL", "Cutaneous T-cell lymphoma", "Lymphome cutané à cellules T", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("DLBCL", "Diffuse Large B Cell Lymphoma", "Lymphome diffus à grande cellules B", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Double-hit Diffuse Large B Cell Lymphoma", "Double-hit Diffuse Large B Cell Lymphoma", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Essential Thrombocytosis", "Essential Thrombocytosis", "Thrombocytémie essentielle", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("FL", "Follicular Lymphoma", "Lymphome folliculaire", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Haemophagocytic Lymphohistiocytosis", "Haemophagocytic Lymphohistiocytosis", "Lymphohistiocytose hémophagocytaire", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("HCL", "Hairy Cell Leukemia", "Leucémie à tricholeucocytes", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("HD", "Hodgkin\'s Disease", "Lymphome hodgkinien", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("MALT lymphoma", "MALT lymphoma", "Lymphome du MALT", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("MC", "Mantle Cell Lymphoma", "Lymphome des cellules du manteau", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("MDS", "Myelodysplastic Syndrome", "Syndrome myélodisplastique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("MGUS", "Monoclonal gammopathy of undetermined significance", "Gammapathie monoclonale de signification indéterminée", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("MM", "Multiple Myeloma", "Myélome multiple", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("MZL", "Marginal Zone Lymphoma", "Lymphome de la zone marginale", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Plasmablastic lymphoma", "Plasmablastic lymphoma", "lymphome plasmablastique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("PMBCL", "Primary mediastinal B-cell lymphoma", "Lymphome médiastinal primitif à grandes cellules B", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("PTCL", "Peripheral T Cell Lymphoma", "PTCL", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("SLL", "Small Lymphocytic Leukemia", "SLL", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("T-cell large granular lymphocyte leukemia", "T-cell large granular lymphocyte leukemia", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("T-lymphoblastic leukemia/lymphoma", "T-lymphoblastic leukemia/lymphoma", "Lymphome ou leucémie lymphoblastique à précurseurs T", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("T-PPL", "T-cell prolymphocytic leukemia", "Leucémie à prolymphocytes T", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("WD", "Lymphoplasmacytic lymphoma/Waldenström\'s Disease", "Lymphoplasmacytic lymphoma/Maladie de Waldenström", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("jgh_lymphoma_0_to_4", "open", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias)
VALUES
("0", "0"),
("1", "1"),
("2", "2"),
("3", "3"),
("4", "4");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) 
VALUES 
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_0_to_4"), (SELECT id FROM structure_permissible_values WHERE value="0" AND language_alias="0"), "0", "1"),
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_0_to_4"), (SELECT id FROM structure_permissible_values WHERE value="1" AND language_alias="1"), "1", "1"),
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_0_to_4"), (SELECT id FROM structure_permissible_values WHERE value="2" AND language_alias="2"), "2", "1"),
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_0_to_4"), (SELECT id FROM structure_permissible_values WHERE value="3" AND language_alias="3"), "3", "1"),
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_0_to_4"), (SELECT id FROM structure_permissible_values WHERE value="4" AND language_alias="4"), "4", "1");

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("jgh_lymphoma_nhl_stage_nbr", "open", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias) 
VALUES
("I", "I"),
("II", "II"),
("III", "III"),
("IV", "IV");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) 
VALUES 
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_nbr"), (SELECT id FROM structure_permissible_values WHERE value="I" AND language_alias="I"), "1", "1"),
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_nbr"), (SELECT id FROM structure_permissible_values WHERE value="II" AND language_alias="II"), "2", "1"),
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_nbr"), (SELECT id FROM structure_permissible_values WHERE value="III" AND language_alias="III"), "3", "1"),
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_nbr"), (SELECT id FROM structure_permissible_values WHERE value="IV" AND language_alias="IV"), "4", "1");

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("jgh_lymphoma_nhl_stage_alpha", "open", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias) 
VALUES
("A", "A"),
("B", "B"),
("E", "E");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) 
VALUES 
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_alpha"), (SELECT id FROM structure_permissible_values WHERE value="A" AND language_alias="A"), "1", "1"),
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_alpha"), (SELECT id FROM structure_permissible_values WHERE value="B" AND language_alias="B"), "2", "1"),
((SELECT id FROM structure_value_domains WHERE domain_name="jgh_lymphoma_nhl_stage_alpha"), (SELECT id FROM structure_permissible_values WHERE value="E" AND language_alias="E"), "3", "1");

INSERT INTO structures(`alias`) VALUES ('jgh_lymphoma_dxd_lymphoma');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'lymphoma_type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_lymphoma_type_list') , '0', '', '', '', 'lymphoma type', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'primary_hematologist', 'input',  NULL , '0', 'size=30', '', '', 'primary hematologist', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'baseline_history_desc', 'textarea',  NULL , '0', 'rows=3,cols=30', '', '', 'baseline history desc', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'baseline_b_symptoms', 'yes_no',  NULL , '0', '', '', '', 'baseline b symptoms', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'baseline_b_symp_fever', 'yes_no',  NULL , '0', '', '', '', 'b symptoms : fever', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'baseline_b_symp_night_sweating', 'yes_no',  NULL , '0', '', '', '', 'b symptoms : night sweating', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'baseline_b_symp_weight_loss', 'yes_no',  NULL , '0', '', '', '', 'b symptoms : weight loss', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'baseline_ecog', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_0_to_4') , '0', '', '', '', 'baseline ecog', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'baseline_ldh', 'float',  NULL , '0', 'size=5', '', '', 'ldh', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'baseline_ens', 'float',  NULL , '0', 'size=5', '', '', 'ens', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'nhl_stage_nbr', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_nbr') , '0', '', '', '', 'nhl stage', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'nhl_stage_alpha', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_alpha') , '0', '', '', '', '', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'cll_rai_stage', 'input',  NULL , '0', 'size=5', '', '', 'cll rai stage', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'prognosis_ipi', 'float',  NULL , '0', 'size=5', '', '', 'prognosis ipi', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'prognosis_flipi', 'float',  NULL , '0', 'size=5', '', '', 'prognosis flipi', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'prognosis_hd', 'float',  NULL , '0', 'size=5', '', '', 'prognosis hd', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'prognosis_mipi', 'float',  NULL , '0', 'size=5', '', '', 'prognosis mipi', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'ipi_relapse_age', 'integer',  NULL , '0', 'size=5', '', '', 'age', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'ipi_relapse_ecog', 'float',  NULL , '0', 'size=5', '', '', 'ecog', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'ipi_relapse_ldh', 'float',  NULL , '0', 'size=5', '', '', 'ldh', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'ipi_relapse_ens', 'float',  NULL , '0', 'size=5', '', '', 'ens', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'ipi_relapse_nhl_stage_nbr', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_nbr') , '0', '', '', '', 'nhl stage', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'ipi_relapse_nhl_stage_alpha', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_alpha') , '0', '', '', '', '', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'pulmonary_comorbidity', 'yes_no',  NULL , '0', '', '', '', 'pulmonary comorbidity', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'cardiac_comorbidity', 'yes_no',  NULL , '0', '', '', '', 'cardiac comorbidity', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'renal_comorbidity', 'yes_no',  NULL , '0', '', '', '', 'renal comorbidity', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'hepatic_comorbidity', 'yes_no',  NULL , '0', '', '', '', 'hepatic comorbidity', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'cns_comorbidity', 'yes_no',  NULL , '0', '', '', '', 'cns comorbidity', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'other_comorbidity', 'yes_no',  NULL , '0', '', '', '', 'other comorbidity', ''), 
('ClinicalAnnotation', 'DiagnosisDetail', 'jgh_lymphoma_dxd_lymphomas', 'comorbidities_precision', 'input',  NULL , '0', 'size=30', '', '', 'comorbidities precision', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='lymphoma_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_lymphoma_type_list')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='lymphoma type' AND `language_tag`=''), 
'1', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='primary_hematologist' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='' AND `language_label`='primary hematologist' AND `language_tag`=''), 
'1', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='baseline_history_desc' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='rows=3,cols=30' AND `default`='' AND `language_help`='' AND `language_label`='baseline history desc' AND `language_tag`=''), 
'1', '30', 'baseline', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='baseline_b_symptoms' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='baseline b symptoms' AND `language_tag`=''), 
'1', '31', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='baseline_b_symp_fever' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='b symptoms : fever' AND `language_tag`=''), 
'1', '32', '', '2', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='baseline_b_symp_night_sweating' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='b symptoms : night sweating' AND `language_tag`=''), 
'1', '32', '', '2', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='baseline_b_symp_weight_loss' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='b symptoms : weight loss' AND `language_tag`=''), 
'1', '32', '', '2', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='baseline_ecog' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_0_to_4')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='baseline ecog' AND `language_tag`=''), 
'1', '33', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='baseline_ldh' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='ldh' AND `language_tag`=''), 
'1', '34', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='baseline_ens' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='ens' AND `language_tag`=''), 
'1', '35', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='nhl_stage_nbr' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_nbr')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='nhl stage' AND `language_tag`=''), 
'2', '70', 'staging', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='nhl_stage_alpha' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_alpha')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '71', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='cll_rai_stage' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='cll rai stage' AND `language_tag`=''), 
'2', '72', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='prognosis_ipi' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='prognosis ipi' AND `language_tag`=''), 
'2', '80', 'prognosis', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='prognosis_flipi' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='prognosis flipi' AND `language_tag`=''), 
'2', '81', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='prognosis_hd' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='prognosis hd' AND `language_tag`=''), 
'2', '82', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='prognosis_mipi' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='prognosis mipi' AND `language_tag`=''), 
'2', '83', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='ipi_relapse_age' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='age' AND `language_tag`=''), 
'2', '100', 'ipi relapse', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='ipi_relapse_ecog' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='ecog' AND `language_tag`=''), 
'2', '101', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='ipi_relapse_ldh' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='ldh' AND `language_tag`=''), 
'2', '102', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='ipi_relapse_ens' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='ens' AND `language_tag`=''), 
'2', '103', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='ipi_relapse_nhl_stage_nbr' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_nbr')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='nhl stage' AND `language_tag`=''), 
'2', '104', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='ipi_relapse_nhl_stage_alpha' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='jgh_lymphoma_nhl_stage_alpha')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '105', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='pulmonary_comorbidity' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='pulmonary comorbidity' AND `language_tag`=''), 
'3', '140', 'comorbidities', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='cardiac_comorbidity' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='cardiac comorbidity' AND `language_tag`=''), 
'3', '141', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='renal_comorbidity' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='renal comorbidity' AND `language_tag`=''), 
'3', '142', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='hepatic_comorbidity' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='hepatic comorbidity' AND `language_tag`=''), 
'3', '143', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='cns_comorbidity' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='cns comorbidity' AND `language_tag`=''), 
'3', '144', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='other_comorbidity' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other comorbidity' AND `language_tag`=''), 
'3', '146', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='jgh_lymphoma_dxd_lymphoma'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='jgh_lymphoma_dxd_lymphomas' AND `field`='comorbidities_precision' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='' AND `language_label`='comorbidities precision' AND `language_tag`=''), 
'3', '147', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

REPLACE INTO i18n (id,en,fr)
VALUES 
("jgh lymphoma - lymphoma diagnosis", "Lymphoma (JGH Lymphoma)", "Lymphome (JGH Lymphome)"),
("age", "Age", "Age"),
("b symptoms : fever", "Fever (B Sympt.)", ""),
("b symptoms : night sweating", "Night Sweating (B Sympt.)", ""),
("b symptoms : weight loss", "Weight loss (B Sympt.)", ""),
("baseline", "Baseline", ""),
("baseline b symptoms", "B Symptoms", ""),
("baseline ecog", "ECOG", ""),
("baseline history desc", "History", ""),
("cardiac comorbidity", "Cardiac", ""),
("cll rai stage", "CLL RAI", ""),
("cns comorbidity", "Central Nervous System", ""),
("comorbidities", "Comorbidities", ""),
("comorbidities precision", "Precision", ""),
("ecog", "ECOG", "ECOG"),
("ens", "ENS", "ENS"),
("hepatic comorbidity", "Hepatic", ""),
("ipi relapse", "IPI Relpase", ""),
("ldh", "LDH", ""),
("lymphoma type", "Lymphoma Type", ""),
("nhl stage", "NHL", ""),
("other comorbidity", "Other", ""),
("primary hematologist", "Primary Hematologist", ""),
("prognosis", "Prognosis", ""),
("prognosis flipi", "FLIPI", ""),
("prognosis hd", "HD", ""),
("prognosis ipi", "IPI", ""),
("prognosis mipi", "MIPI", ""),
("pulmonary comorbidity", "Pulmonary", ""),
("renal comorbidity", "Renal", ""),
("staging", "Staging", "Stade"),
("A", "A", "A"),
("B", "B", "B"),
("E", "E", "E"),
("I", "I", "I"),
("II", "II", "II"),
("III", "III", "III"),
("IV", "IV", "IV");
 
UPDATE structure_formats SET `flag_summary`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='dx_primary') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='DiagnosisMaster' AND `tablename`='diagnosis_masters' AND `field`='dx_method' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='dx_method') AND `flag_confidential`='0');
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;