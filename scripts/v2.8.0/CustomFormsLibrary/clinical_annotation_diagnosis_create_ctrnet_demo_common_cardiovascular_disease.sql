-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CTRNET Demo : ICD-10 Common Codes for Cardiovascular Disease
-- ................................................................................
-- ICD-10 Common Codes for Cardiovascular Disease based on multiple accessible
-- ressources.
--
-- WARNING: For demo only. Review requested.
-- --------------------------------------------------------------------------------

INSERT INTO `diagnosis_controls` (`id`, `category`, `controls_type`, `flag_active`, `detail_form_alias`, `detail_tablename`, `display_order`) 
VALUES
(null, 'nonneoplastic', 'ctrnet demo - cardiovascular disease common codes', 1, 'ctrnet_demo_dxd_cardiovascular_disease', 'ctrnet_demo_dxd_cardiovascular_diseases', 0);

DROP TABLE IF EXISTS `ctrnet_demo_dxd_cardiovascular_diseases`;
CREATE TABLE IF NOT EXISTS `ctrnet_demo_dxd_cardiovascular_diseases` (
  `diagnosis_master_id` int(11) NOT NULL,
  `icd10_common_code` varchar(250) NOT NULL DEFAULT '',
  KEY `diagnosis_master_id` (`diagnosis_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `ctrnet_demo_dxd_cardiovascular_diseases_revs`;
CREATE TABLE IF NOT EXISTS `ctrnet_demo_dxd_cardiovascular_diseases_revs` (
  `diagnosis_master_id` int(11) NOT NULL,
  `icd10_common_code` varchar(250) NOT NULL DEFAULT '',
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ctrnet_demo_dxd_cardiovascular_diseases`
  ADD CONSTRAINT `FK_ctrnet_demo_dxd_cardiovascular_diseases_diagnosis_masters` FOREIGN KEY (`diagnosis_master_id`) REFERENCES `diagnosis_masters` (`id`);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES
('ctrnet_demo_dxd_common_cardiovascular_disease_codes', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CTRNet Demo : Cardiovascular Disease ICD10 Codes\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('CTRNet Demo : Cardiovascular Disease ICD10 Codes', 1, 250, 'clinical - diagnosis');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CTRNet Demo : Cardiovascular Disease ICD10 Codes');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("r73.01 -- elevated fasting glucose", "R73.01 : Abnormal Glucose, Elevated Fasting Glucose", "", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("r73.01 -- impaired fasting glucose", "R73.01 : Abnormal Glucose, Impaired Fasting Glucose", "", "5", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("r73.09 -- prediabetes", "R73.09 : Abnormal Glucose, Prediabetes", "", "8", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("r73.09 -- abnormal glucose", "R73.09 : Abnormal Glucose, Abnormal Glucose", "", "11", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("r73.9 -- hyperglycemia", "R73.9 : Abnormal Glucose, Hyperglycemia", "", "14", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i20.9 -- angina pectoris, nos", "I20.9 : Circulatory System Diseases, Angina Pectoris, NOS", "", "17", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i21.09 -- myocardial infarction, acute, anterior (initial episode of care)", "I21.09 : Circulatory System Diseases, Myocardial Infarction, Acute, Anterior (initial episode of care)", "", "20", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i21.3 -- myocardial infarction, acute, unspecified (initial episode of care)", "I21.3 : Circulatory System Diseases, Myocardial Infarction, Acute, Unspecified (initial episode of care)", "", "23", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i25.10 -- ashd coronary artery", "I25.10 : Circulatory System Diseases, ASHD Coronary Artery", "", "26", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i25.10 -- ashd unspecified", "I25.10 : Circulatory System Diseases, ASHD Unspecified", "", "29", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i25.10 -- cad (coronary artery disease)/ashd", "I25.10 : Circulatory System Diseases, CAD (Coronary Artery Disease)/ASHD", "", "32", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i25.2 -- old myocardial infarction", "I25.2 : Circulatory System Diseases, Old Myocardial Infarction", "", "35", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i25.84 -- coronary atherosclerosis due to calcified coronary lesion", "I25.84 : Circulatory System Diseases, Coronary Atherosclerosis Due to Calcified Coronary Lesion", "", "38", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i25.9 -- chronic ischemic heart disease", "I25.9 : Circulatory System Diseases, Chronic Ischemic Heart Disease", "", "41", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i10 -- malignant hypertension", "I10 : Hypertensive Disease, Malignant Hypertension", "", "44", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i10 -- benign hypertension", "I10 : Hypertensive Disease, Benign Hypertension", "", "47", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i10 -- hypertension, unspecified", "I10 : Hypertensive Disease, Hypertension, Unspecified", "", "50", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i11.0 -- malignant hypertension heart disease with heart failure", "I11.0 : Hypertensive Disease, Malignant Hypertension Heart Disease with Heart Failure", "", "53", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i11.0 -- benign hypertension heart disease with heart failure", "I11.0 : Hypertensive Disease, Benign Hypertension Heart Disease with Heart Failure", "", "56", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i11.0 -- unspecified hypertension heart disease with heart failure", "I11.0 : Hypertensive Disease, Unspecified Hypertension Heart Disease with Heart Failure", "", "59", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i11.9 -- benign hypertension heart disease without heart failure", "I11.9 : Hypertensive Disease, Benign Hypertension Heart Disease without Heart Failure", "", "62", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i11.9 -- malignant hypertension heart disease without heart failure", "I11.9 : Hypertensive Disease, Malignant Hypertension Heart Disease without Heart Failure", "", "65", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i11.9 -- unspecified hypertension heart disease without heart failure", "I11.9 : Hypertensive Disease, Unspecified Hypertension Heart Disease without Heart Failure", "", "68", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e11.65 -- diabetes mellitus, ii with hyperglycemia", "E11.65 : Metabolic and Nutritional Diseases, Diabetes Mellitus, II with Hyperglycemia", "", "71", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e11.9 -- diabetes mellitus, ii controlled", "E11.9 : Metabolic and Nutritional Diseases, Diabetes Mellitus, II Controlled", "", "74", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e11.9 -- diabetes mellitus, ii uncontrolled", "E11.9 : Metabolic and Nutritional Diseases, Diabetes Mellitus, II Uncontrolled", "", "77", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e55.9 -- vitamin d deficiency", "E55.9 : Metabolic and Nutritional Diseases, Vitamin D Deficiency", "", "80", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e78.0 -- hypercholesterolemia", "E78.0 : Metabolic and Nutritional Diseases, Hypercholesterolemia", "", "83", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e78.1 -- hypertriglyceridemia", "E78.1 : Metabolic and Nutritional Diseases, Hypertriglyceridemia", "", "86", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e78.1 -- pure hypertriglyceridemia", "E78.1 : Metabolic and Nutritional Diseases, Pure Hypertriglyceridemia", "", "89", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e78.2 -- hyperlipidemia, mixed", "E78.2 : Metabolic and Nutritional Diseases, Hyperlipidemia, Mixed", "", "92", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e78.2 -- mixed hyperlipidemia", "E78.2 : Metabolic and Nutritional Diseases, Mixed Hyperlipidemia", "", "95", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e78.5 -- hyperlipidemia, unspecified", "E78.5 : Metabolic and Nutritional Diseases, Hyperlipidemia, Unspecified", "", "98", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e88.81 -- dysmetabolic syndrome", "E88.81 : Metabolic and Nutritional Diseases, Dysmetabolic Syndrome", "", "101", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e88.81 -- insulin resistance", "E88.81 : Metabolic and Nutritional Diseases, Insulin Resistance", "", "104", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("e88.81 -- metabolic syndrome", "E88.81 : Metabolic and Nutritional Diseases, Metabolic Syndrome", "", "107", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i25.10 -- cardiovascular disease, unspecified (ascvd)", "I25.10 : Cardiovascular and Ischaemic Disease, Cardiovascular Disease, Unspecified (ASCVD)", "", "110", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i48.91 -- atrial fibrillation", "I48.91 : Cardiovascular and Ischaemic Disease, Atrial Fibrillation", "", "113", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i50.9 -- congestive heart failure", "I50.9 : Cardiovascular and Ischaemic Disease, Congestive Heart Failure", "", "116", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i63.9 -- cva", "I63.9 : Cardiovascular and Ischaemic Disease, CVA", "", "119", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i63.9 -- stroke", "I63.9 : Cardiovascular and Ischaemic Disease, Stroke", "", "122", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i65.23 -- carotid artery occlusion, bilateral", "I65.23 : Cardiovascular and Ischaemic Disease, Carotid Artery Occlusion, Bilateral", "", "125", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i65.23 -- carotid artery stenosis, bilateral", "I65.23 : Cardiovascular and Ischaemic Disease, Carotid Artery Stenosis, Bilateral", "", "128", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i65.29 -- carotid artery occlusion", "I65.29 : Cardiovascular and Ischaemic Disease, Carotid Artery Occlusion", "", "131", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i65.29 -- carotid artery stenosis", "I65.29 : Cardiovascular and Ischaemic Disease, Carotid Artery Stenosis", "", "134", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i67.2 -- cerebral atherosclerosis", "I67.2 : Cardiovascular and Ischaemic Disease, Cerebral Atherosclerosis", "", "137", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i67.9 -- ischemic cerebrovascular disease", "I67.9 : Cardiovascular and Ischaemic Disease, Ischemic Cerebrovascular Disease", "", "140", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("i73.9 -- peripheral vascular disease", "I73.9 : Cardiovascular and Ischaemic Disease, Peripheral Vascular Disease", "", "143", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structures(`alias`) VALUES ('ctrnet_demo_dxd_cardiovascular_disease');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) VALUES
('ClinicalAnnotation', 'DiagnosisDetail', 'ctrnet_demo_dxd_cardiovascular_diseases', 'icd10_common_code', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_dxd_common_cardiovascular_disease_codes') , '0', '', '', '', 'disease', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) VALUES 
((SELECT id FROM structures WHERE alias='ctrnet_demo_dxd_cardiovascular_disease'), 
(SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='ctrnet_demo_dxd_cardiovascular_diseases' AND `field`='icd10_common_code' AND `structure_value_domain` = (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_dxd_common_cardiovascular_disease_codes') AND `flag_confidential`='0'), 
'1', '6', '', '0', '0', '', '0', '', '1', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_validations(structure_field_id, rule, language_message) VALUES
((SELECT id FROM structure_fields WHERE `model`='DiagnosisDetail' AND `tablename`='ctrnet_demo_dxd_cardiovascular_diseases' AND `field`='icd10_common_code'), 'notBlank', '');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
('disease', "Disease", "Maladie"),
('ctrnet demo - cardiovascular disease common codes', "Common Cardiovascular Disease (CTRNet Demo)", "Maladie cardiovasculaire courante (CTRNet démo)");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;