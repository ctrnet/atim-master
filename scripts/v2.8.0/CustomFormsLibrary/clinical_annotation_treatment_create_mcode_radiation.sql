-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- mCode ((Minimal Common Oncology Data Elements) initiative : 
--   Create radiotherapy form based on 'Treatment' group 
--   and 'Cancer Related Radiation Procedure' profile fields 
--   of the mCODE™ Data Dictionnary Version 0.9.1.
-- ................................................................................

--   List based on :
--    - Code
--       mCODE™ Value Set Name : RadiationProcedureVS
--    - Target Body Site
--       mCODE™ Value Set Name : RadiationTargetBodySiteVS
-- --------------------------------------------------------------------------------

INSERT INTO treatment_extend_controls (id, detail_tablename, detail_form_alias, flag_active, `type`) VALUES
(null, 'mcode_txe_cancer_related_radiations', 'mcode_txe_cancer_related_radiation', 1, 'mcode - cancer related radiation site');

INSERT INTO `treatment_controls` (`id`, `tx_method`, `disease_site`, `flag_active`, `detail_tablename`, `detail_form_alias`, `display_order`, `applied_protocol_control_id`, `extended_data_import_process`, `flag_use_for_ccl`, `treatment_extend_control_id`, `use_addgrid`, `use_detail_form_for_index`) 
VALUES
(null, 'mcode - cancer related radiation procedure', '', 1, 'mcode_txd_cancer_related_radiations', 'mcode_txd_cancer_related_radiation', 0, NULL, NULL, 0, (SELECT id FROM treatment_extend_controls WHERE type = 'mcode - cancer related radiation site'), 0, 1);

DROP TABLE IF EXISTS mcode_txd_cancer_related_radiations;
CREATE TABLE IF NOT EXISTS mcode_txd_cancer_related_radiations (
  treatment_master_id int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  KEY tx_master_id (treatment_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS mcode_txd_cancer_related_radiations_revs;
CREATE TABLE IF NOT EXISTS mcode_txd_cancer_related_radiations_revs (
  treatment_master_id int(11) NOT NULL,
  `code` varchar(50) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS mcode_txe_cancer_related_radiations;
CREATE TABLE IF NOT EXISTS mcode_txe_cancer_related_radiations (
  `site` varchar(50) DEFAULT NULL,
  treatment_extend_master_id int(11) NOT NULL,
  KEY FK_mcode_txe_cancer_related_radiations_treatment_extend_masters (treatment_extend_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS mcode_txe_cancer_related_radiations_revs;
CREATE TABLE IF NOT EXISTS mcode_txe_cancer_related_radiations_revs (
  `site` varchar(50) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  treatment_extend_master_id int(11) NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE mcode_txd_cancer_related_radiations
  ADD CONSTRAINT mcode_txd_cancer_related_radiations_ibfk_1 FOREIGN KEY (treatment_master_id) REFERENCES treatment_masters (id);

ALTER TABLE mcode_txe_cancer_related_radiations
  ADD CONSTRAINT FK_mcode_txe_cancer_related_radiations_treatment_extend_masters FOREIGN KEY (treatment_extend_master_id) REFERENCES treatment_extend_masters (id);

INSERT INTO structures(`alias`) VALUES ('mcode_txd_cancer_related_radiation');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'TreatmentMaster', 'treatment_masters', 'tx_intent', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_TreatmentIntentVS') , '0', '', '', 'help_mcode_tx_cancer_related_radiation_intent', 'treatment intent', ''),
('ClinicalAnnotation', 'TreatmentDetail', 'mcode_txd_cancer_related_radiations', 'code', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_RadiationProcedureVS') , '0', '', '', 'help_mcode_tx_cancer_related_radiation_code', 'code', '');

INSERT INTO structure_validations (structure_field_id, rule, language_message)
VALUES
((SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='mcode_txd_cancer_related_radiations' AND `field`='code'),
'notBlank', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_txd_cancer_related_radiation'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='finish_date' AND `type`='date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_finish_date' AND `language_label`='finish date' AND `language_tag`=''), 
'1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_txd_cancer_related_radiation'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='tx_intent' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_TreatmentIntentVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_tx_cancer_related_radiation_intent' AND `language_label`='treatment intent' AND `language_tag`=''), 
'1', '19', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
((SELECT id FROM structures WHERE alias='mcode_txd_cancer_related_radiation'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentDetail' AND `tablename`='mcode_txd_cancer_related_radiations' AND `field`='code' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_RadiationProcedureVS')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_mcode_tx_cancer_related_radiation_code' AND `language_label`='code' AND `language_tag`=''), 
'1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_txd_cancer_related_radiation'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentMaster' AND `tablename`='treatment_masters' AND `field`='notes' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='rows=3,cols=30' AND `default`='' AND `language_help`='help_notes' AND `language_label`='notes' AND `language_tag`=''), 
'1', '99', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structures(`alias`) VALUES ('mcode_txe_cancer_related_radiation');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'TreatmentExtendDetail', 'mcode_txe_cancer_related_radiations', 'site', 'autocomplete',  NULL , '0', 'size=10,url=/CodingIcd/CodingMCodes/autocomplete/RadiationTargetBodySiteVS,tool=/CodingIcd/CodingMCodes/tool/RadiationTargetBodySiteVS', '', 'help_mcode_tx_cancer_related_radiation_site', 'target body site', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_txe_cancer_related_radiation'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='mcode_txe_cancer_related_radiations' AND `field`='site' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingMCodes/autocomplete/RadiationTargetBodySiteVS,tool=/CodingIcd/CodingMCodes/tool/RadiationTargetBodySiteVS' AND `default`='' AND `language_help`='help_mcode_tx_cancer_related_radiation_site' AND `language_label`='target body site' AND `language_tag`=''), 
'1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0');
INSERT INTO structure_validations (structure_field_id, rule, language_message)
VALUES
((SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='mcode_txe_cancer_related_radiations' AND `field`='site'),
'validateMcodeCode,RadiationTargetBodySiteVS', 'invalid mcode code'),
((SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='mcode_txe_cancer_related_radiations' AND `field`='site'),
'notBlank', '');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('mcode - cancer related radiation procedure', "Cancer Related Radiation (mCODE™)", "Radiation du cancer (mCODE™)"),
('treatment intent', 'Treatment Intent', 'Objectif du traitement'),
('help_mcode_tx_cancer_related_radiation_intent', 
"The code for the purpose of a treatment as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the TreatmentIntentVS Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code définissant 'l'objectif de radiothérapie' tel que défini par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes TreatmentIntentVS de la version 0.9.1 du dictionnaire de données mCode."),
('help_mcode_tx_cancer_related_radiation_code', 
"The code for the radiation therapy procedure performed as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the RadiationProcedureVS Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code définissant la 'procédure de radiothérapie' telle que définie par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes RadiationProcedureVS de la version 0.9.1 du dictionnaire de données mCode."),
('mcode - cancer related radiation site', "Cancer Related Radiation Site (mCODE™)", "Site radiation du cancer (mCODE™)"),
('target body site', 'Target Body Site', 'Site corporel ciblé'),
('help_mcode_tx_cancer_related_radiation_site', 
"The code for the he body location(s) where the procedure was performed as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
Based on the RadiationTargetBodySiteVS Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code définissant le 'site corporel ou la procédure à été effecturée' tel que défini par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Basé sur la liste des codes RadiationTargetBodySiteVS de la version 0.9.1 du dictionnaire de données mCode.");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;