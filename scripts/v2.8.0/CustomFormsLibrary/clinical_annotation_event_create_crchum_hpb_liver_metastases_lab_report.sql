-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CRCHUM Hepatopancreatobiliary Bank : Liver Metastases Lab Report
-- ................................................................................
-- Form developped to capture data for the CRCHUM Hepatopancreatobiliary BioBank.
-- Designed and created by the BioBank PIs.
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, 'liver', 'lab', 'crchum hpb - liver metastases lab report', 1, 'crchum_hpb_ed_liver_metastasis_lab_report', 'crchum_hpb_ed_liver_metastasis_lab_reports', 0, 1, 0, 0);

DROP TABLE IF EXISTS crchum_hpb_ed_liver_metastasis_lab_reports;
CREATE TABLE IF NOT EXISTS crchum_hpb_ed_liver_metastasis_lab_reports (
  event_master_id int(11) NOT NULL,
  histologic_type varchar(100) DEFAULT NULL,
  histologic_type_specify varchar(250) DEFAULT NULL,
  necrosis_perc decimal(3,1) DEFAULT NULL,
  necrosis_perc_list varchar(50) DEFAULT NULL,
  rubbia_brandt varchar(20) DEFAULT NULL,
  blazer varchar(20) DEFAULT NULL,
  lesions_nbr int(6) DEFAULT NULL,
  additional_dimension_a decimal(3,1) DEFAULT NULL,
  additional_dimension_b decimal(3,1) DEFAULT NULL,
  tumor_size_cannot_be_determined tinyint(1) DEFAULT '0',
  tumor_size_greatest_dimension decimal(3,1) DEFAULT NULL,
  other_lesion_size_greatest_dimension decimal(3,1) DEFAULT NULL,
  other_lesion_size_additional_dimension_a decimal(3,1) DEFAULT NULL,
  other_lesion_size_additional_dimension_b decimal(3,1) DEFAULT NULL,
  other_lesion_size_cannot_be_determined char(1) DEFAULT '',
  tumor_site varchar(50) DEFAULT NULL,
  tumor_site_specify varchar(250) DEFAULT NULL,
  surgical_resection_margin varchar(10) DEFAULT NULL,
  distance_of_tumor_from_closest_surgical_resection_margin_cm decimal(3,1) DEFAULT NULL,
  specify_margin varchar(250) DEFAULT NULL,
  adjacent_liver_parenchyma varchar(100) DEFAULT NULL,
  adjacent_liver_parenchyma_specify varchar(250) DEFAULT NULL,
  notes text,
  k_ras varchar(50) DEFAULT NULL,
  n_ras varchar(50) DEFAULT NULL,
  n_ras_codon varchar(150) DEFAULT null,
  n_ras_mutation varchar(150) DEFAULT null,
  k_ras_codon varchar(150) DEFAULT null,
  k_ras_mutation varchar(150) DEFAULT null,
  world_health_organization_classification varchar(60) DEFAULT NULL,
  mitotic_not_applicable tinyint(1) DEFAULT '0',
  less_than_2_mitoses_10_high_power_fields tinyint(1) DEFAULT '0',
  greater_than_or_equal_to_2_mitoses_10_HPF_to_10_mitoses_10_HPF tinyint(1) DEFAULT '0',
  less_than_2_mitoses_specify_per_10_HPF varchar(10) DEFAULT NULL,
  greater_or_equal_to_2_mitoses_specify_per_10_HPF varchar(10) DEFAULT NULL,
  greater_than_10_mitoses_per_10_HPF tinyint(1) DEFAULT '0',
  mitotic_cannot_be_determined tinyint(1) DEFAULT '0',
  less_or_equal_2percent_Ki67_positive_cells tinyint(1) DEFAULT '0',
  3_to_20percent_Ki67_positive_cells tinyint(1) NULL DEFAULT 0,
  great_than_20percent_Ki67_positive_cells tinyint(1) NULL DEFAULT 0  
)  ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS crchum_hpb_ed_liver_metastasis_lab_reports_revs;
CREATE TABLE IF NOT EXISTS crchum_hpb_ed_liver_metastasis_lab_reports_revs (
  event_master_id int(11) NOT NULL,
  histologic_type varchar(100) DEFAULT NULL,
  histologic_type_specify varchar(250) DEFAULT NULL,
  necrosis_perc decimal(3,1) DEFAULT NULL,
  necrosis_perc_list varchar(50) DEFAULT NULL,
  rubbia_brandt varchar(20) DEFAULT NULL,
  blazer varchar(20) DEFAULT NULL,
  lesions_nbr int(6) DEFAULT NULL,
  additional_dimension_a decimal(3,1) DEFAULT NULL,
  additional_dimension_b decimal(3,1) DEFAULT NULL,
  tumor_size_cannot_be_determined tinyint(1) DEFAULT '0',
  tumor_size_greatest_dimension decimal(3,1) DEFAULT NULL,
  other_lesion_size_greatest_dimension decimal(3,1) DEFAULT NULL,
  other_lesion_size_additional_dimension_a decimal(3,1) DEFAULT NULL,
  other_lesion_size_additional_dimension_b decimal(3,1) DEFAULT NULL,
  other_lesion_size_cannot_be_determined char(1) DEFAULT '',
  tumor_site varchar(50) DEFAULT NULL,
  tumor_site_specify varchar(250) DEFAULT NULL,
  surgical_resection_margin varchar(10) DEFAULT NULL,
  distance_of_tumor_from_closest_surgical_resection_margin_cm decimal(3,1) DEFAULT NULL,
  specify_margin varchar(250) DEFAULT NULL,
  adjacent_liver_parenchyma varchar(100) DEFAULT NULL,
  adjacent_liver_parenchyma_specify varchar(250) DEFAULT NULL,
  notes text,
  k_ras varchar(50) DEFAULT NULL,
  n_ras varchar(50) DEFAULT NULL,
  n_ras_codon varchar(150) DEFAULT null,
  n_ras_mutation varchar(150) DEFAULT null,
  k_ras_codon varchar(150) DEFAULT null,
  k_ras_mutation varchar(150) DEFAULT null,
  world_health_organization_classification varchar(60) DEFAULT NULL,
  mitotic_not_applicable tinyint(1) DEFAULT '0',
  less_than_2_mitoses_10_high_power_fields tinyint(1) DEFAULT '0',
  greater_than_or_equal_to_2_mitoses_10_HPF_to_10_mitoses_10_HPF tinyint(1) DEFAULT '0',
  less_than_2_mitoses_specify_per_10_HPF varchar(10) DEFAULT NULL,
  greater_or_equal_to_2_mitoses_specify_per_10_HPF varchar(10) DEFAULT NULL,
  greater_than_10_mitoses_per_10_HPF tinyint(1) DEFAULT '0',
  mitotic_cannot_be_determined tinyint(1) DEFAULT '0',
  less_or_equal_2percent_Ki67_positive_cells tinyint(1) DEFAULT '0',
  3_to_20percent_Ki67_positive_cells tinyint(1) NULL DEFAULT 0,
  great_than_20percent_Ki67_positive_cells tinyint(1) NULL DEFAULT 0, 
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE crchum_hpb_ed_liver_metastasis_lab_reports
  ADD CONSTRAINT crchum_hpb_ed_liver_metastasis_lab_reports_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);
  
INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_liver_metastasis_hitologic_types', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Liver Metastasis Hitologic Types\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Liver Metastasis Hitologic Types', 1, 20, 'clinical - diagnosis');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Liver Metastasis Hitologic Types');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("breast", "Breast", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("colorectal", "Colorectal", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("endocrine", "Endocrine", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_rubbia_brandt_values', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Rubbia Brant Values\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Rubbia Brant Values', 1, 20, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Rubbia Brant Values');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("1", "", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("2", "", "", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("3", "", "", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("4", "", "", "4", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("5", "", "", "5", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_viable_cells_perc', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Liver Metastasis Viable Cells Perc.\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Liver Metastasis Viable Cells Perc.', 1, 50, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Liver Metastasis Viable Cells Perc.');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("0-49%", "", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("50-99%", "", "", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("100%", "", "", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_blazer_values', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Blazer Values\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Blazer Values', 1, 20, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Blazer Values');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("major", "Major", "", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("minor", "Minor", "", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_liver_metastasis_tumor_sites', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Liver Metastasis Tumor Sites\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Liver Metastasis Tumor Sites', 1, 20, 'clinical - diagnosis');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Liver Metastasis Tumor Sites');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("ascending colon", "Ascending Colon", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("bone", "Bone", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("brain", "Brain", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("breast ", "Breast", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("colon", "Colon", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("descending colon", "Descending Colon", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("ileon", "Ileon", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("jejunum", "Jejunum", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("kidney", "Kidney", "Rein", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("Liver", "Liver", "Foie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("pancreas", "Pancreas", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("rectum", "Rectum", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("transverse colon", "Transverse Colon", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("unknown", "Unknown", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source) VALUES ("crchum_hpb_positive_negative", "open", "", NULL);
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("positive", "positive"),("negative", "negative");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="crchum_hpb_positive_negative"), (SELECT id FROM structure_permissible_values WHERE value="positive" AND language_alias="positive"), "1", "1");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="crchum_hpb_positive_negative"), (SELECT id FROM structure_permissible_values WHERE value="negative" AND language_alias="negative"), "3", "1");

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_adjacent_liver_parenchyma', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Adjacent Liver Parenchyma\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('CRCHUM HPB : Adjacent Liver Parenchyma', 1, 100, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Adjacent Liver Parenchyma');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("normal", "Normal", "Normal", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("steatosis", "Steatosis", "Stéatose hépatique", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("hepatic peliosis", "Hepatic Peliosis", "", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("steatosis hepatitis", "Steatosis Hepatitis", "Stéatohépatite", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("chronic hepatitis", "Chronic hepatitis", "Hépatite chronique", "4", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("cirrhosis", "Cirrhosis", "Cirrhose", "5", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "Autre", "6", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_N_K_ras_values', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : N&K Ras Values\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : N&K Ras Values', 1, 0, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : N&K Ras Values');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("wild type", "Wild Type", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("muted", "Muted", "", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("unknown", "Unknown", "Inconnu", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structures(`alias`) VALUES ('crchum_hpb_ed_liver_metastasis_lab_report');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'histologic_type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_liver_metastasis_hitologic_types') , '0', '', '', '', 'histologic type', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'histologic_type_specify', 'input',  NULL , '0', '', '', '', 'histologic type specify', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'necrosis_perc', 'float',  NULL , '0', '', '', '', 'necrosis percentage', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'necrosis_perc_list', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_viable_cells_perc') , '0', '', '', '', 'necrosis percentage list', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'rubbia_brandt', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_rubbia_brandt_values') , '0', '', '', '', 'rubbia brandt', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'blazer', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_blazer_values') , '0', '', '', '', 'blazer', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'lesions_nbr', 'integer',  NULL , '0', '', '', '', 'lesions nbr', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'tumor_size_greatest_dimension', 'float',  NULL , '0', '', '', '', 'tumor size greatest dimension (cm)', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'additional_dimension_a', 'float',  NULL , '0', '', '', '', 'additional dimension (cm)', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'additional_dimension_b', 'float',  NULL , '0', '', '', '', '', 'x'), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'tumor_size_cannot_be_determined', 'checkbox',  NULL , '0', '', '', '', 'cannot be determined', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'other_lesion_size_greatest_dimension', 'float',  NULL , '0', '', '', '', 'other lesion greatest dimension', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'other_lesion_size_additional_dimension_a', 'float',  NULL , '0', '', '', '', 'other lesion additional dimension a', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'other_lesion_size_additional_dimension_b', 'float',  NULL , '0', '', '', '', '', 'additional dimension b'), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'other_lesion_size_cannot_be_determined', 'yes_no', NULL , '0', '', '', '', 'other lesion cannot be determined', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'tumor_site', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_liver_metastasis_tumor_sites') , '0', '', '', '', 'tumor site', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'tumor_site_specify', 'input',  NULL , '0', '', '', '', 'tumor site specify', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'surgical_resection_margin', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_positive_negative') , '0', '', '', '', 'surgical resection margin', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'distance_of_tumor_from_closest_surgical_resection_margin_cm', 'float',  NULL , '0', '', '', '', 'distance of tumor from closest surgical resection margin (cm)', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'specify_margin', 'input',  NULL , '0', '', '', '', '', 'specify margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'adjacent_liver_parenchyma', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_adjacent_liver_parenchyma') , '0', '', '', '', 'adjacent liver parenchyma', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'adjacent_liver_parenchyma_specify', 'input',  NULL , '0', '', '', '', 'adjacent liver parenchyma specify', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'k_ras', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_N_K_ras_values') , '0', '', '', '', 'K-ras', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'world_health_organization_classification', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='WHO_classification_pe') , '0', '', '', '', 'world health organization classification', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'k_ras_codon', 'input',  NULL , '0', 'size=5', '', '', '', 'codon'), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'k_ras_mutation', 'input',  NULL , '0', 'size=5', '', '', '', 'mutation'), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'n_ras', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_N_K_ras_values') , '0', '', '', '', 'n-ras', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'n_ras_codon', 'input',  NULL , '0', 'size=5', '', '', '', 'codon'), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'n_ras_mutation', 'input',  NULL , '0', 'size=5', '', '', '', 'mutation'), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'mitotic_not_applicable', 'checkbox', NULL , '0', '', '', 'mitotic activity', 'not applicable', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'less_than_2_mitoses_10_high_power_fields', 'checkbox', NULL , '0', '', '', '', 'less than 2 mitoses/10 high power fields (HPF)', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'less_than_2_mitoses_specify_per_10_HPF', 'input',  NULL , '0', '', '', '', '', 'specify mitoses per 10 HPF'), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'greater_than_or_equal_to_2_mitoses_10_HPF_to_10_mitoses_10_HPF', 'checkbox', NULL , '0', '', '', '', 'greater than or equal to 2 mitoses/10 HPF to 10 mitoses/10 HPF', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'greater_or_equal_to_2_mitoses_specify_per_10_HPF', 'input',  NULL , '0', '', '', '', '', 'specify mitoses per 10 HPF'), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'greater_than_10_mitoses_per_10_HPF', 'checkbox', NULL , '0', '', '', '', 'greater than 10 mitoses per 10 HPF', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'mitotic_cannot_be_determined', 'checkbox', NULL , '0', '', '', '', 'mitotic cannot be determined', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'less_or_equal_2percent_Ki67_positive_cells', 'checkbox', NULL , '0', '', '', '', 'less or equal 2% Ki67-positive cells', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', '3_to_20percent_Ki67_positive_cells', 'checkbox', NULL , '0', '', '', '', '3%-20% Ki67-positive cells', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'great_than_20percent_Ki67_positive_cells', 'checkbox', NULL , '0', '', '', '', 'great than 20% Ki67-positive cells', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchum_hpb_ed_liver_metastasis_lab_reports', 'notes', 'textarea',  NULL , '0', 'cols=40, rows=6', '', '', 'notes', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='histologic_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_liver_metastasis_hitologic_types')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='histologic type' AND `language_tag`=''), 
'1', '1', 'histology', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='histologic_type_specify' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='histologic type specify' AND `language_tag`=''), 
'1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='necrosis_perc' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='necrosis percentage' AND `language_tag`=''), 
'1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='necrosis_perc_list' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_viable_cells_perc')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='necrosis percentage list' AND `language_tag`=''), 
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='rubbia_brandt' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_rubbia_brandt_values')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='rubbia brandt' AND `language_tag`=''), 
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='blazer' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_blazer_values')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='blazer' AND `language_tag`=''), 
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='lesions_nbr' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='lesions nbr' AND `language_tag`=''), 
'1', '5', 'lesions', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='tumor_size_greatest_dimension' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='tumor size greatest dimension (cm)' AND `language_tag`=''), 
'1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='additional_dimension_a' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='additional dimension (cm)' AND `language_tag`=''), 
'1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='additional_dimension_b' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='x'), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='tumor_size_cannot_be_determined' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='cannot be determined' AND `language_tag`=''), 
'1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='other_lesion_size_greatest_dimension' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other lesion greatest dimension' AND `language_tag`=''), 
'1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='other_lesion_size_additional_dimension_a' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other lesion additional dimension a' AND `language_tag`=''), 
'1', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='other_lesion_size_additional_dimension_b' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='additional dimension b'), 
'1', '12', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='other_lesion_size_cannot_be_determined' AND `type`='yes_no' AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other lesion cannot be determined' AND `language_tag`=''), 
'1', '13', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='tumor_site' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_liver_metastasis_tumor_sites')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='tumor site' AND `language_tag`=''), 
'1', '15', 'tumor site', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='tumor_site_specify' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='tumor site specify' AND `language_tag`=''), 
'1', '16', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='surgical_resection_margin' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_positive_negative')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='surgical resection margin' AND `language_tag`=''), 
'1', '25', 'margins', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='distance_of_tumor_from_closest_surgical_resection_margin_cm' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_tag`=''), 
'1', '26', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='specify_margin' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='specify margin'), 
'1', '28', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='adjacent_liver_parenchyma' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_adjacent_liver_parenchyma')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='adjacent liver parenchyma' AND `language_tag`=''), 
'1', '40', 'parenchyma', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='adjacent_liver_parenchyma_specify' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='adjacent liver parenchyma specify' AND `language_tag`=''), 
'1', '41', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='k_ras' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_N_K_ras_values')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='K-ras' AND `language_tag`=''), 
'1', '42', 'genetic', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='world_health_organization_classification' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='WHO_classification_pe')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='world health organization classification' AND `language_tag`=''), 
'2', '42', 'WHO classification', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='k_ras_codon' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='codon'), 
'1', '43', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='k_ras_mutation' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='mutation'), 
'1', '44', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='n_ras' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_N_K_ras_values')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='n-ras' AND `language_tag`=''), 
'1', '45', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='n_ras_codon' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='codon'), 
'1', '46', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='n_ras_mutation' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='mutation'), 
'1', '47', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='mitotic_not_applicable' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='mitotic activity' AND `language_label`='not applicable' AND `language_tag`=''), 
'2', '54', 'mitotic activity', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='less_than_2_mitoses_10_high_power_fields' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='less than 2 mitoses/10 high power fields (HPF)' AND `language_tag`=''), 
'2', '55', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='less_than_2_mitoses_specify_per_10_HPF' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='specify mitoses per 10 HPF'), 
'2', '56', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='greater_than_or_equal_to_2_mitoses_10_HPF_to_10_mitoses_10_HPF' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='greater than or equal to 2 mitoses/10 HPF to 10 mitoses/10 HPF' AND `language_tag`=''), 
'2', '57', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='greater_or_equal_to_2_mitoses_specify_per_10_HPF' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='specify mitoses per 10 HPF'), 
'2', '58', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='greater_than_10_mitoses_per_10_HPF' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='greater than 10 mitoses per 10 HPF' AND `language_tag`=''), 
'2', '59', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='mitotic_cannot_be_determined' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='mitotic cannot be determined' AND `language_tag`=''), 
'2', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='less_or_equal_2percent_Ki67_positive_cells' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='less or equal 2% Ki67-positive cells' AND `language_tag`=''), 
'2', '61', 'Ki67 labeling index', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='3_to_20percent_Ki67_positive_cells' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='3%-20% Ki67-positive cells' AND `language_tag`=''), 
'2', '62', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='great_than_20percent_Ki67_positive_cells' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='great than 20% Ki67-positive cells' AND `language_tag`=''), 
'2', '63', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_liver_metastasis_lab_report'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchum_hpb_ed_liver_metastasis_lab_reports' AND `field`='notes' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='cols=40, rows=6' AND `default`='' AND `language_help`='' AND `language_label`='notes' AND `language_tag`=''), 
'2', '300', 'other', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("crchum hpb - liver metastases lab report", "Liver Metastases Lab Report (CRCHUM Hpb)", "Rapport métastases au foie (CRCHUM Hpb)"),
("distance of tumor from closest surgical resection margin (cm)", "Distance of Tumor from Closest Surgical Resection Margin (cm)", ""),
("genetic", "Genetic", ""),
("parenchyma", "Parenchyma", "Parenchyme"),
("3%-20% Ki67-positive cells", "3&#37;-20&#37; Ki67-Positive Cells", "3&#37;-20&#37; cellules Ki67 positives"),
("additional dimension (cm)", "Additional Dimension (cm)", ""),
("additional dimension b", "x", "x"),
("adjacent liver parenchyma", "Adjacent liver parenchyma", "Parenchyme adjacent au foie"),
("adjacent liver parenchyma specify", "Adjacent liver parenchyma specify", ""),
("blazer", "Blazer", ""),
("cannot be determined", "Cannot be determined ", ""),
("codon", "Codon", "Codon"),
("distance of tumor from closest surgical resection margin", "Distance of Tumor from Closest Surgical Resection Margin", ""),
("great than 20% Ki67-positive cells", "&gt;20&#37; Ki67-Positive Cells", "&gt;20&#37; cellules Ki67 positives"),
("greater than 10 mitoses per 10 HPF", "Greater than 10 mitoses per 10 HPF", ""),
("greater than or equal to 2 mitoses/10 HPF to 10 mitoses/10 HPF", "Greater than or equal to 2 mitoses/10 HPF to 10 mitoses/10 HPF", ""),
("histologic type", "Histologic type", "Type histologique"),
("histologic type specify", "Specify", ""),
("K-ras", "K-ras", ""),
("lesions nbr", "Lesions Nbr", ""),
("lesions", "Lesions", ""),
("less or equal 2% Ki67-positive cells", "&lt;=2&#37; Ki67-Positive Cells", "&lt;=2&#37; cellules Ki67 positives"),
("less than 2 mitoses/10 high power fields (HPF)", "Less than 2 mitoses/10 high power fields (HPF)", ""),
("mitotic cannot be determined", "Mitotic Cannot Be Determined", ""),
("mutation", "Mutation", "Mutation"),
("n-ras", "N-ras", "N-ras"),
("necrosis percentage", "Necrosis &#37;", "Nécrose &#37;"),
("necrosis percentage list", "Necrosis &#37; (List)", "Nécrose &#37; (liste)"),
("not applicable", "Not Applicable", "Non applicable"),
("notes", "Notes", "Notes"),
("other lesion additional dimension a", "Other Lesion - Additional Dimension", "Dimension supplémentaire - Autre lésion"),
("other lesion cannot be determined", "Other Lesion Greatest Dimension (cm)", ""),
("other lesion greatest dimension", "Other Lesion Additional Dimensions (cm)", ""),
("rubbia brandt", "Rubbia Brandt", ""),
("specify margin", "Specify margin", "Spécifiez la marge"),
("specify mitoses per 10 HPF", "Specify mitoses per 10 HPF", ""),
("surgical resection margin", "Surgical Resection Margin", ""),
("tumor site", "Tumor Site", "Site tumoral"),
("tumor site specify", "Specify", ""),
("tumor size greatest dimension (cm)", "Tumor Size Greatest Dimension (cm)", ""),
("world health organization classification", "World Health Organization Classification", "");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;