-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CTRNet Demo : Biomarker
-- ................................................................................
-- Form developped to capture data of biomarker tests.
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, '', 'lab', 'ctrnet demo - biomarker', 1, 'ctrnet_demo_ed_lab_biomarker', 'ctrnet_demo_ed_lab_biomarkers', 0, 0, 1, 1);

DROP TABLE IF EXISTS ctrnet_demo_ed_lab_biomarkers;
CREATE TABLE IF NOT EXISTS ctrnet_demo_ed_lab_biomarkers (
  event_master_id int(11) NOT NULL,
  biomarker_test varchar(100) DEFAULT NULL,
  biomarker_test_precision varchar(250) DEFAULT NULL,
  biomarker_test_result decimal(10,2) DEFAULT NULL,
  biomarker_test_unit varchar(20) DEFAULT NULL,
  biomarker_test_ccl varchar(20) DEFAULT NULL,
  biomarker_test_result_precision varchar(250) DEFAULT NULL,
  KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS ctrnet_demo_ed_lab_biomarkers_revs;
CREATE TABLE IF NOT EXISTS ctrnet_demo_ed_lab_biomarkers_revs (
  event_master_id int(11) NOT NULL,
  biomarker_test varchar(100) DEFAULT NULL,
  biomarker_test_precision varchar(250) DEFAULT NULL,
  biomarker_test_result decimal(10,2) DEFAULT NULL,
  biomarker_test_unit varchar(20) DEFAULT NULL,
  biomarker_test_ccl varchar(20) DEFAULT NULL,
  biomarker_test_result_precision varchar(250) DEFAULT NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE ctrnet_demo_ed_lab_biomarkers
  ADD CONSTRAINT ctrnet_demo_ed_lab_biomarkers_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES
('ctrnet_demo_biomarkers', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CTRNet Demo : Biomarkers\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('CTRNet Demo : Biomarkers', 1, 100, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CTRNet Demo : Biomarkers');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("Ca125", "", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ER Overall", "ER Overall", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ER Percent", "ER Percent", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("PR Overall", "PR Overall", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("PR Percent", "PR Percent", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("HER2 IHC", "HER2 IHC", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("HER2 FISH", "HER2 FISH", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("HER2 Status", "HER2 Status", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("PSA", "", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES
('ctrnet_demo_biomarker_test_units', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CTRNet Demo : Biomarker Test Units\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('CTRNet Demo : Biomarker Test Units', 1, 20, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CTRNet Demo : Biomarker Test Units');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("%", "", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("ng/ml", "", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("u/L", "", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("umol/L", "", "", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES
('ctrnet_demo_biomarker_test_conclusions', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CTRNet Demo : Biomarker Test Conclusions\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('CTRNet Demo : Biomarker Test Conclusions', 1, 20, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CTRNet Demo : Biomarker Test Conclusions');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("positive", "Positive", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("negative", "Negative", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("n/a", "N/A", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structures(`alias`) VALUES ('ctrnet_demo_ed_lab_biomarker');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biomarkers', 'biomarker_test', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_biomarkers') , '0', '', '', '', 'test', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biomarkers', 'biomarker_test_precision', 'input',  NULL , '0', 'size=20', '', '', '', 'precision'), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biomarkers', 'biomarker_test_result', 'float',  NULL , '0', 'size=5', '', '', 'result', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biomarkers', 'biomarker_test_unit', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_biomarker_test_units') , '0', '', '', '', '', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biomarkers', 'biomarker_test_ccl', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_biomarker_test_conclusions') , '0', '', '', '', 'conclusion', ''), 
('ClinicalAnnotation', 'EventDetail', 'ctrnet_demo_ed_lab_biomarkers', 'biomarker_test_result_precision', 'input',  NULL , '0', 'size=20', '', '', 'precision', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biomarker'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '100', 'summary', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=1', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biomarker'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biomarkers' AND `field`='biomarker_test' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_biomarkers')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='test' AND `language_tag`=''), 
'1', '5', 'test', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biomarker'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biomarkers' AND `field`='biomarker_test_precision' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='precision'), 
'1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biomarker'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biomarkers' AND `field`='biomarker_test_result' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='result' AND `language_tag`=''), 
'1', '7', 'result', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biomarker'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biomarkers' AND `field`='biomarker_test_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_biomarker_test_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),  
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biomarker'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biomarkers' AND `field`='biomarker_test_ccl' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='ctrnet_demo_biomarker_test_conclusions')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='conclusion' AND `language_tag`=''), 
'1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ctrnet_demo_ed_lab_biomarker'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ctrnet_demo_ed_lab_biomarkers' AND `field`='biomarker_test_result_precision' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='precision' AND `language_tag`=''), 
'1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("conclusion", "Conclusion", "Conclusion"),
("ctrnet demo - biomarker", "Biomarker (CTRNet Demo)", "Biomarqueur (CTRNet Demo)");
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;