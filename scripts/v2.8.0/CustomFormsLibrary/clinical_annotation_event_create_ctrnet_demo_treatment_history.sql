-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CTRNet Demo : Biology
-- ................................................................................
-- Form developed to capture chemotherapy and radiotherapy history
-- at a specific date to help to the identification of participants with
-- no radiotherapy or chemotherapy at a specific date.
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, '', 'clinical', 'ctrnet demo - treatment history', 1, 'ed_all_treatments_history', 'ed_all_treatments_histories', 0, 0, 1, 1);

DROP TABLE IF EXISTS `ed_all_treatments_histories`;
CREATE TABLE `ed_all_treatments_histories` (
 `event_master_id` int(11) NOT NULL,
 `tx_radio` char(1) DEFAULT '',
 `tx_chemo` char(1) DEFAULT '',
  KEY `event_master_id` (`event_master_id`),
  CONSTRAINT `ed_all_treatments_histories_ibfk_1` FOREIGN KEY (`event_master_id`) REFERENCES `event_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `ed_all_treatments_histories_revs`;
CREATE TABLE `ed_all_treatments_histories_revs` (
 `event_master_id` int(11) NOT NULL,
 `tx_radio` char(1) DEFAULT '',
 `tx_chemo` char(1) DEFAULT '',
 `version_id` int(11) NOT NULL AUTO_INCREMENT,
 `version_created` datetime NOT NULL,
 PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO structures(`alias`) VALUES ('ed_all_treatments_history');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'ed_all_treatments_histories', 'tx_radio', 'yes_no',  NULL , '0', '', '', '', 'radiotherapy', ''), 
('ClinicalAnnotation', 'EventDetail', 'ed_all_treatments_histories', 'tx_chemo', 'yes_no',  NULL , '0', '', '', '', 'chemotherapy', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ed_all_treatments_history'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='cols=40,rows=6' AND `default`='' AND `language_help`='' AND `language_label`='summary' AND `language_tag`=''), 
'1', '99', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ed_all_treatments_history'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ed_all_treatments_histories' AND `field`='tx_radio' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='radiotherapy' AND `language_tag`=''), 
'1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ed_all_treatments_history'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ed_all_treatments_histories' AND `field`='tx_chemo' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='chemotherapy' AND `language_tag`=''), 
'1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("ctrnet demo - treatment history", "Radio/Chemo Therapy History", "Historique radio/chimio thérapie"),
('radiotherapy', 'Radiotherapy', 'Radiothérapie'),
('treatment date', 'Date of Event', 'Date de l\'évenement');

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;
