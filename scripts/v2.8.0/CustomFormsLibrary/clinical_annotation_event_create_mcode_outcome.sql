-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- mCode ((Minimal Common Oncology Data Elements) initiative : 
--   Create outcome form based on 'Outcome' group
--   and 'Cancer Disease Status' profile fields  
--   of the mCODE™ DataDictionnary Version 0.9.1.
-- ................................................................................

--   List based on :
--    - Status
--       mCODE™ Value Set Name : ConditionStatusTrendVS
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, '', 'clinical', 'mcode - outcome', 1, 'mcode_ed_clinical_outcome', 'mcode_ed_clinical_outcomes', 0, 0, 1, 1);

DROP TABLE IF EXISTS mcode_ed_clinical_outcomes;
CREATE TABLE IF NOT EXISTS mcode_ed_clinical_outcomes (
  event_master_id int(11) NOT NULL,
  status varchar(50) default NULL,
  KEY event_master_id (event_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS mcode_ed_clinical_outcomes_revs;
CREATE TABLE IF NOT EXISTS mcode_ed_clinical_outcomes_revs (
  event_master_id int(11) NOT NULL,
  status varchar(50) default NULL,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE mcode_ed_clinical_outcomes
  ADD CONSTRAINT mcode_ed_clinical_outcomes_ibfk_1 FOREIGN KEY (event_master_id) REFERENCES event_masters (id);

INSERT INTO structures(`alias`) VALUES ('mcode_ed_clinical_outcome');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'mcode_ed_clinical_outcomes', 'status', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='mcode_value_set_ConditionStatusTrendVS') , '0', '', '', 'help_mcode_outcome_status', 'status', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='mcode_ed_clinical_outcome'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_clinical_outcomes' AND `field`='status'), 
'1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='mcode_ed_clinical_outcome'),
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=1', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_validations (structure_field_id, rule, language_message)
VALUES
((SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='mcode_ed_clinical_outcomes' AND `field`='status' AND `type`='select'),
'notBlank', '');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('mcode - outcome', 'Outcome (mCODE™)', 'xxxx (mCODE™)'),

('help_mcode_outcome_status',
"The code representing a clinician's qualitative judgment on the current trend of the cancer, e.g., whether it is stable, worsening (progressing), or improving (responding) as defined by the mCODE™ initiative (Minimal Common Oncology Data Elements).
The judgment may be based a single type or multiple kinds of evidence, such as imaging data, assessment of symptoms, tumor markers, laboratory data, etc.
Based on the ConditionStatusTrendVS Value Set details of the mCODE Data Dictionnary version 0.9.1.", 
"Le code correspondant à l'évaluation qualitatif du clinicien sur l'évolution actuelle du cancer (stable, progression ou amélioration) tel que défini par l'initiative mCODE™ (Minimal Common Oncology Data Elements).
Le jugement peut être basé sur une ou plusieures évaluations, telles que des données d'imagerie, une évaluation des symptômes, des marqueurs tumoraux, des données de laboratoire, etc.
Basé sur la liste des codes ConditionStatusTrendVS de la version 0.9.1 du dictionnaire de données mCode.");
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;