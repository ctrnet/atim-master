-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.3 / 2.7.4
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CRCHUS Oncology Axis Bank : Generic Resection Lab Report
-- ................................................................................
-- Form developped to capture data for the CRCHUS Oncology Axis BioBank.
-- Designed and created by the BioBank PIs.
-- --------------------------------------------------------------------------------

-- --------------------------------------------------------------------------------------------------------------------------------------------
-- CAP GENERIC
-- --------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `event_controls` (`id`, `disease_site`, `event_group`, `event_type`, `flag_active`, `detail_form_alias`, `detail_tablename`, `display_order`, `flag_use_for_ccl`, `use_addgrid`, `use_detail_form_for_index`) VALUES
(null, '', 'lab', 'crchus onco - cap report generic - resect.', 1, 'crchus_onco_ed_cap_report_generic_resections', 'crchus_onco_ed_cap_report_generic_resections', 0, 1, 0, 0);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('crchus onco - cap report generic - resect.', 'Cap Report Generic - Resection (CRCHUS OncoAxis)', "");

INSERT INTO structures(`alias`) VALUES ('crchus_onco_ed_cap_report_generic_resections');

DROP TABLE IF EXISTS `crchus_onco_ed_cap_report_generic_resections`;
CREATE TABLE IF NOT EXISTS `crchus_onco_ed_cap_report_generic_resections` (
  `event_master_id` int(11) NOT NULL,
  KEY `diagnosis_master_id` (`event_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `crchus_onco_ed_cap_report_generic_resections_revs`;
CREATE TABLE IF NOT EXISTS `crchus_onco_ed_cap_report_generic_resections_revs` (
  `event_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `crchus_onco_ed_cap_report_generic_resections`
  ADD CONSTRAINT `crchus_onco_ed_cap_report_generic_resections_ibfk_1` FOREIGN KEY (`event_master_id`) REFERENCES `event_masters` (`id`);

-- Specimen 

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN specimens varchar(500) DEFAULT NULL;
   
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN specimens varchar(500) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_specimens", "", "", CONCAT("StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Specimens\')"));
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Specimens", 1, 100, 'clinical annotation');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'specimens', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_specimens') , '0', 'class=atim-multiple-choice', '', '', 'specimen', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='specimens' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_specimens')), 
'1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) VALUES ("specimen", "Specimen", "");

-- Procedure

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `procedure` varchar(500) DEFAULT NULL,
   ADD COLUMN procedure_invasiveness varchar(100) DEFAULT NULL;
   
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `procedure` varchar(500) DEFAULT NULL,
   ADD COLUMN procedure_invasiveness varchar(100) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_procedure", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Procedures\')"),
("crchus_onco_cap_gen_procedure_invasiveness", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Procedures Invasiveness\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Procedures", 1, 100, 'clinical annotation'),
("Cap. Gen.: Procedures Invasiveness", 1, 100, 'clinical annotation');
SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Procedures");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Not specified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Right hemicolectomy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Transverse colectomy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Left hemicolectomy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Sigmoidectomy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Low anterior resection','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Total abdominal colectomy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Abdominoperineal resection','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Transanal disk excision (local excision)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Endoscopic mucosal resection; Wedge resection','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Partial hepatectomy/Major hepatectomy (3 segments or more)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Partial hepatectomy/Minor hepatectomy (less than 3 segments)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Total hepatectomy; Excisional biopsy (enucleation)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Pancreaticoduodenectomy (Whipple resection), partial pancreatectomy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Total pancreatectomy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Partial pancreatectomy, pancreatic body','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Partial pancreatectomy, pancreatic tail', '', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Procedures Invasiveness");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Open','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Laparoscopic','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Laparoscopic to Open','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Robotic','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Robotic to Laparoscopic','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Robotic to Open','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'procedure', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_procedure') , '0', 'class=atim-multiple-choice', '', '', 'procedure', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'procedure_invasiveness', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_procedure_invasiveness') , '0', '', '', '', 'procedure invasiveness', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='procedure' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_procedure')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='' AND `language_label`='procedure' AND `language_tag`=''), 
'1', '10', 'procedure', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='procedure_invasiveness' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_procedure_invasiveness')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='procedure invasiveness' AND `language_tag`=''), 
'1', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) VALUES ("procedure invasiveness", "Procedure Invasiveness", ""),("procedures", "Procedures", "Procedures"),("procedure", "Procedure", "Procedure");

-- TUMOR CHARACTERISTICS (first fields)

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `preoperative_therapy` varchar(100) DEFAULT NULL,
   ADD COLUMN `primary_tumor` tinyint(1) DEFAULT '0',
   ADD COLUMN `secondary_tumor` tinyint(1) DEFAULT '0',
   ADD COLUMN `nbr_of_tumors` int(5) DEFAULT NULL;
 ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `preoperative_therapy` varchar(100) DEFAULT NULL,
   ADD COLUMN `primary_tumor` tinyint(1) DEFAULT '0',
   ADD COLUMN `secondary_tumor` tinyint(1) DEFAULT '0',
   ADD COLUMN `nbr_of_tumors` int(5) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_preoperative_therapies", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Preoperative Therapies\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Preoperative Therapies", 1, 100, 'clinical annotation');
SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Preoperative Therapies");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('No preoperative therapy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Chemotherapy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Radiation therapy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Chemoradiotherapy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('Radiofrequency ablation','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Transarterial chemo-embolization','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Preoperative therapy, type not specified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'preoperative_therapy', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_preoperative_therapies') , '0', '', '', '', 'preoperative therapy', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'primary_tumor', 'checkbox',  NULL , '0', '', '', '', 'tumor', 'primary'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'secondary_tumor', 'checkbox',  NULL , '0', '', '', '', '', 'secondary'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'nbr_of_tumors', 'integer',  NULL , '0', 'size=3', '', '', 'nbr of tumors', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='preoperative_therapy' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_preoperative_therapies')  AND `flag_confidential`='0' AND `default`='' AND `language_help`='' AND `language_label`='preoperative therapy' AND `language_tag`=''), 
'1', '20', 'tumor characteristics', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='primary_tumor' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='tumor' AND `language_tag`='primary'), 
'1', '21', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='secondary_tumor' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='secondary'), 
'1', '22', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='nbr_of_tumors' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='nbr of tumors' AND `language_tag`=''), 
'1', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
('tumor characteristics', 'Tumor Characteristics', ''),
('preoperative therapy', 'Preoperative Therapy', ''),
('nbr of tumors', 'Nbr Of Tumors', '');

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `tumor_sites_1` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_size_greatest_dimension_1` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_a_1` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_b_1` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_size_cannot_be_determined_1` tinyint(1) DEFAULT '0',
   ADD COLUMN `tumor_size_cannot_be_determined_explain_1` varchar(250) DEFAULT NULL,
   ADD COLUMN `tratment_effects_1` varchar(200) DEFAULT NULL,
  
   ADD COLUMN `tumor_sites_2` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_size_greatest_dimension_2` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_a_2` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_b_2` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_size_cannot_be_determined_2` tinyint(1) DEFAULT '0',
   ADD COLUMN `tumor_size_cannot_be_determined_explain_2` varchar(250) DEFAULT NULL,
   ADD COLUMN `tratment_effects_2` varchar(200) DEFAULT NULL,

   ADD COLUMN `tumor_sites_3` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_size_greatest_dimension_3` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_a_3` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_b_3` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_size_cannot_be_determined_3` tinyint(1) DEFAULT '0',
   ADD COLUMN `tumor_size_cannot_be_determined_explain_3` varchar(250) DEFAULT NULL,
   ADD COLUMN `tratment_effects_3` varchar(200) DEFAULT NULL,

   ADD COLUMN `tumor_sites_4` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_size_greatest_dimension_4` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_a_4` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_b_4` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_size_cannot_be_determined_4` tinyint(1) DEFAULT '0',
   ADD COLUMN `tumor_size_cannot_be_determined_explain_4` varchar(250) DEFAULT NULL,
   ADD COLUMN `tratment_effects_4` varchar(200) DEFAULT NULL,
   
   ADD COLUMN `tumor_sites_5` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_size_greatest_dimension_5` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_a_5` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_b_5` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_size_cannot_be_determined_5` tinyint(1) DEFAULT '0',
   ADD COLUMN `tumor_size_cannot_be_determined_explain_5` varchar(250) DEFAULT NULL,
   ADD COLUMN `tratment_effects_5` varchar(200) DEFAULT NULL;

ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `tumor_sites_1` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_size_greatest_dimension_1` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_a_1` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_b_1` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_size_cannot_be_determined_1` tinyint(1) DEFAULT '0',
   ADD COLUMN `tumor_size_cannot_be_determined_explain_1` varchar(250) DEFAULT NULL,
   ADD COLUMN `tratment_effects_1` varchar(200) DEFAULT NULL,
  
   ADD COLUMN `tumor_sites_2` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_size_greatest_dimension_2` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_a_2` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_b_2` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_size_cannot_be_determined_2` tinyint(1) DEFAULT '0',
   ADD COLUMN `tumor_size_cannot_be_determined_explain_2` varchar(250) DEFAULT NULL,
   ADD COLUMN `tratment_effects_2` varchar(200) DEFAULT NULL,

   ADD COLUMN `tumor_sites_3` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_size_greatest_dimension_3` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_a_3` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_b_3` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_size_cannot_be_determined_3` tinyint(1) DEFAULT '0',
   ADD COLUMN `tumor_size_cannot_be_determined_explain_3` varchar(250) DEFAULT NULL,
   ADD COLUMN `tratment_effects_3` varchar(200) DEFAULT NULL,

   ADD COLUMN `tumor_sites_4` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_size_greatest_dimension_4` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_a_4` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_b_4` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_size_cannot_be_determined_4` tinyint(1) DEFAULT '0',
   ADD COLUMN `tumor_size_cannot_be_determined_explain_4` varchar(250) DEFAULT NULL,
   ADD COLUMN `tratment_effects_4` varchar(200) DEFAULT NULL,
   
   ADD COLUMN `tumor_sites_5` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_size_greatest_dimension_5` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_a_5` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `additional_dimension_b_5` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_size_cannot_be_determined_5` tinyint(1) DEFAULT '0',
   ADD COLUMN `tumor_size_cannot_be_determined_explain_5` varchar(250) DEFAULT NULL,
   ADD COLUMN `tratment_effects_5` varchar(200) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_tumor_sites", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Tumor Sites\')"),
("crchus_onco_cap_gen_treatement_effects", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Treatments Effects\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Tumor Sites", 1, 100, 'clinical annotation'),
("Cap. Gen.: Treatments Effects", 1, 200, 'clinical annotation');

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Tumor Sites");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cecum','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Ileocecal valve','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Ascending colon','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Hepatic flexure','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Transverse colon','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Splenic flexure','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Descending colon','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Sigmoid colon','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Rectosigmoid','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Rectum','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon, not otherwise specified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Right lobe','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Left lobe','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Caudate lobe','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Quadrate lobe','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Segment I','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Segment II','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Segment III','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Segment IV','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Segment V','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Segment VI','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Segment VII','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Segment VIII','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Liver, not otherwise specified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreatic head','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Uncinate process','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreatic body','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreatic tail','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Pancreas, not otherwise specified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Treatments Effects");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not applicable/No preoperative therapy','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present/No viable cancer cells (complete response, score 0)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present/Single cells or rare small groups of cancer cells (near complete response, score 1)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present/Residual cancer with evident tumor regression, but more than single cells or rare small groups of cancer cells (partial response, score 2)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Absent/Extensive residual cancer with no evident tumor regression (poor or no response, score 3)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Complete necrosis (no viable tumor)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Incomplete necrosis (viable tumor present)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('No necrosis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_sites_1', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_sites') , '0', 'class=atim-multiple-choice', '', '', 'tumor 1 - sites', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_greatest_dimension_1', 'float',  NULL , '0', 'size=3', '', '', '- dimension (cm)', 'greatest '), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_dimension_a_1', 'float',  NULL , '0', 'size=3', '', '', '', 'additional'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_dimension_b_1', 'float',  NULL , '0', 'size=3', '', '', '', 'x'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_cannot_be_determined_1', 'checkbox',  NULL , '0', '', '', '', '', 'cannot be determined'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_cannot_be_determined_explain_1', 'input',  NULL , '0', '', '', '', '', 'explain'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tratment_effects_1', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_treatement_effects') , '0', '', '', '', '- treatment effect', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_sites_1'), 
'1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_greatest_dimension_1' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='- dimension (cm)' AND `language_tag`='greatest '), 
'1', '71', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_dimension_a_1' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='additional'), 
'1', '72', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_dimension_b_1' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='x'), 
'1', '73', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_cannot_be_determined_1' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='cannot be determined'), 
'1', '74', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_cannot_be_determined_explain_1' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='explain'), 
'1', '75', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tratment_effects_1' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_treatement_effects')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='- treatment effect' AND `language_tag`=''), 
'1', '76', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_sites_2', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_sites') , '0', 'class=atim-multiple-choice', '', '', 'tumor 2 - sites', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_greatest_dimension_2', 'float',  NULL , '0', 'size=3', '', '', '- dimension (cm)', 'greatest '), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_dimension_a_2', 'float',  NULL , '0', 'size=3', '', '', '', 'additional'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_dimension_b_2', 'float',  NULL , '0', 'size=3', '', '', '', 'x'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_cannot_be_determined_2', 'checkbox',  NULL , '0', '', '', '', '', 'cannot be determined'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_cannot_be_determined_explain_2', 'input',  NULL , '0', '', '', '', '', 'explain'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tratment_effects_2', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_treatement_effects') , '0', '', '', '', '- treatment effect', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_sites_2'), 
'1', '80', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_greatest_dimension_2' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='- dimension (cm)' AND `language_tag`='greatest '), 
'1', '81', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_dimension_a_2' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='additional'), 
'1', '82', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_dimension_b_2' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='x'), 
'1', '83', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_cannot_be_determined_2' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='cannot be determined'), 
'1', '84', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_cannot_be_determined_explain_2' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='explain'), 
'1', '85', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tratment_effects_2' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_treatement_effects')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='- treatment effect' AND `language_tag`=''), 
'1', '86', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_sites_3', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_sites') , '0', 'class=atim-multiple-choice', '', '', 'tumor 3 - sites', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_greatest_dimension_3', 'float',  NULL , '0', 'size=3', '', '', '- dimension (cm)', 'greatest '), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_dimension_a_3', 'float',  NULL , '0', 'size=3', '', '', '', 'additional'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_dimension_b_3', 'float',  NULL , '0', 'size=3', '', '', '', 'x'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_cannot_be_determined_3', 'checkbox',  NULL , '0', '', '', '', '', 'cannot be determined'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_cannot_be_determined_explain_3', 'input',  NULL , '0', '', '', '', '', 'explain'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tratment_effects_3', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_treatement_effects') , '0', '', '', '', '- treatment effect', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_sites_3'), 
'1', '90', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_greatest_dimension_3' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='- dimension (cm)' AND `language_tag`='greatest '), 
'1', '91', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_dimension_a_3' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='additional'), 
'1', '92', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_dimension_b_3' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='x'), 
'1', '93', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_cannot_be_determined_3' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='cannot be determined'), 
'1', '94', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_cannot_be_determined_explain_3' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='explain'), 
'1', '95', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tratment_effects_3' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_treatement_effects')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='- treatment effect' AND `language_tag`=''), 
'1', '96', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_sites_4', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_sites') , '0', 'class=atim-multiple-choice', '', '', 'tumor 4 - sites', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_greatest_dimension_4', 'float',  NULL , '0', 'size=3', '', '', '- dimension (cm)', 'greatest '), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_dimension_a_4', 'float',  NULL , '0', 'size=3', '', '', '', 'additional'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_dimension_b_4', 'float',  NULL , '0', 'size=3', '', '', '', 'x'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_cannot_be_determined_4', 'checkbox',  NULL , '0', '', '', '', '', 'cannot be determined'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_cannot_be_determined_explain_4', 'input',  NULL , '0', '', '', '', '', 'explain'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tratment_effects_4', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_treatement_effects') , '0', '', '', '', '- treatment effect', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_sites_4'), 
'1', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_greatest_dimension_4' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='- dimension (cm)' AND `language_tag`='greatest '), 
'1', '101', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_dimension_a_4' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='additional'), 
'1', '102', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_dimension_b_4' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='x'), 
'1', '103', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_cannot_be_determined_4' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='cannot be determined'), 
'1', '104', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_cannot_be_determined_explain_4' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='explain'), 
'1', '105', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tratment_effects_4' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_treatement_effects')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='- treatment effect' AND `language_tag`=''), 
'1', '106', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_sites_5', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_sites') , '0', 'class=atim-multiple-choice', '', '', 'tumor 5 - sites', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_greatest_dimension_5', 'float',  NULL , '0', 'size=3', '', '', '- dimension (cm)', 'greatest '), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_dimension_a_5', 'float',  NULL , '0', 'size=3', '', '', '', 'additional'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_dimension_b_5', 'float',  NULL , '0', 'size=3', '', '', '', 'x'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_cannot_be_determined_5', 'checkbox',  NULL , '0', '', '', '', '', 'cannot be determined'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_size_cannot_be_determined_explain_5', 'input',  NULL , '0', '', '', '', '', 'explain'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tratment_effects_5', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_treatement_effects') , '0', '', '', '', '- treatment effect', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_sites_5'), 
'1', '110', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_greatest_dimension_5' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='- dimension (cm)' AND `language_tag`='greatest '), 
'1', '111', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_dimension_a_5' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='additional'), 
'1', '112', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_dimension_b_5' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='x'), 
'1', '113', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_cannot_be_determined_5' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='cannot be determined'), 
'1', '114', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_size_cannot_be_determined_explain_5' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='explain'), 
'1', '115', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tratment_effects_5' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_treatement_effects')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='- treatment effect' AND `language_tag`=''), 
'1', '116', '', '5', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
('tumor 1 - sites', 'Tumor 1 - Sites', 'Tumeur 1 - Sites'),
('tumor 2 - sites', 'Tumor 2 - Sites', 'Tumeur 2 - Sites'),
('tumor 3 - sites', 'Tumor 3 - Sites', 'Tumeur 3 - Sites'),
('tumor 4 - sites', 'Tumor 4 - Sites', 'Tumeur 4 - Sites'),
('tumor 5 - sites', 'Tumor 5 - Sites', 'Tumeur 5 - Sites'),
('- dimension (cm)', '- Dimension (cm)', '- Dimension (cm)'),
('greatest', 'Greatest', 'Plus grande'),
('additional', 'Additional', 'Supplémentaires'),
('x', 'x', 'x'),
('- treatment effect', '- Treatment Effect', '');

-- Laterality

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `laterality` varchar(100) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `laterality` varchar(100) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_tumor_laterality", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Tumor Laterality\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Tumor Laterality", 1, 100, 'clinical annotation');
SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Tumor Laterality");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Left','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Right','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Midline','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'laterality', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_laterality') , '0', '', '', '', 'laterality', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='laterality' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_laterality')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='laterality' AND `language_tag`=''), 
'1', '120', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

-- Histologic data

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `histologic_type` varchar(100) DEFAULT NULL,
   ADD COLUMN `histologic_type_component` varchar(100) DEFAULT NULL,
   ADD COLUMN `histologic_type_pct` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `histologic_type_classification` varchar(100) DEFAULT NULL,
   ADD COLUMN `histologic_grade` varchar(100) DEFAULT NULL,
   ADD COLUMN `histologic_grade_system` varchar(100) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `histologic_type` varchar(100) DEFAULT NULL,
   ADD COLUMN `histologic_type_component` varchar(100) DEFAULT NULL,
   ADD COLUMN `histologic_type_pct` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `histologic_type_classification` varchar(100) DEFAULT NULL,
   ADD COLUMN `histologic_grade` varchar(100) DEFAULT NULL,
   ADD COLUMN `histologic_grade_system` varchar(100) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_histologic_types", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Histologic Types\')"),
("crchus_onco_cap_gen_histologic_type_component", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Histologic Types Component\')"),
("crchus_onco_cap_gen_histologic_type_classification_systems", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Histologic Types Classification Systems\')"),
("crchus_onco_cap_gen_histologic_grades", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Histologic Grades\')"),
("crchus_onco_cap_gen_histologic_grade_systems", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Histologic Grades Systems\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Histologic Types", 1, 100, 'clinical annotation'),
("Cap. Gen.: Histologic Types Component", 1, 100, 'clinical annotation'),
("Cap. Gen.: Histologic Types Classification Systems", 1, 100, 'clinical annotation'),
("Cap. Gen.: Histologic Grades", 1, 100, 'clinical annotation'),
("Cap. Gen.: Histologic Grades Systems", 1, 100, 'clinical annotation');

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Histologic Types");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Colon and Rectum/Adenocarcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Mucinous adenocarcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Signet-ring cell carcinoma (poorly cohesive carcinoma)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Medullary carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Serrated adenocarcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Micropapillary carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Adenosquamous carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Undifferentiated carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Carcinoma with sarcomatoid component','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Large cell neuroendocrine carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Small cell neuroendocrine carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Mixed neuroendocrine-non-neuroendocrine neoplasmcomponents)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Colon and Rectum/Carcinoma, type cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Liver/Hepatocellular carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Liver/Hepatocellular carcinoma, fibrolamellar','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Liver/Hepatocellular carcinoma, scirrhous','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Liver/Hepatocellular carcinoma, clear cell type','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Liver/Hepatoblastoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Liver/Carcinoma, type cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Ductal adenocarcinoma (NOS)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Colloid carcinoma (mucinous noncystic carcinoma)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Signet-ring cell carcinoma (poorly cohesive carcinoma)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Intraductal papillary-mucinous neoplasm with an associated invasive carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Intraductal oncocytic papillary neoplasm with associated invasive carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Intraductal papillary neoplasm with an associated invasive carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Mucinous cystic neoplasm with an associated invasive carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Medullary carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Adenosquamous carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Hepatoid carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Large cell carcinoma with rhabdoid phenotype','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Undifferentiated carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Undifferentiated carcinoma with osteoclast-like giant cells','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Acinar cell carcinoma (NOS)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Acinar cell cystadenocarcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Mixed acinar-neuroendocrine carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Mixed ductal-neuroendocrine carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Mixed acinar-endocrine-ductal carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Mixed acinar-ductal carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Pancreatoblastoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Solid-pseudopapillary neoplasm','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Solid-pseudopapillary neoplasm with high grade carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Large cell neuroendocrine carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Small cell neuroendocrine carcinoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreas/Mixed neuroendocrine-non-neuroendocrine neoplasm','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Histologic Types Classification Systems");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('WHO 2010','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('WHO 2019','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Histologic Grades");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Cannot be assessed','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('G1: Well differentiated','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('G2: Moderately differentiated','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('G3: Poorly differentiated','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('G4: Undifferentiated','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('GX: Cannot be assessed','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Low-grade (well differentiated to moderately differentiated)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('High-grade (poorly differentiated to undifferentiated)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Histologic Grades Systems");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('AJCC 7th Edition','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('AJCC 8th Edition','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'histologic_type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_histologic_types') , '0', '', '', '', 'histologic type', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'histologic_type_component', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_histologic_type_component') , '0', '', '', 'crchus_onco_help_histologic_type_component_and_pct', '', 'specify components'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'histologic_type_classification', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_histologic_type_classification_systems') , '0', '', '', '', '', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'histologic_grade', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_histologic_gradess') , '0', '', '', '', 'histologic grade', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'histologic_grade_system', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_histologic_grade_systems') , '0', '', '', '', '', 'grading system');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='histologic_type'), 
'1', '130', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='histologic_type_component'), 
'1', '131', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='histologic_type_classification'), 
'1', '132', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='histologic_grade' AND `type`='select'), 
'1', '133', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='histologic_grade_system' AND `type`='select'), 
'1', '134', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
UPDATE structure_fields SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_histologic_type_classification_systems') ,  `language_tag`='classification system' WHERE model='EventDetail' AND tablename='crchus_onco_ed_cap_report_generic_resections' AND field='histologic_type_classification' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_histologic_type_classification_systems');
UPDATE structure_fields SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='histologic_grade')  WHERE model='EventDetail' AND tablename='crchus_onco_ed_cap_report_generic_resections' AND field='histologic_grade' AND `type`='select' AND structure_value_domain  IS NULL ;

INSERT IGNORE INTO i18n (id,en,fr) VALUES 
('crchus_onco_help_histologic_type_component_and_pct', "Specify Components and their % : For mixed histologic types or presence of subtype(s)", ""),
('classification system', "Classification System", ""),
('specify components', "Specify Components", ""),
('grading system', "Grading System", "");

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'histologic_type_pct', 'float',  NULL , '0', 'size=3', '', '', '', 'pct_sign');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='histologic_type_pct' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='pct_sign'), 
'1', '131', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) VALUES 
('pct_sign', "%", "");

-- Other characteristics

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `tumor_extension` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_necrosis` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_necrosis_pct` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `macroscopic_tumor_perforation` varchar(100) DEFAULT NULL,
   ADD COLUMN `lymphovascular_invasion` varchar(100) DEFAULT NULL,
   ADD COLUMN `perineural_invasion` varchar(100) DEFAULT NULL,
   ADD COLUMN `additional_tumor_char_macro_observation` varchar(500) DEFAULT NULL,
   ADD COLUMN `additional_tumor_char_macro_observation_specify` varchar(250) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `tumor_extension` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_necrosis` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_necrosis_pct` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `macroscopic_tumor_perforation` varchar(100) DEFAULT NULL,
   ADD COLUMN `lymphovascular_invasion` varchar(100) DEFAULT NULL,
   ADD COLUMN `perineural_invasion` varchar(100) DEFAULT NULL,
   ADD COLUMN `additional_tumor_char_macro_observation` varchar(500) DEFAULT NULL,
   ADD COLUMN `additional_tumor_char_macro_observation_specify` varchar(250) DEFAULT NULL;   
   
INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_tumor_extensions", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Tumor Extensions\')"),
("crchus_onco_cap_gen_tumor_necrosis", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Tumor Necrosis\')"),
("crchus_onco_cap_gen_macroscopic_tumor_perforations", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Macroscopic Tumor Perforations\')"),
("crchus_onco_cap_gen_lymphovascular_invasions", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Lymphovascular Invasions\')"),
("crchus_onco_cap_gen_perineural_invasions", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Perineural Invasions\')"),
("crchus_onco_cap_gen_additional_tumor_char_macro_observations", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Addit. Tum. Charact. or Macro. Obs.\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Tumor Extensions", 1, 100, 'clinical annotation'),
("Cap. Gen.: Tumor Necrosis", 1, 100, 'clinical annotation'),
("Cap. Gen.: Macroscopic Tumor Perforations", 1, 100, 'clinical annotation'),
("Cap. Gen.: Lymphovascular Invasions", 1, 100, 'clinical annotation'),
("Cap. Gen.: Perineural Invasions", 1, 100, 'clinical annotation'),
("Cap. Gen.: Addit. Tum. Charact. or Macro. Obs.", 1, 100, 'clinical annotation');

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Tumor Extensions");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Cannot be assessed','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('No evidence of primary tumor','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('No invasion (carcinoma in situ/high-grade dysplasia)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor confined to liver','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor is confined to pancreas','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor invades lamina propria/muscularis mucosae (intramucosal carcinoma)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor invades submucosa','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor invades muscularis propria','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor invades through the muscularis propria into pericolorectal tissue','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor invades the visceral peritoneum (including tumor continuous with serosal surface through area of inflammation)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor invades ampulla of Vater or sphincter of Oddi','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor invades duodenal wall','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor invades peripancreatic soft tissues (retroperitoneal soft tissue, mesenteric adipose tissue, mesocolon, and/or extrapancreatic common bile duct)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor involves posterior surface of pancreas','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor involves anterior surface of pancreas','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor involves vascular bed/groove (corresponding to superior mesenteric vein/portal vein)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor involves a major branch of the portal vein','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor involves hepatic vein(s)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor involves (perforates) visceral peritoneum','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor directly invades gallbladder','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor directly invades diaphragm','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Tumor Necrosis");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not identified (absent)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Macroscopic Tumor Perforations");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not identified (absent)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Lymphovascular Invasions");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not identified (absent)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present (Vessel size not specified)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present (Small vessel lymphovascular invasion)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present (Large vessel invasion/Intramural)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present (Large vessel invasion/Extramural)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present (Large vessel invasion/Intramural and Extramural)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present (Small and Large vessel invasion/Intramural)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Perineural Invasions");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not identified (absent)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Addit. Tum. Charact. or Macro. Obs.");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('None, Not identified (absent)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Type of Polyp in Which Invasive Carcinoma Arose','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor Deposits','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Tumor budding','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Satellitosis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Macroscopic Evaluation of Mesorectum','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_extension', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_extensions') , '0', 'class=atim-multiple-choice', '', '', 'tumor extension', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_necrosis', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_necrosis') , '0', '', '', '', 'tumor necrosis', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_necrosis_pct', 'float',  NULL , '0', 'size=3', '', '', '', 'tumor necrosis pct'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'macroscopic_tumor_perforation', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_macroscopic_tumor_perforations') , '0', '', '', '', 'macroscopic tumor perforation', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'lymphovascular_invasion', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_lymphovascular_invasions') , '0', '', '', '', 'lymphovascular invasion', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'perineural_invasion', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_perineural_invasions') , '0', '', '', '', 'perineural invasion', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_tumor_char_macro_observation', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_additional_tumor_char_macro_observations') , '0', 'class=atim-multiple-choice', '', '', 'additional tumor char macro observation', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_tumor_char_macro_observation_specify', 'input',  NULL , '0', '', '', '', '', 'specify');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_extension' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_extensions')  AND `flag_confidential`='0' AND `default`='' AND `language_help`='' AND `language_label`='tumor extension' AND `language_tag`=''), 
'1', '140', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_necrosis' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_necrosis')  AND `flag_confidential`='0' AND `default`='' AND `language_help`='' AND `language_label`='tumor necrosis' AND `language_tag`=''), 
'1', '141', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_necrosis_pct' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor necrosis pct'), 
'1', '142', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='macroscopic_tumor_perforation' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_macroscopic_tumor_perforations')  AND `flag_confidential`='0' AND `default`='' AND `language_help`='' AND `language_label`='macroscopic tumor perforation' AND `language_tag`=''), 
'1', '143', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='lymphovascular_invasion' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_lymphovascular_invasions')  AND `flag_confidential`='0' AND `default`='' AND `language_help`='' AND `language_label`='lymphovascular invasion' AND `language_tag`=''), 
'1', '144', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='perineural_invasion' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_perineural_invasions')  AND `flag_confidential`='0' AND `default`='' AND `language_help`='' AND `language_label`='perineural invasion' AND `language_tag`=''), 
'1', '145', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_tumor_char_macro_observation' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_additional_tumor_char_macro_observations')  AND `flag_confidential`='0' AND `default`='' AND `language_help`='' AND `language_label`='additional tumor char macro observation' AND `language_tag`=''), 
'1', '146', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_tumor_char_macro_observation_specify' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='specify'), 
'1', '147', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) VALUES 
('tumor extension', "Tumor Extension", ""),
('tumor necrosis', "Tumor Necrosis", ""),
('tumor necrosis pct', "% of Necrosis", ""),
('macroscopic tumor perforation', "Macroscopic Tumor Perforation", ""),
('lymphovascular invasion', "Lymphovascular  Invasion", ""),
('perineural invasion', "Perineural Invasion", ""),
('additional tumor char macro observation', "Additional Tumor Characteristics or Macroscopic Observations", "");

-- Margin

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `margins` varchar(100) DEFAULT NULL,
   ADD COLUMN `closest_margin_to_tumor` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `closest_margin_to_tumor_unit` varchar(10) DEFAULT NULL,
   ADD COLUMN `closest_margin_to_tumor_specify` varchar(100) DEFAULT NULL,
   ADD COLUMN `margins_examined_1` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_1` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_1` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_1` varchar(10) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `margins` varchar(100) DEFAULT NULL,
   ADD COLUMN `closest_margin_to_tumor` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `closest_margin_to_tumor_unit` varchar(10) DEFAULT NULL,
   ADD COLUMN `closest_margin_to_tumor_specify` varchar(100) DEFAULT NULL,
   ADD COLUMN `margins_examined_1` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_1` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_1` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_1` varchar(10) DEFAULT NULL; 

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_margins", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Margins\')"),
("crchus_onco_cap_gen_margin_distance_units", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Margins Distance Units\')"),
("crchus_onco_cap_gen_closest_margin_to_tumor_details", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Specify Closest Margin\')"),
("crchus_onco_cap_gen_margins_examined", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Margins Examined\')"),
("crchus_onco_cap_gen_tumor_status_at_margin", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Status of tumor at margin\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Margins", 1, 100, 'clinical annotation'),
("Cap. Gen.: Margins Distance Units", 1, 10, 'clinical annotation'),
("Cap. Gen.: Specify Closest Margin", 1, 100, 'clinical annotation'),
("Cap. Gen.: Margins Examined", 1, 100, 'clinical annotation'),
("Cap. Gen.: Status of tumor at margin", 1, 100, 'clinical annotation');

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Margins");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cannot be assessed','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('All margins are uninvolved by tumor','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('At least one margin is involved by tumor','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Margins Distance Units");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('mm','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0),
('cm','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Specify Closest Margin");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Proximal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Proximal Pancreatic Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Proximal Margin (Gastric or Duodenal)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Distal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Distal Pancreatic Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Distal Margin (Distal Duodenal or Jejunal)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Radial (circumferential) or Mesenteric Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Deep Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Mucosal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreatic Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreatic Neck/Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Uncinate (Retroperitoneal/Superior Mesenteric Artery) Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Bile Duct Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Margins Examined");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Prépopuler le champs modulable avec les choix suivants : Proximal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Proximal Pancreatic Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Proximal Margin (Gastric or Duodenal)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Distal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Distal Pancreatic Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Distal Margin (Distal Duodenal or Jejunal)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Radial (circumferential) or Mesenteric Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Deep Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Mucosal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Hepatic Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreatic Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreatic Neck/Parenchymal Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Uncinate (Retroperitoneal/Superior Mesenteric Artery) Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Bile Duct Margin','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Status of Tumor at Margin");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Cannot be assessed','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Uninvolved by tumor (negative)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Involved by tumor (positive)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'margins', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins') , '0', '', '', '', 'margins', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'closest_margin_to_tumor', 'float',  NULL , '0', 'size=3', '', '', 'closest_margin_to_tumor', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'closest_margin_to_tumor_unit', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units') , '0', '', '', '', '', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'closest_margin_to_tumor_specify', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_closest_margin_to_tumor_details') , '0', '', '', '', 'closest_margin_to_tumor_specify', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'margins_examined_1', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined') , '0', '', '', '', 'margins_examined', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_status_at_margin_1', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin') , '0', '', '', '', 'tumor_status_at_margin', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_1', 'float',  NULL , '0', 'size=3', '', '', 'tumor_distance_from_margin', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_unit_1', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units') , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='margins' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='margins' AND `language_tag`=''), 
'2', '200', 'margins', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='closest_margin_to_tumor' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='closest_margin_to_tumor' AND `language_tag`=''), 
'2', '201', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='closest_margin_to_tumor_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '202', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='closest_margin_to_tumor_specify' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_closest_margin_to_tumor_details')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='closest_margin_to_tumor_specify' AND `language_tag`=''), 
'2', '203', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='margins_examined_1' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='margins_examined' AND `language_tag`=''), 
'2', '205', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_status_at_margin_1' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='tumor_status_at_margin' AND `language_tag`=''), 
'2', '206', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_1' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='tumor_distance_from_margin' AND `language_tag`=''), 
'2', '207', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_unit_1' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '208', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
UPDATE structure_fields SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined') ,  `setting`='class=atim-multiple-choice' WHERE model='EventDetail' AND tablename='crchus_onco_ed_cap_report_generic_resections' AND field='margins_examined' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined');

INSERT IGNORE INTO i18n (id,en,fr) VALUES
("closest_margin_to_tumor", "Distance of Tumor from Closest Margin", ""),  
("closest_margin_to_tumor_specify", "Specify Closest Margin", ""),  
("margins_examined", "Margins Examined", ""),  
("tumor_status_at_margin", "Status of Tumor at Margin", ""),  
("tumor_distance_from_margin", "Distance of Tumor From Margin", "");

UPDATE structure_fields SET `setting`='' WHERE model='EventDetail' AND tablename='crchus_onco_ed_cap_report_generic_resections' AND field='margins_examined_1' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined');
UPDATE structure_fields SET `language_label`='',  `language_tag`='tumor_status_at_margin' WHERE model='EventDetail' AND tablename='crchus_onco_ed_cap_report_generic_resections' AND field='tumor_status_at_margin_1' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin');
UPDATE structure_fields SET `language_label`='',  `language_tag`='tumor_distance_from_margin' WHERE model='EventDetail' AND tablename='crchus_onco_ed_cap_report_generic_resections' AND field='tumor_distance_from_margin_1' AND `type`='float' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET `language_label`='margins_examined 1' WHERE model='EventDetail' AND tablename='crchus_onco_ed_cap_report_generic_resections' AND field='margins_examined_1' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined');

INSERT IGNORE INTO i18n (id,en,fr) VALUES
("margins_examined 1", "Margins Examined - 1", "");  

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `nbr_of_examined_margins` int(5) DEFAULT NULL AFTER closest_margin_to_tumor_specify;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `nbr_of_examined_margins` int(5) DEFAULT NULL AFTER closest_margin_to_tumor_specify;
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'nbr_of_examined_margins', 'input',  NULL , '0', 'size=3', '', '', 'nbr of margins examined', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='nbr_of_examined_margins' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='nbr of margins examined' AND `language_tag`=''), 
'2', '203', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('nbr of margins examined', 'Nbr of Margins Examined', '');

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `margins_examined_2` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_2` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_2` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_2` varchar(10) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `margins_examined_2` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_2` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_2` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_2` varchar(10) DEFAULT NULL;
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'margins_examined_2', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined') , '0', '', '', '', 'margins_examined 2', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_status_at_margin_2', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin') , '0', '', '', '', '', 'tumor_status_at_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_2', 'float',  NULL , '0', 'size=3', '', '', '', 'tumor_distance_from_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_unit_2', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units') , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='margins_examined_2' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='margins_examined 2' AND `language_tag`=''), 
'2', '210', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_status_at_margin_2' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_status_at_margin'), 
'2', '211', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_2' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_distance_from_margin'), 
'2', '212', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_unit_2' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '213', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr) VALUES ("margins_examined 2", "Margins Examined - 2", ""); 

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `margins_examined_3` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_3` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_3` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_3` varchar(10) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `margins_examined_3` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_3` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_3` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_3` varchar(10) DEFAULT NULL;
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'margins_examined_3', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined') , '0', '', '', '', 'margins_examined 3', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_status_at_margin_3', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin') , '0', '', '', '', '', 'tumor_status_at_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_3', 'float',  NULL , '0', 'size=3', '', '', '', 'tumor_distance_from_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_unit_3', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units') , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='margins_examined_3' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='margins_examined 3' AND `language_tag`=''), 
'2', '214', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_status_at_margin_3' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_status_at_margin'), 
'2', '215', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_3' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_distance_from_margin'), 
'2', '216', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_unit_3' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '217', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr) VALUES ("margins_examined 3", "Margins Examined - 3", ""); 

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `margins_examined_4` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_4` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_4` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_4` varchar(10) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `margins_examined_4` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_4` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_4` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_4` varchar(10) DEFAULT NULL;
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'margins_examined_4', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined') , '0', '', '', '', 'margins_examined 4', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_status_at_margin_4', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin') , '0', '', '', '', '', 'tumor_status_at_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_4', 'float',  NULL , '0', 'size=3', '', '', '', 'tumor_distance_from_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_unit_4', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units') , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='margins_examined_4' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='margins_examined 4' AND `language_tag`=''), 
'2', '218', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_status_at_margin_4' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_status_at_margin'), 
'2', '219', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_4' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_distance_from_margin'), 
'2', '220', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_unit_4' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '221', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr) VALUES ("margins_examined 4", "Margins Examined - 4", ""); 

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `margins_examined_5` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_5` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_5` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_5` varchar(10) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `margins_examined_5` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_5` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_5` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_5` varchar(10) DEFAULT NULL;
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'margins_examined_5', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined') , '0', '', '', '', 'margins_examined 5', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_status_at_margin_5', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin') , '0', '', '', '', '', 'tumor_status_at_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_5', 'float',  NULL , '0', 'size=3', '', '', '', 'tumor_distance_from_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_unit_5', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units') , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='margins_examined_5' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='margins_examined 5' AND `language_tag`=''), 
'2', '222', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_status_at_margin_5' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_status_at_margin'), 
'2', '223', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_5' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_distance_from_margin'), 
'2', '224', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_unit_5' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '225', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr) VALUES ("margins_examined 5", "Margins Examined - 5", ""); 

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `margins_examined_6` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_6` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_6` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_6` varchar(10) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `margins_examined_6` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_6` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_6` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_6` varchar(10) DEFAULT NULL;
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'margins_examined_6', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined') , '0', '', '', '', 'margins_examined 6', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_status_at_margin_6', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin') , '0', '', '', '', '', 'tumor_status_at_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_6', 'float',  NULL , '0', 'size=3', '', '', '', 'tumor_distance_from_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_unit_6', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units') , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='margins_examined_6' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='margins_examined 6' AND `language_tag`=''), 
'2', '226', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_status_at_margin_6' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_status_at_margin'), 
'2', '227', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_6' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_distance_from_margin'), 
'2', '228', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_unit_6' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '229', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr) VALUES ("margins_examined 6", "Margins Examined - 6", ""); 

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `margins_examined_7` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_7` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_7` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_7` varchar(10) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `margins_examined_7` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_7` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_7` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_7` varchar(10) DEFAULT NULL;
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'margins_examined_7', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined') , '0', '', '', '', 'margins_examined 7', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_status_at_margin_7', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin') , '0', '', '', '', '', 'tumor_status_at_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_7', 'float',  NULL , '0', 'size=3', '', '', '', 'tumor_distance_from_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_unit_7', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units') , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='margins_examined_7' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='margins_examined 7' AND `language_tag`=''), 
'2', '230', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_status_at_margin_7' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_status_at_margin'), 
'2', '231', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_7' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_distance_from_margin'), 
'2', '232', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_unit_7' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '233', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr) VALUES ("margins_examined 7", "Margins Examined - 7", ""); 

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `margins_examined_8` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_8` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_8` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_8` varchar(10) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `margins_examined_8` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_8` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_8` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_8` varchar(10) DEFAULT NULL;
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'margins_examined_8', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined') , '0', '', '', '', 'margins_examined 8', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_status_at_margin_8', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin') , '0', '', '', '', '', 'tumor_status_at_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_8', 'float',  NULL , '0', 'size=3', '', '', '', 'tumor_distance_from_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_unit_8', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units') , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='margins_examined_8' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='margins_examined 8' AND `language_tag`=''), 
'2', '234', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_status_at_margin_8' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_status_at_margin'), 
'2', '235', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_8' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_distance_from_margin'), 
'2', '236', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_unit_8' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '237', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr) VALUES ("margins_examined 8", "Margins Examined - 8", ""); 

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `margins_examined_9` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_9` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_9` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_9` varchar(10) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `margins_examined_9` varchar(500) DEFAULT NULL,
   ADD COLUMN `tumor_status_at_margin_9` varchar(100) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_9` decimal(6,1) DEFAULT NULL,
   ADD COLUMN `tumor_distance_from_margin_unit_9` varchar(10) DEFAULT NULL;
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'margins_examined_9', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined') , '0', '', '', '', 'margins_examined 9', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_status_at_margin_9', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin') , '0', '', '', '', '', 'tumor_status_at_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_9', 'float',  NULL , '0', 'size=3', '', '', '', 'tumor_distance_from_margin'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'tumor_distance_from_margin_unit_9', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units') , '0', '', '', '', '', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='margins_examined_9' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margins_examined')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='margins_examined 9' AND `language_tag`=''), 
'2', '238', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_status_at_margin_9' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_tumor_status_at_margin')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_status_at_margin'), 
'2', '239', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_9' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=3' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='tumor_distance_from_margin'), 
'2', '240', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='tumor_distance_from_margin_unit_9' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_margin_distance_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'2', '241', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr) VALUES ("margins_examined 9", "Margins Examined - 9", ""); 

-- Regional Lymph Nodes

ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `no_ymph_nodes_submitted_found` tinyint(1) DEFAULT '0',
   ADD COLUMN `nbr_of_lymph_nodes_involved` int(5) DEFAULT NULL,
   ADD COLUMN `nbr_of_lymph_nodes_involved_can_not_be_determined` tinyint(1) DEFAULT '0',
   ADD COLUMN `nbr_of_lymph_nodes_involved_can_not_be_determined_detail` varchar(250) DEFAULT NULL,
   ADD COLUMN `nbr_of_lymph_nodes_examined` int(5) DEFAULT NULL,
   ADD COLUMN `nbr_of_lymph_nodes_examined_can_not_be_determined` tinyint(1) DEFAULT '0',
   ADD COLUMN `nbr_of_lymph_nodes_examined_can_not_be_determined_detail` varchar(250) DEFAULT NULL,
   ADD COLUMN `extranodal_extension` varchar(100) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `no_ymph_nodes_submitted_found` tinyint(1) DEFAULT '0',
   ADD COLUMN `nbr_of_lymph_nodes_involved` int(5) DEFAULT NULL,
   ADD COLUMN `nbr_of_lymph_nodes_involved_can_not_be_determined` tinyint(1) DEFAULT '0',
   ADD COLUMN `nbr_of_lymph_nodes_involved_can_not_be_determined_detail` varchar(250) DEFAULT NULL,
   ADD COLUMN `nbr_of_lymph_nodes_examined` int(5) DEFAULT NULL,
   ADD COLUMN `nbr_of_lymph_nodes_examined_can_not_be_determined` tinyint(1) DEFAULT '0',
   ADD COLUMN `nbr_of_lymph_nodes_examined_can_not_be_determined_detail` varchar(250) DEFAULT NULL,
   ADD COLUMN `extranodal_extension` varchar(100) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_extranodal_extensions", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Extranodal Extensions\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Extranodal Extensions", 1, 100, 'clinical annotation');
SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Extranodal Extensions");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not identified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Present','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'no_ymph_nodes_submitted_found', 'checkbox',  NULL , '0', '', '', '', 'no_ymph_nodes_submitted_found', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'nbr_of_lymph_nodes_involved', 'integer',  NULL , '0', '', '', '', 'nbr_of_lymph_nodes_involved', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'nbr_of_lymph_nodes_involved_can_not_be_determined', 'checkbox',  NULL , '0', '', '', '', 'can_not_be_determined', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'nbr_of_lymph_nodes_involved_can_not_be_determined_detail', 'input',  NULL , '0', '', '', '', '', 'specify'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'nbr_of_lymph_nodes_examined', 'integer',  NULL , '0', '', '', '', 'nbr_of_lymph_nodes_examined', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'nbr_of_lymph_nodes_examined_can_not_be_determined', 'checkbox',  NULL , '0', '', '', '', 'can_not_be_determined', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'nbr_of_lymph_nodes_examined_can_not_be_determined_detail', 'input',  NULL , '0', '', '', '', '', 'specify'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'extranodal_extension', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_extranodal_extensions') , '0', '', '', '', 'extranodal_extension', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='no_ymph_nodes_submitted_found'), 
'2', '280', 'regional lymph nodes', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='nbr_of_lymph_nodes_involved'), 
'2', '281', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='nbr_of_lymph_nodes_involved_can_not_be_determined'), 
'2', '282', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='nbr_of_lymph_nodes_involved_can_not_be_determined_detail'), 
'2', '283', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='nbr_of_lymph_nodes_examined' AND `type`='integer'), 
'2', '284', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='nbr_of_lymph_nodes_examined_can_not_be_determined'), 
'2', '285', '', '3', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='nbr_of_lymph_nodes_examined_can_not_be_determined_detail' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='specify'), 
'2', '286', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='extranodal_extension' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_extranodal_extensions')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='extranodal_extension' AND `language_tag`=''), 
'2', '287', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
("regional lymph nodes", "Regional Lymph Nodes", ""),
("no_ymph_nodes_submitted_found", "No lymph nodes examined or found", ""),
("nbr_of_lymph_nodes_involved", "Number of Lymph Nodes Involved", ""),
("can_not_be_determined", "Number cannot be determined", ""),
("nbr_of_lymph_nodes_examined", "Number of Lymph Nodes Examined", ""),
("extranodal_extension", "Extranodal Extension", "");

 -- Staging
 
ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `staging_system` varchar(100) DEFAULT NULL,
   ADD COLUMN `other_staging_system` tinyint(1) DEFAULT '0',
   ADD COLUMN `other_staging_system_specify` varchar(100) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `staging_system` varchar(100) DEFAULT NULL,
   ADD COLUMN `other_staging_system` tinyint(1) DEFAULT '0',
   ADD COLUMN `other_staging_system_specify` varchar(100) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `path_tnm_descriptor_m` char(1) DEFAULT '',
   ADD COLUMN `path_tnm_descriptor_r` char(1) DEFAULT '',
   ADD COLUMN `path_tnm_descriptor_y` char(1) DEFAULT '',
   ADD COLUMN `path_tstage` varchar(10) DEFAULT NULL,
   ADD COLUMN `path_nstage` varchar(10) DEFAULT NULL,
   ADD COLUMN `path_mstage` varchar(10) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `path_tnm_descriptor_m` char(1) DEFAULT '',
   ADD COLUMN `path_tnm_descriptor_r` char(1) DEFAULT '',
   ADD COLUMN `path_tnm_descriptor_y` char(1) DEFAULT '',
   ADD COLUMN `path_tstage` varchar(10) DEFAULT NULL,
   ADD COLUMN `path_nstage` varchar(10) DEFAULT NULL,
   ADD COLUMN `path_mstage` varchar(10) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_staging_system", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Pathologic Stage Classification (pTNM)\')"),
("crchus_onco_cap_gen_pt", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: pT\')"),
("crchus_onco_cap_gen_pn", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: pN\')"),
("crchus_onco_cap_gen_pm", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: pM\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Pathologic Stage Classification (pTNM)", 1, 100, 'clinical annotation'),
("Cap. Gen.: pT", 1, 100, 'clinical annotation'),
("Cap. Gen.: pN", 1, 100, 'clinical annotation'),
("Cap. Gen.: pM", 1, 100, 'clinical annotation');
SET @user_system_id = @user_id;

SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Pathologic Stage Classification (pTNM)");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cannot be assessed','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('AJCC 7th Edition','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('AJCC 8th Edition','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: pT");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('pTX','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pT0','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pTis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pT1','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pT1a','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pT1b','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pT1c','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pT2','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pT3','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pT4','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pT4a','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pT4b','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;


SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: pN");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('pNX','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pN0','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pN1','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pN1a','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pN1b','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pN1c','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pN2','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pN2a','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pN2b','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;


SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: pM");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('pM1','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pM1a','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pM1b','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('pM1c','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'staging_system', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_staging_system') , '0', '', '', '', 'patho staging system', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'other_staging_system', 'checkbox',  NULL , '0', '', '', '', '', 'other'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'other_staging_system_specify', 'input',  NULL , '0', '', '', '', '', 'specify');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='staging_system' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_staging_system')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='patho staging system' AND `language_tag`=''), 
'2', '290', 'pathologic staging (pTNM)', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='other_staging_system' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='other'), 
'2', '291', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='other_staging_system_specify' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='specify'), 
'2', '292', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'path_tnm_descriptor_m', 'yes_no',  NULL , '0', '', '', '', 'tnm descriptors', 'm multiple primary tumors'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'path_tnm_descriptor_r', 'yes_no',  NULL , '0', '', '', '', '', 'r recurrent'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'path_tnm_descriptor_y', 'yes_no',  NULL , '0', '', '', '', '', 'y posttreatment'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'path_tstage', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_pt') , '0', '', '', '', 'path tstage', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'path_nstage', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_pn') , '0', '', '', '', 'path nstage', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'path_mstage', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_pm') , '0', '', '', '', 'path mstage', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='path_tnm_descriptor_m' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='tnm descriptors' AND `language_tag`='m multiple primary tumors'), 
'2', '302', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='path_tnm_descriptor_r' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='r recurrent'), 
'2', '303', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='path_tnm_descriptor_y' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='y posttreatment'), 
'2', '304', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='path_tstage' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_pt')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='path tstage' AND `language_tag`=''), 
'2', '310', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='path_nstage' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_pn')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='path nstage' AND `language_tag`=''), 
'2', '311', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='path_mstage' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_pm')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='path mstage' AND `language_tag`=''), 
'2', '340', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
("patho staging system", 'Pathologic Stage Classification (pTNM)', '');

 -- Additional Pathologic Findings
 
ALTER TABLE crchus_onco_ed_cap_report_generic_resections
   ADD COLUMN `additional_pathologic_findings` varchar(500) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs
   ADD COLUMN `additional_pathologic_findings` varchar(500) DEFAULT NULL; 
 
INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_cap_gen_additional_pathologic_findings", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Cap. Gen.: Additional Pathologic Findings\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Cap. Gen.: Additional Pathologic Findings", 1, 100, 'clinical annotation');
SET @user_system_id = @user_id;
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Cap. Gen.: Additional Pathologic Findings");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Not applicable','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cannot be assessed','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Not specified (not mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('None identified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Adenoma(s), not otherwise specified','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Polyp(s)/Tubular adenoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Polyp(s)/Villous adenoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Polyp(s)/Tubulovillous adenoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Polyp(s)/Traditional serrated adenoma','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Polyp(s)/Sessile serrated adenoma/sessile serrated polyp','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Polyp(s)/Hamartomatous polyp','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Ulcerative colitis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Crohn disease','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Diverticulosis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Dysplasia arising in inflammatory bowel disease','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Other polyp(s)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pancreatic intraepithelial neoplasia','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic pancreatitis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Acute pancreatitis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Fibrosis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Cirrhosis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Low-grade dysplastic nodule','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('High-grade dysplastic nodule','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Steatosis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Steatohepatitis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Iron overload','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: HBV without HDV)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: HBV with HDV)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: HCV)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: HEV)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Drug-induced)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Nonalcoholic steatohepatitis/NASH)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Alcoholic)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Autoimmune)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Wilson disease)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Alpha-1 antitrypsin deficiency)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Celiac disease)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Cryptogenic)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Thyroid disorder)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Hereditary hemochromatosis)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Primary biliary cholangitis/cirrhosis)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Overlap syndrome)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Cannot be determined)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Chronic hepatitis (Etiology: Not specified/mentioned)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'additional_pathologic_findings', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_additional_pathologic_findings') , '0', '', '', '', 'additional pathologic findings', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='additional_pathologic_findings' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_additional_pathologic_findings')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='additional pathologic findings' AND `language_tag`=''), 
'2', '450', 'additional pathologic findings', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
UPDATE structure_fields SET `setting`='class=atim-multiple-choice' WHERE model='EventDetail' AND tablename='crchus_onco_ed_cap_report_generic_resections' AND field='additional_pathologic_findings' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_cap_gen_additional_pathologic_findings');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
("additional pathologic findings","Additional Pathologic Findings","");

-- Notes

ALTER TABLE crchus_onco_ed_cap_report_generic_resections ADD COLUMN report TEXT DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_generic_resections_revs ADD COLUMN report TEXT DEFAULT NULL;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_generic_resections', 'report', 'textarea',  NULL , '0', 'rows=1,cols=30', '', '', 'report', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '471', '', '0', '1', 'notes', '0', '', '0', '', '0', '', '1', 'rows=1,cols=30', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_generic_resections'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_generic_resections' AND `field`='report' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='rows=1,cols=30' AND `default`='' AND `language_help`='' AND `language_label`='report' AND `language_tag`=''), 
'2', '470', 'notes', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');

-- --------------------------------------------------------------------------------------------------------------------------------------------
-- CAP GENERIC - Ancylary studies
-- --------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `event_controls` (`id`, `disease_site`, `event_group`, `event_type`, `flag_active`, `detail_form_alias`, `detail_tablename`, `display_order`, `flag_use_for_ccl`, `use_addgrid`, `use_detail_form_for_index`) VALUES
(null, '', 'lab', 'crchus onco - cap report generic anc. std.', 1, 'crchus_onco_ed_cap_report_gen_resect_ancillary_studies', 'crchus_onco_ed_cap_report_gen_resect_ancillary_studies', 0, 0, 1, 1);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ("crchus onco - cap report generic anc. std.", 'Cap Report Generic - Resection - Ancillary Studies (CRCHUS OncoAxis)', "");

INSERT INTO structures(`alias`) VALUES ('crchus_onco_ed_cap_report_gen_resect_ancillary_studies');

DROP TABLE IF EXISTS `crchus_onco_ed_cap_report_gen_resect_ancillary_studies`;
CREATE TABLE IF NOT EXISTS `crchus_onco_ed_cap_report_gen_resect_ancillary_studies` (
  `event_master_id` int(11) NOT NULL,
  KEY `diagnosis_master_id` (`event_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `crchus_onco_ed_cap_report_gen_resect_ancillary_studies_revs`;
CREATE TABLE IF NOT EXISTS `crchus_onco_ed_cap_report_gen_resect_ancillary_studies_revs` (
  `event_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `crchus_onco_ed_cap_report_gen_resect_ancillary_studies`
  ADD CONSTRAINT `crchus_onco_ed_cap_report_gen_resect_ancillary_studies_ibfk_1` FOREIGN KEY (`event_master_id`) REFERENCES `event_masters` (`id`);

ALTER TABLE crchus_onco_ed_cap_report_gen_resect_ancillary_studies
   ADD COLUMN `test_category` varchar(100) DEFAULT NULL,
   ADD COLUMN `biomarker_tested_or_assay` varchar(100) DEFAULT NULL,
   ADD COLUMN `result` varchar(100) DEFAULT NULL,
   ADD COLUMN `result_specify` text DEFAULT NULL,
   ADD COLUMN `method` varchar(100) DEFAULT NULL;
ALTER TABLE crchus_onco_ed_cap_report_gen_resect_ancillary_studies_revs
   ADD COLUMN `test_category` varchar(100) DEFAULT NULL,
   ADD COLUMN `biomarker_tested_or_assay` varchar(100) DEFAULT NULL,
   ADD COLUMN `result` varchar(100) DEFAULT NULL,
   ADD COLUMN `result_specify` text DEFAULT NULL,
   ADD COLUMN `method` varchar(100) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, override, category, source) 
VALUES 
("crchus_onco_ancillary_study_test_catgegoris", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Ancylary Studies: Test Category\')"),
("crchus_onco_ancillary_study_biomarker_tested_or_assay", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Ancylary Studies: Biomarker Tested/Name of Assay\')"),
("crchus_onco_ancillary_study_results", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Ancylary Studies: Results\')"),
("crchus_onco_ancillary_study_methods", "", "", "StructurePermissibleValuesCustom::getCustomDropdown(\'Ancylary Studies: Testing Methods\')");
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES 
("Ancylary Studies: Test Category", 1, 100, 'clinical annotation'),
("Ancylary Studies: Biomarker Tested/Name of Assay", 1, 100, 'clinical annotation'),
("Ancylary Studies: Results", 1, 100, 'clinical annotation'),
("Ancylary Studies: Testing Methods", 1, 100, 'clinical annotation');
SET @user_system_id = @user_id;

SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Ancylary Studies: Test Category");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Mismatch Repair (MMR) Proteins','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Microsatellite Instability (MSI)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Promoter Methylation Analysis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Mutational Analysis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Expression','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Multiparameter Gene Expression/Protein Expression Assay','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Ancylary Studies: Biomarker Tested/Name of Assay");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('None','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('MLH1','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('MSH2','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('MSH6','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('PMS2','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Control','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('NCI panel and/or Mononucleotide panel','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('KRAS (Codons Assessed: 12+13+61+146)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('NRAS (Codons Assessed: 12+13+61)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('BRAF','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('BRAF (Mutation Assessed: V600E)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('PIK3CA','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Ancylary Studies: Results");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Intact nuclear expression','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Loss of nuclear expression','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Background nonneoplastic tissue/internal control shows intact nuclear expression','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('MSI-Stable (MSS)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('MSI-Low (MSI-L)/1%-29% of the markers exhibit instability','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('MSI-Low (MSI-L)/1 of the 5 NCI or mononucleotide markers exhibits instability','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('MSI-Low (MSI-L)/Other','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('MSI-High (MSI-H)/>=30% of the markers exhibit instability','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('MSI-High (MSI-H)/2 or more of the 5 NCI or mononucleotide markers exhibit instability','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('MSI-High (MSI-H)/Other','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('MSI-Cannot be determined','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Promoter hypermethylation present','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Promoter hypermethylation absent','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('No mutations detected','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Mutation(s) identified/Codon 12','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Mutation(s) identified/Codon 13','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Mutation(s) identified/Codon 61','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Mutation(s) identified/Codon 146','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Mutation identified/V600E','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Codon Exon 9 mutation present','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Exon 20 mutation present','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Exon 1-9 mutation present','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Positive cytoplasmic expression','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Negative for cytoplasmic expression','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Low risk','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('Moderate risk','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), 
('High risk','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = "Ancylary Studies: Testing Methods");
INSERT INTO `structure_permissible_values_customs` (`value`, en, fr, `use_as_input`, `control_id`, `modified_by`, `created`, `created_by`, `modified`, display_order)
VALUES
('Immunohistochemistry (IHC)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('In situ hybridization','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Duplication/deletion testing (MLPA)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Whole Genome Sequencing (WGS)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Whole Exome Sequencing (WES)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Methylation-specific real-time polymerase chain reaction (PCR)','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Direct Sanger sequencing','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Pyrosequencing','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('High-resolution melting analysis','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('PCR, allele-specific hybridization','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('Real-time PCR','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0), ('IHC for V600E gene product','', "", '1', @control_id, @user_system_id, NOW(),@user_system_id, NOW(), 0);
UPDATE structure_permissible_values_customs SET en = value WHERE control_id = @control_id;
UPDATE structure_permissible_values_customs SET value = LOWER(value) WHERE control_id = @control_id;

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_gen_resect_ancillary_studies', 'test_category', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_ancillary_study_test_catgegoris') , '0', '', '', '', 'test_category', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_gen_resect_ancillary_studies', 'biomarker_tested_or_assay', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_ancillary_study_biomarker_tested_or_assay') , '0', '', '', '', 'biomarker_tested_or_assay', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_gen_resect_ancillary_studies', 'result', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_ancillary_study_results') , '0', '', '', '', 'result', ''), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_gen_resect_ancillary_studies', 'result_specify', 'textarea',  NULL , '0', 'cols=40,rows=1', '', '', '', 'details'), 
('ClinicalAnnotation', 'EventDetail', 'crchus_onco_ed_cap_report_gen_resect_ancillary_studies', 'method', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_ancillary_study_methods') , '0', '', '', '', 'method', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_gen_resect_ancillary_studies'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'2', '100', 'summary', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=1', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_gen_resect_ancillary_studies'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_gen_resect_ancillary_studies' AND `field`='test_category' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_ancillary_study_test_catgegoris')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='test_category' AND `language_tag`=''), 
'1', '5', 'test', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_gen_resect_ancillary_studies'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_gen_resect_ancillary_studies' AND `field`='biomarker_tested_or_assay' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_ancillary_study_biomarker_tested_or_assay')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='biomarker_tested_or_assay' AND `language_tag`=''), 
'1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_gen_resect_ancillary_studies'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_gen_resect_ancillary_studies' AND `field`='result' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_ancillary_study_results')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='result' AND `language_tag`=''), 
'1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_gen_resect_ancillary_studies'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_gen_resect_ancillary_studies' AND `field`='result_specify' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='cols=40,rows=1' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='details'), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchus_onco_ed_cap_report_gen_resect_ancillary_studies'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='crchus_onco_ed_cap_report_gen_resect_ancillary_studies' AND `field`='method' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchus_onco_ancillary_study_methods')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='method' AND `language_tag`=''), 
'1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT IGNORE INTO i18n (id,en,fr) VALUES ("test_category", "Test category", ""),("biomarker_tested_or_assay", "Biomarker Tested/Name of Assay", "");
