-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CTRNet Demo : Radiotherapy
-- ................................................................................
-- Form developped to capture data of radiotherapy.
-- --------------------------------------------------------------------------------

INSERT INTO treatment_extend_controls (id, detail_tablename, detail_form_alias, flag_active, `type`) VALUES
(null, 'ctrnet_demo_txe_radiations', 'ctrnet_demo_txe_radiations', 1, 'ctrnet demo - radiotherapy');

INSERT INTO treatment_controls (id, tx_method, disease_site, flag_active, detail_tablename, detail_form_alias, display_order, applied_protocol_control_id, extended_data_import_process, flag_use_for_ccl, treatment_extend_control_id, use_addgrid, use_detail_form_for_index) 
VALUES
(null, 'ctrnet demo - radiotherapy', '', 1, 'txd_radiations', 'txd_radiations', 0, NULL, NULL, 0, (SELECT id FROM treatment_extend_controls WHERE type = 'ctrnet demo - radiotherapy'), 0, 0);

DROP TABLE IF EXISTS ctrnet_demo_txe_radiations;
CREATE TABLE IF NOT EXISTS ctrnet_demo_txe_radiations (
  treatment_extend_master_id int(11) NOT NULL,
  ctrnet_trunk_topography varchar(50) DEFAULT NULL,
  notes text,
  KEY FK_ctrnet_demo_txe_radiations_treatment_extend_masters (treatment_extend_master_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS ctrnet_demo_txe_radiations_revs;
CREATE TABLE IF NOT EXISTS ctrnet_demo_txe_radiations_revs (
  treatment_extend_master_id int(11) NOT NULL,
  ctrnet_trunk_topography varchar(50) DEFAULT NULL,
  notes text,
  version_id int(11) NOT NULL AUTO_INCREMENT,
  version_created datetime NOT NULL,
  PRIMARY KEY (version_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ctrnet_demo_txe_radiations`
  ADD CONSTRAINT `ctrnet_demo_txe_radiations_ibfk_2` FOREIGN KEY (`treatment_extend_master_id`) REFERENCES `treatment_extend_masters` (`id`);

INSERT INTO structures(`alias`) VALUES ('ctrnet_demo_txe_radiations');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'TreatmentExtendDetail', 'ctrnet_demo_txe_radiations', 'ctrnet_trunk_topography', 'autocomplete',  NULL , '0', 'size=10,url=/CodingIcd/CodingIcdo3s/autocomplete/topo,tool=/CodingIcd/CodingIcdo3s/tool/topo', '', '', 'site', ''), 
('ClinicalAnnotation', 'TreatmentExtendDetail', 'ctrnet_demo_txe_radiations', 'notes', 'input',  NULL , '0', 'rows=1,cols=30', '', '', 'notes', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ctrnet_demo_txe_radiations'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='ctrnet_demo_txe_radiations' AND `field`='ctrnet_trunk_topography' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingIcdo3s/autocomplete/topo,tool=/CodingIcd/CodingIcdo3s/tool/topo' AND `default`='' AND `language_help`='' AND `language_label`='site' AND `language_tag`=''), 
'1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='ctrnet_demo_txe_radiations'), 
(SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='ctrnet_demo_txe_radiations' AND `field`='notes' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `default`='' AND `language_help`='' AND `language_label`='notes' AND `language_tag`=''), 
'1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '0', '0', '0');

INSERT INTO structure_validations(structure_field_id, rule, language_message) VALUES
((SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='ctrnet_demo_txe_radiations' AND `field`='ctrnet_trunk_topography' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingIcdo3s/autocomplete/topo,tool=/CodingIcd/CodingIcdo3s/tool/topo' AND `default`='' AND `language_help`='' AND `language_label`='site'), 'validateIcdo3TopoCode', 'invalid topography code'),
((SELECT id FROM structure_fields WHERE `model`='TreatmentExtendDetail' AND `tablename`='ctrnet_demo_txe_radiations' AND `field`='ctrnet_trunk_topography' AND `type`='autocomplete' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10,url=/CodingIcd/CodingIcdo3s/autocomplete/topo,tool=/CodingIcd/CodingIcdo3s/tool/topo' AND `default`='' AND `language_help`='' AND `language_label`='site'), 'notBlank', '');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("ctrnet demo - radiotherapy", "Radiotherapy (CTRNet Demo)", "Radiotherapy (CTRNet Demo)");
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;