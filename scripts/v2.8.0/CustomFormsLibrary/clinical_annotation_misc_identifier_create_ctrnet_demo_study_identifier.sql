-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Create a 'Study Participant Identifier'
-- ................................................................................
-- Let user to create identifiers assigned to participants as part of a study.
-- --------------------------------------------------------------------------------

-- Create the new identifier

INSERT INTO `misc_identifier_controls` (`misc_identifier_name`, `flag_active`, `autoincrement_name`, `misc_identifier_format`, `flag_once_per_participant`, `flag_confidential`, `flag_unique`, `pad_to_length`, `reg_exp_validation`, `user_readable_format`, flag_link_to_study) 
VALUES
('ctrnet demo - patient study id', 1, '', '', 0, 0, 0, 0, '', '', '1');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
('ctrnet demo - patient study id', 'Patient Study ID', 'ID Patient - Étude');

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;