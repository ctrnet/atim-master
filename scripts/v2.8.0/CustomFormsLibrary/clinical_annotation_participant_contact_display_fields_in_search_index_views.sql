-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- --------------------------------------------------------------------------------
-- Display participant contact fields missing in search and index views
-- ................................................................................
-- Based on flag_add and flag_edit values.
-- --------------------------------------------------------------------------------

UPDATE structure_formats 
SET `flag_search`='1', `flag_index`='1' 
WHERE structure_id=(SELECT id FROM structures WHERE alias='participantcontacts') 
AND `flag_add`='1' AND `flag_edit`='1';
 
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;