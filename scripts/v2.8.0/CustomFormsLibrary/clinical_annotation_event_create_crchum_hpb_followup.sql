-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CRCHUM Hepatopancreatobiliary Bank : Follow-up
-- ................................................................................
-- Form developped to capture data for the CRCHUM Hepatopancreatobiliary BioBank.
-- Designed and created by the BioBank PIs.
-- --------------------------------------------------------------------------------

INSERT INTO event_controls (id, disease_site, event_group, event_type, flag_active, detail_form_alias, detail_tablename, display_order, flag_use_for_ccl, use_addgrid, use_detail_form_for_index) VALUES
(null, 'liver', 'clinical', 'crchum hpb - follow up', 1, 'crchum_hpb_ed_clinical_followup', 'ed_all_clinical_followups', 0, 0, 1, 1);

ALTER TABLE ed_all_clinical_followups
   ADD COLUMN crchum_hpb_disease_status_precision varchar(100) DEFAULT NULL,
   ADD COLUMN crchum_hpb_recurrence_localization varchar(100) DEFAULT NULL,
   ADD COLUMN crchum_hpb_recurrence_treatment varchar(100) DEFAULT NULL,
   ADD COLUMN crchum_hpb_recurrence_localization_precision varchar(50) DEFAULT NULL;
ALTER TABLE ed_all_clinical_followups_revs
   ADD COLUMN crchum_hpb_disease_status_precision varchar(100) DEFAULT NULL,
   ADD COLUMN crchum_hpb_recurrence_localization varchar(100) DEFAULT NULL,
   ADD COLUMN crchum_hpb_recurrence_treatment varchar(100) DEFAULT NULL,
   ADD COLUMN crchum_hpb_recurrence_localization_precision varchar(50) DEFAULT NULL;

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_vital_status', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Follow-Up Vital Status\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Follow-Up Vital Status', 1, 50, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Follow-Up Vital Status');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("alive with no evidence of disease", "Alive with no evidence of disease", "Vivant sans signe de maladie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("unknown", "Unknown", "Inconnu", "99", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("alive and well after re-current disease", "Alive and Well after re-current disease", "Vivant et bien portant après maladie récurrente", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("lost contact", "Lost contact", "Perte de contact", "98", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("alive with other cancer", "Alive with other cancer", "Vivant avec un autre cancer", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("died of unknown cause", "Died of unknown cause", "Décédé de cause inconnue", "12", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("died of other cause", "Died of other cause", "Décédé d\'autre cause ", "11", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("died of disease", "Died of Disease", "Décédé de maladie", "10", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("alive and well with disease", "Alive and well with disease", "Vivant et bien portant après maladie", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("alive, recurrence status unknown", "Alive, recurrence status unknown", "Vivant, statut de récurrence inconnu", "11", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_follow_up_recurrence_localizations', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Follow-Up Recurrence Localizations\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Follow-Up Recurrence Localizations', 1, 100, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Follow-Up Recurrence Localizations');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("adrenal", "Adrenal", "Surrénale", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("bile duct", "Bile Duct", "Voie biliaire", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("bone", "Bone", "Oc", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("brain", "Brain", "Cerveau", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("carcinosis", "Carcinosis", "Carcinose", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("colon", "Colon", "Colon", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("liver", "Liver", "Foie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("lung", "Lung", "Poumon", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("lymph nodes", "Lymph Nodes", "Ganglions lymphatiques", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("ovary", "Ovary", "Ovaire", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("pancreas", "Pancreas", "Pancréas", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("peritoneum", "Peritoneum", "Péritoine", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("rectum", "Rectum", "Rectum", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("spleen", "Spleen", "Rate", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("multiple", "Multiple", "Multiple", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "Autre", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_recurrence_status', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Follow-Up Recurrence Status\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Follow-Up Recurrence Status', 1, 100, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Follow-Up Recurrence Status');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("systemic", "Systemic", "Systémique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("loco-regional", "Loco-Regional", "Loco-régional", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("loco-regional+systemic", "Loco-Regional + Systemic", "Loco-régional + Systémique", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('crchum_hpb_follow_up_recurrence_treatments', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'CRCHUM HPB : Follow-Up Recurrence treatments\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('CRCHUM HPB : Follow-Up Recurrence Treatments', 1, 100, 'clinical - annotation');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'CRCHUM HPB : Follow-Up Recurrence Treatments');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("chemo-embolization", "Chemo-Embolization", "Chimio-embolisation", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("chemotherapy", "Chemotherapy", "Chimiothérapie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("radiofrequency ablation", "Radiofrequency Ablation", "Ablation par radiofréquence", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("radiotherapy", "Radiotherapy", "Radiothérapie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("surgery", "Surgery", "", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("surgery and chemotherapy", "Surgery and Chemotherapy", "Chirurgie et chimiothérapie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("surgery and radiotherapy", "Surgery and Radiotherapy", "Chirurgie et radiothérapie", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("supportive care", "Supportive Care", "Soins de soutien", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("palliative care", "Palliative Care", "Soin palliatif", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("other", "Other", "Autre", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structures(`alias`) VALUES ('crchum_hpb_ed_clinical_followup');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('ClinicalAnnotation', 'EventDetail', 'ed_all_clinical_followups', 'disease_status', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_vital_status') , '0', '', '', '', 'disease status', ''), 
('ClinicalAnnotation', 'EventDetail', 'ed_all_clinical_followups', 'crchum_hpb_disease_status_precision', 'input',  NULL , '0', 'size=15', '', '', '', 'precision'), 
('ClinicalAnnotation', 'EventDetail', 'ed_all_clinical_followups', 'crchum_hpb_recurrence_localization', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_follow_up_recurrence_localizations') , '0', '', '', '', 'recurrence localization', ''), 
('ClinicalAnnotation', 'EventDetail', 'ed_all_clinical_followups', 'crchum_hpb_recurrence_localization_precision', 'input',  NULL , '0', 'size=15', '', '', '', 'precision'), 
('ClinicalAnnotation', 'EventDetail', 'ed_all_clinical_followups', 'recurrence_status', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_recurrence_status') , '0', '', '', '', 'recurrence status', ''), 
('ClinicalAnnotation', 'EventDetail', 'ed_all_clinical_followups', 'crchum_hpb_recurrence_treatment', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_follow_up_recurrence_treatments') , '0', '', '', '', 'recurrence treatment', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_clinical_followup'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ed_all_clinical_followups' AND `field`='disease_status' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_vital_status')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='disease status' AND `language_tag`=''), 
'1', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_clinical_followup'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ed_all_clinical_followups' AND `field`='recurrence_status' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_recurrence_status')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='recurrence status' AND `language_tag`=''), 
'1', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_clinical_followup'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ed_all_clinical_followups' AND `field`='crchum_hpb_disease_status_precision' AND `type`='input' AND `structure_value_domain`  IS NULL), 
'1', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_clinical_followup'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ed_all_clinical_followups' AND `field`='crchum_hpb_recurrence_localization' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_follow_up_recurrence_localizations')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='recurrence localization' AND `language_tag`=''), 
'1', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_clinical_followup'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ed_all_clinical_followups' AND `field`='crchum_hpb_recurrence_localization_precision' AND `type`='input'), 
'1', '5', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_clinical_followup'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ed_all_clinical_followups' AND `field`='crchum_hpb_recurrence_treatment' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='crchum_hpb_follow_up_recurrence_treatments')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='recurrence treatment' AND `language_tag`=''), 
'1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_clinical_followup'), 
(SELECT id FROM structure_fields WHERE `model`='EventDetail' AND `tablename`='ed_all_clinical_followups' AND `field`='weight' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=4' AND `default`='' AND `language_help`='' AND `language_label`='weight' AND `language_tag`=''), 
'1', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='crchum_hpb_ed_clinical_followup'), 
(SELECT id FROM structure_fields WHERE `model`='EventMaster' AND `tablename`='event_masters' AND `field`='event_summary' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='cols=40,rows=6' AND `default`='' AND `language_help`='' AND `language_label`='summary' AND `language_tag`=''), 
'1', '99', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'cols=40,rows=2', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
UPDATE structure_formats SET `flag_addgrid`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='crchum_hpb_ed_clinical_followup');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("crchum hpb - follow up", "Follow-Up (CRCHUM Hpb)", "Suivi (CRCHUM Hpb)"),
("recurrence localization", "Recurrence Localization", "Localisation de la récidive"),
("recurrence treatment", "Recurrence Treatment", "Treatment de la récidive");

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;