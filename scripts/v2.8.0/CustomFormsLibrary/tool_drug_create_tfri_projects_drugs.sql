-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.7.2 / 2.7.3
-- ------------------------------------------------------

-- user id to define

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- TFRI COEUR & CPCBN Drugs and more...
-- ................................................................................
-- List of drugs created to capture data of prostat and ovarian cancer 
-- for the central ATiM of the Canadian Prostate Cancer Biomarker Network (CPCBN)
-- and the central ATiM of the Canadian Ovarian Experimental Unified Resource 
-- Projects of the Terry Fox Research Institute (TFRI).
-- --------------------------------------------------------------------------------

DROP TABLE IF EXISTS `drugs_tmp`;
CREATE TABLE IF NOT EXISTS `drugs_tmp` (
  `generic_name` varchar(50) NOT NULL DEFAULT '',
   rxnorm_id int(11) DEFAULT NULL,
  `trade_name` varchar(50) NOT NULL DEFAULT '',
  `type` varchar(50) DEFAULT NULL,
  `to_delete` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `drugs_tmp` (`generic_name`, rxnorm_id, `trade_name`, `type`) 
VALUES
('5-ARI', NULL, '', 'hormonal'),
('Abiraterone', '1100072', '', 'hormonal'),
('bicalutamide', '83008', '', 'hormonal'),
('buserelin', '1825', '', 'hormonal'),
('cabazitaxel', '996051', '', 'chemotherapy'),
('CARBOplatin', '40048', '', 'chemotherapy'),
('CISplatin', '2555', '', 'chemotherapy'),
('cyclophosphamide', '3002', '', 'chemotherapy'),
('cyproterone', '3014', '', 'hormonal'),
('Dasatinib', '475342', '', 'chemotherapy'),
('degarelix', '475230', '', 'hormonal'),
('dexamethasone', '3264', '', 'chemotherapy'),
('DOCEtaxel', '72962', '', 'chemotherapy'),
('DOXOrubicin', '3639', '', 'chemotherapy'),
('dutasteride', '228790', '', 'hormonal'),
('enzalutamide', '1307298', '', 'hormonal'),
('Enzastaurin', NULL, '', 'chemotherapy'),
('etoposide', '4179', '', 'chemotherapy'),
('finasteride', '25025', '', 'hormonal'),
('flutamide', '4508', '', 'hormonal'),
('gefitinib', '328134', '', 'chemotherapy'),
('gemcitabine', '12574', '', 'chemotherapy'),
('goserelin', '50610', '', 'hormonal'),
('Leuprorelin', NULL, '', 'hormonal'),
('LHRH agonist', NULL, '', 'hormonal'),
('olaparib', '1597582', '', 'chemotherapy'),
('oxaliplatin', '32592', '', 'chemotherapy'),
('PACLitaxel', '56946', '', 'chemotherapy'),
('risedronic acid', '55685', '', 'other'),
('tamoxifen', '10324', '', 'chemotherapy'),
('Tamsulosin HCl', NULL, '', 'hormonal'),
('topotecan', '57308', '', 'chemotherapy'),
('vinorelbine', '39541', '', 'chemotherapy');

UPDATE drugs_tmp, drugs
SET drugs_tmp.to_delete = 1,
drugs.rxnorm_id = drugs_tmp.rxnorm_id
WHERE drugs.deleted <> 1
AND drugs.generic_name = drugs_tmp.generic_name
AND drugs.type = drugs_tmp.type;

INSERT INTO `drugs` (`generic_name`, rxnorm_id, `trade_name`, `type`, `created`, `created_by`, `modified`, `modified_by`) 
(SELECT `generic_name`, rxnorm_id, `trade_name`, `type`, NOW(), @user_id, NOW(), @user_id FROM drugs_tmp WHERE to_delete = 0);

DROP TABLE IF EXISTS `drugs_tmp`;