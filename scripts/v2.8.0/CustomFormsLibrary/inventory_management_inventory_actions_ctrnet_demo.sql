-- ------------------------------------------------------
-- ATiM CustomFormsLibrary Data Script
-- Compatible version(s) : 2.8.0
-- ------------------------------------------------------

-- user_id (To Define)

SET @user_id = (SELECT id FROM users WHERE username LIKE '%' AND id LIKE '1');

-- --------------------------------------------------------------------------------
-- CTRNet Demo Inventory Action : Quality Control
-- ................................................................................

SET @s_ids = (SELECT GROUP_CONCAT(derivative_sample_control_id ORDER BY derivative_sample_control_id ASC SEPARATOR ', ') FROM parent_to_derivative_sample_controls WHERE flag_active = 1);
SET @a_ids = (SELECT GROUP_CONCAT(id ORDER BY id ASC SEPARATOR ', ') FROM aliquot_controls WHERE flag_active = 1);
INSERT INTO `inventory_action_controls` (`id`, `type`, `category`, `apply_on`, 
`sample_control_ids`, 
`aliquot_control_ids`,
`detail_form_alias`, `detail_tablename`, `display_order`, `flag_active`, `flag_link_to_study`, `flag_link_to_storage`, `flag_test_mode`, `flag_form_builder`, `flag_active_input`, `flag_batch_action`) VALUES
(null, 'quality control', 'quality control', 'samples and aliquots not necessarily related', 
'all', 
'all',
'inventory_action_qualityctrls', 'ac_quality_ctrls', 0, 1, 0, 1, 0, 0, 1, 0);

DROP TABLE IF EXISTS `ac_quality_ctrls`;
CREATE TABLE IF NOT EXISTS `ac_quality_ctrls` (
  `inventory_action_master_id` int(11) NOT NULL,
  `test` varchar(30) DEFAULT NULL,
  `qc_test_precision` varchar(250) DEFAULT NULL,
  `tool` varchar(30) DEFAULT NULL,
  `score` varchar(30) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `conclusion` varchar(30) DEFAULT NULL,
  KEY `FK_inventory_action_masters_ac_quality_ctrls` (`inventory_action_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `ac_quality_ctrls_revs`;
CREATE TABLE IF NOT EXISTS `ac_quality_ctrls_revs` (
  `inventory_action_master_id` int(11) NOT NULL,
  `test` varchar(30) DEFAULT NULL,
  `qc_test_precision` varchar(250) DEFAULT NULL,
  `tool` varchar(30) DEFAULT NULL,
  `score` varchar(30) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `conclusion` varchar(30) DEFAULT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ac_quality_ctrls`
  ADD CONSTRAINT `FK_inventory_action_masters_ac_quality_ctrls` FOREIGN KEY (`inventory_action_master_id`) REFERENCES `inventory_action_masters` (`id`);

INSERT INTO structures(`alias`) VALUES ('inventory_action_qualityctrls');

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('quality_control_tests', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Sample Quality Control Tests\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('Sample Quality Control Tests', 1, 30, 'invetory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Sample Quality Control Tests');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("agarose gel", "Agarose Gel", "Gel d\'agarose", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("bioanalyzer", "BioAnalyzer", "BioAnalyzer", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("immunohistochemistry", "Immunohistochemistry", "Immunohistochimie", "4", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("pcr", "PCR", "PCR", "5", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("spectrophotometer", "Spectrophotometer", "Spectrophotomètre", "6", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

-- INSERT INTO structure_value_domains (domain_name, override, category, source)
-- VALUES
-- ('custom_laboratory_qc_tool', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Quality Control Tools\')');
-- INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
-- VALUES
-- ('Quality Control Toolsxxx', 1, 20, 'inventory - quality control');

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('quality_control_result_units', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Sample Quality Control Result Units\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('Sample Quality Control Result Units', 1, 30, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Sample Quality Control Result Units');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("260/230", "260/230", "260/230", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("260/268", "260/268", "260/268", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("260/280", "260/280", "260/280", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("28/18", "28/18", "28/18", "4", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("RIN", "RIN", "RIN", "5", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('quality_control_conclusions', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Sample Quality Control Conclusions\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('Sample Quality Control Conclusions', 1, 30, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Sample Quality Control Conclusions');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("degraded", "Degraded", "Dégradé", "0", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("partially degraded", "Partially Degraded", "Partiellement dégradé", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("poor", "Poor", "Pauvre", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("acceptable", "Acceptable", "Acceptable", "4", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("good", "Good", "Bon", "6", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("very good", "Very good", "Très bon", "7", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("out of range", "Out of range", "En dehors de l\'échelle", "8", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'test', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='quality_control_tests') , '0', '', '', '', 'qc type', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'tool', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_qc_tool') , '0', '', '', '', 'qc tool', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'score', 'input',  NULL , '0', 'size=5', '', '', 'qc score', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'unit', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='quality_control_result_units') , '0', '', '', '', '', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_quality_ctrls', 'conclusion', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='quality_control_conclusions') , '0', '', '', '', 'qc conclusion', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='inventory_action_qualityctrls'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='test' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='quality_control_tests')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='qc type' AND `language_tag`=''), 
'0', '2', 'result', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='inventory_action_qualityctrls'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='tool' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_qc_tool')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='qc tool' AND `language_tag`=''), 
'1', '31', 'result', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='inventory_action_qualityctrls'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='score' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='qc score' AND `language_tag`=''), 
'1', '41', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='inventory_action_qualityctrls'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='quality_control_result_units')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''), 
'1', '42', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='inventory_action_qualityctrls'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='conclusion' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='quality_control_conclusions')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='qc conclusion' AND `language_tag`=''), 
'1', '43', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0');

INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES
    ((SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='test' ), 'notBlank', NULL),
    ((SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='score' ), 'notBlank', NULL),
    ((SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_quality_ctrls' AND `field`='unit' ), 'notBlank', NULL);
    
INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('quality control', 'Quality Control', 'Contrôle de qualité'),
("conclusion", "Conclusion", "Conclusion"),
("qc score", "Score", "Score"),
("qc tool", "Tool", "Appareil de Mesure"),
("qc type", "Type", "Type"),
("qc type precision", "Precision", "Précision"),
("quality control", "Quality Control", "Contrôle de qualité"),
("type", "Type", "Type");

-- --------------------------------------------------------------------------------
-- CTRNet Demo Inventory Action : Tissue Specimen Review
-- ................................................................................

INSERT INTO `inventory_action_controls` (`id`, `type`, `category`, `apply_on`, 
`sample_control_ids`, 
`aliquot_control_ids`,
`detail_form_alias`, `detail_tablename`, `display_order`, `flag_active`, `flag_link_to_study`, `flag_link_to_storage`, `flag_test_mode`, `flag_form_builder`, `flag_active_input`, `flag_batch_action`) VALUES
(null, 'tissue review', 'specimen review', 'samples and all related aliquots', 
(SELECT id FROM sample_controls WHERE sample_type = 'tissue' AND flag_active = 1), 
'', 
'ac_spr_tissue_reviews', 'ac_spr_tissue_reviews', 0, 1, 1, 0, 0, 0, 1, 0);

DROP TABLE IF EXISTS `ac_spr_tissue_reviews`;
CREATE TABLE IF NOT EXISTS `ac_spr_tissue_reviews` (
  `inventory_action_master_id` int(11) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `pathologist` varchar(50) DEFAULT NULL,
  `length` decimal(5,1) DEFAULT NULL,
  `width` decimal(5,1) DEFAULT NULL,
  `invasive_percentage` decimal(5,1) DEFAULT NULL,
  `in_situ_percentage` decimal(5,1) DEFAULT NULL,
  `normal_percentage` decimal(5,1) DEFAULT NULL,
  `stroma_percentage` decimal(5,1) DEFAULT NULL,
  `necrosis_inv_percentage` decimal(5,1) DEFAULT NULL,
  `necrosis_is_percentage` decimal(5,1) DEFAULT NULL,
  `tumour_grade_score_tubules` decimal(5,1) DEFAULT NULL,
  `quality_score` int(4) DEFAULT NULL,
  `tumour_grade_score_nuclear` decimal(5,1) DEFAULT NULL,
  `tumour_grade_score_mitosis` decimal(5,1) DEFAULT NULL,
  `tumour_grade_score_total` decimal(5,1) DEFAULT NULL,
  `inflammation` int(4) DEFAULT NULL,
  `review_status` varchar(20) DEFAULT NULL,
  KEY `FK_ac_spr_tissue_reviews1` (`inventory_action_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ac_spr_tissue_reviews`
  ADD CONSTRAINT `FK_ac_spr_tissue_reviews2` FOREIGN KEY (`inventory_action_master_id`) REFERENCES `inventory_action_masters` (`id`);
  
DROP TABLE IF EXISTS `ac_spr_tissue_reviews_revs`;
CREATE TABLE IF NOT EXISTS `ac_spr_tissue_reviews_revs` (
  `inventory_action_master_id` int(11) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `pathologist` varchar(50) DEFAULT NULL,
  `length` decimal(5,1) DEFAULT NULL,
  `width` decimal(5,1) DEFAULT NULL,
  `invasive_percentage` decimal(5,1) DEFAULT NULL,
  `in_situ_percentage` decimal(5,1) DEFAULT NULL,
  `normal_percentage` decimal(5,1) DEFAULT NULL,
  `stroma_percentage` decimal(5,1) DEFAULT NULL,
  `necrosis_inv_percentage` decimal(5,1) DEFAULT NULL,
  `necrosis_is_percentage` decimal(5,1) DEFAULT NULL,
  `tumour_grade_score_tubules` decimal(5,1) DEFAULT NULL,
  `quality_score` int(4) DEFAULT NULL,
  `tumour_grade_score_nuclear` decimal(5,1) DEFAULT NULL,
  `tumour_grade_score_mitosis` decimal(5,1) DEFAULT NULL,
  `tumour_grade_score_total` decimal(5,1) DEFAULT NULL,
  `inflammation` int(4) DEFAULT NULL,
  `review_status` varchar(20) DEFAULT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO structures(`alias`) VALUES ('ac_spr_tissue_reviews');

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('tissue_review_types', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Tissue Review Types\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('Tissue Review Types', 1, 100, 'invetory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Tissue Review Types');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("tumor", "Tumor", "Tumeur", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("normal", "Normal", "Normal", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("unknown", "Unknown", "Inconnu", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('tissue_review_status', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Tissue Review Status\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('Tissue Review Status', 1, 20, 'Inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Tissue Review Status');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("in progress", "In Progress", "En cours", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("done", "Done", "Finalisé", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("unknown", "Unknown", "Inconnu", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='tissue_review_types') , '0', '', '', '', 'type', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'pathologist', 'input',  NULL , '0', 'size=30', '', '', 'pathologist', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'length', 'float',  NULL , '0', 'size=5', '', '', 'length', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'width', 'float',  NULL , '0', 'size=5', '', '', 'width', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'invasive_percentage', 'float',  NULL , '0', 'size=5', '', '', 'invasive percentage', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'in_situ_percentage', 'float',  NULL , '0', 'size=5', '', '', 'in situ percentage', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'normal_percentage', 'float',  NULL , '0', 'size=5', '', '', 'normal percentage', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'stroma_percentage', 'float',  NULL , '0', 'size=5', '', '', 'stroma percentage', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'necrosis_inv_percentage', 'float',  NULL , '0', 'size=5', '', '', 'necrosis inv percentage', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'necrosis_is_percentage', 'float',  NULL , '0', 'size=5', '', '', 'necrosis is percentage', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'tumour_grade_score_tubules', 'float',  NULL , '0', 'size=5', '', '', 'tumour grade score tubules', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'quality_score', 'integer',  NULL , '0', 'size=5', '', '', 'quality review score', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'tumour_grade_score_nuclear', 'float',  NULL , '0', 'size=5', '', '', 'tumour grade score nuclear', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'tumour_grade_score_mitosis', 'float',  NULL , '0', 'size=5', '', '', 'tumour grade score mitosis', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'tumour_grade_score_total', 'float',  NULL , '0', 'size=5', '', '', 'tumour grade score total', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_spr_tissue_reviews', 'review_status', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='tissue_review_status') , '0', '', '', '', 'review status', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='tissue_review_types')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='type' AND `language_tag`=''), 
'1', '23', 'reviewing', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='pathologist' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='' AND `language_label`='pathologist' AND `language_tag`=''), 
'0', '104', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='length' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='length' AND `language_tag`=''), 
'1', '24', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='width' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='width' AND `language_tag`=''), 
'1', '25', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='invasive_percentage' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='invasive percentage' AND `language_tag`=''), 
'1', '26', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='in_situ_percentage' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='in situ percentage' AND `language_tag`=''), 
'1', '27', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='normal_percentage' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='normal percentage' AND `language_tag`=''), 
'1', '27', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='stroma_percentage' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='stroma percentage' AND `language_tag`=''), 
'1', '28', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='necrosis_inv_percentage' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='necrosis inv percentage' AND `language_tag`=''), 
'1', '29', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='necrosis_is_percentage' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='necrosis is percentage' AND `language_tag`=''), 
'1', '30', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='tumour_grade_score_tubules' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='tumour grade score tubules' AND `language_tag`=''), 
'2', '32', 'score', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='quality_score' AND `type`='integer' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='quality review score' AND `language_tag`=''), 
'2', '32', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='tumour_grade_score_nuclear' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='tumour grade score nuclear' AND `language_tag`=''), 
'2', '33', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='tumour_grade_score_mitosis' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='tumour grade score mitosis' AND `language_tag`=''), 
'2', '34', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='tumour_grade_score_total' AND `type`='float' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='tumour grade score total' AND `language_tag`=''), 
'2', '35', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='ac_spr_tissue_reviews'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='review_status' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='tissue_review_status')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='review status' AND `language_tag`=''), 
'0', '103', 'details', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0');

INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES
    ((SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_spr_tissue_reviews' AND `field`='review_status' ), 'notBlank', NULL);
    
INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('type', 'Type', 'Type'),
('tissue review', 'Tissue Review', 'Révision tissu'),
('reviewing', 'Reviewing', 'Révision');

-- --------------------------------------------------------------------------------
-- CTRNet Demo Inventory Action : Laboratory 
-- --------------------------------------------------------------------------------

SET @s_ids = (SELECT GROUP_CONCAT(derivative_sample_control_id ORDER BY derivative_sample_control_id ASC SEPARATOR ', ') FROM parent_to_derivative_sample_controls WHERE flag_active = 1);
SET @a_ids = (SELECT GROUP_CONCAT(id ORDER BY id ASC SEPARATOR ', ') FROM aliquot_controls WHERE flag_active = 1);
INSERT INTO `inventory_action_controls` (`id`, `type`, `category`, `apply_on`, 
`sample_control_ids`,
`aliquot_control_ids`, 
`detail_form_alias`, `detail_tablename`, `display_order`, `flag_active`, `flag_link_to_study`, `flag_link_to_storage`, `flag_test_mode`, `flag_form_builder`, `flag_active_input`, `flag_batch_action`) VALUES
(null, 'laboratory activity', 'aliquot_internal_uses', 'aliquots related to samples', 
'all', 
'',
'inventory_action_lab_activities', 'ac_lab_activities', 0, 1, 1, 1, 0, 0, 1, 1);

DROP TABLE IF EXISTS `ac_lab_activities`;
CREATE TABLE IF NOT EXISTS `ac_lab_activities` (
  `inventory_action_master_id` int(11) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `duration` decimal(5,1) DEFAULT NULL,
  `duration_unit` varchar(100) DEFAULT NULL,
  `use_details`  text,
  KEY `FK_ac_lab_activities1` (`inventory_action_master_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `ac_lab_activities`
  ADD CONSTRAINT `FK_ac_lab_activities2` FOREIGN KEY (`inventory_action_master_id`) REFERENCES `inventory_action_masters` (`id`);
  
DROP TABLE IF EXISTS `ac_lab_activities_revs`;
CREATE TABLE IF NOT EXISTS `ac_lab_activities_revs` (
  `inventory_action_master_id` int(11) NOT NULL,
  `type` varchar(100) DEFAULT NULL,
  `duration` decimal(5,1) DEFAULT NULL,
  `duration_unit` varchar(100) DEFAULT NULL,
  `use_details`  text,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO structures(`alias`) VALUES ('inventory_action_lab_activities');

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('inventory_action_lab_activity_duration_unit', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Lab Activity Duration Unit\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('Lab Activity Duration Unit', 1, 20, 'Inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Lab Activity Duration Unit');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("mn", "mn", "mn", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("hr", "hr", "h", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("day", "day", "Jr", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('inventory_action_lab_activity_types', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Lab Activity Types\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('Lab Activity Types', 1, 100, 'Inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Lab Activity Types');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("sequencing", "Sequencing", "Séquencage", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("lab research", "Lab Research", "Recherche en laboratoire", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("room temperature", "Room Temperature", "Températur pièce", "2", "1", @control_id, NOW(), NOW(), @user_id, @user_id), 
("intra institution transfer", "Intra Institution Transfer", "Transfert intra-institution", "3", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'InventoryActionDetail', 'ac_lab_activities', 'type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_lab_activity_types') , '0', '', '', '', 'type', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_lab_activities', 'duration', 'integer_positive',  NULL , '0', 'size=6', '', '', 'duration', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_lab_activities', 'duration_unit', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_lab_activity_duration_unit') , '0', '', '', '', '', ''), 
('InventoryManagement', 'InventoryActionDetail', 'ac_lab_activities', 'use_details', 'textarea',  NULL , '0', '', '', '', 'details', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='inventory_action_lab_activities'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_lab_activities' AND `field`='type'), 
'0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='inventory_action_lab_activities'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_lab_activities' AND `field`='duration' AND `type`='integer_positive'), 
'1', '8', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='inventory_action_lab_activities'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_lab_activities' AND `field`='duration_unit' AND `type`='select'), 
'1', '9', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='inventory_action_lab_activities'), 
(SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_lab_activities' AND `field`='use_details' AND `type`='textarea' ), 
'1', '3', 'details', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0');

INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES
    ((SELECT id FROM structure_fields WHERE `model`='InventoryActionDetail' AND `tablename`='ac_lab_activities' AND `field`='type'), 'notBlank', NULL);

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('laboratory activity', 'Laboratory Activity', 'Activité de laboratoire');

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------

UPDATE versions SET permissions_regenerated = 0;