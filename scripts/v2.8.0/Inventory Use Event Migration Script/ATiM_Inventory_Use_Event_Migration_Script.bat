CLS
@ECHO off
COLOR F
ECHO ==================================================================
ECHO ==     ATiM Inventory Use/Event Migration Script                ==
ECHO ==================================================================
ECHO -

SET databaseUserName=root
SET host=localhost
SET password=
SET port=3306

set /p databaseName=Database Name (default 'atimdev28z' if not completed)?
if "%databaseName%" == "" (
	SET databaseName=atimdev28z
)

set /p databaseUserName=Database databaseUserName (default 'root' if not completed)?
if "%databaseUserName%" == "" (
	SET databaseUserName=root
)

set /p password=Database password?
if NOT "%password%" == "" (
	SET password=-p %password%
)

set /p host=Database Host (localhost)?

set /p port=Database Port# (3306)?

@echo on
php InventoryUseEventMigration.php -u %databaseUserName% %password% -host %host% -d %databaseName% -port %port%

@echo off
PAUSE
