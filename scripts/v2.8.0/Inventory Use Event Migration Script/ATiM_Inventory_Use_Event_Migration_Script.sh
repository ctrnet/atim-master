WHITE='\033[1;37m'
CYAN='\033[1;36m'
YELLOW='\033[1;33m'
RED='\033[0;31m'

clear
echo -e "${WHITE}"

printf "================================================================\n"
printf "==       ATiM Inventory Use/Event Migration Script            ==\n"
printf "================================================================\n\n"

echo -e "${WHITE}"
printf "Database Name(default 'atimdev28z' if not completed)? "
read databaseName
if [ "$databaseName" = "" ];
then
	databaseName="atimdev28z"
fi

printf "Database username (default 'root' if not completed)? "
read databaseUserName
if [ "$databaseUserName" = "" ];
then
	databaseUserName="root"
fi

printf "Database Password? "
read password
if [ "$password" = "" ];
then
	password=""
else
	password="-p $password"
fi

printf "Database Host (localhost)? "
read host
if [ "$host" = "" ];
then
	host="localhost"
fi

printf "Database Port# (3306)? "
read port
if [ "$port" = "" ];
then
	port="3306"
fi


echo "php InventoryUseEventMigration.php -u ${databaseUserName} ${password} -host ${host} -d ${databaseName} -port ${port}"
php InventoryUseEventMigration.php -u $databaseUserName $password -host $host -d $databaseName -port $port
read -n 1 -p "Press any key to continue . . ."
echo""

