<?php

include_once "./libs/ArgvParser.php";

class InventoryUseEventMigration
{
    private $allTables;

    private $db;
    private $result = [];

    private $aliquotControls = [];

    private $tableName;
    private $attrName;
    private $tables = [
        'inventory_action_masters' => true,
        'inventory_action_controls' => true,
        'view_storage_masters' => true,
        'view_samples' => true,
        'view_collections' => true,
        'view_aliquot_uses' => true,
        'view_aliquots' => true,
        'view_storage_masters' => true,
        'collections' => true,
        'sample_masters' => true,
        'aliquot_masters' => true,
        'participants' => true,
    ];

    private $AliquotInternalUseControlIds = [];

    private $version;

    private $config = [
        'mysql_host' => 'localhost',
        'mysql_user' => 'root',
        'mysql_pwd' => '',
        'mysql_port' => 3306,
    ];

    public function __construct()
    {
        $this->parseArgs();
        echo "\n\nDatabase Configuration: \n" . json_encode($this->config, JSON_PRETTY_PRINT);
        $this->getConnection();
    }

    public function startMigration()
    {
        $this->getAllTables();

        $this->version = $this->getVersion();
        if ($this->version < "2.8") {
            $this->result['Errors'][] = "Your ATiM version is not compatible with Inventory action and you need to upgrade it. Please contact ATiM support team.";
            return;
        }

//        $this->createInventoryActionMasterTable();
//        $this->createInventoryActionControlTable();

        $actionsTables = [
            'quality_ctrls',
            'aliquot_internal_uses',
            'specimen_review_controls',
        ];
        foreach ($actionsTables as $this->tableName) {
            $this->attrName = $this->camelize($this->tableName);
            $this->convertOldToInventoryActions();
        }

    }

    private function getConnection(){
        $this->db = @new mysqli($this->config['mysql_host'], $this->config['mysql_user'], $this->config['mysql_pwd']);

        if ($this->db->connect_errno) {
            die('Connect Error: ' . $this->db->connect_errno.": ".$this->db->connect_error);
        }
        if(!$this->db->set_charset("utf8")){
            die("We failed");
        }

        if($this->config['mysql_database'] != NULL){
            if(!$this->db->select_db($this->config['mysql_database'])) {
                die($this->db->error);
            }
        }
    }

    private function parseArgs()
    {
        $argvParser = new ArgvParser();
        $args = array_change_key_case($argvParser->parseConfigs($argv), CASE_LOWER);

        if (!empty($args['h']) || !empty($args['help'])) {
            print_r("php .\InventoryUseEventMigration.php -u username(root) -p password(none) -d database -port portNumber(3306) -host hostname(localhost)");
            exit(-1);
        } else {
            if (!empty($args['u']) || !empty($args['user'])) {
                $this->config['mysql_user'] = empty($args['u']) ? $args['user'] : $args['u'];
            }

            $this->config['mysql_host'] = empty($args['host']) ? $this->config['mysql_host'] : $args['host'];
            $this->config['mysql_port'] = empty($args['port']) ? $this->config['mysql_port'] : $args['port'];

            if (!empty($args['p']) || !empty($args['password'])) {
                $this->config['mysql_pwd'] = empty($args['p']) ? $args['password'] : $args['p'];
            }

            if (!empty($args['d']) || !empty($args['database'])) {
                $this->config['mysql_database'] = empty($args['d']) ? $args['database'] : $args['d'];
            } else {
                $this->config['error'][] = "Database name is required, you can pass it by -d option";
            }
        }
        if (!empty($this->config['error'])) {
            echo(implode('\n\r', $this->config['error']));
            exit(-1);
        }
    }

    private function getVersion()
    {
        $version = "";
        $query = "SELECT MAX(version_number) as version_number FROM `versions` AS `Version` LIMIT 1";
        $result = $this->db->query($query) or die($this->db->error);
        $row = $result->fetch_assoc();
        if (empty($row['version_number'])) {
            $version = "0.0.0";
        } else {
            $version = $row['version_number'];
        }
        return $version;
    }

    public function saveToFile()
    {
        $directory = "migrationScripts";
        if (!file_exists($directory)) {
            mkdir($directory, 0755);
        }
        $filename = "InventoryActionMigrationScripts.sql";

        //Windows Script
        $fileContent = "";
        $os = strtolower(substr(PHP_OS, 0, 3));
        $bathFileContent = "";
        $batchFileName = $directory . "/scripts.bat";
        $bathLinuxFileContent = "";
        $time = date('Y-m-d-H-m-s');

        $nl = "\r\n";
        $bathFileContent .= "CLS$nl";
        $bathFileContent .= "@ECHO off$nl";

        $bathFileContent .= "COLOR F$nl";
        $bathFileContent .= "ECHO All your data will be erased and unrecoverable.$nl";
        $bathFileContent .= "SET /p confirm= \"Are you sure to run it? if yes type YES or anything else for cancel. YES/NO \"$nl$nl";

        $bathFileContent .= "IF NOT \"%confirm%\" == \"YES\" ($nl";
        $bathFileContent .= "EXIT$nl";
        $bathFileContent .= ")$nl$nl";

        $bathFileContent .= "set /p databaseName=Database Name (default '" . $this->config['mysql_database'] . "' if not completed)?$nl";
        $bathFileContent .= "if \"%databaseName%\" == \"\" ($nl";
        $bathFileContent .= "SET databaseName=" . $this->config['mysql_database'] . "$nl";
        $bathFileContent .= ")$nl";
        $bathFileContent .= "set /p databaseUserName=Database databaseUserName (root)?$nl";
        $bathFileContent .= "if \"%databaseUserName%\" == \"\" ($nl";
        $bathFileContent .= "SET databaseUserName=root$nl";
        $bathFileContent .= ")$nl";
        $bathFileContent .= "set /p password=Database password?$nl";
        $bathFileContent .= "if NOT \"%password%\" == \"\" ($nl";
        $bathFileContent .= "SET password=-p%password%$nl";
        $bathFileContent .= ")$nl$nl";
        $bathFileContent .= "@ECHO on$nl";
        $bathFileContent .= "mysqldump -u %databaseUserName% %password% %databaseName% > %databaseName%_backup_" . $time . ".sql $nl";
        $bathFileContent .= "mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < $filename $nl";
        $bathFileContent .= "PAUSE $nl";

        file_put_contents($batchFileName, $bathFileContent);

        //Linux script
        $batchLinuxFileName = $directory . "/scripts.sh";
        $nl = "\n";
        $bathLinuxFileContent .= "WHITE='\\033[1;37m'\n";
        $bathLinuxFileContent .= "CYAN='\\033[1;36m'\n";
        $bathLinuxFileContent .= "YELLOW='\\033[1;33m'\n";
        $bathLinuxFileContent .= "RED='\\033[0;31m'\n";
        $bathLinuxFileContent .= "clear\n";
        $bathLinuxFileContent .= "echo -e \"\${YELLOW}\"\n\n";
        $bathLinuxFileContent .= "COLUMNS=\$(tput cols)\n";
        $bathLinuxFileContent .= "printf \"%*s\\n\\n\\n\" $(((\${#title}+\$COLUMNS)/2)) \"\$title\"\n\n";
        $bathLinuxFileContent .= "echo -e \"\${RED}\"\n";
        $bathLinuxFileContent .= "printf \"All your data will be erased and unrecoverable.\"\n";
        $bathLinuxFileContent .= "echo -e \"\${WHITE}\"\n";
        $bathLinuxFileContent .= "printf \"Are you sure to run it? if yes type YES or anything else for cancel. YES/NO \"\n\n";
        $bathLinuxFileContent .= "read confirm\n\n";
        $bathLinuxFileContent .= "if [ \"\$confirm\" != \"YES\" ];\n";
        $bathLinuxFileContent .= "then\n";
        $bathLinuxFileContent .= "\texit\n";
        $bathLinuxFileContent .= "fi\n\n";
        $bathLinuxFileContent .= "printf \"Database Name ({$this->config['mysql_database']})? \"\n";
//        $bathLinuxFileContent .= "printf \"Database Name? \"\n";
        $bathLinuxFileContent .= "read databaseName\n\n";
        $bathLinuxFileContent .= "if [ \"\$databaseName\" = \"\" ];\n";
        $bathLinuxFileContent .= "then\n";
        $bathLinuxFileContent .= "\tdatabaseName=\"{$this->config['mysql_database']}\"\n";
        $bathLinuxFileContent .= "fi\n\n";
        $bathLinuxFileContent .= "printf \"Database username (root)? \"\n";
        $bathLinuxFileContent .= "read databaseUserName\n";
        $bathLinuxFileContent .= "if [ \"\$databaseUserName\" = \"\" ];\n";
        $bathLinuxFileContent .= "then\n";
        $bathLinuxFileContent .= "\tdatabaseUserName=\"root\"\n";
        $bathLinuxFileContent .= "fi\n\n";
        $bathLinuxFileContent .= "printf \"Database Password? \"\n";
        $bathLinuxFileContent .= "read password\n";
        $bathLinuxFileContent .= "if [ \"\$password\" = \"\" ];\n";
        $bathLinuxFileContent .= "then\n";
        $bathLinuxFileContent .= "\tpassword=\"\"\n";
        $bathLinuxFileContent .= "else\n";
        $bathLinuxFileContent .= "\tpassword=\"-p\$password\"\n";
        $bathLinuxFileContent .= "fi\n\n\n";

        $bathLinuxFileContent .= "echo -e \"\${WHITE}\"\n";
        $bathLinuxFileContent .= "printf \"Do you want to create Backup? if yes type YES or anything else for cancel. YES/NO \"\n";
        $bathLinuxFileContent .= "read createBackup\n\n";
        $bathLinuxFileContent .= "if [ \"\$createBackup\" = \"YES\" ]\n";
        $bathLinuxFileContent .= "then\n";
        $bathLinuxFileContent .= "echo -e \"\\n\${CYAN}\\t\\tCreating the database backup\"\n";
        $bathLinuxFileContent .= "echo -e \"\${WHITE}mysqldump -u \$databaseUserName \$password \$databaseName > backup" . $time . ".sql\"\n";
        $bathLinuxFileContent .= "mysqldump -u \$databaseUserName \$password \$databaseName > backup" . $time . ".sql\n\n";
        $bathLinuxFileContent .= "fi\n";

        $bathLinuxFileContent .= "echo -e \"mysql -u \$databaseUserName \$password \$databaseName --default-character-set=utf8 < $filename \"\n";
        $bathLinuxFileContent .= "mysql -u \$databaseUserName \$password \$databaseName --default-character-set=utf8 < $filename \n";

        $bathLinuxFileContent .= "read -n 1 -p \"Press any key to continue . . .\"\n";
        $bathLinuxFileContent .= "echo\"\"\n";
        $bathLinuxFileContent .= "\n";

        file_put_contents($batchLinuxFileName, $bathLinuxFileContent);
        chmod($batchLinuxFileName, 0777);

        $lineComment = "-- ----------------------------------------------------------------------------------------$nl";

//        array_walk_recursive($this->result, function(&$value) { $value = str_replace("'", "\'", $value); });

        $results = $this->result;
        foreach ($results as $table => $queries) {
            if ($table == 'Errors') {
                $fileContent .= $lineComment . $lineComment . "-- Errors: " . $nl;
                foreach ($queries as $j => $query) {
                    $fileContent .= "-- " . ((int)$j + 1) . ") " . $query . $nl;
                }
            } elseif ($table == 'finalizingScripts') {
                $fileContent .= $lineComment . "-- Finalizing Scripts: " . $nl . $lineComment;
                foreach ($queries as $j => $query) {
                    $fileContent .= implode($nl, $query) . $nl;
                }
            } else {
                if ($table == "aliquot_internal_uses" || $table == "quality_ctrls") {
                    $fileContent .= $lineComment . $lineComment . "-- Table name: " . $table . $nl;
                    foreach ($queries as $description => $scripts) {
                        $fileContent .= $lineComment . "-- $description $nl";
                        foreach ($scripts as $key => $script) {
                            $fileContent .= $script . $nl;
                        }
                    }
                } else {
                    if ($table == "specimen_review_controls") {
                        $fileContent .= "-- Table name: " . $table . $nl;

                        foreach ($queries as $newTableName => $allScripts) {
                            $fileContent .= $lineComment . "-- New table name:  $newTableName  $nl";

                            foreach ($allScripts as $description => $scripts) {
                                $fileContent .= $lineComment . "--  $description $nl";

                                foreach ($scripts as $key => $script) {
                                    $fileContent .= $script . $nl;
                                }
                            }
                        }
                    }
                }
            }
        }
        $filename = "$directory/$filename";

        file_put_contents($filename, $fileContent);
        echo sprintf("\n\nThe migration scripts are created and ready to run: \n\n%-30s$filename\n%-30s$batchFileName\n%-30s$batchLinuxFileName\n\n", "SQL Script:", "Windows batch file:", "Linux shell file:");
        return ['sql' => $filename, 'batch' => $batchFileName, 'batchLinux' => $batchLinuxFileName];

    }

    public function toString()
    {

        $scriptFiles = $this->saveToFile();
        return json_encode(['scripts' => $this->result, "files" => $scriptFiles]);
    }

    private function getAllTables()
    {
        $result = $this->db->query("SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_SCHEMA=' {$this->config['mysql_database']} ' ORDER BY TABLE_NAME") or die($this->db->error);
        $this->allTables = [];
        while ($row = $result->fetch_assoc()) {
            $this->allTables[] = $row['TABLE_NAME'];
        }
    }

    private function createInventoryActionMasterTable()
    {
        $this->tableName = 'inventory_action_masters';
        $query = "
          CREATE TABLE `inventory_action_masters` (
          `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `inventory_action_control_id` int(11) NOT NULL,
          `sample_master_id` int(11) NOT NULL,
          `aliquot_master_id` int(11) NULL,
          `initial_specimen_sample_id` int(11) NOT NULL,
          `title` varchar(50) DEFAULT NULL,
          `study_summary_id` int(11) NULL,
          `person` varchar(50) COLLATE 'latin1_swedish_ci' NULL,
          `date` datetime DEFAULT NULL,
          `date_accuracy` char(1) NOT NULL DEFAULT '',
          `used_volume` decimal(10,5) DEFAULT 'NULL',
          `notes` text,
          `created` datetime NULL,
          `created_by` int(11) NOT NULL,
          `modified` datetime NULL,
          `modified_by` int(11) NOT NULL,
          `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
          FOREIGN KEY (`inventory_action_control_id`) REFERENCES `inventory_action_controls` (`id`) ON DELETE NO ACTION,
          FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`) ON DELETE NO ACTION,
          FOREIGN KEY (`aliquot_master_id`) REFERENCES `aliquot_masters` (`id`) ON DELETE NO ACTION,
          FOREIGN KEY (`initial_specimen_sample_id`) REFERENCES `sample_masters` (`id`) ON DELETE NO ACTION,
          FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION,
          FOREIGN KEY (`study_summary_id`) REFERENCES `study_summaries` (`id`) ON DELETE NO ACTION,
          FOREIGN KEY (`modified_by`) REFERENCES `users` (`id`) ON DELETE NO ACTION,
          INDEX `title` (`title`),
          INDEX `person` (`person`)
        ) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';
        ";
        $this->result["Create inventory_action_master table"] = $query;
    }

    private function createInventoryActionControlTable()
    {
        $this->tableName = 'inventory_action_controls';
        $query = "
            CREATE TABLE `inventory_action_controls` (
              `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `type` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL COMMENT 'The name shown for the user',
              `category` varchar(100) COLLATE 'latin1_swedish_ci' NOT NULL COMMENT 'The way for distinguish the different types of actions like Quality Control, Pathology report and Aliquot Internal Use or etc.',
              `apply_on` set('aliquots','aliquots related to samples','samples','samples and all related aliquots','samples and some related aliquots','samples and aliquots not necessarily related') NOT NULL COMMENT 'Define if test applicable on the samples or aliquots',
              `sample_control_ids` longtext NULL,
              `aliquot_control_ids` longtext NULL,
              `s_ids` longtext NULL COMMENT 'This is an auto-generate column save all sample control ids related to each inventory action control',
              `a_ids` longtext NULL COMMENT 'This is an auto-generate column save all aliquot control ids related to each inventory action control',
              `detail_form_alias` varchar(255) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '',
              `detail_tablename` varchar(255) COLLATE 'latin2_bin' NOT NULL,
              `display_order` int(11) NOT NULL DEFAULT '0',
              `databrowser_label` varchar(50) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT '',
              `flag_active` tinyint(1) NOT NULL DEFAULT '1',
              `flag_link_to_study` tinyint(1) NOT NULL DEFAULT '0',
              `flag_link_to_storage` tinyint(1) NOT NULL DEFAULT '0',
              `flag_test_mode` tinyint(1) NOT NULL DEFAULT '0',
              `flag_form_builder` tinyint(1) NOT NULL DEFAULT '0',
              `flag_active_input` tinyint(1) NOT NULL DEFAULT '1'
            ) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';
        ";
        $this->result["Create inventory_action_control table"] = $query;
    }

    private function camelize($input, $separator = '_')
    {
        return str_replace($separator, '', ucwords($input, $separator));
    }

    private function init($values = [])
    {

        $this->{$this->attrName}['masterFields'] = [
            'id' => ['type' => 'int(11)', 'null' => 'yes', 'alias' => 'old_id'],
            'inventory_action_control_id' => ['type' => 'int(11)', 'null' => 'no'],
            'aliquot_master_id' => ['type' => 'int(11)', 'null' => 'yes'],
            'sample_master_id' => ['type' => 'int(11)', 'null' => 'no'],
            'initial_specimen_sample_id' => ['type' => 'int(11)', 'null' => 'no'],
            'study_summary_id' => ['type' => 'int(11)', 'null' => 'yes'],
//            'type' => array('type' => 'varchar(100)', 'null' => 'yes'),
            'used_volume' => ['type' => 'decimal(10, 5)', 'null' => 'yes', 'default' => null],
            'created' => ['type' => 'datetime', 'null' => 'yes'],
            'created_by' => ['type' => 'int(10) unsigned', 'null' => 'no'],
            'modified' => ['type' => 'datetime', 'null' => 'yes'],
            'modified_by' => ['type' => 'int(10) unsigned', 'null' => 'no'],
            'deleted' => ['type' => 'tinyint(3) unsigned', 'null' => 'no', 'default' => 0],
        ];

        if (in_array($this->tableName, ['quality_ctrls', 'aliquot_internal_uses']) !== false) {
            $this->{$this->attrName}['masterFieldDetailAlias'] = [
//                'type' => ['type' => 'varchar(100)', 'null' => 'YES', 'alias' => 'type'],
            ];

//            $this->{$this->attrName}['masterFields'] = array_merge($this->{$this->attrName}['masterFields'], ['type' => ['type' => 'varchar(100)', 'null' => 'yes']]);

            $this->{$this->attrName}['actionControl']['flag_active'] = '1';
            $this->{$this->attrName}['actionControl']['flag_link_to_storage'] = '0';
            $this->{$this->attrName}['actionControl']['flag_link_to_study'] = '1';
            $this->{$this->attrName}['actionControl']['flag_test_mode'] = '0';
            $this->{$this->attrName}['actionControl']['flag_form_builder'] = '0';
            $this->{$this->attrName}['actionControl']['flag_active_input'] = '1';
            $this->{$this->attrName}['actionControl']['detail_tablename'] = 'ac_' . $this->tableName;

            $this->{$this->attrName}['fields'] = [];
            $this->{$this->attrName}['detailFields'] = [];
            $this->{$this->attrName}['ignoredFields'] = [];
            if ($this->tableName == 'quality_ctrls') {
                $this->{$this->attrName}['masterFieldDetailAlias'] = [];
//                $this->{$this->attrName}['masterFieldDetailAlias']['run_by'] = [
//                    'type' => 'varchar(50)',
//                    'null' => 'yes',
//                    'alias' => 'person',
//                ];
//                $this->{$this->attrName}['masterFieldDetailAlias']['run_id'] = [
//                    'type' => 'varchar(50)',
//                    'null' => 'yes',
//                    'alias' => 'title',
//                ];

                $this->{$this->attrName}['controlFields'] = [];
                $this->{$this->attrName}['masterFields'] = array_merge($this->{$this->attrName}['masterFields'], [
                    'run_id' => array('type' => 'varchar(100)', 'null' => 'yes', 'alias' => 'title'),
                    'run_by' => array('type' => 'varchar(50)', 'null' => 'yes', 'alias' => 'person'),
                    'notes' => ['type' => 'text', 'null' => 'yes', 'alias' => 'notes'],
                    'date' => ['type' => 'date', 'null' => 'yes', 'alias' => 'date'],
                    'date_accuracy' => [
                        'type' => 'char(1)',
                        'null' => 'no',
                        'default' => '',
                        'alias' => 'date_accuracy',
                    ],
                ]);
                $this->{$this->attrName}['actionControl']['type'] = 'quality control';
                $this->{$this->attrName}['actionControl']['category'] = 'quality control';
                $this->{$this->attrName}['actionControl']['apply_on'] = 'samples and aliquots not necessarily related';
                $this->{$this->attrName}['actionControl']['sample_control_ids'] = 'all';
                $this->{$this->attrName}['actionControl']['aliquot_control_ids'] = 'all';
                $this->{$this->attrName}['actionControl']['detail_form_alias'] = 'inventory_action_qualityctrls';
                $this->{$this->attrName}['actionControl']['display_order'] = 0;
                $this->{$this->attrName}['actionControl']['databrowser_label'] = 'quality controls';
                $this->{$this->attrName}['old_alias'] = 'qualityctrls';
            } elseif ($this->tableName == 'aliquot_internal_uses') {

                $this->{$this->attrName}['masterFieldDetailAlias'] = [];
//                $this->{$this->attrName}['masterFieldDetailAlias']['used_by'] = [
//                    'type' => 'varchar(50)',
//                    'null' => 'yes',
//                    'alias' => 'person',
//                ];
//                $this->{$this->attrName}['masterFieldDetailAlias']['use_code'] = [
//                    'type' => 'varchar(50)',
//                    'null' => 'yes',
//                    'alias' => 'title',
//                ];

                $this->{$this->attrName}['controlFields'] = [];
                $this->{$this->attrName}['masterFields'] = array_merge($this->{$this->attrName}['masterFields'], [
                    'use_code' => array('type' => 'varchar(100)', 'null' => 'yes', 'alias' => 'title'),
                    'used_by' => array('type' => 'varchar(50)', 'null' => 'yes', 'alias' => 'person'),
                    'use_details' => ['type' => 'text', 'null' => 'yes', 'alias' => 'notes'],
                    'use_datetime' => ['type' => 'date', 'null' => 'yes', 'alias' => 'date'],
                    'use_datetime_accuracy' => [
                        'type' => 'char(1)',
                        'null' => 'no',
                        'default' => '',
                        'alias' => 'date_accuracy',
                    ],
                ]);
                $this->{$this->attrName}['actionControl']['type'] = 'aliquot_internal_uses';
                $this->{$this->attrName}['actionControl']['category'] = 'aliquot_internal_uses';
                $this->{$this->attrName}['actionControl']['apply_on'] = 'aliquots';
                $this->{$this->attrName}['actionControl']['sample_control_ids'] = null;
                $this->{$this->attrName}['actionControl']['aliquot_control_ids'] = 'all';
                $this->{$this->attrName}['actionControl']['detail_form_alias'] = 'inventory_action_aliquotinternaluses';
                $this->{$this->attrName}['actionControl']['display_order'] = 0;
                $this->{$this->attrName}['actionControl']['databrowser_label'] = 'aliquot uses and events';
                $this->{$this->attrName}['old_alias'] = 'aliquotinternaluses';
            }
        } elseif ($this->tableName == 'specimen_review_controls') {
            $this->prepareSpecimenAliquotReview();
        } else {
            die("$this->tableName is not permitted");
        }

        return $this->attrName;
    }

    private function getAliquotControls()
    {
        try {
            $query = "SELECT * FROM aliquot_controls;";
            $stmt = $this->db->prepare($query) or die("Query ($query) failed at line " . __LINE__ . " " . $query . " " . $this->db->error);
            $stmt->execute();
            $res = $stmt->get_result();
            $row = $res->fetch_assoc();
            if ($row) {
                while ($row) {
                    $this->aliquotControls[$row['id']] = $row;
                    $row = $res->fetch_assoc();
                }
            }
        } catch (Exception $e) {
            $this->result['Errors'][] = "aliquot_controls, " . $e->getMessage();
        }
    }

    private function stringGenerate($string, $counter)
    {
        $number = (ord($string[0]) - 97) * 26 + (ord($string[1]) - 97);
        $number += $counter;
        $string = chr((int)($number / 26) + 97) . chr(($number % 26) + 97);
        return $string;
    }

    private function generateDetailTableName($tmpTableName, $index, $actionControls)
    {
        $detailTableName = $tmpTableName;
        $counter = 0;
        do {
            $suffix = $this->stringGenerate('aa', $counter);
            $found = false;
            foreach ($actionControls as $i => $actionControl) {
                if ($i == $index) {
                    break;
                }
                if ($actionControl['detail_tablename'] == $detailTableName) {
                    $found = true;
                    $detailTableName = $tmpTableName . "_" . $suffix;
                    break;
                }
            }
            $counter++;
        } while ($found);

        return $detailTableName;
    }

    private function generateDetailFormAlias($tmpDetailFormAlias, $index, $actionControls)
    {
        $detailFormAlias = $tmpDetailFormAlias;
        $counter = 0;
        do {
            $suffix = $this->stringGenerate('aa', $counter);
            $found = false;
            foreach ($actionControls as $i => $actionControl) {
                if ($i == $index) {
                    break;
                }
                if ($actionControls[$i]['detail_form_alias'] == $detailFormAlias) {
                    $found = true;
                    $detailFormAlias = $tmpDetailFormAlias . "_" . $suffix;
                    break;
                }
            }
            $counter++;
        } while ($found);

        return $detailFormAlias;
    }

    private function prepareSpecimenAliquotReview()
    {
        $this->getAliquotControls();
        $this->{$this->attrName}['ignoredFields'] = [];
        $this->{$this->attrName}['masterFieldDetailAlias'] = [];
//        $this->{$this->attrName}['masterFieldDetailAlias']['pathologist'] = [
//            'type' => 'varchar(50)',
//            'null' => 'yes',
//            'alias' => 'person',
//        ];
//        $this->{$this->attrName}['masterFieldDetailAlias'][''] = array('type' => 'varchar(50)', 'null' => 'yes', 'alias' => 'title');


        $this->{$this->attrName}['masterFields'] = array_merge($this->{$this->attrName}['masterFields'], [
            'review_code' => ['type' => 'varchar(100)', 'null' => 'yes', 'alias' => 'title'],
//            'pathologist' => array('type' => 'varchar(50)', 'null' => 'yes', 'alias' => 'person'),
            'notes' => ['type' => 'text', 'null' => 'yes', 'alias' => 'notes'],
            'review_date' => ['type' => 'date', 'null' => 'yes', 'alias' => 'date'],
            'review_date_accuracy' => [
                'type' => 'char(1)',
                'null' => 'no',
                'default' => '',
                'alias' => 'date_accuracy',
            ],
        ]);

        try {
            $query = "
                    SELECT
                        s.id s_id,
                        s.sample_control_id,
                        s.aliquot_review_control_id s_aliquot_review_control_id,
                        s.review_type s_review_type,
                        a.review_type a_review_type,
                        s.flag_active s_flag_active,
                        a.flag_active a_flag_active,
                        s.detail_form_alias s_detail_form_alias,
                        a.detail_form_alias a_detail_form_alias,
                        s.detail_tablename s_detail_tablename,
                        a.detail_tablename a_detail_tablename,
                        s.databrowser_label s_databrowser_label,
                        a.databrowser_label a_databrowser_label,
                        a.aliquot_type_restriction a_aliquot_type_restriction
                    FROM `specimen_review_controls` s
                    LEFT JOIN `aliquot_review_controls` a ON a.id=s.aliquot_review_control_id 
                        ORDER BY s_id;";
            $stmt = $this->db->prepare($query) or die("Query ($query) failed at line " . __LINE__ . " " . $query . " " . $this->db->error);
            $stmt->execute();
            $res = $stmt->get_result();
            $row = $res->fetch_assoc();

            if ($row) {
                while ($row) {
                    $index = $row['s_id'];
                    $this->{$this->attrName}['controls'][$index] = $row;
                    $this->{$this->attrName}['actionControl'][$index]['flag_active'] = $row['s_flag_active'];
//                        $this->{$this->attrName}['actionControl'][$index]['flag_link_to_storage'] = '0';
//                        $this->{$this->attrName}['actionControl'][$index]['flag_link_to_study'] = '1';
                    $this->{$this->attrName}['actionControl'][$index]['flag_test_mode'] = '0';
                    $this->{$this->attrName}['actionControl'][$index]['flag_form_builder'] = '0';
//                        $this->{$this->attrName}['actionControl'][$index]['flag_active_input'] = '1';

                    $tmpTableName = 'ac_' . $row['s_detail_tablename'] . (empty($row['a_detail_tablename']) ? '' : '_' . $row['a_detail_tablename']);
                    $this->{$this->attrName}['actionControl'][$index]['detail_tablename'] = $tmpTableName . "_$index";
//                    $this->{$this->attrName}['actionControl'][$index]['detail_tablename'] = $this->generateDetailTableName($tmpTableName, $index, $this->{$this->attrName}['actionControl']);
                    $tmpDetailFormAlias = 'ac_' . preg_replace('/,/', '_',
                            $row['s_detail_form_alias']) . (empty($row['a_detail_form_alias']) ? '' : '_' . preg_replace('/,/',
                                '_', $row['a_detail_form_alias']));
                    $this->{$this->attrName}['actionControl'][$index]['detail_form_alias'] = $this->generateDetailFormAlias($tmpDetailFormAlias,
                        $index, $this->{$this->attrName}['actionControl']);

                    $this->{$this->attrName}['actionControl'][$index]['type'] = $row['s_review_type'];
                    $this->{$this->attrName}['actionControl'][$index]['category'] = 'review';

                    $this->{$this->attrName}['actionControl'][$index]['sample_control_ids'] = $row['sample_control_id'];

                    if (!empty($row['s_aliquot_review_control_id'])) {
                        $this->{$this->attrName}['actionControl'][$index]['apply_on'] = 'samples and some related aliquots';
                        if ($row['a_aliquot_type_restriction'] == 'all') {
                            $this->{$this->attrName}['actionControl'][$index]['aliquot_control_ids'] = 'all';
                        } else {
                            $aliuotIdRestriction = array_filter($this->aliquotControls, function ($item) use ($row) {
                                $aliquotTypeRestriction = array_map('trim',
                                    explode(",", $row['a_aliquot_type_restriction']));
                                return $item['sample_control_id'] == $row['sample_control_id'] && (in_array($item['aliquot_type'],
                                            $aliquotTypeRestriction) !== false);
                            });
                            $this->{$this->attrName}['actionControl'][$index]['aliquot_control_ids'] = implode("|||",
                                array_map(function ($item) {
                                    return $item['id'];
                                }, $aliuotIdRestriction));
                        }
                    } else {
                        $this->{$this->attrName}['actionControl'][$index]['apply_on'] = 'samples';
                        $this->{$this->attrName}['actionControl'][$index]['aliquot_control_ids'] = '';
                    }
                    $this->{$this->attrName}['actionControl'][$index]['display_order'] = 0;
                    $this->{$this->attrName}['actionControl'][$index]['databrowser_label'] = $row['s_databrowser_label'] . (empty($row['a_databrowser_label']) ? "" : " - " . $row['a_databrowser_label']);
                    $this->{$this->attrName}['actionControlOldValues'][$index]['alias'] = $row['s_detail_form_alias'] . (empty($row['a_detail_form_alias']) ? "" : "," . $row['a_detail_form_alias']);
                    $this->{$this->attrName}['actionControlOldValues'][$index]['s_id'] = $row['s_id'];
                    $this->{$this->attrName}['actionControlOldValues'][$index]['a_id'] = $row['s_aliquot_review_control_id'];
                    $this->{$this->attrName}['actionControlOldValues'][$index]['tablename'] = [
                        'sMaster' => 'specimen_review_masters',
                        'aMaster' => 'aliquot_review_masters',
                        'sDetail' => $row['s_detail_tablename'],
                        'aDetail' => $row['a_detail_tablename'],
                    ];
                    $this->{$this->attrName}['actionControlOldValues'][$index]['old_alias'] = "specimen_review_masters," . $this->{$this->attrName}['actionControlOldValues'][$index]['alias'];

                    $row = $res->fetch_assoc();
                }
            }
        } catch (Exception $e) {
            $this->result['Errors'][] = "$this->tableName, " . $e->getMessage();
        }
    }

    private function insertToInventoryActionControl()
    {

        extract($this->{$this->attrName});

        $actionControl['flag_link_to_study'] = in_array('study_summary_id', $fields) !== false ? 1 : 0;

        if ($actionControl['apply_on'] != 'samples') {
            $actionControl['flag_link_to_storage'] = 1;
        }

        $query = [];

        if($this->attrName == "QualityCtrls"){
            $query[] = $this->createInsertQuery('inventory_action_controls', $actionControl);
            $query[] = "SET @actionControlId = LAST_INSERT_ID();";
        }elseif($this->attrName == "AliquotInternalUses"){
            $getTypeValueIAUQuery = "
                SELECT `source`
                FROM structure_value_domains
                where id = (SELECT sfi.structure_value_domain
                            FROM `structure_formats` sfo
                                     JOIN `structure_fields` sfi ON sfo.structure_field_id = sfi.id
                            WHERE `structure_id` = (SELECT id FROM `structures` WHERE `alias` = 'aliquotinternaluses')
                              AND sfi.`field` = 'type') LIMIT 1;
             ";
            $result = $this->db->query($getTypeValueIAUQuery) or die($this->db->error);
            $row = $result->fetch_assoc();
            if (!empty($row['source'])){
                $match = preg_match("/(StructurePermissibleValuesCustom::getCustomDropdown\()'(.+)'(\))/", $row['source'], $a);
                if(!empty($a[2])){
                    $a = $a[2];
                    $getTypeValueIAUQuery = "
                        SELECT `value`, `en`, `fr`  FROM `structure_permissible_values_customs` WHERE `control_id` IN (
                            SELECT id FROM `structure_permissible_values_custom_controls` WHERE `name` = '$a'
                        ) AND `deleted` = 0";
                    $result = $this->db->query($getTypeValueIAUQuery) or die($this->db->error);
                    $rows = $result->fetch_all();

                    if (!empty($rows)){
                        foreach ($rows as $index => $row){
                            $this->AliquotInternalUseControlIds [$row['0']] = $index;
                            $actionControl['type'] = $row[0];
                            $actionControl['flag_batch_action'] = 1;

                            $query[] = sprintf("INSERT IGNORE INTO `i18n` (`id`, `en`, `fr`) VALUES ('%s', '%s', '%s');", addslashes($row[0]), addslashes($row[1]), addslashes($row[2]));
                            $query[] = $this->createInsertQuery('inventory_action_controls', $actionControl);
                            $query[] = "SET @actionControlId$index = LAST_INSERT_ID();";
                        }
                    }
                }
            }
        }


        return $query;
    }

    private function createInsertQuery($tableName, $fields, $definitions = [])
    {
        $query = "INSERT INTO `$tableName` (%s) VALUES %s;";
        if (empty($definitions)) {
            $header = sprintf("`%s`", implode('`, `', array_keys($fields)));
            if (!is_array(current($fields))) {
                $values = '(';
                foreach ($fields as $field) {
                    if (is_string($field)) {
                        if (strpos($field, '(SELECT `') === 0 || strpos($field, '@') === 0) {
                            $values .= "$field, ";
                        } else {
                            $field = str_replace("'", "\'", $field);
                            $values .= "'$field', ";
                        }
                    } elseif ($field === null) {
                        $values .= "NULL, ";
                    } else {
                        $values .= "$field, ";
                    }
                }
                $values = rtrim($values, ", ") . ")";
            } else {
                $values = '(';
                $fieldsKeys = array_keys(current($fields));
                foreach ($fieldsKeys as $key) {
                    foreach ($fields as $field) {
                        $fieldValue = $field[$key];
                        if (is_string($fieldValue)) {
                            $fieldValue = str_replace("'", "\'", $fieldValue);
                            $values .= "'$fieldValue', ";
                        } elseif ($fieldValue === null) {
                            $values .= "NULL, ";
                        } else {
                            $values .= "$fieldValue, ";
                        }
                    }
                    $values = rtrim($values, ", ") . "), (";

                }
                $values = rtrim($values, ", (");
            }
        } else {
            $keys = [];
            $values = '(';
            foreach ($fields as $key => $field) {
                if (isset($definitions[$key])) {
                    $definition = $definitions[$key];
                    $keys[] = isset($definition['alias']) ? $definition['alias'] : $key;
                    if (is_string($field)) {
                        if (strpos($field, '(SELECT `') === 0 || strpos($field, '@') === 0) {
                            $values .= "$field, ";
                        } else {
                            $field = str_replace("'", "\'", $field);
                            $values .= "'$field', ";
                        }
                    } elseif ($field === null) {
                        $values .= "NULL, ";
                    } else {
                        $values .= "$field, ";
                    }
                }
            }
            $values = rtrim($values, ", ") . ")";
            $header = sprintf("`%s`", implode('`, `', $keys));
        }
        $query = sprintf($query, $header, $values);

        return $query;
    }

    private function createDetailTable($tableDesc)
    {

        extract($this->{$this->attrName});
        $detail_tablename = $actionControl['detail_tablename'];
        $query = "CREATE TABLE `$detail_tablename` (`id` int(11) NOT NULL AUTO_INCREMENT, `inventory_action_master_id` int(11) NOT NULL,";
        $this->tables[$detail_tablename] = true;
        $queryRevs =  "CREATE TABLE `{$detail_tablename}_revs` (`id` int(11) NOT NULL, `inventory_action_master_id` int(11) NOT NULL,";
        $postFixQuery = " , PRIMARY KEY (id),  CONSTRAINT `FK_inventory_action_masters_$detail_tablename` FOREIGN KEY (`inventory_action_master_id`) REFERENCES `inventory_action_masters` (`id`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;";
        $postFixQueryRevs = " , version_id int NOT NULL PRIMARY KEY AUTO_INCREMENT, version_created datetime NOT NULL) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';";
        $createFieldQuery = [];
        $createFieldQueryRevs = [];
        extract($this->{$this->attrName});
        $ignoredAndMasterFields = array_merge(array_keys($masterFields), array_keys($ignoredFields), array_keys($masterFieldDetailAlias));
        foreach ($tableDesc as $field) {
            if (in_array($field[0], $ignoredAndMasterFields) === false && ($this->attrName != 'AliquotInternalUses' || $field[0] != 'type')) {
                $createFieldQuery[] = "`{$field[0]}` {$field[1]} " . ($field[2] == 'NO' ? " NOT NULL" : " NULL") . " DEFAULT " . ($field[4] === null ? "NULL" : "'$field[4]'") . ($field[3] == 'UNI' ? " UNIQUE " : "");
                $createFieldQueryRevs[] = "`{$field[0]}` {$field[1]} NULL ";
                $this->{$this->attrName}['detailFields'][$field[0]] = $field[0];
            }
        }
        $query .= implode(", ", $createFieldQuery) . $postFixQuery;
        $queryRevs .= implode(", ", $createFieldQueryRevs) . $postFixQueryRevs;

        return "$query \n$queryRevs";
    }

    private function migrationData()
    {
        $query = "
            SELECT * FROM {$this->tableName};
        ";

        extract($this->{$this->attrName});
        try {
            $stmt = $this->db->prepare($query) or die("Query ($query) failed at line " . __LINE__ . " " . $query . " " . $this->db->error);
            $stmt->execute();
            $res = $stmt->get_result();
            $row = $res->fetch_assoc();

            $insertMasterQuery = [];

            if ($row) {
                while ($row) {
//                    $insertMasterQuery[] = $this->getMasterValue($row, array_merge($masterFields));
                    $insertMasterQuery[] = $this->getMasterValue($row, array_merge($masterFields, $masterFieldDetailAlias));
                    $insertMasterQuery[] = "SET @actionMasterId = LAST_INSERT_ID();";
                    $detailRow = array_filter($row, function ($item) use ($detailFields) {
                        return in_array($item, $detailFields) !== false;
                    }, ARRAY_FILTER_USE_KEY);
                    $detailRow['inventory_action_master_id'] = '@actionMasterId';

                    $insertMasterQuery[] = $this->createInsertQuery($actionControl['detail_tablename'], $detailRow);
                    $row = $res->fetch_assoc();
                }
            }
        } catch (Exception $e) {
            $this->result[$this->tableName]['Errors'][] = $e->getMessage();
        }
        return $insertMasterQuery;
    }

    private function createStructure()
    {
        $response = [];


        extract($this->{$this->attrName});
        $response[] = "INSERT INTO `structures` (`alias`) VALUES ('{$actionControl['detail_form_alias']}');";
        $response[] = "SET @structureID = LAST_INSERT_ID();";

        $allFieldFormat = $this->getAllFieldFormats();
        foreach ($allFieldFormat['field'] as $id => &$sField) {
            $sFormat = $allFieldFormat['format'][$id];
            $alias = $sField['alias'];
            unset ($sField['id']);
            unset ($sField['alias']);
//            if (in_array($sField['field'], array_keys($masterFieldDetailAlias)) === false) {
//                $sField['model'] = 'InventoryActionDetail';
//                $sField['tablename'] = $actionControl['detail_tablename'];
//            } elseif (in_array($sField['field'], array_keys($masterFields)) !== false) {
//                $sField['model'] = 'InventoryActionMaster';
//                $sField['tablename'] = 'inventory_action_masters';
//            }else{
//                $sField['model'] = 'InventoryActionMaster';
//                $sField['tablename'] = 'inventory_action_masters';
//                $sField['field'] = $masterFieldDetailAlias[$sField['field']]['alias'];
//            }
            if (in_array($sField['field'], array_keys($masterFields)) !== false) {
                $sField['model'] = 'InventoryActionMaster';
                $sField['tablename'] = 'inventory_action_masters';
                $sFormat['display_column'] = 0;
            } elseif (in_array($sField['field'], array_keys($masterFieldDetailAlias)) !== false) {
                $sField['model'] = 'InventoryActionMaster';
//                $sField['tablename'] = $actionControl['detail_tablename'];
                $sField['tablename'] = 'inventory_action_masters';
                $sField['field'] = $masterFieldDetailAlias[$sField['field']]['alias'];
//                $sFormat['display_column'] = (int)$sFormat['display_column'] + 1;
                $sFormat['display_column'] = 1;
            } else {
                $sField['model'] = 'InventoryActionDetail';
                $sField['tablename'] = $actionControl['detail_tablename'];
//                $sFormat['display_column'] = (int)$sFormat['display_column'] + 1;
                $sFormat['display_column'] = 1;
            }
            if (empty($sField['structure_value_domain'])) {
                $sField['structure_value_domain'] = null;
            }
            if (!empty($masterFields[$sField['field']]['alias'])) {
                $sField['field'] = $masterFields[$sField['field']]['alias'];
            }

            if(empty($sField['structure_value_domain'])){
                $indexSFId = $sField['field'] . "-" . $sField['type'] . "-" . $sField['model'] . "-" . $sField['tablename'] . "-" . "0";
            }else{
                $indexSFId = $sField['field'] . "-" . $sField['type'] . "-" . $sField['model'] . "-" . $sField['tablename'] . "-" . $sField['structure_value_domain'];
            }

                if (!empty($sField['structure_value_domain'])) {
                    $response[] = "SET @count = (SELECT count(*) FROM structure_fields WHERE `field` = '{$sField['field']}' AND `type` = '{$sField['type']}' AND `model` = '{$sField['model']}' AND `tablename` = '{$sField['tablename']}' AND `structure_value_domain` = '{$sField['structure_value_domain']}');";
                    $response[] = "SET @structureFieldId = (SELECT IF(@count <> 0, (SELECT id FROM structure_fields WHERE `field` = '{$sField['field']}' AND `type` = '{$sField['type']}' AND `model` = '{$sField['model']}' AND `tablename` = '{$sField['tablename']}' AND `structure_value_domain` = '{$sField['structure_value_domain']}'), -1));";
                } else {

                    $response[] = "SET @count = (SELECT count(*) FROM structure_fields WHERE `field` = '{$sField['field']}' AND `type` = '{$sField['type']}' AND `model` = '{$sField['model']}' AND `tablename` = '{$sField['tablename']}' AND `structure_value_domain` IS NULL);";
                    $response[] = "SET @structureFieldId = (SELECT IF(@count <> 0, (SELECT id FROM structure_fields WHERE `field` = '{$sField['field']}' AND `type` = '{$sField['type']}' AND `model` = '{$sField['model']}' AND `tablename` = '{$sField['tablename']}' AND `structure_value_domain` IS NULL), -1));";
                }

                $response[] = $this->createConditionalInsert("structure_fields", $sField, "@count = 0");
                $response[] = "SET @structureFieldId = IF(@count = 0, LAST_INSERT_ID(), @structureFieldId);";

            unset($sFormat['alias']);
            unset($sFormat['id']);
            unset($sFormat['structure_id']);
            unset($sFormat['structure_field_id']);
            $formsTypeCanHaveGride = [
                'flag_add',
                'flag_add_readonly',
                'flag_edit',
                'flag_edit_readonly',
                'flag_batchedit',
                'flag_index',
            ];
            $formsTypewithGride = [
                'flag_addgrid',
                'flag_addgrid_readonly',
                'flag_editgrid',
                'flag_editgrid_readonly',
                'flag_batchedit_readonly',
                'flag_detail',
            ];
            foreach ($formsTypeCanHaveGride as $i => $formType) {
                $sFormat[$formsTypewithGride[$i]] = (!empty($sFormat[$formType])) ? $sFormat[$formType] : $sFormat[$formsTypewithGride[$i]];
                $sFormat[$formType] = (!empty($sFormat[$formsTypewithGride[$i]])) ? $sFormat[$formsTypewithGride[$i]] : $sFormat[$formType];
            }

            $sFormat['structure_id'] = "@structureID";
            $sFormat['structure_field_id'] = "@structureFieldId";

            $sFormat['display_order'] = (int)$sFormat['display_order'] + 20;
            $response[] = $this->createInsertQuery('structure_formats', $sFormat);
        }
        return $response;
    }

    private function getAllFieldFormats($index = null)
    {

        extract($this->{$this->attrName});

        $structFieldColumns = [
            "id",
            "public_identifier",
            "plugin",
            "model",
            "tablename",
            "field",
            "language_label",
            "language_tag",
            "type",
            "setting",
            "default",
            "structure_value_domain",
            "language_help",
            "validation_control",
            "value_domain_control",
            "field_control",
            "flag_confidential",
            "sortable",
        ];
        $structFormatColumns = [
            "id",
            "structure_id",
            "structure_field_id",
            "display_column",
            "display_order",
            "language_heading",
            "flag_override_label",
            "language_label",
            "flag_override_tag",
            "language_tag",
            "flag_override_help",
            "language_help",
            "flag_override_type",
            "type",
            "flag_override_setting",
            "setting",
            "flag_override_default",
            "default",
            "flag_add",
            "flag_add_readonly",
            "flag_edit",
            "flag_edit_readonly",
            "flag_search",
            "flag_search_readonly",
            "flag_addgrid",
            "flag_addgrid_readonly",
            "flag_editgrid",
            "flag_editgrid_readonly",
            "flag_summary",
            "flag_batchedit",
            "flag_batchedit_readonly",
            "flag_index",
            "flag_detail",
            "flag_float",
            "margin",
        ];
//        $detailsAliasFields = array_merge(array_keys($detailFields), array_keys($masterFieldDetailAlias), array_keys($masterFields));
        $detailsAliasFields = array_merge(array_keys($detailFields), array_keys($masterFieldDetailAlias));
        $inQueryFieldsValues = implode("', '", $detailsAliasFields);

        $structFieldColumnsQuery = "SFI.`" . implode("`, SFI.`", $structFieldColumns) . "`";
        $structFormatColumnsQuery = "SFO.`" . implode("`, SFO.`", $structFormatColumns) . "`";
        $structureFields = [];
        $structureFormat = [];

        if (!empty($index)) {
            $old_alias = str_replace(",", "', '", $actionControlOldValues[$index]['old_alias']);
        }

        $query = "
            SELECT S2.alias, $structFieldColumnsQuery 
            FROM structure_formats SFO
            JOIN structure_fields SFI ON SFI.id = SFO.structure_field_id
            JOIN structures S2 ON SFO.structure_id = S2.id
            WHERE 
            SFO.structure_id IN (SELECT id FROM structures S WHERE alias IN ('$old_alias')) 
            AND SFI.field IN ('$inQueryFieldsValues')
            ORDER BY SFO.id
        ";
        try {
            $stmt = $this->db->prepare($query) or die("Query ($query) failed at line " . __LINE__ . " " . $query . " " . $this->db->error);
            $stmt->execute();
            $res = $stmt->get_result();
            $row = $res->fetch_assoc();

            if ($row) {
                while ($row) {
                    $structureFields[$row['id']] = $row;

                    $row = $res->fetch_assoc();
                }
            }
        } catch (Exception $e) {
            $this->result[$this->tableName]['Errors'][] = $e->getMessage();
        }

        $query = "
            SELECT S2.alias, $structFormatColumnsQuery 
            FROM structure_formats SFO
            JOIN structure_fields SFI ON SFI.id = SFO.structure_field_id
            JOIN structures S2 ON SFO.structure_id = S2.id
            WHERE 
            SFO.structure_id IN (SELECT id FROM structures S WHERE alias IN ('$old_alias')) 
            AND SFI.field IN ('$inQueryFieldsValues')
            ORDER BY SFO.id
        ";

        try {
            $stmt = $this->db->prepare($query) or die("Query ($query) failed at line " . __LINE__ . " " . $query . " " . $this->db->error);
            $stmt->execute();
            $res = $stmt->get_result();
            $row = $res->fetch_assoc();

            if ($row) {
                while ($row) {
                    $structureFormat[$row['structure_field_id']] = $row;

                    $row = $res->fetch_assoc();
                }
            }
        } catch (Exception $e) {
            $this->result[$this->tableName]['Errors'][] = $e->getMessage();
        }

        return ['field' => $structureFields, 'format' => $structureFormat];
    }

    private function getMasterValue($row, $generalMasterFields)
    {
        if (!empty($row['aliquot_master_id']) && empty($row['sample_master_id'])) {
            $row['sample_master_id'] = "(SELECT `sample_master_id` FROM `aliquot_masters` WHERE `id` = '{$row['aliquot_master_id']}')";
            if (empty($row['initial_specimen_sample_id'])) {
                $row['initial_specimen_sample_id'] = "(SELECT `initial_specimen_sample_id` FROM `sample_masters` WHERE `id` = (SELECT `sample_master_id` FROM `aliquot_masters` WHERE `id` = '{$row['aliquot_master_id']}'))";
            }
        } elseif (!empty($row['sample_master_id']) && empty($row['initial_specimen_sample_id'])) {
            $row['initial_specimen_sample_id'] = "(SELECT `initial_specimen_sample_id` FROM `sample_masters` WHERE `id` = '{$row['sample_master_id']}')";
        }
        if ($this->attrName == 'QualityCtrls'){
            $row['inventory_action_control_id'] = "@actionControlId";
        }elseif ($this->attrName == 'AliquotInternalUses'){
            $type = $row['type'];
            $row['inventory_action_control_id'] = "@actionControlId" . $this->AliquotInternalUseControlIds[$type];
        }

        if (empty($row['created_by'])) {
            $row['created_by'] = "1";
        }

        if (empty($row['modified_by'])) {
            $row['modified_by'] = "1";
        }

        $tempDataArray = key(array_filter($generalMasterFields, function ($item){
            return isset($item['alias']) && $item['alias'] == 'date';
        }));

        if (isset($row[$tempDataArray]) && isset($row[$tempDataArray."_accuracy"]) && $row[$tempDataArray."_accuracy"] == 'c') {
            if(preg_match("/(^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$)/", $row[$tempDataArray], $matches) === 1){
                $row[$tempDataArray."_accuracy"] = 'h';
            }
        }

        $response = $this->createInsertQuery('inventory_action_masters', $row, $generalMasterFields);

        return $response;
    }

    private function fetchAllAssoc($query, $options = [])
    {
        $response = [];

        if (!empty($options['tablenames'])) {
            $tablenames = $options['tablenames'];
            $headers = [];
            $index = 0;
            foreach ($tablenames as $table) {
                $q = "DESCRIBE $table";
                $stmt = $this->db->prepare($q) or die("Query ($q) failed at line " . __LINE__ . " " . $q . " " . $this->db->error);
                $stmt->execute();

                $res = $stmt->get_result();
                $rows = $res->fetch_all();
                foreach ($rows as $row) {
                    $headers[$index][$table]["$row[0]"] = [];
                    $index++;
                }
            }
        }
        $stmt = $this->db->prepare($query) or die("Query ($query) failed at line " . __LINE__ . " " . $query . " " . $this->db->error);
        $stmt->execute();

        $res = $stmt->get_result();
        $rows = $res->fetch_all();
        $columnNames = array_keys($headers);
        foreach ($rows as $rowIndex => &$row) {
            foreach ($row as $index => &$value) {
//                $headers[$index][key($headers[$index])][key(current($headers[$index]))][] =$value;
                $t = key($headers[$index]);
                $c = key(current($headers[$index]));

                $nextColumnName = "";
                if (isset($rows[$rowIndex][$index+1])){
                    $nextColumnValue = $rows[$rowIndex][$index+1];
                    $nextColumnName = key(current($headers[$index+1]));
                }
                if ($c == 'review_date' && $nextColumnName  == 'review_date_accuracy'){
                    $hFormat = preg_match("/(^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$)/", $value, $matches);
                    if ($nextColumnValue == 'c' && $hFormat === 1){
                        $rows[$rowIndex][$index+1] = 'h';
                    }
                }
                $response[$t][$c][] = $value;
            }
        }
        return $response;
    }

    private function joinAssoc($referencedTable, $childTable, $options = [])
    {
//example:
//        $specimenAliquotReviewMasters = $this->joinAssoc($specimenReviewMasters, $aliquotReviewMasters,
//            array('refrenceAlias' => 's', 'pKey' => 'id', 'childAlias' => 'a', 'fKey' => 'specimen_review_master_id')
//        );

        $joinResult = [];

        foreach ($childTable as $row) {
            extract($options);
            $options['joinValue'] = $row[$fKey];
            $relatedData = array_filter($referencedTable, function ($item) use ($options) {
                return $item[$options['pKey']] == $options['joinValue'];
            });
            $id = $row['id'];
            foreach ($row as $field => $value) {
                $joinResult[$id]["$childAlias.$field"] = $value;
            }

            foreach ($relatedData[0] as $field => $value) {
                $joinResult[$id]["$refrenceAlias.$field"] = $value;
            }
        }
        return $joinResult;
    }

    private function getColumnTableNames($tableName)
    {
        $result = [];
        if (empty($tableName)) {
            return $result;
        }
        try {
            $query = "DESCRIBE $tableName;";
            $stmt = $this->db->prepare($query) or die("Query ($query) failed at line " . __LINE__ . " " . $query . " " . $this->db->error);
            $stmt->execute();
            $res = $stmt->get_result();
            $row = $res->fetch_assoc();
            if ($row) {
                while ($row) {
                    $currentField = array_values($row);
                    $tableDesc[$currentField[0]] = $currentField;
                    $row = $res->fetch_assoc();
                }
            }
        } catch (Exception $e) {
            $this->result['Errors'][] = "$this->tableName, " . $e->getMessage();
        }
        return $tableDesc;
    }

    private function createDetailSpecimenReviewTable($index, $tableDesc)
    {
        extract($this->{$this->attrName});
        $detailTablename = $actionControl[$index]['detail_tablename'];
        $foreignKeyId = (strlen("FK_inventory_action_masters_$detailTablename") > 60) ? substr("FK_inventory_action_masters_$detailTablename",
                0, 61) . $index : "FK_inventory_action_masters_$detailTablename";

        $query = "CREATE TABLE `$detailTablename` (`id` int(11) NOT NULL AUTO_INCREMENT, `inventory_action_master_id` int(11) NOT NULL,";
        $this->tables[$detailTablename] = true;
        $queryRevs = "CREATE TABLE `{$detailTablename}_revs` (`id` int(11) NOT NULL, `inventory_action_master_id` int(11) NOT NULL,";
        $postFixQuery = ",PRIMARY KEY (id), CONSTRAINT `$foreignKeyId` FOREIGN KEY (`inventory_action_master_id`) REFERENCES `inventory_action_masters` (`id`)) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1";
        $postFixQueryRevs = " , version_id int NOT NULL PRIMARY KEY AUTO_INCREMENT, version_created datetime NOT NULL) ENGINE='InnoDB' COLLATE 'latin1_swedish_ci';";
        $createFieldQuery = [];
        $createFieldQueryRevs = [];
//        $ignoredAndMasterFields = array_merge(array_keys($masterFields), array_keys($ignoredFields));
        $ignoredAndMasterFields = array_merge(array_keys($masterFields), array_keys($ignoredFields), array_keys($masterFieldDetailAlias));
        $masterFields = array_merge($tableDesc['sMaster'], $tableDesc['aMaster']);

        $detailIntersection = array_intersect(array_keys($tableDesc['sDetail']), array_keys($tableDesc['aDetail']));
        if (!empty($detailIntersection)) {
            foreach ($detailIntersection as $field) {
                $alias = $field . "1";
                $tableDesc['aDetail'][$alias] = $tableDesc['aDetail'][$field];
                $tableDesc['aDetail'][$alias][0] = $alias;
                unset($tableDesc['aDetail'][$field]);
            }
        }
        $detailFields = array_merge($tableDesc['sDetail'], $tableDesc['aDetail']);
        unset($detailFields['aliquot_review_master_id']);

        $allFields = [];
        foreach ($masterFields as $field) {
            if (in_array($field[0], $ignoredAndMasterFields) === false) {
                if ($field[2] == 'NO' && $field[4] === null) {
                    $createFieldQuery[] = "`{$field[0]}` {$field[1]} " . ($field[2] == 'NO' ? " NOT NULL" : " NULL") . ($field[3] == 'UNI' ? " UNIQUE " : "");
                } else {
                    $createFieldQuery[] = "`{$field[0]}` {$field[1]} " . ($field[2] == 'NO' ? " NOT NULL" : " NULL") . " DEFAULT " . ($field[4] === null ? "NULL" : "'$field[4]'") . ($field[3] == 'UNI' ? " UNIQUE " : "");
                }
                $createFieldQueryRevs[] = "`{$field[0]}` {$field[1]} NULL ";
                $this->{$this->attrName}['detailFields'][$field[0]] = $field[0];
                $allFields[] = $field[0];
            }
        }

        foreach ($detailFields as $field) {
            if (in_array($field[0], $allFields) === false || substr($field[0], strlen($field[0]) - 3,
                    strlen($field[0])) !== "_id") {
                if ($field[2] == 'NO' && $field[4] === null) {
                    $createFieldQuery[] = "`{$field[0]}` {$field[1]} " . " NULL" . ($field[3] == 'UNI' ? " UNIQUE " : "");
                } else {
                    $createFieldQuery[] = "`{$field[0]}` {$field[1]} " . ($field[2] == 'NO' ? " NOT NULL" : " NULL") . " DEFAULT " . ($field[4] === null ? "NULL" : "'$field[4]'") . ($field[3] == 'UNI' ? " UNIQUE " : "");
                }
                $createFieldQueryRevs[] = "`{$field[0]}` {$field[1]} NULL ";
                $this->{$this->attrName}['detailFields'][$field[0]] = $field[0];
            }
        }
        $query .= implode(", ", $createFieldQuery) . " $postFixQuery;";
        $queryRevs .= implode(", ", $createFieldQueryRevs) . $postFixQueryRevs;
        return "$query \n$queryRevs";
    }

    private function migrationSpecimenReviewData($index)
    {
        $insertMasterQuery = [];
        extract($this->{$this->attrName});
        if (!empty($actionControl[$index]['aliquot_control_ids'])) {
            $sMaster = $actionControlOldValues[$index]['tablename']['sMaster'];
            $sDetail = $actionControlOldValues[$index]['tablename']['sDetail'];
            $aMaster = $actionControlOldValues[$index]['tablename']['aMaster'];
            $aDetail = $actionControlOldValues[$index]['tablename']['aDetail'];
            $tablenames = [$sMaster, $sDetail, $aMaster, $aDetail];
            $query = "
              SELECT * FROM $sMaster sm
              LEFT JOIN $sDetail sd ON sd.specimen_review_master_id = sm.id
              LEFT JOIN $aMaster am ON am.specimen_review_master_id = sm.id
              LEFT JOIN $aDetail ad ON ad.aliquot_review_master_id = am.id
              WHERE sm.specimen_review_control_id = $index
              ORDER By am.specimen_review_master_id, am.deleted;";
        } else {
            $sMaster = $actionControlOldValues[$index]['tablename']['sMaster'];
            $sDetail = $actionControlOldValues[$index]['tablename']['sDetail'];
            $aMaster = "";
            $aDetail = "";
            $tablenames = [$sMaster, $sDetail];
            $query = "
              SELECT * FROM $sMaster sm
              LEFT JOIN $sDetail sd ON sd.specimen_review_master_id = sm.id
              WHERE sm.specimen_review_control_id = $index;";
        }
        $specimenReviewMasters = $this->fetchAllAssoc($query, ['tablenames' => $tablenames]);
        if (!empty($specimenReviewMasters)) {
            $numberOfRows = count($specimenReviewMasters[$sMaster]['id']);

            $data = [];
            $fields = [];
            for ($j = 0; $j < $numberOfRows; $j++) {

                if (empty($state) || $state['id'] != $specimenReviewMasters[$sMaster]['id'][$j]) {
                    $state = [
                        'id' => $specimenReviewMasters[$sMaster]['id'][$j],
                        'check' => false,
                        'save' => 'sampleAliquot',
                    ];
                }
                if ($specimenReviewMasters[$sMaster]['deleted'][$j] == '1') {
                    if (!empty($specimenReviewMasters[$aMaster]['id'][$j])) {
                        $insertMasterQuery[] = "-- The SpecimenReviewMaster (" . $specimenReviewMasters[$sMaster]['id'][$j] . ") with AliquotMasterId (" . $specimenReviewMasters[$aMaster]['id'][$j] . ") is deleted and not imported.";
                    } else {
                        $insertMasterQuery[] = "-- The SpecimenReviewMaster (" . $specimenReviewMasters[$sMaster]['id'][$j] . ") is deleted and not imported.";
                    }
                    $state['save'] = false;
                } elseif (empty($aMaster) || empty($specimenReviewMasters[$aMaster]['id'][$j])) {
                    $insertMasterQuery[] = "-- The SpecimenReviewMaster (" . $specimenReviewMasters[$sMaster]['id'][$j] . ") does not have AliquotReviewMaster";
                    $state['check'] = true;
                    $state['save'] = 'sample';
                    $specimenReviewMasters[$sMaster]['deleted'][$j] = '0';
                } elseif (!empty($specimenReviewMasters[$aMaster]['id'][$j])) {
                    if ($specimenReviewMasters[$aMaster]['deleted'][$j] == '0') {
                        $state['check'] = true;
                     // There are maybe some ARMs to import but this one is deleted
                    } elseif ($specimenReviewMasters[$aMaster]['deleted'][$j] == '1' && $state['check']) {
                        $insertMasterQuery[] = "-- The AliquotReveiwMaster (" . $specimenReviewMasters[$aMaster]['id'][$j] . ") related to SpecimenReview (" . $specimenReviewMasters[$sMaster]['id'][$j] . ") is deleted and not imported.";
                        $state['save'] = false;
                    // If the current ARMs is deleted try to find at least one which is not deleted
                    } elseif ($specimenReviewMasters[$aMaster]['deleted'][$j] == '1' && !$state['check']) {
                        for ($k = $j + 1; $k < $numberOfRows && !$state['check'] && $specimenReviewMasters[$sMaster]['id'][$k] == $specimenReviewMasters[$sMaster]['id'][$j]; $k++) {
                            if (empty($specimenReviewMasters[$sMaster]['deleted'][$k]) && !empty($specimenReviewMasters[$aMaster]['id'][$k]) && $specimenReviewMasters[$aMaster]['deleted'][$k] == '0') {
                                $state['check'] = true;
                            }
                        }
                        if ($state['check']) {
                            $insertMasterQuery[] = "-- The AliquotReveiwMaster (" . $specimenReviewMasters[$aMaster]['id'][$j] . ") related to SpecimenReview (" . $specimenReviewMasters[$sMaster]['id'][$j] . ") is deleted and not imported.";
                            $state['save'] = false;
                        } else {
                            $insertMasterQuery[] = "-- All the AliquotReveiwMaster related to active SpecimenReview (" . $specimenReviewMasters[$sMaster]['id'][$j] . ") are deleted but the SpecimenReviewMaster saved.";
                            $state['check'] = true;
                            $state['save'] = 'sample';
                        }
                    }
                }

                if (!empty($state['save'])) {
                    $sMasterData = $specimenReviewMasters[$sMaster];
                    if (!empty($aMaster) && !empty($specimenReviewMasters[$aMaster])) {
                        $aMasterData = $specimenReviewMasters[$aMaster];
                    }

                    $detailTableName = $actionControl[$index]['detail_tablename'];

                    foreach ($specimenReviewMasters as $table => $specimenReviewMaster) {

                        if ($state['save'] == 'sample' && ($table == $aMaster || $table == $aDetail)) {
                            continue;
                        }
                        foreach ($specimenReviewMaster as $field => $values) {
                            if ($j == 0) {
                                if (in_array($field, ["specimen_review_master_id", "aliquot_review_master_id", "created", "created_by", "modified", "modified_by", "id", "deleted"]) === false) {
                                    if ($field == "review_code") {
                                        if ($table == $sMaster || empty($fields[$field])) {
                                            $fields[$field][] = ['alias' => $field, 'table' => $table];
                                        } else {
                                            continue;
                                        }
                                    } elseif (!empty($fields[$field])) {
//                                        if (!empty($masterFields[$field]) && ($table == $sMaster || $table == $aMaster)) {
                                        if (!empty($masterFieldDetailAlias[$field]) && ($table == $sMaster || $table == $aMaster)) {
                                            $rnd = "1";
                                            $fields[$field][0]['alias'] .= $rnd;
                                            $insertMasterQuery[] = "-- The '$field' field of the '$table' table has already exists, so in the database it's renamed to " . $fields[$field][0]['alias'] . ".";
                                            $fields[$field][1] = array_merge($fields[$field],
//                                                array_merge($masterFields[$field], array('table' => $table)));
                                                array_merge($masterFieldDetailAlias[$field], ['table' => $table]));
                                        } else {
                                            $fields[$field][0]['alias'] = $field;
                                            $rnd = "1";
                                            $fields[$field][1] = ['alias' => $field . $rnd, 'table' => $table];
                                            $insertMasterQuery[] = "-- The '$field' field of the '$table' table has already exists, so in the database it's renamed to " . $fields[$field][1]['alias'] . ".";
                                        }
                                    } else {
//                                        if (!empty($masterFields[$field])) {
                                        if (!empty($masterFieldDetailAlias[$field])) {

//                                            $fields[$field][0] = array_merge($masterFields[$field],
                                            $fields[$field][0] = array_merge($masterFieldDetailAlias[$field],
                                                ['table' => $table]);
                                        } else {
                                            $fields[$field][0] = ['alias' => $field, 'table' => $table];
                                        }
                                    }
                                }
                            }
                            $fieldKey = $field;

                            if (isset($fields[$field]) && is_array($fields[$field])) {
                                foreach ($fields[$field] as $f) {
                                    if ($table == $f['table'] && !empty($f['alias'])) {
                                        $fieldKey = $f['alias'];
                                        if ($fieldKey == 'person' && $field == 'pathologist') {
                                            $fieldKey = "pathologist";
                                        }
                                    }
                                }
                            }
                            $data[$j][$fieldKey] = $values[$j];
                        }
                    }

                    if ($j == 0) {
                        $this->{$this->attrName}['all_fields'][$index] = $fields;
                    }

                    $data[$j]['inventory_action_control_id'] = "@actionControlId";
                    $data[$j]['notes'] = $specimenReviewMasters[$sMaster]['notes'][$j];
                    $data[$j]['sample_master_id'] = $sMasterData['sample_master_id'][$j];
                    if ($state['save'] == 'sample') {
                        $data[$j]['id'] = $sMasterData['id'][$j];
                        $data[$j]['review_code'] = $sMasterData['review_code'][$j];
                        $data[$j]['created'] = $sMasterData['created'][$j];
                        $data[$j]['created_by'] = $sMasterData['created_by'][$j];
                        $data[$j]['modified'] = $sMasterData['modified'][$j];
                        $data[$j]['modified_by'] = $sMasterData['modified_by'][$j];
                        $data[$j]['deleted'] = $sMasterData['deleted'][$j];
                        $data[$j]['initial_specimen_sample_id'] = "(SELECT `initial_specimen_sample_id` FROM `sample_masters` WHERE `id` = '{$sMasterData['sample_master_id'][$j]}')";;
                    } elseif ($state['save'] == 'sampleAliquot') {
                        $data[$j]['id'] = $sMasterData['id'][$j] . "-" . $aMasterData['id'][$j];
                        $data[$j]['review_code'] = $sMasterData['review_code'][$j] . (empty($aMasterData['review_code'][$j]) ? "" : "-") . $aMasterData['review_code'][$j];
                        $data[$j]['created'] = $aMasterData['created'][$j];
                        $data[$j]['created_by'] = $aMasterData['created_by'][$j];
                        $data[$j]['modified'] = $aMasterData['modified'][$j];
                        $data[$j]['modified_by'] = $aMasterData['modified_by'][$j];
                        $data[$j]['deleted'] = $aMasterData['deleted'][$j];
                        if (!empty($sMasterData['sample_master_id'][$j])) {
                            $data[$j]['initial_specimen_sample_id'] = "(SELECT `initial_specimen_sample_id` FROM `sample_masters` WHERE `id` = '{$sMasterData['sample_master_id'][$j]}')";;
                        } else {
                            $data[$j]['initial_specimen_sample_id'] = "(SELECT `initial_specimen_sample_id` FROM `sample_masters` WHERE `id` = (SELECT `sample_master_id` FROM `aliquot_masters` WHERE `id` = '{$aMasterData['aliquot_master_id'][$j]}'))";
                        }
                    }

                    if (isset($detailRow['created_by']) && empty($detailRow['created_by'])) {
                        $detailRow['created_by'] = "1";
                    }

                    if (isset($detailRow['modified_by']) && empty($detailRow['modified_by'])) {
                        $detailRow['modified_by'] = "1";
                    }
                    $insertMasterQuery[] = $this->createInsertQuery('inventory_action_masters', $data[$j],
                        array_merge($masterFields, $masterFieldDetailAlias));
//                    $insertMasterQuery[] = $this->createInsertQuery('inventory_action_masters', $data[$j], $masterFieldDetailAlias);

                    $insertMasterQuery[] = "SET @actionMasterId = LAST_INSERT_ID();";

                    $detailRow = array_filter($data[$j], function ($item) use ($detailFields) {
                        return in_array($item, $detailFields) !== false;
                    }, ARRAY_FILTER_USE_KEY);
                    $detailRow['inventory_action_master_id'] = '@actionMasterId';
                    $insertMasterQuery[] = $this->createInsertQuery($detailTableName, $detailRow);
                }

            }
        } else {
            $insertMasterQuery[] = "-- There is no data related to this SpecimenReview(id = $index).";
        }
        return $insertMasterQuery;
    }

    private function createSpecimenReviewStructure($index)
    {
        $response = [];
        extract($this->{$this->attrName});

        $response[] = "INSERT INTO `structures` (`alias`) VALUES ('{$actionControl[$index]['detail_form_alias']}');";
        $response[] = "SET @structureID = LAST_INSERT_ID();";

        $allFieldFormat = $this->getAllFieldFormats($index);
        foreach ($allFieldFormat['field'] as $id => &$sField) {
            $sFormat = $allFieldFormat['format'][$id];
            $alias = $sField['alias'];
            unset ($sField['id']);
            unset ($sField['alias']);

            if (in_array($sField['field'], array_keys($masterFields)) !== false) {
                $sField['model'] = 'InventoryActionMaster';
                $sField['tablename-old'] = $sField['tablename'];
                $sField['tablename'] = 'inventory_action_masters';
                $sFormat['display_column'] = 0;
            } elseif (in_array($sField['field'], array_keys($masterFieldDetailAlias)) !== false) {
                $sField['model'] = 'InventoryActionMaster';
                $sField['tablename-old'] = $sField['tablename'];
                $sField['tablename'] = 'inventory_action_masters';
//                $sField['tablename'] = $actionControl[$index]['detail_tablename'];
//                $sFormat['display_column'] = (int)$sFormat['display_column'] + 10;
                $sFormat['display_column'] = 1;
            } else {
                $sField['model'] = 'InventoryActionDetail';
                $sField['tablename-old'] = $sField['tablename'];
                $sField['tablename'] = $actionControl[$index]['detail_tablename'];
//                $sFormat['display_column'] = (int)$sFormat['display_column'] + 10;
                $sFormat['display_column'] = 1;
            }
            if (!empty($all_fields[$index][$sField['field']])) {
                $fields = $all_fields[$index][$sField['field']];
                if (!empty($fields[0]) && is_array($fields[0])) {
                    foreach ($fields as $k => $f) {
                        if ($f['table'] == $sField['tablename-old']) {
//                                $i = $k;
                            $sField['field'] = $f['alias'];
                            break;
                        }
                    }
                }
            }
            unset($sField['tablename-old']);
            //check all fields array and also the table name
            if (empty($sField['structure_value_domain'])) {
                $sField['structure_value_domain'] = null;
            }
//            if (!empty($aliasDuplicated) || in_array($alias, $aliasAlreadyExists) !== false && $sField['tablename'] == 'inventory_action_masters'){
//echo "<br>___",json_encode($sField),"____<br>";
//                $setStructureFieldIdQuery = "SET @structureFieldId = (%s);";
//                $selectCondition = "SELECT id FROM `structure_fields` WHERE `field` = '{$sField['field']}' and `type`= '{$sField['type']}' and `model`='{$sField['model']}' and `tablename`= '{$sField['tablename']}'";
//                if (empty($sField['structure_value_domain'])){
//                    $selectCondition .= " and `structure_value_domain` IS NULL";
//                }else{
//                    $selectCondition .= " and `structure_value_domain` = '{$sField['structure_value_domain']}'";
//                }
//                $response[] = sprintf($setStructureFieldIdQuery, $selectCondition);
//            }else{
//                $response[] = $this->createInsertQuery('structure_fields', $sField);
//                $response[] = "SET @structureFieldId = LAST_INSERT_ID();";
//            }
//echo $sField['field']."-".$sField['model']."<br>";
            if ($sField['model'] != 'InventoryActionMaster') {
                $response[] = $this->createInsertQuery('structure_fields', $sField);
                $response[] = "SET @structureFieldId = LAST_INSERT_ID();";
            } else {
                if (!empty($sField['structure_value_domain'])) {
                    $response[] = "SET @count = (SELECT count(*) FROM structure_fields WHERE `field` = '{$sField['field']}' AND `type` = '{$sField['type']}' AND `model` = '{$sField['model']}' AND `tablename` = '{$sField['tablename']}' AND `structure_value_domain` = '{$sField['structure_value_domain']}');";
                    $response[] = "SET @structureFieldId = (SELECT IF(@count <> 0, (SELECT id FROM structure_fields WHERE `field` = '{$sField['field']}' AND `type` = '{$sField['type']}' AND `model` = '{$sField['model']}' AND `tablename` = '{$sField['tablename']}' AND `structure_value_domain` = '{$sField['structure_value_domain']}'), -1));";
                } else {

                    $response[] = "SET @count = (SELECT count(*) FROM structure_fields WHERE `field` = '{$sField['field']}' AND `type` = '{$sField['type']}' AND `model` = '{$sField['model']}' AND `tablename` = '{$sField['tablename']}' AND `structure_value_domain` IS NULL);";
                    $response[] = "SET @structureFieldId = (SELECT IF(@count <> 0, (SELECT id FROM structure_fields WHERE `field` = '{$sField['field']}' AND `type` = '{$sField['type']}' AND `model` = '{$sField['model']}' AND `tablename` = '{$sField['tablename']}' AND `structure_value_domain` IS NULL), -1));";
                }

                $response[] = $this->createConditionalInsert("structure_fields", $sField, "@count = 0");
                $response[] = "SET @structureFieldId = IF(@count = 0, LAST_INSERT_ID(), @structureFieldId);";
            }

            unset($sFormat['id']);
            unset($sFormat['alias']);
            unset($sFormat['structure_id']);
            unset($sFormat['structure_field_id']);
            $sFormat['structure_id'] = "@structureID";
            $sFormat['structure_field_id'] = "@structureFieldId";
            $formsTypeCanHaveGride = [
                'flag_add',
                'flag_add_readonly',
                'flag_edit',
                'flag_edit_readonly',
                'flag_batchedit',
                'flag_index',
            ];
            $formsTypewithGride = [
                'flag_addgrid',
                'flag_addgrid_readonly',
                'flag_editgrid',
                'flag_editgrid_readonly',
                'flag_batchedit_readonly',
                'flag_detail',
            ];
            foreach ($formsTypeCanHaveGride as $i => $formType) {
                $sFormat[$formsTypewithGride[$i]] = (!empty($sFormat[$formType])) ? $sFormat[$formType] : $sFormat[$formsTypewithGride[$i]];
                $sFormat[$formType] = (!empty($sFormat[$formsTypewithGride[$i]])) ? $sFormat[$formsTypewithGride[$i]] : $sFormat[$formType];
            }

//            $response[] = "SELECT @structureFieldId as ID_1, @structureID as ID_2, @count as counter;";
            $sFormat['display_order'] = (int)$sFormat['display_order'] + 20;
            $response[] = $this->createInsertQuery('structure_formats', $sFormat);
        }
        return $response;
    }

    private function createConditionalInsert($tablename, $fields, $conditions)
    {
        $query = "INSERT INTO `$tablename` (`%s`) SELECT '%s' WHERE $conditions;";

        if ($fields['structure_value_domain'] == null) {
            $fields['structure_value_domain'] = 'NULL';
        }
        $fieldsString = implode("`, `", array_keys($fields));
        $valueString = implode("', '", array_values($fields));
        $valueString = str_replace("'NULL'", "NULL", $valueString);

        $query = sprintf($query, $fieldsString, $valueString);
        return $query;
    }

    private function generateSpecimenQueries()
    {

        extract($this->{$this->attrName});

        $specimenReviewMasterColumns = $this->getColumnTableNames('specimen_review_masters');
        $aliquotReviewMasterColumns = $this->getColumnTableNames('aliquot_review_masters');
        foreach ($actionControl as $index => $action) {
            $query = [];
            $aliquotDetailTableName = $actionControlOldValues[$index]['tablename']['aDetail'];
            $specimenDetailTableName = $actionControlOldValues[$index]['tablename']['sDetail'];
            $aliquotReviewDetailColumns = $this->getColumnTableNames($aliquotDetailTableName);
            $specimenReviewDetailColumns = $this->getColumnTableNames($specimenDetailTableName);
            $fields = array_merge(array_keys($specimenReviewMasterColumns), array_keys($aliquotReviewMasterColumns),
                array_keys($aliquotReviewDetailColumns), array_keys($specimenReviewDetailColumns));
            $action['flag_link_to_study'] = in_array('study_summary_id', $fields) !== false ? 1 : 0;
            $query [] = $this->createInsertQuery('inventory_action_controls', $action);
            $query [] = "SET @actionControlId = LAST_INSERT_ID();";

            $this->result[$this->tableName][$action['detail_tablename']]['Insert to Inventory Action Control Table'] = $query;

            $this->result[$this->tableName][$action['detail_tablename']]["Create Detail Table"][] = $this->createDetailSpecimenReviewTable($index,
                [
                    'sMaster' => $specimenReviewMasterColumns,
                    'aMaster' => $aliquotReviewMasterColumns,
                    'sDetail' => $specimenReviewDetailColumns,
                    'aDetail' => $aliquotReviewDetailColumns,
                ]);

            $this->result[$this->tableName][$action['detail_tablename']]["MigrationDataQueries"] = $this->migrationSpecimenReviewData($index);

            $this->result[$this->tableName][$action['detail_tablename']]["Structure"] = $this->createSpecimenReviewStructure($index);
        }
    }

    private function convertOldToInventoryActions()
    {
        $this->init();
        $tableDesc = [];
        try {
            if (in_array($this->tableName, ['quality_ctrls', 'aliquot_internal_uses']) !== false) {
                $query = "DESCRIBE $this->tableName;";
                $stmt = $this->db->prepare($query) or die("Query ($query) failed at line " . __LINE__ . " " . $query . " " . $this->db->error);
                $stmt->execute();
                $res = $stmt->get_result();
                $row = $res->fetch_assoc();
                if ($row) {
                    while ($row) {
                        $currentField = array_values($row);
                        $tableDesc[$currentField[0]] = $currentField;
                        $this->{$this->attrName}['fields'][] = $currentField[0];
                        $row = $res->fetch_assoc();
                    }
                    $this->result[$this->tableName]['Insert to Inventory Action Control Table'] = $this->insertToInventoryActionControl();
                    $this->result[$this->tableName]["Create Detail Table"][] = $this->createDetailTable($tableDesc);
                    $this->result[$this->tableName]["MigrationDataQueries"] = $this->migrationData();
                    $this->result[$this->tableName]["Structure"] = $this->createStructure();
                }
            } elseif ($this->tableName == 'specimen_review_controls') {
                $this->generateSpecimenQueries();
            }
        } catch (Exception $e) {
            $this->result['Errors'][] = "$this->tableName, " . $e->getMessage();
        }

    }

    public function __destruct()
    {
    }

    public function finalizingMigration()
    {
        $this->result['finalizingScripts'] = [];

        //<editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/400): InventoryUseEventMigration.php : inventory_action_controls.databrowser_label content">
        $this->result['finalizingScripts']["issue400"] [] = "UPDATE `inventory_action_controls` SET `category`= 'specimen review' WHERE `category` = 'review';";
        $this->result['finalizingScripts']["issue400"] [] = "UPDATE `inventory_action_controls` SET `databrowser_label` = `type` WHERE `type` = `category`;";
        $this->result['finalizingScripts']["issue400"] [] = "UPDATE `inventory_action_controls` SET `databrowser_label` = CONCAT(category,'|',type) WHERE `type` != `category`;";
        //</editor-fold>

        //<editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/537): ReIndex the Views and some huge tables in newVersionSetup">
        $this->result['finalizingScripts']["issue537"] [] = "ANALYZE TABLE " . implode(", ", array_keys($this->tables)) . ";";
        //</editor-fold>

        $this->result['finalizingScripts']["version"] [] = "UPDATE versions SET permissions_regenerated = 0;";

    }

}

$inventoryUseEventMigration =  new InventoryUseEventMigration();
$inventoryUseEventMigration->startMigration();
$inventoryUseEventMigration->finalizingMigration();
$inventoryUseEventMigration->saveToFile();