-- ----------------------------------------------------------------------------------------------------------------------------------
-- ATiM Database Upgrade Script
-- Version: 2.8.2
--
-- For more information:
--    ./app/scripts/v2.8.0/ReadMe.me
--    http://wiki.atim-software.ca username/pass: ATiM/ATiMWiKi
-- ----------------------------------------------------------------------------------------------------------------------------------

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/544): In Inventory/Action creations in batch, support the update of the aliquot stock information">
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/544): In Inventory/Action creations in batch, support the update of the aliquot stock information
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO structures(`alias`) VALUES ('iea_aliquot_master_edit_in_batchs');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'FunctionManagement', '', 'remove_from_storage', 'checkbox',  NULL , '0', '', '', '', '', 'or remove');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='in_stock' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_values')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='aliquot_in_stock_help' AND `language_label`='aliquot in stock' AND `language_tag`='new value'),
     '1', '400', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='in_stock_detail' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_detail')  AND `flag_confidential`='0'),
     '1', '500', '', '0', '0', '', '1', 'new value', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_in_stock_detail' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='or erase data'),
     '1', '501', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='recorded_storage_selection_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '1', '701', '', '0', '0', '', '1', 'new storage selection label', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_from_storage' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='or remove'),
     '1', '702', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs'),
     (SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '1', '1180', '', '0', '1', 'study / project', '1', 'new value', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_aliquot_master_study_summary_id' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '1', '1180', '', '0', '0', '', '1', 'new value', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_study_summary_id' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='or erase data'),
     '1', '1181', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs'),
     (SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='sop_master_id' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_sop_list')  AND `flag_confidential`='0'),
     '1', '1200', '', '0', '0', '', '1', 'new value', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_sop_master_id' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='or erase data'),
     '1', '1202', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/546): Some problems in AutoComplete">
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/546): Some problems in AutoComplete
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE `structure_fields`
SET
    `setting` = REPLACE(`setting`, 'autocomplete', 'autoComplete')
WHERE
        `setting` LIKE '%autocomplete%';

UPDATE `structure_formats`
SET
    `setting` = REPLACE(`setting`, 'autocomplete', 'autoComplete')
WHERE
        `setting` LIKE '%autocomplete%' AND
        `flag_override_setting` <> 0;
-- </editor-fold>

-- <editor-fold desc="Add pbmc derivatives links">
-- ----------------------------------------------------------------------------------------------------------------------------------
-- PBMC to t b and no-b cells
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pbmc'), (SELECT id FROM sample_controls WHERE sample_type = 't cell'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pbmc'), (SELECT id FROM sample_controls WHERE sample_type = 'b cell'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pbmc'), (SELECT id FROM sample_controls WHERE sample_type = 'no-b cell'), 0, NULL); 
-- </editor-fold>

-- <editor-fold desc="Add pbmc supernatant derivative">
-- ----------------------------------------------------------------------------------------------------------------------------------
-- PBMC supernatant
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'pbmc supernatant', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_pbmc_supernatants', 0, 'pbmc supernatant');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pbmc'), (SELECT id FROM sample_controls WHERE sample_type = 'pbmc supernatant'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('pbmc supernatant', 'PBMC Supernatant', 'Surnageant de PBMC');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'pbmc supernatant'), 'tube', '', 'ad_der_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0, 'pbmc supernatant|tube');

CREATE TABLE `sd_der_pbmc_supernatants` (
  `sample_master_id` int(11) NOT NULL,
  KEY `FK_sd_der_pbmc_supernatants_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_der_pbmc_supernatants_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_der_pbmc_supernatants_revs` (
  `sample_master_id` int(11) NOT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- </editor-fold>

-- <editor-fold desc="Create cfRNA sample as in chum hbp project">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Create cfRNA sample as in chum hbp project
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order, databrowser_label)
VALUES
(null, 'cfrna', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_rnas', 0, 'cfrna');
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'plasma'), (SELECT id FROM sample_controls WHERE sample_type = 'cfrna'),0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'serum'), (SELECT id FROM sample_controls WHERE sample_type = 'cfrna'), 0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('cfrna', 'cfRNA', 'cfRNA');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order, databrowser_label)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cfrna'), 'tube', '', 'ad_der_tubes_incl_ul_vol_and_conc', 'ad_tubes', 'ul', 0, '', 0, 'cfrna|tube');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/565): Add pasteDisabled and size in FB as setting">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Add pasteDisabled and size in FB as setting
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('Core', 'FunctionManagement', '', 'settings', 'button',  NULL , '0', 'class=fb_settings', '', '', 'settings', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_structure'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='settings' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=fb_settings' AND `default`='' AND `language_help`='' AND `language_label`='settings' AND `language_tag`=''),
     '1', '135', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='form_builder_master_structure'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='settings' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=fb_settings' AND `default`='' AND `language_help`='' AND `language_label`='settings' AND `language_tag`=''),
     '1', '150', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO structures(`alias`) VALUES ('form_builder_settings');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('Core', 'FunctionManagement', '', 'can_paste', 'select',  (SELECT id FROM structure_value_domains WHERE domain_name='yesno') , '0', '', 'yes', 'can_paste_help', 'can_paste', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_settings'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='can_paste' AND `type`='select' AND `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='yesno')  AND `flag_confidential`='0' AND `setting`='' AND `default`='yes' AND `language_help`='can_paste_help' AND `language_label`='can_paste' AND `language_tag`=''),
     '1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

INSERT INTO structure_validations (structure_field_id, rule) VALUES
    ((SELECT id FROM structure_fields WHERE `plugin` = 'Core' AND `model` = 'FunctionManagement' AND `tablename` = '' AND `field` = 'can_paste' AND `type` = 'select' AND `structure_value_domain` = (SELECT id FROM structure_value_domains WHERE domain_name='yesno')), 'notBlank');

INSERT IGNORE INTO i18n (`id`, `en`, `fr`) VALUES
    ('can_paste', 'Can paste', 'Can paste'),
    ('can_paste_help', 'Can Paste help', 'Can Paste help'),
    ('the field settings', 'The field settings', 'The field settings'),
-- ('', '', ''),
-- ('', '', ''),
    ('settings', 'Settings', 'Paramètres');

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/561): Add a column as float for sample actions in Batch">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Add a column as float for sample actions in Batch
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO structures(`alias`) VALUES ('inventory_action_sample_types_float');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_sample_types_float'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='sample_controls' AND `field`='sample_type' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='sample type' AND `language_tag`=''),
     '0', '-1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '1');

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/553): Password Verification input has the different style comparing the password input">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Password Verification input has the different style comparing the password input
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `structure_validations` (`structure_field_id`, `rule`) values
((SELECT id FROM `structure_fields` WHERE `plugin` = '' AND `model` = 'Generated' AND `tablename` = '' AND `field` = 'field1'), 'notBlank');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/576): In Create Batch Action Apply to all action: Add fields to update the in stock fields of the used aliquots">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	In Create Batch Action Apply to all action: Add fields to update the in stock fields of the used aliquots
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE structure_formats SET `display_column`='2000', `language_heading`='apply on aliquots' WHERE structure_id=(SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='in_stock' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_values') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2000' WHERE structure_id=(SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='in_stock_detail' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_in_stock_detail') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2000' WHERE structure_id=(SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_in_stock_detail' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2000' WHERE structure_id=(SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='recorded_storage_selection_label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2000' WHERE structure_id=(SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_from_storage' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`='or remove');
UPDATE structure_formats SET `display_column`='2000' WHERE structure_id=(SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2000', `flag_add`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='autocomplete_aliquot_master_study_summary_id' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2000', `flag_add`='0' WHERE structure_id=(SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_study_summary_id' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2000' WHERE structure_id=(SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='AliquotMaster' AND `tablename`='aliquot_masters' AND `field`='sop_master_id' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_sop_list') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_column`='2000' WHERE structure_id=(SELECT id FROM structures WHERE alias='iea_aliquot_master_edit_in_batchs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='remove_sop_master_id' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT IGNORE INTO `i18n` (`id`, `en`, `fr`) VALUES
("apply on aliquots", "Selected aliquots update (to apply to all)", "Mise à jour des aliquots sélectionnés (à appliquer à tous)");
-- </editor-fold>

-- <editor-fold desc="Added missing value to the drop down list diagnosis_category_fix_list">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Added missing value to the drop down list diagnosis_category_fix_list
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("remission", "remission");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) 
VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="diagnosis_category_fix_list"), (SELECT id FROM structure_permissible_values WHERE value="remission" AND language_alias="remission"), "6", "1");
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/581): Replace model controls table field 'databrowser_label' by a function 'getTranslatedDatabrowserLabel' to populate the action drop down list and more...">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Replace model controls table field 'databrowser_label' by a function 'getTranslatedDatabrowserLabel' to populate the action drop down list and more...
-- ----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE diagnosis_controls DROP COLUMN databrowser_label;
ALTER TABLE event_controls DROP COLUMN databrowser_label;
ALTER TABLE treatment_controls DROP COLUMN databrowser_label;
ALTER TABLE treatment_extend_controls DROP COLUMN databrowser_label;
ALTER TABLE sample_controls DROP COLUMN databrowser_label;
ALTER TABLE aliquot_controls DROP COLUMN databrowser_label;
ALTER TABLE inventory_action_controls DROP COLUMN databrowser_label;

DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_diagnosis') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='ClinicalAnnotation' AND `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='databrowser_label' AND `language_label`='databrowser label' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='databrowser label help' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');
DELETE FROM structure_validations WHERE structure_field_id IN (SELECT id FROM structure_fields WHERE (`public_identifier`='' AND `plugin`='ClinicalAnnotation' AND `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='databrowser_label' AND `language_label`='databrowser label' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='databrowser label help' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0'));
DELETE FROM structure_fields WHERE (`public_identifier`='' AND `plugin`='ClinicalAnnotation' AND `model`='DiagnosisControl' AND `tablename`='diagnosis_controls' AND `field`='databrowser_label' AND `language_label`='databrowser label' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='databrowser label help' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_treatment') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='ClinicalAnnotation' AND `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='databrowser_label' AND `language_label`='databrowser label' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='databrowser label help' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_treatment_import_process') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='ClinicalAnnotation' AND `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='databrowser_label' AND `language_label`='databrowser label' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='databrowser label help' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_event') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='ClinicalAnnotation' AND `model`='EventControl' AND `tablename`='event_controls' AND `field`='databrowser_label' AND `language_label`='databrowser label' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='databrowser label help' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');
DELETE FROM structure_validations WHERE structure_field_id IN (SELECT id FROM structure_fields WHERE (`public_identifier`='' AND `plugin`='ClinicalAnnotation' AND `model`='EventControl' AND `tablename`='event_controls' AND `field`='databrowser_label' AND `language_label`='databrowser label' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='databrowser label help' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0'));
DELETE FROM structure_fields WHERE (`public_identifier`='' AND `plugin`='ClinicalAnnotation' AND `model`='EventControl' AND `tablename`='event_controls' AND `field`='databrowser_label' AND `language_label`='databrowser label' AND `language_tag`='' AND `type`='input' AND `setting`='' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='databrowser label help' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');
DELETE FROM `structure_fields` WHERE `field` = 'databrowser_label' AND `language_label` = 'databrowser label' AND `model` IN ('DiagnosisControl', 'EventControl', 'TreatmentControl');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/574): List all study aliquots whatever the link between aliquot and study">
-- -------------------------------------------------------------------------------------------------------------------------------------------------------
-- Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/574): List all study aliquots whatever the link between aliquot and study
-- List all aliquots/tmas linked to study through an order
-- -------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO i18n (id, en, fr)
VALUES ('order aliquots', 'Order Aliquots', 'Articles de commandes aliquots'),
('order tma slides', 'Order TMA-Slides', 'Articles de commandes TMA-Slides');

INSERT INTO structures(`alias`) VALUES ('orders_for_study_order_items');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='orders_for_study_order_items'), 
(SELECT id FROM structure_fields WHERE `model`='Order' AND `tablename`='orders' AND `field`='order_number' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='order_order number' AND `language_tag`=''), 
'0', '38', 'order', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='orders_for_study_order_items'), 
(SELECT id FROM structure_fields WHERE `model`='Order' AND `tablename`='orders' AND `field`='short_title' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='order_short title' AND `language_tag`=''), 
'0', '39', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');

-- </editor-fold>


-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/586): Add different text type in FB to support the long form">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Add different text type in FB to support the long forms
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("input_short", "input_short");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="input_short" AND language_alias="input_short"), "3", "1");
INSERT IGNORE INTO structure_permissible_values (value, language_alias) VALUES("input_medium", "input_medium");
INSERT INTO structure_value_domains_permissible_values (structure_value_domain_id, structure_permissible_value_id, display_order, flag_active) VALUES ((SELECT id FROM structure_value_domains WHERE domain_name="form_builder_type_list"), (SELECT id FROM structure_permissible_values WHERE value="input_medium" AND language_alias="input_medium"), "4", "1");

INSERT IGNORE INTO i18n (id,en,fr) VALUES
('input_short', 'Text box (Max length = 50)', 'Boîte de texte (longueur maximale = 50)'),
('input_medium', 'Text box (Max length = 200)', 'Boîte de texte (longueur maximale = 200)');

REPLACE INTO i18n (id,en,fr) VALUES
('input', 'Text box (Max length = 1000)', 'Boite de texte (longueur maximale = 1000)');

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/454): Participant Vs Sutdy report">
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Participant Vs Sutdy report 
-- ----------------------------------------------------------------------------------------------------------------------------------
-- ----------------------------------------------------------------------
-- Participant Studies Report :
--    From a list of participant and chosen project : 
--        if participant is linked to project through a model, then count Consent, Identifier, Aliquot Reserved, inventory action, Order Items linked to participant for the project
--    If no project chosen, include all.
-- -----------------------------------------------------------------------
-- datamart_reports
INSERT INTO `datamart_reports` (`id`, `name`, `description`, `form_alias_for_search`, `form_alias_for_results`, `form_type_for_results`, `function`, `flag_active`, `associated_datamart_structure_id`, `limit_access_from_datamart_structure_function`)
VALUES
(null, 'report participant studies', 'report for participant studies', 'participant_studies_search', 'participant_studies_result', 'index', 'participantStudiesReport', 1, (SELECT id FROM datamart_structures WHERE model = 'Participant'), 0);
 
-- datamart_structure_functions
INSERT INTO `datamart_structure_functions` (`id`, `datamart_structure_id`, `label`, `link`, `flag_active`, `ref_single_fct_link`)
VALUES
(null, 
(SELECT id FROM datamart_structures WHERE model = 'Participant'), 
'report participant studies', CONCAT('/Datamart/Reports/manageReport/', (SELECT id FROM datamart_reports WHERE name = 'report participant studies')), 1, 0),
(null, 
(SELECT id FROM datamart_structures WHERE model = 'StudySummary'), 
'report participant studies', CONCAT('/Datamart/Reports/manageReport/', (SELECT id FROM datamart_reports WHERE name = 'report participant studies')), 1, 0);

-- Study multiselect list 
INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('study_name_list', 'open', '', 'Study.StudySummary::getStudyNamePermissibleValues');

-- Participant Studies Search Form
INSERT INTO structures(`alias`) VALUES ('participant_studies_search');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Study', 'StudySummary', 'study_summaries', 'title', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='study_name_list') , '0', 'class=atim-multiple-choice', '', '', 'study_title', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='participant_studies_search'), 
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '5', 'clin_demographics', '0', '0', '', '0', '', '0', '', '0', '', '0', 'size=20,class=range file', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='participant_studies_search'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='report_date_range' AND `type`='date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='date range' AND `language_tag`=''), 
'0', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='participant_studies_search'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='study_name_list')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='' AND `language_label`='study_title' AND `language_tag`=''), 
'0', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- Participant Studies Result Form
INSERT INTO structures(`alias`) VALUES ('participant_studies_result');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'study', 'input',  NULL , '0', 'size=20', '', '', 'study', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='participant_identifier' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='help_participant identifier' AND `language_label`='participant identifier' AND `language_tag`=''), 
'0', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='participant_studies_result'),
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='study' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='study' AND `language_tag`=''), 
'0', '23', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
-- Data (count) to display in columns 
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'consent', 'input',  NULL , '0', 'size=20', '', '', 'consent', ''), 
('Datamart', '0', '', 'miscidentifier', 'input',  NULL , '0', 'size=20', '', '', 'miscidentifier', ''), 
('Datamart', '0', '', 'inventory_action', 'input',  NULL , '0', 'size=20', '', '', 'inventory action', ''), 
('Datamart', '0', '', 'aliquot_reserved', 'input',  NULL , '0', 'size=10', '', '', 'aliquot reserved', ''), 
('Datamart', '0', '', 'order_items', 'input',  NULL , '0', 'size=20', '', '', 'order items', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='consent' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='consent' AND `language_tag`=''), 
'0', '25', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='miscidentifier' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='miscidentifier' AND `language_tag`=''), 
'0', '30', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='inventory action' AND `language_tag`=''), 
'0', '35', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='order_items' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='' AND `language_label`='order items' AND `language_tag`=''), 
'0', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_reserved' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '32', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=20', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
UPDATE structure_formats SET `flag_override_help`='1', `language_help`='help_consent_count' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='consent' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_help`='1', `language_help`='help_misc_identifier_count' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='miscidentifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_help`='1', `language_help`='help_aliquot_reserved_count' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_reserved' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_help`='1', `language_help`='help_inventory_action_count' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_help`='1', `language_help`='help_order_items_count' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='order_items' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT INTO `i18n` (`id`, `en`, `fr`)
VALUES
('participantStudiesReport', 'Participant Studies Report' , 'Rapport de projets des participants'),
('report participant studies', 'Participant Studies Report', 'Rapport de projets des Participant'),
('report for participant studies', 'Report for Participant studies', 'Report de projets des participants'),
('no search parameter chosen', 'No search parameter chosen', 'Aucun paramètre de recherche choisi'),
('count', 'Count', 'Décompte'),
('type of data', 'Type of Data', 'Type de données'),
('help_consent_count', 'Number of consent obtained  linked to the study', 'Nombre de consentement obtenu lié l\'étude'),
('help_misc_identifier_count', 'Number of identifiers linked to the study', 'Nombre d\'identifiant lié à l\'étude'),
('help_aliquot_reserved_count', 'Number of aliquot reserved related to the study', 'Nombre d\'aliquots réservés reliés à l\'étude'),
('help_inventory_action_count', 'Number of inventory actions on the aliquots related to the study', 'Nombre d\'actions d\'inventaire effectuées sur les aliquots reliés cette étude'),
('help_order_items_count', 'Number of order items related to the study', 'Nombre d\'articles dans les commandes reliées à cette étude');

-- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Studies Report
-- For ecah study, count number of Consent, Identifier, Action, Participant With Aliquot inventory action, Aliquot Reserved, Participant With Aliquot Reserved, Participant With Identifer/consent/aliquot, Orders, Order Items, Order Items Shipped.
-- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `datamart_reports` (`id`, `name`, `description`, `form_alias_for_search`, `form_alias_for_results`, `form_type_for_results`, `function`, `flag_active`, `associated_datamart_structure_id`, `limit_access_from_datamart_structure_function`)
VALUES
(null, 'studies report' , 'report for studies', 'studies_report_search', 'studies_report_result', 'index', 'studiesReport', 1, (SELECT id FROM datamart_structures WHERE model = 'StudySummary'), 0);

-- datamart_structure_functions
INSERT INTO `datamart_structure_functions` (`id`, `datamart_structure_id`, `label`, `link`, `flag_active`, `ref_single_fct_link`)
VALUES
(null, 
(SELECT id FROM datamart_structures WHERE model = 'StudySummary'), 
'studies report', CONCAT('/Datamart/Reports/manageReport/', (SELECT id FROM datamart_reports WHERE name = 'studies report')), 1, 0);

-- Studies Report Search from
INSERT INTO structures(`alias`) VALUES ('studies_report_search');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='studies_report_search'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='report_date_range' AND `type`='date' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='date range' AND `language_tag`=''), 
'0', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_search'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='study_name_list')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='' AND `language_label`='study_title' AND `language_tag`=''), 
'0', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- Studies Report Result Form
INSERT INTO structures(`alias`) VALUES ('studies_report_result');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'patient_inventory_action', 'input',  NULL , '0', 'size=10', '', '', 'patient inventory action', ''), 
('Datamart', '0', '', 'patient_aliquot_reserved', 'input',  NULL , '0', 'size=10', '', '', 'patient aliquot reserved', ''), 
('Datamart', '0', '', 'patient_misc_consent_aliquot', 'input',  NULL , '0', 'size=10', '', '', 'patient misc consent aliquot', ''), 
('Datamart', '0', '', 'orders', 'input',  NULL , '0', 'size=10', '', '', 'orders', ''), 
('Datamart', '0', '', 'order_items_shipped', 'input',  NULL , '0', 'size=10', '', '', 'order items shipped', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=10', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='consent' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=10', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='miscidentifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '15', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=10', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=10', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='patient_inventory_action' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='' AND `language_label`='patient inventory action' AND `language_tag`=''), 
'0', '25', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_reserved' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='' AND `language_label`='aliquot reserved' AND `language_tag`=''), 
'0', '30', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='patient_aliquot_reserved' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='' AND `language_label`='patient aliquot reserved' AND `language_tag`=''), 
'0', '35', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='patient_misc_consent_aliquot' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='' AND `language_label`='patient misc consent aliquot' AND `language_tag`=''), 
'0', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='orders' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='' AND `language_label`='orders' AND `language_tag`=''), 
'0', '45', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='order_items' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '50', '', '0', '1', 'order items', '0', '', '0', '', '0', '', '1', 'size=10', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='order_items_shipped' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='' AND `language_label`='order items shipped' AND `language_tag`=''), 
'0', '55', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
UPDATE structure_fields SET  `language_help`='help_patient_inventory_action_count' WHERE model='0' AND tablename='' AND field='patient_inventory_action' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_help`='help_patient_aliquot_reserved_count' WHERE model='0' AND tablename='' AND field='patient_aliquot_reserved' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_help`='help_patient_misc_consent_aliquot_count' WHERE model='0' AND tablename='' AND field='patient_misc_consent_aliquot' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_help`='help_orders_count' WHERE model='0' AND tablename='' AND field='orders' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_help`='help_order_items_shipped_count' WHERE model='0' AND tablename='' AND field='order_items_shipped' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `flag_override_help`='1', `language_help`='help_consent_count' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='consent' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_help`='1', `language_help`='help_misc_identifier_count' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='miscidentifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_help`='1', `language_help`='help_inventory_action_count' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_help`='1', `language_help`='help_aliquot_reserved_count' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_reserved' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_help`='1', `language_help`='help_order_items_count' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='order_items' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT INTO `i18n` (`id`, `en`, `fr`)
VALUES
('studiesReport', 'Studies Report', 'Rapport de projets'),
('studies report', 'Studies Report', 'Rapport de projets'),
('order items shipped', 'Order Items Shipped', 'Articles de Commande Envoyés'),
('report for studies', 'Report for studies', 'Rapport de projets'),
('aliquot reserved', 'Aliquot Reserved', 'aliquot reserved'),
('patient inventory action', 'Participant With Inventory Action', 'Participant avec Événement/annotation d\'inventaire sur aliquot'),
('patient aliquot reserved', 'Participant With Aliquot Reserved', 'Participant ayant des aliquots reservés'),
('patient misc consent aliquot', 'Participant With Identifer/consent/aliquot', 'Participant avec Indentifiant/Consent/Aliquot'),
('help_patient_inventory_action_count', 'Number of participants having event actions performed on the aliquots related to the study', 'Nombre de participants ayant des actions effectuées sur les aliquotes reliés à l\'étude'),
('help_patient_aliquot_reserved_count', 'Number of participants having aliquot reserved related to the study', 'Nombre de participants ayant des aliquots réservés relié à l\'étude'),
('help_patient_misc_consent_aliquot_count', 'Number of participants with consent(s), identifier(s) or aliquot(s) related to the study', 'Number of participants ayant un ou des consentement(s), identifiant(s) ou aliquot(s) reliés à l\'étude'),
('help_orders_count', 'Number of orders related to the study', 'Nombre de commandes reliées à l\'étude'),
('help_order_items_shipped_count', 'Number of order items shipped related to the study', 'Nombre d\'articles de commandes envoyées reliés à l\'étude');

UPDATE structure_formats SET `flag_override_setting`='1', `setting`='size=20,class=file range' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_search') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- Add 'total aliquot count' and rename 'action detail' to 'inventory action' 
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'total_study_aliquot', 'input',  NULL , '0', 'size=20', '', 'help_total_study_aliquot_count', 'total study aliquots', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='total_study_aliquot' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=20' AND `default`='' AND `language_help`='help_total_study_aliquot_count' AND `language_label`='total study aliquots' AND `language_tag`=''), 
'0', '45', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
UPDATE structure_formats SET `language_heading`='general' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `language_heading`='aliquots' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_reserved' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- studies_report_result : re-order and add heading
UPDATE structure_formats SET `language_heading`='general' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='40' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='30' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='patient_inventory_action' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='35', `language_heading`='aliquots' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_reserved' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='25' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='patient_aliquot_reserved' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='20', `language_heading`='participant aliquots'  WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='patient_misc_consent_aliquot' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `flag_override_label`='0', `language_label`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='order_items' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='total_study_aliquot' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=10', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');

UPDATE structure_formats SET `language_heading`='orders' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='orders' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='42' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='total_study_aliquot' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
INSERT INTO `i18n` (`id`, `en`, `fr`)
VALUES
('participant aliquots', 'Participant Aliquots', 'Aliquots de participants'),
('total study aliquots', 'Total Distinct Aliquots', 'Total aliquots distinct'),
('help_total_study_aliquot_count', 'Number of distinct aliquot related to the study through aliquot reserved, inventory actions or orders', 'Nombre d\'aliquotes distinct reliées à l\'étude par aliquot réservé, action/annotations d\'inventaire, commandes');

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/623): Studies Report">
-- -----------------------------
-- Update the study Report
-- -----------------------------
-- Participant Section
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'total_distinct_patient', 'input',  NULL , '0', 'size=10', '', 'help_patient_aliquot_reserved_count', 'total distinct patient', ''), 
('Datamart', '0', '', 'patient_order', 'input',  NULL , '0', 'size=10', '', 'help_patient_inventory_action_count', 'patient with order', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='total_distinct_patient' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='help_patient_aliquot_reserved_count' AND `language_label`='total distinct patient' AND `language_tag`=''), 
'0', '20', 'participant', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='patient_order' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='help_patient_inventory_action_count' AND `language_label`='patient with order' AND `language_tag`=''), 
'0', '33', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
-- delete structure_formats
DELETE FROM structure_formats WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `public_identifier`='' AND `plugin`='Datamart' AND `model`='0' AND `tablename`='' AND `field`='patient_misc_consent_aliquot' AND `language_label`='patient misc consent aliquot' AND `language_tag`='' AND `type`='input' AND `setting`='size=10' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='help_patient_misc_consent_aliquot_count' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');
-- Delete obsolete structure fields and validations
DELETE FROM structure_validations WHERE structure_field_id IN (SELECT id FROM structure_fields WHERE (`public_identifier`='' AND `plugin`='Datamart' AND `model`='0' AND `tablename`='' AND `field`='patient_misc_consent_aliquot' AND `language_label`='patient misc consent aliquot' AND `language_tag`='' AND `type`='input' AND `setting`='size=10' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='help_patient_misc_consent_aliquot_count' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0'));
DELETE FROM structure_fields WHERE (`public_identifier`='' AND `plugin`='Datamart' AND `model`='0' AND `tablename`='' AND `field`='patient_misc_consent_aliquot' AND `language_label`='patient misc consent aliquot' AND `language_tag`='' AND `type`='input' AND `setting`='size=10' AND `default`='' AND `structure_value_domain` IS NULL  AND `language_help`='help_patient_misc_consent_aliquot_count' AND `validation_control`='open' AND `value_domain_control`='open' AND `field_control`='open' AND `flag_confidential`='0' AND `sortable`='1' AND `flag_form_builder`='0' AND `flag_test_mode`='0' AND `flag_copy_from_form_builder`='0');

UPDATE structure_fields SET  `language_help`='help_total_distinct_patient_count' WHERE model='0' AND tablename='' AND field='total_distinct_patient' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_help`='help_patient_order_count' WHERE model='0' AND tablename='' AND field='patient_order' AND `type`='input' AND structure_value_domain  IS NULL ;

INSERT INTO i18n (id, en, fr) VALUES
('total distinct patient', 'Total Distinct Participant', 'Nombre total de participant distinct'),
('patient with order', 'Participant With Orders', 'Nombre de participant ayant des commandes reliées à l\'étude'),
('help_total_distinct_patient_count', 'Number of Participant With Aliquot Related to the Study', 'Nombre de participants avec aliquots relié à l\'étude'),
('help_patient_order_count', 'Number of participants having orders performed on the aliquots related to the study', 'Nombre de participants ayant des commandes effectuées sur les aliquotes reliés à l\'étude');

-- Aliquot Section
UPDATE structure_formats SET `display_order`='38', `language_heading`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_reserved' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='35', `language_heading`='aliquots' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='total_study_aliquot' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'aliquot_in_orders', 'input',  NULL , '0', 'size=10', '', 'help_aliquot_in_orders_count', 'aliquot in orders', ''), 
('Datamart', '0', '', 'aliquot_shipped', 'input',  NULL , '0', 'size=10', '', 'help_aliquot_shipped_count', 'aliquot shipped', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_in_orders' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='help_aliquot_in_orders_count' AND `language_label`='aliquot in orders' AND `language_tag`=''), 
'0', '42', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_shipped' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='help_aliquot_shipped_count' AND `language_label`='aliquot shipped' AND `language_tag`=''), 
'0', '43', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');

INSERT INTO i18n (id, en, fr) VALUES
('aliquot in orders', 'Aliquot in Orders', 'Aliquots dans les commandes'),
('aliquot shipped', 'Aliquot Shipped', 'Aliquots dans les commandes envoyés'),
('help_aliquot_in_orders_count', 'Number of Aliquot in Orders Related to the Study', 'Nombre d\'aliquots en commande relié à l\'étude'),
('help_aliquot_shipped_count', 'Number of Aliquot in Shipped Orders Related to the Study', 'Nombre d\'aliquots en commande envoyé relié à l\'étude');

-- Add Iventory Action Section
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Datamart', '0', '', 'aliquot_inventory_action', 'input',  NULL , '0', 'size=10', '', 'help_aliquot_inventory_action_count', 'aliquot inventory action', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='studies_report_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_inventory_action' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=10' AND `default`='' AND `language_help`='help_aliquot_inventory_action_count' AND `language_label`='aliquot inventory action' AND `language_tag`=''), 
'0', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
UPDATE structure_formats SET `display_order`='44' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `language_heading`='inventory action' WHERE structure_id=(SELECT id FROM structures WHERE alias='studies_report_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT INTO i18n (id, en, fr) VALUES 
('aliquot inventory action', 'Aliquot in Inventory Action', 'Aliquots dans les événement/annotation d\'inventaire'),
('help_aliquot_inventory_action_count', 'Number of Aliquot in Inventory Action Related to the Study', 'Nombre d\'aliquots dans les événement/annotation d\'inventaire relié à l\'étude');

UPDATE i18n SET en='Number of inventory actions related to the study', fr = 'Nombre d\'événements d\'inventaire reliés cette étude' WHERE id = 'help_inventory_action_count';


-- --------------------------------------------------
-- Update the Participant Vs Sutdy report Report
-- Same issue update as #623
-- --------------------------------------------------
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_inventory_action' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '36', 'aliquots', '0', '0', '', '0', '', '1', 'help_aliquot_reserved_count', '0', '', '1', 'size=20', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_in_orders' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '38', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=20', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_shipped' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=20', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='orders' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '46', 'orders', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=20', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='participant_studies_result'), 
(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='order_items_shipped' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '50', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=20', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
UPDATE structure_formats SET `display_order`='34', `language_heading`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_reserved' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='45', `language_heading`='inventory action' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='inventory_action' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='48' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='order_items' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='32' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='total_study_aliquot' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

UPDATE structure_formats SET `language_heading`='aliquots' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='total_study_aliquot' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `language_heading`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='participant_studies_result') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='0' AND `tablename`='' AND `field`='aliquot_inventory_action' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

-- </editor-fold>


-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/587): Add maintenance mode in ATiM">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Add maintenance mode in ATiM
-- ----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `users` ADD `dev_user` char(1) default '0';
-- </editor-fold>

-- <editor-fold desc="Fixed bug on wrong definition of a structure_fields.tablename pointing on ad_cell_tubes table instead ad_tubes">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	"Fixed bug on wrong definition of a structure_fields.tablename pointing on ad_cell_tubes table instead ad_tubes
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE structure_fields  SET tablename = 'ad_tubes' WHERE tablename = 'ad_cell_tubes';
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/578) : Aliquot to rename to aliquote in french">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Aliquot to rename to aliquote in french
-- ----------------------------------------------------------------------------------------------------------------------------------
REPLACE INTO `i18n` (`id`, `en`, `fr`) values
('aliquot inventory actions', 'Aliquot Events/Annotations', 'Événements/annotations d''aliquotes'),
('for more details and access the all aliquot events annotations list click %s',
'List limited to aliquot''s events/annotations and generic data only. For more details and access the all events/annotations list including storage history, realiquotings, derivatives, etc., click %s.',
'Liste limitée aux événements/annotations de l''aliquote et aux données génériques seulement. Pour plus de détails et accéder à toute la liste d''événements/annotations incluant l''historique d''entreposage, les réaliquotages, les dérivés, etc., cliquez %s.'),
('nbr of children by default can not be bigger than 20', 'The number of children aliquots by default can not be bigger than 20!', "Le nombre d'aliquotes enfants par défaut ne peut pas être supérieur à 20!");

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/604) : DataBrowser search Aliquot Use">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	DataBrowser search Aliquot Use
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('inventory_action_aliquot_sample_types', 'open', '', 'InventoryManagement.InventoryActionControl::getActiveAliquotSampleTypesPermissibleValues');

INSERT INTO structures(`alias`) VALUES ('inventory_action_aliquot_sample_masters');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_aliquot_sample_types') , '0', '', '', '', 'inventory action type', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'all_types', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_aliquot_sample_types') , '0', 'class=hidden,class=all-aliquot-type-selected', '', '', 'type', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='category' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_categories')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='inventory action category' AND `language_tag`=''),
     '0', '-2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_aliquot_sample_types')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='inventory action type' AND `language_tag`=''),
     '0', '-1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='Generated' AND `tablename`='' AND `field`='parent_info' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='parent information' AND `language_tag`=''),
     '0', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='title' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='-' AND `language_help`='' AND `language_label`='inventory_action_title' AND `language_tag`=''),
     '0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='date' AND `type`='datetime' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='the date that the inventory action done on aliquot/sample' AND `language_label`='Date' AND `language_tag`=''),
     '0', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='aliquot_master_name' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='aliquot_master_name_for_inventory_action' AND `language_tag`=''),
     '0', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '0', '9', '', '0', '1', 'study / project', '0', '', '0', '', '0', '', '1', 'size=40,placeholder=Study/Etude (if/si applicable)', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='person' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_staff')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='inventory_action_person_in_charge' AND `language_tag`=''),
     '0', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='notes' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='notes' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='inventory_action_control_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='inventory action control id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='sample_master_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='sample master id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='aliquot_master_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='aliquot master id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='initial_specimen_sample_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='initial specimen sample id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='study_summary_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='study summary id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='all_types' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_aliquot_sample_types')  AND `flag_confidential`='0' AND `setting`='class=hidden,class=all-aliquot-type-selected' AND `default`='' AND `language_help`='' AND `language_label`='type' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_sample_masters'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='CopyCtrl' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='copy control' AND `language_tag`=''),
     '100', '10000', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');


INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('inventory_action_aliquot_types', 'open', '', 'InventoryManagement.InventoryActionControl::getActiveAliquotTypesPermissibleValues');

INSERT INTO structures(`alias`) VALUES ('inventory_action_aliquot_masters');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_aliquot_types') , '0', '', '', '', 'inventory action type', ''),
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'all_types', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_aliquot_types') , '0', 'class=hidden,class=all-aliquot-type-selected', '', '', 'type', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='category' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_categories')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='inventory action category' AND `language_tag`=''),
     '0', '-2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_aliquot_types')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='inventory action type' AND `language_tag`=''),
     '0', '-1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='Generated' AND `tablename`='' AND `field`='parent_info' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='parent information' AND `language_tag`=''),
     '0', '1', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='title' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='-' AND `language_help`='' AND `language_label`='inventory_action_title' AND `language_tag`=''),
     '0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='date' AND `type`='datetime' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='the date that the inventory action done on aliquot/sample' AND `language_label`='Date' AND `language_tag`=''),
     '0', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '1', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='aliquot_master_name' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='aliquot_master_name_for_inventory_action' AND `language_tag`=''),
     '0', '4', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='StudySummary' AND `tablename`='study_summaries' AND `field`='title' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '0', '9', '', '0', '1', 'study / project', '0', '', '0', '', '0', '', '1', 'size=40,placeholder=Study/Etude (if/si applicable)', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='person' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_staff')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='inventory_action_person_in_charge' AND `language_tag`=''),
     '0', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='notes' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='notes' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='inventory_action_control_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='inventory action control id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='sample_master_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='sample master id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='aliquot_master_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='aliquot master id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='initial_specimen_sample_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='initial specimen sample id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionMaster' AND `tablename`='inventory_action_masters' AND `field`='study_summary_id' AND `type`='hidden' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='study summary id' AND `language_tag`=''),
     '0', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '0', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='all_types' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_aliquot_types')  AND `flag_confidential`='0' AND `setting`='class=hidden,class=all-aliquot-type-selected' AND `default`='' AND `language_help`='' AND `language_label`='type' AND `language_tag`=''),
     '0', '101', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='CopyCtrl' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='copy control' AND `language_tag`=''),
     '100', '10000', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0');


UPDATE `datamart_structures` SET `display_name` = 'inventory action (aliquot)', `options` = '{"applyOn":"aliquot"}', `structure_id` = (SELECT `id` FROM `structures` WHERE `alias` = 'inventory_action_aliquot_masters')  WHERE `model` = 'InventoryActionMaster';

INSERT INTO `datamart_structures` (`plugin`, `model`, `structure_id`, `adv_search_structure_alias`, `display_name`, `control_master_model`, `index_link`, `batch_edit_link`, `options`)
VALUES ('InventoryManagement', 'InventoryActionMaster', (SELECT `id` FROM `structures` WHERE `alias` = 'inventory_action_aliquot_sample_masters'), NULL, 'inventory action (sample)', 'InventoryActionMaster', '/InventoryManagement/InventoryActionMasters/detail/%%InventoryActionMaster.id%%/', '', '{"applyOn":"sample"}');

SET @datamart_structure_id = LAST_INSERT_ID();

UPDATE `datamart_browsing_controls` SET `id1` = @datamart_structure_id WHERE `id1` = (
        SELECT `id` FROM `datamart_structures` WHERE `model` = 'InventoryActionMaster' AND `options` = '{"applyOn":"aliquot"}'
    ) AND `id2` = (
        SELECT `id` FROM `datamart_structures` WHERE `model` = 'ViewSample'
    );

INSERT INTO `datamart_browsing_controls` (`id1`, `id2`, `flag_active_1_to_2`, `flag_active_2_to_1`, `use_field`) VALUES
(@datamart_structure_id, (SELECT `id` FROM `datamart_structures` WHERE `model` = 'StudySummary'), "1", "1", "study_summary_id");

-- <editor-fold desc="Define the structure to search Aliquot use">
INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('aliquot_use_definition_search', '', '', NULL);

INSERT INTO structures(`alias`) VALUES ('viewaliquotuses_search');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'ViewAliquotUse', 'view_aliquot_uses', 'use_definition', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='aliquot_use_definition_search') , '0', 'class=atim-multiple-choice', '', '', 'use and/or event', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='use_definition' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_use_definition_search')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='' AND `language_label`='use and/or event' AND `language_tag`=''),
     '0', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='use_code' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '0', '1', '', '0', '0', '', '1', ':', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='use_details' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='cols=50,rows=5' AND `default`='' AND `language_help`='' AND `language_label`='details' AND `language_tag`=''),
     '0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='study_summary_title' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=40' AND `default`='' AND `language_help`='' AND `language_label`='study / project' AND `language_tag`=''),
     '0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='used_volume' AND `type`='float_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=5' AND `default`='' AND `language_help`='' AND `language_label`='used volume' AND `language_tag`=''),
     '0', '3', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='aliquot_volume_unit' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_volume_unit')  AND `flag_confidential`='0'),
     '0', '5', '', '0', '1', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='use_datetime' AND `type`='datetime' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='inv_use_datetime_defintion' AND `language_label`='date' AND `language_tag`=''),
     '0', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='duration' AND `type`='integer_positive' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=6' AND `default`='' AND `language_help`='' AND `language_label`='duration' AND `language_tag`=''),
     '0', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='duration_unit' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='duration_unit')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='' AND `language_tag`=''),
     '0', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='used_by' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='custom_laboratory_staff')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='used by' AND `language_tag`=''),
     '0', '7', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'),
    ((SELECT id FROM structures WHERE alias='viewaliquotuses_search'),
     (SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='created' AND `type`='datetime' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_created' AND `language_label`='created (into the system)' AND `language_tag`=''),
     '0', '11', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');

UPDATE `structure_value_domains` SET override = 'open', category = '', source = 'InventoryManagement.ViewAliquotUse::getUseDefinitionsSearch'WHERE `domain_name`='aliquot_use_definition_search';

UPDATE `datamart_structures` SET `structure_id` = (SELECT `id` FROM `structures` WHERE `alias` = 'viewaliquotuses_search') WHERE `model` = 'ViewAliquotUse';
-- </editor-fold>


INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('InventoryManagement', 'InventoryActionControl', 'inventory_action_controls', 'aliquot_master_id_field', 'input',  NULL , '0', 'class=hidden,class=aliquot-master-id-not-null', '1', '', 'aliquot master id', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='inventory_action_aliquot_masters'),
     (SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='aliquot_master_id_field' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=hidden,class=aliquot-master-id-not-null' AND `default`='1' AND `language_help`='' AND `language_label`='aliquot master id' AND `language_tag`=''),
     '0', '101', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');


REPLACE INTO `i18n` (`id`, `en`, `fr`) VALUES
("inventory action (aliquot)", "Inventory Event/Annotation (Aliquot)", "Événement/annotation d'inventaire (Aliquote)"),
("aliquot use foot note", 
"* Aliquot Use Synthesis of certain events performed on the aliquots, such as realiquoting, ordering, derivatives creation and Inventory Events/Annotations.", 
"* La synthèse d''utilisation des aliquotes consiste en certains événements effectués sur les aliquotes, tels que du réaliquotage, les commandes, la création de dérivés et la création d'événements/annotations d'inventaire."),
("inventory action (sample)", "Inventory Event/Annotation (Sample)", "Événement/annotation d'inventaire (Echantillon)"),
("aliquot master id", "Aliquot Master ID", "Aliquot Master ID");
-- ("", "", ""),

UPDATE structure_formats SET `flag_override_setting`='1', `setting`='class=atim-multiple-choice' WHERE structure_id=(SELECT id FROM structures WHERE alias='viewaliquotuses') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquotUse' AND `tablename`='view_aliquot_uses' AND `field`='use_definition' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_use_definition') AND `flag_confidential`='0');

-- </editor-fold>

-- <editor-fold desc="Add new model to add one to many coding system different than ICD-10, ICD-O-3, mCode, etc. (https://gitlab.com/ctrnet/atim/-/issues/609)">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	TABLE coding_other_codes
-- ----------------------------------------------------------------------------------------------------------------------------------

DROP TABLE IF EXISTS coding_other_codes;
CREATE TABLE `coding_other_codes` (
  `value_set_name` varchar(50) NOT NULL DEFAULT '',
  `id` varchar(50) NOT NULL DEFAULT '',
  `en_description` text DEFAULT NULL,
  `fr_description` text DEFAULT NULL,
  `source_code_system` varchar(50) NOT NULL DEFAULT '',
  `source_publisher` varchar(250) DEFAULT NULL,
  `source_title` varchar(250) DEFAULT NULL,
  `source_version` varchar(250) DEFAULT NULL,
  `source_url` varchar(500) DEFAULT NULL,
  `inserted` date DEFAULT NULL,
  KEY (`id`),
  FULLTEXT KEY `en_description` (`en_description`),
  FULLTEXT KEY `fr_description` (`fr_description`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT IGNORE INTO i18n (id,en,fr) VALUES
('search for a %s code','Search For \'%s\' Code','Recherche d\'un code \'%s\''),
('%s code picker','\'%s\' Code Picker','Sélection code \'%s\''),
('select a %s code', 'Select a \'%s\' Code','Sélectioner code \'%s\''),
('invalid other_code code', 'Invalid code', 'Code invalide.');

-- Example below:
-- Script to create either a search on Martial Status using the Coding search tool or using a drop down list

-- CodingOtherCodes ex:         INSERT INTO coding_other_codes (value_set_name, id, en_description, fr_description, source_code_system, source_publisher, source_title, source_version, source_url, inserted) 
-- CodingOtherCodes ex:         VALUES
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'A', 'Annulled', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW()),
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'I', 'Interlocutory', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW()),
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'L', 'Legally', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW()),
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'M', 'Married', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW()),
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'C', 'Common Law', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW()),
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'P', 'Polygamous', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW()),
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'T', 'Domestic partner', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW()),
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'U', 'Unmarried', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW()),
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'S', 'Never Married', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW()),
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'W', 'Widowed', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW()),
-- CodingOtherCodes ex:         ('hl7_fihr_MaritalStatus', 'UNK', 'unknown', "", "HL7 FHIR",  "HL7 International Clinical Interoperability Council",  "MaritalStatus",  "4.3.0",  "https://www.hl7.org/fhir/valueset-marital-status.html",  NOW());

-- Code selection tool

-- CodingOtherCodes ex:         ALTER TABLE participants
-- CodingOtherCodes ex:            ADD COLUMN marital_status_code varchar(50) default NULL AFTER sex;
-- CodingOtherCodes ex:         ALTER TABLE participants_revs
-- CodingOtherCodes ex:            ADD COLUMN marital_status_code varchar(50) default NULL AFTER sex;
-- CodingOtherCodes ex:         
-- CodingOtherCodes ex:         INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
-- CodingOtherCodes ex:         VALUES
-- CodingOtherCodes ex:         ('ClinicalAnnotation', 'Participant', 'participants', 'marital_status_code', 'autocomplete', NULL , '0', 'size=10,url=/CodingIcd/CodingOtherCodes/autocomplete/hl7_fihr_MaritalStatus,tool=/CodingIcd/CodingOtherCodes/tool/hl7_fihr_MaritalStatus', '', 'help_other_code_marital_status', 'marital status', '');
-- CodingOtherCodes ex:         INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
-- CodingOtherCodes ex:         VALUES
-- CodingOtherCodes ex:         ((SELECT id FROM structures WHERE alias='participants'), 
-- CodingOtherCodes ex:         (SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='marital_status_code' AND `type`='autocomplete'), 
-- CodingOtherCodes ex:         '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
-- CodingOtherCodes ex:         INSERT INTO structure_validations (structure_field_id, rule, language_message)
-- CodingOtherCodes ex:         VALUES
-- CodingOtherCodes ex:         ((SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='marital_status_code' AND `type`='autocomplete'),
-- CodingOtherCodes ex:         'validateOtherCode,hl7_fihr_MaritalStatus', 'invalid other_code code');

-- CodingOtherCodes ex:         INSERT IGNORE INTO i18n (id,en,fr)
-- CodingOtherCodes ex:         VALUES
-- CodingOtherCodes ex:         ('marital status', 'Marital Status', 'État civil'),
-- CodingOtherCodes ex:         ('help_other_code_marital_status',
-- CodingOtherCodes ex:         "A code for the person's Marital Statu as defined by the HL7 FHIR .
-- CodingOtherCodes ex:         Based on the MaritalStatus code version 4.3.0 of the HL7 FHIR standard (https://www.hl7.org/fhir/valueset-marital-status.html).", 
-- CodingOtherCodes ex:         "Un code pour définir l'éthnicité de la personne telle que définie par HL7 FHIR .
-- CodingOtherCodes ex:         Basé sur la version 4.3.0 du code MaritalStatus de la norme FHIR HL7 (https://www.hl7.org/fhir/valueset-marital-status.html).");

-- Select field

-- CodingOtherCodes ex:         INSERT INTO structure_value_domains (domain_name, override, category, source) 
-- CodingOtherCodes ex:         VALUES 
-- CodingOtherCodes ex:         ("hl7_fihr_value_MaritalStatus", "", "", "CodingIcd.CodingOtherCode::getValueSetDetails(\'hl7_fihr_MaritalStatus\')");

-- CodingOtherCodes ex:         ALTER TABLE participants
-- CodingOtherCodes ex:            ADD COLUMN marital_status_code_2 varchar(50) default NULL AFTER sex;
-- CodingOtherCodes ex:         ALTER TABLE participants_revs
-- CodingOtherCodes ex:            ADD COLUMN marital_status_code_2 varchar(50) default NULL AFTER sex;

-- CodingOtherCodes ex:         INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
-- CodingOtherCodes ex:         VALUES
-- CodingOtherCodes ex:         ('ClinicalAnnotation', 'Participant', 'participants', 'marital_status_code_2', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='hl7_fihr_value_MaritalStatus') , '0', '', '', 'help_other_code_marital_status', 'marital status', '');
-- CodingOtherCodes ex:         INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
-- CodingOtherCodes ex:         VALUES
-- CodingOtherCodes ex:         ((SELECT id FROM structures WHERE alias='participants'), 
-- CodingOtherCodes ex:         (SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='marital_status_code_2' AND `type`='select'), 
-- CodingOtherCodes ex:         '1', '6', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/608) : Add RXNorm standard drug naming">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Add RXNorm standard drug naming
-- ----------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE `rx_norm_drugs`
(
    `id`        int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name`      varchar(512) NOT NULL,
    `rxnorm_id` int(11) NOT NULL DEFAULT '-1'
);

ALTER TABLE `rx_norm_drugs`
    ADD UNIQUE `rx_norm_drugs_name` (`name`),
    ADD INDEX `rx_norm_drugs_rxnorm_id` (`rxnorm_id`);

CREATE TABLE `rx_norm_versions`
(
    `id`        int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `version`     varchar(64) NOT NULL,
    `api_version` varchar(64) NOT NULL
);

-- Add the rx_Norm_id to the form
UPDATE structure_formats SET `display_order`='10' WHERE structure_id=(SELECT id FROM structures WHERE alias='drugs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Drug' AND `tablename`='drugs' AND `field`='trade_name' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='20', `flag_override_setting`='1', `setting`='size=40,url=/Drug/Drugs/autoCompleteRxNormDrug,class=rxnorm_generic_name', `type` = 'autocomplete' WHERE structure_id=(SELECT id FROM structures WHERE alias='drugs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Drug' AND `tablename`='drugs' AND `field`='generic_name' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='30' WHERE structure_id=(SELECT id FROM structures WHERE alias='drugs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Drug' AND `tablename`='drugs' AND `field`='type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='type') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='40' WHERE structure_id=(SELECT id FROM structures WHERE alias='drugs') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='Drug' AND `tablename`='drugs' AND `field`='description' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
    ('Drug', 'Drug', 'drugs', 'rxnorm_id', 'input',  NULL , '0', 'class=rxnorm_id', '', '', 'rxnorm_id', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='drugs'),
     (SELECT id FROM structure_fields WHERE `model`='Drug' AND `tablename`='drugs' AND `field`='rxnorm_id' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=rxnorm_id' AND `default`='' AND `language_help`='' AND `language_label`='rxnorm_id' AND `language_tag`=''),
     '1', '25', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0');

ALTER TABLE `drugs`
    ADD `rxnorm_id` int(11) NULL AFTER `generic_name`;

INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES ((SELECT `id` FROM `structure_fields` WHERE `model` = 'Drug' AND `tablename` = 'drugs' AND `field` = 'generic_name' AND `type` = 'input' AND `structure_value_domain` IS NULL), 'isUnique', '');

INSERT IGNORE `i18n` (`id`, `en`, `fr`) VALUES
("generic name is not part of the rxnorm library - either add the right one or remove rx norm id",
"Generic name is not part of the RxNorm library. Please enter the right generic name or erase the Rx Norm Id to record the generic name as is.",
"Le nom générique ne fait pas partie de la bibliothèque RxNorm. Veuillez entrer le bon nom générique ou effacer le RxNorm ID pour enregistrer le nom générique tel quel."),
("the rx number is invalid", "Generic name is part of the RxNorm library but the RxNorm ID is either invalid or not completed.",
"Le nom générique fait partie du librairie RxNorm mais le RxNorm ID est invalide ou incomplet."),
("error in connecting to the rx_norm API", "Error in connecting to the RxNorm API. Check the Internet connection or contact the administrator.", "Erreur de connexion à l'API RxNorm. Vérifiez la connexion Internet ou contactez l'administrateur."),
("rxnorm_id", "RxNorm ID", "ID RxNorm"),
("error loading rx norm id", "Error in loading RxNorm ID. Check the Internet connection or contact the administrator.", "Erreur lors du chargement de l'ID de RxNorm. Vérifiez la connexion Internet ou contactez l'administrateur."),
("rx norm id not found", "RxNorm ID not found", "ID RxNorm introuvable"),
("for the drug [%s] you need to give %s [%s]", "For the drug [%s] you need to give %s [%s]", "pour le médicament [%s] vous devez donner %s [%s]"),
("the drug [%s] already exists", "The drug [%s] already exists", "Le médicament [%s] existe déjà");

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/611) : Change blood_type to variable list">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Change blood_type to variable list
-- ----------------------------------------------------------------------------------------------------------------------------------

SELECT "ERROR: Blood type field is already changed to a custom list on your local instance. Comment statements created to 'fix the issue (https://gitlab.com/ctrnet/atim/-/issues/611) : Change blood_type to variable list'."
FROM structure_value_domains WHERE domain_name = 'blood_type' AND source LIKE 'StructurePermissibleValuesCustom::getCustomDropdown%';

INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category) 
VALUES
('Blood Type', 1, 30, '');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Blood Type');
SET @user_id = 2;
SET @strid = (SELECT id FROM structure_value_domains WHERE domain_name = 'blood_type');
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
(SELECT val.value, en,fr, "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id
FROM structure_value_domains_permissible_values link
INNER JOIN structure_permissible_values val ON val.id = structure_permissible_value_id
LEFT JOIN i18n ON i18n.id = val.value
WHERE structure_value_domain_id = @strid
AND link.flag_active = 1);
UPDATE structure_value_domains SET source = 'StructurePermissibleValuesCustom::getCustomDropdown(\'Blood Type\')' WHERE domain_name = 'blood_type';
SET @id = (SELECT id FROM structure_value_domains WHERE domain_name = 'blood_type');
UPDATE structure_value_domains_permissible_values SET flag_active = 0 WHERE structure_value_domain_id = @id;

-- </editor-fold>

-- <editor-fold desc="New sample type : Other Fluid">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	New sample type : New sample type : Other Fluid
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order)
VALUES
(null, 'other fluid', 'specimen', 'sd_spe_other_fluids,specimens', 'sd_spe_other_fluids', 0);
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, null, (SELECT id FROM sample_controls WHERE sample_type = 'other fluid'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'other fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'other fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL);
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'other fluid'), 'tube', '', 'ad_spec_tubes_incl_ml_vol', 'ad_tubes', 'ml', 0, '', 0);

CREATE TABLE `sd_spe_other_fluids` (
  `sample_master_id` int(11) NOT NULL,
  `fluid_description` varchar(250) DEFAULT NULL,
  KEY `FK_sd_spe_other_fluids_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_spe_other_fluids_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_spe_other_fluids_revs` (
  `sample_master_id` int(11) NOT NULL,
  `fluid_description` varchar(250) DEFAULT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('other_fluids_types', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Other Fluids Types\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('Other Fluids Types', 1, 250, 'inventory');
SET @control_id = (SELECT id FROM structure_permissible_values_custom_controls WHERE name = 'Other Fluids Types');
SET @user_id = 2;
INSERT INTO structure_permissible_values_customs (`value`, `en`, `fr`, `display_order`, `use_as_input`, `control_id`, `modified`, `created`, `created_by`, `modified_by`) 
VALUES
("AMN", "Amniotic fluid", "liquide amniotique", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("BIFL", "Bile Fluid", "Liquide biliaire", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("CSF", "Cerebral spinal fluid", "Liquide céphalorachidien", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("CVM", "Cervical Mucus", "Glaire cervicale", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("DUFL", "Duodenal fluid", "liquide duodénal", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("FGA", "Fluid, Abdomen", "Liquide, Abdomen", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("HYDC", "Fluid, Hydrocele", "Fluide, Hydrocèle", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("JNTFLD", "Fluid, Joint", "Fluide, Articulation", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("KIDFLD", "Fluid, Kidney", "Liquide, Rein", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("LSAC", "Fluid, Lumbar Sac", "Liquide, sac lombaire", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("PAFL", "Pancreatic fluid", "liquide pancréatique", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("PCFL", "Fluid, Pericardial", "Liquide, péricardique", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("SMN", "Seminal fluid", "liquide séminal", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("SNV", "Fluid, synovial (Joint fluid)", "Liquide synovial (liquide articulaire)", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("SPT", "Sputum", "Expectorations", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id),
("VITF", "Vitreous Fluid", "", "1", "1", @control_id, NOW(), NOW(), @user_id, @user_id);

INSERT INTO structures(`alias`) VALUES ('sd_spe_other_fluids');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'SampleDetail', 'sd_spe_other_fluids', 'fluid_description', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='other_fluids_types') , '0', '', '', '', 'other fluid type', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sd_spe_other_fluids'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_other_fluids' AND `field`='fluid_description' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='other_fluids_types')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='other fluid type' AND `language_tag`=''), 
'1', '441', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');
INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
('other fluid type', 'Fluid Type', 'Type de liquide'),
('other fluid', 'Other Fluid', 'Autre liquide');

-- </editor-fold>

-- New sample type : 

-- <editor-fold desc="New sample type : Bone">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	New sample type : Bone
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order)
VALUES
(null, 'bone', 'specimen', 'sd_spe_bones,specimens', 'sd_spe_bones', 0);
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, null, (SELECT id FROM sample_controls WHERE sample_type = 'bone'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL);
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'bone'), 'tube', '', 'ad_spec_tubes', 'ad_tubes', '', 0, '', 0);

CREATE TABLE `sd_spe_bones` (
  `sample_master_id` int(11) NOT NULL,
  `bone_name` varchar(250) DEFAULT NULL,
  KEY `FK_sd_spe_bones_sample_masters` (`sample_master_id`),
  CONSTRAINT `FK_sd_spe_bones_sample_masters` FOREIGN KEY (`sample_master_id`) REFERENCES `sample_masters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `sd_spe_bones_revs` (
  `sample_master_id` int(11) NOT NULL,
  `bone_name` varchar(250) DEFAULT NULL,
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_created` datetime NOT NULL,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO structure_value_domains (domain_name, override, category, source)
VALUES
('bones_names', 'open', '', 'StructurePermissibleValuesCustom::getCustomDropdown(\'Bones Names\')');
INSERT INTO structure_permissible_values_custom_controls (name, flag_active, values_max_length, category)
VALUES
('Bones Names', 1, 250, 'invetory');

INSERT INTO structures(`alias`) VALUES ('sd_spe_bones');
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('InventoryManagement', 'SampleDetail', 'sd_spe_bones', 'bone_name', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='bones_names') , '0', '', '', '', 'bone name', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sd_spe_bones'), 
(SELECT id FROM structure_fields WHERE `model`='SampleDetail' AND `tablename`='sd_spe_bones' AND `field`='bone_name' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='bones_names')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='bone name' AND `language_tag`=''), 
'1', '441', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '0', '0', '1', '1', '1', '0');
INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
('bone name', 'Bone Name', 'Nom de l''os'),
('bone', 'Bone', 'Os');

-- </editor-fold>

-- <editor-fold desc="New parent to children sample types relations">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	New parent to children sample types relations
--      - amniotic fluid to dna/rna
--      - skin swab to dna/rna
--      - saliva to rna
--      - vaginal swab to rna
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'amniotic fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'amniotic fluid'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),

(null, (SELECT id FROM sample_controls WHERE sample_type = 'skin swab'), (SELECT id FROM sample_controls WHERE sample_type = 'dna'), 0, NULL),
(null, (SELECT id FROM sample_controls WHERE sample_type = 'skin swab'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),

(null, (SELECT id FROM sample_controls WHERE sample_type = 'saliva'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL),

(null, (SELECT id FROM sample_controls WHERE sample_type = 'vaginal swab'), (SELECT id FROM sample_controls WHERE sample_type = 'rna'), 0, NULL);

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/612): Download multi CSV from Report and Data Browser">
ALTER TABLE `datamart_reports`
ADD `options` varchar(255) COLLATE 'latin1_swedish_ci' NULL;

INSERT INTO `datamart_reports` (`name`, `description`, `form_alias_for_search`, `form_alias_for_results`, `form_type_for_results`, `function`, `flag_active`, `associated_datamart_structure_id`, `limit_access_from_datamart_structure_function`, `options`)
VALUES ('export diagnosis and consent of participant', 'export_diagnosis_and_consent_of_participant', 'participants_for_report', '', 'index', 'exportDiagnosisAndConsentOfParticipant', '0', '4', '0', 'multi-csv=true');

SET @report_id = LAST_INSERT_ID();

INSERT IGNORE INTO `i18n` (`id`, `en`, `fr`) VALUES
("export diagnosis and consent of participant", "Export diagnosis and consent of participant", "Diagnostic export et consentement du participant"),
("export_diagnosis_and_consent_of_participant", "Export list of the diagnosises and the consents of participant", "Exporter la liste des diagnostics et des consentements des participants"),
("can not create the csv directory, contact the administrator", "There is some problem in csv file creation, contact the administrator", "Il y a un problème dans la création du fichier csv, contactez l'administrateur");


INSERT INTO `datamart_structure_functions` (`datamart_structure_id`, `label`, `link`, `flag_active`, `ref_single_fct_link`, `options`) VALUES
((SELECT `id` FROM `datamart_structures` WHERE `model` = 'Participant'), 'export diagnosis and consent of participant', CONCAT('/Datamart/Reports/manageReport/', @report_id), '0', '', 'multi-csv=true'),
((SELECT `id` FROM `datamart_structures` WHERE `model` = 'MiscIdentifier'), 'export diagnosis and consent of participant', CONCAT('/Datamart/Reports/manageReport/', @report_id), '0', '', 'multi-csv=true');

INSERT INTO structures(`alias`) VALUES ('participants_for_report');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='participants_for_report'), 
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '-1', 'clin_demographics', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=20,class=file range', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='participants_for_report'), 
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='created' AND `type`='datetime' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_created' AND `language_label`='created (into the system)' AND `language_tag`=''), 
'3', '99', 'system data', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='participants_for_report'), 
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='last_modification' AND `type`='datetime' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='help_part_last_mod' AND `language_label`='last modification' AND `language_tag`=''), 
'3', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');

INSERT INTO structures(`alias`) VALUES ('participant_identifiers_for_report');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='participant_identifiers_for_report'), 
(SELECT id FROM structure_fields WHERE `model`='Participant' AND `tablename`='participants' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '-1', '', '0', '0', '', '0', '', '0', '', '0', '', '1', 'size=20,class=file range', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0');
-- </editor-fold>

-- <editor-fold desc="Added core variable to activate/disable the collection protocol feature">

INSERT IGNORE INTO `i18n` (`id`, `en`, `fr`) VALUES
("system has activated the collection protocol feature", "The system has activated the collection protocol feature.", "Le système a activé la fonction de protocole de collection."),
("system has disabled the collection protocol feature", "The system has disabled the collection protocol feature.", "Le système a désactivé la fonction de protocole de collection.");

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/613): Add Inventory Configuration in ATiM">
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/613): Add Inventory Configuration in ATiM
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `menus` (`id`, `parent_id`, `is_root`, `display_order`, `language_title`, `language_description`, `use_link`, `use_summary`, `flag_active`, `flag_submenu`) VALUES
("core_CAN_41_sample", "core_CAN_41", "0", "12", "inventory configuration", null, "/Administrate/InventoryConfigurations/search", "", 1, 1);

INSERT IGNORE INTO `i18n` (`id`, `en`, `fr`) VALUES
("inventory configuration", "Inventory Configuration", "Configuration de l'inventaire"),
("to add a new relation, search the sample and assigne the related chidren into it sss", 
"To add a new relation (either derivative type or aliquot type) to an existing sample type, search the sample type and assign the related type into it.", 
"Pour ajouter une nouvelle relation (type de dérivé ou type aliquote) à un type d'échantillon existant, recherchez le type d'échantillon et affectez-lui le type associé."),

('sample types', 'Sample Types', "Types d'échantillons"),
('sample type', 'Sample Type', "Type d'échantillon"),
("derivatives", "Derivatives", "Dérivés"),
('derivative samples', 'Derivative Samples', 'Échantillons dérivés'),
('parent_samples', 'Parent Samples', 'Échantillons parents'),
('aliquot types', 'Aliquot Types', 'Types d''aliquotes'),
('derivatives samples (unchangageable-readonly)', 'Unchangageable', "Non modifiables"),
('derivatives samples (changageable-not readonly)', 'Changageable (Add/Remove)', "Modifiables (Ajouter/Supprimer)"),
('help derivatives samples (unchangageable-readonly)', 
'Derivative types already created from the managed sample type (Sample Type field) in at least one ATiM collection. Data can not be removed anymore.',
"Types de dérivés déjà créés à partir du type d'échantillon configuré (champ type d'échantillon) dans au moins une collection d'ATiM . Les données ne peuvent plus être supprimées."),
('help derivatives samples (changageable-not readonly)', 
'Derivative types never createdin ATiM from the managed sample type (Sample Type field). Data can be modified.',
"Types de dérivés jamais créés dans ATiM à partir du type d'échantillon configuré (champ type d'échantillon). Les données peuvent être modifiées."),
('aliquot types (unchangeable-readonly)', 'Unchangageable', "Non modifiables"),
('aliquot types (changageable-not readonly)', 'Changageable (Add/Remove)', "Modifiables (Ajouter/Supprimer)"),
('help aliquot types (unchangeable-readonly)',
'Aliquot types already created for the managed sample type (Sample Type field) in at least one ATiM collection. Data can not be removed anymore.',
"Types d'aliquotes déjà créés pour le type d'échantillon configuré (champ type d'échantillon) dans au moins une collection d'ATiM . Les données ne peuvent plus être supprimées."),
('help aliquot types (changageable-not readonly)',
'Aliquot types never created in ATiM for the managed sample type (Sample Type field). Data can be modified.',
"Types d'aliquotes jamais créés dans ATiM pour le type d'échantillon configuré (champ type d'échantillon). Les données peuvent être modifiées."),
("deactivate the sample (%s)", "Deactivate the sample (%s)", "Désactiver l'échantillon (%s)"),
("activate the sample (%s)", "Activate the sample (%s)", "activer l'échantillon (%s)"),
("the sample (%s) is deactivated", "The sample (%s) is deactivated", "L'échantillon (%s) est désactivé"),
("this sample (%s) has already some derivatives", "This sample (%s) has already some derivatives", "Cet échantillon (%s) a déjà quelques dérivées"),
("the sample (%s) is activated", "The sample (%s) is activated", "L'échantillon (%s) est activé"),
("cannot change the status of the sample (%s)", "Cannot change the status of the sample (%s)", "Impossible de modifier le statut de l'échantillon (%s)"),
("the sample types have already derivated for %s [%s]", "The sample types have already been defined as derivative for %s [%s]", "Les types d''échantillons ont déjà été définis comme dérivés pour %s [%s]");
-- ("", "", ""),


INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('derivative_type', 'open', '', 'InventoryManagement.SampleControl::getAllDerivatedTypePermissibleValues');
INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('sample_type_all', 'open', '', 'InventoryManagement.SampleControl::getAllSampleType');
INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('derivative_sample_type', 'open', '', 'InventoryManagement.SampleControl::getDerivativeSampleType');

INSERT INTO structures(`alias`) VALUES ('sample_controls_management');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
('InventoryManagement', 'SampleControl', 'sample_controls', 'sample_type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='sample_type_all') , '0', 'class=atim-multiple-choice', '', '', 'sample type', ''),
('Core', 'FunctionManagement', '', 'derivatived', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='derivative_sample_type') , '0', 'class=atim-multiple-choice', '', 'help derivatives samples (unchangageable-readonly)', 'derivatives samples (unchangageable-readonly)', ''), 
('Core', 'FunctionManagement', '', 'derivate', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='derivative_sample_type') , '0', 'class=atim-multiple-choice', '', '', 'derivative samples', ''), 
('Core', 'FunctionManagement', '', 'to_derivate', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='derivative_sample_type') , '0', 'class=atim-multiple-choice', '', 'help derivatives samples (changageable-not readonly)', 'derivatives samples (changageable-not readonly)', ''), 
('Core', 'FunctionManagement', '', 'parent', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='sample_type') , '0', 'class=atim-multiple-choice', '', '', 'parent_samples', ''), 
('Core', 'FunctionManagement', '', 'aliquot_type_read_only', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type') , '0', 'class=atim-multiple-choice', '', 'help aliquot types (unchangeable-readonly)', 'aliquot types (unchangeable-readonly)', ''), 
('Core', 'FunctionManagement', '', 'aliquot_type', 'select', (SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type') , '0', 'class=atim-multiple-choice', '', '', 'aliquot types', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sample_controls_management'), 
(SELECT id FROM structure_fields WHERE `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='sample_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_type_all')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='' AND `language_label`='sample type' AND `language_tag`=''), 
'1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='sample_controls_management'), 
(SELECT id FROM structure_fields WHERE `model`='SampleControl' AND `tablename`='sample_controls' AND `field`='sample_category' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_category')  AND `flag_confidential`='0'), 
'1', '20', '', '0', '0', '', '0', '', '1', '', '0', '', '1', 'class=atim-multiple-choice', '0', '', '0', '0', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='sample_controls_management'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='derivatived' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='derivative_sample_type')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='help derivatives samples (unchangageable-readonly)' AND `language_label`='derivatives samples (unchangageable-readonly)' AND `language_tag`=''), 
'1', '30', 'derivatives', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='sample_controls_management'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='derivate' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='derivative_sample_type')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='' AND `language_label`='derivative samples' AND `language_tag`=''), 
'1', '30', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0'), 
((SELECT id FROM structures WHERE alias='sample_controls_management'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='to_derivate' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='derivative_sample_type')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='help derivatives samples (changageable-not readonly)' AND `language_label`='derivatives samples (changageable-not readonly)' AND `language_tag`=''), 
'1', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='sample_controls_management'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='parent' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='sample_type')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='' AND `language_label`='parent_samples' AND `language_tag`=''), 
'1', '50', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='sample_controls_management'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='aliquot_type_read_only' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='help aliquot types (unchangeable-readonly)' AND `language_label`='aliquot types (unchangeable-readonly)' AND `language_tag`=''), 
'1', '55', 'Aliquot', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='sample_controls_management'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='aliquot_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type')  AND `flag_confidential`='0' AND `setting`='class=atim-multiple-choice' AND `default`='' AND `language_help`='' AND `language_label`='aliquot types' AND `language_tag`=''), 
'1', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='sample_controls_management'), 
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='aliquot_type' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='aliquot_type')  AND `flag_confidential`='0'), 
'1', '60', '', '0', '1', 'aliquot types (changageable-not readonly)', '0', '', '1', 'help aliquot types (changageable-not readonly)', '0', '', '0', '', '0', '', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

-- </editor-fold>

-- <editor-fold desc="Force InventoryActionMasters menu to be active to display storage history">
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Force InventoryActionMasters menu to be active to display storage history
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE `menus`SET flag_active = 1 WHERE `use_link` LIKE '%InventoryActionMasters%';
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/610): Batch Upload Feature : Unable to upload special character [Š]....">
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/610): Batch Upload Feature : Unable to upload special character [Š]....
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO `i18n` (`id`, `en`, `fr`) VALUES
('invalid characters in headers', 'Invalid characters in headers', 'Caractères non valides pour le nom d''un champ'),
("See field label [%s] with pattern '**?**' replacing wrong character(s)", 
"Change header by correcting pattern '**?**' that indicating wrong character(s) in following header [%s].", 
"Modifiez le nom du champ en corrigeant le motif '**?**' indiquant le(s) caractère(s) erroné(s) dans le champ suivant [%s]."
),

('invalid characters for the value of field "%s"', 'Invalid characters for value(s) of field "%s"', 'Caractères non valides pour des valeurs du champ "%s"'),
("See value [%s] with pattern '**?**' replacing wrong character(s) at line [%s]", 
"Change data by correcting pattern '**?**' that indicating wrong character(s) in value [%s] at line [%s].", 
"Modifiez les données en corrigeant le motif '**?**' indiquant le(s) caractère(s) erroné(s) dans la valeur [%s] à la ligne [%s]."
),
("there are some wrong characters at line [%s]", "There are some wrong characters at line [%s]", "Il y a des caractères erronés à la ligne [%s]");
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/614): Add Two Factor Authentication (TFA / 2FA) to ATiM">
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/614): Add Two Factor Authentication (TFA / 2FA) to ATiM
-- ----------------------------------------------------------------------------------------------------------------------------------
    -- <editor-fold desc="Modify The users table">
ALTER TABLE `users`
ADD `secret_key` varchar(100) NULL DEFAULT '',
ADD `is_super_admin` char(1) NULL DEFAULT '0',
ADD `force_tfa_show` char(1) NULL DEFAULT '1';

ALTER TABLE `users_revs`
ADD `secret_key` varchar(100) NULL DEFAULT '',
ADD `is_super_admin` char(1) NULL DEFAULT '0',
ADD `force_tfa_show` char(1) NULL DEFAULT '1';
    -- </editor-fold>

    -- <editor-fold desc="QR Code Form">

INSERT INTO structures(`alias`) VALUES ('user_tfa');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
('Core', 'FunctionManagement', '', 'qrcode', 'button',  NULL , '0', 'class=request-qr-code-tfa', '', 'help_tfa_secret_qr_code', 'tfa_secret_qr_code', ''),
('Core', 'FunctionManagement', '', 'name', 'input',  NULL , '0', '', '', '', 'user name(s)', ''),
('Core', 'FunctionManagement', '', 'secret_key', 'button',  NULL , '0', 'class=request-secret-code-tfa', '', 'help_tfa_secret_url', 'tfa_secret_url', ''),
('Core', 'FunctionManagement', '', 'validation_code', 'password',  NULL , '0', '', '', 'help_tfa_one_time_password', 'tfa_one_time_password', ''),
('Core', 'FunctionManagement', '', 'agreement', 'checkbox',  NULL , '0', '', '', '', 'agree_to_keep_secret_tfa_code_and_url', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
((SELECT id FROM structures WHERE alias='user_tfa'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='qrcode' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=request-qr-code-tfa' AND `default`=''),
'1', '10', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='user_tfa'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='name' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`=''),
'1', '20', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='user_tfa'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='secret_key' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='class=request-secret-code-tfa' AND `default`='' ),
'1', '30', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='user_tfa'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='validation_code' AND `type`='password' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`=''),
'1', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'),
((SELECT id FROM structures WHERE alias='user_tfa'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='agreement' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' ),
'1', '50', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');

INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES ((SELECT `id` FROM `structure_fields` WHERE `model` = 'FunctionManagement' AND `tablename` = '' AND `field` = 'validation_code' AND `type` = 'password' AND `structure_value_domain` IS NULL), 'notBlank', '');
INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES ((SELECT `id` FROM `structure_fields` WHERE `model` = 'FunctionManagement' AND `tablename` = '' AND `field` = 'agreement' AND `type` = 'checkbox' AND `structure_value_domain` IS NULL), 'notBlank', '');

    -- </editor-fold>

    -- <editor-fold desc="TFA Validation form">
INSERT INTO structures(`alias`) VALUES ('user_tfa_login');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
((SELECT id FROM structures WHERE alias='user_tfa_login'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='validation_code' AND `type`='password' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`=''),
'1', '40', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
    -- </editor-fold>

    -- <editor-fold desc="user_tfa_qr_code">
INSERT INTO structures(`alias`) VALUES ('user_tfa_qr_code');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
((SELECT id FROM structures WHERE alias='user_tfa_qr_code'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='qrcode' AND `type`='button' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
'1', '100', '', '0', '0', '', '0', '', '0', '', '0', '', '1', '', '0', 'class=request-qr-code-tfa', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0');
    -- </editor-fold>

    -- <editor-fold desc="Dictionary">    
INSERT IGNORE INTO i18n (id,en,fr) VALUES
('tfa_structure_header', 'Two Factor Authentication', 'Authentification à deux facteurs'),

('tfa_secret_qr_code', 'Secret TFA QR Code', 'Code QR A2F secret'),
('click here to load the tfa_secret_qr_code', 'Click here to display the Secret TFA QR Code', 'Cliquez ici pour afficher le code QR A2F secret'),
('click here to hide the tfa_secret_qr_code', 'Click here to Hide the Secret TFA QR Code', 'Cliquez ici pour cacher le code QR A2F secret'),
('help_tfa_secret_qr_code', 
"Secret TFA QR Code to scan in the authenticator tool (Google Authenticator/Microsoft Authenticator) for new account creation.", 
"Code QR A2F secret à scanner dans l'application d'authentification (Google Authenticator/Microsoft Authenticator) pour créer un nouveau compte."),

('user name(s)', 'User Name(s)', 'Nom(s) utilisateur'),

('tfa_secret_url', 'Secret TFA URL', 'URL A2F secret'),
('click here to load the tfa_secret_url', 'Click here to display the Configuration Key', 'Cliquez ici pour afficher la clé de configuration'),
('click here to hide the tfa_secret_url', 'Click here to Hide the Configuration Key', 'Cliquez ici pour cacher la clé de configuration'),
('help_tfa_secret_url', 
'Secret TFA URL to enter in the authenticator tool (Google Authenticator/Microsoft Authenticator) for new account creation (replace the QR Code).', 
"URL A2F secret à saisir dans l'application d'authentification (Google Authenticator/Microsoft Authenticator) pour créer un nouveau compte (remplace le QR code)."),

('tfa_one_time_password', 'TFA One-Time Password', 'Mot de passe à usage unique A2F'),
('help_tfa_one_time_password', 
'Two Factor Authentication Process. Enter the One-Time Password displayed into the authenticator tool (Google Authenticator/Microsoft Authenticator).', 
"Processus d'authentification à deux facteurs. Saisir le mot de passe à usage unique affiché dans l'application d'authentification (Google Authenticator/Microsoft Authenticator)"),
 
('agree_to_keep_secret_tfa_code_and_url', 
'I agree to keep secret the both TFA URL and QR Code and contact the administrator in case of loss or theft to generate new ones.', 
'Je consent à garder secret les Code QR et URL A2F et à contacter l''administrateur en cas de perte ou vol pour en générer de nouveau.');

INSERT IGNORE INTO i18n (id,en,fr) VALUES
('regenerate the tfa configuration key', 'Regenerate the TFA Configuration Key', "Régénérer la clé de configuration 'A2F'"),
('generate the new qr code help message', 
'Regenerate the TFA Configuration Key for the user', 
'Régénérer la clé de configuration A2F de l''utilisateur'),
('do you want to generate a new secret code for the user?', 
'Do you want to generate a new TFA Configuration Key for the user?', 
'Voulez-vous générer un nouvelle clé de configuration A2F pour l''utilisateur?'),
('the secret key is regenerated', 'The TFA Configuration Key has been regenerated.', 'La clé de configuration A2F a été régénérée.'),

('there are some problems in creating a new secret key', 
'The creation of a new TFA Configuration Key failed.', 
'La création d''une nouvelle de configuration A2F a échoué.'),

('set the user as tfa super admin', 'Define as TFA Super Administrator', 'Définir comme super administrateur A2F'),
('set the user as tfa super admin help message', 'Set the user as TFA Super Administrator', 'Définir l''utilisateur comme super administrateur A2F'),
('remove tfa super admin role', 'Remove the TFA Super Administrator role', 'Supprimer le rôle de super administrateur A2F'),
('remove tfa super admin role help message', 'Remove the TFA Super Administrator role to this user', 'Supprimer le rôle de super administrateur A2F'),

('remove TFA super admin role first', 'Remove TFA Super Administrator role first.', 'Suprimer d''abord le rôle de super administrateur A2F.'),
('the tfa super admin role can not be changed for an inactive user', "The TFA Super Administrator role can not be changed for an inactive user.", "Le rôle de super administrateur A2F ne peut être modifié pour un utilisateur non actif."),
('the user can not change himself his tfa super admin role', "You can not change your TFA Super Administrator role by yourself.", "Vous ne pouvez pas modifier votre rôle de super administrateur A2F par vous même."),

('you can not delete a tfa super admin', "You can not delete a TFA Super Administrator user.", "Vous ne pouvez pas supprimer un super administrateur A2F."),
('you can not inactivate a tfa super admin', "You can not inactivate a TFA Super Administrator user.", "Vous ne pouvez pas inactiver un super administrateur A2F."),

('do you want to set this user as tfa super admin?', 'Do you want to assign the TFA Super Administrator role to this user?', 'Voulez-vous définir cet utilisateur comme super administrateur A2F?'),
('do you want to remove the tfa super admin permission from this user?', 'Do you want to remove the TFA Super Administrator role to this user ?', 'Voulez-vous supprimer le rôle de super administrateur A2F à cet utilisateur ?'),
('user not found', 'User not found', 'Utilisateur non trouvé'),
('the user can not be a tfa super admin', 'User cannot be a TFA Super Administrator', 'L''utilisateur ne peut pas être un super administrateur A2F'),
('the number of tfa super admins have reached the minimum limit', 'The number of TFA Super Administrators have reached the minimum limit.', 'Le nombre de super administrateurs A2F a atteint la limite minimale.'),
('the number of tfa super admins have reached the maximum limit', 'The number of active TFA Super Administrators have reached the maximum limit.', 'Le nombre de super administrateurs A2F actif a atteint la limite maximale.'),
('the user is not tfa super admin anymore', 'The user is not TFA Super Administrator anymore', 'L''utilisateur n''est plus un super administrateur A2F'),
('the user is tfa super admin', 'The user role has been changed to TFA Super Administrator', 'Le rôle de l''utilisateur a été changé à super administrateur A2F'),
('can not change the group for a tfa super admin user', 'Cannot change the group for a TFA Super Administrator user', 'Impossible de modifier le groupe pour un utilisateur super administrateur A2F');

    -- </editor-fold>
    
    -- <editor-fold desc="Super user display">   
INSERT INTO structures(`alias`) VALUES ('users_form_for_admin2');

INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`) 
VALUES
('Administrate', 'User', 'userss', 'is_super_admin', 'checkbox',  NULL , '0', '', '', '', 'tfa super admin', '');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='users_form_for_admin2'), 
(SELECT id FROM structure_fields WHERE `model`='User' AND `tablename`='userss' AND `field`='is_super_admin' AND `type`='checkbox' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' ), 
'1', '0', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '0');

INSERT IGNORE INTO i18n (id,en,fr) VALUES
('tfa super admin', 'TFA Super Administrator', 'Super administrateur A2F');

    -- </editor-fold> 

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/599): Can not apply the read only permissions">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/599): Can not apply the read only permissions
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT IGNORE INTO `i18n` (`id`, `en`, `fr`) VALUES
(
    "the permissions are changed in the tree, to see the changes expande the permission tree",
    "The permissions are changed in the tree, to see the changes, expand the permission tree and to apply them click on Submit.",
    "Les autorisations sont modifiées dans l'arborescence, pour voir les modifications, développez l'arborescence des autorisations et pour les appliquer, cliquez sur Soumettre.");
-- ("", "", ""),

-- </editor-fold>

-- <editor-fold desc="Rename form builder tool fields">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Rename form bilder tool fields
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE structure_fields SET  `language_label`='structure_form_name' WHERE model='FormBuilder' AND tablename='form_builders' AND field='label' AND `type`='input' AND structure_value_domain  IS NULL ;
INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('structure_form_name', 'Form Name', 'Nom du formulaire');

INSERT INTO structures(`alias`) VALUES ('form_builder_index_multi');
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='form_builder_index_multi'), 
(SELECT id FROM structure_fields WHERE `model`='FormBuilder' AND `tablename`='form_builders' AND `field`='label' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'0', '1', '', '0', '1', 'structure_form_types', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='form_builder_index_multi'), 
(SELECT id FROM structure_fields WHERE `model`='FormBuilder' AND `tablename`='form_builders' AND `field`='note' AND `type`='textarea' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='note' AND `language_tag`=''), 
'0', '2', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('structure_form_types', 'Forms Types', 'Types de formulaires');

UPDATE structure_fields SET  `language_label`='structure_form_name' WHERE model='ConsentControl' AND tablename='consent_controls' AND field='controls_type' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_label`='structure_form_name' WHERE model='DiagnosisControl' AND tablename='diagnosis_controls' AND field='controls_type' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_label`='structure_form_name' WHERE model='EventControl' AND tablename='event_controls' AND field='event_type' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_formats SET `flag_override_label`='1', `language_label`='structure_form_name' WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_treatment') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='TreatmentControl' AND `tablename`='treatment_controls' AND `field`='tx_method' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_fields SET  `language_label`='structure_form_name' WHERE model='TreatmentExtendControl' AND tablename='treatment_extend_controls' AND field='type' AND `type`='input' AND structure_value_domain  IS NULL ;
UPDATE structure_fields SET  `language_label`='structure_form_name' WHERE model='InventoryActionControl' AND tablename='inventory_action_controls' AND field='type' AND `type`='input' AND structure_value_domain  IS NULL ;

UPDATE structure_formats SET `display_order`='2' WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_inventory_action') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='category' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='inventory_action_categories') AND `flag_confidential`='0');
UPDATE structure_formats SET `display_order`='1' WHERE structure_id=(SELECT id FROM structures WHERE alias='form_builder_inventory_action') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='InventoryActionControl' AND `tablename`='inventory_action_controls' AND `field`='type' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("form builder simple_model description",
"Standard ATiM forms which cannot be declined in several versions for the same type of data (participant profile, contact, etc.). Form management is limited to modification only.",
"Formulaires ATiM standards qui ne peuvent pas être déclinés en plusieurs versions pour un même type de données (profil participant, contact, etc.). La gestion des formulaires est limitée à la modification uniquement."),
("form builder master_detail_model description",
"ATiM forms which can be declined in several versions for the same type of data according to specific information the form is supposed to contain (surgery treatment form vs radiotherapy treatment form as an example).<br>
All forms of the same type contain common fields and fields specific to each one.<br>
Form management allows new form creation and modification of existing ones. However, note that no new Samples and Aliquots forms can be created.",
"Formulaires ATiM qui peuvent être déclinés en plusieurs versions pour un même type de données en fonction des informations spécifiques que le formulaire est censé contenir (formulaire de traitement chirurgical vs formulaire de traitement radiothérapeutique par exemple).<br>
Tous les formulaires d'un même type contiennent des champs communs et des champs propres à chacun des formulaires.<br>
La gestion des formulaires permet la création de nouveaux formulaires et la modification de formulaires existants. Cependant, notez qu'aucun nouveau formulaire d'échantillons et d'aliquotes ne peut être créé."),
("form builder other_model description",
"Other ATiM form types.",
"Autres types de formulaire ATiM.");

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/626): deleted user and batch upload summary issue">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/626): deleted user and batch upload summary issue
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `structure_value_domains` (`domain_name`, `source`) VALUES
('deleted_undeleted_users_list', 'User::getDeletedUndeletedUsersList'),
('active_users_list', 'User::getActiveUsersList');
UPDATE structure_fields 
SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='deleted_undeleted_users_list')  
WHERE `model`='ImportList' AND `tablename`='batch_uploads' AND `field`='user_id' AND `type`='select';
UPDATE structure_fields 
SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='deleted_undeleted_users_list')  
WHERE model='BatchSet' AND tablename='datamart_batch_sets' AND field='created_by' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='users_list');
UPDATE structure_fields 
SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='deleted_undeleted_users_list')  
WHERE model='CollectionProtocol' AND tablename='collection_protocols' AND field='user_id' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='users_list');
UPDATE structure_fields 
SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='deleted_undeleted_users_list')  
WHERE model='Template' AND tablename='templates' AND field='user_id' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='users_list');
INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('user_deleted', "Deleted", "Supprimé"),
('user_active', "Active", "Actif"),
('user_inactive', "Inactive", "Inactif");

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/631): Collection to participant link deletion: Collecitons.misc_identifier_control_id not erased.">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/631): Collection to participant link deletion: Collecitons.misc_identifier_control_id not erased.
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE collections SET misc_identifier_id = null WHERE participant_id IS null;
INSERT IGNORE INTO i18n (id,en,fr)
VALUES
('click %s to display collection', 'Click %s to display collection.', "Cliquer %s pour aficher la collection."),
('here_lowercase', 'here', 'ici');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/601): Create cell culture block aliquot control.">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/601): Create cell culture block aliquot control.
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO structures(`alias`) VALUES ('ad_der_cell_culture_blocks');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`) 
VALUES
((SELECT id FROM structures WHERE alias='ad_der_cell_culture_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='coll_to_stor_spent_time_msg' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='inv_coll_to_stor_spent_time_msg_defintion' AND `language_label`='collection to storage spent time' AND `language_tag`=''), 
'1', '59', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_cell_culture_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='coll_to_stor_spent_time_msg' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '59', '', '0', '1', 'collection to storage spent time (min)', '0', '', '0', '', '1', 'integer_positive', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_cell_culture_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='creat_to_stor_spent_time_msg' AND `type`='input' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='size=30' AND `default`='' AND `language_help`='inv_creat_to_stor_spent_time_msg_defintion' AND `language_label`='creation to storage spent time' AND `language_tag`=''), 
'1', '60', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_cell_culture_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='view_aliquots' AND `field`='creat_to_stor_spent_time_msg' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'), 
'1', '60', '', '0', '1', 'creation to storage spent time (min)', '0', '', '0', '', '1', 'integer_positive', '1', '', '0', '', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'), 
((SELECT id FROM structures WHERE alias='ad_der_cell_culture_blocks'), 
(SELECT id FROM structure_fields WHERE `model`='AliquotDetail' AND `tablename`='' AND `field`='block_type' AND `type`='select' AND `structure_value_domain` =(SELECT id FROM structure_value_domains WHERE domain_name='block_type')  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='block type' AND `language_tag`=''), 
'1', '70', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '1', '0', '1', '0', '1', '0', '1', '0', '1', '0', '0', '0', '1', '1', '0', '0');

INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'cell culture'), 'block', '', 'ad_der_cell_culture_blocks', 'ad_blocks', '', 0, '', 0);

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/563): Add new sample_control ctDNA (a valider avec nicolas cfdna vs ctdna).">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/563): Add new sample_control ctDNA (a valider avec nicolas cfdna vs ctdna).
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO sample_controls (id, sample_type, sample_category, detail_form_alias, detail_tablename, display_order)
VALUES
(null, 'ctdna', 'derivative', 'sd_undetailed_derivatives,derivatives', 'sd_der_dnas', 0);
INSERT INTO parent_to_derivative_sample_controls (id, parent_sample_control_id, derivative_sample_control_id, flag_active, lab_book_control_id)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'plasma'), (SELECT id FROM sample_controls WHERE sample_type = 'ctdna'),0, NULL);
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('ctdna', 'ctDNA', 'ctDNA');
INSERT INTO aliquot_controls (id, sample_control_id, aliquot_type, aliquot_type_precision, detail_form_alias, detail_tablename, volume_unit, flag_active, comment, display_order)
VALUES
(null, (SELECT id FROM sample_controls WHERE sample_type = 'ctDNA'), 'tube', '', 'ad_der_tubes_incl_ul_vol_and_conc', 'ad_tubes', 'ul', 0, '', 0);

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/tocomplete): tocomplete.">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	tocomplete
-- ----------------------------------------------------------------------------------------------------------------------------------

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/tocomplete): tocomplete.">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	tocomplete
-- ----------------------------------------------------------------------------------------------------------------------------------

-- </editor-fold>

-- <editor-fold desc="i18n clean up">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	i18n clean up
-- ----------------------------------------------------------------------------------------------------------------------------------

DELETE FROM i18n WHERE id = 'shipping';
INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('shipping', 'Shipping', 'Envoi'),
('order and shipping','Order and Shipping','Commande et envoi');
UPDATE datamart_structures SET display_name = 'aliquot use - summary/view' WHERE model = 'ViewAliquotUse'; 
INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('aliquot use - summary/view', 'Aliquot Use (Synthesis)', 'Utilisation d''aliquot (synthèse)');

DELETE from i18n WHERE id = 'use and/or event';
INSERT IGNORE INTO i18n (id,en,fr) 
VALUES
('use and/or event', 'Use/Event/Annotation', 'Utilisation/événement/annotation');

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/635): Typo in IEA control type">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Typo in IEA control type
-- ----------------------------------------------------------------------------------------------------------------------------------
REPLACE INTO `i18n` (`id`, `en`, `fr`) VALUES
(
"inv. action categ. : samples and aliquots not necessarily related",
"Selected samples types or selected aliquots types (types not necessarily related)",
"Types d'échantillons sélectionnés ou types d'aliquotes sélectionnés (types non nécessairement associés)"
);
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/637): The language tags are not translated in FormBuilder in some cases">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	The language tags are not translated in FormBuilder in some cases
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
    ((SELECT id FROM structures WHERE alias='form_builder_deletable_structure'),
     (SELECT id FROM structure_fields WHERE `model`='StructureField' AND `tablename`='structure_fields' AND `field`='language_tag' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0'),
     '1', '11', '', '0', '1', 'language_tag', '1', '', '0', '', '0', '', '1', 'class=pasteDisabled', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '1');

INSERT IGNORE INTO i18n (id,en,fr)
VALUES
("language_tag", "Tag", "Étiquette");
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/641): Lastname and email become required for the users">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Lastname and email become required for the users
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES ((SELECT `id` FROM `structure_fields` WHERE `model` = 'User' AND `tablename` = 'users' AND `field` = 'last_name' AND `type` = 'input' AND `structure_value_domain` IS NULL), 'notBlank', '');
INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES ((SELECT `id` FROM `structure_fields` WHERE `model` = 'User' AND `tablename` = 'users' AND `field` = 'email' AND `type` = 'input' AND `structure_value_domain` IS NULL), 'notBlank', '');
INSERT INTO `structure_validations` (`structure_field_id`, `rule`, `language_message`) VALUES ((SELECT `id` FROM `structure_fields` WHERE `model` = 'User' AND `tablename` = 'users' AND `field` = 'email' AND `type` = 'input' AND `structure_value_domain` IS NULL), 'custom,/^[\\w\\.]+@[\\w\\.]+\\.[a-zA-Z]+$/', 'invalid email address(ex: example@domain.com)');
UPDATE structure_fields SET  `setting`='size=30,placeholder=example@domain.com' WHERE model='User' AND tablename='users' AND field='email' AND `type`='input' AND structure_value_domain  IS NULL;
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('invalid email address(ex: example@domain.com)', 'Invalid Email Address(Ex: example@domain.com)', 'Adresse e-mail invalide (ex: exemple@domaine.com)');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/642): Add button to synchronize existing drugs matching RxNorm dictionary">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Add button to synchronize existing drugs matching RxNorm dictionary
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO i18n (id,en,fr) VALUES
('complete missing drug rx norm id', 'Complete missing drug RxNorm ID', "Compléter RxNorm IDs manquants"),
('Synchronization', 'Synchronization', 'Synchronisation'),
('%s drugs information updated and can not update %s drugs', '%s drugs information updated and can not update %s drugs', '%s drugs information updated and can not update %s drugs'),
('download the saved drugs information (takes time)', 'Download the saved Drugs information (Takes time)', 'Téléchargez les informations enregistrées sur les médicaments (prend du temps)');
-- ('', '', ''),
-- </editor-fold>


-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/643): FB: Unable to create a new form copying Cap Report Generic - Resection (CRCHUS OncoAxis)">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	FB: Unable to create a new form copying Cap Report Generic - Resection (CRCHUS OncoAxis)
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO i18n (id,en,fr) VALUES
('row size is too large, you need to decrease the column size by changing the type', 
'The number and/or the size of fields are too important to allow the form creation in ATiM.', 
'Le nombre et/ou la taille des champs sont trop importants pour permettre la création de formulaire dans ATiM.');
-- ('', '', ''),
-- </editor-fold>

-- <editor-fold desc="Removed feature comparing TNM between diagnosis form and Cap Report form">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Removed feature comparing TNM between diagnosis form and Cap Report form
-- ----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE diagnosis_controls DROP COLUMN flag_compare_with_cap;
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/588): Aliquot to Aliquote in i18n">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Aliquot to Aliquote in i18n
-- ----------------------------------------------------------------------------------------------------------------------------------

UPDATE i18n SET fr = "Aliquotes dans les commandes" WHERE id = "aliquot in orders";
UPDATE i18n SET fr = "Aliquotes dans les Événements/Annotations d'inventaire" WHERE id = "aliquot inventory action";
UPDATE i18n SET fr = "Master ID d'aliquote" WHERE id = "aliquot master id";
UPDATE i18n SET fr = "Aliquote réservée" WHERE id = "aliquot reserved";
UPDATE i18n SET fr = "Aliquotes envoyées" WHERE id = "aliquot shipped";
UPDATE i18n SET fr = "Utilisation d'aliquote (synthèse)" WHERE id = "aliquot use - summary/view";
UPDATE i18n SET fr = "Mise à jour des aliquotes sélectionnées (à appliquer à tous)" WHERE id = "apply on aliquots";
UPDATE i18n SET fr = "Nombre d'aliquotes dans les Événement/Annotation d'inventaire reliés à l'étude" WHERE id = "help_aliquot_inventory_action_count";
UPDATE i18n SET fr = "Nombre d'aliquotes dans les commandes reliées à l'étude" WHERE id = "help_aliquot_in_orders_count";
UPDATE i18n SET fr = "Nombre d'aliquotes réservées pour l'étude" WHERE id = "help_aliquot_reserved_count";
UPDATE i18n SET fr = "Nombre d'aliquotes de commandes reliées à l'étude et envoyées" WHERE id = "help_aliquot_shipped_count";
UPDATE i18n SET fr = "Nombre de participants ayant des aliquotes réservées reliée à l'étude" WHERE id = "help_patient_aliquot_reserved_count";
UPDATE i18n SET fr = "Number of participants ayant un ou des consentement(s), identifiant(s) ou aliquoet(s) reliés à l'étude" WHERE id = "help_patient_misc_consent_aliquot_count";
UPDATE i18n SET fr = "Nombre de participants avec des aliquotes reliées à l'étude" WHERE id = "help_total_distinct_patient_count";

UPDATE i18n SET fr = "Le nombre d'aliquotes enfants par défaut ne peut pas être supérieur à %d!" WHERE id = "nbr of children by default can not be bigger than %d";
UPDATE i18n SET fr = "Aliquotes de commandes" WHERE id = "order aliquots";
UPDATE i18n SET fr = "Aliquotes de participants" WHERE id = "participant aliquots";
UPDATE i18n SET fr = "Participant ayant des aliquotes reservées" WHERE id = "patient aliquot reserved";
UPDATE i18n SET fr = "Participant avec des aliquotes reliées à des Événements/annotations d'inventaire" WHERE id = "patient inventory action";
UPDATE i18n SET fr = "Participant avec Indentifiant/Consentement/Aliquote" WHERE id = "patient misc consent aliquot";
UPDATE i18n SET fr = "Total des aliquotes" WHERE id = "total study aliquots";

DELETE FROM i18n WHERE id = "aliquot review fb_control_description";
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/515): Collection view: Misc Identifier study list - Just display linkable identifier types">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Collection view: Misc Identifier study list - Just display linkable identifier types
-- ----------------------------------------------------------------------------------------------------------------------------------

INSERT INTO `structure_value_domains` (`domain_name`, `override`, `category`, `source`) VALUES ('identifier_names_linkable_to_col_list', '', '', 'ClinicalAnnotation.MiscIdentifierControl::getMiscIdentifierNameLincakbleToColPermissibleValues');
UPDATE structure_fields SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='identifier_names_linkable_to_col_list')  WHERE model='ViewCollection' AND tablename='' AND field='misc_identifier_name' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list');
UPDATE structure_formats SET `flag_override_setting`='0', `setting`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewCollection' AND `tablename`='' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
UPDATE structure_fields SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='identifier_names_linkable_to_col_list')  WHERE model='ViewSample' AND tablename='' AND field='misc_identifier_name' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list');
UPDATE structure_fields SET  `structure_value_domain`=(SELECT id FROM structure_value_domains WHERE domain_name='identifier_names_linkable_to_col_list')  WHERE model='ViewAliquot' AND tablename='' AND field='misc_identifier_name' AND `type`='select' AND structure_value_domain =(SELECT id FROM structure_value_domains WHERE domain_name='identifier_name_list');
UPDATE structure_formats SET `flag_override_setting`='0', `setting`='' WHERE structure_id=(SELECT id FROM structures WHERE alias='view_aliquot_joined_to_sample_and_collection') AND structure_field_id=(SELECT id FROM structure_fields WHERE `model`='ViewAliquot' AND `tablename`='' AND `field`='participant_identifier' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/644): Set the values_max_length to 255 for the long values">
-- ----------------------------------------------------------------------------------------------------------------------------------
-- Set the values_max_length to 255 for the long values
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT IGNORE INTO i18n (id,en,fr) VALUES ('%s dropdown max length values corrected.', '%s dropdown max length values corrected.', '%s valeurs de longueur maximale de la liste déroulante corrigées.');

-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/672): Be able to activate Specimen into Inventory Configuration + in index view add flag active/inactive">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Be able to activate Specimen into Inventory Configuration + in index view add flag active/inactive
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO structure_fields(`plugin`, `model`, `tablename`, `field`, `type`, `structure_value_domain`, `flag_confidential`, `setting`, `default`, `language_help`, `language_label`, `language_tag`)
VALUES
('Core', 'FunctionManagement', '', 'specimen_active', 'yes_no',  NULL , '0', '', '', '', 'active', '');

INSERT INTO structure_formats(`structure_id`, `structure_field_id`, `display_column`, `display_order`, `language_heading`, `margin`, `flag_override_label`, `language_label`, `flag_override_tag`, `language_tag`, `flag_override_help`, `language_help`, `flag_override_type`, `type`, `flag_override_setting`, `setting`, `flag_override_default`, `default`, `flag_add`, `flag_add_readonly`, `flag_edit`, `flag_edit_readonly`, `flag_search`, `flag_search_readonly`, `flag_addgrid`, `flag_addgrid_readonly`, `flag_editgrid`, `flag_editgrid_readonly`, `flag_batchedit`, `flag_batchedit_readonly`, `flag_index`, `flag_detail`, `flag_summary`, `flag_float`)
VALUES
((SELECT id FROM structures WHERE alias='sample_controls_management'),
(SELECT id FROM structure_fields WHERE `model`='FunctionManagement' AND `tablename`='' AND `field`='specimen_active' AND `type`='yes_no' AND `structure_value_domain`  IS NULL  AND `flag_confidential`='0' AND `setting`='' AND `default`='' AND `language_help`='' AND `language_label`='active' AND `language_tag`=''),
'1', '25', '', '0', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0');
-- </editor-fold>

-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/515): Collection view: Misc Identifier study list - Just display linkable identifier types">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Collection view: Misc Identifier study list - Just display linkable identifier types
-- ----------------------------------------------------------------------------------------------------------------------------------
DELETE FROM i18n WHERE id IN 
('inventory_configurations_warning_message', 
'inventory_configurations_specimen_activation_message', 
'inventory_configurations_derivative_activation_message', 
'inventory_configurations_aliquot_activation_message');

INSERT IGNORE INTO i18n (id,en,fr) 
VALUES 
('inventory_configurations_warning_message',
"No new type of sample (specimen or derivative) and no new type of aliquot can be created by an ATiM users. Only ATiM developers can create new types to develop standard across all the instances of ATiM.",
"Aucun nouveau type d'échantillon (spécimen ou dérivé) et aucun nouveau type d'aliquote ne peuvent être créés par un utilisateur ATiM. Seuls les développeurs ATiM peuvent créer de nouveaux types pour développer une norme sur toutes les instances d'ATiM."),

('inventory_configurations_specimen_activation_message',
"<b>How to activate a Specimen sample type</b> 
<br>&nbsp;&nbsp;&nbsp;- Using the Search form, find the Specimen type entering search criteria and clicking on Search button.
<br>&nbsp;&nbsp;&nbsp;- Click on the Detail button on the right of the Specimen.
<br>&nbsp;&nbsp;&nbsp;- Click on the Activate button.
",
"<b>Comment activer un type d'échantillon 'Spécimen'</b>
<br>&nbsp;&nbsp;&nbsp;- À l'aide du formulaire de recherche, trouvez le type de Spécimen en saisissant les critères de recherche et en cliquant sur le bouton Chercher.
<br>&nbsp;&nbsp;&nbsp;- Cliquez sur l'icône Détail à droite du Spécimen.
<br>&nbsp;&nbsp;&nbsp;- Cliquez sur le bouton Activer.
"),

('inventory_configurations_derivative_activation_message',
"<b>How to activate the creation of a specific sample type (Derivative) from a specific parent sample type (Specimen or Derivative)</b> 
<br>&nbsp;&nbsp;&nbsp;- Using the Search form find the parent sample type (Specimen or Derivative) entering search criteria and clicking on Search button.
<br>&nbsp;&nbsp;&nbsp;- Click on the Detail button on the right of the sample type then click on the Edit button.
<br>&nbsp;&nbsp;&nbsp;- In Derivatives form section, add the derivative sample type you expect to create from the parent sample type. 
<br>&nbsp;&nbsp;&nbsp;- Click on Submit button. 
",
"<b>Comment activer la création d'un type d'échantillon spécifique (Dérivé) à partir d'un type d'échantillon parent spécifique (Spécimen ou Dérivé)</b>
<br>&nbsp;&nbsp;&nbsp;- À l'aide du formulaire de recherche, trouvez le type d'échantillon parent (Spécimen ou Dérivé) en saisissant les critères de recherche et en cliquant sur le bouton Chercher.
<br>&nbsp;&nbsp;&nbsp;- Cliquez sur le bouton Détail à droite du type d'échantillon puis cliquez sur le bouton Modifier.
<br>&nbsp;&nbsp;&nbsp;- Dans la section Dérivés du formulaire, ajoutez le type d'échantillon dérivé que vous souhaitez créer à partir du type d'échantillon parent. 
<br>&nbsp;&nbsp;&nbsp;- Cliquez sur le bouton Envoyer. 
"),

('inventory_configurations_aliquot_activation_message',
"<b>How to activate the creation of a specific aliquot type from a specific sample type (Specimen or Derivative)</b> 
<br>&nbsp;&nbsp;&nbsp;- Using the Search form find the  sample type (Specimen or Derivative) entering search criteria and clicking on Search button.
<br>&nbsp;&nbsp;&nbsp;- Click on the Detail button on the right of the sample type then click on the Edit button.
<br>&nbsp;&nbsp;&nbsp;- In Aliquote form section, add the aliquot type you expect to create for the sample type. 
<br>&nbsp;&nbsp;&nbsp;- Click on Submit button. 
",
"<b>Comment activer la création d'un type d'aliquote à partir d'un type d'échantillon spécifique (Spécimen ou Dérivé)</b>
<br>&nbsp;&nbsp;&nbsp;- À l'aide du formulaire de recherche, trouvez le type d'échantillon parent (Spécimen ou Dérivé) en saisissant les critères de recherche et en cliquant sur le bouton Chercher.
<br>&nbsp;&nbsp;&nbsp;- Cliquez sur le bouton Détail à droite du type d'échantillon puis cliquez sur le bouton Modifier.
<br>&nbsp;&nbsp;&nbsp;- Dans la section Aliquotes du formulaire, ajoutez le type de l'aliquote que vous souhaitez créer à partir du type d'échantillon. 
<br>&nbsp;&nbsp;&nbsp;- Cliquez sur le bouton Envoyer. 
");
-- </editor-fold>

-- ----------------------------------------------------------------------------------------------------------------------------------
-- Version
-- ----------------------------------------------------------------------------------------------------------------------------------
INSERT INTO `versions` (version_number, date_installed, trunk_build_number, branch_build_number)
VALUES
('2.8.2', NOW(),'todo','n/a');