@ECHO off
@REM https://www.messletters.com/en/big-text/  -> colossal
CLS
COLOR F
echo =================================================================================================
echo        d8888 88888888888 d8b 888b     d888               .d8888b.       .d8888b.      .d8888b.
echo       d88888     888     Y8P 8888b   d8888              d88P  Y88b     d88P  Y88b     d88P  Y88b
echo      d88P888     888         88888b.d88888                     888     Y88b. d88P            888
echo     d88P 888     888     888 888Y88888P888     888  888      .d88P      "Y88888"           .d88P
echo    d88P  888     888     888 888 Y888P 888     888  888  .od888P"      .d8P""Y8b.      .od888P"
echo   d88P   888     888     888 888  Y8P  888     Y88  88P d88P"          888    888     d88P"
echo  d8888888888     888     888 888   "   888      Y8bd8P  888"       d8b Y88b  d88P d8b 888"
echo d88P     888     888     888 888       888       Y88P   888888888  Y8P  "Y8888P"  Y8P 888888888
echo =================================================================================================

@REM ECHO ==================================================================
@REM ECHO ==   ATiM DEMO DATA                                             ==
@REM ECHO ==     Version 2.8.0                                            ==
@REM ECHO ==================================================================
@REM ECHO -

echo.
ECHO All your data will be erased and unrecoverable.
SET /p confirm= "Are you sure to run it? if yes type YES or anything else for cancel. YES/NO "

IF NOT "%confirm%" == "YES" (
	EXIT
)

set /p databaseName=Database Name (default 'atimdev28z' if not completed)?
if "%databaseName%" == "" (
	SET databaseName=atimdev28z
)
set /p databaseUserName=Database databaseUserName (default 'root' if not completed)?
if "%databaseUserName%" == "" (
	SET databaseUserName=root
)
set /p password=Database password? 
if NOT "%password%" == "" (
	SET password=-p%password%
)

echo.
SET /p createBackup= "Do you want to create Backup? if yes type YES or anything else for cancel. YES/NO "

IF NOT "%createBackup%" == "YES" goto nobackup

REM ===================================================================================================================================
REM  Create backup of the database
REM ===================================================================================================================================
@ECHO off
for /f "tokens=2 delims==" %%a in ('wmic OS Get localdatetime /value') do set "dt=%%a"
set "YY=%dt:~2,2%" & set "YYYY=%dt:~0,4%" & set "MM=%dt:~4,2%" & set "DD=%dt:~6,2%"
set "HH=%dt:~8,2%" & set "Min=%dt:~10,2%" & set "Sec=%dt:~12,2%"
set "fullstamp=%YYYY%-%MM%-%DD%_%HH%-%Min%-%Sec%"

@ECHO on
mysqldump -u %databaseUserName% %password% %databaseName% > the_last_backup_file%fullstamp%.sql
@ECHO off

:nobackup

REM ===================================================================================================================================
REM  DROP DATABASE AND RECREATE IT
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% -e "DROP DATABASE IF EXISTS %databaseName%"
mysql -u %databaseUserName% %password% -e "CREATE DATABASE %databaseName%"
@ECHO off

REM ===================================================================================================================================
REM  ATiM TRUNK CREATION AND UPDATE SCRIPTS
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../atim_v2.8.0_full_installation.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../atim_v2.8.1_upgrade.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../atim_v2.8.2_upgrade.sql
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE misc_identifier_controls SET flag_active = 1;UPDATE consent_controls SET flag_active = 1;UPDATE diagnosis_controls SET flag_active = 1;UPDATE event_controls SET flag_active = 1;UPDATE treatment_controls SET flag_active = 1;UPDATE treatment_extend_controls SET flag_active = 1;UPDATE storage_controls SET flag_active = 1;"
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE misc_identifier_controls SET id = 9 WHERE misc_identifier_name = 'ctrnet demo - patient study id';"
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE diagnosis_masters SET diagnosis_control_id = (SELECT id FROM diagnosis_controls WHERE controls_type = 'ctrnet demo - cardiovascular disease common codes') WHERE diagnosis_control_id = 29;"
@ECHO off

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : CLINICAL ANNOTATION
REM ===================================================================================================================================

REM  Profile & Misc Identifier
REM ==========================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_create_ctrnet_demo_identifiers.sql
@ECHO off

REM  Consent
REM ========

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_hide_consent_national_fields.sql
@ECHO off
REM mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_add_consent_file_upload_field.sql

REM  Diagnosis
REM ==========

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_cpcbn_primary_prostate.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_coeur_primary_ovary.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_qbcf_secondary_breast_progression.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_ctrnet_demo_primary_breast.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_primary_diagnosis.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_cns_replase.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_histological_transformation.sql
@ECHO off

REM  Treatment
REM ==========

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_biopsy_turp.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_prostatectomy.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_crchum_hpb_liver_and_pancreas_surgery.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_radiotherapy.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_hormonotherapy.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_systemic_treatment.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_add_ctrnet_trunk_surgery_site_notes_fields.sql
@ECHO off

REM  Clinical Event
REM ===============

REM Clinical

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_followup.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_imaging.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_imaging.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_medical_history.sql
@ECHO off
	
REM Lab

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_biology.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_liver_metastases_lab_report.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biology.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biomarker.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_onco_axis_genetic_test.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_cap_report.sql
@ECHO off

REM  Contact
REM ========

REM  Message
REM ========

REM  Chronology
REM ===========

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : INVENTORY MANAGEMENT
REM ===================================================================================================================================

REM  Collection
REM ===========

REM  Sample
REM =======
	
REM  Aliquot
REM ========
	
REM  Inventory Event/Action
REM =======================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE inventory_action_controls SET flag_active = '1'  WHERE type IN ('quality control', 'tissue review', 'laboratory activity');
@ECHO off

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : TOOLS
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/tool_drug_create_tfri_projects_drugs.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/datamart_databrowser_activate_ctrnet_demo_links.sql
@ECHO off

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : QUERY TOOL
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/datamart_report_add_ctrnet_demo_identifiers_to_identifiers_report.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/datamart_report_add_multi_CSV_participant_consent_diagnosis_report.sql
@ECHO off

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : CLINICAL ANNOTATION 2 - mCode () fields
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/coding_icd_mcode_codes.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_mcode_patient_fields.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_mcode_diseases.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_comorbid_condition.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_performance_status.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_outcome.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_tumor_marker_test.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_vital_sign.sql

mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_radiation.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_surgery.sql
@ECHO off

REM ===================================================================================================================================
REM  CUSTOM FORM LIBRARIES : CLINICAL ANNOTATION 3 - New forms
REM ===================================================================================================================================

REM  Diagnosis
REM ==========

REM  Clinical Event
REM ===============

REM Clinical

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_treatment_history.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_isaric_covid19.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchus_onco_generic_resection_lab_report.sql
@ECHO off

REM ===================================================================================================================================
REM  DEMO DATA
REM ===================================================================================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ./atim_v2.8.0_demo_data.sql
mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ./custom_post_282_demo_data.sql
REM Remove next lime for large aliquot data set
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE aliquot_masters SET deleted = 1 WHERE barcode like 'alq_brcd#%';"
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE parent_to_derivative_sample_controls SET flag_active = 1;"
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE aliquot_controls SET flag_active = 1;"
@ECHO off

REM ==================================================================
REM  ATiM USERS UPDATE
REM ==================================================================

@ECHO on
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE users SET flag_active=0, deleted = 1, username=CONCAT('user#', id),first_name=CONCAT('user#', id), last_name=CONCAT('user#', id), password='2939a463baec8a8b1f3cef69238e6bfea3795a5c', email = '' WHERE username != 'administrator'"
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE users SET flag_active=1, deleted = 0, force_password_reset = 0, first_name='administrator', last_name='administrator', email = ''  WHERE username = 'administrator'"

mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE groups SET flag_show_confidential = '0'  WHERE name != 'Administrators'"
mysql -u %databaseUserName% %password% %databaseName% -e "UPDATE groups SET flag_show_confidential = '1'  WHERE name = 'Administrators'"
@ECHO off

REM ===================================================================================================================================
REM ===================================================================================================================================

COLOR F

ECHO -
ECHO ==================================================================
ECHO ==   ATiM DEMO SET UP - DONE                                    ==
ECHO ==================================================================
ECHO -

ECHO Use the username 'administrator' and the password 'administrator' to log into ATiM.
ECHO -
ECHO To force password reset for the username 'administrator', execute following query:
ECHO "UPDATE users SET force_password_reset = 1 WHERE username = 'administrator'"
ECHO -
ECHO To change the username 'administrator', execute following query:
ECHO "UPDATE users SET username = 'my_new_username' WHERE username = 'administrator'"
ECHO -

ECHO -

PAUSE
