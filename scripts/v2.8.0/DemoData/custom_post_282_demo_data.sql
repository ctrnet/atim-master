-- <editor-fold desc="Fix the issue (https://gitlab.com/ctrnet/atim/-/issues/587): Add maintenance mode in ATiM">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Add maintenance mode in ATiM
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE `users` SET `dev_user` = 1, `is_super_admin` = 1 WHERE `username` = 'administrator' AND `id` = '1';
-- </editor-fold>

-- <editor-fold desc="Fix the issue (#688): Unable to merge P0002 in P0001">
-- ----------------------------------------------------------------------------------------------------------------------------------
--	Unable to merge P0002 in P0001
-- ----------------------------------------------------------------------------------------------------------------------------------
UPDATE `event_masters`
SET `event_control_id` = (SELECT `id` FROM `event_controls` WHERE `detail_tablename` = 'ed_cap_report_16_colon_resections')
WHERE `event_control_id` = '52';

-- </editor-fold>


