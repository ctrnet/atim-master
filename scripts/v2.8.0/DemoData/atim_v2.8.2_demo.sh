# The Script file to load the demo data for the Linux users.

WHITE='\033[1;37m'
CYAN='\033[1;36m'
YELLOW='\033[1;33m'
RED='\033[0;31m'
GREEN='\033[0;32m'

clear
echo -e "${YELLOW}"

printf "===================================================================================================\n"
printf "        d8888 88888888888 d8b 888b     d888               .d8888b.       .d8888b.      .d8888b.  \n"
printf "       d88888     888     Y8P 8888b   d8888              d88P  Y88b     d88P  Y88b     d88P  Y88b  \n"
printf "      d88P888     888         88888b.d88888                     888     Y88b. d88P            888  \n"
printf "     d88P 888     888     888 888Y88888P888     888  888      .d88P      'Y88888'           .d88P  \n"
printf "    d88P  888     888     888 888 Y888P 888     888  888  .od888P'      .d8P''Y8b.      .od888P\"  \n"
printf "   d88P   888     888     888 888  Y8P  888     Y88  88P d88P'          888    888     d88P\"  \n"
printf "  d8888888888     888     888 888   '   888      Y8bd8P  888'       d8b Y88b  d88P d8b 888\"  \n"
printf " d88P     888     888     888 888       888       Y88P   888888888  Y8P  'Y8888P'  Y8P 888888888  \n"
printf "===================================================================================================\n\n"


#COLUMNS=$(tput cols)
#title="ATiM DEMO DATA (Version 2.8.0)"
#printf "%*s\n\n\n" $(((${#title}+$COLUMNS)/2)) "$title"

echo -e "${RED}"
printf "All your data will be erased and unrecoverable."
echo -e "${WHITE}"
printf "Are you sure to run it? if yes type YES or anything else for cancel. YES/NO "

read confirm

if [ "$confirm" != "YES" ];
then
	exit
fi

printf "Database Name(default 'atimdev28z' if not completed)? "
read databaseName
if [ "$databaseName" = "" ];
then
	databaseName="atimdev28z"
fi

printf "Database username (default 'root' if not completed)? "
read databaseUserName
if [ "$databaseUserName" = "" ];
then
	databaseUserName="root"
fi

printf "Database Password? "
read password
if [ "$password" = "" ];
then
	password=""
else
	password="-p$password"
fi

pass="-ppassword"

echo -e "${WHITE}"
printf "Do you want to create Backup? if yes type YES or anything else for cancel. YES/NO "

read createBackup

if [ "$createBackup" = "YES" ];
then
  timestamp=$(date +%s)
  echo -e "\n${GREEN}\t\tCreating the last database backup\n"
  echo -e "${WHITE}mysqldump -u $databaseUserName $pass $databaseName > the_last_backup_file${timestamp}.sql"
  mysqldump -u $databaseUserName $password $databaseName > the_last_backup_file$timestamp.sql
fi



echo -e "\n${GREEN}\t\tDrop and create the old database\n"
echo -e "${WHITE}mysql -u $databaseUserName $pass -e 'DROP DATABASE IF EXISTS $databaseName'"
mysql -u $databaseUserName $password -e "DROP DATABASE IF EXISTS $databaseName"
echo -e "${WHITE}mysql -u $databaseUserName $pass -e 'CREATE DATABASE $databaseName'"
mysql -u $databaseUserName $password -e "CREATE DATABASE $databaseName"

# -------------------------------------------------------------------------------------------------------------------
# ATiM TRUNK CREATION AND UPDATE SCRIPTS
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${GREEN}\t\tATiM TRUNK CREATION AND UPDATE SCRIPTS\n"
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../atim_v2.8.0_full_installation.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.8.0_full_installation.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../atim_v2.8.1_upgrade.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.8.1_upgrade.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../atim_v2.8.2_upgrade.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../atim_v2.8.2_upgrade.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName -e UPDATE misc_identifier_controls SET flag_active = 1;UPDATE consent_controls SET flag_active = 1;UPDATE diagnosis_controls SET flag_active = 1;UPDATE event_controls SET flag_active = 1;UPDATE treatment_controls SET flag_active = 1;UPDATE treatment_extend_controls SET flag_active = 1;UPDATE storage_controls SET flag_active = 1;"
mysql -u $databaseUserName $password $databaseName -e "UPDATE misc_identifier_controls SET flag_active = 1;UPDATE consent_controls SET flag_active = 1;UPDATE diagnosis_controls SET flag_active = 1;UPDATE event_controls SET flag_active = 1;UPDATE treatment_controls SET flag_active = 1;UPDATE treatment_extend_controls SET flag_active = 1;UPDATE storage_controls SET flag_active = 1;"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName -e UPDATE misc_identifier_controls SET id = 9 WHERE misc_identifier_name = 'ctrnet demo - patient study id';"
mysql -u $databaseUserName $password $databaseName -e "UPDATE misc_identifier_controls SET id = 9 WHERE misc_identifier_name = 'ctrnet demo - patient study id';"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName -e UPDATE diagnosis_masters SET diagnosis_control_id = (SELECT id FROM diagnosis_controls WHERE controls_type = 'ctrnet demo - cardiovascular disease common codes') WHERE diagnosis_control_id = 29;"
mysql -u $databaseUserName $password $databaseName -e "UPDATE diagnosis_masters SET diagnosis_control_id = (SELECT id FROM diagnosis_controls WHERE controls_type = 'ctrnet demo - cardiovascular disease common codes') WHERE diagnosis_control_id = 29;"

echo -e "\n${GREEN}\t\tCUSTOM FORM LIBRARIES : CLINICAL ANNOTATION"
echo -e "${GREEN}\t\tVersion: v2.8.0 demo data"

# -------------------------------------------------------------------------------------------------------------------
# Profile & Misc Identifier
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tProfile & Misc Identifier"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_create_ctrnet_demo_identifiers.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_misc_identifier_create_ctrnet_demo_identifiers.sql

# -------------------------------------------------------------------------------------------------------------------
# Consent
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tConsent"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_hide_consent_national_fields.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_consent_hide_consent_national_fields.sql

# -------------------------------------------------------------------------------------------------------------------
# Diagnosis
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tDiagnosis"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_cpcbn_primary_prostate.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_cpcbn_primary_prostate.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_coeur_primary_ovary.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_tfri_coeur_primary_ovary.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_qbcf_secondary_breast_progression.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_qbcf_secondary_breast_progression.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_ctrnet_demo_primary_breast.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_ctrnet_demo_primary_breast.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_primary_diagnosis.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_primary_diagnosis.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_cns_replase.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_cns_replase.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_histological_transformation.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_jgh_lymphoma_progression_histological_transformation.sql

# -------------------------------------------------------------------------------------------------------------------
# Treatment
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tTreatment"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_biopsy_turp.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_biopsy_turp.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_prostatectomy.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_tfri_cpcbn_prostatectomy.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_crchum_hpb_liver_and_pancreas_surgery.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_crchum_hpb_liver_and_pancreas_surgery.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_radiotherapy.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_radiotherapy.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_hormonotherapy.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_hormonotherapy.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_systemic_treatment.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_ctrnet_demo_systemic_treatment.sql

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_add_ctrnet_trunk_surgery_site_notes_fields.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_add_ctrnet_trunk_surgery_site_notes_fields.sql

# -------------------------------------------------------------------------------------------------------------------
# Clinical Event
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${GREEN}\t\tClinical Event"

echo -e "\n${CYAN}\t\tClinical"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_followup.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_followup.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_imaging.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_imaging.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_imaging.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_imaging.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_medical_history.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_medical_history.sql

# -------------------------------------------------------------------------------------------------------------------
# Lab
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tLab"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_biology.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_biology.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_liver_metastases_lab_report.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_hpb_liver_metastases_lab_report.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biology.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biology.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biomarker.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_biomarker.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_onco_axis_genetic_test.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchum_onco_axis_genetic_test.sql
echo -e "${WHITE}mysql -u %databaseUserName% %password% %databaseName% --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_cap_report.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_cap_report.sql
	
# -------------------------------------------------------------------------------------------------------------------
# Contact
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tContact"

# -------------------------------------------------------------------------------------------------------------------
# Message
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tMessage"

# -------------------------------------------------------------------------------------------------------------------
# Chronology
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tChronology"

# -------------------------------------------------------------------------------------------------------------------
# CUSTOM FORM LIBRARIES : INVENTORY MANAGEMENT
# Version: v2.8.0 demo data
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${GREEN}\t\tCUSTOM FORM LIBRARIES : INVENTORY MANAGEMENT"
echo -e "${GREEN}\t\tVersion: v2.8.0 demo data"

# -------------------------------------------------------------------------------------------------------------------
# Collection
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tCollection"

# -------------------------------------------------------------------------------------------------------------------
# Sample
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tSample"

# -------------------------------------------------------------------------------------------------------------------
# Aliquot
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tAliquot"

# -------------------------------------------------------------------------------------------------------------------
# CUSTOM FORM LIBRARIES : TOOLS
# Version: v2.8.0 demo data
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${GREEN}\t\tCUSTOM FORM LIBRARIES : TOOLS"
echo -e "${GREEN}\t\tVersion: v2.8.0 demo data\n"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/tool_drug_create_tfri_projects_drugs.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/tool_drug_create_tfri_projects_drugs.sql

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/datamart_databrowser_activate_ctrnet_demo_links.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/datamart_databrowser_activate_ctrnet_demo_links.sql

# -------------------------------------------------------------------------------------------------------------------
# CUSTOM FORM LIBRARIES : QUERY TOOL
# Version: v2.8.0 demo data
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${GREEN}\t\tCUSTOM FORM LIBRARIES : QUERY TOOL"
echo -e "\n${GREEN}\t\tVersion: v2.8.0 demo data\n"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/datamart_report_add_ctrnet_demo_identifiers_to_identifiers_report.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/datamart_report_add_ctrnet_demo_identifiers_to_identifiers_report.sql

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/datamart_report_add_multi_CSV_participant_consent_diagnosis_report.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/datamart_report_add_multi_CSV_participant_consent_diagnosis_report.sql

# -------------------------------------------------------------------------------------------------------------------
# mCode () fields
# Version: v2.8.0 demo data
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${GREEN}\t\tmCode () fields"
echo -e "${GREEN}\t\tVersion: v2.8.0 demo data\n"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/coding_icd_mcode_codes.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/coding_icd_mcode_codes.sql

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_mcode_patient_fields.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_profile_add_mcode_patient_fields.sql

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_mcode_diseases.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_diagnosis_create_mcode_diseases.sql

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_comorbid_condition.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_comorbid_condition.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_performance_status.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_performance_status.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_outcome.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_outcome.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_tumor_marker_test.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_tumor_marker_test.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_vital_sign.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_mcode_vital_sign.sql

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_radiation.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_radiation.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_surgery.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_treatment_create_mcode_surgery.sql

# -------------------------------------------------------------------------------------------------------------------
# CUSTOM FORM LIBRARIES : CLINICAL ANNOTATION
# version: v2.8.0 demo data
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${GREEN}\t\tCUSTOM FORM LIBRARIES : CLINICAL ANNOTATION"
echo -e "${GREEN}\t\tVersion: v2.8.0 demo data"

echo -e "\n${CYAN}\t\tDiagnosis"

echo -e "\n${CYAN}\t\tClinical"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_treatment_history.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_ctrnet_demo_treatment_history.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_isaric_covid19.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_isaric_covid19.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchus_onco_generic_resection_lab_report.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ../CustomFormsLibrary/clinical_annotation_event_create_crchus_onco_generic_resection_lab_report.sql

# -------------------------------------------------------------------------------------------------------------------
# CUSTOM FORM LIBRARIES : INVENTORY MANAGEMENT
# version: v2.8.0 demo data
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${GREEN}\t\tCUSTOM FORM LIBRARIES : INVENTORY MANAGEMENT"
echo -e "${GREEN}\t\tVersion: v2.8.0 demo data"

# -------------------------------------------------------------------------------------------------------------------
# Aliquot
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${CYAN}\t\tAliquot"

# -------------------------------------------------------------------------------------------------------------------
# DEMO DATA
# version: v2.8.0 demo data
# -------------------------------------------------------------------------------------------------------------------
echo -e "\n${GREEN}\t\tDEMO DATA"
echo -e "${GREEN}\t\tVersion: v2.8.0 demo data"

echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ./atim_v2.8.0_demo_data.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ./atim_v2.8.0_demo_data.sql
echo -e "${WHITE}mysql -u $databaseUserName $pass $databaseName --default-character-set=utf8 < ./custom_post_282_demo_data.sql"
mysql -u $databaseUserName $password $databaseName --default-character-set=utf8 < ./custom_post_282_demo_data.sql
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName -e UPDATE aliquot_masters SET deleted = 1 WHERE barcode like 'alq_brcd#%';"
mysql -u $databaseUserName $password $databaseName -e "UPDATE aliquot_masters SET deleted = 1 WHERE barcode like 'alq_brcd#%';"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName -e UPDATE parent_to_derivative_sample_controls SET flag_active = 1;"
mysql -u $databaseUserName $password $databaseName -e "UPDATE parent_to_derivative_sample_controls SET flag_active = 1;"
echo -e "${WHITE}mysql -u $databaseUserName $password $databaseName -e UPDATE aliquot_controls SET flag_active = 1;"
mysql -u $databaseUserName $password $databaseName -e "UPDATE aliquot_controls SET flag_active = 1;"

# -------------------------------------------------------------------------------------------------------------------
# ATiM USERS UPDATE
# Version: all
# -------------------------------------------------------------------------------------------------------------------

echo -e "\n${GREEN}\t\tATiM USERS UPDATE"
echo -e "${GREEN}\t\tVersion: all"


echo -e "${WHITE}mysql -u $databaseUserName $pass -e UPDATE users SET flag_active=0, deleted = 1, username=CONCAT('user#', id),first_name=CONCAT('user#', id), last_name=CONCAT('user#', id), password='2939a463baec8a8b1f3cef69238e6bfea3795a5c', email = '' WHERE username != 'administrator'"
mysql -u $databaseUserName $password $databaseName -e "UPDATE users SET flag_active=0, deleted = 1, username=CONCAT('user#', id),first_name=CONCAT('user#', id), last_name=CONCAT('user#', id), password='2939a463baec8a8b1f3cef69238e6bfea3795a5c', email = '' WHERE username != 'administrator'"
echo -e "${WHITE}mysql -u $databaseUserName $pass -e UPDATE users SET flag_active=1, deleted = 0, force_password_reset = 0, first_name='administrator', last_name='administrator', email = ''  WHERE username = 'administrator'"
mysql -u $databaseUserName $password $databaseName -e "UPDATE users SET flag_active=1, deleted = 0, force_password_reset = 0, first_name='administrator', last_name='administrator', email = ''  WHERE username = 'administrator'"


echo -e "${WHITE}mysql -u $databaseUserName $pass -e UPDATE groups SET flag_show_confidential = '0'  WHERE name != 'Administrators'"
mysql -u $databaseUserName $password $databaseName -e "UPDATE groups SET flag_show_confidential = '0'  WHERE name != 'Administrators'"
echo -e "${WHITE}mysql -u $databaseUserName $pass -e UPDATE groups SET flag_show_confidential = '1'  WHERE name = 'Administrators'"
mysql -u $databaseUserName $password $databaseName -e "UPDATE groups SET flag_show_confidential = '1'  WHERE name = 'Administrators'"

COLUMNS=$(tput cols) 
echo -e "${YELLOW}"
title="ATiM DEMO SET UP - DONE" 
printf "%*s\n\n\n" $(((${#title}+$COLUMNS)/2)) "$title"
echo -e "${WHITE}"
read -p "Press Enter to resume ..."